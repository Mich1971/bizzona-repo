<?
  global $DATABASE;

  include_once("./phpset.inc");
  include_once("./engine/functions.inc"); 

  $count_sql_call = 0;
  $start = get_formatted_microtime();   
  $base_memory_usage = memory_get_usage();	
  
  AssignDataBaseSetting();
    
  //ini_set("display_startup_errors", "on");
  //ini_set("display_errors", "on");
  //ini_set("register_globals", "on");

  include_once("./engine/class.city_lite.inc"); 
  $city = new City_Lite();

  include_once("./engine/class.sell_lite.inc"); 
  $sell = new Sell_Lite();

  include_once("./engine/class.RoomCategory.inc");
  $roomCategory = new RoomCategory();	
  
  include_once("./engine/class.geocode.inc"); 
  $geocode = new GeoCode();

  include_once("./engine/class.Room_Lite.inc");
  $room = new Room_Lite();
  
  	include_once("./engine/class.ad.inc");
  	$ad = new Ad();  
  
  require_once("./libs/Smarty.class.php");
  $smarty = new Smarty;
  $smarty->template_dir = "./templates";
  $smarty->compile_dir  = "./templates_c";
  $smarty->cache_dir = "./cache";
  $smarty->compile_check = true;
  $smarty->debugging     = false;
  //$smarty->caching = true; 


  require_once("./regfuncsmarty.php");
 
  $assignDescription = AssignDescription();

  $room = new Room_Lite();
  $room->Id = intval($_GET["ID"]);
  
  //$resultGetItem = $room->GetItem();
  
  // up caching call details room
  $options = array(
  		'cacheDir' => "roomcache/",
   		'lifeTime' => 2592000
  );
		
  $cache = new Cache_Lite($options);
		
  if ($data = $cache->get(''.$room->Id.'____roomitem')) {
	$resultGetItem = unserialize($data);
  } else { 
	$resultGetItem = $room->GetItem();
	$cache->save(serialize($resultGetItem));
  }
  // donw caching call details coop  

  if($resultGetItem->statusId == 17 or $resultGetItem->Id <= 0) {
    header ("Location: http://www.bizzona.ru");
  }


  
  if(!isset($_GET["nocount"])) {
     $room->UpdateCountView();                   	
  }
  

  $geocode->serviceId = intval($_GET["ID"]);
  $geocode->typeServiceId = 1;
	
  $objgeo = $geocode->GetData();
	
  if(intval($resultGetItem->latitude) > 0) {
	$objgeo->latitude = $resultGetItem->latitude;	
  }
	
  if(intval($resultGetItem->longitude) > 0) {
	$objgeo->longitude = $resultGetItem->longitude;	
  }

   $smarty->assign("headertitle", $resultGetItem->SeoTitle); 	
   
	$ad->InitAd($smarty, $_GET);   
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
  <HEAD>
  		<title><?=$resultGetItem->SeoTitle;?>, ��� "������-����"</title>
  		<META NAME="Description" CONTENT="<?=$resultGetItem->ShortDescription;?> ��� "������-����">
  		<meta name="Keywords" content="<?=$resultGetItem->SeoTitle;?>"> 
        <LINK href="http://www.bizzona.ru/general.css" type="text/css" rel="stylesheet">
        <meta http-equiv="content-type" content="text/html; charset=windows-1251"/>
		<link rel="shortcut icon" href="http://www.bizzona.ru/images/favicon.ico">
  </HEAD>

<?
  $smarty->display("./site/headerbanner.tpl");
?>

<?  
    if(strlen($objgeo->latitude) <=0 || strlen($objgeo->longitude) <= 0) {
    	$useMapGoogle = false;
    } else {
    	$useMapGoogle = ( ($_SERVER["SERVER_NAME"] == "www.bizzona.ru" or $_SERVER["SERVER_NAME"] == "bizzona.ru")  ? true : false); 
    	$useMapGoogle = (($resultGetItem->statusId == 15 or $resultGetItem->statusId == 12 or $resultGetItem->statusId == 10 or $resultGetItem->statusId == 0 or $resultGetItem->statusId == 17) ? false : $useMapGoogle);
    }
    
	if($useMapGoogle) {
		
		$zoom = $resultGetItem->zoomId;
			
?>
    			<script charset="utf-8" src="http://maps.google.com/maps?file=api&amp;v=2&amp;hl=ru&key=ABQIAAAAnavXIsM5jdni-p-JoKqdIxTPIkIa5BjXbMz0WvIzZffrVGiazxTRu-mHw0IpP9Ib5fP2129LbcNtTw"
      				type="text/javascript"></script>
    			<script type="text/javascript">
    			//<![CDATA[
    				function load() {
      					if (GBrowserIsCompatible()) {
        					var map = new GMap2(document.getElementById("map"));
        					//map.enableScrollWheelZoom(); 
        					map.setCenter(new GLatLng(<?=$objgeo->longitude;?>,<?=$objgeo->latitude;?>), <?=$zoom;?>);
        					map.addControl(new GMapTypeControl());
        					var bounds = map.getBounds();
          					var point = new GLatLng(<?=$objgeo->longitude;?>, <?=$objgeo->latitude;?>);
          					map.addOverlay(new GMarker(point));
        					map.addControl(new GSmallMapControl());
        					GEvent.addListener(map, "click", function(overlay,latlng) {
          						var lat = latlng.lat();
          						var lon = latlng.lng();
          						var latOffset = 0.01;
          						var lonOffset = 0.01;
       	  						var polygon = new GPolygon([
            					new GLatLng(lat, lon - lonOffset),
            					new GLatLng(lat + latOffset, lon),
            					new GLatLng(lat, lon + lonOffset),
            					new GLatLng(lat - latOffset, lon),
            					new GLatLng(lat, lon - lonOffset)
		  						], "#f33f00", 5, 1, "#ff0000", 0.2);
		  						map.addOverlay(polygon);
        					});
          					
          					
      					}
    				}
    			//]]>
    			</script>  
    			<body onload="load()" onunload="GUnload()">
<?
			$smarty->assign("googlemap", 1);

	}
?> 
  
<?
	$link_country = GetSubMenu();	
	$smarty->assign("ad", $link_country);
	
	$smarty->caching = false;	
	$output_header = $smarty->fetch("./site/header.tpl");
	echo minimizer($output_header);
?>


<table class="w" border="0" cellpadding="0" cellspacing="0" height="100%">
    <tr>
        <td width="30%" valign="top"  height="100%">
		<?
        	$roomcategorylist = $roomCategory->Select();
        	$smarty->assign("roomcategorylist", $roomcategorylist);
			$output_roomcategorylist = $smarty->fetch("./site/roomcategorylist.tpl");
			echo $output_roomcategorylist; 

			$output_call = $smarty->fetch("./site/call.tpl");
		    echo fminimizer($output_call);
		    
        	?><div style="padding-top:2pt;"></div><?
        	
		    $output_proposal = $smarty->fetch("./site/proposal.tpl");
		    echo fminimizer($output_proposal);
		    $output_request = $smarty->fetch("./site/request.tpl");
		    echo fminimizer($output_request);
        ?>  
        </td>
        <td width="70%" valign="top">
			<div style="width:auto;height:100%;" >
            <div class="lftpadding_cnt w" style="padding-top:10pt; padding-bottom: 5pt;height:20pt; padding-top:2pt; padding-bottom:0pt;width:auto;" >
                <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#7C3535" style="height:100%;">
                    <tr>
                    	<td valign="middle" align="left" style="padding-left:3pt; font-size:9pt;font-family:Arial;color:White;font-weight:bold; vertical-align:middle;">���: <?=$resultGetItem->Id;?></td>
                        <td valign="middle" align="center" style="padding-left:3pt; font-size:12pt;font-family:Arial;color:White;font-weight:bold; vertical-align:middle;">��������� �������� ���������</td>
                        <td valign="middle" align="right" style="padding-right:3pt; font-size:9pt;font-family:Arial;color:White;font-weight:bold; vertical-align:middle;"> (<?=strftime("%d %B %Y",  strtotime($resultGetItem->datecreate));?>)</td>
                    </tr>
                </table>
            </div>

               <?
                 if (isset($resultGetItem->Id)) {
                     $smarty->caching = true; 
                     //$smarty->cache_lifetime = 86400;
                     $smarty->cache_lifetime = 1;
                     if (!$smarty->is_cached("./site/detailsRoom.tpl", $resultGetItem->Id."-".$useMapGoogle)) {
                       $smarty->assign("data", $resultGetItem);
                     }

                     $output_detailsRoom = $smarty->fetch("./site/detailsRoom.tpl", $resultGetItem->Id."-".$useMapGoogle);
		             echo fminimizer($output_detailsRoom);
                     
                     $smarty->caching = false;  
                 }
			 ?>
			<?
				include_once("./yadirect.php");
			?>			 
        </td>
    </tr>
</table>
<div style="padding-top:4pt;padding-bottom:5pt;padding-right:10pt;padding-left:10pt;font-size:10pt;font-family:arial;">
	<table cellpadding="0" cellspacing="0" border="0"  style="width:100%;" bgcolor="#eeeee0">
    	<tr>
        	<td style="background-image:url(http://<?=$_SERVER["HTTP_HOST"]?>/images/d1.gif); background-repeat:no-repeat;width:5px;height:5px; background-position:left top;">&nbsp;</td>
            <td rowspan="2" valign="middle" align="center"  style="font-size:8pt; font-family:Arial; color:black;">
            </td>
            <td style="background-image:url(http://<?=$_SERVER["HTTP_HOST"]?>/images/d2.gif); background-repeat:no-repeat;width:5px;height:5px;background-position:right top;">&nbsp;</td>
        </tr>
        <tr>
            <td style="background-image:url(http://<?=$_SERVER["HTTP_HOST"]?>/images/d4.gif); background-repeat:no-repeat;width:5px;height:5px; background-position:left bottom;">&nbsp;</td>
            <td style="background-image:url(http://<?=$_SERVER["HTTP_HOST"]?>/images/d3.gif); background-repeat:no-repeat;width:5px;height:5px; background-position:right bottom;">&nbsp;</td>
        </tr>
    </table>
</div>
<?
	$smarty->display("./site/footer.tpl");
?>
<?
  $end = get_formatted_microtime(); 
  $total = $end - $start;
  echo "<center><span style='font-size:7pt;'>".round($total, 6)." : ".$count_sql_call." : ".$base_memory_usage." : ".memoryUsage(memory_get_usage(), $base_memory_usage)." </span><center>";
?>
<script language='javascript'>
  function AddToSearch(obj) {
	var input_search = document.getElementById("searchtext");
	if(input_search != null) {
		if(obj.innerText != null) {
			input_search.value = obj.innerText;
		} else if (obj.innerHTML != null) {
			input_search.value = obj.innerHTML;
		}
	}
  }  
</script>
</body>
</html>
<?
	$sell->Close();
	$city->Close();
	$geocode->Close();
?>
