<?
	global $DATABASE;
 
	include_once("./phpset.inc");
	include_once("./engine/functions.inc"); 

  	$count_sql_call = 0;
	$start = get_formatted_microtime();   
  	$base_memory_usage = memory_get_usage();	

	AssignDataBaseSetting();
  
  	include_once("./engine/class.category_lite.inc"); 
  	$category = new Category_Lite();

 	include_once("./engine/class.country_lite.inc"); 
  	$country = new Country_Lite();

  	include_once("./engine/class.city_lite.inc");  
  	$city = new City_Lite();

  	include_once("./engine/class.sell_lite.inc"); 
  	$sell = new Sell_Lite();

	include_once("./engine/class.broker.inc"); 
	$broker = new Broker();	
        
  	include_once("./engine/class.ad.inc");
  	$ad = new Ad();  
        
  	include_once("./engine/class.subcategory_lite.inc");
  	$subcategory = new SubCategory_Lite();

	require_once("./libs/Smarty.class.php");
	$smarty = new Smarty;
	$smarty->template_dir = "./templates";
	$smarty->compile_dir  = "./templates_c";
	//$smarty->cache_dir = "./cache";
	$smarty->compile_check = true;
	$smarty->debugging     = false;

	require_once("./regfuncsmarty.php");
  
	if (isset($_GET["brokerId"]) && intval($_GET["brokerId"]) > 0) {
		
		$_GET["brokerId"] = intval($_GET["brokerId"]);
		
		$broker->Id = $_GET["brokerId"];
		$getitem_broker = $broker->GetItem();
		
		$title = "������ ���������� �������� �".$getitem_broker->NameForSite."�";
		$smarty->assign("title", $title);
		
		if(isset($_GET["actionId"]) && intval($_GET["actionId"]) > 0) {
			$_GET["actionId"] = intval($_GET["actionId"]);
					
			if($_GET["actionId"] == 1 /* � �������� */) {
				$smarty->assign("companydata", $getitem_broker->about_forsite);
			} else if ($_GET["actionId"] == 4 /* �������� */) {
				$smarty->assign("companydata", $getitem_broker->contact_forsite);
			} else if ($_GET["actionId"] == 3 /* �������� */) { 
				$smarty->assign("companydata", $getitem_broker->vacancy_forsite);
			} else if ($_GET["actionId"] == 2 /* ������ */) { 
				$smarty->assign("companydata", $getitem_broker->service_forsite);
			} else {
				$smarty->assign("companydata", $getitem_broker->about_forsite);
			}
					
		} else {
			$smarty->assign("companydata", $getitem_broker->about_forsite);
		}
		
	} else {
		header ("Location: http://www.bizzona.ru/broker.php");
	}
	
  include_once("./engine/class.ContactInfo_Lite.inc");
  $contact = new ContactInfo_Lite();  

  $contact->GetContactInfo();  
  $smarty->assign("contact", $contact);	
  $ad->InitAd($smarty, $_GET);
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
  <HEAD>
   <title><?=$title;?></title>		
  
	<META NAME="Description" CONTENT="<?=$title;?>">
        <meta name="Keywords" content="<?=$title;?>"> 
        <LINK href="http://www.bizzona.ru/general.css" type="text/css" rel="stylesheet">
        <meta http-equiv="content-type" content="text/html; charset=windows-1251"/>
        <link rel="shortcut icon" href="http://www.bizzona.ru/images/favicon.ico">
    </HEAD>
<body>

<?
  $smarty->display("./site/headerbanner.tpl");
?>

<?
	$topmenu = GetMenu();
	$smarty->assign("topmenu", $topmenu);
	
	$link_country = GetSubMenu();	
	$smarty->assign("ad", $link_country);
?>

<div class="w">
	<?
		echo minimizer($smarty->fetch("./site/detailssearchheader.tpl"));			
	?>	
</div>

<table class="w" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td width="30%" valign="top" style="padding-top:2pt;">
        <?
        	if(isset($_GET["brokerId"])) {
        		$smarty->assign("brokerId", intval($_GET["brokerId"]));  
        	}
        
        	$smarty->caching = false; 
                echo fminimizer($smarty->fetch("./site/inner_bannertop_left.tpl"));                
	?>
        
        <?
        	if(isset($_GET["brokerId"])) {
        		ShowCategoryForBroker(intval($_GET["brokerId"]));
        	}
        ?>
        </td> 
        <td width="70%" valign="top">
			<?
				echo minimizer($smarty->fetch("./site/brokermenu.tpl"));			
				echo minimizer($smarty->fetch("./site/companydata.tpl"));			
			?>        
        </td>
    </tr>
    
	<tr>
		<td colspan="2">
		
<div style="padding-top:4pt;padding-bottom:5pt;padding-right:10pt;padding-left:10pt;font-size:10pt;font-family:arial;">
	<table cellpadding="0" cellspacing="0" border="0"  style="width:100%;padding-bottom:2pt;" bgcolor="#eeeee0">
    	<tr>
        	<td style="background-image:url(http://www.bizzona.ru/images/d1.gif); background-repeat:no-repeat;width:5px;height:5px; background-position:left top;">&nbsp;</td>
            <td rowspan="2" valign="middle" align="center"  style="font-size:8pt; font-family:Arial; color:black;">
            
<?
			echo minimizer($smarty->fetch("./site/brokersug.tpl"));			
?>
            
            </td>
            <td style="background-image:url(http://www.bizzona.ru/images/d2.gif); background-repeat:no-repeat;width:5px;height:5px;background-position:right top;">&nbsp;</td>
        </tr>
        <tr>
            <td style="background-image:url(http://www.bizzona.ru/images/d4.gif); background-repeat:no-repeat;width:5px;height:5px; background-position:left bottom;">&nbsp;</td>
            <td style="background-image:url(http://www.bizzona.ru/images/d3.gif); background-repeat:no-repeat;width:5px;height:5px; background-position:right bottom;">&nbsp;</td>
        </tr>
    </table>
</div>
		
		
		</td>
	</tr>

	<tr>
		<td colspan="2">
<?
	echo fminimizer($smarty->fetch("./site/site_city.tpl"));
	$smarty->display("./site/footer.tpl");
  	
  $end = get_formatted_microtime(); 
  $total = $end - $start;
  echo "<center><span style='font-size:7pt;'>".round($total, 6)." : ".$count_sql_call." : ".$base_memory_usage." : ".memoryUsage(memory_get_usage(), $base_memory_usage)." </span><center>";
?>		
		
		</td>
	</tr>
</table>


<script language='javascript'>
  function SellPreviewAdd() {
  	
  }
  function AddToSearch(obj) {
	var input_search = document.getElementById("searchtext");
	if(input_search != null) {
		if(obj.innerText != null) {
			input_search.value = obj.innerText;
		} else if (obj.innerHTML != null) {
			input_search.value = obj.innerHTML;
		}
	}
  }  
</script>


</body>
</html>
<?
	$sell->Close();
	$category->Close();
	$city->Close();
	$country->Close();
	$subcategory->Close();
?>