<script src="http://www.bizzona.ru/js/jquery.js"></script>

<?php
	global $DATABASE;

	include_once("./engine/functions.inc"); 

	$start = get_formatted_microtime(); 
	
	$ENTER_SERBER_NAME = $_SERVER["SERVER_NAME"];
	

	InitServerName();
	
	include_once("./phpset.inc");

	ini_set("display_startup_errors", "on");
	ini_set("display_errors", "on");
	ini_set("register_globals", "on");
	
	AssignDataBaseSetting();

	include_once("./engine/class.category.inc");
	$category = new Category();

	include_once("./engine/class.subcategory.inc");
	$subcategory = new SubCategory();
	
	include_once("./engine/class.city.inc");  
	$city = new City();
	
	
	$data = array();
	$data["offset"] = 0;
	$data["rowCount"] = 0;
	
	$listCategory = $category->Select($data);
	
	$listSubCategory = $subcategory->Select($data);
	
	
	GenerateArrayCat($listSubCategory);	

	
	$listRegion = $city->GetRegionbyParent(0, false, false);
	$listRegion_90 = $city->GetRegionbyParent(90, false, false);
	$listRegion_91 = $city->GetRegionbyParent(91, false, false);
	$listRegion_92 = $city->GetRegionbyParent(92, false, false);
	$listRegion_93 = $city->GetRegionbyParent(93, false, false);
	$listRegion_94 = $city->GetRegionbyParent(94, false, false);
	$listRegion_95 = $city->GetRegionbyParent(95, false, false);
	$listRegion_96 = $city->GetRegionbyParent(96, false, false);
	
?>

<script language="javascript">

	function SubCatLoad(obj) {
		var obj_subcat = document.getElementById("subCat");
		clearSelect(obj_subcat);
		var selected = obj.value;
		for(var i in subArr) {  
			var temp = subArr[i];
			if(temp[0] == selected) {	
				addOption(obj_subcat, temp[1], i);
			}
		}
		
		DisabledCtrl();
	}

	function addOption (oListbox, text, value, isDefaultSelected, isSelected)
	{
  		var oOption = document.createElement("option");
  		oOption.appendChild(document.createTextNode(text));
  		oOption.setAttribute("value", value);

  		if (isDefaultSelected) oOption.defaultSelected = true;
  		else if (isSelected) oOption.selected = true;

  		oListbox.appendChild(oOption);
	}
	
	function clearSelect(oListbox)
	{
  		for (var i=oListbox.options.length-1; i > 0; i--)
  		{
      		oListbox.remove(i);
  		}
	}

	function DistrictLoad(obj) {
		$.post(
  			'/cabinet/engine/cabinet/district.php',
  		{
    		action: "get",
    		id: obj.value
  		},
  			onAjaxSuccess
		);

		function onAjaxSuccess(data) {
  			var districtId = document.getElementById("districtId");
  			clearSelect(districtId);
  			
  			$("item", data).each(
  				function() {
					try {  				
						addOption(districtId, $(this).text(), $(this).attr('id'));
					} catch(e) {
						//window.alert(e.description);
					}
  				
  				}
  			);

  			DisabledCtrl();	 			
		}		
	}
	
	
	function MetroLoad(obj) {
		$.post(
  			'/cabinet/engine/cabinet/metro.php',
  		{
    		action: "get",
    		id: obj.value
  		},
  			onAjaxSuccess
		);

		function onAjaxSuccess(data) {
  			var metroId = document.getElementById("metroId");
  			clearSelect(metroId);
  			
  			$("item", data).each(
  				function() {
					try {  				
						addOption(metroId, $(this).text(), $(this).attr('id'));
					} catch(e) {
						//window.alert(e.description);
					}
  				
  				}
  			);  			
  		
  			DisabledCtrl();	
		}			
		
		DistrictLoad(obj);
	}
	
	function SubCityLoad(obj) {
		
		$.post(
  			'/cabinet/engine/cabinet/region.php',
  		{
    		action: "get",
    		id: obj.value
  		},
  			onAjaxSuccess
		);	
		
		function onAjaxSuccess(data) {
  			var subCityId = document.getElementById("subCityId");
  			clearSelect(subCityId);
  			
  			$("item", data).each(
  				function() {
					try {  				
						addOption(subCityId, $(this).text(), $(this).attr('id'));
					} catch(e) {
						//window.alert(e.description);
					}
  				
  				}
  			);  			
  		
  			DisabledCtrl();	
		}

		DistrictLoad(obj);

		MetroLoad(obj);	
	}

</script>


<div class="lftpadding_cnt w" style="padding-top:10pt; padding-bottom: 5pt;padding-top:0pt; padding-bottom:0pt;width:auto;" >

<h1>����������� ����� �������� �� �������</h1>

<form method="GET" action="./detailssearch.php">

<table bgcolor="#eeeee0" style="border-top: 1px;  border-bottom: 1px;  border-left:1px;   border-right:1px;   border-color:#C1C1A4;   border-style: solid;">
<tr>

<td>

<p style="padding:2pt;margin:2pt;font-weight:normal;font-size:10pt;" class="a_nm_h">����� ����� �������</p>

<select onchange="SubCatLoad(this)" name="catId" style="font-size:10pt;">
<option value="0" style="background-color:#EDEDCC;color:#000000;font-weight:bold;">�������</option>
<?
	while (list($k, $v) = each($listCategory)) {
		?><option value="<?=$v->ID;?>"><?=$v->Name;?></option><?
	}
?>
</select>

</td>

<td colspan="3" align="left">

<p  style="padding:2pt;margin:2pt;font-weight:normal;font-size:10pt;" class="a_nm_h">����� ������������ �������</p>

<select id="subCat" name="subCatId" style="width:200pt;font-size:10pt;">
	<option value="0" style="background-color:#EDEDCC;color:#000000;font-weight:bold;">�������</option>
</select>

</td>

</tr>

<tr>

<td>
<p  style="padding:2pt;margin:2pt;font-weight:normal;font-size:10pt;" class="a_nm_h">����� �������/����/�������</p>
<select onchange="SubCityLoad(this)" name="cityId" style="font-size:10pt;">
	<option value="0" style="background-color:#EDEDCC;color:#000000;font-weight:bold;">�������</option>
	<option value="77">������</option>
	<option value="78">�����-���������</option>
	<option value="50">���������� �������</option>
<?
	while (list($k, $v) = each($listRegion)) {
		if(strlen($v->title) > 0) {
			if($v->ID == "90" or $v->ID == "91"  or $v->ID == "92"  or $v->ID == "93"  or $v->ID == "94"  or $v->ID == "95"  or $v->ID == "96") {
				?> <optgroup label="<?=$v->title;?>" style="background-color:#EDEDCC;color:#000000;"> <? 
					if($v->ID == "90") {
						while (list($k_90, $v_90) = each($listRegion_90)) {
							if(($v_90->ID != $v->ID) && ($v_90->ID != 77) && ($v_90->ID != 50) ) {
								?><option style="background-color:#ffffff;color:#000000;"  value="<?=$v_90->ID;?>"><?=$v_90->title;?></option><?							
							}
						}
					}
					if($v->ID == "91") {
						while (list($k_91, $v_91) = each($listRegion_91)) {
							if(($v_91->ID != $v->ID)  && ($v_91->ID != 78)) {
								?><option style="background-color:#ffffff;color:#000000;" value="<?=$v_91->ID;?>"><?=$v_91->title;?></option><?							
							}
						}
					}	
					if($v->ID == "92") {
						while (list($k_92, $v_92) = each($listRegion_92)) {
							if($v_92->ID != $v->ID) {
								?><option style="background-color:#ffffff;color:#000000;" value="<?=$v_92->ID;?>"><?=$v_92->title;?></option><?							
							}
						}
					}		
					if($v->ID == "93") {
						while (list($k_93, $v_93) = each($listRegion_93)) {
							if($v_93->ID != $v->ID) {
								?><option style="background-color:#ffffff;color:#000000;" value="<?=$v_93->ID;?>"><?=$v_93->title;?></option><?							
							}
						}
					}												
					if($v->ID == "94") {
						while (list($k_94, $v_94) = each($listRegion_94)) {
							?><option style="background-color:#ffffff;color:#000000;" value="<?=$v_94->ID;?>"><?=$v_94->title;?></option><?							
						}
					}					
					if($v->ID == "95") {
						while (list($k_95, $v_95) = each($listRegion_95)) {
							if($v_95->ID != $v->ID) {
								?><option style="background-color:#ffffff;color:#000000;" value="<?=$v_95->ID;?>"><?=$v_95->title;?></option><?							
							}
						}
					}			
					if($v->ID == "96") {
						while (list($k_96, $v_96) = each($listRegion_96)) {
							if($v_96->ID != $v->ID) {
								?><option style="background-color:#ffffff;color:#000000;" value="<?=$v_96->ID;?>"><?=$v_96->title;?></option><?							
							}
						}
					}							
				?> </optgroup><?
			} else {
				?><option value="<?=$v->ID;?>"><?=$v->title;?></option><?
			}
		}
	}
?>
</select>

</td>

<td>
<p style="padding:2pt;margin:2pt;font-weight:normal;font-size:10pt;" class="a_nm_h">����� ������</p>
<select id="subCityId"  onchange="MetroLoad(this)" name="subCityId" style="width:120pt;font-size:10pt;">
<option value="0" style="background-color:#EDEDCC;color:#000000;font-weight:bold;">�������</option>
</select>

</td>

<td>
<p style="padding:2pt;margin:2pt;font-weight:normal;font-size:10pt;" class="a_nm_h">����� ������</p>
<select id="districtId"  name="districtId"  style="width:100pt;font-size:10pt;">
<option value="0" style="background-color:#EDEDCC;color:#000000;font-weight:bold;">�������</option>
</select>
</td>

<td>
<p style="padding:2pt;margin:2pt;font-weight:normal;font-size:10pt;" class="a_nm_h">����� �����</p>
<select id="metroId" name="metroId"  style="width:120pt;font-size:10pt;">
<option value="0" style="background-color:#EDEDCC;color:#000000;font-weight:bold;">�������</option>
</select>


<input type="submit" value="�����">

</td>

</tr>

</table>

</form>

<script language="javascript">
	function DisabledCtrl() {
		var _subCatId = document.getElementById("subCat");
		if(_subCatId.length <= 1) {
			_subCatId.disabled = true;
		} else {
			_subCatId.disabled = false;
		}
		
		var _subCityId = document.getElementById("subCityId");
		if(_subCityId.length <= 1) {
			_subCityId.disabled = true;
		} else {
			_subCityId.disabled = false;
		}		
		
		var _metroId = document.getElementById("metroId");
		if(_metroId.length <= 1) {
			_metroId.disabled = true;
		} else {
			_metroId.disabled = false;
		}	
		
		var _districtId = document.getElementById("districtId");
		if(_districtId.length <= 1) {
			_districtId.disabled = true;
		} else {
			_districtId.disabled = false;
		}		
			
	}
	DisabledCtrl();
	
</script>



</div>