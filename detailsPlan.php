<?
  global $DATABASE;

  include_once("./phpset.inc");
  include_once("./engine/functions.inc"); 
  include_once("./engine/plan_functions.inc"); 

  $count_sql_call = 0;
  $start = get_formatted_microtime();   
  $base_memory_usage = memory_get_usage();	


  AssignDataBaseSetting();
  
  include_once("./engine/class.category_lite.inc"); 
  $category = new Category_Lite();

  include_once("./engine/class.country_lite.inc"); 
  $country = new Country_Lite();

  include_once("./engine/class.city_lite.inc");  
  $city = new City_Lite();

  include_once("./engine/class.bizplan.inc"); 
  $bizplan = new BizPlan();

  include_once("./engine/class.bizplan_company.inc"); 
  $bizplan_company = new BizPlanCompany();
  
  include_once("./engine/class.buy_lite.inc"); 
  $buyer = new Buyer_Lite();

  include_once("./engine/class.sell_lite.inc"); 
  $sell = new Sell_Lite();

  	include_once("./engine/class.district_lite.inc"); 
  	$district = new District_Lite();	

  	include_once("./engine/class.subcategory_lite.inc");
  	$subcategory = new SubCategory_Lite();	

  	include_once("./engine/class.streets_lite.inc");
  	$streets = new Streets_Lite();	
  
  	include_once("./engine/class.ad.inc");
  	$ad = new Ad();   	
  	
  require_once("./libs/Smarty.class.php");
  $smarty = new Smarty;
  $smarty->template_dir = "./templates";
  $smarty->compile_dir  = "./templates_c";
  $smarty->cache_dir = "./cache";
  $smarty->compile_check = true;
  $smarty->debugging     = false;
  //$smarty->caching = true; 

  require_once("./regfuncsmarty.php");
  
  $assignDescription = AssignDescription();

  $bizplan->id = intval($_GET["ID"]);	

  $resultGetItem = $bizplan->GetItem();
  
  $headertitle = "������� ������-������";
  $smarty->assign("headertitle", $headertitle);
  
	$ad->InitAd($smarty, $_GET);  
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
  <HEAD>
      <title>������� ������-������ - ��� "������ ����"</title>		
	<META NAME="Description" CONTENT='������� ������-������- ��� "������ ����"'>
    <meta name="Keywords" content="������� ������-������"> 
    <LINK href="http://www.bizzona.ru/general.css" type="text/css" rel="stylesheet">
    <meta http-equiv="content-type" content="text/html; charset=windows-1251"/>
  </HEAD>
<?
  $smarty->display("./site/headerbanner.tpl");
?>
<?
	$smarty->display("./site/headerplan.tpl");
?>

<table class="w" border="0" cellpadding="0" cellspacing="0" height="100%">
    <tr>
        <td width="30%" valign="top"  height="100%">
         
        <?
          $smarty->cache_dir = "./persistentcache"; 
          $smarty->cache_lifetime = 10800;
                  
          if (isset($_GET["action"]) && $_GET["action"]  == "category") {
            $_GET["ID"] = (isset($_GET["ID"])  ? intval($_GET["ID"]) : 0);
            $smarty->caching = true; 
            if (!$smarty->is_cached('./site/categorysp.tpl', $_GET["ID"])) {
              $data = array();
              $data["offset"] = 0;
              $data["rowCount"] = 0;
              $data["sort"] = "CountP";
              $result = $category->Select($data);
              $smarty->assign("data", $result);
            }
            $smarty->display("./site/categorysp.tpl", $_GET["ID"]);
            $smarty->caching = false; 
          } else { 
            $smarty->caching = true; 
            if (!$smarty->is_cached('./site/categorysp.tpl')) {
              $data = array();
              $data["offset"] = 0;
              $data["rowCount"] = 0;
              $data["sort"] = "CountP";
              $result = $category->Select($data);
              $smarty->assign("data", $result);
            }
            $smarty->display("./site/categorysp.tpl");
            $smarty->caching = false; 
          }
          
          $smarty->cache_dir = "./cache";
          $smarty->cache_lifetime = 3600;          
          
          $smarty->display("./site/call.tpl");
        ?>

		<div style="padding-top:2pt;"></div>                    
        <?
        	$smarty->display("./site/proposal.tpl");
         	$smarty->display("./site/request.tpl");
        ?>  
        
        </td>
        <td width="70%" valign="top">
			<div style="width:auto;height:100%;" >
        
				<?
					echo UpPositionAndShare(intval($_GET["ID"]));
					
       				$res_company = $bizplan_company->SelectByStatus(OPEN_BIZPLAN_COMPANY);
       				$smarty->assign("data", $res_company);
           			$smarty->display("./site/bizplancompany.tpl");
           			
       				echo PlanHeaderShow(intval($_GET["ID"]));
				?>
                <?
                 if (isset($resultGetItem->id)) {
                     $smarty->caching = true; 
                     $smarty->cache_lifetime = 86400;
                     if (!$smarty->is_cached("./site/detailsPlan.tpl", $resultGetItem->id)) {
                     	$smarty->assign("data", $resultGetItem);
                     }

                     $smarty->display("./site/detailsPlan.tpl", $resultGetItem->id);
                     $smarty->caching = false;  

                     $bizplan->IncrementQView();
                     
                 }
                 
               ?>
               
				<?
					echo PlanHeaderShow(intval($_GET["ID"]));
					echo UpPositionAndShare(intval($_GET["ID"]));
				?>
               
            </div>
        </td>
    </tr>
</table>

<style>
 td a 
 {
   color:black;
 }
</style>

<div style="padding-top:4pt;padding-bottom:5pt;padding-right:10pt;padding-left:10pt;font-size:10pt;font-family:arial;">
	<table cellpadding="0" cellspacing="0" border="0"  style="width:100%;" bgcolor="#eeeee0">
    	<tr>
        	<td style="background-image:url(http://<?=$_SERVER["HTTP_HOST"]?>/images/d1.gif); background-repeat:no-repeat;width:5px;height:5px; background-position:left top;">&nbsp;</td>
            <td rowspan="2" valign="middle" align="center"  style="font-size:8pt; font-family:Arial; color:black;">
            </td>
            <td style="background-image:url(http://<?=$_SERVER["HTTP_HOST"]?>/images/d2.gif); background-repeat:no-repeat;width:5px;height:5px;background-position:right top;">&nbsp;</td>
        </tr>
        <tr>
            <td style="background-image:url(http://<?=$_SERVER["HTTP_HOST"]?>/images/d4.gif); background-repeat:no-repeat;width:5px;height:5px; background-position:left bottom;">&nbsp;</td>
            <td style="background-image:url(http://<?=$_SERVER["HTTP_HOST"]?>/images/d3.gif); background-repeat:no-repeat;width:5px;height:5px; background-position:right bottom;">&nbsp;</td>
        </tr>
    </table>
</div>

<?
	$smarty->display("./site/footer.tpl");
?>

<?
  $end = get_formatted_microtime(); 
  $total = $end - $start;
  echo "<center><span style='font-size:7pt;'>".round($total, 6)." : ".$count_sql_call." : ".$base_memory_usage." : ".memoryUsage(memory_get_usage(), $base_memory_usage)." </span><center>";
?>

</body>
</html>