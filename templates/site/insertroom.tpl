{literal}
<script language="javascript">
  String.prototype.trim = function() {
  	return this.replace(/(^\s+|\s+$)/g, "");
  }

  function ChangeBorder(obj)
  {
  	try
  	{
		if(obj.value.trim().length > 0) 
		{
			obj.style.borderColor='green';
		}
		else
		{
			obj.style.borderColor='red';
			obj.value = "";
		}
  	}
  	catch(e)
  	{
  		
  	}
  }
</script>
{/literal}

   {if $code > 0 && $insertsettings.type_show eq 1}
       
        {include file="./site/confirmpaymentroom.tpl"}
        
    {elseif $code > 0 && ($insertsettings.type_show eq 2 or $insertsettings.type_show eq 3)}    

        <table width='100%' align='center' bgcolor='#EDEDCC' style='border-top: 1px; border-bottom: 1px; border-left:1px;  border-right:1px;  border-color:#C1C1A4;  border-style: solid;' cellpadding="0" cellspacing="0" >
            <tr>
              <td valign='top' style='padding-left:5pt;padding-right:5pt;'>
                <h2>���� ���������� �� ������� ��������� �������!</h2>
              </td>
            </tr>
        </table>        
        
   {else}

<form  enctype="multipart/form-data" method="post">

    <table width='100%' align='center' bgcolor='#EDEDCC' style='border-top: 1px; border-bottom: 1px; border-left:1px;  border-right:1px;  border-color:#C1C1A4;  border-style: solid;' cellpadding="0" cellspacing="0" >
    
    <tr>
      <td  valign='top' style='padding-left:5pt;padding-right:5pt;'> 
        <div align="center" style="padding-top:5pt;padding-bottom:5pt;font-size:14pt;font-weight:bold;">���������� ���������� �� ������� ���������</div>
        {$insertsettings.content}
        {if $insertsettings.use_promo} 
            {include file='./site/promo.tpl'}
        {/if}        
      </td>
      <td width="25%" valign="top" style="padding-top:5pt;padding-right:5pt;">
      	{include file='./site/requestmin.tpl'}
      </td>
    </tr>
    
   </table>

  <input type='hidden' name='insert' value='insert'>
  <center> 
  <span style='color:red;'>*</span><span style='font-size:10pt;font-family:arial;'> ���������� ���� ����������� ��� ����������</span>
  </center> 


<table width='80%' align='center'>
  <tr>
   <td valign='top' width='30%'>

  <table width='100%' align='center' bgcolor='#ffffff' style='border-top: 1px; border-bottom: 1px; border-left:1px;  border-right:1px;  border-color:#C1C1A4;  border-style: solid;' cellpadding="0" cellspacing="0">

    <tr bgcolor='#E1E1C1' align='left'>
      <td  valign='top' align='left' colspan=2 style='padding-left:5px;padding-top:3px;padding-bottom:3px;'><span style='font-size:10pt;font-family:arial;color:#747464'><b>���������� ������</b></span></td>
    </tr>

    <tr>
      <td  valign='top' align='left' bgcolor='#EDEDCC' width='30%' style='padding-left:5px;'>
        <span style='color:red;'>*</span>&nbsp;<span style='font-size:8pt;font-family:tahoma;color:#747464;'><b>{$Description.FML}</b>&nbsp;(������� �����������)</span>
      </td>
    </tr>         

    <tr>
      <td valign='top' align='left' bgcolor='#EDEDCC' width='30%' style='padding-left:5px;'>
         <input type='text' size=40 id="FML" name="FML" value="{$smarty.post.FML}" maxlength='40' class="{if $ErrorInput_FML}inputErrorData{else}inputDataMandatory{/if}" onclick='this.className="inputDataMandatory"'  onblur="ChangeBorder(this);">
      </td>
    </tr>         


    <tr>
      <td valign='top' align='left' bgcolor='#EDEDCC' width='30%' style='padding-left:5px;'>
        <span style='color:red;'>*</span>&nbsp;<span style='font-size:8pt;font-family:tahoma;color:#747464;'><b>{$Description.PhoneID}</b>&nbsp;(������� �����������)</span>
      </td>
    </tr>         

    <tr>
      <td valign='top' align='left' bgcolor='#EDEDCC' width='30%' style='padding-left:5px;'>
         <input type='text' id="PhoneID" size=40 name="PhoneID" value="{$smarty.post.PhoneID}" maxlength='40' class="{if $ErrorInput_PhoneID}inputErrorData{else}inputDataMandatory{/if}" onclick='this.className="inputDataMandatory"' onblur="ChangeBorder(this);">
      </td>
    </tr>         



    <tr>
      <td valign='top' align='left' bgcolor='#EDEDCC' width='30%' style='padding-left:5px;'>
        <span style='font-size:8pt;font-family:tahoma;color:#747464;'><b>{$Description.FaxID}</b>&nbsp;(������� �����������)</span>
      </td>
    </tr>         

    <tr>
      <td valign='top' align='left' bgcolor='#EDEDCC' width='30%' style='padding-left:5px;'>
         <input type='text'  size=40 name="FaxID" value="{$smarty.post.FaxID}" maxlength='40' style='border-top: 1px; border-bottom: 1px; border-left:1px;  border-right:1px;  border-color:#C1C1A4;  border-style: solid;'>
      </td>
    </tr>         


    <tr>
      <td valign='top' align='left' bgcolor='#EDEDCC' width='30%' style='padding-left:5px;'>
        <span style='color:red;'>*</span>&nbsp;<span style='font-size:8pt;font-family:tahoma;color:#747464;'><b>{$Description.EmailID}</b>&nbsp;(������� �����������)</span>
      </td>
    </tr>         

    <tr>
      <td valign='top' align='left' bgcolor='#EDEDCC' width='30%' style='padding-left:5px;padding-bottom:5px;padding-right:5px;'>
         <input type='text' id="EmailID" size=40 name="EmailID" value="{$smarty.post.EmailID}" maxlength='40' class="{if $ErrorInput_EmailID}inputErrorData{else}inputDataMandatory{/if}" onclick='this.className="inputDataMandatory"'  onblur="ChangeBorder(this);">
      </td>
    </tr>         

  </table>
  
     </td>
     <td valign='top' width='70%'>

  <table width='100%' align='center' bgcolor='#ffffff' style='border-top: 1px; border-bottom: 1px; border-left:1px;  border-right:1px;  border-color:#C1C1A4;  border-style: solid;' cellpadding="0" cellspacing="0">

    <tr bgcolor='#E1E1C1'>
      <td  valign='top' align='left' colspan=2 style='padding-left:5px;padding-top:3px;padding-bottom:3px;'><span style='font-size:10pt;font-family:tahoma;color:#747464'><b>�������� ���������� � ���������</b></span></td>
    </tr>

    <tr>
      <td  align='left' valign='top' align='left' bgcolor='#EDEDCC' width='45%' style='padding-left:5px;padding-bottom:5px;padding-right:5px;'>
         <table cellpadding="0" cellspacing="0">
           <tr>
             <td align='left'>
               <span style='color:red;'>*</span>&nbsp;<span style='font-size:8pt;font-family:tahoma;color:#747464;'><b>��������� ���������</b></span>
             </td>
           </tr>
           <tr>
             <td>
               <nobr>
               <input type='text' size=20 id="CostID" name="CostID" value="{$smarty.post.CostID}" maxlength='40' class="{if $ErrorInput_CostID}inputErrorData{else}inputDataMandatory{/if}" onclick='this.className="inputDataMandatory"' onblur="ChangeBorder(this);">
               <select style='font-size:9pt;font-family:arial;width:45pt;padding-left:2pt;' name="cCostID"><option value=2 {if $smarty.post.cCostID eq "2"}selected{else}{/if}>���<option value=1 {if $smarty.post.cCostID eq "1"}selected{else}{/if}>USD<option value=3 {if $smarty.post.cCostID eq "3"}selected{else}{/if}>EUR</select>
               </nobr>
             </td>
           </tr>
          
           <tr>
              <td>
                <span style='font-size:8pt;font-family:tahoma;color:#747464;'><b>������� ���������</b></span>
              </td> 
           </tr>
           <tr>
             <td>
                <input type='text'  size=50 name="areaID" value="{$smarty.post.areaID}" maxlength='40' class="{if $ErrorInput_areaID}inputErrorData{else}inputDataMandatory{/if}" onclick='this.className="inputDataMandatory"' onblur="ChangeBorder(this);">
             </td> 
          </tr>

                <tr>
                  <td>
                   <span style='font-size:8pt;font-family:tahoma;color:#747464;'><b>��� ���������</b></span>
                  </td> 
                </tr>

                <tr>
                 <td> 
                    <select name="CategoryID" class='inputData' style="width:100%;" >
                      <option value='-1'>������</option>
             			{foreach name='category' item=item key=key from=$listCategory}
       	  					<option style="background-color:#ffffff;" value="{$item->Id}" {if $smarty.post.CategoryID eq $item->Id}selected{/if} >{$item->Name}</option>
             			{/foreach}
                    </select>
                  </td>
                 </tr>

    <tr bgcolor='#E1E1C1' align='left'>
      <td  valign='top' align='left' colspan=2 style='padding-left:5px;padding-top:3px;padding-bottom:3px;'><span style='font-size:10pt;font-family:tahoma;color:#747464'><b>������ �� YouTube �����</b></span></td>
    </tr>

    <tr>
       <td style='padding-left:5px;'>
          <input type='text' size=50  name="youtubeURL" value="{$smarty.post.youtubeURL}" maxlength='150'  class='inputData'>
       </td> 
    </tr>
          
           
         </table>
      </td>
      <td valign='top' bgcolor='#EDEDCC'>
         <table cellpadding="0" cellspacing="0">
           <tr>
             <td>
               <span style='color:red;'>*</span>&nbsp;</span><span style='font-size:8pt;font-family:tahoma;color:#747464;'><b>{$Description.SiteID}</b></span>
             </td> 
           </tr>
           <tr>
             <td>
               <textarea id="SiteID" name='SiteID' cols='35' rows='5' class="{if $ErrorInput_SiteID}inputErrorData{else}inputDataMandatory{/if}"  onclick='this.className="inputDataMandatory"' onblur="ChangeBorder(this);">{$smarty.post.SiteID}</textarea>
             </td> 
           </tr>
           
          <tr>
           	<td>
            	<span style='color:red;'>*</span>&nbsp;<span style='font-size:8pt;font-family:tahoma;color:#747464;'><b>������� ���������� � ����������� ���������</b></span>
            </td> 
          </tr>
          <tr>
          	<td> 
            	<textarea id="ShortDetailsID" name='ShortDetailsID' cols='35' rows='8' class="{if $ErrorInput_ShortDetailsID}inputErrorData{else}inputDataMandatory{/if}"  onclick='this.className="inputDataMandatory"' onblur="ChangeBorder(this);">{$smarty.post.ShortDetailsID}</textarea>
            </td> 
          </tr>
 
         </table> 
      </td>
    </tr>

  </table> 

     </td>
    </tr>
  </table>

  <br>


<table width='80%' align='center'>
  <tr>
   <td>


<br>

  <table width='80%' align='center' bgcolor='#EDEDCC' style='border-top: 1px; border-bottom: 1px; border-left:1px;  border-right:1px;  border-color:#C1C1A4;  border-style: solid;' cellpadding="0" cellspacing="0">


    <tr bgcolor='#E1E1C1' align='left'>
      <td  valign='top' align='left' colspan=2 style='padding-left:5px;padding-top:3px;padding-bottom:3px;'><span style='font-size:10pt;font-family:tahoma;color:#747464'><b>����������</b></span></td>
    </tr>


    <tr>
       <td style='padding-left:5px;'>
          <span style='font-size:8pt;font-family:tahoma;color:#747464;'><b>������ ����� ������ ���� �� ������ 512����� � ����� ���������� (gif, jpg, png, jpeg)</b></span>
       </td> 
    </tr>
    <tr>
       <td style='padding-left:5px;'>
          <input type='file' size=50 name="ImgID" class='inputData'>
       </td> 
    </tr>


    <tr>
      <td  valign='top' align='left' colspan=2><span style='font-size:8pt;font-family:verdana;'>&nbsp;</span></td>
    </tr>


    <tr>
      <td colspan=2 align='right' style='padding-right:5px;padding-bottom:5px;'><input type='submit' value='��������� ������' class='inputData' style='cursor:hand;'></td>
    </tr>

    </table>
    
    <br>

    </td>
   </tr>
     
  </table>


       <center>
         <span style='font-size:12px;font-family:arial;' align=center>
         </span>
       </center>   

</form>
{/if}

{literal}
<script language="javascript">
 
	try
	{
		var obj_FML = document.getElementById("FML");
		ChangeBorder(obj_FML);

		var obj_PhoneID = document.getElementById("PhoneID");
		ChangeBorder(obj_PhoneID);
		
		var obj_EmailID = document.getElementById("EmailID");
		ChangeBorder(obj_EmailID);
		
		var obj_CostID = document.getElementById("CostID");
		ChangeBorder(obj_CostID);
		
		var obj_SiteID = document.getElementById("SiteID");
		ChangeBorder(obj_SiteID);
		
		var obj_ShortDetailsID = document.getElementById("ShortDetailsID");
		ChangeBorder(obj_ShortDetailsID);

		var obj_areaID = document.getElementById("areaID");
		ChangeBorder(obj_areaID);		
		
	}
	catch(e)
	{
		
	}
</script>
{/literal}
