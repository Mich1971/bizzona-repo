<table width='100%' align='center' bgcolor='#EDEDCC' style='border-top: 1px; border-bottom: 1px; border-left:1px;  border-right:1px;  border-color:#C1C1A4;  border-style: solid;' cellpadding="0" cellspacing="0" >
    <tr>   
		<td  valign='top'>
			<table cellpadding="0" cellspacing="0" border="0" width="100%">
				<tr> 
					<td class="d_pro_b"><span style="color:#7C3535;">C��� ������� - �� 30 000 ���.</span></td>
				</tr>
				<tr>
					<td style="padding-left:13pt;padding-top:6pt;padding-bottom:6pt;">
        				<span style='font-size:11pt;font-family:arial;'> 
							� ��������� ������ ��������� ������������ ������: 
							<ul>
							<li>������� ������ �����
							<li>��������� ��������� � ����������� ����������
							</ul>
		 				</span>	
		 				
						<span style='font-size:11pt;font-family:arial;'>		 				
         					� ���������� ������ ������� �����:
         					<ol>
         						<li>� ��������, ������</li>
         						<li>�������</li>
         						<li>�����-���� ��� ����������</li>
         						<li>�������� ��� ������� � ������ (FAQ)</li>
         						<li>��������</li>
         					</ol>

         					������������ ���������� �� ����������:
         					<ol>
         						<li>PHP + MySQL</li>
         						<li>ASP.net + MSSQL</li>
         					</ol>     
         					
							<span style="text-decoration:underline;">����� ���������� (��� ������������� �������������� ���� ����������� ����������) - 7 ������� ����, � ������� ������������ ������-������. </span>
							<br><br>
							Email: <a href="mailto:bizzona.ru@gmail.com">bizzona.ru@gmail.com</a>   
         				</span>
         
					</td>
				</tr>
				<tr>
					<td><hr></td>
				</tr>
				<tr>
					<td class="d_pro_b"><span style="color:#7C3535;">������ ���� - �� 50 000 ���.</span></td>
				</tr>
				<tr>
					<td style="padding-left:13pt;padding-top:6pt;padding-bottom:6pt;">
        				<span style='font-size:11pt;font-family:arial;'>
							� ��������� ������ ��������� ������������ ������: 
							<ul>
							<li>�������������� ������ �����
							<li>��������� ��������� � ����������� ����������
							</ul>
		 				</span>	
		 				
						<span style='font-size:11pt;font-family:arial;'>		 				
         					� ���������� ������ ������� �����:
         					<ol>
         						<li>� ��������</li>
         						<li>�������</li>
         						<li>������</li>
         						<li>������� ��������� / �����-���� (�� ��������� ��������� ������� � ������� �� ����� 50 ������� ������/�����)</li>
         						<li>����� ������ ������, ������
         						<li>�������� ��� ������� � ������ (FAQ)</li>
         						<li>��������</li>
         					</ol>

         					������������ ���������� �� ����������:
         					<ol>
         						<li>PHP + MySQL (AJAX, Web Services, Smarty, XSLT � ������ ��������� ����������)</li>
         						<li>ASP.net + MSSQL (AJAX, Web Services, XSLT � ������ ��������� ����������)</li>
         					</ol>    
         					
							<span style="text-decoration:underline;">����� ���������� (��� ������������� �������������� ���� ����������� ����������) - 14 ������� ����, � ������� ������������ ������-������. </span>
							<br><br>
							Email: <a href="mailto:bizzona.ru@gmail.com">bizzona.ru@gmail.com</a>   
         				</span>
         
					</td>
				</tr>
				<tr>
					<td><hr></td>
				</tr>
				<tr>
					<td class="d_pro_b"><span style="color:#7C3535;">���������������� ����  - �� 75 000 ���.</span></td>
				</tr>
				<tr>
					<td style="padding-left:13pt;padding-top:6pt;padding-bottom:6pt;">
        				<span style='font-size:11pt;font-family:arial;'>
							� ��������� ������ ��������� ������������ ������: 
							<ul>
							<li>������������ ������ ����� � ����������� ���������� �����
							<li>��������� ��������� � ����������� ����������
							</ul>
		 				</span>	
		 				
						<span style='font-size:11pt;font-family:arial;'>		 				
         					� ���������� ������ ������� �����:
         					<ol>
         						<li>� �������� (�������������� ���������� �����������)</li>
         						<li>�������</li>
         						<li>������ (�������������� ���������� �����������)</li>
         						<li>������� ��������� / �����-���� (�� ��������� ��������� ������� � ������� �� ����� 50 ������� ������/�����)</li>
         						<li>����� ������ ������, ������
         						<li>�������� ��� ������� � ������ (FAQ)</li>
                                <li>����������� ��� ����� ���������� ��� ������� ����������� � �.�</li>
         						<li>�������� ���� ��� �������� � ������������ �����������, ���� ������ ��� ������������������ ������������� �� ������</li>
         						<li>��������</li>
         					</ol>

         					������������ ���������� �� ����������:
         					<ol>
         						<li>PHP + MySQL (AJAX, Web Services, Smarty, XSLT � ������ ��������� ����������)</li>
         						<li>ASP.net + MSSQL (AJAX, Web Services, XSLT � ������ ��������� ����������)</li>
         					</ol>    
         					 
							<span style="text-decoration:underline;">����� ���������� (��� ������������� �������������� ���� ����������� ����������) - 21 ������� ����, � ������� ������������ ������-������. </span>
							<br><br>
							Email: <a href="mailto:bizzona.ru@gmail.com">bizzona.ru@gmail.com</a>   
         				</span>
         
					</td>
				</tr>
				
			</table>
		</td>
	</tr>	
</table>		  	