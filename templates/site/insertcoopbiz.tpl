{literal}
<script language="javascript">
  String.prototype.trim = function() {
  	return this.replace(/(^\s+|\s+$)/g, "");
  }

  function ChangeBorder(obj)
  {
  	try
  	{
		if(obj.value.trim().length > 0) 
		{
			obj.style.borderColor='green';
		}
		else
		{
			obj.style.borderColor='red';
			obj.value = "";
		}
  	}
  	catch(e)
  	{
  		
  	}
  }
</script>
{/literal}


<form method="POST">
<div align="center" style="font-family:arial;font-size:10pt;"><span style="color:red;font-weight:bold;">*</span> ���������� ���� ����������� ��� ���������� </div>

<table width='90%' align='center' bgcolor='#EDEDCC' style='border-top: 1px; border-bottom: 1px; border-left:1px;  border-right:1px;  border-color:#C1C1A4;  border-style: solid;' cellpadding="0" cellspacing="0" >

	<tr {if strlen($text_error) > 0 }{else}style="display:none;"{/if}>
		<td colspan="2" align="center" ><span style="font-size:10pt;font-family:arial;color:red;font-weight:bold;">{$text_error}</span></td>
	</tr>
	<tr {if strlen($text_success) > 0 }{else}style="display:none;"{/if}>
		<td colspan="2" align="center"><span style="font-size:10pt;font-family:arial;color:green;font-weight:bold;">{$text_success}</span></td>
	</tr>
	<tr>
		<td valign="top" align='left' bgcolor='#EDEDCC' width='20%' style='padding-left:5px;'><div style="font-size:10pt;font-family:arial;"><span style="color:red;font-weight:bold;">&nbsp;*&nbsp;</span>�������� �������</div></td>
		<td width="80%" style="padding-right:3pt;padding-top:0pt;padding-bottom:2pt;padding-left:2pt;"  class="val_td">
			<table width="100%">
				<tr>
					<td style="padding-right:2pt;">
						<input type="text" id="name" name="name" style="width:100%;" value="{$smarty.post.name}" class="{if $ErrorInput_name}inputErrorData{else}inputDataMandatory{/if}" onclick='this.className="inputDataMandatory"'  onblur="ChangeBorder(this);">
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td valign='top' align='left' bgcolor='#EDEDCC' width='20%' style='padding-left:5px;'><div style="font-size:10pt;font-family:arial;">��� �������</div></td>
		<td width="80%" style="padding-right:3pt;padding-top:0pt;padding-bottom:2pt;padding-left:2pt;"  class="val_td">
			<table width="100%">
				<tr>
					<td>
						<select name="category" class='inputData' style='font-size:10pt;font-family:tahoma;width:100%;'>
							{foreach name='category' item=item key=key from=$listCategory}
								<optgroup label="{$item->Name}"  style="background-color:#EDEDCC;color:#000000;">
									{foreach name='subcategory' item=subitem key=subkey from=$listSubCategory}
									{if $item->ID eq $subitem->CategoryID}
									<option style="background-color:#ffffff;" value="{$item->ID}:{$subitem->ID}" {if $data->CategoryID eq $item->ID and $subitem->ID eq $data->SubCategoryID}selected{/if} >{$subitem->Name}</option>
									{/if}
									{/foreach}
								</optgroup>
							{/foreach}
						</select>   
           			</td>     
				</tr>           			
			</table>				
		</td>
	</tr>
</table>	

<br>

<table width='90%' align='center' bgcolor='#EDEDCC' style='border-top: 1px; border-bottom: 1px; border-left:1px;  border-right:1px;  border-color:#C1C1A4;  border-style: solid;' cellpadding="0" cellspacing="0" >
	<tr>
		<td valign='top' align='left' bgcolor='#EDEDCC' width='20%' style='padding-left:5px;'><div style="font-size:10pt;font-family:arial;"><span style="color:red;font-weight:bold;">&nbsp;*&nbsp;</span>������������</div></td>
		<td width="80%" style="padding-right:3pt;padding-top:0pt;padding-bottom:2pt;padding-left:2pt;"  class="val_td">
			<div style="font-size:8pt;font-family:arial;padding-left:2pt;">���������� ����� / ����� / ���� / ������� (���������� ����� ������� � ������ ��������� ������)</div>
			<table width="100%">
				<tr>
					<td style="padding-right:2pt;">
						<input type="text" id="region_text" name="region_text" value="{$smarty.post.region_text}" style="width:100%;" class="{if $ErrorInput_region_text}inputErrorData{else}inputDataMandatory{/if}" onclick='this.className="inputDataMandatory"'  onblur="ChangeBorder(this);">
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td valign='top' align='left' bgcolor='#EDEDCC' width='20%' style='padding-left:5px;'><div style="font-size:10pt;font-family:arial;"><span style="color:red;font-weight:bold;">&nbsp;*&nbsp;</span>������� ��������</div></td>
		<td width="80%" style="padding-right:3pt;padding-top:2pt;padding-bottom:2pt;padding-left:2pt;"  class="val_td">
			<table width="100%">
				<tr>
					<td style="padding-right:2pt;">
						<textarea style="width:100%;height:70px" id="shortdescription" name="shortdescription" class="{if $ErrorInput_shortdescription}inputErrorData{else}inputDataMandatory{/if}" onclick='this.className="inputDataMandatory"'  onblur="ChangeBorder(this);">{$smarty.post.shortdescription}</textarea>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>	

<br>

<table width='90%' align='center' bgcolor='#EDEDCC' style='border-top: 1px; border-bottom: 1px; border-left:1px;  border-right:1px;  border-color:#C1C1A4;  border-style: solid;' cellpadding="0" cellspacing="0" >
	<tr>
		<td valign='top' align='left' bgcolor='#EDEDCC' width='20%' style='padding-left:5px;'><div style="font-size:10pt;font-family:arial;"><span style="color:red;font-weight:bold;">&nbsp;*&nbsp;</span>������ ��������</div></td>
		<td width="80%" style="padding-right:3pt;padding-top:2pt;padding-bottom:2pt;padding-left:2pt;"  class="val_td">
			<table width="100%">
				<tr>
					<td  style="padding-right:2pt;"><textarea style="width:100%;height:180px" id="description" name="description" class="{if $ErrorInput_description}inputErrorData{else}inputDataMandatory{/if}" onclick='this.className="inputDataMandatory"'  onblur="ChangeBorder(this);">{$smarty.post.description}</textarea></td>
				</tr>
			</table>
		</td>
	</tr>
	
</table>	

<br>

<table width='90%' align='center' bgcolor='#EDEDCC' style='border-top: 1px; border-bottom: 1px; border-left:1px;  border-right:1px;  border-color:#C1C1A4;  border-style: solid;' cellpadding="0" cellspacing="0" >
	<tr>
		<td valign="top" align='left' bgcolor='#EDEDCC' width='20%' style='padding-left:5px;'><div style="font-size:10pt;font-family:arial;">���������� ����������</div></td>
		<td width="80%" style="padding-right:3pt;padding-top:2pt;padding-bottom:2pt;padding-left:2pt;"  class="val_td">
			<table width="100%">
				<tr>
					<td width="25%"><div style="font-size:10pt;font-family:arial;"><span style="color:red;font-weight:bold;">&nbsp;*&nbsp;</span>���������� ����</div></td>
					<td style="padding-right:2pt;"><input type="text" id="contact_face" name="contact_face" style="width:100%;" value="{$smarty.post.contact_face}" class="{if $ErrorInput_contact_face}inputErrorData{else}inputDataMandatory{/if}" onclick='this.className="inputDataMandatory"'  onblur="ChangeBorder(this);"><div style="font-size:8pt;font-family:arial;">(������� �����������)</div></td>
				</tr>
				<tr>
					<td><div style="font-size:10pt;font-family:arial;"><span style="color:red;font-weight:bold;">&nbsp;*&nbsp;</span>��������</div></td>
					<td style="padding-right:2pt;"><input type="text" id="contact_phone" name="contact_phone" style="width:100%;" value="{$smarty.post.contact_phone}" class="{if $ErrorInput_contact_phone}inputErrorData{else}inputDataMandatory{/if}" onclick='this.className="inputDataMandatory"'  onblur="ChangeBorder(this);"><div style="font-size:8pt;font-family:arial;">(������� �����������)</div></td>
				</tr>
				<tr>
					<td><div style="font-size:10pt;font-family:arial;"><span style="color:red;font-weight:bold;">&nbsp;*&nbsp;</span>Email</div></td>
					<td style="padding-right:2pt;"><input type="text" id="contact_email" name="contact_email" value="{$smarty.post.contact_email}" style="width:100%;" class="{if $ErrorInput_contact_email}inputErrorData{else}inputDataMandatory{/if}" onclick='this.className="inputDataMandatory"'  onblur="ChangeBorder(this);"><div style="font-size:8pt;font-family:arial;">(������� �����������)</div></td>
				</tr>
				<tr>
					<td><div style="font-size:10pt;font-family:arial;">����</div></td>
					<td style="padding-right:4pt;"><input type="text" id="contact_site" name="contact_site" value="{$smarty.post.contact_site}" style="width:100%;"></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td valign='top' align='left' bgcolor='#EDEDCC' width='20%' style='padding-left:5px;'><div style="font-size:10pt;font-family:arial;">������������ ����������</div></td>
		<td width="80%" style="padding-right:3pt;padding-top:2pt;padding-bottom:2pt;padding-left:2pt;"  class="val_td">
			<select name="duration_id" style="width:100%;">
				<option value="1" {if $smarty.post.duration_id eq "1"}selected{else}{/if}>1 �����</option>
				<option value="2" {if $smarty.post.duration_id eq "2"}selected{else}{/if}>2 ������</option>
				<option value="3" {if $smarty.post.duration_id eq "3"}selected{else}{/if}>3 ������</option>
			</select>
		</td>
	</tr>
	<tr>
		<td colspan="2" align="right"><input type="submit" value="���������"  style="font-size:10pt;font-family:arial;"></td>
	</tr>
	
</table>

</form>

{literal}
<script language="javascript">

	try
	{
		var obj_name = document.getElementById("name");
		ChangeBorder(obj_name);

		var obj_region_text = document.getElementById("region_text");
		ChangeBorder(obj_region_text);
		
		var obj_shortdescription = document.getElementById("shortdescription");
		ChangeBorder(obj_shortdescription);
		
		var obj_description = document.getElementById("description");
		ChangeBorder(obj_description);

		var obj_contact_face = document.getElementById("contact_face");
		ChangeBorder(obj_contact_face);
		
		var obj_contact_phone = document.getElementById("contact_phone");
		ChangeBorder(obj_contact_phone);

		var obj_contact_email = document.getElementById("contact_email");
		ChangeBorder(obj_contact_email);
	}
	catch(e)
	{
		
	}
</script>
{/literal}