{literal}
<script language="javascript">
  String.prototype.trim = function() {
  	return this.replace(/(^\s+|\s+$)/g, "");
  }

  function ChangeBorder(obj)
  {
  	try
  	{
		if(obj.value.trim().length > 0) 
		{
			obj.style.borderColor='green';
		}
		else
		{
			obj.style.borderColor='red';
			obj.value = "";
		}
  	}
  	catch(e)
  	{
  		
  	}
  }
</script>
{/literal}


<form method="POST" enctype="multipart/form-data" method="post">
<div align="center" style="font-family:arial;font-size:10pt;"><span style="color:red;font-weight:bold;">*</span> ���������� ���� ����������� ��� ���������� </div>
<table width='90%' align='center' bgcolor='#EDEDCC' style='border-top: 1px; border-bottom: 1px; border-left:1px;  border-right:1px;  border-color:#C1C1A4;  border-style: solid;' cellpadding="0" cellspacing="0">
	<tr {if strlen($text_error) > 0 }{else}style="display:none;"{/if}>
		<td colspan="2" align="center" class="val_td"><span style="font-size:10pt;font-family:arial;color:red;font-weight:bold;">{$text_error}</span></td>
	</tr>
	<tr {if strlen($text_success) > 0 }{else}style="display:none;"{/if}>
		<td colspan="2" align="center" class="val_td"><span style="font-size:10pt;font-family:arial;color:green;font-weight:bold;">{$text_success}</span></td>
	</tr>
	<tr>
		<td valign="top" align='left' bgcolor='#EDEDCC' width='25%' style='padding-left:5px;'><div style="font-size:10pt;font-family:arial;"><span style="color:red;font-weight:bold;">&nbsp;*&nbsp;</span>�������� ������������</div></td>
		<td width="75%" style="padding-right:3pt;padding-top:0pt;padding-bottom:2pt;padding-left:2pt;"  class="val_td">
			<table width="100%">
				<tr>
					<td style="padding-right:2pt;">
						<input type="text" id="name" name="name" style="width:100%;" value="{$smarty.post.name}" class="{if $ErrorInput_name}inputErrorData{else}inputDataMandatory{/if}" onclick='this.className="inputDataMandatory"'  onblur="ChangeBorder(this);">
					</td>
				</tr>
			</table>
		</td>
	</tr>
	
	<tr>
		<td valign="top" align='left' bgcolor='#EDEDCC' width='25%' style='padding-left:5px;'><div style="font-size:10pt;font-family:arial;"><span style="color:red;font-weight:bold;">&nbsp;*&nbsp;</span>���������</div></td>
		<td width="75%" style="padding-right:3pt;padding-top:0pt;padding-bottom:2pt;padding-left:2pt;"  class="val_td">
			<table width="100%">
				<tr>
					<td>
						<input type="text" id="cost" name="cost" style="width:60%;" value="{$smarty.post.cost}" class="{if $ErrorInput_cost}inputErrorData{else}inputDataMandatory{/if}" onclick='this.className="inputDataMandatory"' onblur="ChangeBorder(this);">
            			<select style='font-size:10pt;font-family:arial;width:29% ' name="ccost">
            				<option value=usd {if $smarty.post.ccost eq "usd"}selected{else}{/if}>USD (������ ���)
            				<option value=eur {if $smarty.post.ccost eq "eur"}selected{else}{/if}>EUR (����)
            				<option value=rub {if $smarty.post.ccost eq "rub"}selected{else}{/if}>RUB (���������� �����)
            			</select>
            		</td>
            	</tr>
            </table>
		</td>
	</tr>

	<tr>
		<td valign="top" align='left' bgcolor='#EDEDCC' width='25%' style='padding-left:5px;'><div style="font-size:10pt;font-family:arial;"><span style="color:red;font-weight:bold;">&nbsp;*&nbsp;</span>������������</div></td>
		<td width="75%" style="padding-right:3pt;padding-top:0pt;padding-bottom:2pt;padding-left:2pt;"  class="val_td">
			<table width="100%">
				<tr>
					<td style="padding-right:2pt;">
						<input type="text" id="city_text" name="city_text" style="width:100%;" value="{$smarty.post.name}" class="{if $ErrorInput_city_text}inputErrorData{else}inputDataMandatory{/if}" onclick='this.className="inputDataMandatory"'  onblur="ChangeBorder(this);">
					</td>
				</tr>
			</table>
		</td>
	</tr>
	
	<tr>
		<td valign="top" align='left' bgcolor='#EDEDCC' width='25%' style='padding-left:5px;'><div style="font-size:10pt;font-family:arial;">��� ������������</div></td>
		<td width="75%" style="padding-right:3pt;padding-top:0pt;padding-bottom:2pt;padding-left:2pt;"  class="val_td">
			<table width="100%">
				<tr>
					<td>
        				<select name="category_id"  style="width:100%;">
                      		<option value='-1'>������</option>
             				{foreach name='category' item=item key=key from=$listCategory}
               					<optgroup label="{$item->Name}" style="background-color:#EDEDCC;color:#000000;">
               					{foreach name='subcategory' item=subitem key=subkey from=$listSubCategory}
             	  					{if $item->ID eq $subitem->CategoryID}
             	  						<option style="background-color:#ffffff;" value="{$item->ID}:{$subitem->ID}" {if $smarty.post.subCategory_id eq $subitem->ID}selected{/if} >{$subitem->Name}</option>
             	  					{/if}
               					{/foreach}
               					</optgroup>
             				{/foreach}
            			</select>
            		</td>
            	</tr>
            </table>
		</td>
	</tr>

	<tr>
		<td valign="top" align='left' bgcolor='#EDEDCC' width='25%' style='padding-left:5px;'><div style="font-size:10pt;font-family:arial;">������������ ��������������</div></td>
		<td width="75%" style="padding-right:3pt;padding-top:0pt;padding-bottom:2pt;padding-left:2pt;"  class="val_td">
			<table width="100%">
				<tr>
					<td style="padding-right:2pt;">
						<input type="text" name="age" style="width:100%;" value="{$smarty.post.age}">
					</td>
				</tr>
			</table>
		</td>
	</tr>
	
	<tr>
		<td valign="top" align='left' bgcolor='#EDEDCC' width='25%' style='padding-left:5px;'><div style="font-size:10pt;font-family:arial;"><span style="color:red;font-weight:bold;">&nbsp;*&nbsp;</span>������� ��������</div></td>
		<td width="75%" style="padding-right:3pt;padding-top:0pt;padding-bottom:2pt;padding-left:2pt;"  class="val_td">
			<table width="100%">
				<tr>
					<td style="padding-right:2pt;">
						<textarea id="description" name="description" style="width:100%;height:150px;"  class="{if $ErrorInput_description}inputErrorData{else}inputDataMandatory{/if}" onclick='this.className="inputDataMandatory"'  onblur="ChangeBorder(this);">{$smarty.post.description}</textarea>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>

<br>

<table width='90%' align='center' bgcolor='#EDEDCC' style='border-top: 1px; border-bottom: 1px; border-left:1px;  border-right:1px;  border-color:#C1C1A4;  border-style: solid;' cellpadding="0" cellspacing="0">
	<tr>
		<td valign="top" align='left' bgcolor='#EDEDCC' width='25%' style='padding-left:5px;'><div style="font-size:10pt;font-family:arial;"><span style="color:red;font-weight:bold;">&nbsp;*&nbsp;</span>���������� ����</div></td>
		<td width="75%" style="padding-right:3pt;padding-top:0pt;padding-bottom:2pt;padding-left:2pt;"  class="val_td">
			<table width="100%">
				<tr>
					<td style="padding-right:2pt;">
						<input type="text" id="contact_face" name="contact_face" style="width:100%;" value="{$smarty.post.contact_face}" class="{if $ErrorInput_contact_face}inputErrorData{else}inputDataMandatory{/if}" onclick='this.className="inputDataMandatory"'  onblur="ChangeBorder(this);">
					</td>
				</tr>
			</table>
		</td>
	</tr>

	<tr>
		<td valign="top" align='left' bgcolor='#EDEDCC' width='25%' style='padding-left:5px;'><div style="font-size:10pt;font-family:arial;"><span style="color:red;font-weight:bold;">&nbsp;*&nbsp;</span>��������</div></td>
		<td width="75%" style="padding-right:3pt;padding-top:0pt;padding-bottom:2pt;padding-left:2pt;"  class="val_td">
			<table width="100%">
				<tr>
					<td style="padding-right:2pt;">
						<input type="text" id="phone" name="phone" style="width:100%;" value="{$smarty.post.phone}"  class="{if $ErrorInput_phone}inputErrorData{else}inputDataMandatory{/if}" onclick='this.className="inputDataMandatory"'  onblur="ChangeBorder(this);">
					</td>
				</tr>
			</table>
		</td>
	</tr>

	<tr>
		<td valign="top" align='left' bgcolor='#EDEDCC' width='25%' style='padding-left:5px;'><div style="font-size:10pt;font-family:arial;"><span style="color:red;font-weight:bold;">&nbsp;*&nbsp;</span>Email</div></td>
		<td width="75%" style="padding-right:3pt;padding-top:0pt;padding-bottom:2pt;padding-left:2pt;"  class="val_td">
			<table width="100%">
				<tr>
					<td style="padding-right:2pt;">
						<input type="text" id="email" name="email" style="width:100%;" value="{$smarty.post.email}" class="{if $ErrorInput_email}inputErrorData{else}inputDataMandatory{/if}" onclick='this.className="inputDataMandatory"' onblur="ChangeBorder(this);">
					</td>
				</tr>
			</table>
		</td>
	</tr>

	<tr>
		<td valign="top" align='left' bgcolor='#EDEDCC' width='25%' style='padding-left:5px;'><div style="font-size:10pt;font-family:arial;">������������ ����������</div></td>
		<td width="75%" style="padding-right:3pt;padding-top:0pt;padding-bottom:2pt;padding-left:2pt;"  class="val_td">
			<table width="100%">
				<tr>
					<td style="padding-right:2pt;">
						<select name="duration_id" style="width:100%;">
							<option value="1">1 �����</option>
							<option value="2">2 ������</option>
							<option value="3">3 ������</option>
						</select>
					</td>
				</tr>
			</table>
			
		</td>
	</tr>

	<tr>
		<td valign="top" align='left' bgcolor='#EDEDCC' width='25%' style='padding-left:5px;'><div style="font-size:10pt;font-family:arial;">����������</div></td>
		<td width="75%" style="padding-right:3pt;padding-top:0pt;padding-bottom:2pt;padding-left:2pt;"  class="val_td">
			<input type="file" name="icon">
		</td>
	</tr>
	
	
	<tr>
		<td colspan="2" align="right"><input type="submit" value="���������"  style="font-size:10pt;font-family:arial;"></td>
	</tr>
	
</table>

</form>

{literal}
<script language="javascript">

	try
	{
		var obj_name = document.getElementById("name");
		ChangeBorder(obj_nameID);
		
		var obj_cost = document.getElementById("cost");
		ChangeBorder(obj_cost);
		
		var obj_city_text = document.getElementById("city_text");
		ChangeBorder(obj_city_text);
		
		var obj_description = document.getElementById("description");
		ChangeBorder(obj_description);
		
		var obj_contact_face = document.getElementById("contact_face");
		ChangeBorder(obj_contact_face);
		
		var obj_phone = document.getElementById("phone");
		ChangeBorder(obj_phone);
		
		var obj_email = document.getElementById("email");
		ChangeBorder(obj_email);

	}
	catch(e)
	{
		
	}
</script>
{/literal}