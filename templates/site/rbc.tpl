{literal}

<link rel="stylesheet" href="http://www.rbkmoney.ru/sites/default/files/banner/horizontal_files/css_gorizontal.css" type="text/css" media="all"/>
   <div class="rw-banner">
       <div class="rw-b-inner">
            <div class="rw-head">
                <div class="rw-h-text">
                    �� ���������
                </div>
                <div class="rw-h-logo"></div>
            </div>
            <div class="rw-body">
                <div class="rw-b-part">
                    <a href="javascript:;" class="rw-icon" onmouseover="tH(this);"></a>
                    <a href="javascript:;" class="rw-text" onmouseover="tH(this);">���������� �����</a>
                </div>
               <div class="rw-helper" onmouseout="hH(this,event);" id="h0">
                    <div class="rwh-arrow"></div>
                    <div class="rwh-head">���������� �����</div>
                    <div class="rwh-text">
                        �������� ������� � ��������-�������� ����������� ������� VISA � Mastercard ������ �����.
                    </div>
                    <a href="http://www.rbkmoney.ru/oplata-bankovskimi-kartami" target="_blank" class="rwh-link">������ ������</a>
                </div>
                <div class="rw-b-part">
                    <a href="javascript:;" class="rw-icon rw-icon-1" onmouseover="tH(this);"></a>
                    <a href="javascript:;" class="rw-text" onmouseover="tH(this);">����������� �������</a>
                </div>
               <div class="rw-helper" onmouseout="hH(this,event);" id="h1">
                    <div class="rwh-arrow"></div>
                    <div class="rwh-head">����������� �������</div>
                    <div class="rwh-text">
                        ������������ ������ ������� � ������� ������ ������������ �������� RBK Money.
                    </div>
                    <a href="http://www.rbkmoney.ru/oplata-pokupok-v-internet-magazinakh-s-koshelka-rbk-money" target="_blank" class="rwh-link">������ ������</a>
                </div>
                <div class="rw-b-part">
                    <a href="javascript:;" class="rw-icon rw-icon-2" onmouseover="tH(this);"></a>
                    <a href="javascript:;" class="rw-text" onmouseover="tH(this);">���������� ������</a>
                </div>
               <div class="rw-helper" onmouseout="hH(this,event);" id="h2">
                    <div class="rwh-arrow"></div>
                    <div class="rwh-head">���������� ������</div>
                    <div class="rwh-text">
                        �������� ������� � ����� ���������� �����. ���� ���������� ������� �� ���� - 3-5 ������� ����. 
                    </div>
                    <a href="http://www.rbkmoney.ru/popolnenie-bankovskim-platezhom" target="_blank" class="rwh-link">������ ������</a>
                </div>
                <div class="rw-b-part">
                    <a href="javascript:;" class="rw-icon rw-icon-3" onmouseover="tH(this);"></a>
                    <a href="javascript:;" class="rw-text" onmouseover="tH(this);">�������� ��������</a>
                </div>
               <div class="rw-helper" onmouseout="hH(this,event);" id="h3">
                    <div class="rwh-arrow"></div>
                    <div class="rwh-head">�������� ��������</div>
                    <div class="rwh-text">
                        ������ ������� ����� ���������� ������� �������� ��������� CONTACT � Unistream.
                    </div>
                    <a href="http://www.rbkmoney.ru/sistemy-denezhnykh-perevodov" target="_blank" class="rwh-link">������ ������</a>
                </div>
                <div class="rw-b-part">
                    <a href="javascript:;" class="rw-icon rw-icon-4" onmouseover="tH(this);"></a>
                    <a href="javascript:;" class="rw-text" onmouseover="tH(this);">�������� ��������</a>
                </div>
               <div class="rw-helper" onmouseout="hH(this,event);" id="h4">
                    <div class="rwh-arrow"></div>
                    <div class="rwh-head">�������� ��������</div>
                    <div class="rwh-text">
                        �������� ������� � ����� ��������� ����� ������. ���� ���������� ������� - 3-4 ������� ���. 
                    </div>
                    <a href="http://www.rbkmoney.ru/popolnenie-elektronnogo-koshelka-cherez-fgup-pochta-rossii" target="_blank" class="rwh-link">������ ������</a>
                </div>
                <div class="rw-b-part">
                    <a href="javascript:;" class="rw-icon rw-icon-5" onmouseover="tH(this);"></a>
                    <a href="javascript:;" class="rw-text" onmouseover="tH(this);">��������� ���������</a>
                </div>
               <div class="rw-helper" onmouseout="hH(this,event);" id="h5">
                    <div class="rwh-arrow"></div>
                    <div class="rwh-head">��������� ���������</div>
                    <div class="rwh-text">
                        ������ �������  � ���������� ���������� ��������� ������ � ����� ������ ������ - ������ � � ��������� ���������.
                    </div>
                    <a href="http://www.rbkmoney.ru/popolnenie-cherez-platezhnye-terminaly" target="_blank" class="rwh-link">������ ������</a>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        function tH(el){
            var p = el.parentNode, 
                next = p.nextSibling;
            while (next.nodeType != 1) next = next.nextSibling;
            next.style.left = p.offsetLeft+'px';
            next.style.top = p.offsetTop+'px';
            for (var i = 0;i < 6; i++) document.getElementById('h'+i).style.display = 'none';
            next.style.display = 'block';
        }
        function hH(el,ev){
            if (ev.stopPropagation) ev.stopPropagation();
            else ev.cancelBubble = true;
            var target = ev.relatedTarget || ev.srcElement,
                cssClass = target.getAttribute('class');
            if ((!cssClass && el.tagName.toLowerCase != 'div') || cssClass.indexOf('rwh-') == -1) {
                el.style.display = 'none';
            }
        }
    </script>
{/literal}    