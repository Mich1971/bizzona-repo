{literal}
<script language="javascript">
  String.prototype.trim = function() {
  	return this.replace(/(^\s+|\s+$)/g, "");
  }

  function ChangeBorder(obj)
  {
  	try
  	{
		if(obj.value.trim().length > 0) 
		{
			obj.style.borderColor='green';
		}
		else
		{
			obj.style.borderColor='red';
			obj.value = "";
		}
  	}
  	catch(e)
  	{
  		
  	}
  }
</script>
{/literal}

<div  style="width: 100%;height:17pt;background-color:#d9d9b9;">
	<table width="100%" border="0" cellpadding="0" cellspacing="0" height="100%">
    	<tr>
        	<td width="100%" align="center" style="font-size:12pt;font-family:Arial;color:#7C3535;font-weight:bold; vertical-align:middle;">����� ���������� ������ �����</td>
       	</tr>
    </table>
</div>

<div  style="width: 100%;height:27pt;background-color:#EDEDCC;padding-left:5pt;padding-right:5pt;padding-bottom:10pt;padding-top:5pt;">
	<span style='font-size:10pt;font-family:arial;background-color:#EDEDCC;width: 100%;'>
		����������  ������-������ <b style="color:red;">�������</b>. ��������� ���������� ��������������� ���������� ������-������ ������ �� ���� ����� ���������� 1 000 �. 
		<br>��� ���������� ������-������ ������ �� 1 ��� ��������������� ������ 30%. 
		<br>������ ����� ����������� �� ������� (������ �����) ���� ����� ������.������. ����������� ����� ������ �� �������� 8 (917) 5191006. 
	</span>
</div>


<form method="POST" style=" margin-top:2pt;">
<div align="center" style="font-family:arial;font-size:10pt;"><span style="color:#7C3535;font-weight:bold;">*</span> ���������� ���� ����������� ��� ���������� </div>
{if strlen($text_error) > 0 or strlen($text_success) > 0}
<table width='90%' align='center' bgcolor='#EDEDCC' style='border-top: 1px; border-bottom: 1px; border-left:1px;  border-right:1px;  border-color:#C1C1A4;  border-style: solid;' cellpadding="0" cellspacing="0" >
	<tr {if strlen($text_error) > 0 }{else}style="display:none;"{/if}>
		<td colspan="2" align="center" class="val_td"><span style="font-size:10pt;font-family:arial;color:red;font-weight:bold;">{$text_error}</span></td>
	</tr>
	<tr {if strlen($text_success) > 0 }{else}style="display:none;"{/if}>
		<td colspan="2" align="center" class="val_td"><span style="font-size:10pt;font-family:arial;color:green;font-weight:bold;">{$text_success}</span></td>
	</tr>
</table>
<br>	
{else}
{/if}

<table width='90%' align='center' bgcolor='#EDEDCC' style='border-top: 1px; border-bottom: 1px; border-left:1px;  border-right:1px;  border-color:#C1C1A4;  border-style: solid;' cellpadding="0" cellspacing="0" >	
	<tr>
		<td valign="top" align='left' bgcolor='#EDEDCC' width='20%' style='padding-left:5px;'><div style="font-size:10pt;font-family:arial;"><span style="color:#7C3535;font-weight:bold;">&nbsp;*&nbsp;</span>�������� �����</div></td>
		<td width="80%" style="padding-right:3pt;padding-top:0pt;padding-bottom:2pt;padding-left:2pt;"  class="val_td">
			<table width="100%">
				<tr>
					<td>
						<table width="100%">
							<tr>
								<td>
									<textarea rows="3" id="name" name="name" style="width:99%;" class="{if $ErrorInput_name}inputErrorData{else}inputDataMandatory{/if}" onclick='this.className="inputDataMandatory"'  onblur="ChangeBorder(this);">{$smarty.post.name}</textarea>
								</td>
							</tr>																			
						</table>							
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td valign="top" align='left' bgcolor='#EDEDCC' width='20%' style='padding-left:5px;'><div style="font-size:10pt;font-family:arial;"><span style="color:#7C3535;font-weight:bold;">&nbsp;*&nbsp;</span>���������</div></td>
		<td width="80%" style="padding-right:3pt;padding-top:0pt;padding-bottom:2pt;padding-left:2pt;"  class="val_td">
			<table width="100%">
				<tr>
					<td>
						<table width="100%">
							<tr>
								<td>
									<input type="text" id="cost" name="cost" style="width:20%;" value="{$smarty.post.cost}" style="width:100%;" class="{if $ErrorInput_cost}inputErrorData{else}inputDataMandatory{/if}" onclick='this.className="inputDataMandatory"'  onblur="ChangeBorder(this);">
            						<select style='font-size:10pt;font-family:arial;width:25% ' name="ccost">
            							<option value=usd {if $smarty.post.ccost eq "usd"}selected{else}{/if}>USD (������ ���)
            							<option value=eur {if $smarty.post.ccost eq "eur"}selected{else}{/if}>EUR (����)
            							<option value=rub {if $smarty.post.ccost eq "rub"}selected{else}{/if}>RUB (���������� �����)
            						</select>
            					</td>
							</tr>            					
						</table>							
            		</td>
            	</tr>
            </table>
		</td>
	</tr>
	<tr>
		<td valign="top" align='left' bgcolor='#EDEDCC' width='20%' style='padding-left:5px;'><div style="font-size:10pt;font-family:arial;"><span style="color:#7C3535;font-weight:bold;">&nbsp;*&nbsp;</span>���������� �������</div></td>
		<td width="80%" style="padding-right:3pt;padding-top:0pt;padding-bottom:2pt;padding-left:2pt;"  class="val_td">
			<table width="100%">
				<tr>
					<td>
						<table width="100%">
							<tr>
								<td>
									<input type="text" id="count_site" name="count_site" style="width:99%;" value="{$smarty.post.count_site}" class="{if $ErrorInput_count_site}inputErrorData{else}inputDataMandatory{/if}" onclick='this.className="inputDataMandatory"'  onblur="ChangeBorder(this);">
								</td>
							</tr>	
						</table>									
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td valign="top" align='left' bgcolor='#EDEDCC' width='20%' style='padding-left:5px;'><div style="font-size:10pt;font-family:arial;">��������� ������ �����</div></td>
		<td width="80%" style="padding-right:3pt;padding-top:0pt;padding-bottom:2pt;padding-left:2pt;"  class="val_td">
			<table width="100%">
				<tr>
					<td>
						<table width="100%">
							<tr>
								<td>
									<select name="category_id" style="width:100%;font-size:10pt;font-family:arial;">
									{foreach name=nname key=kkey item=iitem from=$categorylist}
										<option value="{$iitem->ID}">{$iitem->Name}</option>
									{/foreach}
									</select>
								</td>
							</tr>								
						</table>							
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td valign="top" align='left' bgcolor='#EDEDCC' width='20%' style='padding-left:5px;'><div style="font-size:10pt;font-family:arial;">������ ��������������</div></td>
		<td width="80%" style="padding-right:3pt;padding-top:0pt;padding-bottom:2pt;padding-left:2pt;"  class="val_td">
			<table width="100%">
				<tr>
					<td>
						<table width="100%">
							<tr>
								<td>
									{include file="./site/viewplan.tpl" dataselect=$smarty.post.display_id}
								</td>
							</tr>
						</table>																			
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td valign="top" align='left' bgcolor='#EDEDCC' width='20%' style='padding-left:5px;'><div style="font-size:10pt;font-family:arial;">���� ������</div></td>
		<td width="80%" style="padding-right:3pt;padding-top:0pt;padding-bottom:2pt;padding-left:2pt;"  class="val_td">
			<table width="100%">
				<tr>
					<td>
						<table width="100%">
							<tr>
								<td>
									{include file="./site/languageplan.tpl" dataselect=$smarty.post.language_id}
								</td>									
							</tr>								
						</table>							
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td valign="top" align='left' bgcolor='#EDEDCC' width='20%' style='padding-left:5px;'><div style="font-size:10pt;font-family:arial;"><span style="color:#7C3535;font-weight:bold;">&nbsp;*&nbsp;</span>�����������</div></td>
		<td width="80%" style="padding-right:3pt;padding-top:0pt;padding-bottom:2pt;padding-left:2pt;"  class="val_td">
			<table width="100%">
				<tr>
					<td>
						{include file="./site/companyplan.tpl" dataselect=$smarty.post.company_id}
					</td>						
				</tr>					
			</table>				
		</td>
	</tr>
	<tr>
		<td valign="top" align='left' bgcolor='#EDEDCC' width='20%' style='padding-left:5px;'><div style="font-size:10pt;font-family:arial;"><span style="color:#7C3535;font-weight:bold;">&nbsp;*&nbsp;</span>������� ��������</div></td>
		<td width="80%" style="padding-right:3pt;padding-top:0pt;padding-bottom:2pt;padding-left:2pt;"  class="val_td">
			<table width="100%">
				<tr>
					<td>
						<table width="100%">
							<tr>
								<td>
									<textarea rows="4" id="shortDescription" name="shortDescription" style="width:99%;" class="{if $ErrorInput_shortDescription}inputErrorData{else}inputDataMandatory{/if}" onclick='this.className="inputDataMandatory"'  onblur="ChangeBorder(this);">{$smarty.post.shortDescription}</textarea>		
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td valign="top" align='left' bgcolor='#EDEDCC' width='20%' style='padding-left:5px;'><div style="font-size:10pt;font-family:arial;"><span style="color:#7C3535;font-weight:bold;">&nbsp;*&nbsp;</span>��������� ��������</div></td>
		<td width="80%" style="padding-right:3pt;padding-top:0pt;padding-bottom:2pt;padding-left:2pt;"  class="val_td">
			<table width="100%">
				<tr>
					<td>
						<table width="100%">
							<tr>
								<td>
									<textarea rows="4" id="description" name="description" style="width:99%;height:400px;" class="{if $ErrorInput_description}inputErrorData{else}inputDataMandatory{/if}" onclick='this.className="inputDataMandatory"'  onblur="ChangeBorder(this);">{$smarty.post.description}</textarea>
								</td>
							</tr>
						</table>
         			</td>
         		</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td colspan="2" align="right" class="val_td"><input type="submit" value="���������"></td>
	</tr>
	
	
</table>

</form>

{literal}
<script language="javascript">
	try
	{
		var obj_name = document.getElementById("name");
		ChangeBorder(obj_name);

		var obj_cost = document.getElementById("cost");
		ChangeBorder(obj_cost);
		
		var obj_count_site = document.getElementById("count_site");
		ChangeBorder(obj_count_site);
		
		var obj_shortDescription = document.getElementById("shortDescription");
		ChangeBorder(obj_shortDescription);
		
		var obj_description = document.getElementById("description");
		ChangeBorder(obj_description);
		
		var obj_company_name = document.getElementById("company_name");
		ChangeBorder(obj_company_name);
		
		var obj_company_text = document.getElementById("company_text");
		ChangeBorder(obj_company_text);
		
		var obj_company_email = document.getElementById("company_email");
		ChangeBorder(obj_company_email);	
		
		var obj_company_phone = document.getElementById("company_phone");	
		ChangeBorder(obj_company_phone);
		
		var obj_company_fml = document.getElementById("company_fml");		
		ChangeBorder(obj_company_fml);
		
	}
	catch(e)
	{
		
	}
</script>
{/literal}