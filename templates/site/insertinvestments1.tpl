{literal}
<script language="javascript">
  String.prototype.trim = function() {
  	return this.replace(/(^\s+|\s+$)/g, "");
  }

  function ChangeBorder(obj)
  {
  	try
  	{
		if(obj.value.trim().length > 0) 
		{
			obj.style.borderColor='green';
		}
		else
		{
			obj.style.borderColor='red';
			obj.value = "";
		}
  	}
  	catch(e)
  	{
  		
  	}
  }
</script>
{/literal}


<form method="POST" style=" margin-top:2pt;">
<div align="center" style="font-family:arial;font-size:10pt;"><span style="color:#7C3535;font-weight:bold;">*</span> ���������� ���� ����������� ��� ���������� </div>
<table width='80%' align='center' bgcolor='#EDEDCC' style='border-top: 1px; border-bottom: 1px; border-left:1px;  border-right:1px;  border-color:#C1C1A4;  border-style: solid;' cellpadding="0" cellspacing="0" >
	<tr {if strlen($text_error) > 0 }{else}style="display:none;"{/if}>
		<td colspan="2" align="center" class="desc_td"><span style="font-size:10pt;font-family:arial;color:red;font-weight:bold;">{$text_error}</span></td>
	</tr>
	<tr {if strlen($text_success) > 0 }{else}style="display:none;"{/if}>
		<td colspan="2" align="center" class="desc_td"><span style="font-size:10pt;font-family:arial;color:green;font-weight:bold;">{$text_success}</span></td>
	</tr>

    <tr bgcolor='#E1E1C1'>
      <td  valign='top' align='left' colspan=2 style='padding-left:5px;padding-top:3px;padding-bottom:3px;'><span style='font-size:10pt;font-family:arial;color:#747464'><b>�������� ����������</b></span></td>
    </tr>
	
	
	<tr>
		<td width="30%" class="val_td" style="background-color:#EDEDCC;padding-right:2pt;" align="right"><div style='font-size:10pt;font-family:arial;color:#747464;font-weight:bold;'><span style="color:#7C3535;font-weight:bold;">&nbsp;*&nbsp;</span>��������</div></td>
		<td width="70%" style="padding-top:2pt;padding-bottom:2pt;" class="val_td">
			<table width="100%">
				<tr>
					<td style="padding-right:2pt;">
						<textarea rows="3" id="name" name="name" style="width:100%;" class="{if $ErrorInput_name}inputErrorData{else}inputDataMandatory{/if}" onclick='this.className="inputDataMandatory"' onblur="ChangeBorder(this);">{$smarty.post.name}</textarea>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td class="val_td" style="background-color:#EDEDCC;padding-right:2pt;" align="right"><div style='font-size:10pt;font-family:arial;color:#747464;font-weight:bold;'><span style="color:#7C3535;font-weight:bold;">&nbsp;*&nbsp;</span>��������� ����� ����������</div></td>
		<td style="padding-top:2pt;padding-bottom:2pt;" class="val_td">
			<table width="100%">
				<tr>
					<td>
						<input type="text" id="cost" name="cost" style="width:20%;" class="{if $ErrorInput_cost}inputErrorData{else}inputDataMandatory{/if}" onclick='this.className="inputDataMandatory"' onblur="ChangeBorder(this);" value="{$smarty.post.cost}">
            			<select style='font-size:10pt;font-family:arial;width:78% ' name="ccost">
            				<option value=usd {if $smarty.post.ccost eq "usd"}selected{else}{/if}>USD (������ ���)
            				<option value=eur {if $smarty.post.ccost eq "eur"}selected{else}{/if}>EUR (����)
            				<option value=rub {if $smarty.post.ccost eq "rub"}selected{else}{/if}>RUB (���������� �����)
            			</select>
					</td>
				</tr>
			</table>
            
		</td>
	</tr>
	<tr>
		<td class="val_td" style="background-color:#EDEDCC;padding-right:2pt;"  align="right"><div style='font-size:10pt;font-family:arial;color:#747464;font-weight:bold;'>��� �������</div></td>
		<td class="val_td" style="padding-top:2pt;padding-bottom:2pt;">
			<table width="100%">
				<tr>
					<td>		
						<select name="category_id" style="width:100%;font-size:10pt;font-family:arial;">
             				{foreach name='category' item=item key=key from=$listCategory}
               					<optgroup label="{$item->Name}" style="background-color:#EDEDCC;color:#000000;">
               					{foreach name='subcategory' item=subitem key=subkey from=$listSubCategory}
             	  					{if $item->ID eq $subitem->CategoryID}
             	  						<option style="background-color:#ffffff;" value="{$item->ID}:{$subitem->ID}" {if $smarty.post.subCategory_id eq $subitem->ID}selected{/if} >{$subitem->Name}</option>
             	  					{/if}
               					{/foreach}
               					</optgroup>
             				{/foreach}
						</select>
					</td>
				</tr>
			</table>										
		</td>
	</tr>
	<tr>
		<td class="val_td" style="background-color:#EDEDCC;padding-right:2pt;"  align="right"><div style='font-size:10pt;font-family:arial;color:#747464;font-weight:bold;'><span style="color:#7C3535;font-weight:bold;">&nbsp;*&nbsp;</span>������</div></td>
		<td style="padding-top:2pt;padding-bottom:2pt;" class="val_td">
			<table width="100%">
				<tr>
					<td style="padding-right:2pt;">		
						<input type="text" id="region_text" name="region_text" style="width:100%;" value="{$smarty.post.region_text}" class="{if $ErrorInput_region_text}inputErrorData{else}inputDataMandatory{/if}" onclick='this.className="inputDataMandatory"' onblur="ChangeBorder(this);">
					</td>
				</tr>
			</table>													
		</td>
	</tr>

	</table>
		
	<br>
	
	<table width='80%' align='center' bgcolor='#EDEDCC' style='border-top: 1px; border-bottom: 1px; border-left:1px;  border-right:1px;  border-color:#C1C1A4;  border-style: solid;' cellpadding="0" cellspacing="0" >
	
    <tr bgcolor='#E1E1C1'>
      <td  valign='top' align='left' colspan=2 style='padding-left:5px;padding-top:3px;padding-bottom:3px;'><span style='font-size:10pt;font-family:arial;color:#747464'><b>���������� ����������</b></span></td>
    </tr>
	
	
	<tr>
		<td class="val_td" width="30%" style="background-color:#EDEDCC;padding-right:2pt;"  align="right"><div style='font-size:10pt;font-family:arial;color:#747464;font-weight:bold;'>���� ����������� ����������</div></td>
		<td style="padding-top:2pt;padding-bottom:2pt;" class="val_td">
			<table width="100%">
				<tr>
					<td style="padding-right:4pt;">		
						<input type="text" name="period" style="width:100%;" value="{$smarty.post.period}">
					</td>
				</tr>
			</table>										
		</td>
	</tr>
	<tr>
		<td class="val_td" style="background-color:#EDEDCC;padding-right:2pt;"  align="right"><div style='font-size:10pt;font-family:arial;color:#747464;font-weight:bold;'>�����������</div></td>
		<td style="padding-top:2pt;padding-bottom:2pt;" class="val_td">
			<table width="100%">
				<tr>
					<td style="padding-right:4pt;">		
						<input type="text" name="payback" style="width:100%;" value="{$smarty.post.payback}">
					</td>
				</tr>
			</table>										
		</td>
	</tr>
	<tr>
		<td class="val_td" style="background-color:#EDEDCC;padding-right:2pt;"  align="right"><div style='font-size:10pt;font-family:arial;color:#747464;font-weight:bold;'>������� ����������</div></td>
		<td style="padding-top:2pt;padding-bottom:2pt;" class="val_td">
			<table width="100%">
				<tr>
					<td>		
						<input type="text" name="yearprofit" style="width:20%;" value="{$smarty.post.yearprofit}">
            			<select style='font-size:10pt;font-family:arial;width:78% ' name="cyearprofit">
            				<option value=usd {if $smarty.post.cyearprofit eq "usd"}selected{else}{/if}>USD (������ ���)
            				<option value=eur {if $smarty.post.cyearprofit eq "eur"}selected{else}{/if}>EUR (����)
            				<option value=rub {if $smarty.post.cyearprofit eq "rub"}selected{else}{/if}>RUB (���������� �����)
            			</select>
					</td>
				</tr>
			</table>													
		</td>
	</tr>
	<tr>
		<td class="val_td" style="background-color:#EDEDCC;padding-right:2pt;" align="right"><div style='font-size:10pt;font-family:arial;color:#747464;font-weight:bold;'>����� ������������� ����������</div></td>
		<td style="padding-top:2pt;padding-bottom:2pt;" class="val_td">
			<table width="100%">
				<tr>
					<td>		
						<input type="text" name="totalinvestcost" style="width:20%;" value="{$smarty.post.totalinvestcost}">
            			<select style='font-size:10pt;font-family:arial;width:78% ' name="totalinvestccost">
            				<option value=usd {if $smarty.post.ccost eq "usd"}selected{else}{/if}>USD (������ ���)
            				<option value=eur {if $smarty.post.ccost eq "eur"}selected{else}{/if}>EUR (����)
            				<option value=rub {if $smarty.post.ccost eq "rub"}selected{else}{/if}>RUB (���������� �����)
            			</select>
					</td>
				</tr>
			</table>													
		</td>
	</tr>

	</table>

	<br>
	
	<table width='80%' align='center' bgcolor='#EDEDCC' style='border-top: 1px; border-bottom: 1px; border-left:1px;  border-right:1px;  border-color:#C1C1A4;  border-style: solid;' cellpadding="0" cellspacing="0" >
	
    <tr bgcolor='#E1E1C1'>
      <td  valign='top' align='left' colspan=2 style='padding-left:5px;padding-top:3px;padding-bottom:3px;'><span style='font-size:10pt;font-family:arial;color:#747464'><b>�������������� ����������</b></span></td>
    </tr>
	
	
	<tr>
		<td class="val_td" width="30%" style="background-color:#EDEDCC;padding-right:2pt;"  align="right"><div style='font-size:10pt;font-family:arial;color:#747464;font-weight:bold;'>������ ����������</div></td>
		<td style="padding-top:2pt;padding-bottom:2pt;">
			<table width="100%">
				<tr>
					<td style="padding-right:4pt;">		
						<textarea rows="4" name="stage" style="width:100%;">{$smarty.post.stage}</textarea>		
					</td>
				</tr>
			</table>			
		</td>
	</tr>
	<tr>
		<td class="val_td" style="background-color:#EDEDCC;padding-right:2pt;"  align="right"><div style='font-size:10pt;font-family:arial;color:#747464;font-weight:bold;'>��������� � �������</div></td>
		<td style="padding-top:2pt;padding-bottom:2pt;">
			<table width="100%">
				<tr>
					<td style="padding-right:4pt;">		
						<textarea rows="4" name="documents" style="width:100%;">{$smarty.post.documents}</textarea>		
					</td>
				</tr>
			</table>			
		</td>
	</tr>
	<tr>
		<td class="val_td" style="background-color:#EDEDCC;padding-right:2pt;"  align="right"><div style='font-size:10pt;font-family:arial;color:#747464;font-weight:bold;'><span style="color:#7C3535;font-weight:bold;">&nbsp;*&nbsp;</span>������� ��������</div></td>
		<td style="padding-top:2pt;padding-bottom:2pt;">
			<table width="100%">
				<tr>
					<td style="padding-right:3pt;"> 		
						<textarea rows="4" id="shortDescription" name="shortDescription" style="width:100%;" class="{if $ErrorInput_shortDescription}inputErrorData{else}inputDataMandatory{/if}" onclick='this.className="inputDataMandatory"' onblur="ChangeBorder(this);">{$smarty.post.shortDescription}</textarea>		
					</td>
				</tr>
			</table>			
		</td>
	</tr>	
	
	<tr>
		<td class="val_td" style="background-color:#EDEDCC;padding-right:2pt;"  align="right"><div style='font-size:10pt;font-family:arial;color:#747464;font-weight:bold;'>������������ ���������</div></td>
		<td style="padding-top:2pt;padding-bottom:2pt;">
			<table width="100%">
				<tr>
					<td style="padding-right:4pt;">		
						<textarea rows="4" name="attractedPerson" style="width:100%;">{$smarty.post.attractedPerson}</textarea>		
					</td>
				</tr>
			</table>			
		</td>
	</tr>	

	</table>

	<br>
	
	<table width='80%' align='center' bgcolor='#EDEDCC' style='border-top: 1px; border-bottom: 1px; border-left:1px;  border-right:1px;  border-color:#C1C1A4;  border-style: solid;' cellpadding="0" cellspacing="0" >
	
    <tr bgcolor='#E1E1C1'>
      <td  valign='top' align='left' colspan=2 style='padding-left:5px;padding-top:3px;padding-bottom:3px;'><span style='font-size:10pt;font-family:arial;color:#747464'><b>���������� ����������</b></span></td>
    </tr>
	
	<tr>
		<td class="val_td" width="30%" style="background-color:#EDEDCC;padding-right:2pt;"  align="right"><div style='font-size:10pt;font-family:arial;color:#747464;font-weight:bold;'><span style="color:#7C3535;font-weight:bold;">&nbsp;*&nbsp;</span>���������� ����</div></td>
		<td style="padding-top:2pt;padding-bottom:2pt;" class="val_td">
			<table width="100%">
				<tr>
					<td style="padding-right:2pt;">		
						<input type="text" id="contactface" name="contactface" style="width:100%;" value="{$smarty.post.contactface}" class="{if $ErrorInput_contactface}inputErrorData{else}inputDataMandatory{/if}" onclick='this.className="inputDataMandatory"' onblur="ChangeBorder(this);">
					</td>
				</tr>
			</table>			
		</td>
	</tr>
	<tr>
		<td class="val_td" style="background-color:#EDEDCC;padding-right:2pt;"  align="right"><div style='font-size:10pt;font-family:arial;color:#747464;font-weight:bold;'><span style="color:#7C3535;font-weight:bold;">&nbsp;*&nbsp;</span>��������</div></td>
		<td style="padding-top:2pt;padding-bottom:2pt;" class="val_td">
			<table width="100%">
				<tr>
					<td style="padding-right:2pt;">		
						<input type="text" id="contactphone" name="contactphone" style="width:100%;" value="{$smarty.post.contactphone}" class="{if $ErrorInput_contactphone}inputErrorData{else}inputDataMandatory{/if}" onclick='this.className="inputDataMandatory"' onblur="ChangeBorder(this);">
					</td>
				</tr>
			</table>			
		</td>
	</tr>
	<tr>
		<td class="val_td" style="background-color:#EDEDCC;padding-right:2pt;"  align="right"><div style='font-size:10pt;font-family:arial;color:#747464;font-weight:bold;'>����</div></td>
		<td style="padding-top:2pt;padding-bottom:2pt;" class="val_td">
			<table width="100%">
				<tr>
					<td style="padding-right:4pt;">		
						<input type="text" name="contactfax" style="width:100%;" value="{$smarty.post.contactfax}">
					</td>
				</tr>
			</table>			
		</td>
	</tr>
	<tr>
		<td class="val_td" style="background-color:#EDEDCC;padding-right:2pt;"  align="right"><div style='font-size:10pt;font-family:arial;color:#747464;font-weight:bold;'><span style="color:#7C3535;font-weight:bold;">&nbsp;*&nbsp;</span>Email</div></td>
		<td style="padding-top:2pt;padding-bottom:2pt;" class="val_td">
			<table width="100%">
				<tr>
					<td style="padding-right:2pt;">		
						<input type="text" id="contactemail" name="contactemail" style="width:100%;" value="{$smarty.post.contactemail}" class="{if $ErrorInput_contactemail}inputErrorData{else}inputDataMandatory{/if}" onclick='this.className="inputDataMandatory"' onblur="ChangeBorder(this);">
					</td>
				</tr>
			</table>			
		</td>
	</tr>
	
    <tr>
		<td class="val_td" style="background-color:#EDEDCC;padding-right:2pt;"  align="right"><div style='font-size:10pt;font-family:arial;color:#747464;font-weight:bold;'>������������ ����������</div></td>
    	<td style="padding-top:2pt;padding-bottom:2pt;"  class="val_td">
			<table width="100%">
				<tr>
					<td>    	
        				<select name='period_id' style="width:100%;">
            				<option value='1' {if $smarty.post.TarifID eq 1}selected{/if} >1 ���.</option>
                			<option value='2' {if $smarty.post.TarifID eq 2}selected{/if} >2 ���.</option>
                			<option value='3' {if $smarty.post.TarifID eq 3}selected{/if} >3 ���.</option>
            			</select>
					</td>
				</tr>
			</table>            
        </td> 
    </tr>
	
	
	<tr>
		<td colspan="2" align="right" class="val_td"><input type="submit" value="���������"></td>
	</tr>
	
</table>

</form>

{literal}
<script language="javascript">

	try
	{
		var obj_name = document.getElementById("name");
		ChangeBorder(obj_name);

		var obj_cost = document.getElementById("cost");
		ChangeBorder(obj_cost);
		
		var obj_shortDescription = document.getElementById("shortDescription");
		ChangeBorder(obj_shortDescription);
		
		var obj_contactface = document.getElementById("contactface");
		ChangeBorder(obj_contactface);

		var obj_contactphone = document.getElementById("contactphone");
		ChangeBorder(obj_contactphone);
		
		var obj_contactemail = document.getElementById("contactemail");
		ChangeBorder(obj_contactemail);
		
		var obj_region_text = document.getElementById("region_text");
		ChangeBorder(obj_region_text);
		
		
	}
	catch(e)
	{
		
	}
</script>
{/literal}
