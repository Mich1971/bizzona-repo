{if sizeof($datasugequipment) > 0}

<div style="background-color:#fff;padding-top:10pt;width:auto;" >

<table class="w" bgcolor="#d9d9b9" border="0">
	<tr>
		<td nowrap align="right">
			<div style="padding-left:13pt;" id="a_nm_h_none"><a href="{$smarty.const.NAMESERVER}insertequipment.php"  class="a_nm_h_none_l"  style="color:#7c3535;text-decoration:underline;">Продать оборудование</a>&nbsp;&nbsp;</div>
		</td>
	</tr>
</table>	  
    
<table class="w"> 

<tr>
 
{foreach name=equipment key=key item=item from=$datasugequipment}

<td width="{if sizeof($datasugequipment) > 3}25%{else}30%{/if}" valign="top">

<table class="w" cellpadding="0" cellspacing="0" border="0">
   <tr>
      <td class="nb_h1_gif" style="background-image:url({$smarty.const.NAMESERVER}images/{if $key is div by 2}nd_g_lt.gif{else}nd_w_lt.gif{/if});"></td>
      <td bgcolor="{if $key is div by 2}#e4e4e2{else}#f6f6f5{/if}" class="nb_10_12" style="background-image:url({$smarty.const.NAMESERVER}images/nd_b_10_12.gif);" width="99%"></td>
      <td class="nb_h2_gif" style="background-image:url({$smarty.const.NAMESERVER}images/{if $key is div by 2}nd_g_rt.gif{else}nd_w_rt.gif{/if});"></td>
   </tr>
   <tr> 
   	<td class="nb_border_l_gif" style="background-image:url({$smarty.const.NAMESERVER}images/{if $key is div by 2}nd_border_10.gif{else}nd_border_w_10.gif{/if});"></td>
   	<td bgcolor="{if $key is div by 2}#e4e4e2{else}#f6f6f5{/if}" valign="top">
   		<table border="0" class="w">
        	<tr>
            	<td valign="middle" >
					<table cellpadding="0" cellspacing="0" border="0" >
                    	<tr>
      						<td class="nb_price_h1_gif" style="background-image:url({$smarty.const.NAMESERVER}images/nd_price_lt.gif);"></td>
      						<td bgcolor="#0e6e9b" class="nb_price_7_1" style="background-image:url({$smarty.const.NAMESERVER}images/nd_price_b_7_1.gif);" width="99%"></td>
      						<td class="nb_price_h2_gif" style="background-image:url({$smarty.const.NAMESERVER}images/nd_price_rt.gif);"></td>                     	
                     	</tr>
   						<tr>
   							<td class="nb_price_border_l_gif" style="background-image:url({$smarty.const.NAMESERVER}images/ng_border_price_7_1.gif);"></td>
   							<td bgcolor="#0e6e9b" valign="middle" align="center"  class="nd_ht__pr" nowrap>
								{if $item->agreementcost or $item->cost == 0}<b>договорная</b>{else}{include file="./site/currency.tpl" type=$item->ccost cost=$item->cost}{/if}
   							</td>
   							<td class="nb_price_border_r_gif" style="background-image:url({$smarty.const.NAMESERVER}images/ng_border_price_7_1.gif);"></td>
   						</tr>
   						<tr>
      						<td class="nb_price_h4_gif" style="background-image:url({$smarty.const.NAMESERVER}images/nd_price_lb.gif);"></td>
      						<td bgcolor="#0e6e9b" class="nb_price_7_1" style="background-image:url({$smarty.const.NAMESERVER}images/nd_price_b_7_1.gif);" width="99%"></td>
      						<td class="nb_price_h3_gif" style="background-image:url({$smarty.const.NAMESERVER}images/nd_price_rb.gif);"></td>
   						</tr>   							
                     </table>
                 </td>
                <td valign="top" width="40%" valign="middle" class="h_cod s{$item->id}" style="color:#484848;">Код: {$item->id}</td>
            </tr>
   		
        	<tr> 
            	<td colspan="2"><div id="a_nm_h_none"><a href="{$smarty.const.NAMESERVER}detailsEquipment/{$item->id}" class="a_nm_h_none" >{$item->name}</a></div></td>
            </tr>
            <tr>
            	<td colspan="2"><div class="sd">{if strlen($item->shopcartimg) > 0}<img src="http://www.bizzona.ru/shopimg/{$item->shopcartimg}" align="right" style="border:1px solid #000000" hspace="2">{/if}  {$item->shortdescription}</div></td>
            </tr>
            <tr>
            	<td colspan="2" align="left">
                	<div id="a_nm_h_none"><span class="txt_term" style="color:#6d6d6d;">Расположение: </span><a href="{$smarty.const.NAMESERVER}searchequipment/city/{$item->city_id}" class="a_hot_city_none">{NameCity ID=$item->city_id}</a></div>
                        {if $item->age}
                        	<div class="txt_term" style="color:#6d6d6d;">Возраст оборудования: <span class="nd_val_term_h">{$item->age}</span> </div>
                        {/if}
                </td>
            </tr>
            <tr>  
            	<td align="left" nowrap><div style="font-size:8pt;font-family:Arial;">просмотрено: <span class="look_h_num">{$item->qview}</span></div></td>
                <td align="right" nowrap><a href="{$smarty.const.NAMESERVER}detailsEquipment/{$item->id}" class="hld">Подробнее</a> <span class="hld">>></span></td>
            </tr>
   		</table>
   	</td>
   	<td class="nb_border_r_gif" style="background-image:url({$smarty.const.NAMESERVER}images/{if $key is div by 2}nd_border_10.gif{else}nd_border_w_10.gif{/if});"></td>
   </tr>
   <tr>
      <td class="nb_h4_gif" style="background-image:url({$smarty.const.NAMESERVER}images/{if $key is div by 2}nd_g_lb.gif{else}nd_w_lb.gif{/if});"></td>
      <td bgcolor="{if $key is div by 2}#e4e4e2{else}#f6f6f5{/if}" class="nb_10_12" style="background-image:url({$smarty.const.NAMESERVER}images/nd_b_10_12.gif);" width="99%"></td>
      <td class="nb_h3_gif" style="background-image:url({$smarty.const.NAMESERVER}images/{if $key is div by 2}nd_g_rb.gif{else}nd_w_rb.gif{/if});"></td>
   </tr>
</table>

</td>

{/foreach}

</tr>
</table>
 
<table class="w" border="0" bgcolor="#d9d9b9">
	<tr>
		<td nowrap align="right"> 
			<div  id="a_nm_h_none"><a href="{$smarty.const.NAMESERVER}searchequipment.php" class="a_nm_h_none_l"  style="font-weight:bold;font-size:10pt;">Все предложения оборудования</a>&nbsp;&nbsp;</div>
		</td>
	</tr>
</table>	 


</div>

{/if}