{literal}
<script language="javascript">
  String.prototype.trim = function() {
  	return this.replace(/(^\s+|\s+$)/g, "");
  }

  function ChangeBorder(obj)
  {
  	try
  	{
		if(obj.value.trim().length > 0) 
		{
			obj.style.borderColor='green';
		}
		else
		{
			obj.style.borderColor='red';
			obj.value = "";
		}
  	}
  	catch(e)
  	{
  		
  	}
  }
</script>
{/literal}


<form method="POST">



<div align="center" style="font-family:arial;font-size:10pt;"><span style="color:red;font-weight:bold;">*</span> ���������� ���� ����������� ��� ���������� </div>

<table width='90%' align='center' bgcolor='#EDEDCC' style='border-top: 1px; border-bottom: 1px; border-left:1px;  border-right:1px;  border-color:#C1C1A4;  border-style: solid;' cellpadding="0" cellspacing="0" >

	<tr {if strlen($text_error) > 0 }{else}style="display:none;"{/if}>
		<td colspan="2" align="center" ><span style="font-size:10pt;font-family:arial;color:red;font-weight:bold;">{$text_error}</span></td>
	</tr>
	<tr {if strlen($text_success) > 0 }{else}style="display:none;"{/if}>
		<td colspan="2" align="center"><span style="font-size:10pt;font-family:arial;color:green;font-weight:bold;">{$text_success}</span></td>
	</tr>
	<tr>
		<td valign="top" align='left' bgcolor='#EDEDCC' width='20%' style='padding-left:5px;'><div style="font-size:10pt;font-family:arial;"><span style="color:red;font-weight:bold;">&nbsp;*&nbsp;</span>�������� �������</div></td>
		<td width="80%" style="padding-right:3pt;padding-top:0pt;padding-bottom:2pt;padding-left:2pt;"  class="val_td">
			<table width="100%">
				<tr>
					<td style="padding-right:2pt;">
						<input type="text" id="nameID" name="nameID" style="width:100%;" value="{$smarty.post.nameID}" class="{if $ErrorInput_nameID}inputErrorData{else}inputDataMandatory{/if}" onclick='this.className="inputDataMandatory"'  onblur="ChangeBorder(this);">
						<div style="font-size:8pt;font-family:arial;">����� "����� �������"</div>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td valign='top' align='left' bgcolor='#EDEDCC' width='20%' style='padding-left:5px;'><div style="font-size:10pt;font-family:arial;">��� �������</div></td>
		<td width="80%" style="padding-right:3pt;padding-top:0pt;padding-bottom:2pt;padding-left:2pt;"  class="val_td">
          <select name="categoryID" class='inputData' style='font-size:10pt;font-family:tahoma;width:100%;'>
             {foreach name='category' item=item key=key from=$listCategory}
               <optgroup label="{$item->Name}" style="background-color:#EDEDCC;color:#000000;">
               {foreach name='subcategory' item=subitem key=subkey from=$listSubCategory}
             	  {if $item->ID eq $subitem->CategoryID}
             	  <option style="background-color:#ffffff;" value="{$item->ID}:{$subitem->ID}" {if $data->CategoryID eq $item->ID and $subitem->ID eq $data->SubCategoryID}selected{/if} >{$subitem->Name}</option>
             	  {/if}
               {/foreach}
               </optgroup>
             {/foreach}
           </select>        
		</td>
	</tr>
	<tr>
		<td valign='top' align='left' bgcolor='#EDEDCC' width='20%' style='padding-left:5px;'><div style="font-size:10pt;font-family:arial;">���������</div></td>
		<td width="80%" style="padding-right:3pt;padding-top:0pt;padding-bottom:2pt;padding-left:2pt;"  class="val_td">
			<table>
				<tr>
					<td><div style="font-size:10pt;font-family:arial;">��</div></td>
					<td><input type="text" name="cost_startID" value="{$smarty.post.cost_startID}"></td>
					<td rowspan="2">
						<select name="currencyID" style='font-size:9pt;font-family:arial;width:45pt;padding-left:2pt;'>
							<option value='rub' {if $smarty.post.currencyID eq "rub"} selected {/if}>���
							<option value='usd' {if $smarty.post.currencyID eq "usd"} selected {/if}>USD
							<option value='eur' {if $smarty.post.currencyID eq "eur"} selected {/if}>EUR
						</select>
					</td>
				</tr>
				<tr>
					<td><div style="font-size:10pt;font-family:arial;">��</div></td>
					<td><input type="text" name="cost_stopID" value="{$smarty.post.cost_stopID}"></td>
				</tr>
			</table>
		</td>
	</tr>
</table>	

<br>

<table width='90%' align='center' bgcolor='#EDEDCC' style='border-top: 1px; border-bottom: 1px; border-left:1px;  border-right:1px;  border-color:#C1C1A4;  border-style: solid;' cellpadding="0" cellspacing="0" >
	
	<tr>
		<td valign='top' align='left' bgcolor='#EDEDCC' width='20%' style='padding-left:5px;'><div style="font-size:10pt;font-family:arial;">����</div></td>
		<td width="80%" style="padding-right:3pt;padding-top:0pt;padding-bottom:2pt;padding-left:2pt;"  class="val_td">
			<table width="100%" >
				<tr>
					<td>
						<input type="text" id="partID" name="partID" value="{$smarty.post.partID}" style="width:100%;" >
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td valign='top' align='left' bgcolor='#EDEDCC' width='20%' style='padding-left:5px;'><div style="font-size:10pt;font-family:arial;"><span style="color:red;font-weight:bold;">&nbsp;*&nbsp;</span>������������</div></td>
		<td width="80%" style="padding-right:3pt;padding-top:0pt;padding-bottom:2pt;padding-left:2pt;"  class="val_td"> <div style="font-size:8pt;font-family:arial;padding-left:2pt;">���������� ����� / ����� / ���� / ������� (���������� ����� ������� � ������ ��������� ������)</div>
			<table width="100%">
				<tr>
					<td style="padding-right:2pt;">
						<input type="text" id="regionTextID" name="regionTextID" value="{$smarty.post.regionTextID}" style="width:100%;" class="{if $ErrorInput_regionTextID}inputErrorData{else}inputDataMandatory{/if}" onclick='this.className="inputDataMandatory"'  onblur="ChangeBorder(this);">
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td valign='top' align='left' bgcolor='#EDEDCC' width='20%' style='padding-left:5px;'><div style="font-size:10pt;font-family:arial;">�������� ��������</div></td>
		<td width="80%" style="padding-right:3pt;padding-top:2pt;padding-bottom:2pt;padding-left:2pt;"  class="val_td">
			<table width="100%">
				<tr>
					<td valign="top"><div style="font-size:10pt;font-family:arial;">������������</div></td>
				</tr>
				<tr>
					<td><textarea style="width:100%;height:70px" name="realtyID">{$smarty.post.realtyID}</textarea></td>
				</tr>
			</table>
		</td>
	</tr>
</table>	

<br>

<table width='90%' align='center' bgcolor='#EDEDCC' style='border-top: 1px; border-bottom: 1px; border-left:1px;  border-right:1px;  border-color:#C1C1A4;  border-style: solid;' cellpadding="0" cellspacing="0" >
	<tr>
		<td valign='top' align='left' bgcolor='#EDEDCC' width='20%' style='padding-left:5px;'><div style="font-size:10pt;font-family:arial;">�������������� ����������</div></td>
		<td width="80%" style="padding-right:3pt;padding-top:2pt;padding-bottom:2pt;padding-left:2pt;"  class="val_td">
			<table width="100%">
				<tr>
					<td colspan="2"><div style="font-size:10pt;font-family:arial;">���� ����������</div></td>
				</tr>
				<tr>
					<td colspan="2"><textarea style="width:100%;height:90px" name="otherinfoID">{$smarty.post.otherinfoID}</textarea></td>
				</tr>
				<tr>
					<td width="30%"><div style="font-size:10pt;font-family:arial;">������� �������</div></td>
					<td><input type="text" name="ageID" value="{$smarty.post.ageID}" style="width:100%;"></td>
				</tr>
				<tr>
					<td><div style="font-size:10pt;font-family:arial;">���� �����������</div></td>
					<td><input type="text" name="paybackID" value="{$smarty.post.paybackID}" style="width:100%;"></td>
				</tr>
				<tr>
					<td><div style="font-size:10pt;font-family:arial;">�����������, ��������</div></td>
					<td><input type="text" name="licenseID" style="width:100%;" value="{$smarty.post.licenseID}"></td>
				</tr>
			</table>
		</td>
	</tr>
	
</table>	

<br>

<table width='90%' align='center' bgcolor='#EDEDCC' style='border-top: 1px; border-bottom: 1px; border-left:1px;  border-right:1px;  border-color:#C1C1A4;  border-style: solid;' cellpadding="0" cellspacing="0" >
	<tr>
		<td valign="top" align='left' bgcolor='#EDEDCC' width='20%' style='padding-left:5px;'><div style="font-size:10pt;font-family:arial;">���������� ����������</div></td>
		<td width="80%" style="padding-right:3pt;padding-top:2pt;padding-bottom:2pt;padding-left:2pt;"  class="val_td">
			<table width="100%">
				<tr>
					<td width="25%"><div style="font-size:10pt;font-family:arial;"><span style="color:red;font-weight:bold;">&nbsp;*&nbsp;</span>���������� ����</div></td>
					<td style="padding-right:2pt;"><input type="text" id="FMLID" name="FMLID" style="width:100%;" value="{$smarty.post.FMLID}" class="{if $ErrorInput_FMLID}inputErrorData{else}inputDataMandatory{/if}" onclick='this.className="inputDataMandatory"'  onblur="ChangeBorder(this);"><div style="font-size:8pt;font-family:arial;">(������� �����������)</div></td>
				</tr>
				<tr>
					<td><div style="font-size:10pt;font-family:arial;"><span style="color:red;font-weight:bold;">&nbsp;*&nbsp;</span>��������</div></td>
					<td style="padding-right:2pt;"><input type="text" id="phoneID" name="phoneID" style="width:100%;" value="{$smarty.post.phoneID}" class="{if $ErrorInput_phoneID}inputErrorData{else}inputDataMandatory{/if}" onclick='this.className="inputDataMandatory"'  onblur="ChangeBorder(this);"><div style="font-size:8pt;font-family:arial;">(������� �����������)</div></td>
				</tr>
				<tr>
					<td><div style="font-size:10pt;font-family:arial;"><span style="color:red;font-weight:bold;">&nbsp;*&nbsp;</span>Email</div></td>
					<td style="padding-right:2pt;"><input type="text" id="emailID" name="emailID" value="{$smarty.post.emailID}" style="width:100%;" class="{if $ErrorInput_emailID}inputErrorData{else}inputDataMandatory{/if}" onclick='this.className="inputDataMandatory"'  onblur="ChangeBorder(this);"><div style="font-size:8pt;font-family:arial;">(������� �����������)</div></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td valign='top' align='left' bgcolor='#EDEDCC' width='20%' style='padding-left:5px;'><div style="font-size:10pt;font-family:arial;">������������ ����������</div></td>
		<td width="80%" style="padding-right:3pt;padding-top:2pt;padding-bottom:2pt;padding-left:2pt;"  class="val_td">
			<select name="durationID" style="width:100%;">
				<option value="1" {if $smarty.post.durationID eq "1"}selected{else}{/if}>1 �����</option>
				<option value="2" {if $smarty.post.durationID eq "2"}selected{else}{/if}>2 ������</option>
				<option value="3" {if $smarty.post.durationID eq "3"}selected{else}{/if}>3 ������</option>
			</select>
		</td>
	</tr>
	<tr>
		<td valign='top' align='left' bgcolor='#EDEDCC' width='20%' style='padding-left:5px;'><div style="font-size:10pt;font-family:arial;">�������� �� Email �����������</div></td>
		<td width="80%" style="padding-right:3pt;padding-top:2pt;padding-bottom:2pt;padding-left:2pt;"  class="val_td">
			<input type="checkbox" name="subscribe_status_id" {if isset($smarty.post) and sizeof($smarty.post) > 0} {if isset($smarty.post.subscribe_status_id)}checked{else}{/if}   {else}checked{/if} >
		</td>
	</tr>
	<tr>
		<td colspan="2" align="right"><input type="submit" value="���������"  style="font-size:10pt;font-family:arial;"></td>
	</tr>
	
</table>

</form>

{literal}
<script language="javascript">

	try
	{
		var obj_nameID = document.getElementById("nameID");
		ChangeBorder(obj_nameID);

		var obj_regionTextID = document.getElementById("regionTextID");
		ChangeBorder(obj_regionTextID);
		
		var obj_FMLID = document.getElementById("FMLID");
		ChangeBorder(obj_FMLID);
		
		var obj_phoneID = document.getElementById("phoneID");
		ChangeBorder(obj_phoneID);
		
		var obj_emailID = document.getElementById("emailID");
		ChangeBorder(obj_emailID);
		
	}
	catch(e)
	{
		
	}
</script>
{/literal}