{literal}
<script language="javascript">
  String.prototype.trim = function() {
  	return this.replace(/(^\s+|\s+$)/g, "");
  }

  function ChangeBorder(obj)
  {
  	try
  	{
		if(obj.value.trim().length > 0) 
		{
			obj.style.borderColor='green';
		}
		else
		{
			obj.style.borderColor='red';
			obj.value = "";
		}
  	}
  	catch(e)
  	{
  		
  	}
  }
</script>
{/literal}

   {if $code > 0}

			 {include file="./site/confirmpaymentarenda.tpl"}

   {else}

<form  enctype="multipart/form-data" method="post">

    <table width='100%' align='center' bgcolor='#EDEDCC' style='border-top: 1px; border-bottom: 1px; border-left:1px;  border-right:1px;  border-color:#C1C1A4;  border-style: solid;' cellpadding="0" cellspacing="0" >
    
    <tr>
      <td  valign='top' style='padding-left:5pt;padding-right:5pt;'> 
      	<div align="center" style="padding-top:5pt;padding-bottom:5pt;font-size:14pt;font-weight:bold;">���������� ���������� �� ������� ������ ������� (� ������ ������)</div>
        {$insertsettings.content}
        {if $insertsettings.use_promo} 
            {include file='./site/promo.tpl'}
        {/if}        
      </td>
      <td width="25%" valign="top" style="padding-top:5pt;padding-right:5pt;">
      	{include file='./site/requestmin.tpl'}
      </td>
    </tr>
    
    <tr> 
    	<td colspan="2" style='padding-left:5pt;padding-right:5pt;'>
	         <span style='font-size:10pt;font-family:arial;'>
				<br><br> 
				���� ���� ������� � ������� ��� ������ � ������ ��������� ��������, <b>{$contact->supportPhone}</b>, e-mail: <b>{$contact->supportEmail}</b> � ����������� �������!
			 </span>
			 <br><br>
    	</td>
    </tr>

   </table>



  


  <input type='hidden' name='insert' value='insert'>
  <center> 
  <span style='color:red;'>*</span><span style='font-size:10pt;font-family:arial;'> ���������� ���� ����������� ��� ����������</span>
  </center> 


<table width='80%' align='center'>
  <tr>
   <td valign='top' width='30%'>

  <table width='100%' align='center' bgcolor='#ffffff' style='border-top: 1px; border-bottom: 1px; border-left:1px;  border-right:1px;  border-color:#C1C1A4;  border-style: solid;' cellpadding="0" cellspacing="0">

    <tr bgcolor='#E1E1C1' align='left'>
      <td  valign='top' align='left' colspan=2 style='padding-left:5px;padding-top:3px;padding-bottom:3px;'><span style='font-size:10pt;font-family:arial;color:#747464'><b>���������� ������</b></span></td>
    </tr>

    <tr>
      <td  valign='top' align='left' bgcolor='#EDEDCC' width='30%' style='padding-left:5px;'>
        <span style='color:red;'>*</span>&nbsp;<span style='font-size:8pt;font-family:tahoma;color:#747464;'><b>{$Description.FML}</b>&nbsp;(������� �����������)</span>
      </td>
    </tr>         

    <tr>
      <td valign='top' align='left' bgcolor='#EDEDCC' width='30%' style='padding-left:5px;'>
         <input type='text' size=40 id="FML" name="FML" value="{$smarty.post.FML}" maxlength='40' class="{if $ErrorInput_FML}inputErrorData{else}inputDataMandatory{/if}" onclick='this.className="inputDataMandatory"'  onblur="ChangeBorder(this);">
      </td>
    </tr>         


    <tr>
      <td valign='top' align='left' bgcolor='#EDEDCC' width='30%' style='padding-left:5px;'>
        <span style='color:red;'>*</span>&nbsp;<span style='font-size:8pt;font-family:tahoma;color:#747464;'><b>{$Description.PhoneID}</b>&nbsp;(������� �����������)</span>
      </td>
    </tr>         

    <tr>
      <td valign='top' align='left' bgcolor='#EDEDCC' width='30%' style='padding-left:5px;'>
         <input type='text' id="PhoneID" size=40 name="PhoneID" value="{$smarty.post.PhoneID}" maxlength='40' class="{if $ErrorInput_PhoneID}inputErrorData{else}inputDataMandatory{/if}" onclick='this.className="inputDataMandatory"' onblur="ChangeBorder(this);">
      </td>
    </tr>         



    <tr>
      <td valign='top' align='left' bgcolor='#EDEDCC' width='30%' style='padding-left:5px;'>
        <span style='font-size:8pt;font-family:tahoma;color:#747464;'><b>{$Description.FaxID}</b>&nbsp;(������� �����������)</span>
      </td>
    </tr>         

    <tr>
      <td valign='top' align='left' bgcolor='#EDEDCC' width='30%' style='padding-left:5px;'>
         <input type='text'  size=40 name="FaxID" value="{$smarty.post.FaxID}" maxlength='40' style='border-top: 1px; border-bottom: 1px; border-left:1px;  border-right:1px;  border-color:#C1C1A4;  border-style: solid;'>
      </td>
    </tr>         


    <tr>
      <td valign='top' align='left' bgcolor='#EDEDCC' width='30%' style='padding-left:5px;'>
        <span style='color:red;'>*</span>&nbsp;<span style='font-size:8pt;font-family:tahoma;color:#747464;'><b>{$Description.EmailID}</b>&nbsp;(������� �����������)</span>
      </td>
    </tr>         

    <tr>
      <td valign='top' align='left' bgcolor='#EDEDCC' width='30%' style='padding-left:5px;padding-bottom:5px;padding-right:5px;'>
         <input type='text' id="EmailID" size=40 name="EmailID" value="{$smarty.post.EmailID}" maxlength='40' class="{if $ErrorInput_EmailID}inputErrorData{else}inputDataMandatory{/if}" onclick='this.className="inputDataMandatory"'  onblur="ChangeBorder(this);">
      </td>
    </tr>         

  </table>
  
     </td>
     <td valign='top' width='70%'>

  <table width='100%' align='center' bgcolor='#ffffff' style='border-top: 1px; border-bottom: 1px; border-left:1px;  border-right:1px;  border-color:#C1C1A4;  border-style: solid;' cellpadding="0" cellspacing="0">

    <tr bgcolor='#E1E1C1'>
      <td  valign='top' align='left' colspan=2 style='padding-left:5px;padding-top:3px;padding-bottom:3px;'><span style='font-size:10pt;font-family:tahoma;color:#747464'><b>�������� ���������� � �������</b></span></td>
    </tr>

    <tr>
      <td  align='left' valign='top' align='left' bgcolor='#EDEDCC' width='45%' style='padding-left:5px;padding-bottom:5px;padding-right:5px;'>
         <table cellpadding="0" cellspacing="0">
           <tr>
             <td align='left'>
               <span style='color:red;'>*</span>&nbsp;<span style='font-size:8pt;font-family:tahoma;color:#747464;'><b>��������� ������</b></span>
             </td>
           </tr>
           <tr>
             <td>
               <nobr>
               <input type='text' size=20 id="CostID" name="CostID" value="{$smarty.post.CostID}" maxlength='40' class="{if $ErrorInput_CostID}inputErrorData{else}inputDataMandatory{/if}" onclick='this.className="inputDataMandatory"' onblur="ChangeBorder(this);">
               <select style='font-size:9pt;font-family:arial;width:45pt;padding-left:2pt;' name="cCostID"><option value=rub {if $smarty.post.cCostID eq "rub"}selected{else}{/if}>���<option value=usd {if $smarty.post.cCostID eq "usd"}selected{else}{/if}>USD<option value=eur {if $smarty.post.cCostID eq "eur"}selected{else}{/if}>EUR</select>
               </nobr>
             </td>
           </tr>
          
           <tr>
              <td>
                <span style='font-size:8pt;font-family:tahoma;color:#747464;'><b>{$Description.MonthAveTurnID}</b></span>
              </td>
           </tr>
           <tr>
              <td>
                <nobr>
                <input type='text'  size=20 name="MonthAveTurnID" value="{$smarty.post.MonthAveTurnID}" maxlength='40' class='inputData'>
                <select style='font-size:9pt;font-family:arial;width:45pt;padding-left:2pt;' name="cMonthAveTurnID"><option value=rub {if $smarty.post.cMonthAveTurnID eq "rub"}selected{else}{/if}>���<option value=usd {if $smarty.post.cMonthAveTurnID eq "usd"}selected{else}{/if}>USD<option value=eur {if $smarty.post.cMonthAveTurnID eq "eur"}selected{else}{/if}>EUR</select>
                </nobr>
              </td>  
           </tr>

           <tr>
              <td>
                <span style='font-size:8pt;font-family:tahoma;color:#747464;'><b>{$Description.ProfitPerMonthID}</b></span>
              </td> 
           </tr>
           <tr>
              <td>
                <nobr>
                <input type='text'  size=20 name="ProfitPerMonthID" value="{$smarty.post.ProfitPerMonthID}" maxlength='40' class='inputData'>
                <select style='font-size:9pt;font-family:arial;width:45pt;padding-left:2pt;' name="cProfitPerMonthID"><option value=rub {if $smarty.post.cProfitPerMonthID eq "rub"}selected{else}{/if}>���<option value=usd {if $smarty.post.cProfitPerMonthID eq "usd"}selected{else}{/if}>USD<option value=eur {if $smarty.post.cProfitPerMonthID eq "eur"}selected{else}{/if}>EUR</select>
                </nobr>
              </td>
           </tr> 

           <tr>
              <td>
                <span style='font-size:8pt;font-family:tahoma;color:#747464;'><b>{$Description.MonthExpemseID}</b></span>
              </td>
           </tr>           
           <tr>
              <td>
                <nobr> 
                <input type='text'  size=20 name="MonthExpemseID" value="{$smarty.post.MonthExpemseID}" maxlength='40' class='inputData'>
                <select style='font-size:9pt;font-family:arial;width:45pt;padding-left:2pt;' name="cMonthExpemseID"><option value=rub {if $smarty.post.cMonthExpemseID eq "rub"}selected{else}{/if}>���<option value=usd {if $smarty.post.cMonthExpemseID eq "usd"}selected{else}{/if}>USD<option value=eur {if $smarty.post.cMonthExpemseID eq "eur"}selected{else}{/if}>EUR</select>
                </nobr>  
              </td>
           </tr>  

           <tr>
              <td>
                <span style='font-size:8pt;font-family:tahoma;color:#747464;'><b>{$Description.WageFundID}</b></span>
              </td>
           </tr>
           <tr>
              <td>
                <nobr>
                <input type='text'  size=20 name="WageFundID"  value="{$smarty.post.WageFundID}" maxlength='40' class='inputData'>
                <select style='font-size:9pt;font-family:arial;width:45pt;padding-left:2pt;' name="cWageFundID"><option value=rub {if $smarty.post.cWageFundID eq "rub"}selected{else}{/if}>���<option value=usd {if $smarty.post.cWageFundID eq "usd"}selected{else}{/if}>USD<option value=eur {if $smarty.post.cWageFundID eq "eur"}selected{else}{/if}>EUR</select>
                </nobr>
              </td>
           </tr>  


         </table>
      </td>
      <td valign='top' bgcolor='#EDEDCC'>
         <table cellpadding="0" cellspacing="0">
           <tr>
             <td>
                <span style='color:red;'>*</span>&nbsp;<span style='font-size:8pt;font-family:tahoma;color:#747464;'><b>{$Description.NameBizID}</b></span>
             </td> 
           </tr>
           <tr> 
             <td>
               <textarea id="NameBizID" name='NameBizID' cols='35' rows='6' class="{if $ErrorInput_NameBizID}inputErrorData{else}inputDataMandatory{/if}"  onclick='this.className="inputDataMandatory"' onblur="ChangeBorder(this);">{$smarty.post.NameBizID}</textarea>
             </td> 
           </tr>
           <tr>
             <td>
               <span style='color:red;'>*</span>&nbsp;</span><span style='font-size:8pt;font-family:tahoma;color:#747464;'><b>{$Description.SiteID}</b></span>
             </td> 
           </tr>
           <tr>
             <td>
               <textarea id="SiteID" name='SiteID' cols='35' rows='6' class="{if $ErrorInput_SiteID}inputErrorData{else}inputDataMandatory{/if}"  onclick='this.className="inputDataMandatory"' onblur="ChangeBorder(this);">{$smarty.post.SiteID}</textarea>
             </td> 
           </tr>
         </table> 
      </td>
    </tr>

  </table> 

     </td>
    </tr>
  </table>

  <br>


<table width='80%' align='center'>
  <tr>
   <td>



  <table width='80%' align='center' style='border-top: 1px; border-bottom: 1px; border-left:1px;  border-right:1px;  border-color:#C1C1A4;  border-style: solid;' cellpadding="0" cellspacing="0">


    <tr bgcolor='#E1E1C1' align='left'>
      <td  valign='top' align='left' colspan=2 style='padding-left:5px;padding-top:3px;padding-bottom:3px;'><span style='font-size:10pt;font-family:tahoma;color:#747464'><b>�������������� ���������� � �������</b></span></td>
    </tr>

    <tr>
      <td colspan=2 bgcolor='#EDEDCC' valign='top'>
        <table cellpadding="0" cellspacing="0">
          <tr>
             <td valign='top' style='padding-left:5px;'>
               <table cellpadding="0" cellspacing="0" >

                 <tr>
                   <td>
                     <span style='font-size:8pt;font-family:tahoma;color:#747464;'><b>{$Description.CountEmplID}</b></span>
                   </td> 
                 </tr>
                 <tr>
                   <td>
                     <input type='text'  size=50 name="CountEmplID" value="{$smarty.post.CountEmplID}" maxlength='40' class='inputData'>
                   </td> 
                 </tr>


                 <tr>
                   <td>
                     <span style='font-size:8pt;font-family:tahoma;color:#747464;'><b>{$Description.CountManEmplID}</b></span>
                   </td> 
                 </tr>
                 <tr>
                   <td>
                    <input type='text'  size=50 name="CountManEmplID" value="{$smarty.post.CountManEmplID}" maxlength='40' class='inputData'>
                   </td> 
                 </tr>

                 <tr>
                   <td>
                     <span style='font-size:8pt;font-family:tahoma;color:#747464;'><b>{$Description.TermBizID}</b></span>
                   </td> 
                 </tr>
                 <tr>
                   <td>
                     <input type='text'  size=50 name="TermBizID"  value="{$smarty.post.TermBizID}" maxlength='40' class='inputData'>
                   </td> 
                 </tr>

                 <tr>
                   <td>
                     <span style='color:red;'>*</span>&nbsp;<span style='font-size:8pt;font-family:tahoma;color:#747464;'><b>{$Description.DetailsID}</b></span>
                   </td> 
                 </tr>
                 
             
                 
                 <tr>
                   <td>
                     <textarea id="DetailsID" name='DetailsID' cols='45' rows='10' class="{if $ErrorInput_DetailsID}inputErrorData{else}inputDataMandatory{/if}"  onclick='this.className="inputDataMandatory"'  onblur="ChangeBorder(this);">{$smarty.post.DetailsID}</textarea>
                   </td> 
                 </tr>

                 <tr>
                   <td>
                     <span style='font-size:8pt;font-family:tahoma;color:#747464;'><b>{$Description.TarifID}</b></span>
                   </td> 
                 </tr>
                 <tr>
                   <td>
                     <select name='TarifID' class='inputData' style="width:100%;">
                       <option value='1' {if $smarty.post.TarifID eq 1}selected{/if} >1 ���.</option>
                       <option value='2' {if $smarty.post.TarifID eq 2}selected{/if} >2 ���.</option>
                       <option value='3' {if $smarty.post.TarifID eq 3}selected{/if} >3 ���.</option>
                     </select>
                   </td> 
                 </tr>
                 
                 
                 
                <tr>
                  <td>
                   <span style='font-size:8pt;font-family:tahoma;color:#747464;'><b>{$Description.CategoryID}</b></span>
                  </td> 
                </tr>

                <tr>
                 <td> 
                    <select name="CategoryID" class='inputData' style="width:100%;" >
                      <option value='-1'>������</option>
             			{foreach name='category' item=item key=key from=$listCategory}
               				<optgroup label="{$item->Name}" style="background-color:#EDEDCC;color:#000000;">
               				{foreach name='subcategory' item=subitem key=subkey from=$listSubCategory}
             	  				{if $item->ID eq $subitem->CategoryID}
             	  					<option style="background-color:#ffffff;" value="{$item->ID}:{$subitem->ID}" {if $smarty.post.SubCategoryID eq $subitem->ID}selected{/if} >{$subitem->Name}</option>
             	  				{/if}
               				{/foreach}
               				</optgroup>
             			{/foreach}
                       
                    </select>
                  </td>
                 </tr>

                 <tr>
                   <td>
                     <span style='font-size:8pt;font-family:tahoma;color:#747464;'><b>{$Description.BizFormID}</b></span>
                   </td> 
                 </tr>
                 <tr>
                   <td>
                     <select name="BizFormID" class='inputData' style="width:100%;">
                      <option value='0' {if $smarty.post.BizFormID eq 0}selected{/if}>������</option>
                      <option value='1' {if $smarty.post.BizFormID eq 1}selected{/if} >�������� � ������������ ����������������</option>
                      <option value='2' {if $smarty.post.BizFormID eq 2}selected{/if}>�������� ����������� ��������</option>
                      <option value='3' {if $smarty.post.BizFormID eq 3}selected{/if}>�������� ����������� ��������</option> 
                      <option value='4' {if $smarty.post.BizFormID eq 4}selected{/if}>���������������� ����������</option>
                      <option value='5' {if $smarty.post.BizFormID eq 5}selected{/if}>���������� ���������</option>
                      <option value='6' {if $smarty.post.BizFormID eq 6}selected{/if}>����</option>
                      <option value='7' {if $smarty.post.BizFormID eq 7}selected{/if}>�������������� �����������</option> 
                      <option value='8' {if $smarty.post.BizFormID eq 8}selected{/if}>�����</option>
                     </select>
                   </td>
                 </tr>

               </table>
             </td> 

             <td valign='top' style='padding-left:5px;padding-bottom:5px;'>

               <table cellpadding="0" cellspacing="0">

                 <tr>
                   <td>
                     <span style='font-size:8pt;font-family:tahoma;color:#747464;'><b>{$Description.ListObjectID}</b></span>
                   </td> 
                 </tr>
                 <tr>
                   <td>
                    <textarea name='ListObjectID' cols='40' rows='6' class='inputData'>{$smarty.post.ListObjectID}</textarea>
                   </td> 
                 </tr>



                 <tr>
                   <td>
                     <span style='font-size:8pt;font-family:tahoma;color:#747464;'><b>{$Description.ContactID}</b></span>
                   </td> 
                 </tr>
                 <tr>
                   <td>
                     <textarea name='ContactID' cols='40' rows='6' class='inputData'>{$smarty.post.ContactID}</textarea>
                   </td> 
                 </tr>



                 <tr>
                   <td>
                     <span style='font-size:8pt;font-family:tahoma;color:#747464;'><b>{$Description.MatPropertyID}</b></span>
                   </td> 
                 </tr>
                 <tr>
                   <td>
                     <textarea name='MatPropertyID' cols='40' rows='6' class='inputData'>{$smarty.post.MatPropertyID}</textarea>
                   </td> 
                 </tr>

                 <tr>
                   <td>
                    <span style='color:red;'>*</span>&nbsp;<span style='font-size:8pt;font-family:tahoma;color:#747464;'><b>{$Description.ShortDetailsID}</b></span>
                   </td> 
                 </tr>
                 <tr>
                   <td> 
                     <textarea id="ShortDetailsID" name='ShortDetailsID' cols='40' rows='4' class="{if $ErrorInput_ShortDetailsID}inputErrorData{else}inputDataMandatory{/if}"  onclick='this.className="inputDataMandatory"' onblur="ChangeBorder(this);">{$smarty.post.ShortDetailsID}</textarea>
                   </td> 
                 </tr>

               </table>

             </td>
          </tr>
        </table>
      </td>
    </tr> 
  </table>


  <br>

  <table width='80%' align='center' bgcolor='#EDEDCC' style='border-top: 1px; border-bottom: 1px; border-left:1px;  border-right:1px;  border-color:#C1C1A4;  border-style: solid;' cellpadding="0" cellspacing="0">

    <tr bgcolor='#E1E1C1' align='left'>
      <td  valign='top' align='left' colspan=2 style='padding-left:5px;padding-top:3px;padding-bottom:3px;'><span style='font-size:10pt;font-family:tahoma;color:#747464'><b>������ �� YouTube �����</b></span></td>
    </tr>

    <tr>
       <td style='padding-left:5px;'>
          <input type='text' size=50  name="youtubeURL" value="{$smarty.post.youtubeURL}" maxlength='150'  class='inputData'>
       </td> 
    </tr>

  </table>

<br>

  <table width='80%' align='center' bgcolor='#EDEDCC' style='border-top: 1px; border-bottom: 1px; border-left:1px;  border-right:1px;  border-color:#C1C1A4;  border-style: solid;' cellpadding="0" cellspacing="0">


    <tr bgcolor='#E1E1C1' align='left'>
      <td  valign='top' align='left' colspan=2 style='padding-left:5px;padding-top:3px;padding-bottom:3px;'><span style='font-size:10pt;font-family:tahoma;color:#747464'><b>����������</b></span></td>
    </tr>


    <tr>
       <td style='padding-left:5px;'>
          <span style='font-size:8pt;font-family:tahoma;color:#747464;'><b>������ ����� ������ ���� �� ����� 1 �� � ����� ���������� (gif, jpg, png, jpeg)</b></span>
       </td> 
    </tr>
    <tr>
       <td style='padding-left:5px;'>
          <input type='file' size=70 name="ImgID" class='inputData'>
       </td> 
    </tr>
    <tr>
       <td style='padding-left:5px;'>
          <input type='file' size=70 name="Img1ID" class='inputData'>
       </td> 
    </tr> 
    <tr>
       <td style='padding-left:5px;'>
          <input type='file' size=70 name="Img2ID" class='inputData'>
       </td> 
    </tr>


    <tr>
      <td  valign='top' align='left' colspan=2><span style='font-size:8pt;font-family:verdana;'>&nbsp;</span></td>
    </tr>


    <tr>
      <td colspan=2 align='right' style='padding-right:5px;padding-bottom:5px;'><input type='submit' value='��������� ������' class='inputData' style='cursor:hand;'></td>
    </tr>

    </table>
    
    <br>

    </td>
   </tr>
     
  </table>


       <center>
         <span style='font-size:12px;font-family:arial;' align=center>
         </span>
       </center>   

</form>
{/if}

    <!--table width='100%' align='center' bgcolor='#EDEDCC' style='border-top: 1px; border-bottom: 1px; border-left:1px;  border-right:1px;  border-color:#C1C1A4;  border-style: solid;' cellpadding="0" cellspacing="0">
    
    <tr>
      <td colspan=2>&nbsp;</td> 
    </tr> 

    <tr>
      <td width='60%' valign='top' style='padding-left:5pt;padding-right:5pt;'>
         <span style='font-size:12px;font-family:arial;'>
         <i>�������������� ���������� ���������� � ������� ������� � ������� "������� �������" �������������� �� ������� ������.
         <br>������ ����� ����������� ����� <a href='http://money.yandex.ru/' style='font-size:10pt;text-decoration:underline;'>������.������</a> �� ����� ����� <b>4100183416641</b>
         </i> 
         </span>
         <table>
           <tr>
             <td width=100%  align=left style='padding-left:5pt;'>
             <span style='font-size:12px;font-family:arial;'>������� �������</span><br>
             <span style='font-size:12px;font-family:arial;'>E-mail: </span><a title='������� ' href='mailto:staff@bizzona.ru' style='font-size:10pt;text-decoration:underline;'>staff@bizzona.ru</a><br>
             <span style='font-size:12px;font-family:arial;'>ICQ: </span><NOBR><A title='������ ������ �� ICQ' href='http://web.icq.com/whitepages/message_me/1,,,00.icq?uin=107242744&amp;action=message' style='font-size:10pt;text-decoration:underline;'><IMG height=18 alt='' src='http://web.icq.com/whitepages/online/1,,,00.gif?icq=107242744&amp;img=5&amp;online=' width=18 align=middle border=0></A>&nbsp;<A class=icq title='������ ������ �� ICQ' href='http://web.icq.com/whitepages/message_me/1,,,00.icq?uin=107242744&amp;action=message'>�������&nbsp;�������</A></NOBR>
             
             </td>
           </tr>
         </table>
      </td>
      <td  width='40%' valign='top'>
      <table width='50%'  style='border-top: 2px; border-bottom: 2px; border-left:2px;  border-right:2px;  border-color:#A6A69A;  border-style: solid;background-color:white;'>
         <tr><th colspan=2 align='left'><span style='font-size:12px;font-family:arial;'>���������</span></th></tr>
         <tr>
            <td><span style='font-size:12px;font-family:arial;'>1 ���.</span></td>
            <td><span style='font-size:12px;font-family:arial;'>350 �</span></td>
        </tr>
        <tr>
            <td><span style='font-size:12px;font-family:arial;'>2 ���.</span></td>
            <td><span style='font-size:12px;font-family:arial;'>450 �</span></td>
        </tr>
        <tr>
            <td><span style='font-size:12px;font-family:arial;'>3 ���.</span></td>
            <td><span style='font-size:12px;font-family:arial;'>500 �</span></td>
        </tr>
        
        </table>
      </td>
    </tr>

   </table-->

{literal}
<script language="javascript">

	try
	{
		var obj_FML = document.getElementById("FML");
		ChangeBorder(obj_FML);

		var obj_PhoneID = document.getElementById("PhoneID");
		ChangeBorder(obj_PhoneID);
		
		var obj_EmailID = document.getElementById("EmailID");
		ChangeBorder(obj_EmailID);
		
		var obj_CostID = document.getElementById("CostID");
		ChangeBorder(obj_CostID);
		
		var obj_NameBizID = document.getElementById("NameBizID");
		ChangeBorder(obj_NameBizID);
		
		var obj_SiteID = document.getElementById("SiteID");
		ChangeBorder(obj_SiteID);
		
		var obj_DetailsID = document.getElementById("DetailsID");
		ChangeBorder(obj_DetailsID);
		
		var obj_ShortDetailsID = document.getElementById("ShortDetailsID");
		ChangeBorder(obj_ShortDetailsID);
		
	}
	catch(e)
	{
		
	}
</script>
{/literal}
