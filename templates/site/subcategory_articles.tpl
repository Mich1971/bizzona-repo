<div style="background-color:#eeeee0;padding:7pt;">
{foreach name=sell key=key item=item from=$subcategory_article}

<table class="w"  cellpadding="0" cellspacing="0" border="0">
   <tr>
      <td class="nb_h1_gif" style="background-image:url({$smarty.const.NAMESERVER}images/{if $key is div by 2}nd_g_lt.gif{else}nd_w_lt.gif{/if});"></td>
      <td bgcolor="{if $key is div by 2}#e4e4e2{else}#f6f6f5{/if}" class="nb_10_12" style="background-image:url({$smarty.const.NAMESERVER}images/nd_b_10_12.gif);" width="99%"></td>
      <td class="nb_h2_gif" style="background-image:url({$smarty.const.NAMESERVER}images/{if $key is div by 2}nd_g_rt.gif{else}nd_w_rt.gif{/if});"></td>
   </tr>
   <tr>
   	<td class="nb_border_l_gif" style="background-image:url({$smarty.const.NAMESERVER}images/{if $key is div by 2}nd_border_10.gif{else}nd_border_w_10.gif{/if});"></td>
   	<td bgcolor="{if $key is div by 2}#e4e4e2{else}#f6f6f5{/if}">
   		<table border="0" class="w">
        	<tr> 
            	<td colspan="2"><div id="a_nm_h_none"><a href="{$item->link}" class="a_nm_h_none">{$item->title}</a></div></td>
            </tr>
            <tr>
            	<td colspan="2"><div class="sd">{$item->shortdescription|truncate:250}</div></td>
            </tr>
            <tr>
                <td align="right" colspan="2"><a href="{$item->link}" class="hld" title="{$item->title}">Подробнее</a> <span class="hld">>></span></td>
            </tr>
   		</table>
   	</td>
   	<td class="nb_border_r_gif" style="background-image:url({$smarty.const.NAMESERVER}images/{if $key is div by 2}nd_border_10.gif{else}nd_border_w_10.gif{/if});"></td>
   </tr>
   <tr>
      <td class="nb_h4_gif" style="background-image:url({$smarty.const.NAMESERVER}images/{if $key is div by 2}nd_g_lb.gif{else}nd_w_lb.gif{/if});"></td>
      <td bgcolor="{if $key is div by 2}#e4e4e2{else}#f6f6f5{/if}" class="nb_10_12" style="background-image:url({$smarty.const.NAMESERVER}images/nd_b_10_12.gif);" width="99%"></td>
      <td class="nb_h3_gif" style="background-image:url({$smarty.const.NAMESERVER}images/{if $key is div by 2}nd_g_rb.gif{else}nd_w_rb.gif{/if});"></td>
   </tr>
</table>
<div style="padding-top:4pt;"></div>
{/foreach}
</div>           