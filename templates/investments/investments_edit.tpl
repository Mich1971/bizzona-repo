{literal}
<!-- Place inside the <head> of your HTML -->
<script type="text/javascript" src="http://{/literal}{$smarty.server.SERVER_NAME}{literal}/engine/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
tinymce.init({
    selector: "textarea.editme", 
    theme: "modern",
    language : 'ru',
    height : 300, 
    plugins: [
         "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
         "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
         "save table contextmenu directionality emoticons template paste textcolor"
   ],
   content_css: "css/content.css",
   toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | l      ink image | print preview media fullpage | forecolor backcolor emoticons", 
   style_formats: [
        {title: 'Bold text', inline: 'b'},
        {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
        {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
        {title: 'Example 1', inline: 'span', classes: 'example1'},
        {title: 'Example 2', inline: 'span', classes: 'example2'},
        {title: 'Table styles'},
        {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
    ]    
 });
</script>
{/literal}


<table width='100%' cellpadding="0" cellspacing="0" style='border-top: 1px; border-bottom: 2px; border-left:1px;  border-right:1px;  border-color:#006699;  border-style: solid;'>
  <tr style="background-color:darkgreen ">
  	<td  width='10%' align='left' nowrap><a href="http://{$smarty.server.SERVER_NAME}/inside/" style="color:white;font-weight:bold;">�������</a></td>
  	<td  width='10%' align='left' nowrap><a href="http://{$smarty.server.SERVER_NAME}{$smarty.server.SCRIPT_NAME}" style="color:white;font-weight:bold;">����� ������</a></td>
  	<td width="80%">&nbsp;</td>
  </tr>
</table>
<form method="POST" style="margin:0pt;">
<table width='100%' align="center" bgcolor="#cccccc">
	<tr>
     	<td align='left' bgcolor="#027DBA">
          <a href="../detailsInvestments.php?ID={$data->id}&nocount=nocount" target="_blank" style='color:white;'>����������� �������� �������</a>
     	</td> 
    	<td bgcolor="#dfdbdb" align='right' style='font-size:9pt;color:green;' colspan="2">
			<a href="./template.php?typeeventobjectId=3&objectId={$data->id}&repeat=1"><img src='../images/trueNotifyPublic.gif' border=0></a>                  		 
    	</td>     	
	</tr>
	
	<tr {if strlen($text_error) > 0 }{else}style="display:none;"{/if}>
		<td colspan="2" align="center" class="desc_td"><span style="font-size:10pt;font-family:arial;color:red;font-weight:bold;">{$text_error}</span></td>
	</tr>
	<tr {if strlen($text_success) > 0 }{else}style="display:none;"{/if}>
		<td colspan="2" align="center" class="desc_td"><span style="font-size:10pt;font-family:arial;color:green;font-weight:bold;">{$text_success}</span></td>
	</tr>
	<tr>
		<td style="padding-left:5pt;" width="30%">��������</td>
		<td bgcolor="#dfdbdb">
			<textarea rows="3" name="name" style="width:100%;">{$data->name}</textarea>
		</td>
	</tr>
	<tr>
		<td style="padding-left:5pt;">��������� ����� ����������</td>
		<td bgcolor="#dfdbdb" nowrap>
			<div><b>������:</b> {$data->costtxt}</div>
			<input type="text" name="cost" style="width:20%;" value="{$data->cost}">
            <select style='font-size:10pt;font-family:arial;width:75% ' name="ccost">
            	<option value=usd {if $data->ccost eq "usd"}selected{else}{/if}>USD (������ ���)
            	<option value=eur {if $data->ccost eq "eur"}selected{else}{/if}>EUR (����)
            	<option value=rub {if $data->ccost eq "rub"}selected{else}{/if}>RUB (���������� �����)
            </select>
		</td>
	</tr>
	<tr>
		<td style="padding-left:5pt;">��� �������</td>
		<td bgcolor="#dfdbdb">																										
        	<select name="category_id"  style="width:100%;">
            	<option value="0">������� ���������</option>
				{foreach name='category' item=item key=key from=$listCategory}
					<optgroup label="{$item->Name}" style="background-color:#EDEDCC;color:#000000;">
						{foreach name='subcategory' item=subitem key=subkey from=$listSubCategory}
							{if $item->ID eq $subitem->CategoryID}
								<option style="background-color:#ffffff;" value="{$item->ID}:{$subitem->ID}" {if $data->subCategory_id eq $subitem->ID}selected{/if} >{$subitem->Name}</option>
							{/if}
						{/foreach}
					</optgroup>
				{/foreach}
			</select>
		</td>
	</tr>
	<tr>
		<td style="padding-left:5pt;">������</td>
		<td bgcolor="#dfdbdb">
			<input type="text" name="region_text" style="width:100%;" value="{$data->region_text}">
         	<select name="region_id" style="width:100%;" onchange='{php}global $sDistrict; $sDistrict->printScript();{/php}'>
			<option value="0">������� �����</option>
           		{foreach name=city key=key item=item from=$listCity}
              		<option value="{$item->ID}" {if $data->region_id eq $item->ID}selected{/if}>{$item->ID} {$item->title}</option> 
           		{/foreach}
         	</select>
			<div id="subRegionDiv" style="padding-top:5pt;padding-bottom:5pt;">&#160;</div>         	
		</td>
	</tr>
	<tr>
		<td style="padding-left:5pt;">���� ����������� ����������</td>
		<td bgcolor="#dfdbdb">
			<input type="text" name="period" style="width:100%;" value="{$data->period}">
		</td>
	</tr>
	<tr>
		<td style="padding-left:5pt;">�����������</td>
		<td bgcolor="#dfdbdb">
			<input type="text" name="payback" style="width:100%;" value="{$data->payback}">
		</td>
	</tr> 
	<tr>
		<td style="padding-left:5pt;">������� ����������</td>
		<td bgcolor="#dfdbdb">
			<div><b>������:</b> {$data->yearprofittxt}</div>
			<input type="text" name="yearprofit" style="width:20%;" value="{$data->yearprofit}">
            <select style='font-size:10pt;font-family:arial;width:75%' name="cyearprofit">
            	<option value=usd {if $data->cyearprofit eq "usd"}selected{else}{/if}>USD (������ ���)
            	<option value=eur {if $data->cyearprofit eq "eur"}selected{else}{/if}>EUR (����)
            	<option value=rub {if $data->cyearprofit eq "rub"}selected{else}{/if}>RUB (���������� �����)
            </select>
		</td>
	</tr>
	<tr>
		<td style="padding-left:5pt;">����� ������������� ����������</td>
		<td bgcolor="#dfdbdb" nowrap>
			<div><b>������:</b> {$data->totalinvestcosttxt}</div>
			<input type="text" name="totalinvestcost" style="width:20%;" value="{$data->totalinvestcost}">
            <select style='font-size:10pt;font-family:arial;width:75%' name="totalinvestccost">
            	<option value=usd {if $data->totalinvestccost eq "usd"}selected{else}{/if}>USD (������ ���)
            	<option value=eur {if $data->totalinvestccost eq "eur"}selected{else}{/if}>EUR (����)
            	<option value=rub {if $data->totalinvestccost eq "rub"}selected{else}{/if}>RUB (���������� �����)
            </select>
		</td>
	</tr>
	<tr>
		<td style="padding-left:5pt;">������ ����������</td>
		<td bgcolor="#dfdbdb">
			<textarea rows="4" name="stage" style="width:100%;">{$data->stage}</textarea>		
		</td>
	</tr>
	<tr>
		<td style="padding-left:5pt;">������������ ���������</td>
		<td bgcolor="#dfdbdb">
			<textarea rows="4" name="attractedPerson" style="width:100%;">{$data->attractedPerson}</textarea>		
		</td>
	</tr>
	
	<tr>
		<td style="padding-left:5pt;">��������� � �������</td>
		<td bgcolor="#dfdbdb">
			<textarea rows="4" name="documents" style="width:100%;">{$data->documents}</textarea>		
		</td>
	</tr> 
	<tr>
		<td style="padding-left:5pt;" valign="top">
                    ������� ��������
                    <div>     
                      {include file="./textreplace.tpl" type="shortDescription"} 
                    </div>                
                </td>
		<td bgcolor="#dfdbdb">
     	 			<textarea name="shortDescription" class='editme'>
      	  				{$data->shortDescription}
      	 			</textarea> 
		</td>
	</tr>
	<tr>
		<td style="padding-left:5pt;">���������� ����</td>
		<td bgcolor="#dfdbdb">
			<input type="text" name="contactface" style="width:100%;" value="{$data->contactface}">
		</td>
	</tr>
	<tr>
		<td style="padding-left:5pt;">��������</td>
		<td bgcolor="#dfdbdb">
			<input type="text" name="contactphone" style="width:100%;" value="{$data->contactphone}">
		</td>
	</tr>
	<tr>
		<td style="padding-left:5pt;">����</td>
		<td bgcolor="#dfdbdb">
			<input type="text" name="contactfax" style="width:100%;" value="{$data->contactfax}">
		</td>
	</tr>
	<tr>
		<td style="padding-left:5pt;">Email</td>
		<td bgcolor="#dfdbdb">
			<input type="text" name="contactemail" style="width:100%;" value="{$data->contactemail}">
		</td>
	</tr>
	
    <tr>
		<td style="padding-left:5pt;">������������ ����������</td>
    	<td bgcolor="#dfdbdb">
        	<select name='period_id' style="width:100%;">
            	<option value='1' {if $data->period_id eq 1}selected{/if} >1 ���.</option>
                <option value='2' {if $data->period_id eq 2}selected{/if} >2 ���.</option>
                <option value='3' {if $data->period_id eq 3}selected{/if} >3 ���.</option>
            </select>
        </td> 
    </tr>

	<tr>
		<td style="padding-left:5pt;"><div style="font-size:10pt;font-family:arial;">������</div></td>
		<td bgcolor="#dfdbdb">
			<select name="status_id"  style="width:100%;">
				<option value="{$smarty.const.NEW_INVESTMENTS}" {if $data->status_id eq $smarty.const.NEW_INVESTMENTS}selected{else}{/if}>�����</option>
				<option value="{$smarty.const.OPEN_INVESTMENTS}" {if $data->status_id eq $smarty.const.OPEN_INVESTMENTS}selected{else}{/if}>������</option>
				<option value="{$smarty.const.CLOSE_INVESTMENTS}" {if $data->status_id eq $smarty.const.CLOSE_INVESTMENTS}selected{else}{/if}>������</option>
				<option value="{$smarty.const.LOOKED_INVESTMENTS}" {if $data->status_id eq $smarty.const.LOOKED_INVESTMENTS}selected{else}{/if}>����������</option>
				<option value="{$smarty.const.DUBLICATE_INVESTMENTS}" {if $data->status_id eq $smarty.const.DUBLICATE_INVESTMENTS}selected{else}{/if}>��������</option>
				<option value="{$smarty.const.DUST_INVESTMENTS}" {if $data->status_id eq $smarty.const.DUST_INVESTMENTS}selected{else}{/if}>�����</option>
			</select>
		</td>
	</tr>
    
	<tr>
		<td colspan="2" bgcolor="#dfdbdb" align="right"><input type="submit" value="���������"></td>
	</tr>
	
</table>

</form>

