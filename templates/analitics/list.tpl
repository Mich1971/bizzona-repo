{literal}
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script type="text/javascript">
    google.load("visualization", "1", {packages:["corechart"]});
    google.setOnLoadCallback(drawChart);
    function drawChart() {
        var data_statregionpayment = google.visualization.arrayToDataTable([
            ['% �����', '������/�������'],
            {/literal}
            {foreach from=$statregionpayment item=i key=k}
                ['{$i.name}',  {$i.procentage}],
            {/foreach}
            {literal}

        ]);


        
        
        var option_statregionpayment = {
            title: '��� �������� �� �������/�������� �� ������� ���',
            hAxis: {title: '% �����', titleTextStyle: {color: 'green'}},
            backgroundColor: 'honeydew',
            is3D: true,
        };

        
        var chart_statregionpayment = new google.visualization.BarChart(document.getElementById('statregionpayment_div'));
        chart_statregionpayment.draw(data_statregionpayment, option_statregionpayment);

        var chart_statregionpayment_area = new google.visualization.PieChart(document.getElementById('statregionpayment_div_area'));
        chart_statregionpayment_area.draw(data_statregionpayment, option_statregionpayment);


    }
</script>
{/literal}

 <div id="statregionpayment_div" style="width: 95%; height: 500px;" align="center"></div>
 <div id="statregionpayment_div_area" style="width: 95%; height: 500px;" align="center"></div>
 