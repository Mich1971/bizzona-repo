{literal}
<!-- Place inside the <head> of your HTML -->
<script type="text/javascript" src="http://{$smarty.server.SERVER_NAME}/engine/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
tinymce.init({
    selector: "textarea.editme", 
    theme: "modern",
    language : 'ru',
    height : 300, 
    plugins: [
         "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
         "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
         "save table contextmenu directionality emoticons template paste textcolor"
   ],
   content_css: "css/content.css",
   toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | l      ink image | print preview media fullpage | forecolor backcolor emoticons", 
   style_formats: [
        {title: 'Bold text', inline: 'b'},
        {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
        {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
        {title: 'Example 1', inline: 'span', classes: 'example1'},
        {title: 'Example 2', inline: 'span', classes: 'example2'},
        {title: 'Table styles'},
        {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
    ]    
 });
</script>
{/literal}




{if isset($item)}
<table width="100%">
<tr>
<td align="left" width="50%"><h3>����� ��� �������������� ����� - � {$item.id}</h3></td>
<td align="right"><a href="http://{$smarty.server.SERVER_NAME}/engine/AdsAction.php" style="font-size:15px;font-weight:bold;background-color:#e2f6ee">������� ����� �����</a></td>
</table>
{else}
<h3>����� ��� ���������� ����� �����</h3>
{/if}



<h3></h3> 
<form method="post" style="margin-top:0pt;margin-bottom:0pt;" enctype="multipart/form-data">

<table  width='100%' bgcolor='#cccccc'>
	<tr>
		<td width="15%" align="left"  style="padding:15px;">������������:</td>
		<td align="left"  bgcolor="#dfdbdb" align="center"  style="padding:15px;">
			<textarea name="subject" rows="3" style="width:95%;">{if isset($item)}{$item.subject}{/if}</textarea>
		</td> 
        </tr>
        <tr>
		<td width="15%" align="left" style="padding:15px;">��������:</td>
		<td bgcolor="#dfdbdb" align="center" style="padding:15px;">
			<textarea name="body" rows="15" class='editme' style="width:95%;">{if isset($item)}{$item.body}{/if}</textarea>
		</td>
	</tr>
	<tr>
		<td colspan="2" align="right" bgcolor="#dfdbdb" style="padding:15px;">
			<input type="submit" value="���������">
		</td>
	</tr>
</table>
{if isset($item)}
	<input type="hidden" name="update" value="update">
{else}
	<input type="hidden" name="add" value="add">
{/if}

</form>