{include file='./error/sendevent.tpl'}
{include file='./error/error.tpl'}

{literal}
<!-- Place inside the <head> of your HTML -->
<script type="text/javascript" src="http://{/literal}{$smarty.server.SERVER_NAME}{literal}/engine/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
tinymce.init({
    selector: "textarea.editme", 
    theme: "modern",
    language : 'ru',
    height : 300, 
    plugins: [
         "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
         "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
         "save table contextmenu directionality emoticons template paste textcolor"
   ],
   content_css: "css/content.css",
   toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | l      ink image | print preview media fullpage | forecolor backcolor emoticons", 
   style_formats: [
        {title: 'Bold text', inline: 'b'},
        {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
        {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
        {title: 'Example 1', inline: 'span', classes: 'example1'},
        {title: 'Example 2', inline: 'span', classes: 'example2'},
        {title: 'Table styles'},
        {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
    ]    
 });
 
 function ShowHideBlockMessage(Id) {
    var block = document.getElementById(Id);
    block.style.display = (block.style.display == 'none') ? '' : 'none';
 }
 
</script>
{/literal}

{if $smarty.get.ID}

<form method="post" style="margin-top:0pt;margin-bottom:0pt;" id="updatesellformID" enctype="multipart/form-data">
  <input type='hidden' name='update' value='update'>
  <input type='hidden' name='ID' value='{$smarty.get.ID}'>
  <table width='100%' bgcolor='#cccccc'>

    <tr>
        <td align='center' bgcolor="#bdd9bd">
        {if $data->typeSellID == 2}<span style="color:yellow;font-weight:bold;">&nbsp;������&nbsp;</span>{else}{/if}
         <a href="../detailsSell.php?ID={$data->ID}&nocount=nocount" target="_blank" style='color:black;'>����������� �������� �������</a>
        </td> 
        
    	<td bgcolor="#ecf8ec" align='right'>
    		<table width="100%">
    			<tr> 
    				<td width="15%">�����: </td>
    				<td>{$data->login}</td>
    				<td width="15%" nowrap>���� ��������:</td>
    				<td>{$data->date}</td>
    				<td width="3%"></td>
                                <td rowspan="2" valign="top">

                                <a href="#" onclick ='ShowHideBlockMessage("div{$item->ID}")' style='color:black;'>��������� ����������</a>                                     

                                <div id="div{$item->ID}" style='margin:8px;border:3px #c3c3c3 solid;display:none;background-color:white;padding:4px;' align="left" >
                                    <ul>
                                        <li><a href="./UpdateSell.php?ID={$data->ID}&typeEventId=11&codeId={$data->ID}" ><img src="http://{$smarty.server.SERVER_NAME}/sicon/restore.png" title="�������������� ������ � ������" border='0' style='padding-right:10px;'>������� ����� � ������</a> 
                                        <li><a href="./UpdateSell.php?ID={$data->ID}&typeEventId=1&codeId={$data->ID}" ><img src="http://{$smarty.server.SERVER_NAME}/sicon/supply.jpg" title="�����������" border='0' style='padding-right:10px;'>�����������</a> 
                                        <li><a href="./UpdateSell.php?ID={$data->ID}&typeEventId=3&codeId={$data->ID}" ><img src="http://{$smarty.server.SERVER_NAME}/sicon/o25.png"  title="������������" border='0' style='padding-right:10px;'>������������</a>  
                                        <li><a href="./UpdateSell.php?ID={$data->ID}&typeEventId=4&codeId={$data->ID}" ><img src="http://{$smarty.server.SERVER_NAME}/sicon/cl25.png" title="�������" border='0' style='padding-right:10px;'>�������</a>      
                                    </ul>
                               </div>                                     
                                    
                                </td>
    			</tr>
    			<tr>
    				<td>������: </td>
    				<td>{$data->password}</td>
    				<td>������� ����:</td>
    				<td>{$data->DataCreate}</td>
    				<td></td>
    			</tr>
                        <tr>
                            <td colspan='6' align="right" bgcolor="#bdd9bd">
                                <a href="http://{$smarty.server.SERVER_NAME}/cabinet/index.php?auth=1&loginID={$data->login}&passwordID={$data->password}" target="_blank" style="color:#000;">����� ������������� � �������</a>
                            </td>
                        </tr>    
    		</table>
    	</td>
    </tr>

    <tr style='background-color:#5da4ed;color:white;margin:2px;'>
      <td width='30%' align='right' valign='top'>�������� �����-�����</td>
      <td>
          <input type='checkbox' name="isPromo"  {if $data->isPromo}checked{/if}>
      </td>
    </tr>
    
    
    <tr>
      <td width='30%' align='right' valign='top'>{$Description.FML}<span style='color:red;'>*</span></td>
      <td bgcolor="#dfdbdb">
         <input type='text' size=50 name="FML" value="{$data->FML}">
      </td>
    </tr>

    <tr>
      <td width='30%' align='right' valign='top'>{$Description.Agent}</td>
      <td bgcolor="#dfdbdb">
         <input type='checkbox' name="Agent"  {if $data->Agent}checked{/if}>
         ������������� ������: {$data->brokerObjectID} {if strlen($data->urlBrokerObject) > 0 } => {$data->urlBrokerObject}{/if}
         &nbsp;&nbsp;<input type='checkbox' name="helpbroker"  {if $data->helpbroker}checked{/if}>������ �������
         
      </td>
    </tr>



    <tr>
      <td  align='right' valign='top'>{$Description.EmailID}<span style='color:red;'>*</span></td>
      <td bgcolor="#dfdbdb">
         <input type='text' size=50 name="EmailID" value="{$data->EmailID}">
      </td>
    </tr>

    <tr>
      <td  align='right' valign='top'>{$Description.Email2ID}<span style='color:red;'></span></td>
      <td bgcolor="#dfdbdb">
         <input type='text' size=50 name="Email2ID" value="{$data->Email2ID}">
      </td>
    </tr>


    <tr>
      <td  align='right' valign='top'>������</td>
      <td bgcolor="#dfdbdb">
      	<table>
      		<tr>
      			<td valign="top">
         			<select name='IconID' ID="IconID"  onchange='{php}global $dIcon; $dIcon->printScript();{/php}'>
          				{foreach  key=k item=i from=$icon}
            			<option value="{$i}" {if $data->Icon eq $i}selected{/if}>{$i}</option>   
          				{/foreach}
         			</select> 
         			<br>
         			<input type="file" name="defaultUserIconFile">(172x116)
         			<table>
         			{if strlen($data->ImgID) > 0}
         			<tr>
         			<td>
         			<span style="color:green;">���������� ���� (1): http://{$smarty.server.SERVER_NAME}/simg/{$data->ImgID}</span>
         			</td>
         			</tr>
         			{else}
         			{/if}
         			{if strlen($data->Img1ID) > 0}
         			<tr>
         			<td>
         			<span style="color:green;">���������� ���� (2): http://{$smarty.server.SERVER_NAME}/simg/{$data->Img1ID}</span>
         			</td>
         			</tr>
         			{else}
         			{/if}
         			{if strlen($data->Img2ID) > 0}
         			<tr>
         			<td>
         			<span style="color:green;">���������� ���� (3): http://{$smarty.server.SERVER_NAME}/simg/{$data->Img2ID}</span>
         			</td>
         			</tr>
         			{else}
         			{/if} 
         			{if strlen($data->Img3ID) > 0}
         			<tr>
         			<td>
         			<span style="color:green;">���������� ���� (4): http://{$smarty.server.SERVER_NAME}/simg/{$data->Img3ID}</span>
         			</td>
         			</tr>
         			{else}
         			{/if} 
         			{if strlen($data->Img4ID) > 0}
         			<tr>
         			<td>
         			<span style="color:green;">���������� ���� (5): http://{$smarty.server.SERVER_NAME}/simg/{$data->Img4ID}</span>
         			</td>
         			</tr>
         			{else}
         			{/if}         			        			        			         			
         			</table>
         		</td>
         		<td>
		 			<div id="iconDiv">&#160;</div>
		 		</td>
		 	</tr>
		</table>		 
      </td>
    </tr>

    <tr>
    	<td  align='right' valign='top'>������ "������� �������"</td>
    	<td bgcolor="#dfdbdb">
    		<input type="file" name="hotimg">(100x85)
    	</td>
    </tr>


    <tr>
      <td  align='right' valign='top'>{$Description.PhoneID}<span style='color:red;'>*</span></td>
      <td bgcolor="#dfdbdb">
         <input type='text' size=50 name="PhoneID" value="{$data->PhoneID}">
         <span style="color:green;">(������ �������� ������� � ������� �����)</span>
      </td>
    </tr>

    <tr>
      <td  align='right' valign='top'>{$Description.FaxID}</td>
      <td bgcolor="#dfdbdb">
         <input type='text'  size=50 name="FaxID" value="{$data->FaxID}">
      </td>
    </tr>


    <tr>
      <td  align='right' valign='top'>
          {$Description.NameBizID}<span style='color:red;'>*</span>
            <div>       
              {include file="./textreplace.tpl" type="NameBizID"} 
            </div>           
      </td>
      <td bgcolor="#dfdbdb">
         <textarea name='NameBizID' cols='80' rows='3'>{$data->NameBizID}</textarea>
         <div>��������� ����������� (������: ������� ������ �������.)</div>
         <textarea name='subtitle' cols='80' rows='2'>{$data->subtitle}</textarea>
      </td>
    </tr>
    
    

    <tr>
      <td  align='right' valign='top'>{$Description.CategoryID}</td>
      <td bgcolor="#dfdbdb">
      	<table>
      		<tr>
      			<td valign="top">
        {if $data->SSubCategoryID > 0}
        	
          <select name="CategoryID" ID="CategoryIconID" onchange='{php}global $dCategoryIcon; $dCategoryIcon->printScript();{/php}'>
			<option value="0">������� ���������</option>
             {foreach name='category' item=item key=key from=$listCategory}
               <optgroup label="{$item->Name}" >
               {foreach name='subcategory' item=subitem key=subkey from=$listSubCategory}
             	  {if $item->ID eq $subitem->CategoryID}
             	  <option value="{$item->ID}:{$subitem->ID}" {if $data->CategoryID eq $item->ID and $subitem->ID eq $data->SSubCategoryID}selected{/if} >{$subitem->Name}</option>
             	  {/if}
               {/foreach}
               </optgroup>
             {/foreach}
           </select>
        {else}
          <select name="CategoryID"  ID="CategoryIconID" onchange='{php}global $dCategoryIcon; $dCategoryIcon->printScript();{/php}'>
          	 <option value="0">������� ���������</option>
             {foreach name='category' item=item key=key from=$listCategory}
               <optgroup label="{$item->Name}" >
               {foreach name='subcategory' item=subitem key=subkey from=$listSubCategory}
             	  {if $item->ID eq $subitem->CategoryID}
             	  <option value="{$item->ID}:{$subitem->ID}" {if $data->CategoryID eq $item->ID and $subitem->enabled eq '1'}selected{/if} >{$subitem->Name}</option>
             	  {/if}
               {/foreach}
               </optgroup>
             {/foreach}
           </select>
         {/if}
                       <br> <br> <br> 
                       <input type="checkbox" {if $data->frannchising eq 1}checked{/if} name="frannchising"> ��������        
         
         		</td>
         		<td>
         			<div ID="dCategoryIconID">&nbsp;</div>
         		</td>
         	</tr>
         </table>
      </td>
    </tr>

    <tr>
      <td  align='right' valign='top'>{$Description.BizFormID}</td>
      <td bgcolor="#dfdbdb">
         <select name="BizFormID">
           <option value='1' {if $data->BizFormID eq 1}selected{/if} >�������� � ������������ ����������������</option>
           <option value='2' {if $data->BizFormID eq 2}selected{/if}>�������� ����������� ��������</option>
           <option value='3' {if $data->BizFormID eq 3}selected{/if}>�������� ����������� ��������</option> 
           <option value='4' {if $data->BizFormID eq 4}selected{/if}>���������������� ����������</option>
           <option value='5' {if $data->BizFormID eq 5}selected{/if}>���������� ���������</option>
           <option value='6' {if $data->BizFormID eq 6}selected{/if}>����</option>
           <option value='7' {if $data->BizFormID eq 7}selected{/if}>�������������� �����������</option> 
           <option value='8' {if $data->BizFormID eq 8}selected{/if}>��</option>
           <option value='0' {if $data->BizFormID eq 0}selected{/if}>������</option>
         </select>
      </td>
    </tr>

    <tr>
      <td  align='right' valign='top'>{$Description.CityID}</td>
      <td bgcolor="#dfdbdb">
      	<table>
      		<tr>
      			<td>
         			<select name="CityID" ID="CityID" onchange='{php}global $sDistrict; $sDistrict->printScript();{/php}'>
         				<option value="0">������� �����</option>
           				{foreach name=city key=key item=item from=$listCity}
              			<option value="{$item->ID}" {if $data->CityID eq $item->ID}selected{/if} >{$item->ID} {$item->title} -> {$item->parent}</option>
           				{/foreach}
					</select> 
					<div id="subRegionDiv" style="padding-top:5pt;padding-bottom:5pt;">&#160;</div>
           		</td>
         		<td>
         			<div id="districtDiv" style="padding-top:5pt;padding-bottom:5pt;">&#160;</div>
         			<div id="metroDiv">&#160;</div>
         		</td>
         	</tr>
         </table>
      </td>
    </tr>

    <tr>
      <td  align='right' valign='top'>{$Description.SiteID}</td>
      <td bgcolor="#dfdbdb">
                        <div style="color:green;"> ������: �. ������, ��. ����� �����, �. 8 (���������� ����������� �������) </div>
			<textarea name='SiteID' cols='40' rows='5'>{$data->SiteID}</textarea>
			<div><i>������ ������ ��: {$whois_city}, {$whois_region}</i></div>
			<div id="streetsDiv">&#160;</div>
      </td>
    </tr>


    <tr>
      <td  align='right' valign='top'>{$Description.CostID}<span style='color:red;'>*</span></td>
      <td bgcolor="#dfdbdb">
      ������: {$data->txtCostID}<br>
         <input type='text' size=50 name="CostID" value="{$data->CostID}">
        <select name="cCostID"><option value=usd  {if $data->cCostID eq "usd"}selected{/if} >USD (������ ���)<option value=eur  {if $data->cCostID eq "eur"}selected{/if} >EUR (����)<option value=rub {if $data->cCostID eq "rub"}selected{/if}>RUB (���������� �����)<option value=aud  {if $data->cCostID eq "aud"}selected{/if} >AUD (������ ��)</select>
        <br>
        �������� ����: <input type='checkbox' name="torgStatusID"  {if $data->torgStatusID}checked{/if}>
      </td>
    </tr>
 
    <tr>
      <td  align='right' valign='top'>{$Description.AgreementCostID}</td>
      <td bgcolor="#dfdbdb">
         <input type='checkbox' name="AgreementCostID"  {if $data->AgreementCostID}checked{/if}>
      </td>
    </tr>


    <tr>
      <td  align='right' valign='top'>{$Description.ProfitPerMonthID}</td>
      <td bgcolor="#dfdbdb">
      	������: {$data->txtProfitPerMonthID}<br>
         <input type='text'  size=50 name="ProfitPerMonthID" value="{$data->ProfitPerMonthID}">
         <select name="cProfitPerMonthID"><option value=usd {if $data->cProfitPerMonthID eq "usd"}selected{/if} >USD (������ ���)<option value=eur  {if $data->cProfitPerMonthID eq "eur"}selected{/if} >EUR (����)<option value=rub {if $data->cProfitPerMonthID eq "rub"}selected{/if}>RUB (���������� �����)<option value=aud  {if $data->cCostID eq "aud"}selected{/if} >AUD (������ ��)</select>
      </td>
    </tr>

    <tr>
      <td  align='right' valign='top'>{$Description.MaxProfitPerMonthID}</td>
      <td bgcolor="#dfdbdb">
         <input type='text'  size=50 name="MaxProfitPerMonthID" value="{$data->MaxProfitPerMonthID}">
         <select name="cMaxProfitPerMonthID"><option value=usd {if $data->cMaxProfitPerMonthID eq "usd"}selected{/if} >USD (������ ���)<option value=eur  {if $data->cMaxProfitPerMonthID eq "eur"}selected{/if} >EUR (����)<option value=rub {if $data->cMaxProfitPerMonthID eq "rub"}selected{/if}>RUB (���������� �����)<option value=aud  {if $data->cCostID eq "aud"}selected{/if} >AUD (������ ��)</select>
      </td>
    </tr>



    <tr>
      <td align='right' valign='top'>{$Description.TimeInvestID}</td>
      <td bgcolor="#dfdbdb">
         <input type='text'  size=50 name="TimeInvestID" value="{$data->TimeInvestID}">
      </td>
    </tr>
 
    <tr>
      <td  align='right' valign='top'>
          {$Description.ListObjectID}
          <div>   
              {include file="./textreplace.tpl" type="ListObjectID"} 
          </div>           
      </td>
      <td bgcolor="#dfdbdb">
      	 <textarea name="ListObjectID" class='editme'>
      	  {$badWords->DistSelect($data->ListObjectID)}
      	 </textarea> 
      </td>
    </tr>

    <tr>
      <td  align='right' valign='top'>{$Description.ContactID}</td>
      <td bgcolor="#dfdbdb">
         <textarea name='ContactID' cols='60' rows='8'>{$data->ContactID}</textarea>
      </td>
    </tr>

    <tr>
      <td align='right' valign='top'>
          {$Description.MatPropertyID}
          <div>  
              {include file="./textreplace.tpl" type="MatPropertyID"} 
          </div>           
      </td>
      <td align='left' bgcolor="#dfdbdb">
      	<textarea name="MatPropertyID" class='editme'>
      	 {$badWords->DistSelect($data->MatPropertyID)}
        </textarea>
      </td>
    </tr>


    <tr>
      <td align='right' valign='top'>{$Description.MonthAveTurnID}</td>
      <td bgcolor="#dfdbdb">
      	������: {$data->txtMonthAveTurnID}<br>
         <input type='text'  size=50 name="MonthAveTurnID" value="{$data->MonthAveTurnID}">
         <select name="cMonthAveTurnID"><option value=usd  {if $data->cMonthAveTurnID eq "usd"}selected{/if} >USD (������ ���)<option value=eur  {if $data->cMonthAveTurnID eq "eur"}selected{/if}>EUR (����)<option value=rub {if $data->cMonthAveTurnID eq "rub"}selected{/if}>RUB (���������� �����)<option value=aud  {if $data->cCostID eq "aud"}selected{/if} >AUD (������ ��)</select>
      </td>
    </tr>


    <tr>
      <td align='right' valign='top'>{$Description.MaxMonthAveTurnID}</td>
      <td bgcolor="#dfdbdb">
         <input type='text'  size=50 name="MaxMonthAveTurnID" value="{$data->MaxMonthAveTurnID}">
         <select name="cMaxMonthAveTurnID"><option value=usd  {if $data->cMaxMonthAveTurnID eq "usd"}selected{/if} >USD (������ ���)<option value=eur  {if $data->cMaxMonthAveTurnID eq "eur"}selected{/if}>EUR (����)<option value=rub {if $data->cMaxMonthAveTurnID eq "rub"}selected{/if}>RUB (���������� �����)<option value=aud  {if $data->cCostID eq "aud"}selected{/if} >AUD (������ ��)</select>
      </td>
    </tr>



    <tr>
      <td align='right' valign='top'>{$Description.MonthExpemseID}</td>
      <td bgcolor="#dfdbdb">
      	������: {$data->txtMonthExpemseID}<br>
         <input type='text'  size=50 name="MonthExpemseID" value="{$data->MonthExpemseID}">
         <select name="cMonthExpemseID"><option value=usd  {if $data->cMonthExpemseID eq "usd"}selected{/if} >USD (������ ���)<option value=eur {if $data->cMonthExpemseID eq "eur"}selected{/if} >EUR (����)<option value=rub {if $data->cMonthExpemseID eq "rub"}selected{/if} >RUB (���������� �����)<option value=aud  {if $data->cCostID eq "aud"}selected{/if} >AUD (������ ��)</select>
      </td>
    </tr>


    <tr>
      <td align='right' valign='top'>{$Description.SumDebtsID}</td>
      <td bgcolor="#dfdbdb">
      ������: {$data->txtSumDebtsID}<br>
         <input type='text'  size=50 name="SumDebtsID" value="{$data->SumDebtsID}">
         <select name="cSumDebtsID"><option value=usd {if $data->cSumDebtsID eq "usd"}selected{/if} >USD (������ ���)<option value=eur {if $data->cSumDebtsID eq "eur"}selected{/if}>EUR (����)<option value=rub {if $data->cSumDebtsID eq "rub"}selected{/if}>RUB (���������� �����)<option value=aud  {if $data->cCostID eq "aud"}selected{/if} >AUD (������ ��)</select>
      </td>
    </tr>


    <tr>
      <td align='right' valign='top'>{$Description.WageFundID}</td>
      <td bgcolor="#dfdbdb">
      ������: {$data->txtWageFundID}<br>
         <input type='text'  size=50 name="WageFundID"  value="{$data->WageFundID}">
         <select name="cWageFundID"><option value=usd {if $data->cWageFundID eq "usd"}selected{/if} >USD (������ ���)<option value=eur {if $data->cWageFundID eq "eur"}selected{/if}>EUR (����)<option value=rub {if $data->cWageFundID eq "rub"}selected{/if}>RUB (���������� �����)<option value=aud  {if $data->cCostID eq "aud"}selected{/if} >AUD (������ ��)</select>
      </td>
    </tr>

    <tr>
      <td align='right' valign='top'>�������� �����</td>
      <td bgcolor="#dfdbdb">
         <input type='text'  size=50 name="ArendaCostID"  value="{$data->ArendaCostID}">
         <select name="cArendaCostID"><option value=usd {if $data->cArendaCostID eq "usd"}selected{/if} >USD (������ ���)<option value=eur {if $data->cArendaCostID eq "eur"}selected{/if}>EUR (����)<option value=rub {if $data->cArendaCostID eq "rub"}selected{/if}>RUB (���������� �����)<option value=aud  {if $data->cCostID eq "aud"}selected{/if} >AUD (������ ��)</select>
         <input type="checkbox" name="ArendaCommAl" {if $data->ArendaCommAl == "1"}checked{/if}>�� ��������
      </td>
    </tr>    

    <tr>
      <td align='right' valign='top'>{$Description.ObligationID}</td>
      <td bgcolor="#dfdbdb">
         <textarea name='ObligationID' cols='60' rows='8'>{$data->ObligationID}</textarea>
      </td>
    </tr>


    <tr>
      <td align='right' valign='top'>{$Description.CountEmplID}</td>
      <td bgcolor="#dfdbdb">
         <input type='text'  size=50 name="CountEmplID" value="{$data->CountEmplID}">
      </td>
    </tr>

    <tr>
      <td align='right' valign='top'>{$Description.CountManEmplID}</td>
      <td bgcolor="#dfdbdb">
         <input type='text'  size=50 name="CountManEmplID" value="{$data->CountManEmplID}">
      </td>
    </tr>

    <tr>
      <td align='right' valign='top'>{$Description.TermBizID}</td>
      <td bgcolor="#dfdbdb">
         <input type='text'  size=50 name="TermBizID"  value="{$data->TermBizID}">
      </td>
    </tr>

    <tr>
      <td width='30%' align='right' valign='top'>�������� ������</td>
      <td bgcolor="#dfdbdb">
         <input type='checkbox' name="arendaStatusID"  {if $data->arendaStatusID}checked{/if}>
      </td>
    </tr>    
    
    <tr>
      <td align='right' valign='top'><span style='font-size:12px;font-family:verdana;'>������� ���������� ��� "�������"</span></td>
      <td bgcolor="#dfdbdb">
        <textarea name='HotShortDetailsID' cols='60' rows='4'>{$data->HotShortDetailsID}</textarea>
      </td>
    </tr>    
    
    <tr>
      <td align='right' valign='top'>
          <span style='font-size:12px;font-family:verdana;'>{$Description.ShortDetailsID}</span><span style='color:red;'>*</span>
          <div> 
              {include file="./textreplace.tpl" type="ShortDetailsID"} 
          </div>          
      </td>
      <td bgcolor="#dfdbdb">
      	<textarea name="ShortDetailsID" class='editme'>
      	 {$badWords->DistSelect($data->ShortDetailsID)}
        </textarea>
      </td>
    </tr>


    <tr>
      <td align='right' valign='top'>
          {$Description.DetailsID}<span style='color:red;'>*</span>
          <div> 
              {include file="./textreplace.tpl" type="DetailsID"} 
          </div>
      </td>
      <td bgcolor="#dfdbdb">
      	<textarea name="DetailsID" class='editme'>
      	 {$badWords->DistSelect($data->DetailsID)}
        </textarea>
      </td>
    </tr>

    <!--tr>
      <td>�����������</td>
      <td>
         <input type='file'  size=50 name="ImgID">
      </td>
    </tr-->


    <tr>
      <td align='right' valign='top'>{$Description.ShareSaleID}</td>
      <td bgcolor="#dfdbdb">
         ��:  <input type='text'  size=10 name="ShareSaleID"  value="{$data->ShareSaleID}">
         ��:  <input type='text'  size=10 name="maxShareSaleID"  value="{$data->maxShareSaleID}">
      </td>
    </tr>


    <tr>
      <td align='right' valign='top'>{$Description.ReasonSellBizID}</td>
      <td bgcolor="#dfdbdb">
      	{if strlen($data->txtReasonSellBizID) > 1}<div>{$data->txtReasonSellBizID}</div>{/if}
        <select name="ReasonSellBizID">
          <option value='1' {if $data->ReasonSellBizID eq 1}selected{/if} >������ ���������� � ����� �������</option>
          <option value='2' {if $data->ReasonSellBizID eq 2}selected{/if} >�������, ���������,���������� �������</option>
          <option value='3' {if $data->ReasonSellBizID eq 3}selected{/if} >������������, ���������� �������</option>
          <option value='4' {if $data->ReasonSellBizID eq 4}selected{/if} >���������� ��������� ������</option>
          <option value='5' {if $data->ReasonSellBizID eq 5}selected{/if} >�������� ������� ��� ��������</option>
          <option value='6' {if $data->ReasonSellBizID eq 6}selected{/if} >������� �� ������ ����� ����������</option> 
          <option value='7' {if $data->ReasonSellBizID eq 7}selected{/if} >������� ������������ �������</option>
          <option value='8' {if $data->ReasonSellBizID eq 8}selected{/if} >����������� ����� �����������</option> 
          <option value='9' {if $data->ReasonSellBizID eq 9}selected{/if} >�������� �������� �����������</option>
          <option value='10' {if $data->ReasonSellBizID eq 10}selected{/if} >������� ����������� � �������� ���������</option>
          <option value='11' {if $data->ReasonSellBizID eq 11}selected{/if} >C���� ���� ������������</option>
          <option value='12' {if $data->ReasonSellBizID eq 12}selected{/if} >�������������� � ������ ������</option>
          <option value='0' {if $data->ReasonSellBizID eq 0}selected{/if} >������</option>
        </select>
      </td>
    </tr>



    <tr>
      <td align='right' valign='top'>{$Description.TarifID}</td>
      <td bgcolor="#dfdbdb">
         <select name='TarifID'>
           <option value='1' {if $data->TarifID eq 1}selected{/if} >1 ���.</option>
           <option value='2' {if $data->TarifID eq 2}selected{/if} >2 ���.</option>
           <option value='3' {if $data->TarifID eq 3}selected{/if} >3 ���.</option>
           <option value='4' {if $data->TarifID eq 4}selected{/if} >4 ���.</option>
           <option value='5' {if $data->TarifID eq 5}selected{/if} >5 ���.</option>
           <option value='6' {if $data->TarifID eq 6}selected{/if} >5 ���.</option>
         </select>
      </td>
    </tr>

    <tr>
    	 <td align='right' valign='top'>��� ����������</td>
    	 <td>
         <select name='typeSellID'>
           <option value='1' {if $data->typeSellID eq 1}selected{/if} >������� �������</option>
           <option value='2' {if $data->typeSellID eq 2}selected{/if} >������ �������</option>
         </select>    	 
    	 </td>
    </tr>

    <tr>
      <td align='right' valign='top'>{$Description.StatusID}</td>
      <td bgcolor="#dfdbdb">
      
      	<table>
      		<tr>
      		 <td valign="top">	
         		<select name='StatusID'>
           			<option value='0' {if $data->StatusID eq 0}selected{/if} >�� �����������</option>
           			<option value='1' {if $data->StatusID eq 1}selected{/if} >����� ������</option>
           			{* <option value='2' {if $data->StatusID eq 2}selected{/if} >������� ������</option> *}
           			<option value='10' {if $data->StatusID eq 10}selected{/if} >���������</option>
           			<option value='5' {if $data->StatusID eq 5}selected{/if} >�����������</option>
           			<option value='12' {if $data->StatusID eq 12}selected{/if} >������</option>
	   			<option value='15' {if $data->StatusID eq 15}selected{/if} >��������</option>
           			<option value='17' {if $data->StatusID eq 17}selected{/if} >�����</option>
                                <option value='250' {if $data->StatusID eq 250}selected{/if} >�����������</option>
         		</select> 
         	 </td>
         	 <td valign="top" bgcolor="#c3e5c8">  
         	 
         	    <table width="100%">
                     {*
         	    	<tr>
         	    		<td width="50%"  bgcolor="#dbf4df"><b>���. ��� ���������:</b></td><td width="50%" bgcolor="#dbf4df"><b>������������ �� �������:</b> (���-�����-�����)</td>
         	    	</tr>
         	    	*}
                    {*
         	    	<tr>
         	    		<td><input type="checkbox" name="position" {if $data->position > 0}checked{/if}>������� �����������</td>
         	    		<td bgcolor="#dbf4df">
         	    			c <input type='text' size=30 name="positiondatestart" value="{if strlen($data->positiondatestart) > 1}{$data->positiondatestart}{else}{$data->DateStart}{/if}">
         	    			�� <input type='text' size=30 name="positiondatestop" value="{if strlen($data->positiondatestop) > 1}{$data->positiondatestop}{else}{$data->DateStart}{/if}">         	    		
         	    		</td>
         	    	</tr>
         	    	<tr>
         	    		<td><input type="checkbox" name="leftblock" {if $data->leftblock > 0}checked{/if}>���-������� �����</td>
         	    		<td bgcolor="#dbf4df" nowrap>
         	    			c <input type='text' size=30 name="leftblockdatestart" value="{if strlen($data->leftblockdatestart) > 1}{$data->leftblockdatestart}{else}{$data->DateStart}{/if}">
         	    			�� <input type='text' size=30 name="leftblockdatestop" value="{if strlen($data->leftblockdatestop) > 1}{$data->leftblockdatestop}{else}{$data->DateStart}{/if}">
         	    		</td>
         	    	</tr>
         	    	<tr>
         	    		<td><input type="checkbox" name="rightblock" {if $data->rightblock > 0}checked{/if}>���-������� ������</td>
         	    		<td bgcolor="#dbf4df">
         	    			c <input type='text' size=30 name="rightblockdatestart" value="{if strlen($data->rightblockdatestart) > 1}{$data->rightblockdatestart}{else}{$data->DateStart}{/if}">
         	    			�� <input type='text' size=30 name="rightblockdatestop" value="{if strlen($data->rightblockdatestop) > 1}{$data->rightblockdatestop}{else}{$data->DateStart}{/if}">
         	    		</td>
         	    	</tr>
         	    	<tr>
         	    		<td><input type="checkbox" name="viptopposition" {if $data->viptopposition > 0}checked{/if}>������ � ���-��������</td>
         	    		<td bgcolor="#dbf4df">
           	    			c <input type='text' size=30 name="viptoppositiondatestart" value="{if strlen($data->viptoppositiondatestart) > 1}{$data->viptoppositiondatestart}{else}{$data->DateStart}{/if}">
         	    			�� <input type='text' size=30 name="viptoppositiondatestop" value="{if strlen($data->viptoppositiondatestop) > 1}{$data->viptoppositiondatestop}{else}{$data->DateStart}{/if}">
         	    		</td>
         	    	</tr>
         	    	*}
                    {*
         	    	<tr>
         	    		<td><input type="checkbox" name="colorset" {if $data->colorset > 0}checked{/if}>�������� ������</td>
         	    		<td bgcolor="#dbf4df"  nowrap>
           	    			c <input type='text' size=30 name="colorsetdatestart" value="{if strlen($data->colorsetdatestart) > 1}{$data->colorsetdatestart}{else}{$data->DateStart}{/if}">
         	    			�� <input type='text' size=30 name="colorsetdatestop" value="{if strlen($data->colorsetdatestop) > 1}{$data->colorsetdatestop}{else}{$data->DateStart}{/if}">
         	    		</td>
         	    	</tr>
                    *}
         	    </table>
         	 
              </td>
         	</tr>
       </table>  
         
      </td>
    </tr>

    <tr>
      <td align='right' valign='top'>������� Google Maps</td>
      <td bgcolor="#dfdbdb">
        {literal}
            <script language="javascript">
               function googleCoordRequest() {
               	
               		var text_address = document.getElementById("text_addressId");
               		var url = "http://maps.google.com/maps/geo?q="+text_address.value+"&output=xml&key=ABQIAAAAnavXIsM5jdni-p-JoKqdIxTPIkIa5BjXbMz0WvIzZffrVGiazxTRu-mHw0IpP9Ib5fP2129LbcNtTw&oe=utf-8";
               		//window.alert(url);
               		window.open(url);
               }
            </script>
        {/literal}	
         <select name='zoomID'>
          <option {if $zoom eq "1"}selected{else}{/if}>1</option>
          <option {if $zoom eq "2"}selected{else}{/if}>2</option>
          <option {if $zoom eq "3"}selected{else}{/if}>3</option>
          <option {if $zoom eq "4"}selected{else}{/if}>4</option>
          <option {if $zoom eq "5"}selected{else}{/if}>5</option>
          <option {if $zoom eq "6"}selected{else}{/if}>6</option>
          <option {if $zoom eq "7"}selected{else}{/if}>7</option>
          <option {if $zoom eq "8"}selected{else}{/if}>8</option>
          <option {if $zoom eq "9"}selected{else}{/if}>9</option>
          <option {if $zoom eq "10"}selected{else}{/if}>10</option>
          <option {if $zoom eq "11"}selected{else}{/if}>11</option>
          <option {if $zoom eq "12"}selected{else}{/if}>12</option>
          <option {if $zoom eq "13"}selected{else}{/if}>13</option>
          <option {if $zoom eq "14"}selected{else}{/if}>14</option>
          <option {if $zoom eq "15"}selected{else}{/if}>15</option>
         </select> 
         latitude: <input type="text" name="latitude" value="{$data->latitude}">
         longitude: <input type="text" name="longitude" value="{$data->longitude}">
         <br>
         <input type="text" size="50" id="text_addressId" value="{$data->SiteLatID}">
         <input type="button" value="��������� ����������" onclick="googleCoordRequest();">
      </td>
    </tr>

    <tr>
      <td align='right' valign='top'>��������</td>
      <td bgcolor="#dfdbdb">
      
      	<select name="brokerId">
      		<option value="0">---</option>
      		{foreach name=nname key=kcompanies item=icompanies from=$companies}
      		    {if $icompanies->accessId eq 2 and $data->brokerId eq $icompanies->Id}
      				<option value="{$icompanies->Id}" {if $icompanies->accessId == 2}style="color:white;background-color:gray;"{/if}  {if $data->brokerId eq $icompanies->Id}selected{/if}>{$icompanies->companyName}</option>
      			{elseif $icompanies->accessId ne 2}
      				<option value="{$icompanies->Id}" {if $icompanies->accessId == 2}style="color:white;background-color:gray;"{/if}  {if $data->brokerId eq $icompanies->Id}selected{/if}>{$icompanies->companyName}</option>
      			{/if}
      		{/foreach}
      	</select>
      </td>
    </tr>
    
    <tr>
      <td align='right' valign='top'>YOUTUBE ������</td>
      <td bgcolor="#dfdbdb">

         ������ �� �������: <input type='text'  size=70 name="youtubeURL" value="{$data->youtubeURL}">
         <br>
         ���������� ������: <input type='text'  size=70 name="youtubeShortURL" value="{$data->youtubeShortURL}">
      
      </td>
    </tr>

    <tr> 
            <td width='30%' align='right' valign='top'>�������� pdf �����:</td>
            <td bgcolor="#dfdbdb"> 
                {if strlen($data->pdf)} 
                    <p align="left"> 
                        <a class="city_a" style="font-size: 10pt;" href="../doc/{$data->pdf}" target="_blank">������� ��������� �������� (pdf)</a> &nbsp;<img src="../images/pdf.gif" alt="" />
                        <input type="checkbox" name="pdfdrop"> �������
                    </p>                 
                {/if}
                <input type="file" name="pdfupload">
            </td>
    </tr> 
    
    
    <tr>
      <td colspan=2 align='right'><input type='submit' value='���������'></td>
    </tr>



    
  </table>
</form>
<br>
<form method="post">
	<input type='hidden' name='updatepayment' value='updatepayment'>
	<input type='hidden' name='ID' value='{$smarty.get.ID}'>

	<table width='100%' bgcolor='#cccccc'>
		<th colspan="2" align="left">[���������� �������� ������]</th>
    	<tr>
      		<td width='30%' align='right' valign='top'>����� �������:</td>
      		<td bgcolor="#dfdbdb">
         		<input type='text' size=50 name="pay_summ" value="{$data->pay_summ}"> 
      		</td>
    	</tr>
    	<tr>
      		<td width='30%' align='right' valign='top'>���� �������:</td>
      		<td bgcolor="#dfdbdb"> 
         		<input type='text' size=50 name="pay_datetime" value="{if strlen($data->pay_datetime) > 1}{$data->pay_datetime}{else}{$data->DateStart}{/if}"> <input type="checkbox" name="pay_datetime_auto">������������ ������� �����
      		</td>
    	</tr> 
    	<tr>
      		<td width='30%' align='right' valign='top'>������ �������:</td>
      		<td bgcolor="#dfdbdb">
         		<select name="pay_status">
         			<option value="0" {if $data->pay_status eq "0"}selected{else}{/if}>�� ��������</option>
         			<option value="1"  {if $data->pay_status eq "1"}selected{else}{/if}>��������</option>
         		</select>
      		</td>
    	</tr>     	   	
    	<tr>
      		<td colspan=2 align='right'><input type='submit' value='���������'></td>
    	</tr>
	</table> 
<br>	

</form>

{*

<form method="post">
	<input type='hidden' name='autoupdate' value='autoupdate'>
	<input type='hidden' name='ID' value='{$smarty.get.ID}'>

 	<table width='100%' bgcolor='#cccccc'>
		<th colspan="2" align="left">[�������������� ��������]</th>
    	<tr>
      		<td width='30%' align='right' valign='top'>��������:</td>
      		<td bgcolor="#dfdbdb">
         		<input type='checkbox' size=50 name="autoupdatestatus" {if $data->autoupdatestatus eq "1"}checked{else}{/if}> 
      		</td>
    	</tr>
    	<tr>
    		<td width='30%' align='right' valign='top'>���� ������:</td>
    		<td bgcolor="#dfdbdb">
				<input type='text' size=50 name="autoupdatedatestart" value="{if strlen($data->autoupdatedatestart) > 1}{$data->autoupdatedatestart}{else}{$data->DateStart}{/if}">     		
    		</td>
    	</tr>
    	<tr>
      		<td width='30%' align='right' valign='top'>�����:</td>
      		<td bgcolor="#dfdbdb">
         		<select name="autoupdatetariff">
         			<option value="0">�������</option>>
         			<option value="10"  {if $data->autoupdatetariff eq "10"}selected{else}{/if}>10 ����</option>
         			<option value="20"  {if $data->autoupdatetariff eq "20"}selected{else}{/if}>20 ����</option>
         			<option value="30"  {if $data->autoupdatetariff eq "30"}selected{else}{/if}>30 ����</option>
         			<option value="60"  {if $data->autoupdatetariff eq "60"}selected{else}{/if}>60 ����</option>
         			<option value="90"  {if $data->autoupdatetariff eq "90"}selected{else}{/if}>90 ����</option>
         		</select>
      		</td>
    	</tr>     	   	
    	<tr>
      		<td colspan=2 align='right'><input type='submit' value='���������'></td>
    	</tr>
	</table>

	
</form>

*}

<form enctype="multipart/form-data" method="post">
	<input type="hidden" name="galleryupdate" value="galleryupdate">
	<table width='100%' bgcolor='#cccccc'>
		<th colspan="2" align="left">[�������� ��������]</th>
		<tr>
			<td width="30%">���� 1 (������� ����)</td>
			<td><input type="file" name="adminfile1"></td>
		</tr>
		<tr>
			<td>���� 2</td>
			<td><input type="file" name="adminfile2"></td>
		</tr>		
		<tr>
			<td>���� 3</td>
			<td><input type="file" name="adminfile3"></td>
		</tr>
		<tr>
			<td>���� 4</td>
			<td><input type="file" name="adminfile4"></td>
		</tr>	
		<tr>
			<td>���� 5</td>
			<td><input type="file" name="adminfile5"></td>
		</tr>				
		<tr>
			<td colspan="2" align="left">������� ��� ����: <input type="checkbox" name="admindeletefile"></td>
		</tr>	
		<tr>
			<td colspan="2" align="right">
				<input type="submit" value="��������">
			</td>
		</tr>
	</table>		
</form>


{include file="sell/processing.tpl"}


{else}


  <table border=0 width='100%' bgcolor='#cccccc'>
    <tr>
      <th>�����</th>
      <th width='3%' nowrap>�</th>
      <th>��������</th> 
      <th nowrap>���� ��������</th> 
      <th nowrap>���� ��������</th>
      <th>������</th>
      <th>����������</th>
      <th>��</th>
      <th></th>
      <th></th>
    <tr>
    {foreach name=sell key=key item=item from=$data}
     <tr  {if $item->StatusID == 0}bgcolor='#58F5D2' {elseif $item->DateBySecondClose <= $currentDateBySecond && $item->StatusID != 10}bgcolor="#F99F2B"{elseif $item->StatusID == 10}bgcolor='#B3B1AE'{elseif $item->StatusID == 5}bgcolor='#F0D3D3'{elseif $item->StatusID == 15}bgcolor='#ffffff'{elseif $item->StatusID == 17}bgcolor='#2F7DAA'{elseif $item->StatusID == 250}bgcolor='#d49696'{else}bgcolor="{cycle values="#bcd1e3,#e9e9e9"}"{/if}>
       <td align="center">
           {if $item->isPromo eq '1'}
               <div style='font-weight:bold;background-color:#5da4ed;color:white;margin:2px;'>�����</div>
               <hr>
           {/if}
           {$item->pay_summ}
       </td>
       <td>{$item->ID}</td>
       
       <td>
       	{if $item->helpbroker == 1}<img src="http://{$smarty.server.SERVER_NAME}/simg/alertbrokerhelp.jpg">&nbsp;{/if}{if $item->typeSellID == 2}<span style="color:black;font-weight:bold;">(������)&nbsp;</span>{else}{/if}<a href='../detailsSell.php?ID={$item->ID}&nocount=nocount' style='text-decoration:none;' target="_blank">{$item->NameBizID}</a>
       </td>
        
       <td align="center" {if strtotime("now") -  $item->DateBySecondCreate > 3600 and $item->StatusID == 0}bgcolor="yellow"{else}{/if} >{$item->DateBySecondCreate|date_format:"%d %B %Y %H:%M"}</td>
       
       <td align="center">{$item->DateBySecondClose|date_format:"%d %B %Y %H:%M"}</td>

       <td nowrap align="center" style="padding:3px;">
       
         {if $item->StatusID == 0}
           �� �������
         {elseif $item->StatusID == 1}
           ����� ������
         {elseif $item->StatusID == 2}
           ������� ������
         {elseif $item->StatusID == 5}
           �����������
         {elseif $item->StatusID == 10}
           ���������
         {elseif $item->StatusID == 15}
           �������� 
         {elseif $item->StatusID == 17}
           �����
         {elseif $item->StatusID == 250}
           �����������
         {/if} 
        
       </td>
       
       <td align="center"><a href="./template.php?typeeventobjectId=1&objectId={$item->ID}"><img src="../sicon/emailhistory.png" border="0" title="������� ����������"></td>
       <td><a href="http://{$smarty.server.SERVER_NAME}/cabinet/index.php?auth=1&loginID={$item->login}&passwordID={$item->password}" target="_blank"><img src="http://{$smarty.server.SERVER_NAME}/images/cabauto.png" border=0></a></td>
       <td>
       	<a href='./UpdateSell.php?ID={$item->ID}'>�������������</a>
       </td>
       <td colspan=2><a href="../inside/clone.php?ID={$item->ID}" target="_blank" title="��������� �� ���������"><img src="../images/clone.gif" border="0"></a></td>	
     </tr>

    <tr>
        <td colspan="2" style="padding:5px;">
        </td>
        <td style="padding:5px;" align="left">
            IP: {if $item->statusblockip}<span style='color:red;font-weight:bold;'>{/if} {$item->ip} {if $item->statusblockip}</span>{/if} {if !$item->statusblockip}<a href='./UpdateSell.php?blockip=down&ip={$item->ip}'><img src='http://www.bizzona.ru/predprinimatel/uploads/images/00/00/01/2014/06/01/b4f55479ed.png' border='0' style='margin:2pt;' alt="�����������" title="�����������"></a>{/if} {if $item->statusblockip}<a href='./UpdateSell.php?blockip=up&ip={$item->ip}'><img src='http://www.bizzona.ru/predprinimatel/uploads/images/00/00/01/2014/06/01/c6dbc52357.png' border='0' style='margin:2pt;' title="��������������" alt="��������������"></a>{/if}
        </td>
        <td colspan="3" style="padding:5px;"  align="left"> 
            {$item->getseo} {$item->defaultUserCountry}  {if strlen($item->source) > 0}��������: <a href="{$item->source}" target="_blank">{$item->source|truncate:30}</a>{/if}
        </td>
        <td colspan="5" align="center" nowrap>
            <a href="#" onclick ='ShowHideBlockMessage("div{$item->ID}")'>��������� ���������� �������</a> 
            <div id="div{$item->ID}" style='margin:8px;border:3px #c3c3c3 solid;display:none;background-color:white;padding:4px;' align="left" >
                <ul>
                    <li><a href="./UpdateSell.php?typeEventId=11&codeId={$item->ID}" ><img src="http://{$smarty.server.SERVER_NAME}/sicon/restore.png" title="�������������� ������ � ������" border='0' style='padding-right:10px;'>������� ����� � ������</a> 
                    <li><a href="./UpdateSell.php?typeEventId=1&codeId={$item->ID}" ><img src="http://{$smarty.server.SERVER_NAME}/sicon/supply.jpg" title="�����������" border='0' style='padding-right:10px;'>�����������</a> 
                    <li><a href="./UpdateSell.php?typeEventId=3&codeId={$item->ID}" ><img src="http://{$smarty.server.SERVER_NAME}/sicon/o25.png"  title="������������" border='0' style='padding-right:10px;'>������������</a>  
                    <li><a href="./UpdateSell.php?typeEventId=4&codeId={$item->ID}" ><img src="http://{$smarty.server.SERVER_NAME}/sicon/cl25.png" title="�������" border='0' style='padding-right:10px;'>�������</a>      
                </ul>
           </div>     
        </td>
    </tr>
     
    {/foreach}
  </table> 

{/if}