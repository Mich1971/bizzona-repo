{literal}
<!-- Place inside the <head> of your HTML -->
<script type="text/javascript" src="http://{/literal}{$smarty.server.SERVER_NAME}{literal}/engine/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
tinymce.init({
    selector: "textarea.editme", 
    theme: "modern",
    language : 'ru',
    height : 300, 
    plugins: [
         "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
         "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
         "save table contextmenu directionality emoticons template paste textcolor"
   ],
   content_css: "css/content.css",
   toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | l      ink image | print preview media fullpage | forecolor backcolor emoticons", 
   style_formats: [
        {title: 'Bold text', inline: 'b'},
        {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
        {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
        {title: 'Example 1', inline: 'span', classes: 'example1'},
        {title: 'Example 2', inline: 'span', classes: 'example2'},
        {title: 'Table styles'},
        {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
    ]    
 });
 
</script>
{/literal}

{foreach name=n key=k item=i from=$list}
    
    <form method="post">
    
    <input type="hidden" name='type_insert' value='{$i.type_insert}'>     
        
    <table width="99%"  align='center' bgcolor='#cccccc'>
        <tr bgcolor="#bcd1e3">
            <td><b>{$i.type_insert_text}</b></td>
            <td><nobr>
                ��� ����������: 
                <select name='type_show'>
                    {foreach key=kl item=li from=$listtypeshow}
                        <option value='{$kl}' {if $i.type_show eq $kl} selected {/if}>{$li}</option>
                    {/foreach} 
                </select>
                ���������� �����-�����: 
                <input type="checkbox" name="use_promo" {if $i.use_promo eq 1}checked="checked"{/if} />
            </nobr></td>
        </tr>    

        <tr>
            <td colspan="2">
                <table width="99%">
                    <tr>
                        <td>����� - "������� ����������"</td>
                        <td>����� - "���������� ����������"</td>
                        <td>����� - "����� ����������"</td>
                    </tr>
                    <tr>
                        <td> 
                            <textarea class='editme' name='pay'>{if isset($i.pay)}{$i.pay}{/if}</textarea>
                        </td>
                        <td>
                            <textarea class='editme' name='no_pay'>{if isset($i.no_pay)}{$i.no_pay}{/if}</textarea>
                        </td>
                        <td>  
                            <textarea class='editme' name='promo'>{if isset($i.promo)}{$i.promo}{/if}</textarea>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        
        <tr bgcolor="#bcd1e3">
            <td colspan="2" align='right'>
                <input type='submit' value="���������">
            </td>
        </tr>
        
    </table>    
    
    </form>    
    <br>    
{/foreach}    
