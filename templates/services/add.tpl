{literal}
<!-- Place inside the <head> of your HTML -->
<script type="text/javascript" src="http://{/literal}{$smarty.server.SERVER_NAME}{literal}/engine/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
tinymce.init({
    selector: "textarea.editme", 
    theme: "modern",
    language : 'ru',
    height : 300, 
    plugins: [
         "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
         "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
         "save table contextmenu directionality emoticons template paste textcolor"
   ],
   content_css: "css/content.css",
   toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | l      ink image | print preview media fullpage | forecolor backcolor emoticons", 
   style_formats: [
        {title: 'Bold text', inline: 'b'},
        {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
        {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
        {title: 'Example 1', inline: 'span', classes: 'example1'},
        {title: 'Example 2', inline: 'span', classes: 'example2'},
        {title: 'Table styles'},
        {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
    ]    
 });
</script>
{/literal}


<form method="POST">

<table width="100%" bgcolor='#cccccc'>
	<tr>
		<td bgcolor="#dfdbdb"align="left">
			��� / ����. ����������: <input type="checkbox" name="statusId" {if isset($item) &&  $item.statusId}checked{/if}><br>
		</td>
		<td bgcolor="#dfdbdb"align="right">
			��� ��������: <input type="checkbox" name="isBroker" {if isset($item) &&  $item.isBroker}checked{/if}><br>
			��������� �������� <a href="http://{$smarty.server.SERVER_NAME}/kak-prodat-biznes-bistro.php" target="_blank">"��� ������� �������?"</a>: <input type="checkbox" name="isFastShow" {if isset($item) &&  $item.isFastShow}checked{/if}> <br>
			C������� <a href="http://{$smarty.server.SERVER_NAME}/prise.php" target="_blank">"����"</a>: <input type="checkbox" name="isPrice" {if isset($item) &&  $item.isPrice}checked{/if}> <br>
			���������� � �������� ���������� : <input type="checkbox" name="isCabinet" {if isset($item) &&  $item.isCabinet}checked{/if}> <br>
		</td>
	</tr>
	<tr>
		<td width="20%" valign="top">������� ������������ ������:</td>
		<td bgcolor="#dfdbdb">
			<input type="text" name="shortName" value='{if isset($item)}{$item.shortName}{/if}' style="width:80%;">
		</td>
	</tr>
	<tr>
		<td width="20%" valign="top">������ ������������ ������:</td>
		<td bgcolor="#dfdbdb">
     	 			<textarea name="name" class='editme'>
     	 				{php}
     	 					global $item; 
     	 					echo $item['name'];
     	 				{/php}
      	 			</textarea> 									
		</td>
	</tr>	
	<tr>
		<td width="20%" valign="top">��������� �������� ������:</td>
		<td bgcolor="#dfdbdb">
     	 			<textarea name="description" class='editme'>
     	 				{php}
     	 					global $item; 
     	 					echo $item['description'];
     	 				{/php}
      	 			</textarea> 									
		</td>
	</tr>	
	<tr>
		<td colspan="2" align="right">
		{if !isset($item)}
			<input type="hidden" name="add" value="add">
		{else}
			<input type="hidden" name="update" value="update">
		{/if}	
			<input type="submit" value="���������">
		</td>
	</tr>
	
</table>

</form>