{literal}
<!-- Place inside the <head> of your HTML -->
<script type="text/javascript" src="http://{/literal}{$smarty.server.SERVER_NAME}{literal}/engine/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
tinymce.init({
    selector: "textarea.editme", 
    theme: "modern",
    language : 'ru',
    height : 300, 
    plugins: [
         "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
         "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
         "save table contextmenu directionality emoticons template paste textcolor"
   ],
   content_css: "css/content.css",
   toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | l      ink image | print preview media fullpage | forecolor backcolor emoticons", 
   style_formats: [
        {title: 'Bold text', inline: 'b'},
        {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
        {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
        {title: 'Example 1', inline: 'span', classes: 'example1'},
        {title: 'Example 2', inline: 'span', classes: 'example2'},
        {title: 'Table styles'},
        {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
    ]    
 });
</script>
{/literal}


{if isset($error)} 
	<h3 style="color:red;" align="center">{$error}</h3>
{/if}

<table width="100%">
<tr>
<td align="left" width="50%">
{if isset($smarty.get.add)}
<h3 style="color:green;">����� ��� ���������� ������� ����������</h3>
{else}
<h3>����� ��� �������������� ������� ���������� ({$item->TypeEventName} => {$item->TypeEventObjectName})</h3>
{/if}
</td>
</table>
<form method="post" style="margin-top:0pt;margin-bottom:0pt;" enctype="multipart/form-data">

<table  width='100%' bgcolor='#cccccc'>
	<tr>
		<td align="left" bgcolor="#e9e9e9">
			<table width="90%">
				<tr>
					<td valign="top" bgcolor="#d1e1cc">
						���. / ����. ����������: <input type="checkbox" name="statusId" {if $item->statusId == 1}checked{/if}>	
					</td>
					<td valign="top" bgcolor="#d1e1cc">
						<table>
							<tr>
								<td>���������� ����������: </td>
								<td>
									<select name="isBrokerId">
										<option value="0">�����������</option>
										<option value="1" {if $item->isBrokerId == 1}selected{/if}>������</option>
									</select>				
								</td>
							</tr>
							<tr>
								<td>��� ����������:  </td>
								<td>
									<select name="typeEventId">
										{foreach name=n key=k item=i from=$listtypeevent}
											<option value="{$i.Id}" {if $item->TypeEventId == $i.Id}selected{/if}>{$i.name}</option>
										{/foreach}
									</select>								
								</td>
							</tr>	
							<tr>
								<td>��� ����������:  </td>
								<td>
									<select name="typeEventObjectId">
										{foreach name=n key=k item=i from=$listtypeeventobject}
											<option value="{$i.Id}" {if $item->TypeEventObjectId == $i.Id}selected{/if}>{$i.name}</option>
										{/foreach}
									</select>								
								</td>
							</tr>
						</table>
					</td>
					<td valign="top" bgcolor="#d1e1cc">
						<table>
							<tr>
								<td valign="top">�����:</td>
								<td valign="top">
									<select name="listCityIds[]" multiple>
										<option value="">�������</option>
											{foreach name=n key=k item=i from=$listcity}
												<option value="{$i.parent}"  {if isset($item) && $i.parent|in_array:$item->alistCityIds}selected{else}{/if}  >{$i.name}</option>
											{/foreach}			
									</select>
								</td>
							</tr>		
						</table>
					</td>
    				{if $item->TypeEventId == 5 or $smarty.get.add}
						<td align="left"  bgcolor="#d1e1cc" valign="top">
							<p>���������� ����: <input type="text" value="{$item->countdays}" name="countdays"></p>	
						</td>
					{/if}
				</tr>
			</table>
		</td>
	</tr>
	
	<tr> 
		<td align="left" bgcolor="#e9e9e9">
			<p>��������� ������� ������:</p>	
		</td>
	</tr>
	<tr>
		<td align="center">
			<textarea name="title"  rows="4" style="width:80%">{$item->title}</textarea>
		</td>
	</tr>
	<tr>
		<td align="left" bgcolor="#e9e9e9">
		 	<p>����� ������� ������:</p>			
		</td>
	</tr>
	<tr>
		<td align="center">
     	 			<textarea name="template" class='editme'>
      	  				{php}
      	  					global $item;
      	  					echo $item->template;
      	  				{/php}
      	 			</textarea> 									
		</td>
	</tr>
	<tr>
		<td align="right">
			<input type="submit" value="���������">
		</td>
	</tr>
</table>

{if isset($smarty.get.add)}
	<input type="hidden" name="add" value="add">
{else}
	<input type="hidden" name="update" value="update">
{/if}

</form>