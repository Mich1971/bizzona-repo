{literal}
<!-- Place inside the <head> of your HTML -->
<script type="text/javascript" src="http://{/literal}{$smarty.server.SERVER_NAME}{literal}/engine/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
tinymce.init({
    selector: "textarea.editme", 
    theme: "modern",
    language : 'ru',
    height : 300, 
    plugins: [
         "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
         "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
         "save table contextmenu directionality emoticons template paste textcolor"
   ],
   content_css: "css/content.css",
   toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | l      ink image | print preview media fullpage | forecolor backcolor emoticons", 
   style_formats: [
        {title: 'Bold text', inline: 'b'},
        {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
        {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
        {title: 'Example 1', inline: 'span', classes: 'example1'},
        {title: 'Example 2', inline: 'span', classes: 'example2'},
        {title: 'Table styles'},
        {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
    ]    
 });
</script>
{/literal}


<table width='100%' cellpadding="0" cellspacing="0" style='border-top: 1px; border-bottom: 2px; border-left:1px;  border-right:1px;  border-color:#006699;  border-style: solid;'>
  <tr style="background-color:darkgreen ">
  	<td  width='10%' align='left' nowrap><a href="http://{$smarty.server.SERVER_NAME}/inside/" style="color:white;font-weight:bold;">�������</a></td>
  	<td  width='10%' align='left' nowrap><a href="http://{$smarty.server.SERVER_NAME}{$smarty.server.SCRIPT_NAME}" style="color:white;font-weight:bold;">����� ������</a></td>
  	<td width="80%">&nbsp;</td>
  </tr>
</table>
<form method="POST" style="margin-top:0pt;">
<table width='100%' align="center" bgcolor="#cccccc">

	<tr>
		<td align='left' bgcolor="red">
			<a href="../detailsCoop.php?ID={$smarty.get.ID}&nocount=nocount" target="_blank"  style='color:white;font-weight:bold;' >����������� �������� ����������</a>
		</td>
    	<td bgcolor="#dfdbdb" align='right' style='font-size:9pt;color:green;'>
			<a href="./template.php?typeeventobjectId=5&objectId={$smarty.get.ID}&repeat=1"><img src='../images/trueNotifyPublic.gif' border=0></a>          		 
    	</td>		
	</tr>


	<tr>
		<td style="padding-left:5pt;" width="30%">�������� �������</td>
		<td bgcolor="#dfdbdb">
			<table width="100%">
				<tr>
					<td style="padding-right:2pt;">
						<input type="text" id="name" name="name" style="width:100%;" value="{$data->name}">
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td style="padding-left:5pt;" width="30%">��� �������</td>
		<td bgcolor="#dfdbdb">
			<table width="100%">
				<tr>
					<td>
						<select name="category" style='font-size:10pt;font-family:tahoma;width:100%;'>
							<option value="0">������� ���������</option>
							{foreach name='category' item=item key=key from=$listCategory}
								<optgroup label="{$item->Name}" >
									{foreach name='subcategory' item=subitem key=subkey from=$listSubCategory}
									{if $item->ID eq $subitem->CategoryID}
									<option value="{$item->ID}:{$subitem->ID}" {if $data->category_id eq $item->ID and $subitem->ID eq $data->subcategory_id}selected{/if} >{$subitem->Name}</option>
									{/if}
									{/foreach}
								</optgroup>
							{/foreach}
						</select>   
           			</td>     
				</tr>           			
			</table>				
		</td>
	</tr>
	<tr>
		<td style="padding-left:5pt;" width="30%">������������</td>
		<td  bgcolor="#dfdbdb">
			<table width="100%">
				<tr>
					<td style="padding-right:2pt;">
						<input type="text" id="region_text" name="region_text" value="{$data->region_text}" style="width:100%;" >
         				<select name="region_id" style="width:100%;" onchange='{php}global $sDistrict; $sDistrict->printScript();{/php}'>
					<option value="0">������� �����</option>
           				{foreach name=city key=key item=item from=$listCity}
              				<option value="{$item->ID}" {if $data->region_id eq $item->ID}selected{/if}>{$item->ID} {$item->title}</option> 
           				{/foreach}
         				</select>
         				<div id="subRegionDiv" style="padding-top:5pt;padding-bottom:5pt;">&#160;</div>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td style="padding-left:5pt;" width="30%">������� ��������</td>
		<td bgcolor="#dfdbdb">
			<table width="100%">
				<tr>
					<td style="padding-right:2pt;">
						<textarea style="width:100%;height:70px" id="shortdescription" name="shortdescription" >{$data->shortdescription}</textarea>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td style="padding-left:5pt;" width="30%" valign="top">
                    ������ ��������
                    <div>      
                      {include file="./textreplace.tpl" type="description"} 
                    </div>                     
                </td>
		<td bgcolor="#dfdbdb">
			<table width="100%">
				<tr>
					<td  style="padding-right:2pt;">
     	 			<textarea name="description" class='editme'>
      	  				{$data->description}
      	 			</textarea> 							
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td style="padding-left:5pt;" width="30%">���������� ����������</td>
		<td bgcolor="#dfdbdb">
			<table width="100%">
				<tr>
					<td width="25%"><div style="font-size:10pt;font-family:arial;"><span style="color:red;font-weight:bold;">&nbsp;*&nbsp;</span>���������� ����</div></td>
					<td style="padding-right:2pt;"><input type="text" id="contact_face" name="contact_face" style="width:100%;" value="{$data->contact_face}" ></td>
				</tr>
				<tr>
					<td><div style="font-size:10pt;font-family:arial;"><span style="color:red;font-weight:bold;">&nbsp;*&nbsp;</span>��������</div></td>
					<td style="padding-right:2pt;"><input type="text" id="contact_phone" name="contact_phone" style="width:100%;" value="{$data->contact_phone}" ></td>
				</tr>
				<tr>
					<td><div style="font-size:10pt;font-family:arial;"><span style="color:red;font-weight:bold;">&nbsp;*&nbsp;</span>Email</div></td>
					<td style="padding-right:2pt;"><input type="text" id="contact_email" name="contact_email" value="{$data->contact_email}" style="width:100%;" ></td>
				</tr>
				<tr>
					<td><div style="font-size:10pt;font-family:arial;">����</div></td>
					<td style="padding-right:4pt;"><input type="text" id="contact_site" name="contact_site" value="{$data->contact_site}" style="width:100%;"></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td style="padding-left:5pt;" width="30%">������������ ����������</td>
		<td bgcolor="#dfdbdb">
			<select name="duration_id" style="width:100%;">
				<option value="1" {if $data->duration_id eq "1"}selected{else}{/if}>1 �����</option>
				<option value="2" {if $data->duration_id eq "2"}selected{else}{/if}>2 ������</option>
				<option value="3" {if $data->duration_id eq "3"}selected{else}{/if}>3 ������</option>
			</select>
		</td>
	</tr>
	
	<tr>
		<td style="padding-left:5pt;"><div style="font-size:10pt;font-family:arial;">������</div></td>
		<td bgcolor="#dfdbdb">
			<select name="status_id"  style="width:100%;">
				<option value="{$smarty.const.NEW_COOPBIZ}" {if $data->status_id eq $smarty.const.NEW_COOPBIZ}selected{else}{/if}>�����</option>
				<option value="{$smarty.const.OPEN_COOPBIZ}" {if $data->status_id eq $smarty.const.OPEN_COOPBIZ}selected{else}{/if}>������</option>
				<option value="{$smarty.const.CLOSE_COOPBIZ}" {if $data->status_id eq $smarty.const.CLOSE_COOPBIZ}selected{else}{/if}>������</option>
				<option value="{$smarty.const.LOOKED_COOPBIZ}" {if $data->status_id eq $smarty.const.LOOKED_COOPBIZ}selected{else}{/if}>����������</option>
				<option value="{$smarty.const.DUBLICATE_COOPBIZ}" {if $data->status_id eq $smarty.const.DUBLICATE_COOPBIZ}selected{else}{/if}>��������</option>
				<option value="{$smarty.const.DUST_COOPBIZ}" {if $data->status_id eq $smarty.const.DUST_COOPBIZ}selected{else}{/if}>�����</option>
				
			</select>
		</td>
	</tr>
	
	<tr>
		<td colspan="2" align="right"><input type="submit" value="���������"  style="font-size:10pt;font-family:arial;"></td>
	</tr>
	
</table>
</form>