{literal}
<!-- Place inside the <head> of your HTML -->
<script type="text/javascript" src="http://{/literal}{$smarty.server.SERVER_NAME}{literal}/engine/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
tinymce.init({
    selector: "textarea.editme", 
    theme: "modern",
    language : 'ru',
    height : 300, 
    plugins: [
         "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
         "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
         "save table contextmenu directionality emoticons template paste textcolor"
   ],
   content_css: "css/content.css",
   toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | l      ink image | print preview media fullpage | forecolor backcolor emoticons", 
   style_formats: [
        {title: 'Bold text', inline: 'b'},
        {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
        {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
        {title: 'Example 1', inline: 'span', classes: 'example1'},
        {title: 'Example 2', inline: 'span', classes: 'example2'},
        {title: 'Table styles'},
        {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
    ]    
 });
</script>
{/literal}



<table width='100%' cellpadding="0" cellspacing="0" style='border-top: 1px; border-bottom: 2px; border-left:1px;  border-right:1px;  border-color:#006699;  border-style: solid;'>
  <tr style="background-color:darkgreen ">
  	<td  width='10%' align='left' nowrap><a href="http://{$smarty.server.SERVER_NAME}/inside/" style="color:white;font-weight:bold;">�������</a></td>
  	<td  width='10%' align='left' nowrap><a href="http://{$smarty.server.SERVER_NAME}{$smarty.server.SCRIPT_NAME}" style="color:white;font-weight:bold;">����� ������</a></td>
  	<td width="80%">&nbsp;</td>
  </tr>
</table>


<form method="POST"  style="margin-top:0pt;margin-bottom:0pt;" enctype="multipart/form-data">

<table width='100%' align="center" bgcolor="#cccccc">
	<tr>
		<td  bgcolor="red"><a href="../detailsEquipment.php?ID={$data->id}&nocount=nocount" target="_blank" style="color:white;font-weight:bold;" >�����������  �� �����</a></td>
    	<td bgcolor="#dfdbdb" align='right' style='font-size:9pt;color:green;'>
			<a href="./template.php?typeeventobjectId=4&objectId={$data->id}&repeat=1"><img src='../images/trueNotifyPublic.gif' border=0></a>  
    	</td>		
	</tr>
	<tr {if strlen($text_error) > 0 }{else}style="display:none;"{/if}>
		<td colspan="2" align="center" class="desc_td"><span style="font-size:10pt;font-family:arial;color:red">{$text_error}</span></td>
	</tr>
	<tr {if strlen($text_success) > 0 }{else}style="display:none;"{/if}>
		<td colspan="2" align="center" class="desc_td"><span style="font-size:10pt;font-family:arial;color:green">{$text_success}</span></td>
	</tr>
	<tr>
		<td width="30%"><div style="font-size:10pt;font-family:arial;">�������� ������������</div></td>
		<td style="padding-right:3pt;" bgcolor="#dfdbdb">
			<input type="text" name="name" style="width:100%;" value='{$data->name}'>
		</td>
	</tr>
	
	<tr>
		<td style="padding-left:5pt;"><div style="font-size:10pt;font-family:arial;">���������</div></td>
		<td bgcolor="#dfdbdb">
			<div><b>������:</b> {$data->costtxt}</div>
			<input type="text" name="cost" style="width:50%;" value="{$data->cost}">
            <select style='font-size:10pt;font-family:arial;width:40% ' name="ccost">
            	<option value=usd {if $data->ccost eq "usd"}selected{else}{/if}>USD (������ ���)
            	<option value=eur {if $data->ccost eq "eur"}selected{else}{/if}>EUR (����)
            	<option value=rub {if $data->ccost eq "rub"}selected{else}{/if}>RUB (���������� �����)
            </select>
		</td>
	</tr>
	
	<tr>
		<td style="padding-left:5pt;"><div style="font-size:10pt;font-family:arial;">��������� ������� ����������</div></td>
		<td bgcolor="#dfdbdb">
			<input type="checkbox" name="agreementcost" {if $data->agreementcost}checked{else}{/if}>
		</td>
	</tr>

	<tr>
		<td style="padding-left:5pt;"><div style="font-size:10pt;font-family:arial;">����� ������ � ������</div></td>
		<td bgcolor="#dfdbdb">
			<input type="checkbox" name="leasing" {if $data->leasing}checked{else}{/if}>
		</td>
	</tr>
	
	<tr>
		<td  style="padding-left:5pt;"><div style="font-size:10pt;font-family:arial;">������������</div></td>
		<td bgcolor="#dfdbdb">
			<input type="text" name="city_text" style="width:100%;" value="{$data->city_text}">
         	<select name="city_id" style="width:100%;" onchange='{php}global $sDistrict; $sDistrict->printScript();{/php}'>
			<option value="0">������� �����</option>
           		{foreach name=city key=key item=item from=$listCity}
              		<option value="{$item->ID}" {if $data->city_id eq $item->ID}selected{/if} >{$item->ID} {$item->title}</option> 
           		{/foreach}
         	</select>
			<div id="subRegionDiv" style="padding-top:5pt;padding-bottom:5pt;">&#160;</div>
		</td>
	</tr>
    <tr>
      <td  align='right' valign='top'>������</td>
      <td bgcolor="#dfdbdb">
      		<table>
      			<tr>
      				<td valign="top"><input type="file" name="defaultUserIconFile"></td>
      				<td>{if strlen($data->sicon) > 0}<img src="http://{$smarty.server.SERVER_NAME}/sicon/{$data->sicon}">{/if}</td>
      			</tr>
      		</table>
			
			{if strlen($data->icon) > 0}
			<div>���������� ���� (1): http://{$smarty.server.SERVER_NAME}/simg/{$data->icon}</div>
			{/if}	
      </td>
	</tr>		
	<tr>
		<td  style="padding-left:5pt;"><div style="font-size:10pt;font-family:arial;">��� ������������</div></td>
		<td bgcolor="#dfdbdb">																										
        	<select name="category_id"  style="width:100%;">
            	<option value="0">������� ���������</option>
					{foreach name='category' item=item key=key from=$listCategory}
						<optgroup label="{$item->Name}" style="background-color:#EDEDCC;color:#000000;">
							{foreach name='subcategory' item=subitem key=subkey from=$listSubCategory}
								{if $item->ID eq $subitem->CategoryID}
									<option style="background-color:#ffffff;" value="{$item->ID}:{$subitem->ID}" {if $data->subCategory_id eq $subitem->ID}selected{/if} >{$subitem->Name}</option>
								{/if}
							{/foreach}
						</optgroup>
					{/foreach}
            </select>
		</td>
	</tr>

	<tr>
		<td style="padding-left:5pt;"><div style="font-size:10pt;font-family:arial;">������������ ��������������</div></td>
		<td bgcolor="#dfdbdb">
			<input type="text" name="age" style="width:100%;" value="{$data->age}">
		</td>
	</tr>
	
	<tr>
		<td style="padding-left:5pt;" valign="top">
                    <div style="font-size:10pt;font-family:arial;">������� ��������</div>
                    <div>     
                      {include file="./textreplace.tpl" type="shortdescription"} 
                    </div>                       
                </td>
		<td bgcolor="#dfdbdb">
     	 			<textarea name="shortdescription" class='editme'>
      	  				{$data->shortdescription}
      	 			</textarea> 		
		</td>
	</tr>
	
	<tr>
		<td style="padding-left:5pt;" valign="top">
                    <div style="font-size:10pt;font-family:arial;">��������</div>
                    <div>      
                      {include file="./textreplace.tpl" type="description"} 
                    </div>                       
                </td>
		<td bgcolor="#dfdbdb">
     	 			<textarea name="description" class='editme'>
      	  				{$data->description}
      	 			</textarea> 		
		</td>
	</tr>

	<tr>
		<td style="padding-left:5pt;"><div style="font-size:10pt;font-family:arial;">���������� ����</div></td>
		<td bgcolor="#dfdbdb">
			<input type="text" name="contact_face" style="width:100%;" value="{$data->contact_face}">
		</td>
	</tr>

	<tr>
		<td style="padding-left:5pt;"><div style="font-size:10pt;font-family:arial;">��������</div></td>
		<td bgcolor="#dfdbdb">
			<input type="text" name="phone" style="width:100%;" value="{$data->phone}">
		</td>
	</tr>

	<tr>
		<td style="padding-left:5pt;"><div style="font-size:10pt;font-family:arial;">Email</div></td>
		<td bgcolor="#dfdbdb">
			<input type="text" name="email" style="width:100%;" value="{$data->email}">
		</td>
	</tr>

	<tr>
		<td style="padding-left:5pt;"><div style="font-size:10pt;font-family:arial;">������������</div></td>
		<td bgcolor="#dfdbdb">
			<select name="duration_id"  style="width:100%;">
				<option value="1" {if $data->duration_id eq "1"}selected{else}{/if}>1 �����</option>
				<option value="2" {if $data->duration_id eq "2"}selected{else}{/if}>2 ������</option>
				<option value="3" {if $data->duration_id eq "3"}selected{else}{/if}>3 ������</option>
			</select>
		</td>
	</tr>

	<tr>
		<td style="padding-left:5pt;"><div style="font-size:10pt;font-family:arial;">������</div></td>
		<td bgcolor="#dfdbdb">
			<select name="status_id"  style="width:100%;">
				<option value="{$smarty.const.NEW_EQUIPMENT}" {if $data->status_id eq $smarty.const.NEW_EQUIPMENT}selected{else}{/if}>�����</option>
				<option value="{$smarty.const.OPEN_EQUIPMENT}" {if $data->status_id eq $smarty.const.OPEN_EQUIPMENT}selected{else}{/if}>������</option>
				<option value="{$smarty.const.CLOSE_EQUIPMENT}" {if $data->status_id eq $smarty.const.CLOSE_EQUIPMENT}selected{else}{/if}>������</option>
				<option value="{$smarty.const.LOOKED_EQUIPMENT}" {if $data->status_id eq $smarty.const.LOOKED_EQUIPMENT}selected{else}{/if}>����������</option>
				<option value="{$smarty.const.DUBLICATE_EQUIPMENT}" {if $data->status_id eq $smarty.const.DUBLICATE_EQUIPMENT}selected{else}{/if}>��������</option>
				<option value="{$smarty.const.DUST_EQUIPMENT}" {if $data->status_id eq $smarty.const.DUST_EQUIPMENT}selected{else}{/if}>�����</option>
				
			</select>
		</td>
	</tr>

    <tr>
      <td align='right' valign='top'>YOUTUBE ������</td>
      <td bgcolor="#dfdbdb">

         ������ �� �������: <input type='text'  size=70 name="youtubeURL" value="{$data->youtubeURL}">
         <br>
         ���������� ������: <input type='text'  size=70 name="youtubeShortURL" value="{$data->youtubeShortURL}">
      
      </td>
    </tr>
	
	
	<tr>
		<td colspan="2" align="right"><input type="submit" value="���������"  style="font-size:10pt;font-family:arial;"></td>
	</tr>
	
</table>

</form>


<form method="post">
	<input type='hidden' name='updatepayment' value='updatepayment'>
	<input type='hidden' name='ID' value='{$smarty.get.ID}'>

	<table width='100%' bgcolor='#cccccc'>
		<th colspan="2" align="left">[���������� �������� ������]</th>
    	<tr>
      		<td width='30%' align='right' valign='top'>����� �������:</td>
      		<td bgcolor="#dfdbdb">
         		<input type='text' size=50 name="pay_summ" value="{$data->pay_summ}"> 
      		</td>
    	</tr>
    	<tr>
      		<td width='30%' align='right' valign='top'>���� �������:</td>
      		<td bgcolor="#dfdbdb"> 
         		<input type='text' size=50 name="pay_datetime" value="{if strlen($data->pay_datetime) > 1}{$data->pay_datetime}{else}{$data->DateStart}{/if}"> <input type="checkbox" name="pay_datetime_auto">������������ ������� �����
      		</td>
    	</tr> 
    	<tr>
      		<td width='30%' align='right' valign='top'>������ �������:</td>
      		<td bgcolor="#dfdbdb">
         		<select name="pay_status">
         			<option value="0" {if $data->pay_status eq "0"}selected{else}{/if}>�� ��������</option>
         			<option value="1"  {if $data->pay_status eq "1"}selected{else}{/if}>��������</option>
         		</select>
      		</td>
    	</tr>     	   	
    	
    	<tr>
      		<td colspan=2 align='right'><input type='submit' value='���������'></td>
    	</tr>
	</table> 
<br>	

</form>


<form method="post">
	<input type='hidden' name='updateshop' value='updateshop'>
	<input type='hidden' name='ID' value='{$smarty.get.ID}'>

	<table width='100%' bgcolor='#cccccc'>
		<th colspan="2" align="left">[������ �������� ��������]</th>
    	<tr>
      		<td width='30%' align='right' valign='top'>������������ ��������:</td>
      		<td bgcolor="#dfdbdb">
         		<select name="companyId">
         			<option value="0">�������</option>
         			<option value="1" {if $data->companyId eq "1"}selected{else}{/if}>������� ���������� ������������ (ek@massage-tables.ru, order@massage-tables.ru)</option>
         			<option value="2" {if $data->companyId eq "2"}selected{else}{/if}>�������� "billiardstyle" (best-billiards@yandex.ru)</option>
         			<option value="3" {if $data->companyId eq "3"}selected{else}{/if}>�������� "�����������" (podrazbor@gmail.com)</option>
         			<option value="4" {if $data->companyId eq "4"}selected{else}{/if}>�������� "���-������������" (eag-vol@yandex.ru)</option>
         		</select>
      		</td>
    	</tr>
    	<tr>
      		<td width='30%' align='right' valign='top'>��� �������:</td>
      		<td bgcolor="#dfdbdb"> 
         		<input type='text' size=20 name="objCompanyId" value="{$data->objCompanyId}">
      		</td>
    	</tr> 
    	<tr>
      		<td width='30%' align='right' valign='top'>URL �������:</td>
      		<td bgcolor="#dfdbdb">
      			<input type='text' size=100 name="urlobjCompanyId" value="{$data->urlobjCompanyId}">
      		</td>
    	</tr>     	   	
    	<tr>
      		<td colspan=2 align='right'><input type='submit' value='���������'></td>
    	</tr>
	</table> 
<br>	

</form>



<table width='100%' cellpadding="0" cellspacing="0" style='border-top: 1px; border-bottom: 2px; border-left:1px;  border-right:1px;  border-color:#006699;  border-style: solid;'>
  <tr style="background-color:darkgreen ">
  	<td  width='10%' align='left' nowrap><a href="http://{$smarty.server.SERVER_NAME}/inside/" style="color:white;font-weight:bold;">�������</a></td>
  	<td  width='10%' align='left' nowrap><a href="http://{$smarty.server.SERVER_NAME}{$smarty.server.SCRIPT_NAME}" style="color:white;font-weight:bold;">����� ������</a></td>
  	<td width="80%">&nbsp;</td>
  </tr>
</table>
