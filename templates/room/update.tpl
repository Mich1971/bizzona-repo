{literal}
<!-- Place inside the <head> of your HTML -->
<script type="text/javascript" src="http://{/literal}{$smarty.server.SERVER_NAME}{literal}/engine/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
tinymce.init({
    selector: "textarea.editme", 
    theme: "modern",
    language : 'ru',
    height : 300, 
    plugins: [
         "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
         "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
         "save table contextmenu directionality emoticons template paste textcolor"
   ],
   content_css: "css/content.css",
   toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | l      ink image | print preview media fullpage | forecolor backcolor emoticons", 
   style_formats: [
        {title: 'Bold text', inline: 'b'},
        {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
        {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
        {title: 'Example 1', inline: 'span', classes: 'example1'},
        {title: 'Example 2', inline: 'span', classes: 'example2'},
        {title: 'Table styles'},
        {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
    ]    
 });
</script>
{/literal}


{if $smarty.get.ID}

<form method="post" style="margin-top:0pt;margin-bottom:0pt;" id="updateroomformID" enctype="multipart/form-data">
  <input type='hidden' name='update' value='update'>
  <input type='hidden' name='ID' value='{$smarty.get.ID}'>

  <table width='100%' bgcolor='#cccccc'>

    <tr>
        <td align='center' bgcolor="red">
         <a href="../detailsroom.php?ID={$data->Id}&nocount=nocount" target="_blank" style='color:white;font-weight:bold;'>����������� �������� �������</a>
        </td> 
    	<td bgcolor="#dfdbdb" align='right' style='font-size:9pt;color:green;'>
			<a href="./template.php?typeeventobjectId=6&objectId={$data->Id}&repeat=1"><img src='../images/trueNotifyPublic.gif' border=0></a>
    	</td>
    </tr>  

    <tr>
      <td width='30%' align='right' valign='top'>{$Description.FML}<span style='color:red;'>*</span></td>
      <td bgcolor="#dfdbdb">
         <input type='text' size=50 name="fio" value="{$data->fio}">
      </td>
    </tr>    
    
    <tr>
      <td  align='right' valign='top'>{$Description.EmailID}<span style='color:red;'>*</span></td>
      <td bgcolor="#dfdbdb">
         <input type='text' size=50 name="email" value="{$data->email}">
      </td>
    </tr>
    
    <tr>
      <td  align='right' valign='top'>{$Description.PhoneID}<span style='color:red;'>*</span></td>
      <td bgcolor="#dfdbdb">
         <input type='text' size=50 name="phone" value="{$data->phone}">
         <span style="color:green;">(������ �������� ������� � ������� �����)</span>
      </td>
    </tr>

    <tr>
      <td  align='right' valign='top'>{$Description.FaxID}</td>
      <td bgcolor="#dfdbdb">
         <input type='text'  size=50 name="fax" value="{$data->fax}">
      </td>
    </tr>    
    
    <tr>
      <td  align='right' valign='top'>{$Description.NameBizID}<span style='color:red;'>*</span></td>
      <td bgcolor="#dfdbdb">
         <textarea name='Name' cols='40' rows='5'>{$data->Name}</textarea>
         <div>��������� ����������� (������: ������� ������ �������.)</div>
         <textarea name='SeoTitle' cols='40' rows='2'>{$data->SeoTitle}</textarea>
      </td>
    </tr>
    
    <tr>
      <td  align='right' valign='top'>����������</td>
      <td bgcolor="#dfdbdb">
      	<span style="color:green;">���������� ����: http://{$smarty.server.SERVER_NAME}/simg/{$data->photo_1}</span>
      	<table>
      		<tr>
      			<td valign="top">
      			    {$data->smallphoto_1}
         			<br>
         			small (172(������) x 116): <input type="file" name="smallphoto_1">
         			<br>
         			
         		</td>
      			<td valign="top">
      			    {$data->middlephoto_1}
         			<br>
         			middle: (300(������) x (����� ��������)) <input type="file" name="middlephoto_1">
         			<br>
         		</td>         		
		 	</tr>
		</table>		 
      </td>
    </tr>
    
    
    <tr>
      <td  align='right' valign='top'>{$Description.SiteID}</td>
      <td bgcolor="#dfdbdb">
      		<table>
      			<tr>
      				<td align="left" width="50%">
            			<div style="color:green;"> ������: �. ������, ��. ������������, �. 8 (���������� ����������� �������) </div>
						<textarea name='Address' cols='40' rows='5'>{$data->Address}</textarea>
					</td>
					<td align="left">
						<div>{$Description.CityID}</div>  
         				<select name="cityId" ID="cityId">
         					<option value="0">������� �����</option>
           						{foreach name=city key=key item=item from=$listCity}
              						<option value="{$item->ID}" {if $data->cityId eq $item->ID}selected{/if} >{$item->ID} {$item->title} -> {$item->parent}</option>
           						{/foreach}
						</select>					
					</td>
				</tr>
			</table>
      </td>
    </tr>    

    <tr>
      <td  align='right' valign='top'>��������� ���������</td>
      <td bgcolor="#dfdbdb">
      		<table>
      			<tr>
      				<td>������: {$data->cost_text}</td>
      			</tr>
      			<tr>
      				<td>
            			<nobr>
							<input type='text'  size=15 name="cost" value="{$data->cost}"> 
							<select style='font-size:9pt;font-family:arial;' name="cCostId"><option value=1 {if $data->cCostId eq "1"}selected{else}{/if}>USD (������ ���)<option value=3 {if $data->cCostId eq "3"}selected{else}{/if}>EUR (����)<option value=2 {if $data->cCostId eq "2"}selected{else}{/if}>RUB (���������� �����)</select>
						</nobr>
					</td>
				</tr>											
			</table>
      </td>
    </tr>    
    
    
    <tr>
      <td  align='right' valign='top'>������� ���������</td>
      <td bgcolor="#dfdbdb">
      		<table>
      			<tr>
      				<td>������: {$data->area_text}</td>
      			</tr>
      			<tr>
      				<td>
						�� <input type='text'  size=15 name="area_from" value="{$data->area_from}"> 
						�� <input type='text'  size=15 name="area_to" value="{$data->area_to}"> 
					</td>
				</tr>
			</table>
      </td>
    </tr>    
    
    <tr>
      <td align='right' valign='top'>��������� ���������</td>
      <td bgcolor="#dfdbdb">
      
      	<select name="RoomCategoryId">
      		<option value="0">---</option>
      		{foreach name=ndataroomcategory key=kdataroomcategory item=idataroomcategory from=$dataroomcategory}
      			<option value="{$idataroomcategory->Id}" {if $data->RoomCategoryId eq $idataroomcategory->Id}selected{/if}>{$idataroomcategory->Name}</option>
      		{/foreach}
      	</select>
      </td>
    </tr>    

    <tr>
      <td align='right' valign='top'>
        ������� ��������<span style='color:red;'>*</span> 
        <div>      
          {include file="./textreplace.tpl" type="ShortDescription"} 
        </div>          
      </td>
      <td bgcolor="#dfdbdb">
     	 			<textarea name="ShortDescription" class='editme'>
      	  				{$data->ShortDescription}
      	 			</textarea> 							
      </td>
    </tr>
    
    <tr>
      <td align='right' valign='top'>
           {$Description.DetailsID}<span style='color:red;'>*</span>
            <div>       
              {include file="./textreplace.tpl" type="Description"} 
            </div>           
      </td>
      <td bgcolor="#dfdbdb">
     	 			<textarea name="Description" class='editme'>
      	  				{$data->Description}
      	 			</textarea> 							
      </td>
    </tr>

    
    <tr>
      <td align='right' valign='top'>{$Description.StatusID}</td>
      <td bgcolor="#dfdbdb">
         <select name='statusId'>
           <option value='0' {if $data->statusId eq 0}selected{/if} >�� �����������</option>
           <option value='1' {if $data->statusId eq 1}selected{/if} >����� ������</option>
           <option value='2' {if $data->statusId eq 2}selected{/if} >������� ������</option>
           <option value='10' {if $data->statusId eq 10}selected{/if} >���������</option>
           <option value='5' {if $data->statusId eq 5}selected{/if} >�����������</option>
           <option value='12' {if $data->statusId eq 12}selected{/if} >������</option>
	   	   <option value='15' {if $data->statusId eq 15}selected{/if} >��������</option>
           <option value='17' {if $data->statusId eq 17}selected{/if} >�����</option> 
         </select>
      </td>
    </tr>
    
    <tr>
      <td align='right' valign='top'>������� Google Maps</td>
      <td bgcolor="#dfdbdb">
        {literal}
            <script language="javascript">
               function googleCoordRequest() {
               	
               		var text_address = document.getElementById("text_addressId");
               		var url = "http://maps.google.com/maps/geo?q="+text_address.value+"&output=xml&key=ABQIAAAAnavXIsM5jdni-p-JoKqdIxTPIkIa5BjXbMz0WvIzZffrVGiazxTRu-mHw0IpP9Ib5fP2129LbcNtTw&oe=utf-8";
               		//window.alert(url);
               		window.open(url);
               }
            </script>
        {/literal}	
         <select name='zoomId'>
          <option {if $data->zoomId eq "1"}selected{else}{/if}>1</option>
          <option {if $data->zoomId eq "2"}selected{else}{/if}>2</option>
          <option {if $data->zoomId eq "3"}selected{else}{/if}>3</option>
          <option {if $data->zoomId eq "4"}selected{else}{/if}>4</option>
          <option {if $data->zoomId eq "5"}selected{else}{/if}>5</option>
          <option {if $data->zoomId eq "6"}selected{else}{/if}>6</option>
          <option {if $data->zoomId eq "7"}selected{else}{/if}>7</option>
          <option {if $data->zoomId eq "8"}selected{else}{/if}>8</option>
          <option {if $data->zoomId eq "9"}selected{else}{/if}>9</option>
          <option {if $data->zoomId eq "10"}selected{else}{/if}>10</option>
          <option {if $data->zoomId eq "11"}selected{else}{/if}>11</option>
          <option {if $data->zoomId eq "12"}selected{else}{/if}>12</option>
          <option {if $data->zoomId eq "13"}selected{else}{/if}>13</option>
          <option {if $data->zoomId eq "14"}selected{else}{/if}>14</option>
          <option {if $data->zoomId eq "15"}selected{else}{/if}>15</option>
         </select> 
         latitude: <input type="text" name="latitude" value="{$data->latitude}">
         longitude: <input type="text" name="longitude" value="{$data->longitude}">
         <br>
         <input type="text" size="50" id="text_addressId" value="{$data->AddressLat}">
         <input type="button" value="��������� ����������" onclick="googleCoordRequest();">
      </td>
    </tr>
   
    <tr>
      <td align='right' valign='top'>��������</td>
      <td bgcolor="#dfdbdb">
      
      	<select name="brokerId">
      		<option value="0">---</option>
      		{foreach name=nname key=kcompanies item=icompanies from=$companies}
      			<option value="{$icompanies->Id}" {if $data->brokerId eq $icompanies->Id}selected{/if}>{$icompanies->companyName}</option>
      		{/foreach}
      	</select>
      </td>
    </tr>     
    
    <tr>
      <td colspan=2 align='right'><input type='submit' value='���������'></td>
    </tr>
    
    
  </table>  
    
</form>  

<br>
<form method="post">
	<input type='hidden' name='updatepayment' value='updatepayment'>
	<input type='hidden' name='ID' value='{$smarty.get.ID}'>

	<table width='100%' bgcolor='#cccccc'>
		<th colspan="2" align="left">[���������� �������� ������]</th>
    	<tr>
      		<td width='30%' align='right' valign='top'>����� �������:</td>
      		<td bgcolor="#dfdbdb">
         		<input type='text' size=50 name="pay_summ" value="{$data->pay_summ}"> 
      		</td>
    	</tr>
    	<tr>
      		<td width='30%' align='right' valign='top'>���� �������:</td>
      		<td bgcolor="#dfdbdb"> 
         		<input type='text' size=50 name="pay_datetime" value="{if strlen($data->pay_datetime) > 1}{$data->pay_datetime}{else}{$data->datecreate}{/if}">  <input type="checkbox" name="pay_datetime_auto">������������ ������� �����
      		</td>
    	</tr> 
    	<tr>
      		<td width='30%' align='right' valign='top'>������ �������:</td>
      		<td bgcolor="#dfdbdb">
         		<select name="pay_status">
         			<option value="0" {if $data->pay_status eq "0"}selected{else}{/if}>�� ��������</option>
         			<option value="1"  {if $data->pay_status eq "1"}selected{else}{/if}>��������</option>
         		</select>
      		</td>
    	</tr>     	   	
    	<tr>
      		<td colspan=2 align='right'><input type='submit' value='���������'></td>
    	</tr>
	</table> 
<br>	

</form>



{else}

	<table border=0 width='100%' bgcolor='#cccccc'>
		<th>�����</th>
		<th width='3%' nowrap>�</th>
		<th>��������</th> 
		<th nowrap>���� ��������</th> 
		<th>������</th>
		<th>ip</th>
		<th>ads</th>
		<th></th>
		<th></th>

		{foreach name=room key=key item=item from=$data}
		
     	<tr {if $item->statusId == 0}bgcolor='#58F5D2' {elseif $item->statusId == 10}bgcolor='#B3B1AE'{elseif $item->statusId == 5}bgcolor='#F0D3D3'{elseif $item->statusId == 15}bgcolor='#ffffff'{elseif $item->statusId == 17}bgcolor='#2F7DAA'{else}bgcolor="{cycle values="#bcd1e3,#e9e9e9"}"{/if}>
       		<td>{$item->pay_summ}</td>
       		<td><a href='http://{$smarty.server.SERVER_NAME}/detailsroom.php?ID={$item->Id}&nocount=nocount' target="_blank">{$item->Id}</a></td>
       		<td>{$item->Name}</td>
       		<td>{$item->datecreate}</td>
       		<td>
         		{if $item->statusId == 0}
           			�� �������
         		{elseif $item->statusId == 1}
           			����� ������
         		{elseif $item->statusId == 2}
           			������� ������
         		{elseif $item->statusId == 5}
           			����������
         		{elseif $item->statusId == 10}
           			���������
         		{elseif $item->statusId == 15}
           			�������� 
         		{elseif $item->statusId == 17}
           			�����
         		{/if} 
       		</td>
       		<td nowrap>
                    {if $item->statusblockip}<span style='color:red;font-weight:bold;'>{/if}{$item->ip}{if $item->statusblockip}</span>{/if}
                    {if $item->statusblockip}</span>{/if} {if !$item->statusblockip}<a href='./UpdateRoom.php?blockip=down&ip={$item->ip}'><img src='http://www.bizzona.ru/predprinimatel/uploads/images/00/00/01/2014/06/01/b4f55479ed.png' border='0' style='margin:2pt;' alt="�����������" title="�����������"></a>{/if} {if $item->statusblockip}<a href='./UpdateRoom.php?blockip=up&ip={$item->ip}'><img src='http://www.bizzona.ru/predprinimatel/uploads/images/00/00/01/2014/06/01/c6dbc52357.png' border='0' style='margin:2pt;' title="��������������" alt="��������������"></a>{/if}           
                </td>
       		<td>{$item->partnerid}</td>
       		<td><a href='./UpdateRoom.php?ID={$item->Id}'>�������������</a></td>
       		<td>
				<a href="./template.php?typeeventobjectId=6&objectId={$item->Id}&repeat=1"><img src='../images/trueNotifyPublic.gif' border=0></a>
       		</td>
       		<td>
       			<a href="./template.php?typeeventobjectId=6&objectId={$item->Id}"><img src="../sicon/emailhistory.png" border="0" title="������� ����������">
       		</td>       		
	   		
     	</tr>
		
		
  		{/foreach}
  
  </table>
{/if}