{literal}
<!-- Place inside the <head> of your HTML -->
<script type="text/javascript" src="http://{/literal}{$smarty.server.SERVER_NAME}{literal}/engine/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
tinymce.init({
    selector: "textarea.editme", 
    theme: "modern",
    language : 'ru',
    height : 300, 
    plugins: [ 
         "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
         "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
         "save table contextmenu directionality emoticons template paste textcolor"
   ],
   content_css: "css/content.css",
   toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | l      ink image | print preview media fullpage | forecolor backcolor emoticons", 
   style_formats: [
        {title: 'Bold text', inline: 'b'},
        {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
        {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
        {title: 'Example 1', inline: 'span', classes: 'example1'},
        {title: 'Example 2', inline: 'span', classes: 'example2'},
        {title: 'Table styles'},
        {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
    ]    
 });
</script>
{/literal}


{literal}
	<script>
    	$(function() {
        	$( "#datetime" ).datepicker({
            changeMonth: true,
            changeYear: true
        });
        $.datepicker.setDefaults($.datepicker.regional['']);
        $("#datetime").datepicker();
        });
    </script>
{/literal}


{if isset($item)}
<table width="100%">
<tr>
<td align="left" width="50%"><h3>����� ��� �������������� ������� - � {$item.Id}</h3></td>
<td align="right"><a href="http://{$smarty.server.SERVER_NAME}/engine/news.php" style="font-size:15px;font-weight:bold;background-color:#e2f6ee">������� ����� ������</a></td>
</table>
{else}
<h3>����� ��� ���������� ����� �������</h3>
{/if}



<h3></h3> 
<form method="post" style="margin-top:0pt;margin-bottom:0pt;" enctype="multipart/form-data">

<table  width='100%' bgcolor='#cccccc'>
	<tr>
		<td width="15%" align="left"  style="padding:15px;">������������:</td>
		<td width="35%" align="left"  bgcolor="#dfdbdb" align="center"  style="padding:15px;">
			<textarea name="title" rows="3" style="width:95%;">{if isset($item)}{$item.title}{/if}</textarea>
		</td> 
		<td width="15%" align="left" style="padding:15px;">������� ��������:</td>
		<td width="35%" bgcolor="#dfdbdb" align="center" style="padding:15px;">
			<textarea name="shorttext" rows="5" style="width:95%;">{if isset($item)}{$item.shorttext}{/if}</textarea>
		</td>
	</tr>
	<tr>
		<td colspan="4" align="left" style="padding:15px;">
		��������� ��������:
		</td>
	</tr>
	<tr>
		<td colspan="4" align="center" style="padding:15px;" bgcolor="#dfdbdb">
     	 			<textarea name="detailstext" class='editme'>
      	  				{php}
      	  					global $item;			 
      	  					echo $item['detailstext'];
      	  				{/php}
      	 			</textarea> 									
		</td>
	</tr>
	<tr>
		<td width="15%" align="left"  style="padding:15px;">���� ���������:</td>
		<td width="35%" align="left"  bgcolor="#dfdbdb" align="center"  style="padding:15px;">
			<input type="text" name="datetime" id="datetime" value="{if isset($item)}{$item.datetimeStr}{else}{/if}">
		</td>
		<td width="15%" align="left"  style="padding:15px;">������ ����������:</td>
		<td width="35%" align="left"  bgcolor="#dfdbdb" align="center"  style="padding:15px;">
			<select name="statusId">
			{foreach name=n key=k item=i from=$liststatus}
				<option value="{$i.id}" {if isset($item) && $item.statusId == $i.id}selected{else}{/if}>{$i.name}</option>
			{/foreach}			
			</select>
		</td> 
	</tr>	
	<tr>
		<td colspan="4" align="right" bgcolor="#dfdbdb" style="padding:15px;">
			<input type="submit" value="���������">
		</td>
	</tr>
</table>
{if isset($item)}
	<input type="hidden" name="update" value="update">
{else}
	<input type="hidden" name="add" value="add">
{/if}

</form>