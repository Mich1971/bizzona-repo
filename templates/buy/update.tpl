{literal}
<!-- Place inside the <head> of your HTML -->
<script type="text/javascript" src="http://{/literal}{$smarty.server.SERVER_NAME}{literal}/engine/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
tinymce.init({
    selector: "textarea.editme", 
    theme: "modern",
    language : 'ru',
    height : 300, 
    plugins: [
         "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
         "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
         "save table contextmenu directionality emoticons template paste textcolor"
   ],
   content_css: "css/content.css",
   toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | l      ink image | print preview media fullpage | forecolor backcolor emoticons", 
   style_formats: [
        {title: 'Bold text', inline: 'b'},
        {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
        {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
        {title: 'Example 1', inline: 'span', classes: 'example1'},
        {title: 'Example 2', inline: 'span', classes: 'example2'},
        {title: 'Table styles'},
        {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
    ]    
 });
</script>
{/literal}


{if $smarty.get.ID}

<form method="POST" style="margin-top:0pt;">

	<input type='hidden' name='ID' value='{$smarty.get.ID}'>

<table width="100%" align="center" bgcolor="#cccccc">

	<tr>
		<td align='left' bgcolor="red" nowrap>
			<a href="../detailsBuy.php?ID={$smarty.get.ID}&nocount=nocount" target="_blank"  style='color:white;font-weight:bold;' >������� ����������</a>
		</td>
    	<td bgcolor="#dfdbdb" align='right' style='font-size:9pt;color:green;'>
          <a href="./template.php?typeeventobjectId=2&objectId={$smarty.get.ID}&repeat=1"><img src='../images/trueNotifyPublic.gif' border=0></a>          
    	</td>		
	</tr>


	<tr>
		<td colspan="2" align="center" class="desc_td"><span style="font-size:10pt;font-family:arial;color:red">{$text_error}</span></td>
	</tr>
	<tr>
		<td colspan="2" align="center" class="desc_td"><span style="font-size:10pt;font-family:arial;color:green">{$text_success}</span></td>
	</tr>
	<tr>
		<td width="25%"  style="padding-left:5pt;"><div style="font-size:10pt;font-family:arial;">�������� �������</div></td>
		<td class="val_td" bgcolor="#dfdbdb">
			<input type="text" name="nameID" style="width:80%;" value="{$data->name}">
         	<div>��������� ����������� (������: ����� ������ �������.)</div>
         	<textarea name='subtitle' cols='40' rows='2'>{$data->subtitle}</textarea>			
		</td>
	</tr>
	<tr>
		<td width="25%" style="padding-left:5pt;"><div style="font-size:10pt;font-family:arial;">��� �������</div></td>
		<td class="val_td" bgcolor="#dfdbdb" >
        {if $data->subTypeBizID > 0}
          <select name="categoryID">
          	<option value="0">������� ���������</option>
             {foreach name='category' item=item key=key from=$listCategory}
               <optgroup label="{$item->Name}" >
               {foreach name='subcategory' item=subitem key=subkey from=$listSubCategory}
             	  {if $item->ID eq $subitem->CategoryID}
             	  <option value="{$item->ID}:{$subitem->ID}" {if $data->typeBizID eq $item->ID and $subitem->ID eq $data->subTypeBizID}selected{/if} >{$subitem->Name}</option>
             	  {/if}
               {/foreach}
               </optgroup>
             {/foreach}
           </select>        
        {else}
          <select name="categoryID">
          	<option value="0">������� ���������</option>
             {foreach name='category' item=item key=key from=$listCategory}
               <optgroup label="{$item->Name}" >
               {foreach name='subcategory' item=subitem key=subkey from=$listSubCategory}
             	  {if $item->ID eq $subitem->CategoryID}
             	  <option value="{$item->ID}:{$subitem->ID}" {if $data->typeBizID eq $item->ID and $subitem->enabled eq '1'}selected{/if} >{$subitem->Name}</option>
             	  {/if}
               {/foreach}
               </optgroup>
             {/foreach}
           </select>
         {/if}

		</td>
	</tr>
	<tr>
		<td style="padding-left:5pt;"><div style="font-size:10pt;font-family:arial;">���������</div></td>
		<td class="val_td"  bgcolor="#dfdbdb">
			<table>
				<tr>
					<td><div style="font-size:10pt;font-family:arial;">��</div></td>
					<td><input type="text" name="cost_startID" value="{$data->cost_start}"></td>
					<td rowspan="2">
						<select name="currencyID" style='font-size:9pt;font-family:arial;'>
							<option value='usd' {if $data->currencyID eq "usd"} selected {/if}>USD (������ ���)
							<option value='eur' {if $data->currencyID eq "eur"} selected {/if}>EUR (����)
							<option value='rub' {if $data->currencyID eq "rub"} selected {/if}>RUB (���������� �����)
						</select>
					</td>
				</tr>
				<tr>
					<td><div style="font-size:10pt;font-family:arial;">��</div></td>
					<td><input type="text" name="cost_stopID" value="{$data->cost_stop}"></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td style="padding-left:5pt;"><div style="font-size:10pt;font-family:arial;">����</div></td>
		<td class="val_td"  bgcolor="#dfdbdb"><input type="text" name="partID" value="{$data->part}"></td>
	</tr>
	<tr>
		<td style="padding-left:5pt;"><div style="font-size:10pt;font-family:arial;">������������</div></td>
		<td class="val_td"  bgcolor="#dfdbdb">
			<input type="text" name="regionTextID" style="width:80%;" value="{$data->regionText}">
			<div><i>������ ������ ��: {$whois_city}, {$whois_region}</i></div>
			<table>
				<tr>
					<td valign="top">
         				<select name="CityID" onchange='{php}global $sDistrict; $sDistrict->printScript();{/php}'>
					<option value="0">������� �����</option>
           				{foreach name=city key=key item=item from=$listCity}
            				<option value="{$item->ID}" {if $data->regionID eq $item->ID}selected{/if} >{$item->ID} {$item->title}</option> 
           				{/foreach}
         				</select>
         				<div id="subRegionDiv" style="padding-top:5pt;padding-bottom:5pt;">&#160;</div>
					</td>
         			<td>
         				<div id="districtDiv">&#160;</div>
					</td>
					<td>
						<div id="metroDiv">&#160;</div>
					</td>
				</tr>
			</table>
		</td>
	</tr>

	<tr>
		<td style="padding-left:5pt;" valign="top">
                    <div style="font-size:10pt;font-family:arial;">������� ��������</div>
                    <div>   
                      {include file="./textreplace.tpl" type="otherinfoID"} 
                    </div>                                             
                </td>
		<td class="val_td"  bgcolor="#dfdbdb">
			<table width="100%"> 
				<tr>
					<td colspan="2">
                                              <div style="font-size:10pt;font-family:arial;">������� ��������</div>
                                        </td>
				</tr>
				<tr>
					<td colspan="2">
      	 			<textarea name="otherinfoID" class='editme'>
      	  				{$data->otherinfo}
      	 			</textarea> 
					</td>
				</tr>
				<tr>
					<td><div style="font-size:10pt;font-family:arial;">������� �������</div></td>
					<td><input type="text" name="ageID" value="{$data->age}"></td>
				</tr>
				<tr>
					<td><div style="font-size:10pt;font-family:arial;">���� �����������</div></td>
					<td><input type="text" name="paybackID" value="{$smarty.post.paybackID}"></td>
				</tr>
				<tr>
					<td><div style="font-size:10pt;font-family:arial;">�����������, ��������</div></td>
					<td><input type="text" name="licenseID" style="width:80%;" value="{$data->license}"></td>
				</tr>
			</table>
		</td>
	</tr>
	
	<tr>
		<td style="padding-left:5pt;" valign="top">
                    <div style="font-size:10pt;font-family:arial;">�������� ��������</div>
                    <div>   
                      {include file="./textreplace.tpl" type="realtyID"} 
                    </div>                                             
                </td>
		<td class="val_td"  bgcolor="#dfdbdb">
			<table width="100%">
				<tr>
					<td><div style="font-size:10pt;font-family:arial;">������������</div></td>
				</tr>
				<tr>
					<td>
      	 			<textarea name="realtyID" class='editme'>
      	  				{$data->realty}
      	 			</textarea> 
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td style="padding-left:5pt;" valign="top">
                    <div style="font-size:10pt;font-family:arial;">������ ��������</div>
                    <div>    
                      {include file="./textreplace.tpl" type="descriptionID"} 
                    </div>                    
                </td>
		<td class="val_td"  bgcolor="#dfdbdb">
			<table width="100%">
				<tr>
					<td><div style="font-size:10pt;font-family:arial;">������ ��������</div></td>
				</tr>
				<tr>
					<td>
      	 			<textarea name="descriptionID" class='editme'>
      	  				{$data->description}
      	 			</textarea> 
					</td>
				</tr>
			</table>
		</td>
	</tr>	
	

	<tr>
		<td style="padding-left:5pt;"><div style="font-size:10pt;font-family:arial;">���������� ����������</div></td>
		<td class="val_td"  bgcolor="#dfdbdb">
			<table width="100%">
				<tr>
					<td width="25%"><div style="font-size:10pt;font-family:arial;">���������� ����</div></td>
					<td><input type="text" name="FMLID" style="width:80%;" value="{$data->FML}"></td>
				</tr>
				<tr>
					<td><div style="font-size:10pt;font-family:arial;">��������</div></td>
					<td><input type="text" name="phoneID" style="width:80%;" value="{$data->phone}"></td>
				</tr>
				<tr>
					<td><div style="font-size:10pt;font-family:arial;">Email</div></td>
					<td><input type="text" name="emailID" value="{$data->email}"></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td style="padding-left:5pt;"><div style="font-size:10pt;font-family:arial;">������������ ����������</div></td>
		<td class="val_td"  bgcolor="#dfdbdb">
			<select name="durationID">
				<option value="1" {if $data->duration eq "1"}selected{else}{/if}>1 �����</option>
				<option value="2" {if $data->duration eq "2"}selected{else}{/if}>2 ������</option>
				<option value="3" {if $data->duration eq "3"}selected{else}{/if}>3 ������</option>
			</select>
		</td>
	</tr>
	<tr>
		<td style="padding-left:5pt;"><div style="font-size:10pt;font-family:arial;">������</div></td>
		<td class="val_td"  bgcolor="#dfdbdb">
			<select name="statusID">
				<option value="0" {if $data->statusID eq "0"}selected{else}{/if}>�����</option>
				<option value="1" {if $data->statusID eq "1"}selected{else}{/if}>������</option>
				<option value="2" {if $data->statusID eq "2"}selected{else}{/if}>������</option>
				<option value="3" {if $data->statusID eq "3"}selected{else}{/if}>����������</option>
	  			<option value="15" {if $data->statusID eq "15"}selected{/if} >��������</option>
			        <option value="17" {if $data->statusID eq "17"}selected{/if} >�����</option>

			</select>
		</td>
	</tr>
	<tr>
		<td style="padding-left:5pt;"><div style="font-size:10pt;font-family:arial;">�������� �� Email �����������</div></td>
		<td class="val_td"  bgcolor="#dfdbdb">
			<input type="checkbox" name="subscribe_status_id" {if $data->subscribe_status_id eq "1"}checked{else}{/if} >
		</td>
	</tr>
	 
    <tr>
      <td align='right' valign='top'>��������</td>
      <td bgcolor="#dfdbdb">
      
      	<select name="brokerId">
      		<option value="0">---</option>
      		{foreach name=nname key=kcompanies item=icompanies from=$companies}
      			<option value="{$icompanies->Id}" {if $data->brokerId eq $icompanies->Id}selected{/if}>{$icompanies->companyName}</option>
      		{/foreach}
      	</select>
      </td>
    </tr>
	
	
	<tr>
		<td colspan="2" align="right"><input type="submit" value="���������"  style="font-size:10pt;font-family:arial;"></td>
	</tr>
	
</table>

</form>

<br>
<form method="post">
	<input type='hidden' name='updatepayment' value='updatepayment'>
	<input type='hidden' name='ID' value='{$smarty.get.ID}'>

	<table width='100%' bgcolor='#cccccc'>
		<th colspan="2" align="left">[���������� �������� ������]</th>
    	<tr>
      		<td width='30%' align='right' valign='top'>����� �������:</td>
      		<td bgcolor="#dfdbdb">
         		<input type='text' size=50 name="pay_summ" value="{$data->pay_summ}"> 
      		</td>
    	</tr>
    	<tr>
      		<td width='30%' align='right' valign='top'>���� �������:</td>
      		<td bgcolor="#dfdbdb"> 
         		<input type='text' size=50 name="pay_datetime" value="{if strlen($data->pay_datetime) > 1}{$data->pay_datetime}{else}{$data->DateStart}{/if}"> <input type="checkbox" name="pay_datetime_auto">������������ ������� �����
      		</td>
    	</tr> 
    	<tr>
      		<td width='30%' align='right' valign='top'>������ �������:</td>
      		<td bgcolor="#dfdbdb">
         		<select name="pay_status">
         			<option value="0" {if $data->pay_status eq "0"}selected{else}{/if}>�� ��������</option>
         			<option value="1"  {if $data->pay_status eq "1"}selected{else}{/if}>��������</option>
         		</select>
      		</td>
    	</tr>     	   	
    	<tr>
      		<td colspan=2 align='right'><input type='submit' value='���������'></td>
    	</tr>
	</table> 
<br>	

</form>



{else}
  <table border=0 width='100%' bgcolor='#cccccc'>
  <th>�����</th>
  <th width='3%' nowrap>�</th>
  <th>��������</th> 
  <th nowrap>���� ��������</th>
  <th nowrap>���� ��������</th>
  <th>������</th> 
  <th>ip</th>
  <th>ads</th>
  <th></th>
  <th></th>
  <th></th>
    {foreach name=sell key=key item=item from=$data}
     <tr  {if $item->statusID == 0}bgcolor='#58F5D2' {elseif $item->statusID == 10}bgcolor='#B3B1AE'{elseif $item->statusID == 5}bgcolor='#F0D3D3'{elseif $item->statusID == 15}bgcolor='#ffffff'{elseif $item->statusID == 17}bgcolor='#2F7DAA'{else}bgcolor="{cycle values="#bcd1e3,#e9e9e9"}"{/if}  >
       <td>{$item->pay_summ}</td>
       <td>{$item->id}</td>
       <td><a href='../detailsBuy.php?ID={$item->id}&nocount=nocount' target="_blank" style='text-decoration:none;'>{$item->name}</a></td>
       <td>{$item->datecreate}</td>
       <td>{$item->dateclose}</td>
       <td style="font-size:10pt;font-family:arial;" nowrap>
         {if $item->statusID == 0}
           �����
         {elseif $item->statusID == 1}
           ����� ������
         {elseif $item->statusID == 3}
           �����������
         {elseif $item->statusID == 2}
           ���������
         {elseif $item->statusID == 15}
           �������� 
         {elseif $item->statusID == 17}
           �����
         {/if} 
       </td>
       <td nowrap>
           {if $item->statusblockip}<span style='color:red;font-weight:bold;'>{/if}{$item->ip}{if $item->statusblockip}</span>{/if}
           {if $item->statusblockip}</span>{/if} {if !$item->statusblockip}<a href='./UpdateBuyer.php?blockip=down&ip={$item->ip}'><img src='http://www.bizzona.ru/predprinimatel/uploads/images/00/00/01/2014/06/01/b4f55479ed.png' border='0' style='margin:2pt;' alt="�����������" title="�����������"></a>{/if} {if $item->statusblockip}<a href='./UpdateBuyer.php?blockip=up&ip={$item->ip}'><img src='http://www.bizzona.ru/predprinimatel/uploads/images/00/00/01/2014/06/01/c6dbc52357.png' border='0' style='margin:2pt;' title="��������������" alt="��������������"></a>{/if}           
           <div>{$item->defaultUserCountry}</div>
       </td>
       <td>{$item->partnerid}</td>

       <td><a href='./UpdateBuyer.php?ID={$item->id}'>�������������</a></td>
       <td>
          <a href="./template.php?typeeventobjectId=2&objectId={$item->id}&repeat=1"><img src='../images/trueNotifyPublic.gif' border=0></a>         
       </td>
       <td>
       <a href="./template.php?typeeventobjectId=2&objectId={$item->id}"><img src="../sicon/emailhistory.png" border="0" title="������� ����������">
       </td>
     </tr>

    {/foreach}
  </table> 
{/if}

