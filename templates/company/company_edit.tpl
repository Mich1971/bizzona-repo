{literal}
<!-- Place inside the <head> of your HTML -->
<script type="text/javascript" src="http://{/literal}{$smarty.server.SERVER_NAME}{literal}/engine/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
tinymce.init({
    selector: "textarea.editme", 
    theme: "modern",
    language : 'ru',
    height : 300, 
    plugins: [
         "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
         "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
         "save table contextmenu directionality emoticons template paste textcolor"
   ],
   content_css: "css/content.css",
   toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | l      ink image | print preview media fullpage | forecolor backcolor emoticons", 
   style_formats: [
        {title: 'Bold text', inline: 'b'},
        {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
        {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
        {title: 'Example 1', inline: 'span', classes: 'example1'},
        {title: 'Example 2', inline: 'span', classes: 'example2'},
        {title: 'Table styles'},
        {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
    ]    
 });
</script>
{/literal}


<script type="text/javascript" src="http://{$smarty.server.SERVER_NAME}/js/jquery-1.7.2.min.js" ></script>
<script type="text/javascript" src="http://{$smarty.server.SERVER_NAME}/js/jquery.ui.core.js"></script>
<script type="text/javascript" src="http://{$smarty.server.SERVER_NAME}/js/jquery.ui.datepicker.js"></script>
<script type="text/javascript" src="http://{$smarty.server.SERVER_NAME}/js/jquery.ui.datepicker-ru.js"></script>
<script type="text/javascript" src="http://{$smarty.server.SERVER_NAME}/js/jquery.dataTables.js"></script>

<LINK href="http://{$smarty.server.SERVER_NAME}/css/jquery-ui.css" type="text/css" rel="stylesheet">
<LINK href="http://{$smarty.server.SERVER_NAME}/css/demo_table.css" type="text/css" rel="stylesheet">


{literal}
	<script>
    	$(function() {
        	$( "#startDate" ).datepicker({
            changeMonth: true,
            changeYear: true
        });
        $.datepicker.setDefaults($.datepicker.regional['']);
        $("#startDate").datepicker();
        });
        
        
    	$(function() {
        	$( "#stopDate" ).datepicker({
            changeMonth: true,
            changeYear: true
        });
         
        $.datepicker.setDefaults($.datepicker.regional['']);
        $("#stopDate").datepicker();
        }); 

        
    	$(function() {
        	$( "#auto_update_datestart" ).datepicker({
            changeMonth: true,
            changeYear: true
        });
         
        $.datepicker.setDefaults($.datepicker.regional['']);
        $("#auto_update_datestart").datepicker();
        }); 

    	$(function() {
        	$( "#auto_update_datestop" ).datepicker({
            changeMonth: true,
            changeYear: true
        });
         
        $.datepicker.setDefaults($.datepicker.regional['']);
        $("#auto_update_datestop").datepicker();
        });               
    </script>
{/literal} 


<table width='100%' cellpadding="0" cellspacing="0" style='border-top: 1px; border-bottom: 2px; border-left:1px;  border-right:1px;  border-color:#006699;  border-style: solid;'>
  <tr style="background-color:darkgreen ">
  	<td  width='10%' align='left' nowrap><a href="http://{$smarty.server.SERVER_NAME}/inside/" style="color:white;font-weight:bold;">�������</a></td>
  	<td  width='10%' align='left' nowrap><a href="http://{$smarty.server.SERVER_NAME}{$smarty.server.SCRIPT_NAME}" style="color:white;font-weight:bold;">����� ������</a></td>
  	<td width="80%">&nbsp;</td>
  </tr>
</table>

{if isset($error)}
<div style="margin:4px;padding:4px;border:3px #c3c3c3 solid;background-color:#ecf8ec ">
<ol style="color:red;">
{foreach from=$error key=k item=item}
<li>{$item}</li>
{/foreach}
</ol>
</div>
{/if}

{include file='./company/infobloker.tpl'}

<form method="POST"  action="http://{$smarty.server.SERVER_NAME}/engine/UpdateCompany.php?ID={$smarty.get.ID}" style="margin-top:0pt;margin-bottom:0pt;" enctype="multipart/form-data">

 <input type='hidden' name='ID' value='{$smarty.get.ID}'>

<table width='100%' align="center" bgcolor="#cccccc">
	<tr {if strlen($text_error) > 0 }{else}style="display:none;"{/if}>
		<td colspan="2" align="center" class="desc_td"><span style="font-size:10pt;font-family:arial;color:red">{$text_error}</span></td>
	</tr>
	<tr {if strlen($text_success) > 0 }{else}style="display:none;"{/if}>
		<td colspan="2" align="center" class="desc_td"><span style="font-size:10pt;font-family:arial;color:green">{$text_success}</span></td>
	</tr>
	<tr>
		<td width="30%" align='right' valign='top'>�������� ��������:</td>
		<td bgcolor="#dfdbdb">
			<textarea name="companyName" style="width:100%" rows="3">{$data->companyName}</textarea>
			<br>
			������������ ��� ��������� �����������
			<br>
			<input type="text" value="{$data->alt_seo_text}" style="width:50%" name="alt_seo_text">
		</td>
	</tr>
	
	<tr>
		<td width="30%" align='right' valign='top'>��� ��������:</td>
		<td bgcolor="#dfdbdb">
			<select name="type_company">
				<option value="0">�������</option>
				<option value="1" {if $data->type_company == "1"}selected{else}{/if}>������</option>
				<option value="2" {if $data->type_company == "2"}selected{else}{/if}>��������</option>
			</select>		
		</td>
	</tr>	
	
	<tr>
		<td width="30%" align='right' valign='top'>������� ������������ ��������:</td>
		<td bgcolor="#dfdbdb">
			<input type="text" value="{$data->prefix_name_company}" style="width:30%" name="prefix_name_company">		
			<br>
			(3-4 ���. �����, ������������ � ������� ��������� �� �������� ���������)
		</td>
	</tr>
	
	<tr>
		<td width="30%" align='right' valign='top'>����������� ����� ��������:</td>
		<td bgcolor="#dfdbdb">
			<textarea name="legalAddress" style="width:100%" rows="2">{$data->legalAddress}</textarea>
		</td>
	</tr>

	<tr>
		<td width="30%" align='right' valign='top'>����������� ����� ��������:</td>
		<td bgcolor="#dfdbdb">
			<textarea name="actualAddress" style="width:100%" rows="2">{$data->actualAddress}</textarea>
		</td>
	</tr>

	<tr>
		<td width="30%" align='right' valign='top'>������� ��������:</td>
		<td bgcolor="#dfdbdb">
			<input type="text" value="{$data->phoneCompany}" style="width:50%" name="phoneCompany">		
		</td>
	</tr>

	<tr>
		<td width="30%" align='right' valign='top'>���� ��������:</td>
		<td bgcolor="#dfdbdb">
			<input type="text" value="{$data->faxCompany}" style="width:30%" name="faxCompany">		
		</td>
	</tr>
	
	<tr>
		<td width="30%" align='right' valign='top'>Email:</td>
		<td bgcolor="#dfdbdb">
			<input type="text" value="{$data->email}" style="width:30%" name="emailCompany">		
		</td>
	</tr>	
	
	<tr>
		<td width="30%" align='right' valign='top'>����� �����:</td>
		<td bgcolor="#dfdbdb">
			<input type="text" value="{$data->site_company}" style="width:30%" name="site_company">		
		</td>
	</tr>	
	
	<tr>
		<td width="30%" align='right' valign='top'>�����:</td>
		<td bgcolor="#dfdbdb">
			<input type="text" value="{$data->login}" style="width:30%" name="login">		
		</td>
	</tr>	
	
	<tr>
		<td width="30%" align='right' valign='top'>������:</td>
		<td bgcolor="#dfdbdb">
			<input type="text" value="{$data->password}" style="width:30%" name="password">
		</td>
	</tr>	
	
	<tr>
		<td width="30%" align='right' valign='top'>������������ �����:</td>
		<td bgcolor="#dfdbdb">
			<input type="text" value="{$data->nameBank}" style="width:30%" name="nameBank">		
		</td>
	</tr>

	<tr>
		<td width="30%" align='right' valign='top'>�/� �����:</td>
		<td bgcolor="#dfdbdb" nowrap>
			<input type="text" value="{$data->ksBank}" style="width:30%" name="ksBank">		
			�/� �����:
			<input type="text" value="{$data->rsBank}" style="width:30%" name="rsBank">		
			��� �����:
			<input type="text" value="{$data->bikBank}" style="width:30%" name="bikBank">
		</td>
	</tr>

	<tr>
		<td width="30%" align='right' valign='top'>��� ��������:</td>
		<td bgcolor="#dfdbdb">
			<input type="text" value="{$data->innCompany}" style="width:30%" name="innCompany">		
			��� ��������:
			<input type="text" value="{$data->kppCompany}" style="width:30%" name="kppCompany">		
		</td>
	</tr>
	
	<tr>
		<td width="30%" align='right' valign='top'>������ ��������:</td>
		<td bgcolor="#dfdbdb" nowrap>
			<select name="show_in_partner">
				<option value="0">�������</option>
				<option value="1" {if $data->show_in_partner == "1"}selected{else}{/if}>�����������</option>
				<option value="2" {if $data->show_in_partner == "2"}selected{else}{/if}>�� �����������</option>
			</select>
			{*
			�����������:
			<input type="text" value="{$data->order_num}" style="width:30;" name="order_num">
			*}
			�����������: <input type="checkbox" name="accessId" {if $data->accessId ==2 }checked{else}{/if}>
		</td>
	</tr>	
	
	
	<tr>
		<td width="30%" align='right' valign='top'>�����:</td>
		<td bgcolor="#dfdbdb">
			<table>
			<tr><td valign="top">
			
			<select name="tariffId">
					<option value="0">�������</option>
				{foreach name=n key=k item=i from=$s_datatariff}
					<option value="{$i.id}" {if $data->tariffId == $i.id}selected{else}{/if}>{$i.shortName}</option>
				{/foreach}  
			</select>			
			
			
			</td>
			<td valign="top" nowrap>
			����������: c: <input type="text" name="datestart" id="startDate" value="{if isset($data)}{$data->datestartStr}{else}{/if}">
			��: <input type="text" name="datestop"  id="stopDate" value="{if isset($data)}{$data->datestopStr}{else}{/if}">
			</td>
			</tr>
			</table>
	</tr>	
	
	
	<tr>
		<td align='right' valign='top'>��������� ����������:</td>
		<td  bgcolor="#dfdbdb">
			<table width="100%">
				<tr>
					<td width="30%" style="vertical-align:top;">
						������������� ���������: <input type="checkbox" name="auto_update" {if $data->auto_update ==1 }checked{else}{/if}>
					</td>
					<td align="left">
						<table width="100%" bgcolor="#e7e5e5">
							<tr>
								<td align="left" nowrap colspan="2">
									�������: 
								    � <input type="text" name="auto_update_datestart" id="auto_update_datestart" value="{if isset($data)}{$data->auto_update_datestartStr}{else}{/if}"> �� <input type="text" name="auto_update_datestop" id="auto_update_datestop" value="{if isset($data)}{$data->auto_update_datestopStr}{else}{/if}">
								</td>
							</tr>
							<tr>
								<td align="left" nowrap>
									������� �������� (� ����): <input type="text" name="auto_update_period" value="{if isset($data)}{$data->auto_update_period}{else}7{/if}" size="5">
								</td>
								<td align="left" nowrap>
									���-�� ���������� �� 1 ��������: <input type="text" name="count_per_update" value="{if isset($data)}{$data->count_per_update}{else}1{/if}" size="5">
								</td>                                                                
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>

        <tr>
            <td align='right' valign='top'>��������� �������:</td>
            <td bgcolor="#dfdbdb">
                <table width="100%"> 
                    <tr>
                        <td colspan="2">
                            ��������� ������ ������� ����������<hr>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            ������ � ������ ��������: 
                            <input type="checkbox" name="access_call_robot" {if $data->access_call_robot ==1 }checked{else}{/if}>
                        </td> 
                        <td align="left" nowrap>
                            ����� �������:  
                            <input type="text" size="50" name="url_import_robot" value="{if isset($data)}{$data->url_import_robot}{else}{/if}">
                        </td>
                    </tr>
                    
                </table>
            </td>
        </tr>
        
	
	<tr>
		<td align='right' valign='top'>�������� �������� (������ ��������):</td>
		<td bgcolor="#dfdbdb">
     	 			<textarea name="shortDescription" class='editme'>
      	  				{$data->shortDescription}
      	 			</textarea> 									
		</td>
	</tr>
	
	<tr>
		<td align='right' valign='top'>���� ������ ������ (������ ��������):</td>
		<td bgcolor="#dfdbdb">
     	 			<textarea name="block_links" class='editme'>
      	  				{$data->block_links}
      	 			</textarea> 									
		</td>
	</tr>	
	

	<tr>
		<td align='right' valign='top'>���� �������� (������ ��������):</td>
		<td bgcolor="#dfdbdb">
			<table>
			 <tr><td valign="top">
			<input type="file" name="logo_partner">
			<td><td valign="top">
			{if strlen($data->logo_partner) >0}<img src="http://{$smarty.server.SERVER_NAME}/imgbroker/{$data->logo_partner}" border="0" >{/if}
			</td></tr>
			</table>
		</td>
	</tr>	
	
	<tr>
		<td align='right' valign='top'>���� �������� (������ ����������):</td>
		<td bgcolor="#dfdbdb">
		    <table>
		    <tr><td valign="top">
			<input type="file" name="logo_in_list">
			</td><td valign="top">
			{if strlen($data->logo_in_list) >0}<img src="http://{$smarty.server.SERVER_NAME}/imgbroker/{$data->logo_in_list}" border="0" >{/if}
			</td></tr>
			</table>
		</td>
	</tr>	
	
	<tr>
		<td align='right' valign='top'>���� �������� <br>(������ ��������� ���������� �������� ��� ��������� �������� ���������� ����������):</td>
		<td bgcolor="#dfdbdb">
			<table>
			<tr><td valign="top">
			<input type="file" name="logo_in_sublist">
			</td><td valign="top">
			{if strlen($data->logo_in_sublist) >0}<img src="http://{$smarty.server.SERVER_NAME}/imgbroker/{$data->logo_in_sublist}" border="0" >{/if}
			</td></tr>
			</table>
		</td>
	</tr>	
	
	<tr>
		<td colspan="2" align="left" style="font-weight:bold;" bgcolor="#d2e0e3">���������� ��� ��������� ������ � �������� ����� ���� �� ������ <a href="http://{$smarty.server.SERVER_NAME}/broker.php" target="_blank">��������</a></td>
	</tr>

	<tr>
		<td align="right" bgcolor="#d2e0e3">� ��������</td>
		<td bgcolor="#d2e0e3">
     	 			<textarea name="about_forsite" class='editme'>
      	  				{$data->about_forsite}
      	 			</textarea> 									
		</td>
	</tr>

	<tr>
		<td align="right" bgcolor="#d2e0e3">������</td>
		<td bgcolor="#d2e0e3">
     	 			<textarea name="service_forsite" class='editme'>
      	  				{$data->service_forsite}
      	 			</textarea> 									
		</td>
	</tr>
	
	<tr>
		<td align="right" bgcolor="#d2e0e3">��������</td>
		<td bgcolor="#d2e0e3">
     	 			<textarea name="contact_forsite" class='editme'>
      	  				{$data->contact_forsite}
      	 			</textarea> 									
		</td>
	</tr>

	<tr>
		<td align="right" bgcolor="#d2e0e3">������� ������������</td>
		<td bgcolor="#d2e0e3">
			<input type="text" name="NameForSite" value="{$data->NameForSite}" style="width:50%">
		</td>
	</tr>

	
	
	<tr>
		<td colspan="2" align="right"><input type="submit" value="���������"  style="font-size:10pt;font-family:arial;"></td>
	</tr>
	
</table>

</form>

<table width='100%' cellpadding="0" cellspacing="0" style='border-top: 1px; border-bottom: 2px; border-left:1px;  border-right:1px;  border-color:#006699;  border-style: solid;'>
  <tr style="background-color:darkgreen ">
  	<td  width='10%' align='left' nowrap><a href="http://{$smarty.server.SERVER_NAME}/inside/" style="color:white;font-weight:bold;">�������</a></td>
  	<td  width='10%' align='left' nowrap><a href="http://{$smarty.server.SERVER_NAME}{$smarty.server.SCRIPT_NAME}" style="color:white;font-weight:bold;">����� ������</a></td>
  	<td width="80%">&nbsp;</td>
  </tr>
</table>

<div width='100%' cellpadding="0" cellspacing="0" style='border-top: 1px; border-bottom: 2px; border-left:1px;  border-right:1px;  border-color:#006699;  border-style: solid;background-color:#c9b023;'>

<h3>������� ����������� - �������</h3>
<ul>
<li>	���������� ����������� ��� �������� �������� � ������ ����������� �������� �������� (http://{$smarty.server.SERVER_NAME}/broker.php) ���������� ���������� �������� -  <b>�����������/�� �����������</b>.
<li>	�������� �������� ���������� ������ ����� �������.
<li>	���������� �������� ���������� �������� � ������ ����������� �������� �������� ���������� ����� ������������ �������� � ���� � <b>�����������</b>.  �������� ���������� ������� � ���� �����: 1 � ������ �������, 2 � ������  � �.�.
</ul>


</div>