<script type="text/javascript" src="http://{$smarty.server.SERVER_NAME}/js/jquery-1.7.2.min.js" ></script>
<script type="text/javascript" src="http://{$smarty.server.SERVER_NAME}/js/jquery.ui.core.js"></script>
<script type="text/javascript" src="http://{$smarty.server.SERVER_NAME}/js/jquery.ui.datepicker.js"></script>
<script type="text/javascript" src="http://{$smarty.server.SERVER_NAME}/js/jquery.ui.datepicker-ru.js"></script>

<LINK href="http://{$smarty.server.SERVER_NAME}/css/jquery-ui.css" type="text/css" rel="stylesheet">
<LINK href="http://{$smarty.server.SERVER_NAME}/css/demo_table.css" type="text/css" rel="stylesheet">

{literal}
	<script>
    	$(function() {
        	$( "#startDate" ).datepicker({
            changeMonth: true,
            changeYear: true
        });
        $.datepicker.setDefaults($.datepicker.regional['']);
        $("#startDate").datepicker();
        });
        
        
    	$(function() {
        	$( "#stopDate" ).datepicker({
            changeMonth: true,
            changeYear: true
        });
        
        $.datepicker.setDefaults($.datepicker.regional['']);
        $("#stopDate").datepicker();
        });        
    </script>
{/literal}


<form method="post">  
    <input type='hidden' name='search' value="search" />
    <table bgcolor="#a3d2b6" style="border:1px #c3c3c3 solid;" width="100%">
        <tr>
            <td colspan="3"><hr></td>
        </tr>
        <tr>
            <td align="center" width="33%" style="vertical-align: top;">
                <h3>���� �������� ����������:</h3>
                <table>
                    <tr>
                        <td>c:</td>
                        <td>
                            <input type="text" name='datestart' id="startDate" value="{if isset($smarty.post.datestart)}{$smarty.post.datestart}{/if}">
                        </td>
                        <td>��:</td>
                        <td>
                            <input type="text" name='datestop' id="stopDate" value="{if isset($smarty.post.datestart)}{$smarty.post.datestop}{/if}">
                        </td>
                        <td>
                            <select name="type">
                                <option value='1'>������� �������</option>
                                <option value=''>������� �������</option>
                                <option value=''>������� ������������</option>
                                <option value=''>����������</option>
                                <option value=''>���������</option>
                                <option value=''>���������� ������</option>
                            </select>
                        </td>
                    </tr>
                </table>
            </td> 
            <td align="center" width="33%" style="vertical-align: top;">
                <h3>��������� �������:</h3>
                <table>
                    <tr>
                        <td>c:</td>
                        <td>
                            <input type="text" name='coststart' value="{if isset($smarty.post.coststart)}{$smarty.post.coststart}{/if}">
                        </td>
                        <td>��:</td>
                        <td>
                            <input type="text" name='coststop' value="{if isset($smarty.post.coststop)}{$smarty.post.coststop}{/if}">
                        </td>
                        <td>
                            <select name="costId">
                                <option value=rub {if $smarty.post.costId eq 'rub'}selected{/if}>���</option>
                                <option value=usd {if $smarty.post.costId eq 'usd'}selected{/if}>USD</option>
                                <option value=eur {if $smarty.post.costId eq 'eur'}selected{/if}>EUR</option>
                            </select>
                        </td>    
                    </tr>
                </table>
            </td>
            <td width="33%" style="vertical-align: top;">
                <h3>���. ���������:</h3>
                <p>
                    <input type='checkbox' name='broker' {if isset($smarty.post.broker)}checked="checked"{/if} >������
                    <input type='checkbox' name='owner' {if isset($smarty.post.owner)}checked="checked"{/if} >�����������
                </p>
            </td>
        </tr>
        <tr>
            <td colspan="3"><hr></td>
        </tr>
        <tr>
            <td colspan="3">
                <h2>���������� � ������ ����:</h2>
                <table width="100%">
                    <tr>
                    {foreach key=k item=column from=$columns}
                        <td style="vertical-align: top;" bgcolor='#aac4ac'>
                            {foreach key=kfield item=ifield from=$column}
                                <p><input type="checkbox" name='{$ifield.field}' checked="checked" >{$ifield.title}</p>
                            {/foreach}    
                        </td>    
                    {/foreach}
                    </tr>
                </table>    
            </td>
        </tr>
        <tr>
            <td colspan="3"><hr></td>
        </tr>
        <tr>
            <td colspan="3" align="right" bgcolor="#d9d3d3">
                <input type="submit" value="������">
            </td>
        </tr>
    </table>
</form>