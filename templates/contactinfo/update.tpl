{literal}
<!-- Place inside the <head> of your HTML -->
<script type="text/javascript" src="http://{/literal}{$smarty.server.SERVER_NAME}{literal}/engine/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
tinymce.init({
    selector: "textarea.editme", 
    theme: "modern",
    language : 'ru',
    height : 300, 
    plugins: [
         "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
         "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
         "save table contextmenu directionality emoticons template paste textcolor"
   ],
   content_css: "css/content.css",
   toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | l      ink image | print preview media fullpage | forecolor backcolor emoticons", 
   style_formats: [
        {title: 'Bold text', inline: 'b'},
        {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
        {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
        {title: 'Example 1', inline: 'span', classes: 'example1'},
        {title: 'Example 2', inline: 'span', classes: 'example2'},
        {title: 'Table styles'},
        {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
    ]    
 });
</script>
{/literal}


<h3>���������� ���������� ����������</h3> 
<form method="post" style="margin-top:0pt;margin-bottom:0pt;">

<table  width='100%' bgcolor='#cccccc'>
	<tr>
		<td colspan="4" align="left" style="padding:15px;">
		��������� �������� ��������� (��������: http://{$smarty.server.SERVER_NAME}/contact.php):
		</td>
	</tr>
	<tr>
		<td colspan="4" align="center" style="padding:15px;" bgcolor="#dfdbdb">
     	 			<textarea name="textContactInfo" class='editme'>
      	  				{$contact->textContactInfo}
      	 			</textarea> 									
		</td>
	</tr>
	
	<tr>
		<td width="50%">
			<table>
				<tr>
					<td width="40%" align="left" nowrap>�������� ���. ���������:</td>
					<td><input type="text" value="{$contact->supportPhone}" name="supportPhone" size="45"></td>
				</tr>
				<tr>
					<td width="40%" align="left" nowrap>Email's ���. ���������:</td>
					<td><input type="text" value="{$contact->supportEmail}" name="supportEmail" size="45"></td>
				</tr>				
			</table>
		</td>
		<td width="50%">
			<table>
				<tr>
					<td width="40%" align="left" nowrap>�������� ���������� ������:</td>
					<td><input type="text" value="{$contact->adsPhone}" name="adsPhone" size="45"></td>
				</tr>
				<tr>
					<td width="40%" align="left" nowrap>Email's  ���������� ������:</td>
					<td><input type="text" value="{$contact->adsEmail}" name="adsEmail" size="45"></td>
				</tr>				
			</table>
		</td>		
	</tr>
	
	<tr> 
		<td colspan="4" align="right" bgcolor="#dfdbdb" style="padding:15px;">
			<input type="hidden" name="update" value="update">
			<input type="submit" value="���������">
		</td>
	</tr>
</table>

</form>