{literal}
<!-- Place inside the <head> of your HTML -->
<script type="text/javascript" src="http://{/literal}{$smarty.server.SERVER_NAME}{literal}/engine/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
tinymce.init({
    selector: "textarea.editme", 
    theme: "modern",
    language : 'ru',
    height : 300, 
    plugins: [
         "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
         "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
         "save table contextmenu directionality emoticons template paste textcolor"
   ],
   content_css: "css/content.css",
   toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | l      ink image | print preview media fullpage | forecolor backcolor emoticons", 
   style_formats: [
        {title: 'Bold text', inline: 'b'},
        {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
        {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
        {title: 'Example 1', inline: 'span', classes: 'example1'},
        {title: 'Example 2', inline: 'span', classes: 'example2'},
        {title: 'Table styles'},
        {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
    ]    
 });
</script>
{/literal}

<form method="post" style="margin-top:0pt;margin-bottom:0pt;" id="updateroomformID" enctype="multipart/form-data">
    
  {if isset($smarty.get.edit)}  
    <input type='hidden' name='update' value='update'>
    <input type='hidden' name='edit' value='{$smarty.get.edit}'>
  {elseif isset($smarty.get.add)}
    <input type='hidden' name='add' value='1'>
  {/if}
      
  <table width='100%' bgcolor='#cccccc'>
      
    <tr>
      <td width='30%' align='right' valign='top'>���</td>
      <td bgcolor="#dfdbdb">
         <input type='text' size=50 name="fio" value="{if isset($data->_fio)}{$data->_fio}{else}�����������{/if}">
      </td>
    </tr>      
      
    <tr>
      <td width='30%' align='right' valign='top'>������������ ����������</td>
      <td bgcolor="#dfdbdb">
         <input type='text' size=50 name="name" value="{if isset($data->_name)}{$data->_name}{else}����� ������� ������{/if}">
      </td>
    </tr>    
     
    <tr>
      <td align='right' valign='top'>����� ������</td>
      <td bgcolor="#dfdbdb">
        <textarea name="comment" class='editme'>
                {$data->_comment}
        </textarea> 							
      </td>
    </tr>    
     
    <tr>
      <td width='30%' align='right' valign='top'>������ ����������</td>
      <td bgcolor="#dfdbdb">
         <select name='statusId'>  
         {foreach key=key item=item from=$listStatus} 
             <option value="{$key}" {if $key eq $data->_statusId}selected{else}{/if}>{$item}</option>
         {/foreach}    
         </select>
      </td>
    </tr>     
    
    <tr>
      <td colspan=2 align='right'><input type='submit' value='���������'></td>
    </tr>    
    
  </table>
  
</form>  