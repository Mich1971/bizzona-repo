{literal}
	<script>
    	$(function() {
        	$( "#startDate" ).datepicker({
            changeMonth: true,
            changeYear: true
        });
        $.datepicker.setDefaults($.datepicker.regional['']);
        $("#startDate").datepicker();
        });
        
        
    	$(function() {
        	$( "#stopDate" ).datepicker({
            changeMonth: true,
            changeYear: true
        });
        
        $.datepicker.setDefaults($.datepicker.regional['']);
        $("#stopDate").datepicker();
        });        
    </script>
{/literal}

{if isset($item)}
<table width="100%">
<tr>
<td align="left" width="50%"><h3>����� ��� �������������� ������� - � {$item->Id}</h3></td>
<td align="right"><a href="http://{$smarty.server.SERVER_NAME}/engine/ad.php" style="font-size:15px;font-weight:bold;background-color:#e2f6ee">������� ����� �������</a></td>
</table>
{else}
<h3>����� ��� ���������� ����� �������</h3>
{/if}
<form method="post" style="margin-top:0pt;margin-bottom:0pt;" enctype="multipart/form-data">

<table  width='100%' bgcolor='#cccccc'>
	<tr>
		<td width="15%" align="left">����� ����������:</td>
		<td width="35%" align="left"  bgcolor="#dfdbdb">
			<select name="placeId">
			{foreach name=n key=k item=i from=$listplace}
				<option value="{$i.id}" {if isset($item) && $item->placeId == $i.id}selected{else}{/if}>{$i.description} ({$i.name})</option>
			{/foreach}	
			</select>  
		</td> 
		<td width="15%" align="left">������� ������:</td>
		<td width="35%" bgcolor="#dfdbdb">
			<select name="listCityIds[]" multiple>
			<option value="">�������</option>
			{foreach name=n key=k item=i from=$listcity}
				<option value="{$i.id}"  {if isset($item) && $i.id|in_array:$item->alistCityIds}selected{else}{/if}  >{$i.name}</option>
			{/foreach}			
			</select>		
		</td>
	</tr>
	<tr>
		<td width="15%" align="left">���� ���������� �������:</td>
		<td align="left"  bgcolor="#dfdbdb">
			<table>
				<tr>
					<td>c: <input type="text" name="startDate" id="startDate" value="{if isset($item)}{$item->startDateStr}{else}{/if}"></td>
					<td>��: <input type="text" name="stopDate"  id="stopDate" value="{if isset($item)}{$item->stopDateStr}{else}{/if}"></td>
				</tr>
			</table>
		</td>
		<td width="15%" align="left">������� ���������:</td>
		<td width="35%" bgcolor="#dfdbdb">
			<select name="listCategoryIds[]" multiple size="10">
			{foreach name=n key=k item=i from=$listcategory}
				<optgroup label="{$k}" style="background-color:#EDEDCC;color:#000000;">
					{foreach name=nn key=kk item=ii from=$i}
					  <option value="{$ii.id}" {if isset($item) && $ii.id|in_array:$item->alistCategoryIds}selected{else}{/if} >{$ii.name}</option>
					{/foreach}			
				</optgroup>
			{/foreach}			
			</select>		
		</td>		
	</tr>
	<tr>
		<td width="15%" align="left">��� �������:</td>
		<td align="left"  bgcolor="#dfdbdb">
			<select name="typeAdsId">
			{foreach name=n key=k item=i from=$listtypeads}
				<option value="{$i.id}" {if isset($item) && $item->typeAdsId == $i.id}selected{else}{/if} >{$i.description} ({$i.name})</option>
			{/foreach}			
			</select>
		</td>
		<td width="15%" align="left">��� ��������:</td>
		<td align="left"  bgcolor="#dfdbdb">
			<select name="siteTypeId">
			{foreach name=n key=k item=i from=$listsitetype}
				<option value="{$i.id}" {if isset($item) && $item->siteTypeId == $i.id}selected{else}{/if}>{$i.name}</option>
			{/foreach}			
			</select>
		</td>
		
	</tr>
	<tr>
		<td width="15%" align="left">������ �������:</td>
		<td align="left"  bgcolor="#dfdbdb">
			<select name="statusId">
			{foreach name=n key=k item=i from=$liststatus}
				<option value="{$i.id}" {if isset($item) && $item->statusId == $i.id}selected{else}{/if}>{$i.name}</option>
			{/foreach}			
			</select>
		</td>
		<td width="15%" align="left">���� ������� (%):</td>
		<td align="left"  bgcolor="#dfdbdb">
			<input type="text" value="{if isset($item) }{$item->shareShowAd}{else}{/if}" name="shareShowAd">
		</td>
	</tr>
	<tr>
		<td width="15%" align="left">����:</td>
		<td align="left"  bgcolor="#dfdbdb">
			<input type="file" name="fileload">
		</td>
		<td width="15%" align="left">����������:</td>
		<td>
			<textarea name="note" rows="5" style="width:80%">{if isset($item) }{$item->note}{else}{/if}</textarea>
		</td>		
	</tr>
	<tr>
		<td width="15%" align="left">������:</td>
		<td align="left"  bgcolor="#dfdbdb">
			<input type="text" value="{if isset($item) }{$item->url}{else}{/if}" name="url">
		</td>
		<td></td>
		<td></td>		
	</tr>		
	<tr>
		<td colspan="4" align="right">
			<input type="submit" value="���������">
		</td>
	</tr>
</table>
{if isset($item)}
<input type="hidden" name="update" value="update">
{else}
<input type="hidden" name="add" value="add">
{/if}

</form>