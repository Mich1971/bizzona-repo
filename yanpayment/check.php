<?php
    ini_set("display_startup_errors", "on");
    ini_set("display_errors", "on");
    ini_set("register_globals", "on");

    $date = date_create(date('Y-m-d'), timezone_open('Europe/Moscow'));
    $date = date_format($date, 'Y-m-d H:i:sP');
    
    define("PAYMENT_S", 1); // ������ ������� �������
    define("PAYMENT_S_UP", 2); // ������ �������� ������� ������� �������
 
    define("PAYMENT_B", 3); // ������ - ������� �������
    define("PAYMENT_E", 4); // ������ - ������������
    define("PAYMENT_I", 5); // ������ - ����������
    define("PAYMENT_C", 6); // ������ - ���������� ������
    define("PAYMENT_R", 7); // ������ - ���������
	
    define("PAYMENT_S_WALLET", 10); // ������ - ���������� ������� �������� � �������� ���������� �� ������� �������	
	
    global $DATABASE;

    include_once("../phpset.inc");
    include_once("../engine/functions.inc");

    AssignDataBaseSetting();
  	
    $requestDatetime = $_POST['requestDatetime'];
    $action = $_POST['action'];
    $md5 = $_POST['md5'];
    $shopId = $_POST['shopId'];
    $shopArticleId = $_POST['shopArticleId'];
    $invoiceId = $_POST['invoiceId'];
    $orderNumber = $_POST['orderNumber'];
    $customerNumber = $_POST['customerNumber'];
    $orderCreatedDatetime = $_POST['orderCreatedDatetime'];
    $orderSumAmount = $_POST['orderSumAmount'];
    $orderSumCurrencyPaycash = $_POST['orderSumCurrencyPaycash'];
    $orderSumBankPaycash = $_POST['orderSumBankPaycash'];    
    $shopSumAmount = $_POST['shopSumAmount'];
    $shopSumCurrencyPaycash = $_POST['shopSumCurrencyPaycash'];
    $shopSumBankPaycash = $_POST['shopSumBankPaycash'];
    $paymentPayerCode = $_POST['paymentPayerCode'];
    $paymentType = $_POST['paymentType'];

    header("Content-type: text/xml");

    if($action == '�heckOrder') {
     
        echo '<?xml version="1.0" encoding="UTF-8"?> 
<checkOrderResponse performedDatetime="'.$date.'" 
code="0" invoiceId="'.$invoiceId.'" 
shopId="'.$shopId.'"/>';
        
    } else if($action == 'paymentAviso') {
        
        echo '<?xml version="1.0" encoding="UTF-8"?> 
<paymentAvisoResponse 
performedDatetime ="'.$date.'" 
code="0" invoiceId="'.$invoiceId.'" 
shopId="'.$shopId.'"/>';
        
        include_once("../engine/class.sell.inc"); 
        $sell = new Sell();

        include_once("../engine/class.buy.inc");
        $buy = new Buyer();

        include_once("../engine/class.equipment.inc"); 
        $equipment = new Equipment();  	

        include_once("../engine/class.Room.inc"); 
        $room = new Room();  

        include_once("../engine/class.investments.inc"); 
        $investments = new Investments();

        include_once("../engine/class.coopbiz.inc"); 
        $coopbiz = new CoopBiz();

        include_once("../engine/class.ControllerWallet.inc"); 
        $wallet = new ControllerWallet();

        include_once("../engine/class.templateevent.inc"); 
        $templateEvent = new TemplateEvent();

        $merchantPaymentAmount = $orderSumAmount;
        $userName = $paymentType;
        
	$aOrderId = explode('-', $orderNumber);
	if(sizeof($aOrderId) == 2) {

            if($aOrderId[0] == PAYMENT_S_WALLET && $paymentStatus == 5) {
                    $wallet->_codeId = intval($aOrderId[1]);
                    $wallet->_summ = intval($merchantPaymentAmount);
                    $wallet->_typeId = 'sell';		
                    $wallet->_typePaymentId = 'yanmoney';
                    $wallet->_externPaymetCodeId = $paymentId;

                    $wallet->AddBalance();

                    $templateEvent->_sumpayment = $wallet->_summ;
                    $templateEvent->EmailEvent(6,1, intval($aOrderId[1]));
            }

            if(($aOrderId[0] == PAYMENT_S or $aOrderId[0] == PAYMENT_S_UP) && $paymentStatus == 5) {
                    $result = $sell->SetPayment(intval($aOrderId[1]), 1, intval($merchantPaymentAmount), '', $userName);

                    $templateEvent->EmailEvent(2,1, intval($aOrderId[1]));

            }

            if($aOrderId[0] == PAYMENT_B  && $paymentStatus == 5) {
                    $result = $buy->SetPayment(intval($aOrderId[1]), 1, intval($merchantPaymentAmount), '', $userName);

                    $templateEvent->EmailEvent(2,2, intval($aOrderId[1]));
            }		

            if($aOrderId[0] == PAYMENT_E  && $paymentStatus == 5) {
                    $result = $equipment->SetPayment(intval($aOrderId[1]), 1, intval($merchantPaymentAmount), '', $userName);

                    $templateEvent->EmailEvent(2,4, intval($aOrderId[1]));
            }

            if($aOrderId[0] == PAYMENT_R  && $paymentStatus == 5) {
                    $result = $room->SetPayment(intval($aOrderId[1]), 1, intval($merchantPaymentAmount), '', $userName);

                    $templateEvent->EmailEvent(2,6, intval($aOrderId[1]));
            }

            if($aOrderId[0] == PAYMENT_I  && $paymentStatus == 5) {
                    $result = $investments->SetPayment(intval($aOrderId[1]), 1, intval($merchantPaymentAmount), '', $userName);

                    $templateEvent->EmailEvent(2,3, intval($aOrderId[1]));
            }		

            if($aOrderId[0] == PAYMENT_C  && $paymentStatus == 5) {
                    $result = $coopbiz->SetPayment(intval($aOrderId[1]), 1, intval($merchantPaymentAmount), '', $userName);

                    $templateEvent->EmailEvent(2,5, intval($aOrderId[1]));
            }		
		
	}
        
        
        
        
        
    } else {
     
        echo '<?xml version="1.0" encoding="UTF-8"?> 
<paymentAvisoResponse 
performedDatetime ="'.$date.'" 
code="200" invoiceId="0" 
shopId="13"/>';
    
    }
    
?>