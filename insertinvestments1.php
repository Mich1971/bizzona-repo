<?
  global $DATABASE;
  
  include_once("./phpset.inc");
  include_once("./engine/functions.inc"); 
  AssignDataBaseSetting();
  
  include_once("./engine/class.category.inc"); 
  $category = new Category();

  include_once("./engine/class.subcategory.inc"); 
  $subCategory = new SubCategory();
  
  include_once("./engine/class.country.inc"); 
  $country = new Country();

  include_once("./engine/class.city.inc"); 
  $city = new City();

  include_once("./engine/class.bizplan_company.inc"); 
  $bizplan_company = new BizPlanCompany();

  include_once("./engine/class.investments.inc"); 
  $investments = new Investments();
  
  
  include_once("./engine/class.bizplan.inc");
  $bizplan = new BizPlan();

  include("./engine/FCKeditor/fckeditor.php"); 

  require_once("./libs/Smarty.class.php");
  $smarty = new Smarty;
  $smarty->template_dir = "./templates";
  $smarty->compile_dir  = "./templates_c";
  //$smarty->cache_dir = "./cache";
  $smarty->compile_check = true;
  $smarty->debugging     = false;

  
  $data = array();
  $data["offset"] = 0;
  $data["rowCount"] = 0;
  $data["sort"] = "ID";
    
  $listCategory = $category->Select($data);
  $smarty->assign("listCategory", $listCategory);
  
  $listSubCategory = $subCategory->Select($data);
  $smarty->assign("listSubCategory", $listSubCategory);

  $companylist = $bizplan_company->SelectByStatus(OPEN_BIZPLAN_COMPANY);
  $smarty->assign("companylist", $companylist);
  
  $data = array();
  $data["offset"] = 0;
  $data["rowCount"] = 0;
  $data["sort"] = "Count";
  $result = $category->Select($data);
  $smarty->assign("categorylist", $result);
  
  if (isset($_POST) && sizeof($_POST)) {
  	
  	$error = false;
  	$error_text = "";
 	
	$t_name = htmlspecialchars(trim($_POST["name"]), ENT_QUOTES);
	$_POST["name"] = $t_name;
	
	$total_typeBizID = htmlspecialchars(trim($_POST["category_id"]), ENT_QUOTES);
	$arr_typeBizID = split("[:]",$total_typeBizID);
	if(sizeof($arr_typeBizID) == 2) {
		$data["category_id"] = intval($arr_typeBizID[0]);
		$data["subCategory_id"] = intval($arr_typeBizID[1]);
		$_POST["subCategory_id"] = $data["subCategory_id"];
		$t_category_id = $data["category_id"];
		$t_subCategory_id = $data["subCategory_id"];
	} else {
		$data["category_id"] = "-1";
		$data["subCategory_id"] = "-1";
		$t_category_id = $data["category_id"];
		$t_subCategory_id = $data["subCategory_id"];
	}
	
	$t_region_text = htmlspecialchars(trim($_POST["region_text"]), ENT_QUOTES);
	$_POST["region_text"] = $t_region_text;
	
	$t_region_id = "";
	$t_cost = htmlspecialchars(trim($_POST["cost"]), ENT_QUOTES);
	$_POST["cost"] = $t_cost;
	
	$t_ccost = htmlspecialchars(trim($_POST["ccost"]), ENT_QUOTES);
	$_POST["ccost"] = $t_ccost;
	
	$t_period = htmlspecialchars(trim($_POST["period"]), ENT_QUOTES);
	$_POST["period"] = $t_period;
	
	$t_payback = htmlspecialchars(trim($_POST["payback"]), ENT_QUOTES);
	$_POST["payback"] = $t_payback;
	
	$t_yearprofit = htmlspecialchars(trim($_POST["yearprofit"]), ENT_QUOTES);
	$_POST["yearprofit"] = $t_yearprofit;
	
	$t_cyearprofit = htmlspecialchars(trim($_POST["cyearprofit"]), ENT_QUOTES);
	$_POST["cyearprofit"] = $t_cyearprofit;
	
	$t_totalinvestcost = htmlspecialchars(trim($_POST["totalinvestcost"]), ENT_QUOTES);
	$_POST["totalinvestcost"] = $t_totalinvestcost;
	
	$t_totalinvestccost = htmlspecialchars(trim($_POST["totalinvestccost"]), ENT_QUOTES);
	$_POST["totalinvestccost"] = $t_totalinvestccost;
	
	$t_stage = htmlspecialchars(trim($_POST["stage"]), ENT_QUOTES);
	$_POST["stage"] = $t_stage;
	
	$t_documents = htmlspecialchars(trim($_POST["documents"]), ENT_QUOTES);
	$_POST["documents"] = $t_documents;
	
	$t_contactface = htmlspecialchars(trim($_POST["contactface"]), ENT_QUOTES);
	$_POST["contactface"] = $t_contactface;
	
	$t_contactphone = htmlspecialchars(trim($_POST["contactphone"]), ENT_QUOTES);
	$_POST["contactphone"] = $t_contactphone;
	
	$t_contactfax = htmlspecialchars(trim($_POST["contactfax"]), ENT_QUOTES);
	$_POST["contactfax"] = $t_contactfax;
	
	$t_contactemail = htmlspecialchars(trim($_POST["contactemail"]), ENT_QUOTES);
	$_POST["contactemail"] = $t_contactemail;
	
	$t_shortDescription = htmlspecialchars(trim($_POST["shortDescription"]), ENT_QUOTES);
	$_POST["shortDescription"] = $t_shortDescription;
	
	$t_attractedPerson = htmlspecialchars(trim($_POST["attractedPerson"]), ENT_QUOTES);
	$_POST["attractedPerson"] = $t_attractedPerson;
  	
  	if (strlen(trim($t_name)) <= 0)
  	{
  		$error = true;	
  		$text_error = "������ ����� ������ \"�������� �������\"";
  		$smarty->assign("ErrorInput_name",true);
  	}
  	else if(strlen(trim($t_cost)) <= 0)
  	{
  		$error = true;
  		$text_error = "������ ����� ������ \"��������� ����� ����������\"";
  		$smarty->assign("ErrorInput_cost",true);
  	}
  	else if(strlen(trim($t_region_text)) <= 0)
  	{
  		$error = true;
  		$text_error = "������ ����� ������ \"������\"";
  		$smarty->assign("ErrorInput_region_text",true);
  	}
  	else if(strlen(trim($t_shortDescription)) <= 0)
  	{
  		$error = true;
  		$text_error = "������ ����� ������ \"������� ��������\"";
  		$smarty->assign("ErrorInput_shortDescription",true);
  	}
  	
  	else if(strlen(trim($t_contactface)) <= 0)
  	{
  		$error = true;
  		$text_error = "������ ����� ������ \"���������� ����\"";
  		$smarty->assign("ErrorInput_contactface",true);
  	}
  	else if(strlen(trim($t_contactphone)) <= 0)
  	{
  		$error = true;
  		$text_error = "������ ����� ������ \"��������\"";
  		$smarty->assign("ErrorInput_contactphone",true);
  	}
  	else if(strlen(trim($t_contactemail)) <= 0)
  	{
  		$error = true;
  		$text_error = "������ ����� ������ \"Email\"";
  		$smarty->assign("ErrorInput_contactemail",true);
  	}
  	
  	if(!$error)
  	{
		$investments->name = $t_name;
		$investments->category_id = $t_category_id;
		$investments->subCategory_id = $t_subCategory_id;
		$investments->region_text = $t_region_text;
		$investments->region_id = $t_region_id;
		$investments->cost = $t_cost;
		$investments->ccost = $t_ccost;
		$investments->period = $t_period;
		$investments->payback = $t_payback;
		$investments->yearprofit = $t_yearprofit;
		$investments->cyearprofit = $t_cyearprofit;
		$investments->totalinvestcost = $t_totalinvestcost;
		$investments->totalinvestccost = $t_totalinvestccost;
		$investments->stage = $t_stage;
		$investments->documents = $t_documents;
		$investments->contactface = $t_contactface;
		$investments->contactphone = $t_contactphone;
		$investments->contactfax = $t_contactface;
		$investments->contactemail = $t_contactemail;
		$investments->shortDescription = $t_shortDescription;
		$investments->qview = 0;
		$investments->attractedPerson = $t_attractedPerson;
		$investments->Insert();
		
        $to      = 'bizzona.ru@gmail.com';
        $subject = 'NEW INVESTMENTS: '.$investments->name.'';

        $message = "�������� ".$investments->name."\n";
        $message.= "��������� ����� ���������� ".$investments->cost."\n";
        $message.= "��������� ����� ����������(���) ".$investments->ccost."\n";
        $message.= "��� ������� ".$investments->category_id."\n";
        $message.= "������ ".$investments->region_text."\n";
        $message.= "���� ����������� ���������� ".$investments->period."\n";
        $message.= "����������� ".$investments->payback."\n";
        $message.= "������� ���������� ".$investments->yearprofit."\n";
        $message.= "����� ������������� ���������� ".$investments->totalinvestcost."\n";
        $message.= "����� ������������� ����������(���) ".$investments->totalinvestccost."\n";
        $message.= "������ ���������� ".$investments->stage."\n";
        $message.= "��������� � ������� ".$investments->documents."\n";
        $message.= "������� �������� ".$investments->shortDescription."\n";
        $message.= "������������ ��������� ".$investments->attractedPerson."\n";
        $message.= "���������� ���� ".$investments->contactface."\n";
        $message.= "�������� ".$investments->contactphone."\n";
        $message.= "Email ".$investments->contactemail."\n";
        $message.= "������������ ���������� ".$investments->period_id."\n";

        $from = "bizzona.ru@gmail.com";

    	$headers =  "From: BizZONA.ru  <".$from.">\r\n".               
    				"Reply-To: BizZONA.ru <".$from.">\r\n".
    				"Content-type: text/plain; charset=windows-1251;\r\n".
    				"Return-Path: ".$from."\r\n";
                    
      
        mail($to, $subject, $message, $headers);
		
		
  		unset($_POST);
  		$smarty->assign("text_success", "������ ��� ��������. � ������� ���� �� ����� ���������� � �����������.");
  	}
  	else
  	{
  		$smarty->assign("text_error", $text_error);
  	}
  	
  }
  
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>

  <HEAD id="Head">
      <title>���������� ������ �������������� - ������ ����</title>		
	<META NAME="Description" CONTENT="������ ��������������, ������� �������, ������� �������, ������� ������, ������� �������, ������� �������� �������, ������� �������, ������� ��������,  ������� ��������, ������� ��������, ���� ������� ������?">
        <meta name="Keywords" content="������ ��������������, ������� �������, ������� �������, ������� ������, ������� �������, ������� �������� �������, ������� �������, ������� ��������,  ������� ��������, ������� ��������, ���� ������� ������?, Ready business, �����������, �������� � ��������, ���������, ����������, �����������"> 
        <LINK href="./general.css" type="text/css" rel="stylesheet">
        <script src="http://tools.spylog.ru/counter.js" type="text/javascript"> </script>
  </HEAD>

<?
	$smarty->display("./site/headeri.tpl");
?>

<table class="w" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td width="100%">
         <table width='100%' height='100%' cellpadding="0" cellspacing="0">
           <tr>
             <td valign='top' width='60%' height='100%'>
               <?
                  $smarty->display("./site/insertinvestments1.tpl");
               ?>
             </td>
           </tr>
         </table>
        </td>
    </tr>
</table>

<?
	$smarty->display("./site/footer.tpl");
?>

</body>
</html>


