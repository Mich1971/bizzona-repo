  <?
  global $DATABASE;

  include_once("./phpset.inc");
  include_once("./engine/functions.inc"); 
  include_once("./engine/equipment_functions.inc"); 
  require_once("./engine/Cache/Lite.php");
  
  $count_sql_call = 0;
  $start = get_formatted_microtime();   
  $base_memory_usage = memory_get_usage();	

  AssignDataBaseSetting();
  
  include_once("./engine/class.category_lite.inc"); 
  $category = new Category_Lite();

  include_once("./engine/class.country_lite.inc"); 
  $country = new Country_Lite();

  include_once("./engine/class.city_lite.inc");  
  $city = new City_Lite();

  include_once("./engine/class.equipment_lite.inc"); 
  $equipment = new Equipment_Lite();

  include_once("./engine/class.sell_lite.inc"); 
  $sell = new Sell_Lite(); 

  include_once("./engine/class.metro_lite.inc"); 
  $metro = new Metro_Lite();

  include_once("./engine/class.district_lite.inc"); 
  $district = new District_Lite();

  include_once("./engine/class.subcategory_lite.inc");
  $subcategory = new SubCategory_Lite();

  include_once("./engine/class.streets_lite.inc");
  $streets = new Streets_Lite();

        include_once("./engine/class.ad.inc");
        $ad = new Ad();

include_once("./engine/class.frontend.inc");


  require_once("./libs/Smarty.class.php");
  $smarty = new Smarty;
  $smarty->template_dir = "./templates";
  $smarty->compile_dir  = "./templates_c";
  $smarty->cache_dir = "./cache";
  $smarty->compile_check = true;
  $smarty->debugging     = false;
  //$smarty->caching = true; 

  require_once("./regfuncsmarty.php");
  
  $assignDescription = AssignDescription();

		
  $cache = new Cache_Lite($options);
		
  
	$cache->setLifeTime(10800);
  	if ($data = $cache->get('82____suggestedsell')) {
		$resultsell = unserialize($data);
  	} else { 
  			
		$datasuggsell = array();
		$datasuggsell["offset"] = 0;
		$datasuggsell["rowCount"] = 5;
		$datasuggsell["catId"] = 82;
		$datasuggsell["StatusID"] = "-1";
		$datasuggsell["sort"] = "DataCreate";
		$resultsell = $sell->DetailsSelect($datasuggsell); 	
  		$cache->save(serialize($resultsell));
  	}  
  
 $ad->InitAd($smarty, $_GET); 
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
  <HEAD>
      <title>���������������� ������������, ��� "������-����"</title>		
		<META NAME="Description" CONTENT="���������������� ������������">
        <meta name="Keywords" content="���������������� ������������"> 
        <LINK href="http://www.bizzona.ru/general.css" type="text/css" rel="stylesheet">
        <meta http-equiv="content-type" content="text/html; charset=windows-1251"/>
   		<script src="http://www.bizzona.ru/js/jquery.min.js"></script>
		<script src="http://www.bizzona.ru/js/jquery.colorbox.js"></script> 
		<link media="screen" rel="stylesheet" href="http://www.bizzona.ru/css/colorbox.css" />        
		<script>
		$(document).ready(function(){
			$("a[rel='sendorder']").colorbox({width:"60%", height:"80%", iframe:true});
			$("a[rel='sendask']").colorbox({width:"60%", height:"80%", iframe:true});
			$("#click").click(function(){ 
				$('#click').css({"background-color":"#f00", "color":"#fff", "cursor":"inherit"}).text("");
				return false;
			}); 
		});
		</script>		
		
		
  </HEAD>

<?
	$smarty->display("./site/headerbanner.tpl");
	$smarty->assign("headertitle", "���������������� ������������");
?>

<table class="w" border="0" cellpadding="0" cellspacing="0" height="100%">
	<tr>
		<td colspan="2">
		<?
			$topmenu = GetMenu();
			$smarty->assign("topmenu", $topmenu);

			$link_country = GetSubMenu();
			$smarty->assign("ad", $link_country);

			$smarty->caching = false;
			echo minimizer($smarty->fetch("./site/headere.tpl"));		
		?>
		</td>
	</tr>
	<tr>
		<td colspan="2">
				<?
					echo UpPositionAndShare(intval($_GET["ID"]));
				?>
		</td>
	</tr>
    <tr>
        <td width="70%" valign="top" style="padding:2pt;">
			<div style="width:auto;height:100%;" >

				<h3 style="color:#006699;font-family:arial;">���������������� ������������</h3>
				
				<p style="font-size:10pt;font-family:arial">
					������� ����������������� ������������ ������� �����   �� ����� ����� ������� ��������� �����������.  � ��������� ����� �� ��������� ������, ����� ��������, ��������� ���������������� ������������ ��������� ���� ����������� �� ��������� ������ ����������, �������, �� ����, �������� ������ ����� ������.  ������� � ������������� ��������� � ������� �������, � ���� ��� ��������.  ��������� � ���� ������, ��� ��������� ���������� ���������������  �� �����. ���� �������� �������� ��������� �� ������ ���, ��� �� ��������� ������ ������� �������� �������, �� � ���, ��� �� ��������� ��������� ����������������� ������������ ����� ��� ����� ���� �������� ���������� ������ ����� �� ���� ��������� ����������� � ������� ������������� ��������� ��� �����.  ��� ��� �� �������� �������� ����������� ����������� � ��������� �������, �� �� ���� ������ �� ���������� ����������� � ���� ������������ ��� �������.   ������ � ���� ������, ���� ���������� �������� � ��������������� �������� ����������� �� ��� �������, � ��� ���������������� ������, ������� ��������������� ����� ���������.  ���� � ��� �������� � �������� ����������������� ������������, ��������� � ���� �� ����� ��������� �� ����� ��������� ��� ���������� ����������� ���� ��������������.     
				</p>
			
				<div style="font-size:10pt;font-family:arial" align="right">
					�������, ��� �������� ��� ����� ������ �������!
				</div>
				
            	<?
            		echo "<br>";
            		echo SugEqipmentHeaderShow();
            	
            		$data = array();
            		$data["offset"] = 0;
            		$data["rowCount"] = 15;
            		$data["sort"] = "ID";
            		//$data["goodcatparent_id"] = 10;
            		$data["category_id"] = 82;
            		
                    $result = $equipment->Select($data);
                    
                    if(sizeof($result) < 3) {
                    	unset($data["goodcatparent_id"]);
                    	$result = $equipment->Select($data);
                    }
                    
                    if(sizeof($result) < 3) {
                    	unset($data["category_id"]);
                    	$result = $equipment->Select($data);
                    }
                    
                    $smarty->assign("data", $result);
				  	echo minimizer($smarty->fetch("./site/listsellshop.tpl", $_SERVER["REQUEST_URI"]));            
				  	
				  	echo SugEqipmentHeaderShow();
				?>

               	<?
					echo UpPositionAndShare(intval($_GET["ID"]));				
				?>
				
            </div>
        </td>
        <td width="30%" valign="top"  height="100%" style="padding:2pt;">
        <?
          $smarty->cache_dir = "./persistentcache"; 
          $smarty->cache_lifetime = 10800;
                  
          if (isset($_GET["action"]) && $_GET["action"]  == "category") {
            $_GET["ID"] = (isset($_GET["ID"])  ? intval($_GET["ID"]) : 0);
            $smarty->caching = true; 
            if (!$smarty->is_cached('./site/categoryse.tpl', $_GET["ID"])) {
              $data = array();
              $data["offset"] = 0;
              $data["rowCount"] = 0;
              $data["sort"] = "CountE";
              $result = $category->Select($data);
              $smarty->assign("data", $result);
            }
            $smarty->display("./site/categoryse.tpl", $_GET["ID"]);
            $smarty->caching = false; 
          } else { 
            $smarty->caching = true; 
            if (!$smarty->is_cached('./site/categoryse.tpl')) {
              $data = array();
              $data["offset"] = 0;
              $data["rowCount"] = 0;
              $data["sort"] = "CountE";
              $result = $category->Select($data);
              $smarty->assign("data", $result);
            }
            $smarty->display("./site/categoryse.tpl");
            $smarty->caching = false; 
          }
          
          $smarty->cache_dir = "./cache";
          $smarty->cache_lifetime = 3600;          
          
          $smarty->display("./site/call.tpl");
        ?>
        
		<div style="padding-top:2pt;"></div>   	        
		
        <?
			echo minimizer($smarty->fetch("./site/proposal.tpl"));
			echo minimizer($smarty->fetch("./site/request.tpl"));
        ?>  
        
        <div style="background-color:#f6f6f5;">
        
		<?
            $smarty->assign("data", $resultsell);
            if(isset($resultsell) && sizeof($resultsell) > 0) {
				echo fminimizer($smarty->fetch("./site/sugnavsearch_1.tpl"));
            }

			$smarty->assign('colorsell', FrontEnd::ListColorSet());

			echo fminimizer($smarty->fetch("./site/ilistsell_1.tpl", $_SERVER["REQUEST_URI"]));
                    
            if(isset($resultsell) && sizeof($resultsell) > 0) {
				echo fminimizer($smarty->fetch("./site/sugnavsearch_1.tpl"));                    	
            }            
		?>
		</div>
        
        </td>
        
    </tr>
    <tr>
    	<td colspan="2">
			<div style="padding-top:4pt;padding-bottom:5pt;padding-right:10pt;padding-left:10pt;font-size:10pt;font-family:arial;">
				<table cellpadding="0" cellspacing="0" border="0"  style="width:100%;" bgcolor="#eeeee0">
    				<tr>
        				<td style="background-image:url(http://<?=$_SERVER["HTTP_HOST"]?>/images/d1.gif); background-repeat:no-repeat;width:5px;height:5px; background-position:left top;">&nbsp;</td>
            			<td rowspan="2" valign="middle" align="center"  style="font-size:8pt; font-family:Arial; color:black;">
            			</td>
            			<td style="background-image:url(http://<?=$_SERVER["HTTP_HOST"]?>/images/d2.gif); background-repeat:no-repeat;width:5px;height:5px;background-position:right top;">&nbsp;</td>
        			</tr>
        			<tr>
            			<td style="background-image:url(http://<?=$_SERVER["HTTP_HOST"]?>/images/d4.gif); background-repeat:no-repeat;width:5px;height:5px; background-position:left bottom;">&nbsp;</td>
            			<td style="background-image:url(http://<?=$_SERVER["HTTP_HOST"]?>/images/d3.gif); background-repeat:no-repeat;width:5px;height:5px; background-position:right bottom;">&nbsp;</td>
        			</tr>
    			</table>
			</div>
			<?
				$smarty->display("./site/footer.tpl");
			?>
    	</td>
    </tr>
</table>

<?
  $end = get_formatted_microtime(); 
  $total = $end - $start;
  echo "<center><span style='font-size:7pt;'>".round($total, 6)." : ".$count_sql_call." : ".$base_memory_usage." : ".memoryUsage(memory_get_usage(), $base_memory_usage)." </span><center>";
?>


</body>
</html>
<?
	$sell->Close();
	$category->Close();
	$city->Close();
	$country->Close();
	$equipment->Close();
	$metro->Close();
	$district->Close();
	$subcategory->Close();
    $streets->Close();
?>
