<?
    global $DATABASE;

    include_once("./phpset.inc");
    include_once("./engine/functions.inc");

    $start = get_formatted_microtime();

    AssignDataBaseSetting();
    include_once("./engine/class.category.inc"); 
    $category = new Category();

    include_once("./engine/class.subcategory.inc"); 
    $subCategory = new SubCategory();

    include_once("./engine/class.BlockIP.inc"); 
    $blockIp = new BlockIP();

    $blocking  = $blockIp->YouAreBlocking(array(
        'ip' => $_SERVER['REMOTE_ADDR']
    ));

    include_once("./engine/class.insert_settings.inc"); 
    $insertSettings = new InsertSettings();  

    $insertSettings->type_insert = ConstInsertSettings::BUYER;
    $insertSettingsData = $insertSettings->GetItem();

    include_once("./engine/class.buy.inc"); 
    $buyer = new Buyer();

    require_once("./libs/Smarty.class.php");
    $smarty = new Smarty;
    $smarty->template_dir = "./templates";
    $smarty->compile_dir  = "./templates_c";
    //$smarty->cache_dir = "./cache";
    $smarty->compile_check = true;
    $smarty->debugging     = false;

    require_once("./regfuncsmarty.php");
    
    $data = array();
    $data["offset"] = 0;
    $data["rowCount"] = 0;
    $data["sort"] = "ID";

    $listCategory = $category->Select($data);   
    $smarty->assign("listCategory", $listCategory);

    $listSubCategory = $subCategory->Select($data);
    $smarty->assign("listSubCategory", $listSubCategory);

  
  if (isset($_POST) && sizeof($_POST) > 0 && !$blocking) {
  	
  	$status_error = false;
  	$text_error = "";
  	
  	$t_duration = htmlspecialchars(trim($_POST["durationID"]), ENT_QUOTES);
  	$_POST["durationID"] = $t_duration;
  	
  	$t_name = htmlspecialchars(trim($_POST["nameID"]), ENT_QUOTES);
  	$_POST["nameID"] = $t_name;
  	
  	$t_cost_start = htmlspecialchars(trim($_POST["cost_startID"]), ENT_QUOTES);
  	$_POST["cost_startID"] = $t_cost_start;
  	
  	$t_cost_stop = htmlspecialchars(trim($_POST["cost_stopID"]), ENT_QUOTES);
  	$_POST["cost_stopID"] = $t_cost_stop;
  	
  	$t_regionText = htmlspecialchars(trim($_POST["regionTextID"]), ENT_QUOTES);
  	$_POST["regionTextID"] = $t_regionText;
  	
  	$total_typeBizID = htmlspecialchars(trim($_POST["categoryID"]), ENT_QUOTES);
  	$_POST["categoryID"] = $total_typeBizID;
  	
  	$arr_typeBizID = split("[:]",$total_typeBizID);
  	if(sizeof($arr_typeBizID) == 2)
  	{
  		$t_typeBizID = intval($arr_typeBizID[0]);
  		$t_subTypeBizID = intval($arr_typeBizID[1]);
  	}
  	else 
  	{
  		$t_typeBizID = 0;
  		$t_subTypeBizID = 0;
  		
  	}
  	
  	$t_description = htmlspecialchars(trim($_POST["otherinfoID"]), ENT_QUOTES);
  	$_POST["otherinfoID"] = $t_description;
  	
  	$t_FML = htmlspecialchars(trim($_POST["FMLID"]), ENT_QUOTES);
  	$_POST["FMLID"] = $t_FML;
  	
  	$t_phone = htmlspecialchars(trim($_POST["phoneID"]), ENT_QUOTES);
  	$_POST["phoneID"] = $t_phone;
  	
  	$t_email = htmlspecialchars(trim(strtolower($_POST["emailID"])), ENT_QUOTES);
  	$_POST["emailID"] = $t_email;
  	
  	$t_age = htmlspecialchars(trim($_POST["ageID"]), ENT_QUOTES);
  	$_POST["ageID"] = $t_age;
  	
  	$t_otherinfo = htmlspecialchars(trim($_POST["otherinfoID"]), ENT_QUOTES);
  	$_POST["otherinfoID"] = $t_otherinfo;
  	
  	$t_realty = htmlspecialchars(trim($_POST["realtyID"]), ENT_QUOTES);
  	$_POST["realtyID"] = $t_realty;
  	
  	$t_currencyID = htmlspecialchars(trim($_POST["currencyID"]), ENT_QUOTES);
  	$_POST["currencyID"] = $t_currencyID;
  	
  	$t_partID = htmlspecialchars(trim($_POST["partID"]), ENT_QUOTES);
  	$_POST["partID"] = $t_partID;
  	
  	$t_licenseID = htmlspecialchars(trim($_POST["licenseID"]), ENT_QUOTES);
  	$_POST["licenseID"] = $t_licenseID;
  	
  	$t_paybackID = htmlspecialchars(trim($_POST["paybackID"]), ENT_QUOTES);
  	$_POST["paybackID"] = $t_paybackID;
  	
  	if(isset($_POST["subscribe_status_id"])) {
  		$t_subscribe_status_id = 1;
  	} else {
  		$t_subscribe_status_id = 0;
  	}
  	
  	
  	if (strlen(trim($t_name)) <= 0) {
  		
  		$status_error = true;	
  		$text_error = "������ ����� \"�������� �������\"";
  		$smarty->assign("ErrorInput_nameID", true);
  		
  	} elseif (strlen(trim($t_regionText)) <= 0) {
  		
  		$status_error = true;	
  		$text_error = "������ ����� \"������������\"";
  		$smarty->assign("ErrorInput_regionTextID", true);
  		
  	} elseif(strlen(trim($t_FML)) <= 0) {
  		
  		$status_error = true;
  		$text_error = "������ ����� \"���������� ����\"";
  		$smarty->assign("ErrorInput_FMLID", true);
  		
  	} elseif (strlen(trim($t_phone)) <= 0) {
  		
  		$status_error = true;
  		$text_error = "������ ����� \"��������\"";
  		$smarty->assign("ErrorInput_phoneID", true);
  		
  	} elseif(strlen(trim($t_email)) <= 0) {
  		
  		$status_error = true;
  		$text_error = "������ ����� \"Email\"";
  		$smarty->assign("ErrorInput_emailID", true);
  	} 

  	
  	if(!$status_error)
  	{
  		$data = array();
  		$data["duration"] = $t_duration;
  		$data["name"] = FixName(ucfirst($t_name));
  		$data["cost_start"] = str_replace(" ","", $t_cost_start);
  		$data["cost_stop"] = str_replace(" ", "", $t_cost_stop);
  		$data["regionText"] = $t_regionText;
  		$data["typeBizID"] = $t_typeBizID;
  		$data["subTypeBizID"] = $t_subTypeBizID;
  		$data["description"] = $t_description;
  		$data["FML"] = $t_FML;
  		$data["phone"] = $t_phone;
  		$data["email"] = $t_email;
  		$data["statusID"] = $t_statusID;
  		$data["age"] = $t_age;
  		$data["otherinfo"] = $t_otherinfo;
  		$data["realty"] = $t_realty;
  		$data["currencyID"] = $t_currencyID;
  		$data["part"] = $t_partID;
  		$data["license"] = $t_licenseID;
  		$data["payback"] = $t_paybackID;
  		$data["subscribe_status_id"] = $t_subscribe_status_id;
  		
		if(isset($_SESSION["partnerid"])) {
			$data["partnerid"] =  intval($_SESSION["partnerid"]);
		} else {
			$data["partnerid"] = 0;
		}  		
  		
		if(!isset($_SESSION["fml"])) {
			$_SESSION["fml"] = $data["FML"];
		}
		
		if(!isset($_SESSION["email"])) {
			$_SESSION["email"] = $data["email"];
		}
		
		if(!isset($_SESSION["phone"])) {
			$_SESSION["phone"] = $data["phone"];
		}
		
		$code = $buyer->Insert($data);  
                
                $_SESSION['timepost'] = time();
                
                
		unset($_POST); 

 

		$secret_code = "vbhjplfybt";
		$purse = "4146"; // sms:bank id        ������������� ���:�����
		$order_id = intval($code) + 1000000; //  ������������� ��������
		$amount = "3"; // transaction sum  ����� ����������
		$clear_amount  = "0"; // billing algorithm  �������� �������� ���������
		$description  = "������ ���������� ������� �������"; // operation desc  �������� ��������
  		$submit  = "���������"; // submit label    ������� �� ������ submit

		$sign = ref_sign($purse, $order_id, $amount, $clear_amount, $description, $secret_code);

		$m_description = cp1251_to_utf8($description."  (".$order_id.") ");
		$m_description = base64_encode($m_description);		
		
       	$smarty->assign("purse", $purse);
       	$smarty->assign("order_id", $order_id);
       	$smarty->assign("rbk_order_id", intval($code));
       	$smarty->assign("amount", $amount);
       	$smarty->assign("clear_amount", $clear_amount);
       	$smarty->assign("description", $description);
       	$smarty->assign("m_description", $m_description);
       	$smarty->assign("submit", $submit);
       	$smarty->assign("sign", $sign);
       	$smarty->assign("email", $data["email"]);

		$text_success = "������ ���� ������� �������� �� ������";
		$smarty->assign("text_success", $text_success);
		
		// up send to email - dublicate
        $to      = 'bizzona.ru@gmail.com';
        $subject = 'NEW BUYER: '.$data["FML"].'';
        $message = "���������� ����: ".$data["FML"]."\n";
        $message.= "Email: ".$data["email"]."\n";
        $message.= "������������ ����������: ".$data["duration"]."\n";
        $message.= "�������� �������: ".$data["name"]."\n";
        $message.= "��������� ��: ".$data["cost_start"]."\n";
        $message.= "��������� ��: ".$data["cost_stop"]."\n";
        $message.= "������������: ".$data["regionText"]."\n";
        $message.= "��� �������: ".$data["typeBizID"]."\n";
        $message.= "������ �������: ".$data["subTypeBizID"]."\n";
        $message.= "���� ����������: ".$data["description"]."\n";
        $message.= "�������: ".$data["age"]."\n";
        $message.= "������ ����������: ".$data["otherinfo"]."\n";
        $message.= "������������: ".$data["realty"]."\n";
        $message.= "��� ������: ".$data["currencyID"]."\n";
        $message.= "����: ".$data["part"]."\n";
        $message.= "��������: ".$data["license"]."\n";
        $message.= "���� �����������: ".$data["payback"]."\n";
        $headers = 'BizZONA.RU: new buyer' . "\r\n" .'Reply-To: '.$data["EmailID"].' ' . "\r\n" .'X-Mailer: PHP/' . phpversion();
        mail($to, $subject, $message, $headers);
        
		// down send to email - dublicate
  	} else {
  		$smarty->assign("text_error", $text_error);
  	}
  }

    include_once("./engine/class.ContactInfo_Lite.inc");
    $contact = new ContactInfo_Lite();  

    $contact->GetContactInfo();  
    $smarty->assign("contact", $contact);      

    $smarty->assign("insertsettings", $insertSettingsData);      
?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>

  <HEAD>
      <title>���������� ������� - ������� � ������� �������. ������� ������ - ������ ����</title>		
	<META NAME="Description" CONTENT="������� �������, ������� �������, ������� ������, ������� �������, ������� �������� �������, ������� �������, ������� ��������,  ������� ��������, ������� ��������, ���� ������� ������?">
        <meta name="Keywords" content="������� �������, ������� �������, ������� ������, ������� �������, ������� �������� �������, ������� �������, ������� ��������,  ������� ��������, ������� ��������, ���� ������� ������?, Ready business, �����������, �������� � ��������, ���������, ����������, �����������"> 
	<meta http-equiv="content-type" content="text/html; charset=windows-1251"/>
        <LINK href="./general.css" type="text/css" rel="stylesheet">
  </HEAD>


<?
    $topmenu = GetMenu();
    $smarty->assign("topmenu", $topmenu);

    $link_country.= "&nbsp;&nbsp;<a href='".NAMESERVER."help.php' class='city_a'  style='color:white;font-size:10pt;color:yellow;' title='������ - ������� �������' target='_blank'>������</a>&nbsp;&nbsp;";	
    $smarty->assign("ad", $link_country);

    $smarty->display("./site/headerbuy.tpl");
?>
  


<table class="w" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td width="100%">


       		<?
                    if (isset($code) && $code > 0 && (int)$insertSettingsData['type_show'] == ConstInsertSettings::TYPE_SHOW_PAY) {

                        $pay = $smarty->fetch("./site/confirmpaymentbuyer.tpl");
                            echo $pay;

                    }  else if (isset($code) && $code > 0 && in_array($insertSettingsData['type_show'], array(ConstInsertSettings::TYPE_SHOW_NO_PAY, ConstInsertSettings::TYPE_SHOW_PROMO)) ) {
                          ?>

                            <table width='100%' align='center' bgcolor='#EDEDCC' style='border-top: 1px; border-bottom: 1px; border-left:1px;  border-right:1px;  border-color:#C1C1A4;  border-style: solid;' cellpadding="0" cellspacing="0" >
                                <tr>
                                  <td valign='top' style='padding-left:5pt;padding-right:5pt;'>
                                    <h2>���� ���������� �� ������� ������� �������!</h2>
                                  </td>
                                </tr>
                            </table>            

                          <?
                    } else {
         	?>
                    <table width='100%' align='center' bgcolor='#EDEDCC' style='border-top: 1px; border-bottom: 1px; border-left:1px;  border-right:1px;  border-color:#C1C1A4;  border-style: solid;' cellpadding="0" cellspacing="0" >

                        <tr>
                             <td  valign='top' style='padding-left:5pt;padding-right:5pt;'>
                                <div align="center" style="padding-top:5pt;padding-bottom:5pt;font-size:14pt;font-weight:bold;">���������� ������ �� ������� ������� (� ������� ������)</div>
                                    <?
                                        echo $insertSettingsData['content'];
                                        if($insertSettingsData['use_promo']) {
                                            
                                            $promo = $smarty->fetch("./site/promo.tpl");
                                            echo $promo;
                                        }
                                    ?>
                             </td>
                             <td width="25%" valign="top" style="padding-top:5pt;padding-right:5pt;">
                                <?
                                   $out_req = $smarty->fetch("./site/requestmin.tpl");
                                   echo $out_req;
                                ?>
                             </td>
                         </tr>

                    </table>

                     <table width='100%' height='100%'>
                       <?
                          if (strlen($error) > 0) { 
                       ?> 
                       <tr  valign='top' width='100%' height='100%'>
                         <td align='center'><span style="color:red;"><b><?=$error;?></b></span></td> 
                       </tr>
                       <?
                          }
                       ?> 
                       <tr>
                         <td valign='top' width='60%' height='100%'>
                           <?
                              if($blocking) {
                                  $smarty->display("./site/blockip.tpl");
                              }  else {
                                  $smarty->display("./site/insertbuy.tpl");
                              }
                           ?>
                         </td>
                       </tr>
                    </table>
         	<?
         }
       ?>
        
        </td>
    </tr>
</table>


<style>
 td a 
 {
   color:black;
 }
</style>


<div style="padding-top:4pt;padding-bottom:5pt;padding-right:10pt;padding-left:10pt;font-size:10pt;font-family:arial;">
	<table cellpadding="0" cellspacing="0" border="0"  style="width:100%;padding-bottom:2pt;" bgcolor="#eeeee0">
    	<tr>
        	<td style="background-image:url(<?=$_SERVER["host_name"]?>images/d1.gif); background-repeat:no-repeat;width:5px;height:5px; background-position:left top;">&nbsp;</td>
            <td rowspan="2" valign="middle" align="center"  style="font-size:8pt; font-family:Arial; color:black;">
			<?
			?>
            </td>
            <td style="background-image:url(<?=$_SERVER["host_name"]?>images/d2.gif); background-repeat:no-repeat;width:5px;height:5px;background-position:right top;">&nbsp;</td>
        </tr>
        <tr>
            <td style="background-image:url(<?=$_SERVER["host_name"]?>images/d4.gif); background-repeat:no-repeat;width:5px;height:5px; background-position:left bottom;">&nbsp;</td>
            <td style="background-image:url(<?=$_SERVER["host_name"]?>images/d3.gif); background-repeat:no-repeat;width:5px;height:5px; background-position:right bottom;">&nbsp;</td>
        </tr>
    </table>
</div>



<?
	$smarty->display("./site/footer.tpl");
?>

<?
  $end = get_formatted_microtime();
  $total = $end - $start;
  echo "<center><span style='font-size:7pt;'>".round($total, 6)."</span><center>";
?>

</body>
</html>