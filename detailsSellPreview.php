<?
    global $DATABASE;

    include_once("./phpset.inc");
    include_once("./engine/functions.inc"); 
    include_once("./engine/sell_functions.inc"); 
    require_once("./engine/Cache/Lite.php");

    $start = get_formatted_microtime(); 

    $count_sql_call = 0;

    AssignDataBaseSetting(); 
 
    //ini_set("display_startup_errors", "on");
    //ini_set("display_errors", "on");
    //ini_set("register_globals", "on");

    include_once("./engine/class.sell.inc"); 
    $sell = new Sell();

    require_once("./libs/Smarty.class.php");
    $smarty = new Smarty;
    $smarty->template_dir = "./templates";
    $smarty->compile_dir  = "./templates_c";
    $smarty->cache_dir = "./cache";
    $smarty->compile_check = true;
    $smarty->debugging     = false;
    //$smarty->caching = true; 

    require_once("./regfuncsmarty.php");

    $assignDescription = AssignDescription();

    $sell = new Sell();

    /*
    $sell->Loging(array(
        'filename' => './engine/log/trace.txt',
        'message' => $_SERVER["REMOTE_ADDR"]." ".date("F j, Y, g:i a")." preview: ".(isset($_SESSION['previewsell']) ? 'true' : 'false')."\r\n", 
    ));    
    */
    
    $resultGetItem = $sell->GetItemPreView();

    
    $smarty->assign("datasell", $resultGetItem);
    
    $BizFormID =  NameBizFormByID($resultGetItem->BizFormID);
    $ReasonSellBizID = NameReasonSellBizByID($resultGetItem->ReasonSellBizID);
    $smarty->assign("BizFormID", $BizFormID); 
    $smarty->assign("ReasonSellBizID", $ReasonSellBizID);		
    
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
    <HEAD>
        <script src="http://www.bizzona.ru/js/jquery.min.js"></script>
        <script src="http://www.bizzona.ru/js/jquery.colorbox.js"></script>        
        <LINK href="http://www.bizzona.ru/general.css" type="text/css" rel="stylesheet">
        <link media="screen" rel="stylesheet" href="http://www.bizzona.ru/css/colorbox.css" />
        <meta http-equiv="content-type" content="text/html; charset=windows-1251"/>
        <link rel="shortcut icon" href="http://www.bizzona.ru/images/favicon.ico">
    </HEAD>

<table class="w" border="0" cellpadding="0" cellspacing="0" height="100%"  bgcolor="#eeeee0">
    <tr> 
        <td valign="top" style="padding:2pt;" colspan="2">
                <div style='font-family:arial;font-size:11pt;border-top: 1px; border-bottom: 1px; border-left:1px;  border-right:1px;  border-color:#C1C1A4;  border-style: solid;padding:5px;background-color:#ffffff;' >
                    <?
                        $output_detalssellleft = $smarty->fetch("./site/detalssellleft.tpl");
                        echo minimizer($output_detalssellleft);
                    ?>
                </div>		
        </td>
    </tr>
    <tr>
        <td colspan="2" align="center">
            <table  align="center">
                <tr>
                    <td>
                        <? if(isset($_SESSION['newname_ImgID'])): ?>
                            <img src="http://www.bizzona.ru/simg/<?=$_SESSION['newname_ImgID'];?>" width="100" height="100">
                        <? endif ?>
                    </td>
                    <td>
                        <? if(isset($_SESSION['newname_Img1ID'])): ?>
                            <img src="http://www.bizzona.ru/simg/<?=$_SESSION['newname_Img1ID'];?>" width="100" height="100">
                        <? endif ?>
                    </td>
                    <td>
                        <? if(isset($_SESSION['newname_Img2ID'])): ?>
                            <img src="http://www.bizzona.ru/simg/<?=$_SESSION['newname_Img2ID'];?>" width="100" height="100">
                        <? endif ?>
                    </td>
                    <td>
                        <? if(isset($_SESSION['newname_Img3ID'])): ?>
                            <img src="http://www.bizzona.ru/simg/<?=$_SESSION['newname_Img3ID'];?>" width="100" height="100">
                        <? endif ?>
                    </td>
                    <td>
                        <? if(isset($_SESSION['newname_Img4ID'])): ?>
                            <img src="http://www.bizzona.ru/simg/<?=$_SESSION['newname_Img4ID'];?>" width="100" height="100">
                        <? endif ?>
                    </td>
                    
                </tr>
            </table>
        </td>
    </tr>
</table>
</body>
</html>
