<?
	global $DATABASE;
  	include_once("./phpset.inc");
  	include_once("./engine/functions.inc");

  	//mail("eugenekurilov@gmail.com", 1, 1);
  
  	include_once("./seterror.php");
  
  	//ini_set("display_startup_errors", "on");
  	//ini_set("display_errors", "on");
  	//ini_set("register_globals", "on");  	
  	
  	$start = get_formatted_microtime(); 

  	AssignDataBaseSetting();
  
 	include_once("./engine/class.equipment_communication_lite.inc");  
  	$equipment = new Equipment_Communication_Lite();
  	
  	include_once("./engine/class.communication.inc"); 
  	$communication = new Communication();   	
  	
  	require_once("./libs/Smarty.class.php");
  	$smarty = new Smarty;
  	$smarty->template_dir = "./templates";
  	$smarty->compile_dir  = "./templates_c";
  	$smarty->cache_dir = "./cache";
  	$smarty->compile_check = true;
  	$smarty->debugging     = false;  

  	$typeId = 4;
  	
  	if(isset($_GET["typeId"])) {
  		$typeId = intval($_GET["typeId"]);	
  	}
  	
  	$objectId = 0;
  	
  	if(isset($_GET["typeId"]) && in_array(intval($_GET["typeId"]), array(1,2,3))) {
  		$typeId = 	intval($_GET["typeId"]);
  		$_GET["typeId"] = $typeId;
  	}
  	
  	if(isset($_GET["objectId"])) {
  		$objectId = intval($_GET["objectId"]);	
  		$smarty->assign("objectId", $objectId);
  	}
  	
  	$equipment->id = $objectId;
  	$equipment_item = $equipment->GetItemForCommunication();

	if (isset($_POST) && sizeof($_POST) > 0 && isset($_POST["order"])) {
		if (strlen(trim($_POST["order_phone"])) <= 0 && strlen(trim($_POST["order_email"])) <= 0) {
			
			$smarty->assign("res_order", "������: �� ������� ��������!");
			
		} else {
		
			if(strlen(trim($_POST["order_tax"])) <= 0) {
		
				$communication->name = $_POST["order_name"];
				$communication->phone = $_POST["order_phone"];
				$communication->email = $_POST["order_email"];
				$communication->count = $_POST["order_count"];
				$communication->message = $_POST["order_description"];		
				$communication->objectId = $objectId;
				$communication->ownerEmail = $equipment_item->email;
				$communication->typeId = $typeId;
				$communication->repeatme = (isset($_POST["order_repeat"]) ? 1 : 0);

				$result = $communication->InsertEquipment();
		
				if($result) {
					$res_order = "��� ����� ������. ����� ������";
				} else {
					$res_order =  "��������� ������ ��� �������� ���������, ����������, ��������� �����";
				}
		
				if($typeId == 6) {
					$res_order = "��� ������ ��� ������� ���������. ����� ������";	
				}
				
				$smarty->assign("res_order", $res_order);
		
				if(strlen($communication->name) > 0) {
					$_SESSION["fml"] = $communication->name;
				}
				
				if(strlen($communication->email) > 0) {
					$_SESSION["email"] = $communication->email;
				}

				if(strlen($communication->phone) > 0) {
					$_SESSION["phone"] = $communication->phone;
				}				
				
			}
		}
	}   	

	if(isset($_SESSION["fml"])) {
		$smarty->assign("order_name", $_SESSION["fml"]);
	}
  	
	if(isset($_SESSION["email"])) {
		$smarty->assign("order_email", $_SESSION["email"]);
	}

	if(isset($_SESSION["phone"])) {
		$smarty->assign("order_phone", $_SESSION["phone"]);
	}	
	
  	$smarty->assign("equipment_item", $equipment_item);
  	
  	$title = "���������� ������ �� ������� ������������";
  	
  	$message = "������ ����, � ���� ���������� ������������ http://www.bizzona.ru/detailsEquipment/".$equipment->id.". ��������� �� ����, ����������!";

  	if($typeId == 6) {
  		$title = "������ ������ �� ������������";
  		$message = "������ ����, � ���� ������ ��������� ������ �� ������������ http://www.bizzona.ru/detailsEquipment/".$equipment->id.". ";
  	}
  	
  	
  	$smarty->assign("title", $title);
  	$smarty->assign("message", $message);
  	
  	?>
  		<html>
  			<head>
  				<LINK href="./general.css" type="text/css" rel="stylesheet">
  			</head>
  			<body bgcolor="#eeeee0">
  	<?
  	
  	if($typeId == 4 or $typeId == 6) {
		$communication_equipment = $smarty->fetch("./site/communication_equipment.tpl");
		echo fminimizer($communication_equipment);  		
  	} 

  	?>
  			</body>
  		</html>
  	<?
  	
  	
?>