<?php
    //ini_set("session.use_cookies", "0");
    //ini_set("session.use_trans_sid", "1");
    //session_cache_limiter("private");
 	//header("Cache-Control: no-store, no-cache, must-revalidate");


	session_start(); 
	
	
	global $DATABASE;
	include_once("../phpset.inc");
	include_once("../engine/functions.inc");

	AssignDataBaseSetting("../config.ini");	
	 
	ini_set("display_startup_errors", "on");
	ini_set("display_errors", "on");
	ini_set("register_globals", "on");
	
	
	if(isset($_GET["exit"])) {
		if(isset($_SESSION["brokerId"])) unset($_SESSION["brokerId"]);
		session_destroy();
	}	
	
	require_once("../libs/Smarty.class.php");
	$smarty = new Smarty;
	$smarty->template_dir = "../templates";
	$smarty->compile_dir  = "../templates_c";
	$smarty->cache_dir = "../cache";
	$smarty->compile_check = false; 
	$smarty->debugging     = false;
	$smarty->clear_all_cache();		
	 
	$smarty->caching = false; 
	$smarty->clear_compiled_tpl();
	
	include_once("../engine/class.company.inc"); 
	$company = new Company();	

	include_once("../engine/class.sell.inc");
	$sell = new Sell();	
	
	include_once("../engine/class.tariff.inc");
	$tariff = new Tariff();	

			?><pre><? 
			var_dump($_SESSION);
			?><pre><?
	
	
	
	if(isset($_SESSION["brokerId"]) && isset($_GET["closeID"]) && intval($_GET["closeID"]) > 0) {
		
		$dataclose["ID"] = intval($_GET["closeID"]);
		$dataclose["brokerId"] = intval($_SESSION["brokerId"]);
		$sell->CabinetCloseSell($dataclose);
	}

	if(isset($_SESSION["brokerId"]) && isset($_GET["openID"]) && intval($_GET["openID"]) > 0) {
		
		$dataopen["ID"] = intval($_GET["openID"]);
		$dataopen["brokerId"] = intval($_SESSION["brokerId"]);
		$sell->CabinetOpenSell($dataopen);
	}	
	
	if(isset($_SESSION["brokerId"]) && isset($_GET["updateID"]) && intval($_GET["updateID"]) > 0) {
		
		$dataupdate["ID"] = intval($_GET["updateID"]);
		$dataupdate["brokerId"] = intval($_SESSION["brokerId"]);
		$sell->CabinetUpdateSell($dataupdate);
	}	
	
	echo "<h1>".$_SESSION["brokerId"]."</h1>";
	echo "<h1>".session_id()."</h1>";
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<script src="../js/jquery.js"></script>
	<link rel="stylesheet" href="../js/ui.tabs.css" type="text/css" media="screen">
	<LINK href="../general.css" type="text/css" rel="stylesheet">
	<script type="text/javascript" src="../js/ui.base.js"></script>
</head>
<body bgcolor="#d9d8b9">
<?

	if(isset($_POST["loginID"]) && isset($_POST["passwordID"])) {
		
		$_loginID = htmlspecialchars(trim($_POST["loginID"]), ENT_QUOTES);
		$_passwordID = htmlspecialchars(trim($_POST["passwordID"]), ENT_QUOTES);
		
		$company->login = $_loginID;
		$company->password = $_passwordID;
		$company->GetItemAuth();
		
		if(intval($company->Id) > 0) {
			$_SESSION['brokerId'] = $company->Id;
			$_SESSION['loginID'] = $company->login;
			$_SESSION['passwordID'] = $company->password;
			
			$_SESSION['company'] = serialize($company);
			
			$tariff->Id = $company->tariffId;
			$_tariff = $tariff->GetItem();
			
			$_SESSION['tariff'] = serialize($_tariff);
			
			?><pre><? 
			var_dump($_SESSION);
			?><pre><?
			
		}
		
	}
 
	if(empty($_SESSION['brokerId'])) {
	
		$output_cabinet = $smarty->fetch("./site/broker/login_test.tpl");
		echo minimizer($output_cabinet);
		
	} else {
		
		$data["BrokerCabinet"] = 1;
		$data["brokerId"] = $_SESSION['brokerId'];
		$data["offset"] = 0;
		$data["rowCount"] = 0;
		$data["sort"] = "ID";
		
		$result_sell = $sell->CabinetDetailsSelect($data);
		
		$count_open = 0;
		
		while (list($k, $v) = each($result_sell)) {
			
			if($v->StatusID == 1 || $v->StatusID == 2) {
				$count_open+=1;
			}
		}
		
		reset($result_sell);
		
		$_SESSION["count_open"] = $count_open;
		$_SESSION["totalsell"] = sizeof($result_sell);

		$smarty->assign("totalsell", $_SESSION["totalsell"]);
		$smarty->assign("count_open", $_SESSION["count_open"]);
		
		$smarty->assign("listsell", $result_sell);
		
		$company = unserialize($_SESSION['company']);
		$smarty->assign("companyname", $company->companyName);
		$smarty->assign("datestart", $company->datestart);
		$smarty->assign("datestop", $company->datestop);
		
		$tariff = unserialize($_SESSION['tariff']);
		$smarty->assign("tariffname", $tariff->Name);

		$output_index = $smarty->fetch("./site/broker/cabinet_test.tpl");
		echo minimizer($output_index);		
		 
		
	}

	
?>
</body>
<?	
	
?>