<?php
    setlocale(LC_ALL, 'ru_RU.CP1251');
    setlocale(LC_TIME, "ru_RU.CP1251");
    ini_set("memory_limit","128M");    

    #session_start();  

    function PureHtmlSimple($str) {
        return strip_tags($str, '<ul><li><ol><p><strong><b><a><span><em><table><tr><td><th><sub><sup>');        
    }     
    
    global $DATABASE;
    include_once("../phpset.inc");
    include_once("../engine/functions.inc");
    require_once("../engine/Cache/Lite.php");

    AssignDataBaseSetting("../config.ini");	

    ini_set("display_startup_errors", "off");
    ini_set("display_errors", "off");
    ini_set("register_globals", "on");

    $count_sql_call = 0;
    $start = get_formatted_microtime();   
    $base_memory_usage = memory_get_usage();	

    $auto_update_period = 14;

    if(isset($_GET["exit"])) {
        if(isset($_SESSION["brokerId"])) unset($_SESSION["brokerId"]);
    }	

    require_once("../libs/Smarty.class.php");
    $smarty = new Smarty;
    $smarty->template_dir = "../templates";
    $smarty->compile_dir  = "../templates_c";
    $smarty->cache_dir = "../cache";
    $smarty->compile_check = false; 
    $smarty->debugging     = false;
    $smarty->clear_all_cache();		

    $smarty->caching = false; 
    $smarty->clear_compiled_tpl();

    include_once("../engine/class.company.inc"); 
    $company = new Company();	

    include_once("../engine/class.sell.inc");
    $sell = new Sell();	

    //include_once("../engine/class.tariff.inc");
    //$tariff = new Tariff();	

    include_once("../engine/class.services.inc");
    $services = new Services();


    include_once("../engine/class.buy.inc"); 
    $buyer = new Buyer();

    function  ClearCacheSell($id) {

        global $smarty;

        $smarty->cache_dir = "../sellcache";

        $useMapGoogle = true;
        $smarty->clear_cache("./site/detailsSell.tpl", intval($id)."-".$useMapGoogle);
        $useMapGoogle = false;
        $smarty->clear_cache("./site/detailsSell.tpl", intval($id)."-".$useMapGoogle);		

        $smarty->cache_dir = "../cache";

        $options = array(
                'cacheDir' => "../sellcache/",
                'lifeTime' => 0
        );

        $cache = new Cache_Lite($options);
        $cache->remove(''.intval($id).'____sellitem');


    } 

    if(isset($_SESSION["brokerId"])) {
        $auto_update_period = $company->GetAutoUpdatePeriod((int)$_SESSION["brokerId"]);   
        $access_call_robot = $company->getAccessCallRobot((int)$_SESSION["brokerId"]); 
        
        if(isset($_GET['robot_import']) && (int)$_GET['robot_import'] == 1 && $access_call_robot) {
            $url_import_robot = $company->getUrlImportRobot((int)$_SESSION["brokerId"]); 
            if(strlen($url_import_robot) > 0) {
                file_get_contents($url_import_robot);
            }
        }
        
    }
    
    if(isset($_SESSION["brokerId"]) && isset($_GET["closeID"]) && intval($_GET["closeID"]) > 0) {

        $dataclose["ID"] = intval($_GET["closeID"]);
        $dataclose["brokerId"] = intval($_SESSION["brokerId"]);
        $sell->CabinetCloseSell($dataclose);

        ClearCacheSell($dataclose["ID"]);
    }

    if(isset($_SESSION["brokerId"]) && isset($_GET["openID"]) && intval($_GET["openID"]) > 0) {

            $dataopen["ID"] = intval($_GET["openID"]);
            $dataopen["brokerId"] = intval($_SESSION["brokerId"]);
            $sell->CabinetOpenSell($dataopen);

            ClearCacheSell($dataopen["ID"]);
    }	

    if(isset($_SESSION["brokerId"]) && isset($_GET["updateID"]) && intval($_GET["updateID"]) > 0) {

        $dataupdate["ID"] = intval($_GET["updateID"]);
        $dataupdate["brokerId"] = intval($_SESSION["brokerId"]);
        $dataupdate["auto_update_period"] = $auto_update_period;
        $sell->CabinetUpdateSell($dataupdate);

        ClearCacheSell($dataupdate["ID"]);
    }	
	
    include_once("../engine/class.ContactInfo_Lite.inc");
    $contact = new ContactInfo_Lite();  

    $contact->GetContactInfo();  
    $smarty->assign("contact", $contact); 
      
    $smarty->assign("years", array_reverse(range(2010, date("Y")))); 
    
    $smarty->assign("access_call_robot", $access_call_robot);
    
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<script src="../js/jquery.js"></script>
	<link rel="stylesheet" href="../js/ui.tabs.css" type="text/css" media="screen">
	<LINK href="../general.css" type="text/css" rel="stylesheet">
	<script type="text/javascript" src="../js/ui.base.js"></script>
	<title>������ ������� ������� - BizZONA.ru</title>
</head>
<body bgcolor="#d9d8b9">
<?php

        // ���������� ����������� �������� ����� � ������� ������� ����� �������
        if(isset($_GET["loginID"]) && isset($_GET["passwordID"])) {
            $_POST["loginID"] = trim($_GET["loginID"]);
            $_POST["passwordID"] = trim($_GET["passwordID"]);
        }

	if(isset($_POST["loginID"]) && isset($_POST["passwordID"])) {
		
            $_loginID = htmlspecialchars(trim($_POST["loginID"]), ENT_QUOTES);
            $_passwordID = htmlspecialchars(trim($_POST["passwordID"]), ENT_QUOTES);

            $company->login = $_loginID;
            $company->password = $_passwordID;
            $company->GetItemAuth();

            if(intval($company->accessId) == 2) {
                $smarty->assign("blocked", "������������ (��� ������)");
                unset($_SESSION['brokerId']);
            } else if (intval($company->Id) > 0) {
                $_SESSION['brokerId'] = $company->Id;
                $_SESSION['loginID'] = $company->login;
                $_SESSION['passwordID'] = $company->password;
                $_SESSION['paystatus'] = $company->paystatus;

                $_SESSION['company'] = serialize($company);

                $services->_id = $company->tariffId;
                $_tariff = $services->GetItem();

                $_SESSION['tariff'] = serialize($_tariff);

                $auto_update_period = $company->GetAutoUpdatePeriod((int)$company->Id);     
            }
		
	}
 
	if(empty($_SESSION['brokerId'])) {
	
		$output_cabinet = $smarty->fetch("./site/broker/login.tpl");
		echo minimizer($output_cabinet);
		
	} else { 
            
                $company->login = $_SESSION['loginID'];
                $company->password = $_SESSION['passwordID'];
                $company->GetItemAuth();
     

                if(!$company->Id) {
                    
                    unset($_SESSION['brokerId']);
                    $output_cabinet = $smarty->fetch("./site/broker/login.tpl");
                    echo minimizer($output_cabinet);
                    
                } else {


                    $access_call_robot = $company->getAccessCallRobot($company->Id);
                    $smarty->assign('access_call_robot', $access_call_robot); 


                    if(isset($_GET['message'])) {
                    
                        include_once("../engine/class.communication.inc");
                        $communication = new Communication();        
                         
                        $aComm = $communication->Select(array(
                            'limit' => 200, 
                            'brokerId' => 1,
                            'brokerId' =>  (int)$_SESSION["brokerId"],
                        ));

                        $smarty->assign("aComm", $aComm);                
                    }
                    
                    if(isset($_GET['processing'])) {
                        
                        include_once("../engine/class.HistoryRequestBrokers.inc"); 
                        $historyRequest = new HistoryRequestBrokers();
                        $historyRequest->brokerId = (int)$_SESSION["brokerId"];
                        $aProcessing = $historyRequest->Select();
                        $smarty->assign("aProcessing", $aProcessing);
                    }
                    
                    if(isset($_GET['relation'])) {
                        
                        include_once("../engine/class.mappingobjectbroker.inc"); 
                        $mapping = new MappingObjectBroker();
                        $mapping->brokerId = (int)$_SESSION["brokerId"];
                        $aRelations = $mapping->RelationObjects(); 
                        $smarty->assign("aRelations", $aRelations);
                    }                    
                    
                    if(isset($_GET['addsell'])) {

                        include_once("../engine/class.category.inc"); 
                        $category = new Category();

                        include_once("../engine/class.subcategory.inc"); 
                        $subCategory = new SubCategory();                        
                        
                        $listCategory = $category->Select(array(
                            "offset" => 0, 
                            "rowCount" => 0, 
                            "sort" => 'Count',
                        )); 
                        $smarty->assign("listCategory", $listCategory);    
                        $listSubCategory = $subCategory->Select(array(
                            "offset" => 0, 
                            "rowCount" => 0, 
                        ));
                        $smarty->assign("listSubCategory", $listSubCategory);
                        
                        
                    }
                    
                    if(isset($_POST['addsell'])) { 

                        include_once('../inserthelp.php');
                        
                        $newname_ImgID = LoadClientImg("ImgID", 1);
                        $newname_Img1ID = LoadClientImg("Img1ID", 2);
                        $newname_Img2ID = LoadClientImg("Img2ID", 3);
                        $newname_Img3ID = LoadClientImg("Img3ID", 4);
                        $newname_Img4ID = LoadClientImg("Img4ID", 5);
                        
                         
                        $dataSell["ImgID"]  = (filesize("../simg/".$newname_ImgID) > 0 ? $newname_ImgID : ''); 
                        $dataSell["Img1ID"] = (filesize("../simg/".$newname_Img1ID) > 0 ? $newname_Img1ID : ''); 
                        $dataSell["Img2ID"] = (filesize("../simg/".$newname_Img2ID) > 0 ? $newname_Img2ID : ''); 
                        $dataSell["Img3ID"] = (filesize("../simg/".$newname_Img3ID) > 0 ? $newname_Img3ID : ''); 
                        $dataSell["Img4ID"] = (filesize("../simg/".$newname_Img4ID) > 0 ? $newname_Img4ID : '');                         
                        
                        $dataSell['CostID'] = addslashes(str_replace(' ', '', trim($_POST['CostID'])));
                        $dataSell['cCostID'] = addslashes($_POST['cCostID']);
                        $dataSell['NameBizID'] = addslashes(trim($_POST['NameBizID']));
                        $dataSell['ShortDetailsID'] = addslashes(PureHtmlSimple(trim($_POST['ShortDetailsID'])));
                        $dataSell['ProfitPerMonthID'] = addslashes(str_replace(' ', '', trim($_POST['ProfitPerMonthID'])));
                        $dataSell['MaxProfitPerMonthID'] = addslashes(str_replace(' ', '', trim($_POST['MaxProfitPerMonthID'])));
                        $dataSell['cProfitPerMonthID'] = addslashes($_POST['cProfitPerMonthID']);
                        $dataSell['cMaxProfitPerMonthID'] = addslashes($_POST['cMaxProfitPerMonthID']);

                        $dataSell['MonthAveTurnID'] = addslashes(str_replace(' ', '', trim($_POST['MonthAveTurnID'])));
                        $dataSell['MaxMonthAveTurnID'] = addslashes(str_replace(' ', '', trim($_POST['MaxMonthAveTurnID'])));
                        $dataSell['cMonthAveTurnID'] = addslashes($_POST['cMonthAveTurnID']);
                        $dataSell['cMaxMonthAveTurnID'] = addslashes($_POST['cMaxMonthAveTurnID']);

                        $dataSell['MonthExpemseID'] = addslashes(str_replace(' ', '', trim($_POST['MonthExpemseID'])));
                        $dataSell['cMonthExpemseID'] = addslashes($_POST['cMonthExpemseID']);

                        $dataSell['TimeInvestID'] = addslashes($_POST['TimeInvestID']);

                        $dataSell['WageFundID'] = addslashes(str_replace(' ', '', trim($_POST['WageFundID'])));
                        $dataSell['cWageFundID'] = addslashes($_POST['cWageFundID']);

                        $dataSell['ArendaCostID'] = addslashes(str_replace(' ', '', trim($_POST['ArendaCostID'])));
                        $dataSell['cArendaCostID'] = addslashes($_POST['cArendaCostID']);

                        $dataSell['ShareSaleID'] = addslashes($_POST['ShareSaleID']);
                        $dataSell['maxShareSaleID'] = addslashes($_POST['maxShareSaleID']);

                        $dataSell['SumDebtsID'] = addslashes(str_replace(' ', '', trim($_POST['SumDebtsID'])));
                        $dataSell['cSumDebtsID'] = addslashes($_POST['cSumDebtsID']);

                        $dataSell['TermBizID'] = addslashes($_POST['TermBizID']);
                        $dataSell['CountEmplID'] = addslashes($_POST['CountEmplID']);
                        $dataSell['CountManEmplID'] = addslashes($_POST['CountManEmplID']);
                        $dataSell['ContactID'] = addslashes($_POST['ContactID']);


                        $dataSell['DetailsID'] = addslashes(PureHtmlSimple(trim($_POST['DetailsID']))); 

                        $dataSell['MatPropertyID'] = addslashes(PureHtmlSimple($_POST['MatPropertyID']));
                        $dataSell['ListObjectID'] = addslashes(PureHtmlSimple($_POST['ListObjectID']));

                        $dataSell['FML'] = addslashes(trim($_POST['FML']));
                        $dataSell['EmailID'] = addslashes(trim($_POST['EmailID']));
                        $dataSell['PhoneID'] = addslashes(trim($_POST['PhoneID']));
                        $dataSell['FaxID'] = addslashes($_POST['FaxID']);
                        $dataSell['SiteID'] = addslashes(trim($_POST['SiteID']));

                        $dataSell['AgreementCostID'] = (isset($_POST['AgreementCostID']) ? 1 : 0);
                        $dataSell['torgStatusID'] = (isset($_POST['torgStatusID']) ? 1 : 0);
                        $dataSell['BizFormID'] = addslashes($_POST['BizFormID']);
                        $dataSell['ReasonSellBizID'] = addslashes($_POST['ReasonSellBizID']);

                        $aCategoryID = split("[:]",$_POST["CategoryID"]);
                        $dataSell['CategoryID'] = (isset($aCategoryID[0]) ? $aCategoryID[0] : 0);
                        $dataSell['SubCategoryID'] = (isset($aCategoryID[1]) ? $aCategoryID[1] : 0);
 
                        $dataSell['brokerId'] = $company->Id;
                        $dataSell["source"] = '������� ������� � '.$company->Id; 
                        
                        $aError = array();
                        
                        if(strlen($dataSell['NameBizID']) <= 1) {
                            $aError[] = "�� ������� ������������ �������";
                        } 
                        
                        if(strlen($dataSell['ShortDetailsID']) <= 1) {
                            $aError[] = "�� ������� ������� �������� �������";
                        }                        
                        
                        if(strlen($dataSell['DetailsID']) <= 1) {
                            $aError[] = "�� ������� ��������� �������� �������";
                        }                        
                        
                        if(strlen($dataSell['FML']) <= 1) {
                            $aError[] = "�� ������� ��� ��� ��������� ����������";
                        }                        
                        
                        if(strlen($dataSell['EmailID']) <= 1) {
                            $aError[] = "�� ������ email ��� ��������� ����������";
                        }                        
                        
                        if(strlen($dataSell['PhoneID']) <= 1) {
                            $aError[] = "�� ������ ������� ��� ��������� ����������";
                        }                        
                        
                        if(strlen($dataSell['SiteID']) <= 1) {
                            $aError[] = "�� ������� ������������ �������";
                        }                        
                        
                        if(count($aError)) {
                            
                            $smarty->assign('aError', $aError); 
                            
                        } else {
                        
                            $newId = $sell->Insert($dataSell);

                            if($newId > 0) {
                                //session_regenerate_id(true);
                                header ('Location: ../broker/'); 
                                //session_write_close(); 
                            }
                        
                        }
                    }
                    
                    
                     
                    
                    $data["BrokerCabinet"] = 1;
                    $data["brokerId"] = $_SESSION['brokerId'];
                    $data["offset"] = 0; 
                    $data["rowCount"] = 0;
                    $data["sort"] = "ID";
                    $data["auto_update_period"] = $auto_update_period; 

                    if(isset($_GET["statusId"])) {
                            $data["statusId"] = intval($_GET["statusId"]);
                            $_GET["statusId"] = $data["statusId"];
                    }
                     
                    if(isset($_GET["yearId"])) { 
                            $data["yearId"] = intval($_GET["yearId"]);
                            $_GET["yearId"] = $data["yearId"];
                    } 

                    if(isset($_GET["codeId"])) { 
                            $data["codeId"] = intval($_GET["codeId"]);
                            $_GET["codeId"] = $data["codeId"];
                    }                    
                    
                    
                    $result_sell = $sell->CabinetDetailsSelect($data);
                    $result_OpenUpdateCount = $sell->CabinetSellOpenUpdateCount($data);
                    $_SESSION["totalsell"] = $sell->CabinetDetailsSelectCount($data);

                    $count_open = 0;
                    $count_refresh = 0;

                    while (list($k, $v) = each($result_OpenUpdateCount)) {

                            if($v->StatusID == 1 || $v->StatusID == 2) {
                                    $count_open+=1;
                            }

                            if($v->UpdateStatusID == 1 && ($v->StatusID == 1 or $v->StatusID == 2)) {
                                    $count_refresh+=1;		
                            }

                    }

                    reset($result_OpenUpdateCount);

                    $_SESSION["count_open"] = $count_open;
                    $_SESSION["count_refresh"] = $count_refresh;

                    $smarty->assign("count_open", $_SESSION["count_open"]);
                    $smarty->assign("count_refresh", $_SESSION["count_refresh"]);
                    $smarty->assign("totalsell", $_SESSION["totalsell"]);

                    $smarty->assign("listsell", $result_sell);

                    $company = unserialize($_SESSION['company']);
                    $smarty->assign("companyname", $company->companyName);
                    $smarty->assign("datestart", $company->datestart);
                    $smarty->assign("datestop", $company->datestop);

                    $tariff = unserialize($_SESSION['tariff']);
                    if(is_array($tariff) && isset($tariff['shortName'])) {
                            $smarty->assign("tariffname", $tariff['shortName']);
                    }


                    $result_ListSellForBroker  = $sell->ListSellForBroker();
                    $smarty->assign("listsellforbroker", $result_ListSellForBroker);

                    $result_ListBuyerForBroker  = $buyer->ListBuyerForBroker();


                    $result_BuyerBroker  = $buyer->ListBuyerForBroker(intval($_SESSION['brokerId']));

                    $smarty->assign("listbuyerbroker", $result_BuyerBroker);
                    $smarty->assign("countlistbuyerbroker", sizeof($result_BuyerBroker));

                    $smarty->assign("listbuyerforbroker", $result_ListBuyerForBroker);


                    $output_index = $smarty->fetch("./site/broker/cabinet.tpl");
                    echo minimizer($output_index);		 
		 
                }
	}

	
?>
<?
  $end = get_formatted_microtime(); 
  $total = $end - $start;
  echo "<center><span style='font-size:7pt;'>".round($total, 6)." : ".$count_sql_call." : ".$base_memory_usage." : ".memoryUsage(memory_get_usage(), $base_memory_usage)." </span><center>";
?>

</body>
