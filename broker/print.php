<?php
    setlocale(LC_ALL, 'ru_RU.CP1251');
    setlocale(LC_TIME, "ru_RU.CP1251");

	session_start(); 
	
	global $DATABASE;
	//include_once("../phpset.inc");
	include_once("../engine/functions.inc");

	AssignDataBaseSetting("../config.ini");	
	
	ini_set("display_startup_errors", "off");
	ini_set("display_errors", "off");
	ini_set("register_globals", "off");
	
	require_once("../libs/Smarty.class.php");
	$smarty = new Smarty;
	$smarty->template_dir = "../templates";
	$smarty->compile_dir  = "../templates_c";
	$smarty->cache_dir = "../cache";
	$smarty->compile_check = false; 
	$smarty->debugging     = false;
	$smarty->clear_all_cache();		
	 
	$smarty->caching = false; 
	$smarty->clear_compiled_tpl();
	
	include_once("../engine/class.sell.inc");
	$sell = new Sell();	
	
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<script src="../js/jquery.js"></script>
	<link rel="stylesheet" href="../js/ui.tabs.css" type="text/css" media="screen">
	<LINK href="../general.css" type="text/css" rel="stylesheet">
	<script type="text/javascript" src="../js/ui.base.js"></script>
	<title>������ - ������ ������� ������� - BizZONA.ru</title>
</head>
<body>
<?
	if(empty($_SESSION['brokerId'])) {
		$output_cabinet = $smarty->fetch("./site/broker/login.tpl");
		echo minimizer($output_cabinet);
	} else {
		
		$data["BrokerCabinet"] = 1;
		$data["brokerId"] = $_SESSION['brokerId'];
		$data["offset"] = 0;
		$data["rowCount"] = 0;
		$data["sort"] = "ID";
		
		if(isset($_GET["statusId"])) {
			$data["statusId"] = intval($_GET["statusId"]);
		}
		
		$result_sell = $sell->CabinetDetailsSelect($data);
		
		$smarty->assign("listsell", $result_sell);
		
		$output_index = $smarty->fetch("./site/broker/print.tpl");
		echo minimizer($output_index);		
	}
?>
</body>
</html>