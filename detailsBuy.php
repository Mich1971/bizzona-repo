<?
    global $DATABASE;

    include_once("./phpset.inc");
    include_once("./engine/functions.inc");
    include_once("./engine/buy_functions.inc");
    require_once("./engine/Cache/Lite.php");  

    $count_sql_call = 0;
    $start = get_formatted_microtime();   
    $base_memory_usage = memory_get_usage();	

    AssignDataBaseSetting();

    include_once("./engine/class.category_lite.inc"); 
    $category = new Category_Lite();

    include_once("./engine/class.country_lite.inc"); 
    $country = new Country_Lite();

    include_once("./engine/class.city_lite.inc");  
    $city = new City_Lite();

    include_once("./engine/class.buy_lite.inc"); 
    $buyer = new Buyer_Lite();

    include_once("./engine/class.sell_lite.inc"); 
    $sell = new Sell_Lite();

    include_once("./engine/class.metro_lite.inc"); 
    $metro = new Metro_Lite();

    include_once("./engine/class.district_lite.inc"); 
    $district = new District_Lite();

    include_once("./engine/class.subcategory_lite.inc");
    $subcategory = new SubCategory_Lite();

    include_once("./engine/class.streets_lite.inc");
    $streets = new Streets_Lite();

    include_once("./engine/class.ad.inc");
    $ad = new Ad();

    include_once("./engine/class.frontend.inc");

    require_once("./libs/Smarty.class.php");
    $smarty = new Smarty;
    $smarty->template_dir = "./templates";
    $smarty->compile_dir  = "./templates_c";
    $smarty->cache_dir = "./cache";
    $smarty->compile_check = true;
    $smarty->debugging     = false;
    //$smarty->caching = true; 

    require_once("./regfuncsmarty.php");

    $assignDescription = AssignDescription();

    $dataBuyer = Array();
    $dataBuyer["ID"] = intval($_GET["ID"]);

    //$resultGetItem = $buyer->GetItem($dataBuyer);

    // up caching call details buyer
    $options = array(
        'cacheDir' => "buycache/",
        'lifeTime' => 2592000
    );

  
  
  if(isset($_GET["guid"]) && isset($_GET["statId"]) ) {
  	
  	$aStatId = array(1,2);
  	
    $statId = intval($_GET["statId"]);
  	$guid = htmlspecialchars(trim($_GET["guid"]), ENT_QUOTES);
  	$Id = $dataBuyer["ID"];
  	
  	if(in_array($statId, $aStatId)) {
  
  		$params = array('guid' => $guid, 'Id' => $Id, 'statId' => $statId);	
  		$buyer->ChangeCustomerStatus($params);
  		
  		$custom_status_text = ($statId == 1 ? '���� ���������� ������������' : '���� ���������� ����� � ����������');
  		
		// up update cache buy
  		$smarty->cache_dir = "./buycache";
  		$smarty->clear_cache("./site/detailsBuy.tpl", $Id);
  		$smarty->cache_dir = "./cache";
  		
  		$options = array(
  			'cacheDir' => "./buycache/",
   			'lifeTime' => 1
  		);
		
  		$cache = new Cache_Lite($options);
 		$cache->remove(''.intval($_GET["ID"]).'____buyitem');  		
  		// up update cache buy		
  	}
  	
  	unset($Id);
  	unset($statId);
  	unset($guid);
  	unset($aStatId);
  }
  

  
  
  $cache = new Cache_Lite($options);
		
  if(isset($custom_status_text)) {
  	$resultGetItem = $buyer->GetItem($dataBuyer);

  } else {
  		if ($data = $cache->get(''.$dataBuyer["ID"].'____buyitem')) {
			$resultGetItem = unserialize($data);
  		} else { 
			$resultGetItem = $buyer->GetItem($dataBuyer);
			$cache->save(serialize($resultGetItem));
  		}
  }
  
  
  $smarty->assign("data", $resultGetItem);
  // donw caching call details buyer  
  
  $headertitle = "";
  
  if (isset($resultGetItem->id) && $resultGetItem->statusID != 17) {
    $seoname = $resultGetItem->seoname;
    if(strlen($seoname) > 0) {
    	$seoname =  $seoname.", ";
    }
    if(strlen($resultGetItem->subtitle) > 0) {
  		$headertitle = $resultGetItem->subtitle;
  		$seoname = 	$headertitle;
  		$seoname = str_replace(".", ", ", $seoname);
    }
  } else {
  	header ("Location: http://www.bizzona.ru");
  }
  
  $data = array();
  $data["offset"] = 0;
  $data["rowCount"] = 3;
  $data["CategoryID"] = $resultGetItem->typeBizID;
  $data["CityID"] = $resultGetItem->regionID;
  $data["StatusID"] = "-1";
  $data["sort"] = "DataCreate";
  $resultsell = $sell->Select($data);

  $ad->InitAd($smarty, $_GET);
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
  <HEAD>
      <title><?=(strlen($headertitle) > 0 ? $headertitle : "������� � ������� �������.");?>   ����� <?=strtolower($resultGetItem->name);?>, ��� "������ ����"</title>		
		<META NAME="Description" CONTENT="������� �������, ������� �������, ������� ������, ������� �������, ������� �������� �������, ������� �������, ������� ��������,  ������� ��������, ������� ��������, ���� ������� ������?">
        <meta name="Keywords" content="<?=$seoname;?>������� �������, ������� ��������, ����� <?=$resultGetItem->name;?>"> 
        <LINK href="http://www.bizzona.ru/general.css" type="text/css" rel="stylesheet">
        <meta http-equiv="content-type" content="text/html; charset=windows-1251"/>
		<script type="text/javascript" src="http://www.bizzona.ru/ja.js"></script>        
  </HEAD>

<?
  	$smarty->display("./site/headerbanner.tpl");
?>

<table class="w" border="0" cellpadding="0" cellspacing="0" height="100%">
	<tr>
		<td colspan="2">
		<?
			$smarty->caching = false; 
	
			$smarty->assign("headertitle", $headertitle);
	
			$topmenu = GetMenu();
			$smarty->assign("topmenu", $topmenu);

			$link_country = GetSubMenu();
			$smarty->assign("ad", $link_country);

			echo minimizer($smarty->fetch("./site/headerbuy.tpl"));	
	
			$smarty->caching = true;
		?>		
		</td>
	</tr>
	<?
		if(isset($custom_status_text)) {
			?>
			<tr>
				<td colspan="2" align="center" style="font-size:14pt;color:green;font-weight:bold;">
					<?=$custom_status_text;?>
				</td>
			</tr>
			<?
		}
	?>
	<tr>
		<td colspan="2">
			<?
				echo UpPositionAndShare(intval($_GET["ID"]));
			?>
		</td>
	</tr>	
    <tr>
        <td width="70%" valign="top" style="padding:2pt;">
			<div style="width:auto;height:100%;">
       			
       			<?
   				 echo BuyHeaderShow($resultGetItem->id, $resultGetItem->datecreate);
   				 
                 if (isset($resultGetItem->id)) {
                 	
        			 if(isset($custom_status_text)) {
        			 	$smarty->caching = false; 
        			 } else {
        			 	$smarty->caching = true; 	
        			 }
                 	
                     $smarty->cache_dir = "./buycache";
                   	 $smarty->cache_lifetime = 86400*30;                     

                     $smarty->display("./site/detailsBuy.tpl", $resultGetItem->id);
                     
                     $smarty->cache_dir = "./cache";
                     $smarty->caching = false;  
                     
					 echo BuyHeaderShow($resultGetItem->id, $resultGetItem->datecreate);
                     
					 
                     ShowDetailsSearchBuyerForm();

					 SeoAdaptionDetailsBuyer($dataBuyer["ID"]);
                     
                     
                     $dataBuyer["ID"] = $resultGetItem->id;
                     
					 if(!isset($_GET["nocount"])) {
                     	$buyer->IncrementQView($dataBuyer);
					 }
                     
                    $smarty->assign("data", $resultsell);
                    $smarty->assign("class", " ");
                    if(isset($resultsell) && sizeof($resultsell) > 0) {
						echo fminimizer($smarty->fetch("./site/sugnavsearch.tpl"));
                    }
                    
					echo $smarty->fetch("./site/saveme.tpl");

                     $smarty->assign('colorsell', FrontEnd::ListColorSet());

					echo fminimizer($smarty->fetch("./site/ilistsell.tpl", $_SERVER["REQUEST_URI"]));
                    
                    if(isset($resultsell) && sizeof($resultsell) > 0) {
						echo fminimizer($smarty->fetch("./site/sugnavsearch.tpl"));                    	
                    }

                    if(isset($resultsell) && sizeof($resultsell) <= 0) {
            		?>
					<div style="padding-top:10pt; padding-bottom: 5pt;padding-top:0pt; padding-bottom:0pt; width:auto;" >
					<?
			    		$smarty->caching = true; 
          				$smarty->cache_dir = "./persistentcache"; 
          				$smarty->cache_lifetime = 10800;			    	
						if (!$smarty->is_cached("./site/keybuy.tpl")) {
							$datakeysell = $category->GetListSubCategoryActiveBuy();
							$smarty->assign("datakeysell", $datakeysell);
						}
						
						echo minimizer($smarty->fetch("./site/keybuy.tpl"));
         				$smarty->cache_dir = "./cache";
         				$smarty->cache_lifetime = 3600;					
						$smarty->caching = false; 
					?>
					</div>
                    <?
                    }

               ?>     
               <div style="background-color:#EEEEE0">
       		   <?
         			$smarty->cache_dir = "./cache";
         			$smarty->cache_lifetime = 3600;           			
           			$smarty->caching = false; 
           			
					if (isset($resultGetItem->id)) {
						$buyer->typeBizID = $resultGetItem->typeBizID;
						$_GET["subcategory"] = $resultGetItem->subTypeBizID;
						$_GET["ID"] = $resultGetItem->typeBizID;
						$res_sub = $buyer->SelectSubCategory();
						if(sizeof($res_sub) > 1)
						{
							$smarty->assign("subbizcategory", $res_sub);
							echo minimizer($smarty->fetch("./site/subbizbuy.tpl"));
						} 
						unset($_GET["subcategory"]);
						unset($_GET["ID"]);
					}
       		   ?>
               </div>
               
               <div style="background-color:#EEEEE0">
               <?     
                     
					$seocity = $city->GetSeoParent($resultGetItem->regionID);
					$dataseocity = $city->GetRegionbyParentForBuyer($resultGetItem->regionID);
					if (strlen(trim($seocity->seobuy)) > 0 && sizeof($dataseocity) > 1) {
           				?>
           				<div style="padding-top:10pt; padding-bottom: 5pt;padding-top:0pt; padding-bottom:0pt; width:auto;" >
           					<br>
							<div style="width:auto;">
								<div  style="background-image:url(http://www.bizzona.ru/images/bl.gif); background-repeat:repeat-x;">&nbsp;</div>
							</div>
           				</div>
           				<?
						$smarty->assign("seocity", $seocity);
						$smarty->assign("dataseocity", $dataseocity);
						$smarty->display("./site/hsubregionbuyer.tpl");
					}
                 }
               ?>
               </div>

               <div style="background-color:#EEEEE0">
       			
               <?
           			$smarty->caching = true; 
           			$smarty->cache_lifetime = 10800;
           			$smarty->cache_dir = "./persistentcache"; 
           			if (!$smarty->is_cached('./site/cityb.tpl', 'buyer')) {
               			$data = array();
               			$data["offset"] = 0;
               			$data["rowCount"] = 0;
               			$data["sort"] = "CountB";
               			$result = $city->SelectActiveB($data);
               			$smarty->assign("data", $result);
           			} 

					echo minimizer($smarty->fetch("./site/cityb.tpl", "buyer"));
				?>
				
				</div>
				
            </div>
        </td>
        <td width="30%"  valign="top" height="100%" style="padding:2pt;">
       	<?
       		$smarty->assign("data", $resultGetItem);
       		$smarty->caching = false; 
			$output_servicecompany = $smarty->fetch("./site/servicecompany.tpl");
			echo minimizer($output_servicecompany);        	
			$smarty->caching = false;        	
		?>
		
        <?
        
			$smarty->display("./site/call.tpl");        
        
          $smarty->cache_dir = "./persistentcache"; 
          $smarty->cache_lifetime = 10800;          
        
          if (isset($_GET["action"]) && $_GET["action"]  == "category") {
            $_GET["ID"] = (isset($_GET["ID"])  ? intval($_GET["ID"]) : 0);
            $smarty->caching = true; 
            if (!$smarty->is_cached('./site/categorysb.tpl', $_GET["ID"])) {
              $data = array();
              $data["offset"] = 0;
              $data["rowCount"] = 0;
              $data["sort"] = "CountB";
              $result = $category->Select($data);
              $smarty->assign("data", $result);
            }
            
			echo fminimizer($smarty->fetch("./site/categorysb.tpl", $_GET["ID"]));            
            
            $smarty->caching = false; 
          } else { 
            $smarty->caching = true; 
            if (!$smarty->is_cached('./site/categorysb.tpl')) {
              $data = array();
              $data["offset"] = 0;
              $data["rowCount"] = 0;
              $data["sort"] = "CountB";
              $result = $category->Select($data);
              $smarty->assign("data", $result);
            }
            
			echo fminimizer($smarty->fetch("./site/categorysb.tpl"));            
            
            $smarty->caching = false; 
          }
          
          $smarty->cache_dir = "./cache";
          $smarty->cache_lifetime = 3600;          
        ?>
		<div style="padding-top:2pt;"></div>                    
        <?
			echo minimizer($smarty->fetch("./site/proposal.tpl"));
			echo minimizer($smarty->fetch("./site/request.tpl"));
			
        	if (isset($resultGetItem->id) && sizeof($resultsell) > 0) {
				$smarty->caching = true; 
				if (!$smarty->is_cached("./site/keybuylist.tpl")) {
					$datakeybuy = $category->GetListSubCategoryActiveBuy();
					$smarty->assign("datakeybuy", $datakeybuy);
				}
				$smarty->assign("countdatakeybuy", 18);
				echo minimizer($smarty->fetch("./site/keybuylist.tpl"));
				$smarty->caching = false; 					        	
        	}
		?>
        </td>
    </tr>
    <tr>
    	<td colspan="2">
				<div style="padding-top:4pt;padding-bottom:5pt;padding-right:10pt;padding-left:10pt;font-size:10pt;font-family:arial;">
					<table cellpadding="0" cellspacing="0" border="0"  style="width:100%;" bgcolor="#eeeee0">
    					<tr>
        					<td style="background-image:url(http://<?=$_SERVER["HTTP_HOST"]?>/images/d1.gif); background-repeat:no-repeat;width:5px;height:5px; background-position:left top;">&nbsp;</td>
            				<td rowspan="2" valign="middle" align="center"  style="font-size:8pt; font-family:Arial; color:black;">
								<h1 style='margin:0pt;padding:0pt;font-family:arial;font-size:8pt;text-align:center;margin-bottom:0pt;color:#000;' >��� "������-����" - <?=$seoname;?> �������� ����� ����������� ������� �� �������.</h1> 
            				</td>
            				<td style="background-image:url(http://<?=$_SERVER["HTTP_HOST"]?>/images/d2.gif); background-repeat:no-repeat;width:5px;height:5px;background-position:right top;">&nbsp;</td>
        				</tr>
        				<tr>
            				<td style="background-image:url(http://<?=$_SERVER["HTTP_HOST"]?>/images/d4.gif); background-repeat:no-repeat;width:5px;height:5px; background-position:left bottom;">&nbsp;</td>
            				<td style="background-image:url(http://<?=$_SERVER["HTTP_HOST"]?>/images/d3.gif); background-repeat:no-repeat;width:5px;height:5px; background-position:right bottom;">&nbsp;</td>
        				</tr>
    				</table>
				</div>
    	</td>
    </tr>
    <tr>
    	<td colspan="2">
			<?
				$smarty->display("./site/footer.tpl");
			?>
    	</td>
    </tr>
</table>
<?
  $end = get_formatted_microtime(); 
  $total = $end - $start;
  echo "<center><span style='font-size:7pt;'>".round($total, 6)." : ".$count_sql_call." : ".$base_memory_usage." : ".memoryUsage(memory_get_usage(), $base_memory_usage)." </span><center>";
?>
</body>
</html>
<?
	$sell->Close();
	$category->Close();
	$city->Close();
	$country->Close();
	$buyer->Close();
	$metro->Close();	
	$district->Close();
	$subcategory->Close();
    $streets->Close();
?>
