<?
	global $DATABASE;

	include_once("./phpset.inc");
	include_once("./engine/functions.inc"); 
	
	$count_sql_call = 0;

	$start = get_formatted_microtime(); 

	AssignDataBaseSetting();
  
	include_once("./engine/class.category.inc");
	$category = new Category();

	include_once("./engine/class.country.inc"); 
	$country = new Country();

	include_once("./engine/class.city.inc");  
	$city = new City();

	include_once("./engine/class.sell.inc"); 
	$sell = new Sell();

	include_once("./engine/class.buy.inc"); 
	$buyer = new Buyer();
  
	include_once("./engine/class.metro.inc");
	$metro = new Metro();
  
	include_once("./engine/class.district.inc"); 
	$district = new District();

	include_once("./engine/class.subcategory.inc");
	$subcategory = new SubCategory();

	include_once("./engine/class.streets.inc"); 
	$streets = new streets();

	require_once("./libs/Smarty.class.php");
	$smarty = new Smarty;
	$smarty->template_dir = "./templates";
	$smarty->compile_dir  = "./templates_c";
	//$smarty->cache_dir = "./cache";
	$smarty->compile_check = true;
	$smarty->debugging     = false;
	
	require_once("./regfuncsmarty.php");
  
	if(isset($_GET["ID"])) {
		$_GET["ID"] = intval($_GET["ID"]);
	}
  
	if(isset($_GET["rowCount"])) {
		$_GET["rowCount"] = intval($_GET["rowCount"]);
	}

	if(isset($_GET["offset"])) {
		$_GET["offset"] = intval($_GET["offset"]);
	}

	if(isset($_GET["streetID"])) {
		$_GET["streetID"] = intval($_GET["streetID"]);
	}
  
	$name_words = array("turagenstvo", "restoran", "parik", "krasota", "otel");
	if(isset($_GET["name"])) {
		if(in_array($_GET["name"], $name_words)) {
		} else {
			$_GET["name"] = "";
		}
	}
  
	$action_words = array("category", "city", "price", "topbiz");
	if(isset($_GET["action"])) {
		if(in_array($_GET["action"], $action_words)) {
		
		} else {
			$_GET["action"] = "";
		}
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
  <HEAD>
<?
 $titleseokey = "";
 $seokey = "";
 $seocity = "";

  if (isset($_GET["action"])) {
    switch(trim($_GET["action"])) {
      case "category": {
        $seokey = $category->GetKeySell(intval($_GET["ID"]));
        if(isset($_GET["subcategory"])) {
        	$dsub["ID"] = intval($_GET["subcategory"]);
        	$rsubcategory = $subcategory->GetItem($dsub);
        	if(isset($rsubcategory) && strlen($rsubcategory->titlekeysell) > 0) {
        		$titleseokey = $rsubcategory->titlekeysell;
        	}
        }
        ?>
          <title>������� � ������� ������� <?=(isset($titleseokey) && strlen($titleseokey) > 0 ? " - ".$titleseokey : "");?>. ������� ������ - ��� "������ ����"</title>	
        <?

        if(strlen($seokey) > 0) {
        	$seokey = $seokey.", ";
        }
      } break;
      case "city": {
      	if(isset($_GET["subCityID"]) && intval($_GET["subCityID"]) > 0) {
      		$dataCity["ID"] = intval($_GET["subCityID"]); 
      	} else {
      		$dataCity["ID"] = intval($_GET["ID"]);	
      	}
        
        $objCity = $city->GetItem($dataCity);
        $cp_objCity = $objCity; 
        $epoplistbiz_title = $objCity->title;
        $seocity = $objCity->seo;
        if(strlen($seocity) > 0) {
        	$seocity = $seocity.", ";
        }
        
        if(isset($_GET["streetID"]) && intval($_GET["streetID"]) > 0)  {
        	$streets->id = intval($_GET["streetID"]);
        	$res_streets = $streets->GetItem();
        	$objCity->title =  $objCity->title." (".$res_streets->name.")";
        	$smarty->assign("streetname", " (".$res_streets->name.")");
        }
        
        ?>
          <title>������� ������� <?=$objCity->title;?> | ������� ������� <?=$objCity->title;?> | ������� ������ - ��� "������ ����"</title>		
        <?
        $seokey = "������� ������� ".$objCity->title.", ������� ������� ".$objCity->title.", ";
      } break;
      case "price": {
        ?>
          <title>������� � ������� �������. ������� ������ - ��� "������ ����"</title>
        <?
      } break;
      default: {
        ?>
          <title>������� � ������� �������. ������� ������ - ��� "������ ����"</title>		
        <?
      }   
    }
  } else {
    ?>
      <title>������� � ������� �������. ������� ������ - ��� "������ ����"</title>		
    <?
  } 
  
	// up subcity show
  
	$smarty->caching = true; 
    $smarty->cache_lifetime = 3600;
	$city_cach = ((isset($_GET["action"]) && $_GET["action"] == "city") ? intval($_GET["ID"]) : 0 );
	if($city_cach > 0) {
    	if (!$smarty->is_cached('./site/subcity.tpl', $city_cach)) {
    		$data = array();
        	$data["offset"] = 0;
        	$data["rowCount"] = 0;
			$city->CityID = intval($_GET["ID"]);            		
        	$result = $city->SelectChildActive($data);
        	$smarty->assign("items", sizeof($result)/3);
        	$smarty->assign("datasubcity", $result);
                	
    		$dataCity["ID"] = intval($_GET["ID"]);	
        	$objCity = $city->GetItem($dataCity);                	
        	$smarty->assign("citytitleparent", $objCity->titleParentSelect);
		} 
	}
	// down subcity show
?>
	<META NAME="Description" CONTENT="������� �������, ������� �������, ������� ������, ������� �������, ������� �������� �������, ������� �������, ������� ��������,  ������� ��������, ������� ��������, ���� ������� ������?">
        <meta name="Keywords" content="<?=(isset($titleseokey) && strlen($titleseokey) > 0 ? $titleseokey."," : "");?><?=$seocity;?><?=$seokey;?>������� �������, ������� �������, ������� ������, ������� �������, ������� �������� �������, ������� �������, ������� ��������,  ������� ��������, ������� ��������, Ready business, �����������, �������� � ��������, ���������, ����������, �����������"> 
        <LINK href="http://www.bizzona.ru/general.css" type="text/css" rel="stylesheet">
        <meta http-equiv="content-type" content="text/html; charset=windows-1251"/>
        <link rel="shortcut icon" href="http://www.bizzona.ru/images/favicon.ico">
    </HEAD>
<body>

<?php
  	$smarty->display("./site/headerbanner.tpl");
	$headertitle = "������� � ������� �������� ������� ".$titleseokey."".(isset($objCity) ? $objCity->title : "")."";
	$smarty->assign("headertitle", $headertitle);
?>
<table class="w" border="0" cellpadding="0" cellspacing="0">

	<tr>
		<td colspan="2">
		<?
			$topmenu = GetMenu();
			$smarty->assign("topmenu", $topmenu);

			$link_country = GetSubMenu();
			$smarty->assign("ad", $link_country);

			$smarty->caching = false;
			$output_header = $smarty->fetch("./site/header.tpl");
			echo minimizer($output_header);
			$smarty->caching = true; 
    		$smarty->cache_lifetime = 3600;	
		?>
		</td>
	</tr>
    <tr>
        <td width="30%" valign="top" style="padding-top:2pt;">
        <?
		if(isset($_GET["action"]) && $_GET["action"]  == "category") {
			//$smarty->caching = false;
			if (!$smarty->is_cached("./site/subbizleftmenu.tpl", intval($_GET["ID"]))) {
				$sell->CategoryID = (isset($_GET["ID"])  ? intval($_GET["ID"]) : 0);
				$res_sub = $sell->SelectSubCategory();
				$smarty->assign("subbizcategory", $res_sub);
			}
			$output_subbiz = $smarty->fetch("./site/subbizleftmenu.tpl", intval($_GET["ID"]));
			echo fminimizer($output_subbiz);			 
			//$smarty->caching = true;
		}        
        
          $smarty->cache_dir = "./persistentcache"; 
          $smarty->cache_lifetime = 10800;
          if (isset($_GET["action"]) && $_GET["action"]  == "city" && intval($_GET["ID"]) > 0) {
          	
          	$sell->CityID = intval($_GET["ID"]);
          	$smarty->caching = true;
          	if (!$smarty->is_cached("./site/listdetailssearchcategory.tpl", $sell->CityID)) {
          		$res_ListDetailsSearchCategory = $sell->ListDetailsSearchCategory();
          		$smarty->assign("data", $res_ListDetailsSearchCategory);          	
          	}
  			$output_categorys = $smarty->fetch("./site/listdetailssearchcategory.tpl", $sell->CityID);
  			echo fminimizer($output_categorys);
  			$smarty->caching = false;
          } else {
        
          	if (isset($_GET["action"]) && $_GET["action"]  == "category") {
            	$_GET["ID"] = (isset($_GET["ID"])  ? intval($_GET["ID"]) : 0);
            	$smarty->caching = true; 
            	if (!$smarty->is_cached('./site/categorys.tpl', $_GET["ID"])) {
              		$data = array();
              		$data["offset"] = 0;
              		$data["rowCount"] = 0;
              		$data["sort"] = "Count";
              		$result = $category->Select($data);
              		$smarty->assign("data", $result);
            	}
  				$output_categorys = $smarty->fetch("./site/categorys.tpl", $_GET["ID"]);
  				echo fminimizer($output_categorys);
            	$smarty->caching = false; 
          	} else { 
            	$smarty->caching = true; 
            	if (!$smarty->is_cached('./site/categorys.tpl')) {
              		$data = array();
              		$data["offset"] = 0;
              		$data["rowCount"] = 0;
              		$data["sort"] = "Count";
              		$result = $category->Select($data);
              		$smarty->assign("data", $result);
            	}
				$output_categorys = $smarty->fetch("./site/categorys.tpl");
				echo fminimizer($output_categorys);
            
            	$smarty->caching = false; 
          	}
          
         }
         $smarty->cache_dir = "./cache";
         $smarty->cache_lifetime = 3600;
        ?>
            <?
            	$datahotbuyer = array();
            	$datahotbuyer["offset"] = 0;
            	$datahotbuyer["rowCount"] = 4;
            	$datahotbuyer["sort"] = "datecreate";
            	$datahotbuyer["StatusID"] = "-1";
            	
                if (isset($_GET["action"])) {
                   switch(trim($_GET["action"])) {
	                   case "category": {
    	                 $datahotbuyer["typeBizID"] = intval($_GET["ID"]);
                       } break;
                       case "city": {
                         $datahotbuyer["regionID"] = intval($_GET["ID"]);
                       } break;                       
        	           default: {
                         		
                       }
                       break;
                   }            	
                }		
            	
                //$smarty->display("./site/call.tpl");
                
				$output_call = $smarty->fetch("./site/call.tpl");
				echo fminimizer($output_call);
				
  	          	$res_datahotbuyer = $buyer->Select($datahotbuyer);
  	          	if(sizeof($res_datahotbuyer) > 0) {
    				$smarty->assign("databuyer", $res_datahotbuyer);
					$output_iblockbuyer = $smarty->fetch("./site/iblockbuyer.tpl");
					echo fminimizer($output_iblockbuyer);
  	          	} else {
  	          		//$smarty->display("./site/begun.tpl");
  	          	}
  	          	
        	//$smarty->display("./site/proposal.tpl");
			$output_proposal = $smarty->fetch("./site/proposal1.tpl");
			echo fminimizer($output_proposal);
        	
         	//$smarty->display("./site/request.tpl");
			$output_request = $smarty->fetch("./site/request1.tpl");
			echo fminimizer($output_request);
			
			$output_citydomains = $smarty->fetch("./site/citydomains.tpl");
           	echo minimizer($output_citydomains);
        ?>              
        
        <?
        	SeoAdaptionSearchSellCity();
		?>
        
        </td>
        <td width="70%" valign="top">
        
            <?
                if(intval($objCity->ID) > 0) {
                	
					$smarty->assign("subCityId", $objCity->ID);
					$smarty->assign("poplistbiztitle", "���������� ����� ������� (".$epoplistbiz_title.")");
					$smarty->assign("poplistbizcitytitle", $epoplistbiz_title);
            
					$smarty->cache_dir = "./persistentcache";
					$smarty->cache_lifetime = 10800;
					$smarty->caching = true;  
					if (!$smarty->is_cached('./site/epoplistbizcity_custom.tpl', $objCity->ID)) {
						$counttopcategory = $sell->ListCountTopCategoryForCity_Custom($objCity->ID);
						$smarty->assign("counttopcategory", $counttopcategory);						
					}
					$output_poplistbiz = $smarty->fetch("./site/epoplistbizcity_custom.tpl", $objCity->ID);
					echo fminimizer($output_poplistbiz);
					
					$smarty->cache_lifetime = 83600;
					$smarty->cache_dir = "./cache";
					$smarty->caching = false; 
					
                } else {
                	ShowSelectRegion();
                }
            ?>        
        
            <?

       			// up subcity show
				$output_city = $smarty->fetch("./site/subcity.tpl", $city_cach);
				echo fminimizer($output_city);
       			$smarty->caching = false;       			
       			// down subcity show
       			
       			if(intval($objCity->ID) <= 0) {
                	$smarty->caching = true; 
                	$smarty->cache_lifetime = 10800;
                	$smarty->cache_dir = "./persistentcache";
					if (!$smarty->is_cached('./site/epoplistbiz.tpl')) {
						$counttopcategory = $sell->ListCountTopCategory();
						$smarty->assign("counttopcategory", $counttopcategory);
					}
					$output_poplistbiz = $smarty->fetch("./site/epoplistbiz.tpl");
					echo fminimizer($output_poplistbiz);
					$smarty->cache_dir = "./cache";
					$smarty->caching = false;     
       			}  			
				
                //$smarty->display("./site/navsearch.tpl");
				$output_navsearch = $smarty->fetch("./site/navsearch.tpl");
				echo fminimizer($output_navsearch);
				
                $smarty->caching = true; 
                  if (!$smarty->is_cached('./site/city.tpl', $_SERVER["REQUEST_URI"])) {
                    $data = array();
                    $dataPage = array(); 
                    $pagesplit = GetPageSplit();

                    if (isset($_GET["offset"])) {
                      $data["offset"] = intval($_GET["offset"]);
                    } else {
                      $data["offset"] = 0;
                    }
                    if (isset($_GET["rowCount"])) { 
                      $data["rowCount"] = intval($_GET["rowCount"]);
                    } else {
                      $data["rowCount"] = $pagesplit;
                    }

                    $data["sort"] = "ID";
                    
                    if (isset($_GET["streetID"]) && intval($_GET["streetID"]) > 0) {
                    	$data["streetID"] = intval($_GET["streetID"]);
                    	$dataPage["streetID"] = $data["streetID"];
                    }
                    
                    if(isset($_GET["subCityID"]) && intval($_GET["subCityID"]) > 0) {
                    	$data["subCityID"] = intval($_GET["subCityID"]);
                    	$dataPage["subCityID"] = intval($_GET["subCityID"]);
                    }
                    
                    

                    if (isset($_GET["action"])) {
                       switch(trim($_GET["action"])) {
                         case "category": {
                           $_GET["ID"] = intval($_GET["ID"]); 
                           $data["CategoryID"] = $_GET["ID"];
                           if(isset($_GET["subcategory"])) {
                             $data["SubCategoryID"] = intval($_GET["subcategory"]);
                             $dataPage["SubCategoryID"] = intval($_GET["subcategory"]);
                           }
                           $dataPage["CategoryID"] = $_GET["ID"];
                           $_SESSION["uCategoryID"] = $_GET["ID"];
                           if (isset($_SESSION["uCityID"])) unset($_SESSION["uCityID"]);
                         } break;
                         case "city": {
                           $_GET["ID"] = intval($_GET["ID"]);
                           $data["CityID"] = $_GET["ID"];
                           $dataPage["CityID"] = $_GET["ID"];
                           $_SESSION["uCityID"] = $_GET["ID"];
                           if (isset($_SESSION["uCategoryID"]))  unset($_SESSION["uCategoryID"]);
                         } break;
                         case "price": {
                           $_GET["MinCostID"] = isset($_GET["MinCostID"]) ? $_GET["MinCostID"]: 0;
                           $_GET["MaxCostID"] = isset($_GET["MaxCostID"]) ? $_GET["MaxCostID"]: 0; 
                           $data["MinCostID"] = intval($_GET["MinCostID"]);
                           $dataPage["MinCostID"] = intval($_GET["MinCostID"]);
                           $data["MaxCostID"] = intval($_GET["MaxCostID"]);
                           $dataPage["MaxCostID"] = intval($_GET["MaxCostID"]);
                         } break;
                         case "topbiz": {
                           $_GET["name"]  = ( isset($_GET["name"]) ? htmlspecialchars(trim($_GET["name"]), ENT_QUOTES) : "" );
                           if($_GET["name"] == "turagenstvo")
                           {
                              $data["icon"] =   96;
                              $dataPage["icon"] = 96;
                           }   
                           else if($_GET["name"] == "restoran") {
                              $data["icon"] =   92;
                              $dataPage["icon"] = 92;
                           } else if( $_GET["name"] == "otel") {  
                              $data["icon"] =   93;
                              $dataPage["icon"] = 93;
                           } else if ($_GET["name"] == "parik" || $_GET["name"] == "krasota") {   
                              $data["icon"] =   109;
                              $dataPage["icon"] = 109;
                           } else {
                              //$data["icon"] =   $_GET["name"]."."."gif";
                              //$dataPage["icon"] = $_GET["name"]."."."gif";
                              $_GET["name"] = "";
                           }
                         }
                         default: {
                         }   
                       }
                    }
                    
                    $dataPage["offset"] = 0;
                    $dataPage["rowCount"] = 0;
                    $dataPage["sort"] = "ID";
                    $dataPage["StatusID"] = "-1";
                    
                    
                    $resultPage = $sell->CountSelect($dataPage);
                    
                    $smarty->assign("CountRecord",$resultPage);
                    $smarty->assign("CountSplit", $pagesplit);
                    $smarty->assign("CountPage", ceil($resultPage/5));

                    $data["StatusID"] = "-1";
                    $data["sort"] = "pay_datetime";

                    $result = $sell->Select($data);

                    $smarty->assign("data", $result);

                  }

				  $output_pagesplit = $smarty->fetch("./site/pagesplit.tpl", $_SERVER["REQUEST_URI"]);
				  echo fminimizer($output_pagesplit);
                  
				  $saveme = $smarty->fetch("./site/saveme.tpl");
				  echo $saveme;
                  
				  $output_ilistsell = $smarty->fetch("./site/ilistsell.tpl", $_SERVER["REQUEST_URI"]);
				  echo fminimizer($output_ilistsell);
                  
                  $smarty->caching = false; 
               ?>
            	<div class="lftpadding" style="width: auto;">
       				<div  style="background-image:url(<?=$_SERVER["HTTP_HOST"]?>/images/bl.gif); background-repeat:repeat-x;">&nbsp;</div>
            	</div>
            	<?
				  	echo fminimizer($output_pagesplit);
				  	echo fminimizer($output_navsearch);
				  	
					if(intval($objCity->ID) > 0) {  	
						ShowSelectRegion(); 
					}
				  	
				?>
					<div class="lftpadding_cnt" style="padding-top:10pt; padding-bottom: 5pt;padding-top:0pt; padding-bottom:0pt; width:auto;" >
				<?
				
			    $smarty->caching = true; 
				$smarty->cache_lifetime = 86400;
				$smarty->cache_dir = "./persistentcache";
				
            	if (!$smarty->is_cached("./site/keysell.tpl")) {
					$datakeysell = $category->GetListSubCategoryActive();
					$smarty->assign("datakeysell", $datakeysell);
            	}
				$output_keysell = $smarty->fetch("./site/keysell.tpl");
                echo fminimizer($output_keysell);
                
                $smarty->cache_dir = "./cache";
                $smarty->caching = false; 
            	?>
            		</div>
				<?
				  if (isset($_GET["action"])) {
    				switch(trim($_GET["action"])) {
						case "city": {
							$parentID = (isset($_GET["ID"])  ? intval($_GET["ID"]) : 0);
							$seocity = $city->GetSeoParent($parentID);
							$dataseocity = $city->GetRegionbyParent($parentID);
							if(strlen(trim($seocity->seosell)) > 0 && sizeof($dataseocity) > 1) {
								$smarty->assign("seocity", $seocity);
								$smarty->assign("dataseocity", $dataseocity);
								$smarty->display("./site/hsubregion.tpl");
							}
						}	break;
    				}
				  }
     		   ?>            		

			  <!-- div class="lftpadding_cnt" style="padding-top:10pt; padding-bottom: 5pt;padding-top:0pt; padding-bottom:0pt; width:auto;" -->
			   <?
		            /* ��������� ��� ������ �� �������	   		
			        if(isset($_GET["subCityID"]) && intval($_GET["subCityID"]) > 0) {
						$pref_cach = intval($_GET["subCityID"]);
            			$smarty->caching = true; 
						if (!$smarty->is_cached('./site/streetssell.tpl', $pref_cach)) {
							$streets->subCityId = intval($_GET["subCityID"]);
							$res_streets = $streets->SelectStreetSell();
							//echo $streets->sql;
							if(sizeof($res_streets) > 0) {
								$smarty->assign("streets", $res_streets);
							}
						} 
						$output_streetssell = $smarty->fetch("./site/streetssell.tpl", $pref_cach);
						echo fminimizer($output_streetssell);
						$smarty->caching = false; 
						
			        } else if (isset($_GET["action"]) && intval($_GET["ID"]) > 0 && $_GET["action"] == "city") {
						$pref_cach = intval($_GET["ID"]);
            			$smarty->caching = true; 
						if (!$smarty->is_cached('./site/streetssell.tpl', $pref_cach)) {
							$streets->cityId = intval($_GET["ID"]);
							$res_streets = $streets->SelectStreetSell();
							//echo $streets->sql;
							if(sizeof($res_streets) > 0) {
								$smarty->assign("streets", $res_streets);
							}
						} 
						$output_streetssell = $smarty->fetch("./site/streetssell.tpl", $pref_cach);
						echo fminimizer($output_streetssell);
						$smarty->caching = false; 
					}
					*/
			   ?>
			   <!-- /div -->
			   
				<div class="lftpadding_cnt" style="padding-top:10pt; padding-bottom: 5pt;padding-top:0pt; padding-bottom:0pt; width:auto;" >	    
					<table border="0" align="right" width="100%">
                		<tr>
                				<td width="50%"></td>
                				<td>
									<script type="text/javascript" src="http://vkontakte.ru/js/api/share.js?10" charset="windows-1251"></script>
									<script type="text/javascript">
									<!--
										document.write(VK.Share.button(false,{type: "round", text: "���������"}));
									-->
									</script>            
								</td>
								<td>
									<a class="mrc__share" href="http://connect.mail.ru/share">� ��� ���</a>
									<script src="http://cdn.connect.mail.ru/js/share/2/share.js" type="text/javascript" charset="UTF-8"></script>
								</td>
                				<td>
									<a href="http://twitter.com/share" class="twitter-share-button" data-count="horizontal">Tweet</a><script type="text/javascript" src="http://platform.twitter.com/widgets.js"></script>                
                				</td>	
                				<td>
                				<?
                					if(isset($cp_objCity) && strlen($cp_objCity->yandexwidgetsell) > 0) {
                						?>
                							<a href="<?=$cp_objCity->yandexwidgetsell;?>" target="_blank"><img src="http://img.yandex.net/i/service/wdgt/yand-add-b.png" border="0" alt="<?=$cp_objCity->titleyandexwidgetsell;?>"/></a>
                						<?
                					}
								?>
                				</td>			
							</tr>
						</table>
				</div>			   
			   
        </td>
    </tr>
	<tr>
		<td colspan="2">
<div style="padding-top:4pt;padding-bottom:5pt;padding-right:10pt;padding-left:10pt;font-size:10pt;font-family:arial;">
	<table cellpadding="0" cellspacing="0" border="0"  style="width:100%;padding-bottom:2pt;" bgcolor="#eeeee0">
    	<tr>
        	<td style="background-image:url(http://<?=$_SERVER["HTTP_HOST"]?>/images/d1.gif); background-repeat:no-repeat;width:5px;height:5px; background-position:left top;">&nbsp;</td>
            <td rowspan="2" valign="middle" align="center"  style="font-size:8pt; font-family:Arial; color:black;">
			 <h2 style='margin:0pt;padding:0pt;color:#000;font-family:arial;font-size:8pt;text-align:center;margin-bottom:0pt;' ><a href="http://www.bizzona.ru/searchsell/" title="������� �������" style='color:#000;font-family:arial;font-size:8pt;font-weight:bold;text-decoration:none;'>������� �������</a> ��� <a href="http://www.bizzona.ru/searchbuy/" style='color:#000;font-family:arial;font-size:8pt;font-weight:bold;text-decoration:none;' title="������� �������">������� �������</a>. ��� "������-����" - �� ���������� ������ ������ � ����������� ��� �����. ������� ������� <?=ucwords($titleseokey);?> <?=isset($objCity) ? $objCity->title : "";?>.</h2>              
            </td>
            <td style="background-image:url(http://<?=$_SERVER["HTTP_HOST"]?>/images/d2.gif); background-repeat:no-repeat;width:5px;height:5px;background-position:right top;">&nbsp;</td>
        </tr>
        <tr>
            <td style="background-image:url(http://<?=$_SERVER["HTTP_HOST"]?>/images/d4.gif); background-repeat:no-repeat;width:5px;height:5px; background-position:left bottom;">&nbsp;</td>
            <td style="background-image:url(http://<?=$_SERVER["HTTP_HOST"]?>/images/d3.gif); background-repeat:no-repeat;width:5px;height:5px; background-position:right bottom;">&nbsp;</td>
        </tr>
    </table>
</div>
<?
	if (isset($_GET["action"])) {
		switch(trim($_GET["action"])) {
			case "city": {
				$cID = (isset($_GET["ID"])  ? intval($_GET["ID"]) : 0);
				
				if($cID == 928 || $cID == 23) {
					$output_adv = $smarty->fetch("./site/adv23.tpl");
					echo minimizer($output_adv);
				} else if ($cID == 77 || $cID == 50) {
					$output_adv = $smarty->fetch("./site/adv77.tpl");
					echo minimizer($output_adv);
                                } else if ($cID == 1310 || $cID == 54) {
					$output_adv = $smarty->fetch("./site/adv54.tpl");
					echo minimizer($output_adv);
                                } else if ($cID == 1915 || $cID == 66) {
					$output_adv = $smarty->fetch("./site/adv66.tpl");
					echo minimizer($output_adv);
                                } else if ($cID == 47 || $cID == 78) {
					$output_adv = $smarty->fetch("./site/adv78.tpl");
					echo minimizer($output_adv);
                                } else if ($cID == 985 || $cID == 24) {
					$output_adv = $smarty->fetch("./site/adv24.tpl");
					echo minimizer($output_adv);
                                } else if ($cID == 1441 || $cID == 59) {
					$output_adv = $smarty->fetch("./site/adv59.tpl");
					echo minimizer($output_adv);
                                } else if ($cID == 1816 || $cID == 63) {
					$output_adv = $smarty->fetch("./site/adv63.tpl");
					echo minimizer($output_adv);
                                } else if ($cID == 1264 || $cID == 52) {
					$output_adv = $smarty->fetch("./site/adv52.tpl");
					echo minimizer($output_adv);
                                } else if ($cID == 304 || $cID == 2) {
					$output_adv = $smarty->fetch("./site/adv2.tpl");
					echo minimizer($output_adv);
                                } else if ($cID == 634 || $cID == 39) {
					$output_adv = $smarty->fetch("./site/adv39.tpl");
					echo minimizer($output_adv);
                                } else if ($cID == 1721 || $cID == 16) {
					$output_adv = $smarty->fetch("./site/adv16.tpl");
					echo minimizer($output_adv);
                                } else if ($cID == 1791 || $cID == 61) {
					$output_adv = $smarty->fetch("./site/adv61.tpl");
					echo minimizer($output_adv);
                                } else if ($cID == 1353 || $cID == 55) {
					$output_adv = $smarty->fetch("./site/adv55.tpl");
					echo minimizer($output_adv);
                                } else if ($cID == 1100 || $cID == 48) {
					$output_adv = $smarty->fetch("./site/adv48.tpl");
					echo minimizer($output_adv);
                                } else if ($cID == 2326 || $cID == 74) {
					$output_adv = $smarty->fetch("./site/adv74.tpl");
					echo minimizer($output_adv);
                                } else if ($cID == 554 || $cID == 38) {
					$output_adv = $smarty->fetch("./site/adv38.tpl");
					echo minimizer($output_adv);
                                } else if ($cID == 460 || $cID == 36) {
					$output_adv = $smarty->fetch("./site/adv36.tpl");
					echo minimizer($output_adv);
                                }

			}
			case "category": {
				$subCatID = (isset($_GET["subcategory"])  ? intval($_GET["subcategory"]) : 0);
				$cID = (isset($_GET["ID"])  ? intval($_GET["ID"]) : 0);
				if($cID == 109) {
					$output_adv = $smarty->fetch("./site/adv-salon.tpl");
					echo minimizer($output_adv);
				} else if($cID == 82) {
					$output_adv = $smarty->fetch("./site/advsub1.tpl");
					echo minimizer($output_adv);
				} else if($cID == 115) {
					$output_adv = $smarty->fetch("./site/advsub156.tpl");
					echo minimizer($output_adv);
				} else if($cID == 92) {
					$output_adv = $smarty->fetch("./site/advsub8.tpl");
					echo minimizer($output_adv);
				} else if($cID == 88 && $subCatID == 22) {
					$output_adv = $smarty->fetch("./site/advsub22.tpl");
					echo minimizer($output_adv);
				} else if($cID == 88 && $subCatID == 23) {
					$output_adv = $smarty->fetch("./site/advsub23.tpl");
					echo minimizer($output_adv);
				} else if($cID == 90 && $subCatID == 41) {
					$output_adv = $smarty->fetch("./site/advsub41.tpl");
					echo minimizer($output_adv);
				} else if($cID == 90 && $subCatID == 42) {
					$output_adv = $smarty->fetch("./site/advsub42.tpl");
					echo minimizer($output_adv);
				} else if($cID == 91 && ($subCatID == 81 || $subCatID == 80)) {
					$output_adv = $smarty->fetch("./site/advsub81.tpl");
					echo minimizer($output_adv);
				} else if($cID == 114 && ($subCatID == 157 || $subCatID == 51 || $subCatID == 52 || $subCatID == 118)) {
					$output_adv = $smarty->fetch("./site/advsub157.tpl");
					echo minimizer($output_adv);
				} else if($cID == 93 && $subCatID == 18) {
					$output_adv = $smarty->fetch("./site/advsub18.tpl");
					echo minimizer($output_adv);
				} else if($cID == 93 && $subCatID == 19) {
					$output_adv = $smarty->fetch("./site/advsub19.tpl");
					echo minimizer($output_adv);
				} else if($cID == 93 && $subCatID == 20) {
					$output_adv = $smarty->fetch("./site/advsub20.tpl");
					echo minimizer($output_adv);
				}

			} 
			case "topbiz": {
				if (isset($_GET["name"]) && ($_GET["name"] == "parik" || $_GET["name"] == "krasota"))	{
					$output_adv = $smarty->fetch("./site/adv-salon.tpl");
					echo minimizer($output_adv);
				}
			}
		}
	} else {
		$output_adv = $smarty->fetch("./site/adv.tpl");
		echo fminimizer($output_adv);
	}
?>

<div style="padding-top:4pt;padding-bottom:5pt;padding-right:10pt;padding-left:10pt;font-size:10pt;font-family:arial;">
	<table cellpadding="0" cellspacing="0" border="0"  style="width:100%;padding-bottom:2pt;" bgcolor="#eeeee0">
    	<tr>
        	<td style="background-image:url(http://<?=$_SERVER["HTTP_HOST"]?>/images/d1.gif); background-repeat:no-repeat;width:5px;height:5px; background-position:left top;">&nbsp;</td>
            <td rowspan="2" valign="middle" align="center"  style="font-size:8pt; font-family:Arial; color:black;">
             <a href="http://www.bizzona.ru/prodazha-biznesa-avtomoika.php" title="������� ���������" style="color:#000;">������� ���������</a>&nbsp;
             <a href="http://www.bizzona.ru/prodazha-biznesa-avtoskola.php" title="������� ���������" style="color:#000;">������� ���������</a>&nbsp;
             <a href="http://www.bizzona.ru/prodazha-biznesa-employment-agency.php" title="������� ��������� ���������" style="color:#000;">������� ��������� ���������</a>&nbsp;
             <a href="http://www.bizzona.ru/prodazha-biznesa-building-company.php" title="������� ������������ �����" style="color:#000;">������� ������������ �����</a>&nbsp;
             <a href="http://www.bizzona.ru/prodazha-biznesa-car-care-center.php" title="������� �����������" style="color:#000;">������� �����������</a>&nbsp;
             <a href="http://www.bizzona.ru/prodazha-biznesa-car-magazin.php" title="������� ��������" style="color:#000;">������� ��������</a>&nbsp;
            </td>
            <td style="background-image:url(http://<?=$_SERVER["HTTP_HOST"]?>/images/d2.gif); background-repeat:no-repeat;width:5px;height:5px;background-position:right top;">&nbsp;</td>
        </tr>
        <tr>
            <td style="background-image:url(http://<?=$_SERVER["HTTP_HOST"]?>/images/d4.gif); background-repeat:no-repeat;width:5px;height:5px; background-position:left bottom;">&nbsp;</td>
            <td style="background-image:url(http://<?=$_SERVER["HTTP_HOST"]?>/images/d3.gif); background-repeat:no-repeat;width:5px;height:5px; background-position:right bottom;">&nbsp;</td>
        </tr>
    </table>
</div>
<?
	$smarty->display("./site/footer.tpl");
?>
		</td>
	</tr>
</table>			
<?
  $end = get_formatted_microtime(); 
  $total = $end - $start;
  echo "<center><span style='font-size:7pt;'>".round($total, 6)." : ".$count_sql_call." </span><center>";
?>
<script type="text/javascript" src="http://www.bizzona.ru/ja.js"></script>
</body>
</html>
<?
	$sell->Close();
	$category->Close();
	$city->Close();
	$country->Close();
	$buyer->Close();
	$metro->Close();
	$district->Close();
	$subcategory->Close();
	$streets->Close();
?>
