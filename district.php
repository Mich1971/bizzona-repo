<?
	global $DATABASE;

	include_once("./phpset.inc");
	include_once("./engine/functions.inc"); 

	$count_sql_call = 0;
	$start = get_formatted_microtime(); 
	$base_memory_usage = memory_get_usage();	

	AssignDataBaseSetting();
  
	include_once("./engine/class.category_lite.inc"); 
	$category = new Category_Lite();

	include_once("./engine/class.country_lite.inc"); 
	$country = new Country_Lite();

	include_once("./engine/class.city_lite.inc"); 
	$city = new City_Lite();

	include_once("./engine/class.sell_lite.inc"); 
	$sell = new Sell_Lite();

	include_once("./engine/class.buy_lite.inc"); 
	$buyer = new Buyer_Lite();
  
	include_once("./engine/class.metro_lite.inc"); 
	$metro = new Metro_Lite();

	include_once("./engine/class.district_lite.inc"); 
	$district = new District_Lite();

	include_once("./engine/class.subcategory_lite.inc");
	$subcategory = new SubCategory_Lite();

	include_once("./engine/class.streets_lite.inc");
	$streets = new Streets_Lite();

	include_once("./engine/class.equipment_lite.inc"); 
	$equipment = new Equipment_Lite();
  
  	include_once("./engine/class.ad.inc");
  	$ad = new Ad(); 	

  	include_once("./engine/class.company.inc"); 
  	$company = new Company();

        include_once("./engine/class.review.inc"); 
        $review = new Review();        
        
    include_once("./engine/class.frontend.inc");

  	require_once("./libs/Smarty.class.php");
  	$smarty = new Smarty;
  	$smarty->template_dir = "./templates";
  	$smarty->compile_dir  = "./templates_c";
  	//$smarty->cache_dir = "./cache";
  	$smarty->compile_check = true;
  	$smarty->debugging     = false;

  	$colorset = GetColorSet();
  	$smarty->assign("colorset", $colorset);  	
  	
	$smarty->assign("listcompany", $company->ListCompanyInList());  	  	  	
  	
	$title = "������� � ������� �������. ������� ������ - ������ ����";  
    $seodistrict = "";
	require_once("./regfuncsmarty.php");

	if(isset($_GET["districtID"])) {
  		$_GET["districtID"] = intval($_GET["districtID"]);
  		if($_GET["districtID"] > 0) {
  			$district->ID = $_GET["districtID"];
  			$textseosell = $district->GetSeoSell();
  			$textseobuy = $district->GetSeoBuy();
  			if(strlen(trim($textseosell)) > 0 && $textseobuy) {
  				$title = $textseosell." | ".$textseobuy." | ������� ������ - ������ ����";  
  				$seodistrict = $textseosell.",".$textseobuy.",";
  			}
  		}
	}

	if(isset($_GET["regionID"])) {
  		$_GET["regionID"] = intval($_GET["regionID"]);
  		$sell->CityID = $_GET["regionID"];
	}
	
	$ad->InitAd($smarty, $_GET);  		
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
  <HEAD>
<?

	$seokey = "";
  	$seocity = "";

  	if (isset($_GET["action"])) {
    	switch(trim($_GET["action"])) {
      		case "category": {
        		?><title>������� � ������� �������. ������� ������ - ��� "������-����"</title><?
        		$seokey = $category->GetKeySell(intval($_GET["ID"]));
        		if(strlen($seokey) > 0) {
        			$seokey = $seokey.", ";
        		}
      		} break;
      		case "city": {
        		$dataCity["ID"] = intval($_GET["ID"]);
        		$objCity = $city->GetItem($dataCity);
        		$seocity = $objCity->seo;
        		if(strlen($seocity) > 0) {
        			$seocity = $seocity.", ";
        		}
        		?>
          			<title>������� ������� <?=$objCity->title;?> | ������� ������� <?=$objCity->title;?> | ������� ������ - ��� "������-����"</title>		
        		<?
        		$seokey = "������� ������� ".$objCity->title.", ������� ������� ".$objCity->title.", ";
      		} break;
      		case "price": {
        		?>
          			<title>������� � ������� �������. ������� ������ - ��� "������-����"</title>
        		<?
      		} break;
      		default: {
        		?>
          			<title>������� � ������� �������. ������� ������ - ��� "������-����"</title>		
        		<?
      		}   
    	}
  	} else {
    	?>
      		<title><?=$title;?></title>		
    	<?
  	} 
?>
	<META NAME="Description" CONTENT="������� �������, ������� �������, ������� ������, ������� �������, ������� �������� �������, ������� �������, ������� ��������,  ������� ��������, ������� ��������, ���� ������� ������?">
        <meta name="Keywords" content="<?=$seodistrict;?><?=$seocity;?><?=$seokey;?>������� �������, ������� �������, ������� ������, ������� �������, ������� �������� �������, ������� �������, ������� ��������,  ������� ��������, ������� ��������, Ready business, �����������, �������� � ��������, ���������, ����������, �����������"> 
        <LINK href="http://www.bizzona.ru/general.css" type="text/css" rel="stylesheet">
        <meta http-equiv="content-type" content="text/html; charset=windows-1251"/>
        <script type="text/javascript" src="ja.js"></script>
  </HEAD>
<body>

<?
	$smarty->display("./site/headerbanner.tpl");
?>
<?
	$link_country = GetSubMenu();	
	$smarty->assign("ad", $link_country);
	echo minimizer($smarty->fetch("./site/header.tpl"));
?>
<table class="w" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td width="30%" valign="top"  style="padding-top:2pt;">
        
        <?
        	$smarty->caching = false; 
			echo fminimizer($smarty->fetch("./site/inner_bannertop_left.tpl"));
		?>        
        
        <?
        	$smarty->cache_dir = "./persistentcache"; 
          	$smarty->cache_lifetime = 83600; 
                    
          	if (isset($_GET["action"]) && $_GET["action"]  == "category") {
            	$_GET["ID"] = (isset($_GET["ID"])  ? intval($_GET["ID"]) : 0);
            	$smarty->caching = true; 
            	if (!$smarty->is_cached('./site/categorys.tpl', $_GET["ID"])) {
              		$data = array();
              		$data["offset"] = 0;
              		$data["rowCount"] = 0;
              		$data["sort"] = "Count";
              		$result = $category->Select($data);
              		$smarty->assign("data", $result);
            	}
            
  				echo fminimizer($smarty->fetch("./site/categorys.tpl", $_GET["ID"]));
            	$smarty->caching = false; 
          	} else { 
            	$smarty->caching = true; 
            	if (!$smarty->is_cached('./site/categorys.tpl')) {
              		$data = array();
              		$data["offset"] = 0;
              		$data["rowCount"] = 0;
              		$data["sort"] = "Count";
              		$result = $category->Select($data);
              		$smarty->assign("data", $result);
            	}
            
				echo fminimizer($smarty->fetch("./site/categorys.tpl"));
            
            	$smarty->caching = false; 
          	}
          	$smarty->cache_dir = "./cache";
        	?>
        	
            <?
            	//echo fminimizer($smarty->fetch("./site/call.tpl"));
            
            	/* up ���������� ���� ����� ��� ����������� �������� ������ http://bizzona.ru/bgz/view.php?id=8 */
            	//$dataadvertising['realty'] = true;
            	//$dataadvertising['vip'] = true;
            	//$dataadvertising['leftblock'] = 1;
            	
            	//$dataad = $sell->SelectSellAdvertising($dataadvertising);
            	//$smarty->assign("datarealty", $dataad);
                
                LeftBlockTopSell();
            
            	/* down ���������� ���� ����� ��� ����������� �������� ������ http://bizzona.ru/bgz/view.php?id=8 */            	
            	
           	    /* up  ����� ���� ����������� ���������� �� ������� �������� ������ http://bizzona.ru/bgz/view.php?id=8
				$smarty->caching = true; 
				$smarty->cache_lifetime = 3600;
            	
            	if (!$smarty->is_cached("./site/iblockbuyer.tpl", "action=".(isset($_GET["action"]) ? $_GET["action"] : ""))) {
            	
            		$datahotbuyer = array();
            		$datahotbuyer["offset"] = 0;
            		$datahotbuyer["rowCount"] = 4;
            		$datahotbuyer["sort"] = "datecreate";
            		$datahotbuyer["StatusID"] = "-1";

                	if (isset($_GET["action"])) {
                   		switch(trim($_GET["action"])) {
	                   		case "category": {
    	                 		$datahotbuyer["typeBizID"] = intval($_GET["ID"]);
                       		} break;
                       		case "city": {
                         		$datahotbuyer["regionID"] = intval($_GET["ID"]);
                       		} break;                       
        	           		default: {
                       		}
                       		break;
                   		}            	
                	}		
            	
  	          		$res_datahotbuyer = $buyer->Select($datahotbuyer);
  	          		
    				$smarty->assign("databuyer", $res_datahotbuyer);        	
            	} 	
            	echo fminimizer($smarty->fetch("./site/iblockbuyer.tpl", "action=".(isset($_GET["action"]) ? $_GET["action"] : "")));
            	$smarty->caching = false; 
            	down  ����� ���� ����������� ���������� �� ������� �������� ������ http://bizzona.ru/bgz/view.php?id=8
            	*/  	          	
			?>
		<div style="padding-top:2pt;"></div>                    
        <?

        
                $smarty->caching = true; 
                
				if (!$smarty->is_cached('./site/epoplistbizmain.tpl')) {
					$counttopcategory = $sell->ListCountTopCategory();
					$smarty->assign("counttopcategory", $counttopcategory);
				
					$smarty->assign("countarenda", $sell->CountArenda());
					$smarty->assign("countbuyer", $buyer->CountBuyer());
					$smarty->assign("countequipment", $equipment->CountEquipment());
					$smarty->assign("countroom", $room->CountRoom());
					$smarty->assign("countcoop", $coop->CountCoop());
					$smarty->assign("countinvest", $invest->CountInvest());
					$smarty->assign("countfrannchising", $sell->CountSell(true));
					$smarty->assign("countsell", $sell->CountSell());
					
				}
				
				echo fminimizer($smarty->fetch("./site/epoplistbizmain.tpl"));

				$smarty->cache_dir = "./cache";
				$smarty->caching = false; 
        
        
			echo fminimizer($smarty->fetch("./site/request.tpl"));
        ?>              
        </td>
        <td width="70%" valign="top">
        
			<?
				if(isset($objCity) && strlen($objCity->phone) > 0) {
					$smarty->assign("phone", $objCity->phone);
					$smarty->assign("phonetitle", $objCity->phonetitle);
				} 
				
				$smarty->caching = false;
				echo minimizer($smarty->fetch("./site/phonecode.tpl"));						
				$smarty->caching = true;
			?>        
        
            <?
            	$smarty->cache_dir = "./persistentcache"; 
            	$smarty->caching = true; 
            	$smarty->cache_lifetime = 3600; 
				$city_cach = ((isset($_GET["action"]) && $_GET["action"] == "city") ? intval($_GET["ID"]) : 0 );
            	if (!$smarty->is_cached('./site/districtcity.tpl', $city_cach)) {
            		$data = array();
            		$data["offset"] = 0;
            		$data["rowCount"] = 0;
            		$data["sort"] = "Count";
            		$result = $city->SelectActive($data);
                	$smarty->assign("data", $result);
                } 
                
				$output_city = $smarty->fetch("./site/districtcity.tpl", $city_cach);
				echo fminimizer($output_city);
				
				if(isset($_GET["regionID"]) && intval($_GET["regionID"]) > 0) {
					if(!$smarty->is_cached('./site/districtsell.tpl', intval($_GET["regionID"]))) {
						$district->regionID =  intval($_GET["regionID"]);
						$district_list = $district->SelectActive();
						$smarty->assign("districtdata", $district_list);
					}
					$output_district = $smarty->fetch("./site/districtsell.tpl", intval($_GET["regionID"]));
					echo fminimizer($output_district);
				}
				
				$smarty->cache_dir = "./cache";
       			$smarty->caching = false; 
            ?>
            <?
				echo fminimizer($smarty->fetch("./site/navsearchdistrict.tpl"));
            ?>
            <?

                $pagesplit = GetPageSplit();

                if (isset($_GET["offset"])) {
                	$sell->offset = intval($_GET["offset"]);
                } else {
                	$sell->offset = 0;
                }
                if (isset($_GET["rowCount"])) { 
                	$sell->rowCount = intval($_GET["rowCount"]);
                } else {
                	$sell->rowCount = $pagesplit;
                }               
               
				$sell->districtID = (isset($_GET["districtID"]) ? intval($_GET["districtID"]) : "null");
               	$sell->metroID = (isset($_GET["metroID"]) ? intval($_GET["metroID"]) : "null");
               	$sell->CategoryID = (isset($_GET["CategoryID"]) ? intval($_GET["CategoryID"]) : "null");
               	$sell->SubCategoryID = (isset($_GET["SubCategoryID"]) ? intval($_GET["SubCategoryID"]) : "null");
               	$sell->CityID = (isset($_GET["regionID"]) ? intval($_GET["regionID"]) : "null");
               	$sell->streetID = (isset($_GET["streetID"]) ? intval($_GET["streetID"]) : "null");
               		
                $resultPage = $sell->CountSelectTarget();
                    
                $smarty->assign("CountRecord", $resultPage);
                $smarty->assign("CountSplit", $pagesplit);
                $smarty->assign("CountPage", ceil($resultPage/5));
               	
                $result = $sell->SelectTarget();

                $smarty->assign("data", $result);
                    
                
				$output_pagesplit = $smarty->fetch("./site/pagesplitdistrict.tpl", $_SERVER["REQUEST_URI"]);
				echo fminimizer($output_pagesplit);
                  
				$saveme = $smarty->fetch("./site/saveme.tpl");
				echo $saveme;

                $smarty->assign('colorsell', FrontEnd::ListColorSet());

                $smarty->caching = false; 
                //$smarty->display("./site/ilistsell.tpl", $_SERVER["REQUEST_URI"]);
		$output_ilistsell = $smarty->fetch("./site/ilistsell.tpl", $_SERVER["REQUEST_URI"]);
		echo fminimizer($output_ilistsell);
                  
                $smarty->caching = false; 
                
				?>
            	<div class="lftpadding" style="width: auto;">
       				<div  style="background-image:url(<?=$_SERVER["HTTP_HOST"]?>images/bl.gif); background-repeat:repeat-x;">&nbsp;</div>
            	</div>
            	<?
				  	echo fminimizer($output_pagesplit);
            	?> 
            	<?
				  	echo fminimizer($smarty->fetch("./site/navsearchdistrict.tpl"));
				?>
				
				<?
					include_once("./yadirect.php");
				?>				
                                    <? if (IsShowBlockSeoWords()):  ?>
					<div class="lftpadding_cnt" style="padding-top:10pt; padding-bottom: 5pt;padding-top:0pt; padding-bottom:0pt; width:auto;" >
                                            <?
				  	  	
                                                $smarty->caching = true;  
                                                $smarty->cache_dir = "./persistentcache"; 
                                                $smarty->cache_lifetime = 83600;			    
                                                if (!$smarty->is_cached("./site/keysell.tpl")) {
                                                    $datakeysell = $category->GetListSubCategoryActive();
                                                    $smarty->assign("datakeysell", $datakeysell);
                                                }
                                                $output_keysell = $smarty->fetch("./site/keysell.tpl");
                                                echo fminimizer($output_keysell);
                                                $smarty->cache_dir = "./cache";
                                                $smarty->cache_lifetime = 3600;
                                                $smarty->caching = false; 
                                            ?>
                                        </div>
                                    <? endif ?>
            
				<?
				  if (isset($_GET["action"])) {
    				switch(trim($_GET["action"])) {
						case "city": {
							$parentID = (isset($_GET["ID"])  ? intval($_GET["ID"]) : 0);
							$seocity = $city->GetSeoParent($parentID);
							$dataseocity = $city->GetRegionbyParent($parentID);
							if(strlen(trim($seocity->seosell)) > 0 && sizeof($dataseocity) > 1) {
								$smarty->assign("seocity", $seocity);
								$smarty->assign("dataseocity", $dataseocity);
								$smarty->display("./site/hsubregion.tpl");
							}
						}	break;
    				}
				  }
				?>            		
				
				<div class="lftpadding_cnt" style="padding-top:10pt; padding-bottom: 5pt;padding-top:0pt; padding-bottom:0pt; width:auto;" >	    
					<table border="0" align="right" width="100%">
                		<tr>
                			<td width="30%"></td>
                			<td width="70%" align="right">
								<script type="text/javascript" src="//yandex.st/share/share.js" charset="utf-8"></script>
								<div class="yashare-auto-init" data-yashareType="button" data-yashareQuickServices="yaru,vkontakte,facebook,twitter,odnoklassniki,moimir,lj,friendfeed,moikrug"></div>                			
               				</td>			
							</tr>
						</table>
				</div>
            
                               <div class="lftpadding_cnt">
                                <?
                                    echo $review->LastBlockReview();
                                ?>
                               </div>             
				
				
        </td>
    </tr>
    <tr>
    	<td colspan="2">
				<?
				
					$smarty->caching = true; 
					$smarty->cache_lifetime = 10500;
					
					if (!$smarty->is_cached('./site/sugequipment.tpl')) {
            
						$datasugequipment = array();
						$datasugequipment["offset"] = 0;
						$datasugequipment["rowCount"] = 4;
						$datasugequipment["sort"] = "datecreate";
						$datasugequipment["status_id"] = "-1";
            	 
						$res_datasugequipment = $equipment->SugSelect($datasugequipment);
  	          	
						$smarty->assign("datasugequipment", $res_datasugequipment);        	
					}				
				
					?><div class="lftpadding_cnt"><?
						echo fminimizer($smarty->fetch("./site/sugequipment.tpl"));
					?></div><?
					
					$smarty->caching = false; 					
				?>	           		
    	</td>
    </tr>
</table>
<style>
 td a 
 {
   color:black;
 }
</style>
<div style="padding-top:4pt;padding-bottom:5pt;padding-right:10pt;padding-left:10pt;font-size:10pt;font-family:arial;">
	<table cellpadding="0" cellspacing="0" border="0"  style="width:100%;padding-bottom:2pt;" bgcolor="#eeeee0">
    	<tr>
        	<td style="background-image:url(http://<?=$_SERVER["HTTP_HOST"]?>/images/d1.gif); background-repeat:no-repeat;width:5px;height:5px; background-position:left top;">&nbsp;</td>
            <td rowspan="2" valign="middle" align="center"  style="font-size:8pt; font-family:Arial; color:black;">
            </td>
            <td style="background-image:url(http://<?=$_SERVER["HTTP_HOST"]?>/images/d2.gif); background-repeat:no-repeat;width:5px;height:5px;background-position:right top;">&nbsp;</td>
        </tr>
        <tr>
            <td style="background-image:url(http://<?=$_SERVER["HTTP_HOST"]?>/images/d4.gif); background-repeat:no-repeat;width:5px;height:5px; background-position:left bottom;">&nbsp;</td>
            <td style="background-image:url(http://<?=$_SERVER["HTTP_HOST"]?>/images/d3.gif); background-repeat:no-repeat;width:5px;height:5px; background-position:right bottom;">&nbsp;</td>
        </tr>
    </table>
</div>
<?
	$smarty->display("./site/footer.tpl");
?>
<?
  $end = get_formatted_microtime(); 
  $total = $end - $start;
  echo "<center><span style='font-size:7pt;'>".round($total, 6)." : ".$count_sql_call." : ".$base_memory_usage." : ".memoryUsage(memory_get_usage(), $base_memory_usage)." </span><center>";
?>

</td>
</tr>
</table>

</body>
</html>

<?
	$sell->Close();
	$category->Close();
	$city->Close();
	$country->Close();
	$buyer->Close();
	$metro->Close();
	$district->Close();
	$subcategory->Close();
    $streets->Close();
?>