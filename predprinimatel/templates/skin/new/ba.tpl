<div class="topic">

{assign var="oUser" value=$oBa->getUser()}
{assign var="oVote" value=$oBa->getVote()} 
<div class="favorite {if $oUserCurrent}{if $oBa->getIsFavourite()}active{/if}{else}fav-guest{/if}"><a href="#" onclick="lsFavourite.toggle({$oBa->getId()},this,'ba'); return false;"></a></div>
<h1 class="title">{$oBa->getTags()|escape:'html'}</h1>

				<ul class="action">					
					{if $oUserCurrent and ($oUserCurrent->getId()==$oBa->getUserId() or $oUserCurrent->isAdministrator())}
  						<li class="edit"><a href="{cfg name='path.root.web'}/ba/edit/{$oBa->getId()}/" title="{$aLang.topic_edit}">{$aLang.topic_edit}</a></li>
  					{/if}
					{if $oUserCurrent and $oUserCurrent->isAdministrator()}
  						<li class="delete"><a href="{router page='ba'}delete/{$oBa->getId()}/?security_ls_key={$LIVESTREET_SECURITY_KEY}" title="{$aLang.topic_delete}" onclick="return confirm('{$aLang.topic_delete_confirm}');">{$aLang.topic_delete}</a></li>
  					{/if}
				</ul>

<div class="content">
					
	<p>
		{$oBa->getText()|nl2br}
	</p>
	<div class="line"></div>
</div>

				<ul class="tags">
					{foreach from=$oBa->getTagsArray() item=sTag name=tags_list}
						<li><a href="{router page='batag'}{$sTag|escape:'html'}/">{$sTag|escape:'html'}</a>{if !$smarty.foreach.tags_list.last}, {/if}</li>
					{/foreach}									
				</ul>	
				<ul class="voting {if $oVote || ($oUserCurrent && $oBa->getUserId()==$oUserCurrent->getId())|| strtotime($oBa->getDateAdd())<$smarty.now-$oConfig->GetValue('acl.vote.topic.limit_time')}{if $oBa->getRating()>0}positive{elseif $oBa->getRating()<0}negative{/if}{/if} {if !$oUserCurrent || $oBa->getUserId()==$oUserCurrent->getId() || strtotime($oBa->getDateAdd())<$smarty.now-$oConfig->GetValue('acl.vote.topic.limit_time')}guest{/if} {if $oVote} voted {if $oVote->getDirection()>0}plus{elseif $oVote->getDirection()<0}minus{/if}{/if}">
					<li class="plus"><a href="#" onclick="lsVote.vote({$oBa->getId()},this,1,'ba'); return false;"></a></li>
					<li class="total" title="{$aLang.topic_vote_count}: {$oBa->getCountVote()}">{if $oVote || ($oUserCurrent && $oBa->getUserId()==$oUserCurrent->getId()) || strtotime($oBa->getDateAdd())<$smarty.now-$oConfig->GetValue('acl.vote.topic.limit_time')} {if $oBa->getRating()>0}+{/if}{$oBa->getRating()} {else} <a href="#" onclick="lsVote.vote({$oBa->getId()},this,0,'ba'); return false;">&mdash;</a> {/if}</li>
					<li class="minus"><a href="#" onclick="lsVote.vote({$oBa->getId()},this,-1,'ba'); return false;"></a></li>
					<li class="date">{date_format date=$oBa->getDateAdd()}</li>
					<li class="author"><a href="{$oUser->getUserWebPath()}">{$oUser->getLogin()}</a></li>
				</ul>
{include file='social.tpl'}
</div>
