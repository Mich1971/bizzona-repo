{if count($aBas)>0} 
	{foreach from=$aBas item=oBa name=name}
            
                {if in_array($smarty.foreach.name.iteration, array(3,6,9)) }
                    {include file='yadirect.tpl'}
                {/if}
            
			<!-- Ba -->			
			{assign var="oUser" value=$oBa->getUser()} 
			{assign var="oVote" value=$oBa->getVote()} 
			<div class="topic">
				<div class="favorite {if $oUserCurrent}{if $oBa->getIsFavourite()}active{/if}{else}fav-guest{/if}"><a href="#" onclick="lsFavourite.toggle({$oBa->getId()},this,'ba'); return false;"></a></div>
				<h1 class="title"><a href="{$oBa->getUrl()}">{$oBa->getTags()|escape:'html'}</a></h1>
			
			
				<ul class="action"> 
				
					{if $oUserCurrent and ($oUserCurrent->getId()==$oBa->getUserId() or $oUserCurrent->isAdministrator()) }
  						<li class="edit"><a href="{router page='ba'}edit/{$oBa->getId()}/" title="{$aLang.topic_edit}">{$aLang.topic_edit}</a></li>
  					{/if}
					{if $oUserCurrent and $oUserCurrent->isAdministrator()}
							<li class="delete"><a href="{router page='ba'}delete/{$oBa->getId()}/?security_ls_key={$LIVESTREET_SECURITY_KEY}" title="{$aLang.topic_delete}" onclick="return confirm('{$aLang.qa_delete_confirm}');">{$aLang.topic_delete}</a></li> 
					{/if}
					
				</ul>				
				
			<div class="content">
				{$oBa->getText()} 
				<br><br>( <a href="{$oBa->getUrl()}" title="{$aLang.topic_read_more}">{$aLang.qa_read_more}</a> )
			</div>
				
			
			<ul class="tags">
				{foreach from=$oBa->getTagsArray() item=sTag name=tags_list}
					<li><a href="{router page='batag'}{$sTag|escape:'html'}/">{$sTag|escape:'html'}</a>{if !$smarty.foreach.tags_list.last}, {/if}</li>
				{/foreach}								
			</ul>
							
			<ul class="voting  {if $oVote || ($oUserCurrent && $oBa->getUserId()==$oUserCurrent->getId()) || strtotime($oBa->getDateAdd())<$smarty.now-$oConfig->GetValue('acl.vote.topic.limit_time')}{if $oBa->getRating()>0}positive{elseif $oBa->getRating()<0}negative{/if}{/if} {if !$oUserCurrent || $oBa->getUserId()==$oUserCurrent->getId() || strtotime($oBa->getDateAdd())<$smarty.now-$oConfig->GetValue('acl.vote.topic.limit_time')}guest{/if} {if $oVote} voted {if $oVote->getDirection()>0}plus{elseif $oVote->getDirection()<0}minus{/if}{/if}">
				<li class="plus"><a href="#" onclick="lsVote.vote({$oBa->getId()},this,1,'ba'); return false;"></a></li>
				<li class="total" title="{$aLang.topic_vote_count}: {$oBa->getCountVote()}">{if $oVote || ($oUserCurrent && $oBa->getUserId()==$oUserCurrent->getId()) || strtotime($oBa->getDateAdd())<$smarty.now-$oConfig->GetValue('acl.vote.topic.limit_time')} {if $oBa->getRating()>0}+{/if}{$oBa->getRating()} {else} <a href="#" onclick="lsVote.vote({$oBa->getId()},this,0,'ba'); return false;">&mdash;</a> {/if}</li>
				<li class="minus"><a href="#" onclick="lsVote.vote({$oBa->getId()},this,-1,'ba'); return false;"></a></li>
				<li class="date">{date_format date=$oBa->getDateAdd()}</li>
				<li class="author"><a href="{$oUser->getUserWebPath()}">{$oUser->getLogin()}</a></li>
					<li class="comments-total">
						{if $oBa->getCountComment()>0}
							<a href="{$oBa->getUrl()}#comments" title="{$aLang.topic_comment_read}"><span class="green">{$oBa->getCountComment()}</span></a>
						{/if}
						<a href="{$oBa->getUrl()}#comments" title="{$aLang.qa_comment_add}"><span class="red">{$aLang.qa_comment_add}</span></a>
					</li>				
			</ul>
			
			</div>
			<!-- /Ba -->
	{/foreach}	
	
	{include file='paging.tpl' aPaging=`$aPaging`}
	
{else}
{$aLang.blog_no_topic}
{/if}