{if count($aBbs)>0} 
	{foreach from=$aBbs item=oBb name=name}
            
                {if in_array($smarty.foreach.name.iteration, array(3,6,9)) }
                    {include file='yadirect.tpl'}
                {/if}            
            
			<!-- Bb -->			
			{assign var="oUser" value=$oBb->getUser()} 
			{assign var="oVote" value=$oBb->getVote()} 
			<div class="topic">
				<div class="favorite {if $oUserCurrent}{if $oBb->getIsFavourite()}active{/if}{else}fav-guest{/if}"><a href="#" onclick="lsFavourite.toggle({$oBb->getId()},this,'bb'); return false;"></a></div>			
				<h1 class="title"><a href="{$oBb->getUrl()}">{$oBb->getTitle()|escape:'html'}</a></h1>
			
				<ul class="action"> 
				
					{if $oUserCurrent and ($oUserCurrent->getId()==$oBb->getUserId() or $oUserCurrent->isAdministrator()) }
  						<li class="edit"><a href="{router page='bb'}edit/{$oBb->getId()}/" title="{$aLang.topic_edit}">{$aLang.topic_edit}</a></li>
  					{/if}
					{if $oUserCurrent and $oUserCurrent->isAdministrator()}
							<li class="delete"><a href="{router page='bb'}delete/{$oBb->getId()}/?security_ls_key={$LIVESTREET_SECURITY_KEY}" title="{$aLang.topic_delete}" onclick="return confirm('{$aLang.qa_delete_confirm}');">{$aLang.topic_delete}</a></li> 
					{/if}
					
				</ul>				
				
			<div class="content">
				{$oBb->getTextShort()} 
				<br><br>( <a href="{$oBb->getUrl()}" title="{$aLang.topic_read_more}">{$aLang.qa_read_more}</a> )
			</div>
				
			
			<ul class="tags">
				{foreach from=$oBb->getTagsArray() item=sTag name=tags_list}
					<li><a href="{router page='bbtag'}{$sTag|escape:'html'}/">{$sTag|escape:'html'}</a>{if !$smarty.foreach.tags_list.last}, {/if}</li>
				{/foreach}								
			</ul>
							
			<ul class="voting  {if $oVote || ($oUserCurrent && $oBb->getUserId()==$oUserCurrent->getId()) || strtotime($oBb->getDateAdd())<$smarty.now-$oConfig->GetValue('acl.vote.topic.limit_time')}{if $oBb->getRating()>0}positive{elseif $oBb->getRating()<0}negative{/if}{/if} {if !$oUserCurrent || $oBb->getUserId()==$oUserCurrent->getId() || strtotime($oBb->getDateAdd())<$smarty.now-$oConfig->GetValue('acl.vote.topic.limit_time')}guest{/if} {if $oVote} voted {if $oVote->getDirection()>0}plus{elseif $oVote->getDirection()<0}minus{/if}{/if}">
				<li class="plus"><a href="#" onclick="lsVote.vote({$oBb->getId()},this,1,'bb'); return false;"></a></li>
				<li class="total" title="{$aLang.topic_vote_count}: {$oBb->getCountVote()}">{if $oVote || ($oUserCurrent && $oBb->getUserId()==$oUserCurrent->getId()) || strtotime($oBb->getDateAdd())<$smarty.now-$oConfig->GetValue('acl.vote.topic.limit_time')} {if $oBb->getRating()>0}+{/if}{$oBb->getRating()} {else} <a href="#" onclick="lsVote.vote({$oBb->getId()},this,0,'bb'); return false;">&mdash;</a> {/if}</li>
				<li class="minus"><a href="#" onclick="lsVote.vote({$oBb->getId()},this,-1,'bb'); return false;"></a></li>
				<li class="date">{date_format date=$oBb->getDateAdd()}</li>
				<li class="author"><a href="{$oUser->getUserWebPath()}">{$oUser->getLogin()}</a></li>
					<li class="comments-total">
						{if $oBb->getCountComment()>0}
							<a href="{$oBb->getUrl()}#comments" title="{$aLang.topic_comment_read}"><span class="green">{$oBb->getCountComment()}</span></a>
						{/if}
						<a href="{$oBb->getUrl()}#comments" title="{$aLang.qa_comment_add}"><span class="red">{$aLang.qa_comment_add}</span></a>
					</li>				
			</ul>
			
			</div>
			<!-- /Bb -->
	{/foreach}	
	
	{include file='paging.tpl' aPaging=`$aPaging`}
	
{else}
{$aLang.blog_no_topic}
{/if}