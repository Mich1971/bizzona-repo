			<div class="block stream">
				<div class="tl"><div class="tr"></div></div>
				<div class="cl">
					<div class="cr">
						<h1>{$aLang.block_bb}</h1>
<ul class="stream-content">

	{foreach from=$aBb item=oBb name="cmt"}
		{assign var="oUser" value=$oBb->getUser()}
		<li {if $smarty.foreach.cmt.iteration % 2 == 1}class="even"{/if}>
			<a href="{$oUser->getUserWebPath()}" class="stream-author">{$oUser->getLogin()}</a>&nbsp;&#8594;
								<span class="stream-topic-icon"></span><a href="{$oBb->getUrl()}" class="stream-topic">{$oBb->getTitle()|escape:'html'}</a>
			<span>{$oBb->getCountComment()}</span>
		</li>		
	{/foreach}
	
</ul>

<div class="right"><a href="{router page='bb'}">{$aLang.block_bbs_all}</a></div>

					</div>
				</div>
				<div class="bl"><div class="br"></div></div>
			</div>