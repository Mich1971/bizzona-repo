		<ul class="menu">
			<li>
				<a href="{cfg name='path.root.web'}/bs/">{$aLang.bs}</a>
			</li>

{if $oUserCurrent}
		<li {if $sMenuSubItemSelect=='add'}class="active"{/if}>
			<a href="{cfg name='path.root.web'}/bs/add/">{$aLang.bs_menu_add}</a>
		</li>

		<li>
			<a href="{cfg name='path.root.web'}/mybs/{$oUserCurrent->getLogin()}/">{$aLang.bs_my}</a>
		</li>	
		<li>
			<a href="{cfg name='path.root.web'}/mybs/{$oUserCurrent->getLogin()}/comment">{$aLang.bi_mycomment}</a>
		</li>	
		
		<li>
			<a href="{cfg name='path.root.web'}/bs/favourites/">{$aLang.bs_fav}</a>
		</li>	
{/if}	

	</ul>
