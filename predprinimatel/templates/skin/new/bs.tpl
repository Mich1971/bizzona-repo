<div class="topic">
{assign var="oUser" value=$oBs->getUser()}
{assign var="oVote" value=$oBs->getVote()} 

<div class="favorite {if $oUserCurrent}{if $oBs->getIsFavourite()}active{/if}{else}fav-guest{/if}"><a href="#" onclick="lsFavourite.toggle({$oBs->getId()},this,'bs'); return false;"></a></div>
<h1 class="title">{$oBs->getText()|truncate|escape:'html'}</h1>

				<ul class="action">					
					{if $oUserCurrent and ($oUserCurrent->getId()==$oBs->getUserId() or $oUserCurrent->isAdministrator())}
  						<li class="edit"><a href="{cfg name='path.root.web'}/bs/edit/{$oBs->getId()}/" title="{$aLang.topic_edit}">{$aLang.topic_edit}</a></li>
  					{/if}
					{if $oUserCurrent and $oUserCurrent->isAdministrator()}
  						<li class="delete"><a href="{router page='bs'}delete/{$oBs->getId()}/?security_ls_key={$LIVESTREET_SECURITY_KEY}" title="{$aLang.topic_delete}" onclick="return confirm('{$aLang.topic_delete_confirm}');">{$aLang.topic_delete}</a></li>
  					{/if}
				</ul>


<div class="content">
					
	<p>
		{$oBs->getText()|nl2br}
	</p>
	<div class="line"></div>
</div>

				<ul class="voting {if $oVote || ($oUserCurrent && $oBs->getUserId()==$oUserCurrent->getId())|| strtotime($oBs->getDateAdd())<$smarty.now-$oConfig->GetValue('acl.vote.topic.limit_time')}{if $oBs->getRating()>0}positive{elseif $oBs->getRating()<0}negative{/if}{/if} {if !$oUserCurrent || $oBs->getUserId()==$oUserCurrent->getId() || strtotime($oBs->getDateAdd())<$smarty.now-$oConfig->GetValue('acl.vote.topic.limit_time')}guest{/if} {if $oVote} voted {if $oVote->getDirection()>0}plus{elseif $oVote->getDirection()<0}minus{/if}{/if}">
					<li class="plus"><a href="#" onclick="lsVote.vote({$oBs->getId()},this,1,'bs'); return false;"></a></li>
					<li class="total" title="{$aLang.topic_vote_count}: {$oBs->getCountVote()}">{if $oVote || ($oUserCurrent && $oBs->getUserId()==$oUserCurrent->getId()) || strtotime($oBs->getDateAdd())<$smarty.now-$oConfig->GetValue('acl.vote.topic.limit_time')} {if $oBs->getRating()>0}+{/if}{$oBs->getRating()} {else} <a href="#" onclick="lsVote.vote({$oBs->getId()},this,0,'bs'); return false;">&mdash;</a> {/if}</li>
					<li class="minus"><a href="#" onclick="lsVote.vote({$oBs->getId()},this,-1,'bs'); return false;"></a></li>
					<li class="date">{date_format date=$oBs->getDateAdd()}</li>
					<li class="author"><a href="{$oUser->getUserWebPath()}">{$oUser->getLogin()}</a></li>
				</ul>	

{include file='social.tpl'}
</div>
