<ul class="menu">
	<li>
		<a href="{cfg name='path.root.web'}/qa/">{$aLang.qa_all}</a>
	</li>	
	{if $oUserCurrent}
	<li {if $sMenuSubItemSelect=='add'}class="active"{/if}>
		<a href="{cfg name='path.root.web'}/qa/add/">{$aLang.qa_menu_add}</a>
	</li>
	<li>
		<a href="{cfg name='path.root.web'}/myqa/{$oUserCurrent->getLogin()}/">{$aLang.qa_my}</a>
	</li>
	<li>
		<a href="{cfg name='path.root.web'}/myqa/{$oUserCurrent->getLogin()}/answer">{$aLang.qa_myanswer}</a>
	</li>		
	<li>
		<a href="{cfg name='path.root.web'}/qa/favourites/">{$aLang.qa_fav}</a>
	</li>	
	{/if}
	
</ul>


