{if count($aBss)>0} 
	{foreach from=$aBss item=oBs name=name}
            
                {if in_array($smarty.foreach.name.iteration, array(3,6,9)) }
                    {include file='yadirect.tpl'}
                {/if}              
              
			<!-- Bs -->			
			{assign var="oUser" value=$oBs->getUser()} 
			{assign var="oVote" value=$oBs->getVote()} 
			
			<div class="topic">
				<div class="favorite {if $oUserCurrent}{if $oBs->getIsFavourite()}active{/if}{else}fav-guest{/if}"><a href="#" onclick="lsFavourite.toggle({$oBs->getId()},this,'bs'); return false;"></a></div>						
				<h1 class="title"><a href="{$oBs->getUrl()}">{$oBs->getText()|truncate|escape:'html'}</a></h1>

				<ul class="action"> 
				
					{if $oUserCurrent and ($oUserCurrent->getId()==$oBs->getUserId() or $oUserCurrent->isAdministrator()) }
  						<li class="edit"><a href="{router page='bs'}edit/{$oBs->getId()}/" title="{$aLang.topic_edit}">{$aLang.topic_edit}</a></li>
  					{/if}
					{if $oUserCurrent and $oUserCurrent->isAdministrator()}
							<li class="delete"><a href="{router page='bs'}delete/{$oBs->getId()}/?security_ls_key={$LIVESTREET_SECURITY_KEY}" title="{$aLang.topic_delete}" onclick="return confirm('{$aLang.bs_delete_confirm}');">{$aLang.topic_delete}</a></li> 
					{/if}
					
				</ul>				
				
			<div class="content">
				{$oBs->getText()|truncate:800}
				<br><br>( <a href="{$oBs->getUrl()}" title="{$aLang.topic_read_more}">{$aLang.qa_read_more}</a> )
			</div>
							
			<ul class="voting  {if $oVote || ($oUserCurrent && $oBs->getUserId()==$oUserCurrent->getId()) || strtotime($oBs->getDateAdd())<$smarty.now-$oConfig->GetValue('acl.vote.topic.limit_time')}{if $oBs->getRating()>0}positive{elseif $oBs->getRating()<0}negative{/if}{/if} {if !$oUserCurrent || $oBs->getUserId()==$oUserCurrent->getId() || strtotime($oBs->getDateAdd())<$smarty.now-$oConfig->GetValue('acl.vote.topic.limit_time')}guest{/if} {if $oVote} voted {if $oVote->getDirection()>0}plus{elseif $oVote->getDirection()<0}minus{/if}{/if}">
				<li class="plus"><a href="#" onclick="lsVote.vote({$oBs->getId()},this,1,'bs'); return false;"></a></li>
				<li class="total" title="{$aLang.topic_vote_count}: {$oBs->getCountVote()}">{if $oVote || ($oUserCurrent && $oBs->getUserId()==$oUserCurrent->getId()) || strtotime($oBs->getDateAdd())<$smarty.now-$oConfig->GetValue('acl.vote.topic.limit_time')} {if $oBs->getRating()>0}+{/if}{$oBs->getRating()} {else} <a href="#" onclick="lsVote.vote({$oBs->getId()},this,0,'bs'); return false;">&mdash;</a> {/if}</li>
				<li class="minus"><a href="#" onclick="lsVote.vote({$oBs->getId()},this,-1,'bs'); return false;"></a></li>
				<li class="date">{date_format date=$oBs->getDateAdd()}</li>
				<li class="author"><a href="{$oUser->getUserWebPath()}">{$oUser->getLogin()}</a></li>
					<li class="comments-total">
						{if $oBs->getCountComment()>0}
							<a href="{$oBs->getUrl()}#comments" title="{$aLang.topic_comment_read}"><span class="green">{$oBs->getCountComment()}</span></a>
						{/if}
						<a href="{$oBs->getUrl()}#comments" title="{$aLang.qa_comment_add}"><span class="red">{$aLang.qa_comment_add}</span></a>
					</li>				
			</ul>
			
			</div>
			<!-- /Bs -->
	{/foreach}	
	
	{include file='paging.tpl' aPaging=`$aPaging`}
	
{else}
{$aLang.blog_no_topic}
{/if}