{if count($aBis)>0}
	{foreach from=$aBis item=oBi name=name}   
            
                {if in_array($smarty.foreach.name.iteration, array(3,6,9)) }
                    {include file='yadirect.tpl'}
                {/if}            
            
            
			{assign var="oUser" value=$oBi->getUser()} 
			{assign var="oVote" value=$oBi->getVote()} 
			<!-- Bi -->			
			<div class="topic">
				<div class="favorite {if $oUserCurrent}{if $oBi->getIsFavourite()}active{/if}{else}fav-guest{/if}"><a href="#" onclick="lsFavourite.toggle({$oBi->getId()},this,'bi'); return false;"></a></div>			
				<h1 class="title">		
					<a href="{if $oBi->getType()=='link'}{router page='link'}go/{$oBi->getId()}/{else}{$oBi->getUrl()}{/if}">{$oBi->getTitle()|escape:'html'}</a>
				</h1>
				<ul class="action">
					{if $oUserCurrent and ($oUserCurrent->getId()==$oBi->getUserId() or $oUserCurrent->isAdministrator())}
  						<li class="edit"><a href="{router page='bi'}edit/{$oBi->getId()}/" title="{$aLang.topic_edit}">{$aLang.topic_edit}</a></li>
  					{/if}
					{if $oUserCurrent and $oUserCurrent->isAdministrator()}
							<li class="delete"><a href="{router page='bi'}delete/{$oBi->getId()}/?security_ls_key={$LIVESTREET_SECURITY_KEY}" title="{$aLang.bi_delete}" onclick="return confirm('{$aLang.bi_delete_confirm}');">{$aLang.bi_delete}</a></li>
					{/if}
				</ul>				
				<div class="content">
					{$oBi->getTextShort()}
				</div>				
				<ul class="tags">
					{foreach from=$oBi->getTagsArray() item=sTag name=tags_list}
						<li><a href="{router page='bitag'}{$sTag|escape:'html'}/">{$sTag|escape:'html'}</a>{if !$smarty.foreach.tags_list.last}, {/if}</li>
					{/foreach}								
				</ul>

				<ul class="voting {if $oVote || ($oUserCurrent && $oBi->getUserId()==$oUserCurrent->getId()) || strtotime($oBi->getDateAdd())<$smarty.now-$oConfig->GetValue('acl.vote.topic.limit_time')}{if $oBi->getRating()>0}positive{elseif $oBi->getRating()<0}negative{/if}{/if} {if !$oUserCurrent || $oBi->getUserId()==$oUserCurrent->getId() || strtotime($oBi->getDateAdd())<$smarty.now-$oConfig->GetValue('acl.vote.topic.limit_time')}guest{/if} {if $oVote} voted {if $oVote->getDirection()>0}plus{elseif $oVote->getDirection()<0}minus{/if}{/if}">
					<li class="plus"><a href="#" onclick="lsVote.vote({$oBi->getId()},this,1,'bi'); return false;"></a></li>
					<li class="total" title="{$aLang.topic_vote_count}: {$oBi->getCountVote()}">{if $oVote || ($oUserCurrent && $oBi->getUserId()==$oUserCurrent->getId()) || strtotime($oBi->getDateAdd())<$smarty.now-$oConfig->GetValue('acl.vote.topic.limit_time')} {if $oBi->getRating()>0}+{/if}{$oBi->getRating()} {else} <a href="#" onclick="lsVote.vote({$oBi->getId()},this,0,'bi'); return false;">&mdash;</a> {/if}</li>
					<li class="minus"><a href="#" onclick="lsVote.vote({$oBi->getId()},this,-1,'bi'); return false;"></a></li>				
					<li class="date">{date_format date=$oBi->getDateAdd()}</li>
					<li class="author"><a href="{$oUser->getUserWebPath()}">{$oUser->getLogin()}</a></li> 
					<li class="comments-total">
						{if $oBi->getCountComment()>0}
							<a href="{$oBi->getUrl()}#comments" title="{$aLang.topic_comment_read}"><span class="green">{$oBi->getCountComment()}</span></a>
						{/if}
						<a href="{$oBi->getUrl()}#comments" title="{$aLang.qa_comment_add}"><span class="red">{$aLang.qa_comment_add}</span></a>
					</li>					
				</ul>
							
			</div>
			<!-- /Bi -->
	{/foreach}	
		
    {include file='paging.tpl' aPaging=`$aPaging`}
	
{else}
{$aLang.blog_no_topic}
{/if}