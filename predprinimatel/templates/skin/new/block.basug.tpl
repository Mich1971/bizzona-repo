			<div class="block stream">
				<div class="tl"><div class="tr"></div></div>
				<div class="cl">
					<div class="cr">
						<h1>{$aLang.ba_sug_block_title}</h1>
<ul class="stream-content">
	{foreach from=$aBaSug item=oBa name="cmt"}
		{assign var="oUser" value=$oBa->getUser()}
		<li {if $smarty.foreach.cmt.iteration % 2 == 1}class="even"{/if}>
			<a href="{$oUser->getUserWebPath()}" class="stream-author">{$oUser->getLogin()}</a>&nbsp;&#8594;
								<span class="stream-topic-icon"></span><a href="{$oBa->getUrl()}" class="stream-topic">{$oBa->getText()|escape:'html'}</a>
			<span>{$oBa->getCountComment()}</span>
		</li>		
	{/foreach}
</ul>

<div class="right"><a href="{router page='ba'}">{$aLang.block_bas_all}</a></div>

					</div>
				</div>
				<div class="bl"><div class="br"></div></div>
			</div>