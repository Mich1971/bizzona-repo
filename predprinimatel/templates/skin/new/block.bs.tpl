			<div class="block stream">
				<div class="tl"><div class="tr"></div></div>
				<div class="cl">
					<div class="cr">
						<h1>{$aLang.block_bs}</h1>
<ul class="stream-content">

	{foreach from=$aBs item=oBs name="cmt"}
		{assign var="oUser" value=$oBs->getUser()}
		<li {if $smarty.foreach.cmt.iteration % 2 == 1}class="even"{/if}>
			<a href="{$oUser->getUserWebPath()}" class="stream-author">{$oUser->getLogin()}</a>&nbsp;&#8594;
								<span class="stream-topic-icon"></span><a href="{$oBs->getUrl()}" class="stream-topic">{$oBs->getText()|truncate|escape:'html'}</a>
			<span>{$oBs->getCountComment()}</span>
		</li>		
	{/foreach}
	
</ul>

<div class="right"><a href="{router page='bs'}">{$aLang.block_bss_all}</a></div>

					</div>
				</div>
				<div class="bl"><div class="br"></div></div>
			</div>