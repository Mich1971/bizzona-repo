
	<ul class="menu">
		<li>
			<a href="{cfg name='path.root.web'}/ba/">{$aLang.ba_all}</a>
		</li>	
		{if $oUserCurrent}
		<li {if $sMenuSubItemSelect=='add'}class="active"{/if}>
			<a href="{cfg name='path.root.web'}/ba/add/">{$aLang.ba_menu_add}</a>
		</li>
		<li>
			<a href="{cfg name='path.root.web'}/myba/{$oUserCurrent->getLogin()}/">{$aLang.ba_my}</a>
		</li>		
		<li>
			<a href="{cfg name='path.root.web'}/myba/{$oUserCurrent->getLogin()}/comment">{$aLang.bi_mycomment}</a>
		</li>		
		<li>
			<a href="{cfg name='path.root.web'}/ba/favourites/">{$aLang.ba_fav}</a>
		</li>
		{/if}
	</ul>
