<ul class="menu">
	<li>
		<a href="{cfg name='path.root.web'}/bi/">{$aLang.bi}</a>
	</li>	
{if $oUserCurrent}		
	<li {if $sMenuSubItemSelect=='add'}class="active"{/if}>
		<a href="{cfg name='path.root.web'}/bi/add/">{$aLang.bi_menu_add}</a>
	</li>
	<li>
		<a href="{cfg name='path.root.web'}/mybi/{$oUserCurrent->getLogin()}/">{$aLang.bi_my}</a>
	</li>	
	<li>
		<a href="{cfg name='path.root.web'}/mybi/{$oUserCurrent->getLogin()}/comment">{$aLang.bi_mycomment}</a>
	</li>	
	<li>
		<a href="{cfg name='path.root.web'}/bi/favourites/">{$aLang.bi_fav}</a>
	</li>		
{/if}		
</ul>