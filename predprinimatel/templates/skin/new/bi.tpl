<div class="topic">

{assign var="oUser" value=$oBi->getUser()}
{assign var="oVote" value=$oBi->getVote()}

<div class="favorite {if $oUserCurrent}{if $oBi->getIsFavourite()}active{/if}{else}fav-guest{/if}"><a href="#" onclick="lsFavourite.toggle({$oBi->getId()},this,'bi'); return false;"></a></div>
<h1 class="title">{$oBi->getTitle()|escape:'html'}</h1>

				<ul class="action">					
					{if $oUserCurrent and ($oUserCurrent->getId()==$oBi->getUserId() or $oUserCurrent->isAdministrator())}
  						<li class="edit"><a href="{cfg name='path.root.web'}/bi/edit/{$oBi->getId()}/" title="{$aLang.topic_edit}">{$aLang.topic_edit}</a></li>
  					{/if}
					{if $oUserCurrent and $oUserCurrent->isAdministrator()}
  						<li class="delete"><a href="{router page='bi'}delete/{$oBi->getId()}/?security_ls_key={$LIVESTREET_SECURITY_KEY}" title="{$aLang.topic_delete}" onclick="return confirm('{$aLang.topic_delete_confirm}');">{$aLang.topic_delete}</a></li>
  					{/if}
				</ul>

<div class="content">
					
	<p>
		{$oBi->getText()|nl2br}
	</p>
	<div class="line"></div>
</div>

				<ul class="tags">
					{foreach from=$oBi->getTagsArray() item=sTag name=tags_list}
						<li><a href="{router page='bitag'}{$sTag|escape:'html'}/">{$sTag|escape:'html'}</a>{if !$smarty.foreach.tags_list.last}, {/if}</li>
					{/foreach}									
				</ul>	
				<ul class="voting {if $oVote || ($oUserCurrent && $oBi->getUserId()==$oUserCurrent->getId())|| strtotime($oBi->getDateAdd())<$smarty.now-$oConfig->GetValue('acl.vote.topic.limit_time')}{if $oBi->getRating()>0}positive{elseif $oBi->getRating()<0}negative{/if}{/if} {if !$oUserCurrent || $oBi->getUserId()==$oUserCurrent->getId() || strtotime($oBi->getDateAdd())<$smarty.now-$oConfig->GetValue('acl.vote.topic.limit_time')}guest{/if} {if $oVote} voted {if $oVote->getDirection()>0}plus{elseif $oVote->getDirection()<0}minus{/if}{/if}">
					<li class="plus"><a href="#" onclick="lsVote.vote({$oBi->getId()},this,1,'bi'); return false;"></a></li>
					<li class="total" title="{$aLang.topic_vote_count}: {$oBi->getCountVote()}">{if $oVote || ($oUserCurrent && $oBi->getUserId()==$oUserCurrent->getId()) || strtotime($oBi->getDateAdd())<$smarty.now-$oConfig->GetValue('acl.vote.topic.limit_time')} {if $oBi->getRating()>0}+{/if}{$oBi->getRating()} {else} <a href="#" onclick="lsVote.vote({$oBi->getId()},this,0,'bi'); return false;">&mdash;</a> {/if}</li>
					<li class="minus"><a href="#" onclick="lsVote.vote({$oBi->getId()},this,-1,'bi'); return false;"></a></li>
					<li class="date">{date_format date=$oBi->getDateAdd()}</li>
					<li class="author"><a href="{$oUser->getUserWebPath()}">{$oUser->getLogin()}</a></li>
				</ul>
{include file='social.tpl'}
</div>
