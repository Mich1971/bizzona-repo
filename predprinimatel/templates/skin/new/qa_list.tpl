{if count($aQas)>0} 
	{foreach from=$aQas item=oQa name=name}
            
                {if in_array($smarty.foreach.name.iteration, array(3,6,9)) }
                    {include file='yadirect.tpl'}
                {/if}              
            
			<!-- Qa -->			
			{assign var="oUser" value=$oQa->getUser()} 
			{assign var="oVote" value=$oQa->getVote()} 
			
			<div class="topic">

				<div class="favorite {if $oUserCurrent}{if $oQa->getIsFavourite()}active{/if}{else}fav-guest{/if}"><a href="#" onclick="lsFavourite.toggle({$oQa->getId()},this,'qa'); return false;"></a></div>			
			
				<h1 class="title"><a href="{$oQa->getUrl()}">{$oQa->getTitle()|escape:'html'}</a></h1>
			
				<ul class="action"> 
				
					{if $oUserCurrent and ($oUserCurrent->getId()==$oQa->getUserId() or $oUserCurrent->isAdministrator()) }
  						<li class="edit"><a href="{router page='qa'}edit/{$oQa->getId()}/" title="{$aLang.topic_edit}">{$aLang.topic_edit}</a></li>
  					{/if}
					{if $oUserCurrent and $oUserCurrent->isAdministrator()}
							<li class="delete"><a href="{router page='qa'}delete/{$oQa->getId()}/?security_ls_key={$LIVESTREET_SECURITY_KEY}" title="{$aLang.topic_delete}" onclick="return confirm('{$aLang.qa_delete_confirm}');">{$aLang.topic_delete}</a></li> 
					{/if}
					
				</ul>				
				
			<div class="content">
				{$oQa->getDescription()}
				<br><br>( <a href="{$oQa->getUrl()}" title="{$aLang.topic_read_more}">{$aLang.qa_read_more}</a> )
			</div>
			
			<ul class="tags">
				{foreach from=$oQa->getTagsArray() item=sTag name=tags_list}
					<li><a href="{router page='qatag'}{$sTag|escape:'html'}/">{$sTag|escape:'html'}</a>{if !$smarty.foreach.tags_list.last}, {/if}</li>
				{/foreach}								
			</ul>
							
			<ul class="voting  {if $oVote || ($oUserCurrent && $oQa->getUserId()==$oUserCurrent->getId()) || strtotime($oQa->getDateAdd())<$smarty.now-$oConfig->GetValue('acl.vote.topic.limit_time')}{if $oQa->getRating()>0}positive{elseif $oQa->getRating()<0}negative{/if}{/if} {if !$oUserCurrent || $oQa->getUserId()==$oUserCurrent->getId() || strtotime($oQa->getDateAdd())<$smarty.now-$oConfig->GetValue('acl.vote.topic.limit_time')}guest{/if} {if $oVote} voted {if $oVote->getDirection()>0}plus{elseif $oVote->getDirection()<0}minus{/if}{/if}">
				<li class="plus"><a href="#" onclick="lsVote.vote({$oQa->getId()},this,1,'qa'); return false;"></a></li>
				<li class="total" title="{$aLang.topic_vote_count}: {$oQa->getCountVote()}">{if $oVote || ($oUserCurrent && $oQa->getUserId()==$oUserCurrent->getId()) || strtotime($oQa->getDateAdd())<$smarty.now-$oConfig->GetValue('acl.vote.topic.limit_time')} {if $oQa->getRating()>0}+{/if}{$oQa->getRating()} {else} <a href="#" onclick="lsVote.vote({$oQa->getId()},this,0,'qa'); return false;">&mdash;</a> {/if}</li>
				<li class="minus"><a href="#" onclick="lsVote.vote({$oQa->getId()},this,-1,'qa'); return false;"></a></li>
				<li class="date">{date_format date=$oQa->getDateAdd()}</li>
				<li class="author"><a href="{$oUser->getUserWebPath()}">{$oUser->getLogin()}</a></li>
					<li class="comments-total">
						{if $oQa->getCountComment()>0}
							<a href="{$oQa->getUrl()}#comments" title="{$aLang.topic_comment_read}"><span class="green">{$oQa->getCountComment()}</span></a>
						{/if}
						<a href="{$oQa->getUrl()}#comments" title="{$aLang.qa_comment_add}"><span class="red">{$aLang.qa_comment_add}</span></a>
					</li>				
			</ul>
			
			</div>
			<!-- /Qa -->
	{/foreach}	
	
	{include file='paging.tpl' aPaging=`$aPaging`}
	
{else}
{$aLang.blog_no_topic}
{/if}