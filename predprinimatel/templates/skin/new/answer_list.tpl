{foreach from=$aComments item=oComment}
	{assign var="oQa" value=$oComment->getTarget()}
	{assign var="oUser" value=$oComment->getUser()}
		<div class="comments padding-none">
			<div class="comment">
				<div class="comment-topic"><a href="{$oQa->getUrl()}">{$oQa->getTitle()|escape:'html'}</a>  <a href="{$oQa->getUrl()}#comments" class="comment-total">{$oQa->getCountComment()}</a></div>
				<div class="voting {if $oComment->getRating()>0}positive{elseif $oComment->getRating()<0}negative{/if}">
					<div class="total">{if $oComment->getRating()>0}+{/if}{$oComment->getRating()}</div>
				</div>
				<div class="content">
					<div class="tb"><div class="tl"><div class="tr"></div></div></div>							
					<div class="text">
		        		{$oComment->getText()}
					</div>			
					<div class="bl"><div class="bb"><div class="br"></div></div></div>
				</div>								
			</div>
			<div class="info">
				<a href="{$oUser->getUserWebPath()}"><img src="{$oUser->getProfileAvatarPath(24)}" alt="avatar" class="avatar" /></a>
					<p><a href="{$oUser->getUserWebPath()}" class="author">{$oUser->getLogin()}</a></p>
				<ul>
					<li class="date">{date_format date=$oComment->getDate()}</li>								
					<li><a href="{$oQa->getUrl()}#comment{$oComment->getId()}" class="imglink link"></a></li>  									
   					{if $oUserCurrent}
						<li class="favorite {if $oComment->getIsFavourite()}active{/if}"><a href="#" onclick="lsFavourite.toggle({$oComment->getId()},this,'comment'); return false;"></a></li>	
					{/if}	
				</ul>
			</div>				
		</div>
{/foreach}	
	
{include file='paging.tpl' aPaging=`$aPaging`}