<div class="topic">
    
{include file='social.tpl'}    
    
{assign var="oUser" value=$oBb->getUser()}
{assign var="oVote" value=$oBb->getVote()} 
<div class="favorite {if $oUserCurrent}{if $oBb->getIsFavourite()}active{/if}{else}fav-guest{/if}"><a href="#" onclick="lsFavourite.toggle({$oBb->getId()},this,'bb'); return false;"></a></div>
<h1 class="title">{$oBb->getTitle()|escape:'html'}</h1>

				<ul class="action">					
					{if $oUserCurrent and ($oUserCurrent->getId()==$oBb->getUserId() or $oUserCurrent->isAdministrator())}
  						<li class="edit"><a href="{cfg name='path.root.web'}/bb/edit/{$oBb->getId()}/" title="{$aLang.topic_edit}">{$aLang.topic_edit}</a></li>
  					{/if}
					{if $oUserCurrent and $oUserCurrent->isAdministrator()}
  						<li class="delete"><a href="{router page='bb'}delete/{$oBb->getId()}/?security_ls_key={$LIVESTREET_SECURITY_KEY}" title="{$aLang.topic_delete}" onclick="return confirm('{$aLang.topic_delete_confirm}');">{$aLang.topic_delete}</a></li>
  					{/if}
				</ul>

<div class="content">
					
	<p>
		{$oBb->getText()|nl2br}
	</p>
	<div class="line"></div>
</div>	
				<ul class="tags">
					{foreach from=$oBb->getTagsArray() item=sTag name=tags_list}
						<li><a href="{router page='bbtag'}{$sTag|escape:'html'}/">{$sTag|escape:'html'}</a>{if !$smarty.foreach.tags_list.last}, {/if}</li>
					{/foreach}									
				</ul>	
				<ul class="voting {if $oVote || ($oUserCurrent && $oBb->getUserId()==$oUserCurrent->getId())|| strtotime($oBb->getDateAdd())<$smarty.now-$oConfig->GetValue('acl.vote.topic.limit_time')}{if $oBb->getRating()>0}positive{elseif $oBb->getRating()<0}negative{/if}{/if} {if !$oUserCurrent || $oBb->getUserId()==$oUserCurrent->getId() || strtotime($oBb->getDateAdd())<$smarty.now-$oConfig->GetValue('acl.vote.topic.limit_time')}guest{/if} {if $oVote} voted {if $oVote->getDirection()>0}plus{elseif $oVote->getDirection()<0}minus{/if}{/if}">
					<li class="plus"><a href="#" onclick="lsVote.vote({$oBb->getId()},this,1,'bb'); return false;"></a></li>
					<li class="total" title="{$aLang.topic_vote_count}: {$oBb->getCountVote()}">{if $oVote || ($oUserCurrent && $oBb->getUserId()==$oUserCurrent->getId()) || strtotime($oBb->getDateAdd())<$smarty.now-$oConfig->GetValue('acl.vote.topic.limit_time')} {if $oBb->getRating()>0}+{/if}{$oBb->getRating()} {else} <a href="#" onclick="lsVote.vote({$oBb->getId()},this,0,'bb'); return false;">&mdash;</a> {/if}</li>
					<li class="minus"><a href="#" onclick="lsVote.vote({$oBb->getId()},this,-1,'bb'); return false;"></a></li>
					<li class="date">{date_format date=$oBb->getDateAdd()}</li>
					<li class="author"><a href="{$oUser->getUserWebPath()}">{$oUser->getLogin()}</a></li>
				</ul>	
	
{include file='social.tpl'}

</div>
