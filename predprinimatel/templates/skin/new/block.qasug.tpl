			<div class="block stream">
				<div class="tl"><div class="tr"></div></div>
				<div class="cl">
					<div class="cr">
						<h1>{$aLang.qa_sug_block_title}</h1>
<ul class="stream-content">
	{foreach from=$aQaSug item=oQa name="cmt"}
		{assign var="oUser" value=$oQa->getUser()}
		<li {if $smarty.foreach.cmt.iteration % 2 == 1}class="even"{/if}>
			<a href="{$oUser->getUserWebPath()}" class="stream-author">{$oUser->getLogin()}</a>&nbsp;&#8594;
								<span class="stream-topic-icon"></span><a href="{$oQa->getUrl()}" class="stream-topic">{$oQa->getTitle()|escape:'html'}</a>
			<span>{$oQa->getCountComment()}</span>
		</li>		
	{/foreach}
</ul>

<div class="right"><a href="{router page='qa'}">{$aLang.block_qas_all}</a></div>

					</div>
				</div>
				<div class="bl"><div class="br"></div></div>
			</div>