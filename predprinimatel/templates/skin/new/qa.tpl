<div class="topic">
{assign var="oUser" value=$oQa->getUser()}
{assign var="oVote" value=$oQa->getVote()} 

<div class="favorite {if $oUserCurrent}{if $oQa->getIsFavourite()}active{/if}{else}fav-guest{/if}"><a href="#" onclick="lsFavourite.toggle({$oQa->getId()},this,'qa'); return false;"></a></div>
<h1 class="title">{$oQa->getTitle()|escape:'html'}</h1>

				<ul class="action">					
					{if $oUserCurrent and ($oUserCurrent->getId()==$oQa->getUserId() or $oUserCurrent->isAdministrator())}
  						<li class="edit"><a href="{cfg name='path.root.web'}/qa/edit/{$oQa->getId()}/" title="{$aLang.topic_edit}">{$aLang.topic_edit}</a></li>
  					{/if}
					{if $oUserCurrent and $oUserCurrent->isAdministrator()}
  						<li class="delete"><a href="{router page='qa'}delete/{$oQa->getId()}/?security_ls_key={$LIVESTREET_SECURITY_KEY}" title="{$aLang.topic_delete}" onclick="return confirm('{$aLang.topic_delete_confirm}');">{$aLang.topic_delete}</a></li>
  					{/if}
				</ul>


<div class="content">
					
	<p>
		{$oQa->getDescription()|nl2br}
	</p>
	<div class="line"></div>
</div>

				<ul class="tags">
					{foreach from=$oQa->getTagsArray() item=sTag name=tags_list}
						<li><a href="{router page='qatag'}{$sTag|escape:'html'}/">{$sTag|escape:'html'}</a>{if !$smarty.foreach.tags_list.last}, {/if}</li>
					{/foreach}									
				</ul>	
				<ul class="voting {if $oVote || ($oUserCurrent && $oQa->getUserId()==$oUserCurrent->getId())|| strtotime($oQa->getDateAdd())<$smarty.now-$oConfig->GetValue('acl.vote.topic.limit_time')}{if $oQa->getRating()>0}positive{elseif $oQa->getRating()<0}negative{/if}{/if} {if !$oUserCurrent || $oQa->getUserId()==$oUserCurrent->getId() || strtotime($oQa->getDateAdd())<$smarty.now-$oConfig->GetValue('acl.vote.topic.limit_time')}guest{/if} {if $oVote} voted {if $oVote->getDirection()>0}plus{elseif $oVote->getDirection()<0}minus{/if}{/if}">
					<li class="plus"><a href="#" onclick="lsVote.vote({$oQa->getId()},this,1,'qa'); return false;"></a></li>
					<li class="total" title="{$aLang.topic_vote_count}: {$oQa->getCountVote()}">{if $oVote || ($oUserCurrent && $oQa->getUserId()==$oUserCurrent->getId()) || strtotime($oQa->getDateAdd())<$smarty.now-$oConfig->GetValue('acl.vote.topic.limit_time')} {if $oQa->getRating()>0}+{/if}{$oQa->getRating()} {else} <a href="#" onclick="lsVote.vote({$oQa->getId()},this,0,'qa'); return false;">&mdash;</a> {/if}</li>
					<li class="minus"><a href="#" onclick="lsVote.vote({$oQa->getId()},this,-1,'qa'); return false;"></a></li>
					<li class="date">{date_format date=$oQa->getDateAdd()}</li>
					<li class="author"><a href="{$oUser->getUserWebPath()}">{$oUser->getLogin()}</a></li>
				</ul>	

{include file='social.tpl'}
</div>
