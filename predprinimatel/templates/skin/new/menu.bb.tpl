
	<ul class="menu">
		<li>
			<a href="{cfg name='path.root.web'}/bb/">{$aLang.bb}</a>
		</li>	
		{if $oUserCurrent}
		<li {if $sMenuSubItemSelect=='add'}class="active"{/if}>
			<a href="{cfg name='path.root.web'}/bb/add/">{$aLang.bb_menu_add}</a>
		</li>
		<li>
			<a href="{cfg name='path.root.web'}/mybb/{$oUserCurrent->getLogin()}/">{$aLang.bb_my}</a>
		</li>		
		<li>
			<a href="{cfg name='path.root.web'}/mybb/{$oUserCurrent->getLogin()}/comment">{$aLang.bb_mycomment}</a>
		</li>
		<li>
			<a href="{cfg name='path.root.web'}/bb/favourites/">{$aLang.bb_fav}</a>
		</li>		
		{/if}
	</ul>
