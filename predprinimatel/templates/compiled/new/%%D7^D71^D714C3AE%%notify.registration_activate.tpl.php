<?php /* Smarty version 2.6.19, created on 2011-02-10 14:38:11
         compiled from notify/russian/notify.registration_activate.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'cfg', 'notify/russian/notify.registration_activate.tpl', 1, false),array('function', 'router', 'notify/russian/notify.registration_activate.tpl', 7, false),)), $this); ?>
Вы зарегистрировались на сайте <a href="<?php echo smarty_function_cfg(array('name' => 'path.root.web'), $this);?>
"><?php echo smarty_function_cfg(array('name' => 'view.name'), $this);?>
</a><br>
Ваши регистрационные данные:<br>
&nbsp;&nbsp;&nbsp;логин: <b><?php echo $this->_tpl_vars['oUser']->getLogin(); ?>
</b><br>
&nbsp;&nbsp;&nbsp;пароль: <b><?php echo $this->_tpl_vars['sPassword']; ?>
</b><br>
<br>
Для завершения регистрации вам необходимо активировать аккаунт пройдя по ссылке: 
<a href="<?php echo smarty_function_router(array('page' => 'registration'), $this);?>
activate/<?php echo $this->_tpl_vars['oUser']->getActivateKey(); ?>
/"><?php echo smarty_function_router(array('page' => 'registration'), $this);?>
activate/<?php echo $this->_tpl_vars['oUser']->getActivateKey(); ?>
/</a>

<br><br>
С уважением, администрация сайта <a href="<?php echo smarty_function_cfg(array('name' => 'path.root.web'), $this);?>
"><?php echo smarty_function_cfg(array('name' => 'view.name'), $this);?>
</a>