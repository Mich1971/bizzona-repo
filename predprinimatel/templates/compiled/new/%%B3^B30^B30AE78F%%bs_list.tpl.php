<?php /* Smarty version 2.6.19, created on 2014-05-19 11:00:15
         compiled from bs_list.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'truncate', 'bs_list.tpl', 14, false),array('modifier', 'escape', 'bs_list.tpl', 14, false),array('function', 'router', 'bs_list.tpl', 19, false),array('function', 'date_format', 'bs_list.tpl', 36, false),)), $this); ?>
<?php if (count ( $this->_tpl_vars['aBss'] ) > 0): ?> 
	<?php $_from = $this->_tpl_vars['aBss']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['name'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['name']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['oBs']):
        $this->_foreach['name']['iteration']++;
?>
            
                <?php if (in_array ( $this->_foreach['name']['iteration'] , array ( 3 , 6 , 9 ) )): ?>
                    <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'yadirect.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
                <?php endif; ?>              
              
			<!-- Bs -->			
			<?php $this->assign('oUser', $this->_tpl_vars['oBs']->getUser()); ?> 
			<?php $this->assign('oVote', $this->_tpl_vars['oBs']->getVote()); ?> 
			
			<div class="topic">
				<div class="favorite <?php if ($this->_tpl_vars['oUserCurrent']): ?><?php if ($this->_tpl_vars['oBs']->getIsFavourite()): ?>active<?php endif; ?><?php else: ?>fav-guest<?php endif; ?>"><a href="#" onclick="lsFavourite.toggle(<?php echo $this->_tpl_vars['oBs']->getId(); ?>
,this,'bs'); return false;"></a></div>						
				<h1 class="title"><a href="<?php echo $this->_tpl_vars['oBs']->getUrl(); ?>
"><?php echo ((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['oBs']->getText())) ? $this->_run_mod_handler('truncate', true, $_tmp) : smarty_modifier_truncate($_tmp)))) ? $this->_run_mod_handler('escape', true, $_tmp, 'html') : smarty_modifier_escape($_tmp, 'html')); ?>
</a></h1>

				<ul class="action"> 
				
					<?php if ($this->_tpl_vars['oUserCurrent'] && ( $this->_tpl_vars['oUserCurrent']->getId() == $this->_tpl_vars['oBs']->getUserId() || $this->_tpl_vars['oUserCurrent']->isAdministrator() )): ?>
  						<li class="edit"><a href="<?php echo smarty_function_router(array('page' => 'bs'), $this);?>
edit/<?php echo $this->_tpl_vars['oBs']->getId(); ?>
/" title="<?php echo $this->_tpl_vars['aLang']['topic_edit']; ?>
"><?php echo $this->_tpl_vars['aLang']['topic_edit']; ?>
</a></li>
  					<?php endif; ?>
					<?php if ($this->_tpl_vars['oUserCurrent'] && $this->_tpl_vars['oUserCurrent']->isAdministrator()): ?>
							<li class="delete"><a href="<?php echo smarty_function_router(array('page' => 'bs'), $this);?>
delete/<?php echo $this->_tpl_vars['oBs']->getId(); ?>
/?security_ls_key=<?php echo $this->_tpl_vars['LIVESTREET_SECURITY_KEY']; ?>
" title="<?php echo $this->_tpl_vars['aLang']['topic_delete']; ?>
" onclick="return confirm('<?php echo $this->_tpl_vars['aLang']['bs_delete_confirm']; ?>
');"><?php echo $this->_tpl_vars['aLang']['topic_delete']; ?>
</a></li> 
					<?php endif; ?>
					
				</ul>				
				
			<div class="content">
				<?php echo ((is_array($_tmp=$this->_tpl_vars['oBs']->getText())) ? $this->_run_mod_handler('truncate', true, $_tmp, 800) : smarty_modifier_truncate($_tmp, 800)); ?>

				<br><br>( <a href="<?php echo $this->_tpl_vars['oBs']->getUrl(); ?>
" title="<?php echo $this->_tpl_vars['aLang']['topic_read_more']; ?>
"><?php echo $this->_tpl_vars['aLang']['qa_read_more']; ?>
</a> )
			</div>
							
			<ul class="voting  <?php if ($this->_tpl_vars['oVote'] || ( $this->_tpl_vars['oUserCurrent'] && $this->_tpl_vars['oBs']->getUserId() == $this->_tpl_vars['oUserCurrent']->getId() ) || strtotime ( $this->_tpl_vars['oBs']->getDateAdd() ) < time()-$this->_tpl_vars['oConfig']->GetValue('acl.vote.topic.limit_time')): ?><?php if ($this->_tpl_vars['oBs']->getRating() > 0): ?>positive<?php elseif ($this->_tpl_vars['oBs']->getRating() < 0): ?>negative<?php endif; ?><?php endif; ?> <?php if (! $this->_tpl_vars['oUserCurrent'] || $this->_tpl_vars['oBs']->getUserId() == $this->_tpl_vars['oUserCurrent']->getId() || strtotime ( $this->_tpl_vars['oBs']->getDateAdd() ) < time()-$this->_tpl_vars['oConfig']->GetValue('acl.vote.topic.limit_time')): ?>guest<?php endif; ?> <?php if ($this->_tpl_vars['oVote']): ?> voted <?php if ($this->_tpl_vars['oVote']->getDirection() > 0): ?>plus<?php elseif ($this->_tpl_vars['oVote']->getDirection() < 0): ?>minus<?php endif; ?><?php endif; ?>">
				<li class="plus"><a href="#" onclick="lsVote.vote(<?php echo $this->_tpl_vars['oBs']->getId(); ?>
,this,1,'bs'); return false;"></a></li>
				<li class="total" title="<?php echo $this->_tpl_vars['aLang']['topic_vote_count']; ?>
: <?php echo $this->_tpl_vars['oBs']->getCountVote(); ?>
"><?php if ($this->_tpl_vars['oVote'] || ( $this->_tpl_vars['oUserCurrent'] && $this->_tpl_vars['oBs']->getUserId() == $this->_tpl_vars['oUserCurrent']->getId() ) || strtotime ( $this->_tpl_vars['oBs']->getDateAdd() ) < time()-$this->_tpl_vars['oConfig']->GetValue('acl.vote.topic.limit_time')): ?> <?php if ($this->_tpl_vars['oBs']->getRating() > 0): ?>+<?php endif; ?><?php echo $this->_tpl_vars['oBs']->getRating(); ?>
 <?php else: ?> <a href="#" onclick="lsVote.vote(<?php echo $this->_tpl_vars['oBs']->getId(); ?>
,this,0,'bs'); return false;">&mdash;</a> <?php endif; ?></li>
				<li class="minus"><a href="#" onclick="lsVote.vote(<?php echo $this->_tpl_vars['oBs']->getId(); ?>
,this,-1,'bs'); return false;"></a></li>
				<li class="date"><?php echo smarty_function_date_format(array('date' => $this->_tpl_vars['oBs']->getDateAdd()), $this);?>
</li>
				<li class="author"><a href="<?php echo $this->_tpl_vars['oUser']->getUserWebPath(); ?>
"><?php echo $this->_tpl_vars['oUser']->getLogin(); ?>
</a></li>
					<li class="comments-total">
						<?php if ($this->_tpl_vars['oBs']->getCountComment() > 0): ?>
							<a href="<?php echo $this->_tpl_vars['oBs']->getUrl(); ?>
#comments" title="<?php echo $this->_tpl_vars['aLang']['topic_comment_read']; ?>
"><span class="green"><?php echo $this->_tpl_vars['oBs']->getCountComment(); ?>
</span></a>
						<?php endif; ?>
						<a href="<?php echo $this->_tpl_vars['oBs']->getUrl(); ?>
#comments" title="<?php echo $this->_tpl_vars['aLang']['qa_comment_add']; ?>
"><span class="red"><?php echo $this->_tpl_vars['aLang']['qa_comment_add']; ?>
</span></a>
					</li>				
			</ul>
			
			</div>
			<!-- /Bs -->
	<?php endforeach; endif; unset($_from); ?>	
	
	<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'paging.tpl', 'smarty_include_vars' => array('aPaging' => ($this->_tpl_vars['aPaging']))));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
	
<?php else: ?>
<?php echo $this->_tpl_vars['aLang']['blog_no_topic']; ?>

<?php endif; ?>