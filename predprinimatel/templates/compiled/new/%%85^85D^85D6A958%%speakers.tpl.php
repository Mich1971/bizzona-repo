<?php /* Smarty version 2.6.19, created on 2011-06-02 16:43:31
         compiled from actions/ActionTalk/speakers.tpl */ ?>
			<div class="block blogs">
				<div class="tl"><div class="tr"></div></div>
				<div class="cl"><div class="cr">
					
					<h1><?php echo $this->_tpl_vars['aLang']['talk_speaker_title']; ?>
</h1>
				<?php if ($this->_tpl_vars['oTalk']->getUserId() == $this->_tpl_vars['oUserCurrent']->getId() || $this->_tpl_vars['oUserCurrent']->isAdministrator()): ?>
				<?php echo '
						<script language="JavaScript" type="text/javascript">
						document.addEvent(\'domready\', function() {	
							new Autocompleter.Request.HTML(
								$(\'talk_speaker_add\'),
								 DIR_WEB_ROOT+\'/include/ajax/userAutocompleter.php?security_ls_key=\'+LIVESTREET_SECURITY_KEY, 
								 {
									\'indicatorClass\': \'autocompleter-loading\',
									\'minLength\': 1,
									\'selectMode\': \'pick\',
									\'multiple\': true
								}
							);
						});						
						
						function deleteFromTalk(element,idTalk) {
							element.getParent(\'li\').fade(0.7);							
							idTarget = element.get(\'id\').replace(\'speaker_item_\',\'\');
		
			                JsHttpRequest.query(
			                        \'POST \'+aRouter[\'talk\']+\'ajaxdeletetalkuser/\',                      
			                        { idTarget:idTarget,idTalk:idTalk, security_ls_key: LIVESTREET_SECURITY_KEY },
			                        function(result, errors) {     
			                            if (!result) {
							                msgErrorBox.alert(\'Error\',\'Please try again later\');
							                element.getParent().fade(1);           
							        	}    
							        	if (result.bStateError) {
							                msgErrorBox.alert(result.sMsgTitle,result.sMsg);
							                element.getParent().fade(1);
							        	} else {
							                element.getParent(\'li\').destroy();
							        	}                                 
			                        },
			                        true
			                ); 
										                
							return true;
						}
						function addListItem(sId,sLogin,sUserLink,sTalkId) {
							oUser=new Element(\'a\',
								{
									\'class\'  : \'user\',
									\'text\'   : sLogin,
									\'href\'   : sUserLink
								}
							);
							oLink=new Element(\'a\',
								{
									\'id\'    : \'speaker_item_\'+sId,
									\'href\'  : "#",
									\'class\' : \'delete\',
									\'events\': {
										\'click\': function() {
											deleteFromTalk(this,sTalkId); 
											return false;
										}
									}
								}
							);
							oItem=new Element(\'li\');
							$(\'speakerList\').adopt(oItem.adopt(oUser,oLink));
						}
						function addToTalk(idTalk) {
							sUsers=$(\'talk_speaker_add\').get(\'value\');
							if(sUsers.length<2) {
								msgErrorBox.alert(\'Error\',\'Пользователь не указан\');
								return false;
							}
							$(\'talk_speaker_add\').set(\'value\',\'\');
			                JsHttpRequest.query(
			                        \'POST \'+aRouter[\'talk\']+\'ajaxaddtalkuser/\',                      
			                        { users: sUsers, idTalk: idTalk, security_ls_key: LIVESTREET_SECURITY_KEY },
			                        function(result, errors) {     
			                            if (!result) {
							                msgErrorBox.alert(\'Error\',\'Please try again later\');         
							        	}    
							        	if (result.bStateError) {
							                msgErrorBox.alert(result.sMsgTitle,result.sMsg);
							        	} else {
							        		var aUsers = result.aUsers;
							        		aUsers.each(function(item,index) { 
							        			if(item.bStateError){
							        				msgErrorBox.alert(item.sMsgTitle, item.sMsg);
							        			} else {
							                		addListItem(item.sUserId,item.sUserLogin,item.sUserLink,idTalk);
							        			}
							        		});
							        	}                                 
			                        },
			                        true
			                ); 							
							return false;
						}
						</script>
					'; ?>


					<div class="block-content">
						<form onsubmit="addToTalk(<?php echo $this->_tpl_vars['oTalk']->getId(); ?>
); return false;">
							<p><label for="talk_speaker_add"><?php echo $this->_tpl_vars['aLang']['talk_speaker_add_label']; ?>
:</label><br />
							<input type="text" id="talk_speaker_add" name="add" value="" class="w100p" />
							</p>										
						</form>
					</div>
			<?php endif; ?>	
			<div class="block-content" id="speakerListBlock">
				<?php if ($this->_tpl_vars['oTalk']->getTalkUsers()): ?>
					<ul class="list" id="speakerList">
						<?php $_from = $this->_tpl_vars['oTalk']->getTalkUsers(); if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['users'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['users']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['oTalkUser']):
        $this->_foreach['users']['iteration']++;
?>
							<?php if ($this->_tpl_vars['oTalkUser']->getUserId() != $this->_tpl_vars['oUserCurrent']->getId()): ?>
							<?php $this->assign('oUser', $this->_tpl_vars['oTalkUser']->getUser()); ?>	
								<?php if ($this->_tpl_vars['oTalkUser']->getUserActive() != $this->_tpl_vars['TALK_USER_DELETE_BY_AUTHOR']): ?><li><a class="user <?php if ($this->_tpl_vars['oTalkUser']->getUserActive() != $this->_tpl_vars['TALK_USER_ACTIVE']): ?>inactive<?php endif; ?>" href="<?php echo $this->_tpl_vars['oUser']->getUserWebPath(); ?>
"><?php echo $this->_tpl_vars['oUser']->getLogin(); ?>
</a><?php if ($this->_tpl_vars['oTalkUser']->getUserActive() == $this->_tpl_vars['TALK_USER_ACTIVE'] && ( $this->_tpl_vars['oTalk']->getUserId() == $this->_tpl_vars['oUserCurrent']->getId() || $this->_tpl_vars['oUserCurrent']->isAdministrator() )): ?><a href="#" id="speaker_item_<?php echo $this->_tpl_vars['oTalkUser']->getUserId(); ?>
" onclick="deleteFromTalk(this,<?php echo $this->_tpl_vars['oTalk']->getId(); ?>
); return false;" class="delete"></a><?php endif; ?></li><?php endif; ?>						
							<?php endif; ?>
						<?php endforeach; endif; unset($_from); ?>
					</ul>
				<?php endif; ?>
			</div>
				<br />	
				</div></div>
				<div class="bl"><div class="br"></div></div>
			</div>