<?php /* Smarty version 2.6.19, created on 2011-01-30 11:53:50
         compiled from menu.bb.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'cfg', 'menu.bb.tpl', 4, false),)), $this); ?>

	<ul class="menu">
		<li>
			<a href="<?php echo smarty_function_cfg(array('name' => 'path.root.web'), $this);?>
/bb/"><?php echo $this->_tpl_vars['aLang']['bb']; ?>
</a>
		</li>	
		<?php if ($this->_tpl_vars['oUserCurrent']): ?>
		<li <?php if ($this->_tpl_vars['sMenuSubItemSelect'] == 'add'): ?>class="active"<?php endif; ?>>
			<a href="<?php echo smarty_function_cfg(array('name' => 'path.root.web'), $this);?>
/bb/add/"><?php echo $this->_tpl_vars['aLang']['bb_menu_add']; ?>
</a>
		</li>
		<li>
			<a href="<?php echo smarty_function_cfg(array('name' => 'path.root.web'), $this);?>
/mybb/<?php echo $this->_tpl_vars['oUserCurrent']->getLogin(); ?>
/"><?php echo $this->_tpl_vars['aLang']['bb_my']; ?>
</a>
		</li>		
		<li>
			<a href="<?php echo smarty_function_cfg(array('name' => 'path.root.web'), $this);?>
/mybb/<?php echo $this->_tpl_vars['oUserCurrent']->getLogin(); ?>
/comment"><?php echo $this->_tpl_vars['aLang']['bb_mycomment']; ?>
</a>
		</li>
		<li>
			<a href="<?php echo smarty_function_cfg(array('name' => 'path.root.web'), $this);?>
/bb/favourites/"><?php echo $this->_tpl_vars['aLang']['bb_fav']; ?>
</a>
		</li>		
		<?php endif; ?>
	</ul>