<?php /* Smarty version 2.6.19, created on 2011-02-08 23:15:43
         compiled from actions/ActionBlog/admin.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'router', 'actions/ActionBlog/admin.tpl', 5, false),)), $this); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'header.tpl', 'smarty_include_vars' => array('menu' => 'blog_edit','showWhiteBack' => true)));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>



			<h1><?php echo $this->_tpl_vars['aLang']['blog_admin']; ?>
: <a href="<?php echo smarty_function_router(array('page' => 'blog'), $this);?>
<?php echo $this->_tpl_vars['oBlogEdit']->getUrl(); ?>
/"><?php echo $this->_tpl_vars['oBlogEdit']->getTitle(); ?>
</a></h1>

		<?php if ($this->_tpl_vars['aBlogUsers']): ?>
			<form action="" method="POST" enctype="multipart/form-data">
				<input type="hidden" name="security_ls_key" value="<?php echo $this->_tpl_vars['LIVESTREET_SECURITY_KEY']; ?>
" />
				<table class="table-blog-users">
					<thead>
						<tr>
							<td></td>
							<td width="10%"><?php echo $this->_tpl_vars['aLang']['blog_admin_users_administrator']; ?>
</td>
							<td width="10%"><?php echo $this->_tpl_vars['aLang']['blog_admin_users_moderator']; ?>
</td>
							<td width="10%"><?php echo $this->_tpl_vars['aLang']['blog_admin_users_reader']; ?>
</td>
							<td width="10%"><?php echo $this->_tpl_vars['aLang']['blog_admin_users_bun']; ?>
</td>
						</tr>
					</thead>
					<tbody>
						<?php $_from = $this->_tpl_vars['aBlogUsers']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['oBlogUser']):
?>
						<?php $this->assign('oUser', $this->_tpl_vars['oBlogUser']->getUser()); ?>
						<tr>
							<td class="username"><a href="<?php echo smarty_function_router(array('page' => 'profile'), $this);?>
<?php echo $this->_tpl_vars['oUser']->getLogin(); ?>
/"><?php echo $this->_tpl_vars['oUser']->getLogin(); ?>
</a></td>
							<?php if ($this->_tpl_vars['oUser']->getId() == $this->_tpl_vars['oUserCurrent']->getId()): ?>
							<td colspan="3" align="center"><?php echo $this->_tpl_vars['aLang']['blog_admin_users_current_administrator']; ?>
</td>
							<?php else: ?>
								<td><input type="radio" name="user_rank[<?php echo $this->_tpl_vars['oUser']->getId(); ?>
]"  value="administrator" <?php if ($this->_tpl_vars['oBlogUser']->getIsAdministrator()): ?>checked<?php endif; ?>/></td>
								<td><input type="radio" name="user_rank[<?php echo $this->_tpl_vars['oUser']->getId(); ?>
]"  value="moderator" <?php if ($this->_tpl_vars['oBlogUser']->getIsModerator()): ?>checked<?php endif; ?>/></td>
								<td><input type="radio" name="user_rank[<?php echo $this->_tpl_vars['oUser']->getId(); ?>
]"  value="reader" <?php if ($this->_tpl_vars['oBlogUser']->getUserRole() == $this->_tpl_vars['BLOG_USER_ROLE_USER']): ?>checked<?php endif; ?>/></td>
								<td><input type="radio" name="user_rank[<?php echo $this->_tpl_vars['oUser']->getId(); ?>
]"  value="ban" <?php if ($this->_tpl_vars['oBlogUser']->getUserRole() == $this->_tpl_vars['BLOG_USER_ROLE_BAN']): ?>checked<?php endif; ?>/></td>						
							<?php endif; ?>
						</tr>
						<?php endforeach; endif; unset($_from); ?>						
					</tbody>
				</table>
								
				<input type="submit" name="submit_blog_admin" value="<?php echo $this->_tpl_vars['aLang']['blog_admin_users_submit']; ?>
">
			</form>
		<?php else: ?>
			<?php echo $this->_tpl_vars['aLang']['blog_admin_users_empty']; ?>
 
		<?php endif; ?>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'footer.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>