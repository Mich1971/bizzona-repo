<?php /* Smarty version 2.6.19, created on 2011-06-02 16:43:31
         compiled from notify/russian/notify.talk_new.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'router', 'notify/russian/notify.talk_new.tpl', 1, false),array('function', 'cfg', 'notify/russian/notify.talk_new.tpl', 8, false),array('modifier', 'escape', 'notify/russian/notify.talk_new.tpl', 2, false),)), $this); ?>
Вам пришло новое письмо от пользователя <a href="<?php echo $this->_tpl_vars['oUserFrom']->getUserWebPath(); ?>
"><?php echo $this->_tpl_vars['oUserFrom']->getLogin(); ?>
</a>, прочитать и ответить на него можно перейдя по <a href="<?php echo smarty_function_router(array('page' => 'talk'), $this);?>
read/<?php echo $this->_tpl_vars['oTalk']->getId(); ?>
/">этой ссылке</a><br>
Тема письма: <b><?php echo ((is_array($_tmp=$this->_tpl_vars['oTalk']->getTitle())) ? $this->_run_mod_handler('escape', true, $_tmp, 'html') : smarty_modifier_escape($_tmp, 'html')); ?>
</b><br>
<?php if ($this->_tpl_vars['oConfig']->GetValue('sys.mail.include_talk')): ?>
	Текст сообщения: <i><?php echo $this->_tpl_vars['oTalk']->getText(); ?>
</i>	<br>			
<?php endif; ?>
Не забудьте предварительно авторизоваться!
<br><br>
С уважением, администрация сайта <a href="<?php echo smarty_function_cfg(array('name' => 'path.root.web'), $this);?>
"><?php echo smarty_function_cfg(array('name' => 'view.name'), $this);?>
</a>