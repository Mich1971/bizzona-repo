<?php /* Smarty version 2.6.19, created on 2011-03-25 11:49:30
         compiled from qa.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'escape', 'qa.tpl', 6, false),array('modifier', 'nl2br', 'qa.tpl', 21, false),array('function', 'cfg', 'qa.tpl', 10, false),array('function', 'router', 'qa.tpl', 13, false),array('function', 'date_format', 'qa.tpl', 35, false),)), $this); ?>
<div class="topic">
<?php $this->assign('oUser', $this->_tpl_vars['oQa']->getUser()); ?>
<?php $this->assign('oVote', $this->_tpl_vars['oQa']->getVote()); ?> 

<div class="favorite <?php if ($this->_tpl_vars['oUserCurrent']): ?><?php if ($this->_tpl_vars['oQa']->getIsFavourite()): ?>active<?php endif; ?><?php else: ?>fav-guest<?php endif; ?>"><a href="#" onclick="lsFavourite.toggle(<?php echo $this->_tpl_vars['oQa']->getId(); ?>
,this,'qa'); return false;"></a></div>
<h1 class="title"><?php echo ((is_array($_tmp=$this->_tpl_vars['oQa']->getTitle())) ? $this->_run_mod_handler('escape', true, $_tmp, 'html') : smarty_modifier_escape($_tmp, 'html')); ?>
</h1>

				<ul class="action">					
					<?php if ($this->_tpl_vars['oUserCurrent'] && ( $this->_tpl_vars['oUserCurrent']->getId() == $this->_tpl_vars['oQa']->getUserId() || $this->_tpl_vars['oUserCurrent']->isAdministrator() )): ?>
  						<li class="edit"><a href="<?php echo smarty_function_cfg(array('name' => 'path.root.web'), $this);?>
/qa/edit/<?php echo $this->_tpl_vars['oQa']->getId(); ?>
/" title="<?php echo $this->_tpl_vars['aLang']['topic_edit']; ?>
"><?php echo $this->_tpl_vars['aLang']['topic_edit']; ?>
</a></li>
  					<?php endif; ?>
					<?php if ($this->_tpl_vars['oUserCurrent'] && $this->_tpl_vars['oUserCurrent']->isAdministrator()): ?>
  						<li class="delete"><a href="<?php echo smarty_function_router(array('page' => 'qa'), $this);?>
delete/<?php echo $this->_tpl_vars['oQa']->getId(); ?>
/?security_ls_key=<?php echo $this->_tpl_vars['LIVESTREET_SECURITY_KEY']; ?>
" title="<?php echo $this->_tpl_vars['aLang']['topic_delete']; ?>
" onclick="return confirm('<?php echo $this->_tpl_vars['aLang']['topic_delete_confirm']; ?>
');"><?php echo $this->_tpl_vars['aLang']['topic_delete']; ?>
</a></li>
  					<?php endif; ?>
				</ul>


<div class="content">
					
	<p>
		<?php echo ((is_array($_tmp=$this->_tpl_vars['oQa']->getDescription())) ? $this->_run_mod_handler('nl2br', true, $_tmp) : smarty_modifier_nl2br($_tmp)); ?>

	</p>
	<div class="line"></div>
</div>

				<ul class="tags">
					<?php $_from = $this->_tpl_vars['oQa']->getTagsArray(); if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['tags_list'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['tags_list']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['sTag']):
        $this->_foreach['tags_list']['iteration']++;
?>
						<li><a href="<?php echo smarty_function_router(array('page' => 'qatag'), $this);?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['sTag'])) ? $this->_run_mod_handler('escape', true, $_tmp, 'html') : smarty_modifier_escape($_tmp, 'html')); ?>
/"><?php echo ((is_array($_tmp=$this->_tpl_vars['sTag'])) ? $this->_run_mod_handler('escape', true, $_tmp, 'html') : smarty_modifier_escape($_tmp, 'html')); ?>
</a><?php if (! ($this->_foreach['tags_list']['iteration'] == $this->_foreach['tags_list']['total'])): ?>, <?php endif; ?></li>
					<?php endforeach; endif; unset($_from); ?>									
				</ul>	
				<ul class="voting <?php if ($this->_tpl_vars['oVote'] || ( $this->_tpl_vars['oUserCurrent'] && $this->_tpl_vars['oQa']->getUserId() == $this->_tpl_vars['oUserCurrent']->getId() ) || strtotime ( $this->_tpl_vars['oQa']->getDateAdd() ) < time()-$this->_tpl_vars['oConfig']->GetValue('acl.vote.topic.limit_time')): ?><?php if ($this->_tpl_vars['oQa']->getRating() > 0): ?>positive<?php elseif ($this->_tpl_vars['oQa']->getRating() < 0): ?>negative<?php endif; ?><?php endif; ?> <?php if (! $this->_tpl_vars['oUserCurrent'] || $this->_tpl_vars['oQa']->getUserId() == $this->_tpl_vars['oUserCurrent']->getId() || strtotime ( $this->_tpl_vars['oQa']->getDateAdd() ) < time()-$this->_tpl_vars['oConfig']->GetValue('acl.vote.topic.limit_time')): ?>guest<?php endif; ?> <?php if ($this->_tpl_vars['oVote']): ?> voted <?php if ($this->_tpl_vars['oVote']->getDirection() > 0): ?>plus<?php elseif ($this->_tpl_vars['oVote']->getDirection() < 0): ?>minus<?php endif; ?><?php endif; ?>">
					<li class="plus"><a href="#" onclick="lsVote.vote(<?php echo $this->_tpl_vars['oQa']->getId(); ?>
,this,1,'qa'); return false;"></a></li>
					<li class="total" title="<?php echo $this->_tpl_vars['aLang']['topic_vote_count']; ?>
: <?php echo $this->_tpl_vars['oQa']->getCountVote(); ?>
"><?php if ($this->_tpl_vars['oVote'] || ( $this->_tpl_vars['oUserCurrent'] && $this->_tpl_vars['oQa']->getUserId() == $this->_tpl_vars['oUserCurrent']->getId() ) || strtotime ( $this->_tpl_vars['oQa']->getDateAdd() ) < time()-$this->_tpl_vars['oConfig']->GetValue('acl.vote.topic.limit_time')): ?> <?php if ($this->_tpl_vars['oQa']->getRating() > 0): ?>+<?php endif; ?><?php echo $this->_tpl_vars['oQa']->getRating(); ?>
 <?php else: ?> <a href="#" onclick="lsVote.vote(<?php echo $this->_tpl_vars['oQa']->getId(); ?>
,this,0,'qa'); return false;">&mdash;</a> <?php endif; ?></li>
					<li class="minus"><a href="#" onclick="lsVote.vote(<?php echo $this->_tpl_vars['oQa']->getId(); ?>
,this,-1,'qa'); return false;"></a></li>
					<li class="date"><?php echo smarty_function_date_format(array('date' => $this->_tpl_vars['oQa']->getDateAdd()), $this);?>
</li>
					<li class="author"><a href="<?php echo $this->_tpl_vars['oUser']->getUserWebPath(); ?>
"><?php echo $this->_tpl_vars['oUser']->getLogin(); ?>
</a></li>
				</ul>	

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'social.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
</div>