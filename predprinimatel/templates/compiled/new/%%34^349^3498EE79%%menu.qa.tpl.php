<?php /* Smarty version 2.6.19, created on 2011-01-29 18:56:02
         compiled from menu.qa.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'cfg', 'menu.qa.tpl', 3, false),)), $this); ?>
<ul class="menu">
	<li>
		<a href="<?php echo smarty_function_cfg(array('name' => 'path.root.web'), $this);?>
/qa/"><?php echo $this->_tpl_vars['aLang']['qa_all']; ?>
</a>
	</li>	
	<?php if ($this->_tpl_vars['oUserCurrent']): ?>
	<li <?php if ($this->_tpl_vars['sMenuSubItemSelect'] == 'add'): ?>class="active"<?php endif; ?>>
		<a href="<?php echo smarty_function_cfg(array('name' => 'path.root.web'), $this);?>
/qa/add/"><?php echo $this->_tpl_vars['aLang']['qa_menu_add']; ?>
</a>
	</li>
	<li>
		<a href="<?php echo smarty_function_cfg(array('name' => 'path.root.web'), $this);?>
/myqa/<?php echo $this->_tpl_vars['oUserCurrent']->getLogin(); ?>
/"><?php echo $this->_tpl_vars['aLang']['qa_my']; ?>
</a>
	</li>
	<li>
		<a href="<?php echo smarty_function_cfg(array('name' => 'path.root.web'), $this);?>
/myqa/<?php echo $this->_tpl_vars['oUserCurrent']->getLogin(); ?>
/answer"><?php echo $this->_tpl_vars['aLang']['qa_myanswer']; ?>
</a>
	</li>		
	<li>
		<a href="<?php echo smarty_function_cfg(array('name' => 'path.root.web'), $this);?>
/qa/favourites/"><?php echo $this->_tpl_vars['aLang']['qa_fav']; ?>
</a>
	</li>	
	<?php endif; ?>
	
</ul>

