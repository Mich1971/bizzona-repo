<?php /* Smarty version 2.6.19, created on 2011-06-02 16:43:31
         compiled from actions/ActionTalk/read.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'escape', 'actions/ActionTalk/read.tpl', 7, false),array('function', 'router', 'actions/ActionTalk/read.tpl', 9, false),array('function', 'date_format', 'actions/ActionTalk/read.tpl', 16, false),)), $this); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'header.tpl', 'smarty_include_vars' => array('menu' => 'talk','showUpdateButton' => true)));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

			<?php $this->assign('oUser', $this->_tpl_vars['oTalk']->getUser()); ?>
			
			<div class="topic talk">
				<div class="favorite <?php if ($this->_tpl_vars['oTalk']->getIsFavourite()): ?>active<?php else: ?>guest<?php endif; ?>"><a href="#" onclick="lsFavourite.toggle(<?php echo $this->_tpl_vars['oTalk']->getId(); ?>
,this,'talk'); return false;"></a></div>			
				<h1 class="title"><?php echo ((is_array($_tmp=$this->_tpl_vars['oTalk']->getTitle())) ? $this->_run_mod_handler('escape', true, $_tmp, 'html') : smarty_modifier_escape($_tmp, 'html')); ?>
</h1>				
				<ul class="action">
					<li><a href="<?php echo smarty_function_router(array('page' => 'talk'), $this);?>
"><?php echo $this->_tpl_vars['aLang']['talk_inbox']; ?>
</a></li>
					<li class="delete"><a href="<?php echo smarty_function_router(array('page' => 'talk'), $this);?>
delete/<?php echo $this->_tpl_vars['oTalk']->getId(); ?>
/?security_ls_key=<?php echo $this->_tpl_vars['LIVESTREET_SECURITY_KEY']; ?>
"  onclick="return confirm('<?php echo $this->_tpl_vars['aLang']['talk_inbox_delete_confirm']; ?>
');"><?php echo $this->_tpl_vars['aLang']['talk_inbox_delete']; ?>
</a></li>
				</ul>				
				<div class="content">
					<?php echo $this->_tpl_vars['oTalk']->getText(); ?>
				
				</div>				
				<ul class="voting">
					<li class="date"><?php echo smarty_function_date_format(array('date' => $this->_tpl_vars['oTalk']->getDate()), $this);?>
</li>
					<li class="author"><a href="<?php echo $this->_tpl_vars['oUser']->getUserWebPath(); ?>
"><?php echo $this->_tpl_vars['oUser']->getLogin(); ?>
</a></li>
				</ul>
			</div>
			
			<?php $this->assign('oTalkUser', $this->_tpl_vars['oTalk']->getTalkUser()); ?>
			
			<?php if (! $this->_tpl_vars['bNoComments']): ?>
			<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'comment_tree.tpl', 'smarty_include_vars' => array('iTargetId' => $this->_tpl_vars['oTalk']->getId(),'sTargetType' => 'talk','iCountComment' => $this->_tpl_vars['oTalk']->getCountComment(),'sDateReadLast' => $this->_tpl_vars['oTalkUser']->getDateLast(),'sNoticeCommentAdd' => $this->_tpl_vars['aLang']['topic_comment_add'],'bNoCommentFavourites' => true)));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
			<?php endif; ?>
			
			
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'footer.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>