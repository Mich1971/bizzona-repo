<?php /* Smarty version 2.6.19, created on 2011-06-11 19:12:14
         compiled from actions/ActionSettings/invite.tpl */ ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'header.tpl', 'smarty_include_vars' => array('menu' => 'settings','showWhiteBack' => true)));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

			<h1><?php echo $this->_tpl_vars['aLang']['settings_invite']; ?>
</h1>
			<form action="" method="POST" enctype="multipart/form-data">
				<input type="hidden" name="security_ls_key" value="<?php echo $this->_tpl_vars['LIVESTREET_SECURITY_KEY']; ?>
" /> 
				
				<p>
					<?php echo $this->_tpl_vars['aLang']['settings_invite_available']; ?>
: <strong><?php if ($this->_tpl_vars['oUserCurrent']->isAdministrator()): ?><?php echo $this->_tpl_vars['aLang']['settings_invite_many']; ?>
<?php else: ?><?php echo $this->_tpl_vars['iCountInviteAvailable']; ?>
<?php endif; ?></strong><br />
					<?php echo $this->_tpl_vars['aLang']['settings_invite_used']; ?>
: <strong><?php echo $this->_tpl_vars['iCountInviteUsed']; ?>
</strong>
				</p>			
				<p>
					<label for="invite_mail"><?php echo $this->_tpl_vars['aLang']['settings_invite_mail']; ?>
:</label><br />
					<input type="text" class="w300" name="invite_mail" id="invite_mail"/><br />
					<span class="form_note"><?php echo $this->_tpl_vars['aLang']['settings_invite_mail_notice']; ?>
</span>
				</p>				
				<input type="submit" value="<?php echo $this->_tpl_vars['aLang']['settings_invite_submit']; ?>
" name="submit_invite" />
			</form>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'footer.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>