<?php

class ActionBi extends Action {

	protected $sMenuHeadItemSelect='bi';

	protected $sMenuItemSelect='bi';

	protected $sMenuSubItemSelect='add';

	protected $oUserCurrent=null;
	
	public function Init() {

		//if (!$this->User_IsAuthorization()) {
		//	return parent::EventNotFound();
		//}
		
		$this->oUserCurrent=$this->User_GetUserCurrent();
		$this->SetDefaultEvent('index');
		$this->Viewer_AddHtmlTitle($this->Lang_Get('bi_title'));
		
		Router::SetIsShowStats(false);
	}

	protected function RegisterEvent() {

		$this->AddEventPreg('/^(\d+)\.html$/i','/^$/i','EventShowBi');
		$this->AddEventPreg('/^(page(\d+))?$/i','EventIndex');		
		
		$this->AddEvent('index','EventIndex');
		$this->AddEvent('add','EventAdd');			
		$this->AddEvent('edit','EventEdit');
		$this->AddEvent('ajaxaddcomment','AjaxAddComment');
		$this->AddEvent('delete','EventDelete');
		$this->AddEvent('favourites','EventFavourites');		
	}

	protected function EventFavourites() {				
		
		$iPage=$this->GetEventMatch(2) ? $this->GetEventMatch(2) : 1;

		$aResult=$this->Bi_GetBisFavouriteByUserId(
			$this->oUserCurrent->getId(),
			$iPage,Config::Get('module.qa.per_page')
		);	
		
		$aBis=$aResult['collection'];		 
		
		$aPaging=$this->Viewer_MakePaging($aResult['count'],$iPage,Config::Get('module.qa.per_page'),4,Router::GetPath('bi'));
		
		$this->Viewer_Assign('aPaging',$aPaging);
		$this->Viewer_Assign('aBis',$aBis);
		
		$this->SetTemplateAction('index');
		
	}	
	
	protected function EventDelete() {
		$this->Security_ValidateSendForm();

		$sBiId=$this->GetParam(0);
		if (!($oBi=$this->Bi_GetBiById($sBiId))) {
			return parent::EventNotFound();
		}

		if (!$this->ACL_IsAllowDeleteBi($oBi,$this->oUserCurrent)) {
			return parent::EventNotFound();
		}

		$this->Hook_Run('bi_delete_before', array('oBi'=>$oBi));
		$this->Bi_DeleteBi($oBi);
		$this->Hook_Run('bi_delete_after', array('oBi'=>$oBi));

		Router::Location(Router::GetPath('bi'));
	}	
	
	protected function AjaxAddComment() {
		$this->Viewer_SetResponseAjax();
		$this->SubmitComment();
	}	
	
	protected function SubmitComment() {
		
		$sText=$this->Text_Parser(getRequest('comment_text'));
		
		if (!($oBi=$this->Bi_GetBiById(getRequest('cmt_target_id')))) {
			$this->Message_AddErrorSingle($this->Lang_Get('system_error'),$this->Lang_Get('error'));
			return;
		}		
		
		if (!func_check($sText,'text',2,10000)) {			
			$this->Message_AddErrorSingle($this->Lang_Get('qa_comment_add_text_error'),$this->Lang_Get('error'));
			return;
		}		
		$sParentId=(int)getRequest('reply');
		if (!func_check($sParentId,'id')) {			
			$this->Message_AddErrorSingle($this->Lang_Get('system_error'),$this->Lang_Get('error'));
			return;
		}
		$oCommentParent=null;
		if ($sParentId!=0) {
			if (!($oCommentParent=$this->Comment_GetCommentById($sParentId))) {				
				$this->Message_AddErrorSingle($this->Lang_Get('system_error'),$this->Lang_Get('error'));
				return;
			}
			if ($oCommentParent->getTargetId()!=$oTopic->getId()) {
				$this->Message_AddErrorSingle($this->Lang_Get('system_error'),$this->Lang_Get('error'));
				return;
			}
		} else {
			$sParentId=null;
		}		

		$oCommentNew=Engine::GetEntity('Comment');
		$oCommentNew->setTargetId($oBi->getId());
		$oCommentNew->setTargetType('bi');
		$oCommentNew->setUserId($this->oUserCurrent->getId());		
		$oCommentNew->setText($sText);
		$oCommentNew->setDate(date("Y-m-d H:i:s"));
		$oCommentNew->setUserIp(func_getIp());
		$oCommentNew->setPid($sParentId);
		$oCommentNew->setTextHash(md5($sText));
			
		$this->Hook_Run('comment_add_before', array('oCommentNew'=>$oCommentNew,'oCommentParent'=>$oCommentParent,'oBi'=>$oBi));
		if ($this->Comment_AddComment($oCommentNew)) {
			
			$this->Hook_Run('comment_add_after', array('oCommentNew'=>$oCommentNew,'oCommentParent'=>$oCommentParent,'oBi'=>$oBi));
			
			$this->Viewer_AssignAjax('sCommentId',$oCommentNew->getId());

			$oCommentOnline=Engine::GetEntity('Comment_CommentOnline');
			$oCommentOnline->setTargetId($oCommentNew->getTargetId());
			$oCommentOnline->setTargetType($oCommentNew->getTargetType());
			$oCommentOnline->setTargetParentId($oCommentNew->getTargetParentId());
			$oCommentOnline->setCommentId($oCommentNew->getId());
				
			$this->Comment_AddCommentOnline($oCommentOnline);
			
			$this->oUserCurrent->setDateCommentLast(date("Y-m-d H:i:s"));
			$this->User_Update($this->oUserCurrent);
			$oUserBi=$oBi->getUser();
			
			
			if ($oCommentNew->getUserId()!=$oUserBi->getId()) {
				$this->Notify_SendCommentNewToAuthorBi($oUserBi,$oBi,$oCommentNew,$this->oUserCurrent);
			}
			
			
			if ($oCommentParent and $oCommentParent->getUserId()!=$oBi->getUserId() and $oCommentNew->getUserId()!=$oCommentParent->getUserId()) {
				$oUserAuthorComment=$oCommentParent->getUser();
				
				$this->Notify_SendCommentReplyToAuthorBiParentComment($oUserAuthorComment,$oBi,$oCommentNew,$this->oUserCurrent);
			}
			
			
		} else {
			
			$this->Message_AddErrorSingle($this->Lang_Get('system_error'),$this->Lang_Get('error'));
			
		}		
	}	
	
	protected function EventShowBi() {
		
		$sBiUrl='';
		$sBiUrl=$this->sCurrentEvent;
		
		$iBiId=$this->GetEventMatch(1);
		
		if (!($oBi=$this->Bi_GetBiById($iBiId))) {
			return parent::EventNotFound();
		}		
		
		if (isset($_REQUEST['submit_comment'])) {
			$this->SubmitComment();
		}		
 		 
		$this->Hook_Run('bi_show',array("oBi"=>$oBi));		
		
		$sTextSeo=preg_replace("/<.*>/Ui",' ',$oBi->getText());
		$this->Viewer_SetHtmlDescription(func_text_words($sTextSeo,20));
		$this->Viewer_SetHtmlKeywords(func_text_words($sTextSeo,20));		
		
		$aReturn=$this->Comment_GetCommentsByTargetId($oBi->getId(),'bi');
		$iMaxIdComment=$aReturn['iMaxIdComment'];
		$aComments=$aReturn['comments'];		
		
		$this->Viewer_Assign('aComments',$aComments);
		$this->Viewer_Assign('iMaxIdComment',$iMaxIdComment);
		
		
		if ($this->oUserCurrent) {
			
			$oBiRead=Engine::GetEntity('Bi_BiRead');
			$oBiRead->setBiId($oBi->getId());
			$oBiRead->setUserId($this->oUserCurrent->getId());
			$oBiRead->setCommentCountLast($oBi->getCountComment());
			$oBiRead->setCommentIdLast($iMaxIdComment);
			$oBiRead->setDateRead(date("Y-m-d H:i:s"));
			$this->Bi_SetBiRead($oBiRead);
		}		
		
		
		$this->Viewer_AddHtmlTitle($oBi->getTitle());		
		$this->Viewer_Assign('oBi',$oBi);
		
		$this->SetTemplateAction('bi');
			
	}	
	
	
	protected function EventIndex() {
		
		$this->sMenuSubItemSelect='add';

		$iPage=$this->GetEventMatch(2) ? $this->GetEventMatch(2) : 1;

		$aResult=$this->Bi_GetBisGood($iPage,Config::Get('module.topic.per_page'));			
		$aBis=$aResult['collection'];	

		$aPaging=$this->Viewer_MakePaging($aResult['count'],$iPage,Config::Get('module.topic.per_page'),4,Router::GetPath('bi'));

		$this->Viewer_Assign('aBis',$aBis);
		$this->Viewer_Assign('aPaging',$aPaging);		

		$this->SetTemplateAction('index');
	}	
	
	protected function EventEdit() {

		if (!$this->User_IsAuthorization()) {
			return parent::EventNotFound();
		}		
		
		$this->sMenuSubItemSelect='';
		$this->sMenuItemSelect='bi';

		$sBiId=$this->GetParam(0);
		
		if (!($oBi=$this->Bi_GetBiById($sBiId))) {
			return parent::EventNotFound();
		}
		
		if (!$this->ACL_IsAllowEditBi($oBi,$this->oUserCurrent)) {
			return parent::EventNotFound();
		}	

		
		$this->Hook_Run('bi_edit_show',array('oBi'=>$oBi));

		$this->Viewer_AddHtmlTitle($this->Lang_Get('topic_topic_edit'));

		$this->SetTemplateAction('add');		

		
		if (isset($_REQUEST['submit_bi_publish'])) {
			return $this->SubmitEdit($oBi);
		} else {
			$_REQUEST['bi_title']=$oBi->getTitle();
			$_REQUEST['bi_text']=$oBi->getTextSource();
			$_REQUEST['bi_tags']=$oBi->getTags();
			$_REQUEST['bi_id']=$oBi->getId();
		}		
		
	}

	protected function EventAdd() {
		
		if (!$this->User_IsAuthorization()) {
			return parent::EventNotFound();
		}		
		
		$this->sMenuSubItemSelect='add';	
		$this->Hook_Run('bi_add_show');
		$this->Viewer_AddHtmlTitle($this->Lang_Get('bi_bi_create'));
		return $this->SubmitAdd();		
		
	}	

	protected function EventShowBis() {		
	}

	protected function checkBiFields() {
		
		$this->Security_ValidateSendForm();
		
		$bOk=true;

		if (!func_check(getRequest('bi_title',null,'post'),'text',2,200)) {
			$this->Message_AddError($this->Lang_Get('bi_create_title_error'),$this->Lang_Get('error'));
			$bOk=false;
		}

		
		if (!func_check(getRequest('bi_text',null,'post'),'text',2,Config::Get('module.topic.max_length'))) {
			$this->Message_AddError($this->Lang_Get('bi_create_text_error'),$this->Lang_Get('error'));
			$bOk=false;
		}

		if (!func_check(getRequest('bi_tags',null,'post'),'text',2,500)) {
			$this->Message_AddError($this->Lang_Get('bi_create_tags_error'),$this->Lang_Get('error'));
			$bOk=false;
		}		
		
		$this->Hook_Run('check_bi_fields', array('bOk'=>&$bOk));
		
		return $bOk;
		
	}	
	
	protected function SubmitAdd() {
		
		if (!isPost('submit_bi_publish')) {
			return false;
		}	

		if (!$this->checkBiFields()) {
			return false;	
		}	
			

		$oBi=Engine::GetEntity('Bi');
		
		$oBi->setUserId($this->oUserCurrent->getId());
		$oBi->setTitle(getRequest('bi_title'));
		$oBi->setTextHash(md5(getRequest('bi_text')));

		list($sTextShort,$sTextNew,$sTextCut) = $this->Text_Cut(getRequest('bi_text'));
		
		$oBi->setCutText($sTextCut);
		$oBi->setText($this->Text_Parser($sTextNew));
		$oBi->setTextShort($this->Text_Parser($sTextShort));
		
		$oBi->setTextSource(getRequest('bi_text'));
		$oBi->setTags(getRequest('bi_tags'));
		$oBi->setDateAdd(date("Y-m-d H:i:s"));
		$oBi->setUserIp(func_getIp());
		$oBi->setPublishIndex(0);
		$oBi->setForbidComment(0);
		$oBi->setExtra("-");

		if (isset($_REQUEST['submit_bi_publish'])) {
			$oBi->setPublish(1);
			$oBi->setPublishDraft(1);
		} 
				
		$this->Hook_Run('bi_add_before', array('oBi'=>$oBi));

		if ($this->Bi_AddBi($oBi)) {
			$this->Hook_Run('bi_add_after', array('oBi'=>$oBi));
			Router::Location($oBi->getUrl());
		} else {
			$this->Message_AddErrorSingle($this->Lang_Get('system_error'));
			return Router::Action('error');
		}		
	}

	protected function SubmitEdit($oBi) {			
		
		if (!$this->checkBiFields()) {
			return false;	
		}	
		
		$oBi->setTitle(getRequest('bi_title'));
		$oBi->setTextHash(md5(getRequest('bi_text')));

		list($sTextShort,$sTextNew,$sTextCut) = $this->Text_Cut(getRequest('bi_text'));

		$oBi->setCutText($sTextCut);
		$oBi->setText($this->Text_Parser($sTextNew));
		$oBi->setTextShort($this->Text_Parser($sTextShort));
		
		$oBi->setTextSource(getRequest('bi_text'));
		$oBi->setTags(getRequest('bi_tags'));
		$oBi->setUserIp(func_getIp());

		$bSendNotify=false;

		$this->Hook_Run('bi_edit_before', array('oBi'=>$oBi));

		
		if ($this->Bi_UpdateBi($oBi)) {
			$this->Hook_Run('bi_edit_after', array('oBi'=>$oBi,'bSendNotify'=>&$bSendNotify));

			Router::Location($oBi->getUrl());
		} else {
			$this->Message_AddErrorSingle($this->Lang_Get('system_error'));
			return Router::Action('error');
		}
		
		
	}

	public function EventShutdown() {
		$this->Viewer_Assign('sMenuHeadItemSelect',$this->sMenuHeadItemSelect);
		//$this->Viewer_Assign('sMenuItemSelect',$this->sMenuItemSelect);
		$this->Viewer_Assign('sMenuSubItemSelect',$this->sMenuSubItemSelect);
	}
}
?>