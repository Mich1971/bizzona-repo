<?php

class ActionMyBi extends Action {

	protected $sUserLogin=null;

	protected $oUserProfile=null;
	
	protected $sMenuSubItemSelect='mybi';
	
	public function Init() {
		
		Router::SetIsShowStats(false);
	}
	
	protected function RegisterEvent() {	
		
		$this->AddEventPreg('/^[\w\-\_]+$/i','/^(page(\d+))?$/i','EventIndex');
		$this->AddEventPreg('/^[\w\-\_]+$/i','/^comment$/i','/^(page(\d+))?$/i','EventComments');
		
		/*
		$this->AddEventPreg('/^[\w\-\_]+$/i','/^(page(\d+))?$/i','EventTopics');						
		$this->AddEventPreg('/^[\w\-\_]+$/i','/^comment$/i','/^(page(\d+))?$/i','EventComments');						
		*/
	}
		
	
	protected function EventIndex() {
		
		$sUserLogin=$this->sCurrentEvent;					
		if (!($this->oUserProfile=$this->User_GetUserByLogin($sUserLogin))) {			
			return parent::EventNotFound();
		}
		$iPage=$this->GetParamEventMatch(0,2) ? $this->GetParamEventMatch(0,2) : 1;	
		
		$aResult=$this->Bi_GetBisPersonalByUser($this->oUserProfile->getId(),1,$iPage,Config::Get('module.topic.per_page'));	
		$aBis=$aResult['collection'];
		
		//var_dump($aBis);
		
		$aPaging=$this->Viewer_MakePaging($aResult['count'],$iPage,Config::Get('module.topic.per_page'),4,Router::GetPath('mybi').$this->oUserProfile->getLogin());		
		$this->Viewer_Assign('aPaging',$aPaging);			
		$this->Viewer_Assign('aBis',$aBis);
		
		$this->SetTemplateAction('index');
	}
	
	protected function EventComments() {
		
		$sUserLogin=$this->sCurrentEvent;					
		if (!($this->oUserProfile=$this->User_GetUserByLogin($sUserLogin))) {			
			return parent::EventNotFound();
		}
		$iPage=$this->GetParamEventMatch(1,2) ? $this->GetParamEventMatch(1,2) : 1;
		$aResult=$this->Comment_GetCommentsByUserId($this->oUserProfile->getId(),'bi',$iPage,Config::Get('module.comment.per_page'));	
		$aComments=$aResult['collection'];		
		$aPaging=$this->Viewer_MakePaging($aResult['count'],$iPage,Config::Get('module.comment.per_page'),4,Router::GetPath('mybi').$this->oUserProfile->getLogin().'/comment');		
		$this->Viewer_Assign('aPaging',$aPaging);			
		$this->Viewer_Assign('aComments',$aComments);	
		$this->Viewer_AddHtmlTitle($this->Lang_Get('user_menu_publication').' '.$this->oUserProfile->getLogin());
		$this->Viewer_AddHtmlTitle($this->Lang_Get('user_menu_publication_comment'));
		$this->SetTemplateAction('comment');		
		
	}	

	public function EventShutdown() {
		/*
		if (!$this->oUserProfile)	 {
			return ;
		}
		$iCountTopicUser=$this->Topic_GetCountTopicsPersonalByUser($this->oUserProfile->getId(),1);
		$iCountCommentUser=$this->Comment_GetCountCommentsByUserId($this->oUserProfile->getId(),'topic');
		$this->Viewer_Assign('oUserProfile',$this->oUserProfile);		
		$this->Viewer_Assign('iCountTopicUser',$iCountTopicUser);		
		$this->Viewer_Assign('iCountCommentUser',$iCountCommentUser);
		*/
		$this->Viewer_Assign('sMenuSubItemSelect',$this->sMenuSubItemSelect);
	}
}
?>