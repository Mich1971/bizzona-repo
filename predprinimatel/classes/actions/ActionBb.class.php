<?php

class ActionBb extends Action {

	protected $sMenuHeadItemSelect='bb';

	protected $sMenuItemSelect='bb';

	protected $sMenuSubItemSelect='add';

	protected $oUserCurrent=null;
	
	public function Init() {
		
		$this->oUserCurrent=$this->User_GetUserCurrent();
		$this->SetDefaultEvent('index');		
		$this->Viewer_AddHtmlTitle($this->Lang_Get('bb_title'));
		
		Router::SetIsShowStats(false);
	}

	protected function RegisterEvent() {
		
		$this->AddEventPreg('/^(\d+)\.html$/i','/^$/i','EventShowBb');
		$this->AddEventPreg('/^(page(\d+))?$/i','EventIndex');		
		
		$this->AddEvent('index','EventIndex');
		$this->AddEvent('add','EventAdd');			
		$this->AddEvent('edit','EventEdit');
		$this->AddEvent('delete','EventDelete');
		
		$this->AddEvent('ajaxaddcomment','AjaxAddComment');
		$this->AddEvent('favourites','EventFavourites');
	}
		
	protected function EventFavourites() {				
		
		$iPage=$this->GetEventMatch(2) ? $this->GetEventMatch(2) : 1;

		$aResult=$this->Bb_GetBbsFavouriteByUserId(
			$this->oUserCurrent->getId(),
			$iPage,Config::Get('module.qa.per_page')
		);	
		
		$aBbs=$aResult['collection'];		 
		
		$aPaging=$this->Viewer_MakePaging($aResult['count'],$iPage,Config::Get('module.qa.per_page'),4,Router::GetPath('bb'));
		
		$this->Viewer_Assign('aPaging',$aPaging);
		$this->Viewer_Assign('aBbs',$aBbs);
		
		$this->SetTemplateAction('index');
		
	}	
	
	protected function AjaxAddComment() {
		$this->Viewer_SetResponseAjax();
		$this->SubmitComment();
	}	
	
	protected function SubmitComment() {

		if (!$this->User_IsAuthorization()) {
			$this->Message_AddErrorSingle($this->Lang_Get('need_authorization'),$this->Lang_Get('error'));
			return;
		}

		if (!($oBb=$this->Bb_GetBbById(getRequest('cmt_target_id')))) {
			$this->Message_AddErrorSingle($this->Lang_Get('system_error'),$this->Lang_Get('error'));
			return;
		}
		
		if (!$this->ACL_CanPostComment($this->oUserCurrent) and !$this->oUserCurrent->isAdministrator()) {			
			$this->Message_AddErrorSingle($this->Lang_Get('topic_comment_acl'),$this->Lang_Get('error'));
			return;
		}

		$sText=$this->Text_Parser(getRequest('comment_text'));
		if (!func_check($sText,'text',2,10000)) {			
			$this->Message_AddErrorSingle($this->Lang_Get('topic_comment_add_text_error'),$this->Lang_Get('error'));
			return;
		}

		$sParentId=(int)getRequest('reply');
		if (!func_check($sParentId,'id')) {			
			$this->Message_AddErrorSingle($this->Lang_Get('system_error'),$this->Lang_Get('error'));
			return;
		}
		$oCommentParent=null;
		if ($sParentId!=0) {
			if (!($oCommentParent=$this->Comment_GetCommentById($sParentId))) {				
				$this->Message_AddErrorSingle($this->Lang_Get('system_error'),$this->Lang_Get('error'));
				return;
			}
			if ($oCommentParent->getTargetId()!=$oTopic->getId()) {
				$this->Message_AddErrorSingle($this->Lang_Get('system_error'),$this->Lang_Get('error'));
				return;
			}
		} else {
			$sParentId=null;
		}

		$oCommentNew=Engine::GetEntity('Comment');
		$oCommentNew->setTargetId($oBb->getId());
		$oCommentNew->setTargetType('bb');
		$oCommentNew->setUserId($this->oUserCurrent->getId());		
		$oCommentNew->setText($sText);
		$oCommentNew->setDate(date("Y-m-d H:i:s"));
		$oCommentNew->setUserIp(func_getIp());
		$oCommentNew->setPid($sParentId);
		$oCommentNew->setTextHash(md5($sText));
			
		if ($this->Comment_AddComment($oCommentNew)) {
			
			$this->Viewer_AssignAjax('sCommentId',$oCommentNew->getId());

			$oCommentOnline=Engine::GetEntity('Comment_CommentOnline');
			$oCommentOnline->setTargetId($oCommentNew->getTargetId());
			$oCommentOnline->setTargetType($oCommentNew->getTargetType());
			$oCommentOnline->setTargetParentId($oCommentNew->getTargetParentId());
			$oCommentOnline->setCommentId($oCommentNew->getId());
				
			$this->Comment_AddCommentOnline($oCommentOnline);
			
			$this->oUserCurrent->setDateCommentLast(date("Y-m-d H:i:s"));
			$this->User_Update($this->oUserCurrent);
			$oUserBb=$oBb->getUser();
			
			
			if ($oCommentNew->getUserId()!=$oUserBb->getId()) {
				$this->Notify_SendCommentNewToAuthorBb($oUserBb,$oBb,$oCommentNew,$this->oUserCurrent);
			}
			
			if ($oCommentParent and $oCommentParent->getUserId()!=$oBb->getUserId() and $oCommentNew->getUserId()!=$oCommentParent->getUserId()) {
				$oUserAuthorComment=$oCommentParent->getUser();
				
				$this->Notify_SendCommentReplyToAuthorBbParentComment($oUserAuthorComment,$oBb,$oCommentNew,$this->oUserCurrent);
			}			
			
		} else {
			$this->Message_AddErrorSingle($this->Lang_Get('system_error'),$this->Lang_Get('error'));
		}
	}	
	
	protected function EventShowBb() {

		$sBbUrl='';
		$sBbUrl=$this->sCurrentEvent;
		
		$iBbId=$this->GetEventMatch(1);
		
		if (!($oBb=$this->Bb_GetBbById($iBbId))) {
			return parent::EventNotFound();
		}		
		
		if (isset($_REQUEST['submit_comment'])) {
			$this->SubmitComment();
		}
				
 		 
		$this->Hook_Run('bb_show',array("oBb"=>$oBb));		
		
		
		$sTextSeo=preg_replace("/<.*>/Ui",' ',$oBb->getText());
		$this->Viewer_SetHtmlDescription(func_text_words($sTextSeo,20));
		$this->Viewer_SetHtmlKeywords(func_text_words($sTextSeo,20));		
		
		
		$aReturn=$this->Comment_GetCommentsByTargetId($oBb->getId(),'bb');
		$iMaxIdComment=$aReturn['iMaxIdComment'];
		$aComments=$aReturn['comments'];		
		
		$this->Viewer_Assign('aComments',$aComments);
		$this->Viewer_Assign('iMaxIdComment',$iMaxIdComment);
		
		$this->Viewer_AddHtmlTitle($oBb->getTitle());		
		$this->Viewer_Assign('oBb',$oBb);
		
		$this->SetTemplateAction('bb');
	}	
	
	protected function EventIndex() {

		
		$this->sMenuSubItemSelect='add';

		$iPage=$this->GetEventMatch(2) ? $this->GetEventMatch(2) : 1;

		$aResult=$this->Bb_GetBbsGood($iPage,Config::Get('module.topic.per_page'));			
		$aBbs=$aResult['collection'];	

		$aPaging=$this->Viewer_MakePaging($aResult['count'],$iPage,Config::Get('module.topic.per_page'),4,Router::GetPath('bb'));

		$this->Viewer_Assign('aBbs',$aBbs);
		$this->Viewer_Assign('aPaging',$aPaging);		
		
		$this->SetTemplateAction('index');
	}	
	
	protected function EventEdit() {

		
		$this->sMenuSubItemSelect='';
		$this->sMenuItemSelect='bb';
		$sBbId=$this->GetParam(0);
		if (!($oBb=$this->Bb_GetBbById($sBbId))) {
			return parent::EventNotFound();
		}

		if (!$this->ACL_IsAllowEditBb($oBb,$this->oUserCurrent)) {
			return parent::EventNotFound();
		}	

		$this->Hook_Run('bb_edit_show',array('oBb'=>$oBb));

		$this->Viewer_AddHtmlTitle($this->Lang_Get('bb_bb_edit'));

		$this->SetTemplateAction('add');		

		if (isset($_REQUEST['submit_bb_publish'])) {
			return $this->SubmitEdit($oBb);
		} else {

			$_REQUEST['bb_title']=$oBb->getTitle();
			$_REQUEST['bb_text']=$oBb->getTextSource();
			$_REQUEST['bb_tags']=$oBb->getTags();
			$_REQUEST['bb_id']=$oBb->getId();
		}
			
	}

	protected function EventDelete() {
		
		$this->Security_ValidateSendForm();

		$sBbId=$this->GetParam(0);
		if (!($oBb=$this->Bb_GetBbById($sBbId))) {
			return parent::EventNotFound();
		}

		if (!$this->ACL_IsAllowDeleteBb($oBb,$this->oUserCurrent)) {
			return parent::EventNotFound();
		}

		$this->Hook_Run('bb_delete_before', array('oBb'=>$oBb));
		$this->Bb_DeleteBb($oBb);
		$this->Hook_Run('bb_delete_after', array('oBb'=>$oBb));
		Router::Location(Router::GetPath('bb'));
		
	}

	protected function EventAdd() {
		
		$this->sMenuSubItemSelect='add';	
		$this->Hook_Run('bb_add_show');
		$this->Viewer_AddHtmlTitle($this->Lang_Get('bb_bb_create'));
		return $this->SubmitAdd();		
		
	}	

	protected function SubmitAdd() {
		
		if (!isPost('submit_bb_publish')) {
			return false;
		}

		if (!$this->checkBbFields()) {
			return false;	
		}

		$oBb=Engine::GetEntity('Bb');
		$oBb->setUserId($this->oUserCurrent->getId());
		$oBb->setTitle(getRequest('bb_title'));
		$oBb->setTextHash(md5(getRequest('bb_text')));

		list($sTextShort,$sTextNew,$sTextCut) = $this->Text_Cut(getRequest('bb_text'));
		
		$oBb->setCutText($sTextCut);
		$oBb->setText($this->Text_Parser($sTextNew));
		$oBb->setTextShort($this->Text_Parser($sTextShort));
		
		$oBb->setTextSource(getRequest('bb_text'));
		$oBb->setTags(getRequest('bb_tags'));
		$oBb->setDateAdd(date("Y-m-d H:i:s"));
		$oBb->setUserIp(func_getIp());
		$oBb->setPublish(1);

		if ($this->Bb_AddBb($oBb)) {
			$oBb=$this->Bb_GetBbById($oBb->getId());
			Router::Location($oBb->getUrl());
		} else {
			$this->Message_AddErrorSingle($this->Lang_Get('system_error'));
			return Router::Action('error');
		}
		
	}

	protected function SubmitEdit($oBb) {				
		
		if (!$this->checkBbFields()) {
			return false;	
		}

		$oBb->setTitle(getRequest('bb_title'));
		$oBb->setTextHash(md5(getRequest('bb_text')));
		list($sTextShort,$sTextNew,$sTextCut) = $this->Text_Cut(getRequest('bb_text'));

		$oBb->setCutText($sTextCut);
		$oBb->setText($this->Text_Parser($sTextNew));
		$oBb->setTextShort($this->Text_Parser($sTextShort));
		
		$oBb->setTextSource(getRequest('bb_text'));
		$oBb->setTags(getRequest('bb_tags'));
		$oBb->setUserIp(func_getIp());
		$oBb->setPublish(1);
		$oBb->setDateAdd(date("Y-m-d H:i:s"));

		$this->Hook_Run('bb_edit_before', array('oBb'=>$oBb,'oBlog'=>$oBlog));
		if ($this->Bb_UpdateBb($oBb)) {
			$this->Hook_Run('bb_edit_after', array('oBb'=>$oBb,'oBlog'=>$oBlog,'bSendNotify'=>&$bSendNotify));
			Router::Location($oBb->getUrl());
		} else {
			$this->Message_AddErrorSingle($this->Lang_Get('system_error'));
			return Router::Action('error');
		}
	}

	protected function checkBbFields() {
		
		
		$this->Security_ValidateSendForm();
		
		$bOk=true; 
		
		if (!func_check(getRequest('bb_title',null,'post'),'text',2,200)) {
			$this->Message_AddError($this->Lang_Get('bb_create_title_error'),$this->Lang_Get('error'));
			$bOk=false;
		}
		if (!func_check(getRequest('bb_text',null,'post'),'text',2,Config::Get('module.topic.max_length'))) {
			$this->Message_AddError($this->Lang_Get('bb_create_text_error'),$this->Lang_Get('error'));
			$bOk=false;
		}
		if (!func_check(getRequest('bb_tags',null,'post'),'text',2,500)) {
			$this->Message_AddError($this->Lang_Get('bb_create_tags_error'),$this->Lang_Get('error'));
			$bOk=false;
		}
		
		$this->Hook_Run('check_bb_fields', array('bOk'=>&$bOk));
		
		return $bOk;
	}

	public function EventShutdown() {
		
		$this->Viewer_Assign('sMenuHeadItemSelect',$this->sMenuHeadItemSelect);
		//$this->Viewer_Assign('sMenuItemSelect',$this->sMenuItemSelect);
		$this->Viewer_Assign('sMenuSubItemSelect',$this->sMenuSubItemSelect);
		
	}
}
?>