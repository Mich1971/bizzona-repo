<?php

class ActionBa extends Action {

	protected $sMenuHeadItemSelect='ba';

	protected $sMenuItemSelect='ba';

	protected $sMenuSubItemSelect='add';

	protected $oUserCurrent=null;
	
	public function Init() {
		
		$this->oUserCurrent=$this->User_GetUserCurrent();
		$this->SetDefaultEvent('index');
		$this->Viewer_AddHtmlTitle($this->Lang_Get('ba_title'));
		
		Router::SetIsShowStats(false);
		
	}

	protected function RegisterEvent() {

		$this->AddEventPreg('/^(\d+)\.html$/i','/^$/i','EventShowBa');
		$this->AddEventPreg('/^(page(\d+))?$/i','EventIndex');		
		
		$this->AddEvent('index','EventIndex');
		$this->AddEvent('add','EventAdd');			
		$this->AddEvent('edit','EventEdit');
		$this->AddEvent('ajaxaddcomment','AjaxAddComment');
		$this->AddEvent('delete','EventDelete');
		
		$this->AddEvent('favourites','EventFavourites');

	}
	
	protected function EventFavourites() {				
		
		$iPage=$this->GetEventMatch(2) ? $this->GetEventMatch(2) : 1;

		$aResult=$this->Ba_GetBasFavouriteByUserId(
			$this->oUserCurrent->getId(),
			$iPage,Config::Get('module.qa.per_page')
		);	
		
		$aBas=$aResult['collection'];		 
		
		$aPaging=$this->Viewer_MakePaging($aResult['count'],$iPage,Config::Get('module.qa.per_page'),4,Router::GetPath('ba'));
		
		$this->Viewer_Assign('aPaging',$aPaging);
		$this->Viewer_Assign('aBas',$aBas);
		
		$this->SetTemplateAction('index');
		
	}	
		
	protected function EventDelete() {
		
		$this->Security_ValidateSendForm();

		$sBaId=$this->GetParam(0);
		if (!($oBa=$this->Ba_GetBaById($sBaId))) {
			return parent::EventNotFound();
		}

		if (!$this->ACL_IsAllowDeleteBa($oBa,$this->oUserCurrent)) {
			return parent::EventNotFound();
		}

		$this->Hook_Run('ba_delete_before', array('oBa'=>$oBa));
		$this->Ba_DeleteBa($oBa);
		$this->Hook_Run('ba_delete_after', array('oBa'=>$oBa));

		Router::Location(Router::GetPath('ba'));
		
	}	
	
	protected function AjaxAddComment() {

		$this->Viewer_SetResponseAjax();
		$this->SubmitComment();
		
	}	
	
	protected function SubmitComment() {
		
		$sText=$this->Text_Parser(getRequest('comment_text'));
		
		if (!($oBa=$this->Ba_GetBaById(getRequest('cmt_target_id')))) {
			$this->Message_AddErrorSingle($this->Lang_Get('system_error'),$this->Lang_Get('error'));
			return;
		}		
		
		if (!func_check($sText,'text',2,10000)) {			
			$this->Message_AddErrorSingle($this->Lang_Get('qa_comment_add_text_error'),$this->Lang_Get('error'));
			return;
		}		
		$sParentId=(int)getRequest('reply');
		if (!func_check($sParentId,'id')) {			
			$this->Message_AddErrorSingle($this->Lang_Get('system_error'),$this->Lang_Get('error'));
			return;
		}
		$oCommentParent=null;
		if ($sParentId!=0) {
			if (!($oCommentParent=$this->Comment_GetCommentById($sParentId))) {				
				$this->Message_AddErrorSingle($this->Lang_Get('system_error'),$this->Lang_Get('error'));
				return;
			}
			if ($oCommentParent->getTargetId()!=$oTopic->getId()) {
				$this->Message_AddErrorSingle($this->Lang_Get('system_error'),$this->Lang_Get('error'));
				return;
			}
		} else {
			$sParentId=null;
		}		

		$oCommentNew=Engine::GetEntity('Comment');
		$oCommentNew->setTargetId($oBa->getId());
		$oCommentNew->setTargetType('ba');
		$oCommentNew->setUserId($this->oUserCurrent->getId());		
		$oCommentNew->setText($sText);
		$oCommentNew->setDate(date("Y-m-d H:i:s"));
		$oCommentNew->setUserIp(func_getIp());
		$oCommentNew->setPid($sParentId);
		$oCommentNew->setTextHash(md5($sText));
			
		$this->Hook_Run('comment_add_before', array('oCommentNew'=>$oCommentNew,'oCommentParent'=>$oCommentParent,'oBa'=>$oBa));
		if ($this->Comment_AddComment($oCommentNew)) {
			
			$this->Hook_Run('comment_add_after', array('oCommentNew'=>$oCommentNew,'oCommentParent'=>$oCommentParent,'oBa'=>$oBa));
			$this->Viewer_AssignAjax('sCommentId',$oCommentNew->getId());
			
			$oCommentOnline=Engine::GetEntity('Comment_CommentOnline');
			$oCommentOnline->setTargetId($oCommentNew->getTargetId());
			$oCommentOnline->setTargetType($oCommentNew->getTargetType());
			$oCommentOnline->setTargetParentId($oCommentNew->getTargetParentId());
			$oCommentOnline->setCommentId($oCommentNew->getId());
				
			$this->Comment_AddCommentOnline($oCommentOnline);
			
			$this->oUserCurrent->setDateCommentLast(date("Y-m-d H:i:s"));
			$this->User_Update($this->oUserCurrent);
			$oUserBa=$oBa->getUser();
			
			
			if ($oCommentNew->getUserId()!=$oUserBa->getId()) {
				$this->Notify_SendCommentNewToAuthorBa($oUserBa,$oBa,$oCommentNew,$this->oUserCurrent);
			}
			
			
			if ($oCommentParent and $oCommentParent->getUserId()!=$oBa->getUserId() and $oCommentNew->getUserId()!=$oCommentParent->getUserId()) {
				$oUserAuthorComment=$oCommentParent->getUser();
				
				$this->Notify_SendCommentReplyToAuthorBaParentComment($oUserAuthorComment,$oBa,$oCommentNew,$this->oUserCurrent);
				
			}
			
			
		} else {
			
			$this->Message_AddErrorSingle($this->Lang_Get('system_error'),$this->Lang_Get('error'));
			
		}
				
	}	
	
	protected function EventShowBa() {
		
		$sBaUrl='';
		$sBaUrl=$this->sCurrentEvent;
		
		$iBaId=$this->GetEventMatch(1);
		
		if (!($oBa=$this->Ba_GetBaById($iBaId))) {
			return parent::EventNotFound();
		}		
		
		if (isset($_REQUEST['submit_comment'])) {
			$this->SubmitComment();
		}		
 		 
		$this->Hook_Run('ba_show',array("oBa"=>$oBa));		
		
		$sTextSeo=preg_replace("/<.*>/Ui",' ',$oBa->getText());
		$this->Viewer_SetHtmlDescription(func_text_words($sTextSeo,20));
		$this->Viewer_SetHtmlKeywords(func_text_words($sTextSeo,20));		
		
		$aReturn=$this->Comment_GetCommentsByTargetId($oBa->getId(),'ba');
		$iMaxIdComment=$aReturn['iMaxIdComment'];
		$aComments=$aReturn['comments'];		
		
		$this->Viewer_Assign('aComments',$aComments);
		$this->Viewer_Assign('iMaxIdComment',$iMaxIdComment);
		
		
		if ($this->oUserCurrent) {
			
			$oBaRead=Engine::GetEntity('Ba_BaRead');
			$oBaRead->setBaId($oBa->getId());
			$oBaRead->setUserId($this->oUserCurrent->getId());
			$oBaRead->setCommentCountLast($oBa->getCountComment());
			$oBaRead->setCommentIdLast($iMaxIdComment);
			$oBaRead->setDateRead(date("Y-m-d H:i:s"));
			$this->Ba_SetBaRead($oBaRead);
		}		
		
		
		$this->Viewer_AddHtmlTitle($oBa->getTags());		
		$this->Viewer_Assign('oBa',$oBa);
		
		$this->SetTemplateAction('ba');
			
	}	
	
	
	protected function EventIndex() {
		
		
		
		$this->sMenuSubItemSelect='add';

		$iPage=$this->GetEventMatch(2) ? $this->GetEventMatch(2) : 1;

		$aResult=$this->Ba_GetBasGood($iPage,Config::Get('module.topic.per_page'));			
		$aBas=$aResult['collection'];	

		$aPaging=$this->Viewer_MakePaging($aResult['count'],$iPage,Config::Get('module.topic.per_page'),4,Router::GetPath('ba'));

		$this->Viewer_Assign('aBas',$aBas);
		$this->Viewer_Assign('aPaging',$aPaging);		

		$this->SetTemplateAction('index');
		
	}	
	
	protected function EventEdit() {

		if (!$this->User_IsAuthorization()) {
			return parent::EventNotFound();
		}		
		
		$this->sMenuSubItemSelect='';
		$this->sMenuItemSelect='ba';

		$sBaId=$this->GetParam(0);
		
		if (!($oBa=$this->Ba_GetBaById($sBaId))) {
			return parent::EventNotFound();
		}
		
		if (!$this->ACL_IsAllowEditBa($oBa,$this->oUserCurrent)) {
			return parent::EventNotFound();
		}	

		$this->Hook_Run('ba_edit_show',array('oBa'=>$oBa));

		$this->Viewer_AddHtmlTitle($this->Lang_Get('topic_topic_edit'));

		$this->SetTemplateAction('add');		

		
		if (isset($_REQUEST['submit_ba_publish'])) {
			return $this->SubmitEdit($oBa);
		} else {
			$_REQUEST['ba_text']=$oBa->getText();
			$_REQUEST['ba_tags']=$oBa->getTags();
			$_REQUEST['ba_id']=$oBa->getId();
		}		
		
	}

	protected function EventAdd() {
		
		
		if (!$this->User_IsAuthorization()) {
			return parent::EventNotFound();
		}		
		
		$this->sMenuSubItemSelect='add';	
		$this->Hook_Run('ba_add_show');
		$this->Viewer_AddHtmlTitle($this->Lang_Get('ba_ba_create'));
		return $this->SubmitAdd();		
		
	}	

	protected function EventShowBas() {		
	}

	protected function checkBaFields() {
		
		
		$this->Security_ValidateSendForm();
		
		$bOk=true;

		if (!func_check(getRequest('ba_text',null,'post'),'text',2,Config::Get('module.topic.max_length'))) {
			$this->Message_AddError($this->Lang_Get('ba_create_text_error'),$this->Lang_Get('error'));
			$bOk=false;
		}

		if (!func_check(getRequest('ba_tags',null,'post'),'text',2,500)) {
			$this->Message_AddError($this->Lang_Get('qa_create_tags_error'),$this->Lang_Get('error'));
			$bOk=false;
		}		
		
		$this->Hook_Run('check_ba_fields', array('bOk'=>&$bOk));
		
		return $bOk;
		
	}	
	
	protected function SubmitAdd() {
		
		if (!isPost('submit_ba_publish')) {
			return false;
		}	

		if (!$this->checkBaFields()) {
			return false;	
		}	
			
		$oBa=Engine::GetEntity('Ba');
		
		$oBa->setUserId($this->oUserCurrent->getId());
		$oBa->setText(getRequest('ba_text'));
		$oBa->setTags(getRequest('ba_tags'));
		$oBa->setDateAdd(date("Y-m-d H:i:s"));
		$oBa->setUserIp(func_getIp());

		$this->Hook_Run('ba_add_before', array('oBa'=>$oBa));

		if ($this->Ba_AddBa($oBa)) {
			$this->Hook_Run('ba_add_after', array('oBa'=>$oBa));
			Router::Location($oBa->getUrl());
		} else {
			$this->Message_AddErrorSingle($this->Lang_Get('system_error'));
			return Router::Action('error');
		}
				
	}

	protected function SubmitEdit($oBa) {			
		
		if (!$this->checkBaFields()) {
			return false;	
		}	
		
		$oBa->setText(getRequest('ba_text'));
		$oBa->setTags(getRequest('ba_tags'));

		$bSendNotify=false;

		$this->Hook_Run('ba_edit_before', array('oBa'=>$oBa));

		
		if ($this->Ba_UpdateBa($oBa)) {
			$this->Hook_Run('ba_edit_after', array('oBa'=>$oBa,'bSendNotify'=>&$bSendNotify));

			Router::Location($oBa->getUrl());
		} else {
			$this->Message_AddErrorSingle($this->Lang_Get('system_error'));
			return Router::Action('error');
		}
		
	}

	public function EventShutdown() {
		
		$this->Viewer_Assign('sMenuHeadItemSelect',$this->sMenuHeadItemSelect);
		//$this->Viewer_Assign('sMenuItemSelect',$this->sMenuItemSelect);
		$this->Viewer_Assign('sMenuSubItemSelect',$this->sMenuSubItemSelect);
		
	}
}
?>