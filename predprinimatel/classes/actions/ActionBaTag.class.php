<?php

class ActionBaTag extends Action {	

	protected $sMenuHeadItemSelect='ba';

	public function Init() {
		
		Router::SetIsShowStats(false);
		
	}
	
	protected function RegisterEvent() {	
		$this->AddEventPreg('/^.+$/i','/^(page(\d+))?$/i','EventBaTags');
	}
		
	protected function EventBaTags() {

		$sTag=$this->sCurrentEvent;

		$iPage=$this->GetParamEventMatch(0,2) ? $this->GetParamEventMatch(0,2) : 1;		

		$aResult=$this->Ba_GetBasByTag($sTag,$iPage,Config::Get('module.topic.per_page'));
		$aBas=$aResult['collection'];	

		
		$aPaging=$this->Viewer_MakePaging($aResult['count'],$iPage,Config::Get('module.topic.per_page'),4,Router::GetPath('batag').htmlspecialchars($sTag));

		
		$this->Viewer_Assign('aPaging',$aPaging);
		$this->Viewer_Assign('aBas',$aBas);
		$this->Viewer_Assign('sTag',$sTag);
		$this->Viewer_AddHtmlTitle($this->Lang_Get('tag_title'));
		$this->Viewer_AddHtmlTitle($sTag);

		$this->SetTemplateAction('index');		
		
	}	
	
	public function EventShutdown() {		
		$this->Viewer_Assign('sMenuHeadItemSelect',$this->sMenuHeadItemSelect);
	}
}
?>