<?php

class ActionQa extends Action {

	protected $oUserCurrent=null;	
	
	protected $sMenuHeadItemSelect='qa';
	protected $sMenuSubItemSelect='add';
	
	public function Init() {
		
		$this->oUserCurrent=$this->User_GetUserCurrent();
		$this->SetDefaultEvent('index');
		$this->Viewer_AddHtmlTitle($this->Lang_Get('qa_title'));
		
		Router::SetIsShowStats(false);
	}
	
	protected function RegisterEvent() {
		
		$this->AddEventPreg('/^(\d+)\.html$/i','/^$/i','EventShowQa');
		$this->AddEventPreg('/^[\w\-\_]+$/i','/^(\d+)\.html$/i','EventShowQa');
		$this->AddEventPreg('/^(page(\d+))?$/i','EventIndex');
		
		$this->AddEvent('ajaxaddcomment','AjaxAddComment');
		$this->AddEvent('add','EventAdd');
		$this->AddEvent('index','EventIndex');
		$this->AddEvent('delete','EventDelete');
		$this->AddEvent('edit','EventEdit');
		$this->AddEvent('favourites','EventFavourites');
	}	
	
	protected function EventFavourites() {				
		
	
		$iPage=$this->GetEventMatch(2) ? $this->GetEventMatch(2) : 1;

		$aResult=$this->Qa_GetQasFavouriteByUserId(
			$this->oUserCurrent->getId(),
			$iPage,Config::Get('module.qa.per_page')
		);	
		
		$aQas=$aResult['collection'];		 
		
		$aPaging=$this->Viewer_MakePaging($aResult['count'],$iPage,Config::Get('module.qa.per_page'),4,Router::GetPath('qa'));
		
		$this->Viewer_Assign('aPaging',$aPaging);
		$this->Viewer_Assign('aQas',$aQas);
		
		$this->SetTemplateAction('index');
		
	}	
	
	protected function EventEdit() {
		
		if (!$this->User_IsAuthorization()) {
			return parent::EventNotFound();
		}		
		
		$sQaId=$this->GetParam(0);
		if (!($oQa=$this->Qa_GetQaById($sQaId))) {
			return parent::EventNotFound();
		}

		if (!$this->ACL_IsAllowEditQa($oQa,$this->oUserCurrent)) {
			return parent::EventNotFound();
		}	

		
		$this->Hook_Run('qa_edit_show',array('oQa'=>$oQa));
		/*
		$this->Viewer_Assign('aBlogsAllow',$this->Blog_GetBlogsAllowByUser($this->oUserCurrent));
        */ 
		$this->Viewer_AddHtmlTitle($this->Lang_Get('topic_topic_edit'));
		$this->SetTemplateAction('add');		

		if (isset($_REQUEST['submit_qa_publish'])) {
			return $this->SubmitEdit($oQa);
		} else {
			
			$_REQUEST['qa_title']=$oQa->getTitle();
			$_REQUEST['qa_text']=$oQa->getDescription();
			$_REQUEST['qa_id']=$oQa->getId();
			$_REQUEST['qa_tags']=$oQa->getTags();
		}
			
	}	
	
	protected function checkQaFields() {
		
		$this->Security_ValidateSendForm();
		
		$bOk=true;

		/*
		if (!func_check(getRequest('blog_id',null,'post'),'id')) {
			$this->Message_AddError($this->Lang_Get('topic_create_blog_error_unknown'),$this->Lang_Get('error'));
			$bOk=false;
		}
		*/

		if (!func_check(getRequest('qa_title',null,'post'),'text',2,200)) {
			$this->Message_AddError($this->Lang_Get('qa_create_title_error'),$this->Lang_Get('error'));
			$bOk=false;
		}

		
		if (!func_check(getRequest('qa_text',null,'post'),'text',2,Config::Get('module.topic.max_length'))) {
			$this->Message_AddError($this->Lang_Get('qa_create_text_error'),$this->Lang_Get('error'));
			$bOk=false;
		}

		if (!func_check(getRequest('qa_tags',null,'post'),'text',2,500)) {
			$this->Message_AddError($this->Lang_Get('qa_create_tags_error'),$this->Lang_Get('error'));
			$bOk=false;
		}		
		
		$this->Hook_Run('check_qa_fields', array('bOk'=>&$bOk));
		
		return $bOk;
		
	}	
	
	protected function SubmitEdit($oQa) {				

		if (!$this->checkQaFields()) {
			return false;	
		}
		
		if (isset($_REQUEST['submit_qa_publish'])) {
			$oQa->setTitle(getRequest('qa_title')); 
			$oQa->setDescription(getRequest('qa_text'));	
			$oQa->setTags(getRequest('qa_tags'));
		}	
		
		if ($this->Qa_UpdateQa($oQa)) {
			Router::Location(Router::GetPath('qa').$oQa->getId().'.html');
		} else {
			$this->Message_AddErrorSingle($this->Lang_Get('system_error'));
			return Router::Action('error');
		}
				
	}	
	
	
	protected function EventDelete() {
		
		$this->Security_ValidateSendForm();

		$sQaId=$this->GetParam(0);
		if (!($oQa=$this->Qa_GetQaById($sQaId))) {
			return parent::EventNotFound();
		}

		if (!$this->ACL_IsAllowDeleteQa($oQa,$this->oUserCurrent)) {
			return parent::EventNotFound();
		}

		$this->Hook_Run('qa_delete_before', array('oQa'=>$oQa));
		$this->Qa_DeleteQa($oQa);
		$this->Hook_Run('qa_delete_after', array('oQa'=>$oQa));

		Router::Location(Router::GetPath('qa'));
	}	
	
	protected function EventIndex() {
		
		$iPage=$this->GetEventMatch(2) ? $this->GetEventMatch(2) : 1;
		 
		//echo Config::Get('module.qa.per_page');
		$aFilter = array();
		$aResult=$this->Qa_GetQasByFilter($aFilter,$iPage,Config::Get('module.qa.per_page'));			
		
		$aQas=$aResult['collection'];		 
		
		$aPaging=$this->Viewer_MakePaging($aResult['count'],$iPage,Config::Get('module.qa.per_page'),4,Router::GetPath('qa'));
		//var_dump($aPaging);
		
		
		$this->Viewer_Assign('aPaging',$aPaging);
		$this->Viewer_Assign('aQas',$aQas);
		
		$this->SetTemplateAction('index');
	}
	
	protected function EventAdd() {

		if (!$this->User_IsAuthorization()) {
			return parent::EventNotFound();
		}		
		
		return $this->SubmitAdd();
	}	
	
	protected function SubmitAdd() {
		
		if (!isPost('submit_qa_publish')) {
			return false;
		}		
		
		if (!$this->checkQaFields()) {
			return false;	
		}		
		
		$oQa=Engine::GetEntity('Qa');
		$oQa->setOwnerId($this->oUserCurrent->getId());
		$oQa->setTitle(getRequest('qa_title')); 
		$oQa->setDescription(getRequest('qa_text'));
		$oQa->setTags(getRequest('qa_tags'));
		
		$oQa->setLimitRatingTopic(0); 
		$oQa->setDateAdd(date("Y-m-d H:i:s"));
		$oQa->setType("open");
		
		if ($sId = $this->Qa_AddQa($oQa)) {
			
			Router::Location(Router::GetPath('qa').$sId .'.html');
		} else {
			$this->Message_AddErrorSingle($this->Lang_Get('system_error'));
			return Router::Action('error');
		}
		
				
	}
	
	protected function AjaxAddComment() {
		$this->Viewer_SetResponseAjax();
		$this->SubmitComment();
	}	
	
	protected function SubmitComment() {
		
		$sText=$this->Text_Parser(getRequest('comment_text'));
		
		if (!($oQa=$this->Qa_GetQaById(getRequest('cmt_target_id')))) {
			$this->Message_AddErrorSingle($this->Lang_Get('system_error'),$this->Lang_Get('error'));
			return;
		}		
		
		if (!func_check($sText,'text',2,10000)) {			
			$this->Message_AddErrorSingle($this->Lang_Get('qa_comment_add_text_error'),$this->Lang_Get('error'));
			return;
		}		
		$sParentId=(int)getRequest('reply');
		if (!func_check($sParentId,'id')) {			
			$this->Message_AddErrorSingle($this->Lang_Get('system_error'),$this->Lang_Get('error'));
			return;
		}
		$oCommentParent=null;
		if ($sParentId!=0) {
			if (!($oCommentParent=$this->Comment_GetCommentById($sParentId))) {				
				$this->Message_AddErrorSingle($this->Lang_Get('system_error'),$this->Lang_Get('error'));
				return;
			}
			if ($oCommentParent->getTargetId()!=$oTopic->getId()) {
				$this->Message_AddErrorSingle($this->Lang_Get('system_error'),$this->Lang_Get('error'));
				return;
			}
		} else {
			$sParentId=null;
		}		
		

		$oCommentNew=Engine::GetEntity('Comment');
		$oCommentNew->setTargetId($oQa->getId());
		$oCommentNew->setTargetType('qa');
		$oCommentNew->setUserId($this->oUserCurrent->getId());		
		$oCommentNew->setText($sText);
		$oCommentNew->setDate(date("Y-m-d H:i:s"));
		$oCommentNew->setUserIp(func_getIp());
		$oCommentNew->setPid($sParentId);
		$oCommentNew->setTextHash(md5($sText));
			
		$this->Hook_Run('comment_add_before', array('oCommentNew'=>$oCommentNew,'oCommentParent'=>$oCommentParent,'oQa'=>$oQa));
		if ($this->Comment_AddComment($oCommentNew)) {
			
			$this->Hook_Run('comment_add_after', array('oCommentNew'=>$oCommentNew,'oCommentParent'=>$oCommentParent,'oQa'=>$oQa));
			
			$this->Viewer_AssignAjax('sCommentId',$oCommentNew->getId());

			$oCommentOnline=Engine::GetEntity('Comment_CommentOnline');
			$oCommentOnline->setTargetId($oCommentNew->getTargetId());
			$oCommentOnline->setTargetType($oCommentNew->getTargetType());
			$oCommentOnline->setTargetParentId($oCommentNew->getTargetParentId());
			$oCommentOnline->setCommentId($oCommentNew->getId());
				
			$this->Comment_AddCommentOnline($oCommentOnline);
			
			$this->oUserCurrent->setDateCommentLast(date("Y-m-d H:i:s"));
			$this->User_Update($this->oUserCurrent);
			$oUserQa=$oQa->getUser();
			
			
			if ($oCommentNew->getUserId()!=$oUserQa->getId()) {
				$this->Notify_SendCommentNewToAuthorQa($oUserQa,$oQa,$oCommentNew,$this->oUserCurrent);
			}
			
			if ($oCommentParent and $oCommentParent->getUserId()!=$oQa->getUserId() and $oCommentNew->getUserId()!=$oCommentParent->getUserId()) {
				$oUserAuthorComment=$oCommentParent->getUser();
				$this->Notify_SendCommentReplyToAuthorQaParentComment($oUserAuthorComment,$oQa,$oCommentNew,$this->oUserCurrent);
			}
			
			
			
		} else {
			
			$this->Message_AddErrorSingle($this->Lang_Get('system_error'),$this->Lang_Get('error'));
			
		}		
	}	
	
	protected function EventShowQa() {
		
		$sQaUrl='';
		$sQaUrl=$this->sCurrentEvent;
		
		$iQaId=$this->GetEventMatch(1);
		
		if (!($oQa=$this->Qa_GetQaById($iQaId))) {
			return parent::EventNotFound();
		}		
		
		if (isset($_REQUEST['submit_comment'])) {
			$this->SubmitComment();
		}		
 		 
		$this->Hook_Run('qa_show',array("oQa"=>$oQa));		
		
		$sTextSeo=preg_replace("/<.*>/Ui",' ',$oQa->getText());
		$this->Viewer_SetHtmlDescription(func_text_words($sTextSeo,20));
		$this->Viewer_SetHtmlKeywords(func_text_words($sTextSeo,20));		
		
		$aReturn=$this->Comment_GetCommentsByTargetId($oQa->getId(),'qa');
		$iMaxIdComment=$aReturn['iMaxIdComment'];
		$aComments=$aReturn['comments'];		
		
		$this->Viewer_Assign('aComments',$aComments);
		$this->Viewer_Assign('iMaxIdComment',$iMaxIdComment);
		
		if ($this->oUserCurrent) {
			
			$oQaRead=Engine::GetEntity('Qa_QaRead');
			$oQaRead->setQaId($oQa->getId());
			$oQaRead->setUserId($this->oUserCurrent->getId());
			$oQaRead->setCommentCountLast($oQa->getCountComment());
			$oQaRead->setCommentIdLast($iMaxIdComment);
			$oQaRead->setDateRead(date("Y-m-d H:i:s"));
			$this->Qa_SetQaRead($oQaRead);
		}		
		
		$this->Viewer_AddHtmlTitle($oQa->getTitle());		
		$this->Viewer_Assign('oQa',$oQa);
		
		$this->SetTemplateAction('qa');
			
	}
	
	public function EventShutdown() {
		$this->Viewer_Assign('sMenuHeadItemSelect',$this->sMenuHeadItemSelect);
		//$this->Viewer_Assign('sMenuItemSelect',$this->sMenuItemSelect);
		$this->Viewer_Assign('sMenuSubItemSelect',$this->sMenuSubItemSelect);
	}	
}
?>