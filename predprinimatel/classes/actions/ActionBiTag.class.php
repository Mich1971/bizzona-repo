<?php

class ActionBiTag extends Action {	

	protected $sMenuHeadItemSelect='bi';

	public function Init() {
		
		Router::SetIsShowStats(false);
		
	}
	
	protected function RegisterEvent() {	
		$this->AddEventPreg('/^.+$/i','/^(page(\d+))?$/i','EventBiTags');
	}
		
	protected function EventBiTags() {

		$sTag=$this->sCurrentEvent;

		$iPage=$this->GetParamEventMatch(0,2) ? $this->GetParamEventMatch(0,2) : 1;		

		$aResult=$this->Bi_GetBisByTag($sTag,$iPage,Config::Get('module.topic.per_page'));
		$aBis=$aResult['collection'];	

		
		$aPaging=$this->Viewer_MakePaging($aResult['count'],$iPage,Config::Get('module.topic.per_page'),4,Router::GetPath('bitag').htmlspecialchars($sTag));

		
		$this->Viewer_Assign('aPaging',$aPaging);
		$this->Viewer_Assign('aBis',$aBis);
		$this->Viewer_Assign('sTag',$sTag);
		$this->Viewer_AddHtmlTitle($this->Lang_Get('tag_title'));
		$this->Viewer_AddHtmlTitle($sTag);

		$this->SetTemplateAction('index');		
		
	}	
	
	public function EventShutdown() {		
		$this->Viewer_Assign('sMenuHeadItemSelect',$this->sMenuHeadItemSelect);
	}
}
?>