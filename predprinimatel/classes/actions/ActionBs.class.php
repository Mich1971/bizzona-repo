<?php

class ActionBs extends Action {

	protected $oUserCurrent=null;	
	
	protected $sMenuHeadItemSelect='bs';
	protected $sMenuSubItemSelect='add';
	
	public function Init() {
		
		$this->oUserCurrent=$this->User_GetUserCurrent();
		$this->SetDefaultEvent('index');
		$this->Viewer_AddHtmlTitle($this->Lang_Get('bs_title'));
		
		Router::SetIsShowStats(false);
	}
	
	protected function RegisterEvent() {
		
		$this->AddEvent('index','EventIndex');
		$this->AddEvent('add','EventAdd');
		$this->AddEvent('edit','EventEdit');
		$this->AddEvent('ajaxaddcomment','AjaxAddComment');
		$this->AddEvent('delete','EventDelete');
		
		$this->AddEventPreg('/^(\d+)\.html$/i','/^$/i','EventShowBs');
		$this->AddEventPreg('/^[\w\-\_]+$/i','/^(\d+)\.html$/i','EventShowBs');
		$this->AddEventPreg('/^(page(\d+))?$/i','EventIndex');
		
		$this->AddEvent('favourites','EventFavourites');
	}	
	
	protected function EventFavourites() {				
		
		$iPage=$this->GetEventMatch(2) ? $this->GetEventMatch(2) : 1;

		$aResult=$this->Bs_GetBssFavouriteByUserId(
			$this->oUserCurrent->getId(),
			$iPage,Config::Get('module.qa.per_page')
		);	
		
		$aBss=$aResult['collection'];		 
		
		$aPaging=$this->Viewer_MakePaging($aResult['count'],$iPage,Config::Get('module.qa.per_page'),4,Router::GetPath('bs'));
		
		$this->Viewer_Assign('aPaging',$aPaging);
		$this->Viewer_Assign('aBss',$aBss);
		
		$this->SetTemplateAction('index');
		
	}	
	
	
	protected function EventEdit() {
		
		
		if (!$this->User_IsAuthorization()) {
			return parent::EventNotFound();
		}		
		
		$sBsId=$this->GetParam(0);
		if (!($oBs=$this->Bs_GetBsById($sBsId))) {
			return parent::EventNotFound();
		}

		
		if (!$this->ACL_IsAllowEditBs($oBs,$this->oUserCurrent)) {
			return parent::EventNotFound();
		}	
		
		$this->Hook_Run('bs_edit_show',array('oBs'=>$oBs));

		$this->Viewer_AddHtmlTitle($this->Lang_Get('topic_topic_edit'));
		$this->SetTemplateAction('add');		

		if (isset($_REQUEST['submit_bs_publish'])) {
			return $this->SubmitEdit($oBs);
		} else {
			
			$_REQUEST['bs_text']=$oBs->getText();
			$_REQUEST['bs_id']=$oBs->getId();
		}
			
	}	
	
	protected function checkBsFields() {
			
		$this->Security_ValidateSendForm();
		
		$bOk=true;

		if (!func_check(getRequest('bs_text',null,'post'),'text',2,Config::Get('module.topic.max_length'))) {
			$this->Message_AddError($this->Lang_Get('bs_create_text_error'),$this->Lang_Get('error'));
			$bOk=false;
		}

		$this->Hook_Run('check_bs_fields', array('bOk'=>&$bOk));
		
		return $bOk;
	}	
	
	protected function SubmitEdit($oBs) {				

		
		if (!$this->checkBsFields()) {
			return false;	
		}
		
		if (isset($_REQUEST['submit_bs_publish'])) {
			$oBs->setText(getRequest('bs_text'));	
		}	
		
		if ($this->Bs_UpdateBs($oBs)) {
			Router::Location(Router::GetPath('bs').$oBs->getId().'.html');
		} else {
			$this->Message_AddErrorSingle($this->Lang_Get('system_error'));
			return Router::Action('error');
		}
	}	
	
	
	protected function EventDelete() {
		
		
		$this->Security_ValidateSendForm();

		$sBsId=$this->GetParam(0);
		if (!($oBs=$this->Bs_GetBsById($sBsId))) {
			return parent::EventNotFound();
		}

		if (!$this->ACL_IsAllowDeleteBs($oBs,$this->oUserCurrent)) {
			return parent::EventNotFound();
		}

		$this->Hook_Run('bs_delete_before', array('oBs'=>$oBs));
		$this->Bs_DeleteBs($oBs);
		$this->Hook_Run('bs_delete_after', array('oBs'=>$oBs));

		Router::Location(Router::GetPath('bs'));
		
	}	
	
	protected function EventIndex() {
				
		$iPage=$this->GetEventMatch(2) ? $this->GetEventMatch(2) : 1;
		
		$aResult=$this->Bs_GetBssByFilter($iPage,Config::Get('module.bs.per_page'));			
		
		$aBss=$aResult['collection'];		 
		
		$aPaging=$this->Viewer_MakePaging($aResult['count'],$iPage,Config::Get('module.bs.per_page'),4,Router::GetPath('bs'));
		
		$this->Viewer_Assign('aPaging',$aPaging);
		$this->Viewer_Assign('aBss',$aBss);
		
		$this->SetTemplateAction('index');
		
	}
	
	protected function EventAdd() {
		
		if (!$this->User_IsAuthorization()) {
			return parent::EventNotFound();
		}		
		
		return $this->SubmitAdd();
		
	}	
	
	protected function SubmitAdd() {
		
		if (!isPost('submit_bs_publish')) {
			return false;
		}		
		
		if (!$this->checkBsFields()) {
			return false;	
		}		
		
		$oBs=Engine::GetEntity('Bs');
		$oBs->setOwnerId($this->oUserCurrent->getId());
		$oBs->setText(getRequest('bs_text'));
		
		$oBs->setDateAdd(date("Y-m-d H:i:s"));
		
		if ($sId = $this->Bs_AddBs($oBs)) {
			Router::Location(Router::GetPath('bs').$sId .'.html');
		} else {
			$this->Message_AddErrorSingle($this->Lang_Get('system_error'));
			return Router::Action('error');
		}
	}
	
	protected function AjaxAddComment() {
		
		$this->Viewer_SetResponseAjax();
		$this->SubmitComment();
		
	}	
	
	protected function SubmitComment() {
		
		$sText=$this->Text_Parser(getRequest('comment_text'));
		
		if (!($oBs=$this->Bs_GetBsById(getRequest('cmt_target_id')))) {
			$this->Message_AddErrorSingle($this->Lang_Get('system_error'),$this->Lang_Get('error'));
			return;
		}		
		
		if (!func_check($sText,'text',2,10000)) {			
			$this->Message_AddErrorSingle($this->Lang_Get('bs_comment_add_text_error'),$this->Lang_Get('error'));
			return;
		}		
		$sParentId=(int)getRequest('reply');
		if (!func_check($sParentId,'id')) {			
			$this->Message_AddErrorSingle($this->Lang_Get('system_error'),$this->Lang_Get('error'));
			return;
		}
		$oCommentParent=null;
		if ($sParentId!=0) {
			if (!($oCommentParent=$this->Comment_GetCommentById($sParentId))) {				
				$this->Message_AddErrorSingle($this->Lang_Get('system_error'),$this->Lang_Get('error'));
				return;
			}
			if ($oCommentParent->getTargetId()!=$oTopic->getId()) {
				$this->Message_AddErrorSingle($this->Lang_Get('system_error'),$this->Lang_Get('error'));
				return;
			}
		} else {
			$sParentId=null;
		}		

		$oCommentNew=Engine::GetEntity('Comment');
		$oCommentNew->setTargetId($oBs->getId());
		$oCommentNew->setTargetType('bs');
		$oCommentNew->setUserId($this->oUserCurrent->getId());		
		$oCommentNew->setText($sText);
		$oCommentNew->setDate(date("Y-m-d H:i:s"));
		$oCommentNew->setUserIp(func_getIp());
		$oCommentNew->setPid($sParentId);
		$oCommentNew->setTextHash(md5($sText));
			
		$this->Hook_Run('comment_add_before', array('oCommentNew'=>$oCommentNew,'oCommentParent'=>$oCommentParent,'oBs'=>$oBs));
		if ($this->Comment_AddComment($oCommentNew)) {
			
			$this->Hook_Run('comment_add_after', array('oCommentNew'=>$oCommentNew,'oCommentParent'=>$oCommentParent,'oBs'=>$oBs));
			
			$this->Viewer_AssignAjax('sCommentId',$oCommentNew->getId());

			$oCommentOnline=Engine::GetEntity('Comment_CommentOnline');
			$oCommentOnline->setTargetId($oCommentNew->getTargetId());
			$oCommentOnline->setTargetType($oCommentNew->getTargetType());
			$oCommentOnline->setTargetParentId($oCommentNew->getTargetParentId());
			$oCommentOnline->setCommentId($oCommentNew->getId());
				
			$this->Comment_AddCommentOnline($oCommentOnline);
			
			$this->oUserCurrent->setDateCommentLast(date("Y-m-d H:i:s"));
			$this->User_Update($this->oUserCurrent);
			$oUserBs=$oBs->getUser();
			
			if ($oCommentNew->getUserId()!=$oUserBs->getId()) {
				$this->Notify_SendCommentNewToAuthorBs($oUserBs,$oBs,$oCommentNew,$this->oUserCurrent);
			}
			
			if ($oCommentParent and $oCommentParent->getUserId()!=$oBs->getUserId() and $oCommentNew->getUserId()!=$oCommentParent->getUserId()) {
				$oUserAuthorComment=$oCommentParent->getUser();
				$this->Notify_SendCommentReplyToAuthorBsParentComment($oUserAuthorComment,$oBs,$oCommentNew,$this->oUserCurrent);
			}
			
		} else {
			$this->Message_AddErrorSingle($this->Lang_Get('system_error'),$this->Lang_Get('error'));
		}
	}	
	
	protected function EventShowBs() {
		
		$sBsUrl='';
		$sBsUrl=$this->sCurrentEvent;
		
		$iBsId=$this->GetEventMatch(1);
		
		if (!($oBs=$this->Bs_GetBsById($iBsId))) {
			return parent::EventNotFound();
		}		
		
		if (isset($_REQUEST['submit_comment'])) {
			$this->SubmitComment();
		}		
 		 
		$this->Hook_Run('bs_show',array("oBs"=>$oBs));		
		
		//$sTextSeo=preg_replace("/<.*>/Ui",' ',$oBs->getText());
		$sTextSeo = substr($oBs->getText(), 0, 80); 
		$this->Viewer_SetHtmlDescription(func_text_words($sTextSeo,20));
		$this->Viewer_SetHtmlKeywords(func_text_words($sTextSeo,20));		
		
		$aReturn=$this->Comment_GetCommentsByTargetId($oBs->getId(),'bs');
		$iMaxIdComment=$aReturn['iMaxIdComment'];
		$aComments=$aReturn['comments'];		
		
		$this->Viewer_Assign('aComments',$aComments);
		$this->Viewer_Assign('iMaxIdComment',$iMaxIdComment);
		
		$this->Viewer_AddHtmlTitle($sTextSeo);		
		$this->Viewer_Assign('oBs',$oBs);
		
		$this->SetTemplateAction('bs');

	}
	
	public function EventShutdown() {
		
		$this->Viewer_Assign('sMenuHeadItemSelect',$this->sMenuHeadItemSelect);
		//$this->Viewer_Assign('sMenuItemSelect',$this->sMenuItemSelect);
		$this->Viewer_Assign('sMenuSubItemSelect',$this->sMenuSubItemSelect);
		
	}	
}
?>