<?php

class ModuleBi_EntityBi extends Entity 
{    
	protected $aExtra=null;
	
    public function getId() {
        return $this->_aData['bi_id'];
    }  
    public function getUserId() {
        return $this->_aData['user_id'];
    }
    public function getType() {
        return $this->_aData['bi_type'];
    }
    public function getTitle() {
        return $this->_aData['bi_title'];
    }
    public function getText() {
        return $this->_aData['bi_text'];
    }    
    public function getTextShort() {
        return $this->_aData['bi_text_short'];
    }
    public function getTextSource() {
        return $this->_aData['bi_text_source'];
    }
    public function getExtra() {
    	if (isset($this->_aData['bi_extra'])) {
        	return $this->_aData['bi_extra'];
    	}
    	return serialize('');
    } 
    public function getTags() {
        return $this->_aData['bi_tags'];
    }
    public function getDateAdd() {
        return $this->_aData['bi_date_add'];
    }
    public function getDateEdit() {
        return $this->_aData['bi_date_edit'];
    }
    public function getUserIp() {
        return $this->_aData['bi_user_ip'];
    }
    public function getPublish() {
        return $this->_aData['bi_publish'];
    }
    public function getPublishDraft() {
        return $this->_aData['bi_publish_draft'];
    }
    public function getPublishIndex() {
        return $this->_aData['bi_publish_index'];
    }
    public function getRating() {            
        return number_format(round($this->_aData['bi_rating'],2), 0, '.', '');
    }
    public function getCountVote() {
        return $this->_aData['bi_count_vote'];
    }
    public function getCountRead() {
        return $this->_aData['bi_count_read'];
    }
    public function getCountComment() {
        return $this->_aData['bi_count_comment'];
    }
    public function getCutText() {
        return $this->_aData['bi_cut_text'];
    }
    public function getForbidComment() {
        return $this->_aData['bi_forbid_comment'];
    }
    public function getTextHash() {
        return $this->_aData['bi_text_hash'];
    }
    
    public function getTagsArray() {
    	return explode(',',$this->getTags());    	
    } 
    public function getCountCommentNew() {
        return $this->_aData['count_comment_new'];
    }  
    public function getDateRead() {
        return $this->_aData['date_read'];
    }  
    public function getUser() {
        return $this->_aData['user'];
    }
    
    public function getUrl() {
    	return Router::GetPath('bi').$this->getId().'.html';
    }
    
    /*
    public function setUrl($data) {
        $this->_aData['qa_url']=$data;
    } 
    */   
    
    public function getVote() {
        return $this->_aData['vote'];
    }    
    public function setVote($data) {
        $this->_aData['vote']=$data;
    }    
    public function getUserQuestionIsVote() {
        return $this->_aData['user_question_is_vote'];
    }	
    public function getIsFavourite() {
        return $this->_aData['bi_is_favourite'];
    }
    
    protected function extractExtra() {
    	if (is_null($this->aExtra)) {
    		$this->aExtra=unserialize($this->getExtra());
    	}
    }
    public function getLinkUrl($bShort=false) {
    }    
    public function setLinkUrl($data) {
    }
    public function getLinkCountJump() {
    }
    public function setLinkCountJump($data) {
    }
    
	public function setId($data) {
        $this->_aData['bi_id']=$data;
    }
    public function setUserId($data) {
        $this->_aData['user_id']=$data;
    }
    public function setType($data) {
        $this->_aData['bi_type']=$data;
    }
    public function setTitle($data) {
        $this->_aData['bi_title']=$data;
    }
    public function setText($data) {
        $this->_aData['bi_text']=$data;
    }    
    public function setExtra($data) {
        //$this->_aData['bi_extra']=serialize($data);
        $this->_aData['bi_extra']= $data;
    }
    public function setTextShort($data) {
        $this->_aData['bi_text_short']=$data;
    }
    public function setTextSource($data) {
        $this->_aData['bi_text_source']=$data;
    }
    public function setTags($data) {
        $this->_aData['bi_tags']=$data;
    }
    public function setDateAdd($data) {
        $this->_aData['bi_date_add']=$data;
    }
    public function setDateEdit($data) {
        $this->_aData['bi_date_edit']=$data;
    }
    public function setUserIp($data) {
        $this->_aData['bi_user_ip']=$data;
    }
    public function setPublish($data) {
        $this->_aData['bi_publish']=$data;
    }
    public function setPublishDraft($data) {
        $this->_aData['bi_publish_draft']=$data;
    }
    public function setPublishIndex($data) {
        $this->_aData['bi_publish_index']=$data;
    }
    public function setRating($data) {
        $this->_aData['bi_rating']=$data;
    }
    public function setCountVote($data) {
        $this->_aData['bi_count_vote']=$data;
    }
    public function setCountRead($data) {
        $this->_aData['bi_count_read']=$data;
    }
    public function setCountComment($data) {
        $this->_aData['bi_count_comment']=$data;
    }
    public function setCutText($data) {
        $this->_aData['bi_cut_text']=$data;
    }
    public function setForbidComment($data) {
        $this->_aData['bi_forbid_comment']=$data;
    }
    public function setTextHash($data) {
        $this->_aData['bi_text_hash']=$data;
    }
    
    public function setCountCommentNew($data) {
        $this->_aData['count_comment_new']=$data;
    }
    public function setDateRead($data) {
        $this->_aData['date_read']=$data;
    }
    public function setIsFavourite($data) {
        $this->_aData['bi_is_favourite']=$data;
    }
}
?>