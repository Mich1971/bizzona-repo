<?php

class ModuleBi extends Module {		
	protected $oMapperBi;
	protected $oUserCurrent=null;
		
	public function Init() {		
		$this->oMapperBi=Engine::GetMapper(__CLASS__);
		$this->oUserCurrent=$this->User_GetUserCurrent();
	}

	public function GetBiRating($iCurrPage,$iPerPage) {		
		
		if (false === ($data = $this->Cache_Get("bi_rating_{$iCurrPage}_{$iPerPage}"))) {				
			$data = array('collection'=>$this->oMapperBi->GetBiRating($iCount,$iCurrPage,$iPerPage),'count'=>$iCount);
			$this->Cache_Set($data, "bi_rating_{$iCurrPage}_{$iPerPage}", array("bi_update","bi_new"), 60*60*24*2);
		}
				 
		$data['collection'] = $this->GetBisAdditionalData($data['collection']);
		return $data; 
	}	
	
	public function increaseBiCountComment($sBiId) {
		$this->Cache_Delete("bi_{$sBiId}");
		$this->Cache_Clean(Zend_Cache::CLEANING_MODE_MATCHING_TAG,array("bi_update"));
		return $this->oMapperBi->increaseBiCountComment($sBiId);		
		
	}	
	
	public function GetBisAdditionalData($aBiId,$aAllowData=array('user'=>array(),'blog'=>array('owner'=>array(),'relation_user'),'vote','favourite','comment_new')) {
		
		$aAllowData['favourite'] = true;
		
		func_array_simpleflip($aAllowData);
		if (!is_array($aBiId)) {
			$aBiId=array($aBiId);
		}

		$aBis=$this->GetBisByArrayId($aBiId);
		
		$aUserId=array();
		$aBisVote=array();
		$aFavouriteBis=array();

		foreach ($aBis as $oBi) {
			if (isset($aAllowData['user'])) {
				$aUserId[]=$oBi->getUserId();
			}			
		}
		
		$aUsers=isset($aAllowData['user']) && is_array($aAllowData['user']) ? $this->User_GetUsersAdditionalData($aUserId,$aAllowData['user']) : $this->User_GetUsersAdditionalData($aUserId);

		if (isset($aAllowData['vote']) and $this->oUserCurrent) {
			$aBisVote=$this->Vote_GetVoteByArray($aBiId,'bi',$this->oUserCurrent->getId());
		}		
		
		if (isset($aAllowData['favourite']) and $this->oUserCurrent) {
			$aFavouriteBis=$this->GetFavouriteBisByArray($aBiId,$this->oUserCurrent->getId());	
		}		

		foreach ($aBis as $oBi) {

			if (isset($aUsers[$oBi->getUserId()])) {
				$oBi->setUser($aUsers[$oBi->getUserId()]);
			} else {
				$oBi->setUser(null);
			}
						
			if (isset($aBisVote[$oBi->getId()])) {
				$oBi->setVote($aBisVote[$oBi->getId()]);				
			} else {
				$oBi->setVote(null);
			}		
			
			if (isset($aFavouriteBis[$oBi->getId()])) {
				$oBi->setIsFavourite(true);
			} else {
				$oBi->setIsFavourite(false);
			}				
			
		}		
		
		return $aBis;		
		
	}

	public function GetFavouriteBisByArray($aBiId,$sUserId) {
		return $this->Favourite_GetFavouritesByArray($aBiId,'bi',$sUserId);
	}	
	
	public function AddBi(ModuleBi_EntityBi $oBi) {
		
		if ($sId=$this->oMapperBi->AddBi($oBi)) {
			
			$oBi->setId($sId);
			
			$aTags=explode(',',$oBi->getTags());
			foreach ($aTags as $sTag) {
				$oTag=Engine::GetEntity('Bi_BiTag');
				$oTag->setBiId($oBi->getId());
				$oTag->setUserId($oBi->getUserId());
				$oTag->setText($sTag);
				$this->oMapperBi->AddBiTag($oTag);
			}
			
			$this->Cache_Clean(Zend_Cache::CLEANING_MODE_MATCHING_TAG,array('bi_new',"bi_update_user_{$oBi->getUserId()}",""));						
			return $oBi;
		}
		return false;
		
	}
	
	public function DeleteBiTagsByBiId($sBiId) {
		
		return $this->oMapperBi->DeleteBiTagsByBiId($sBiId);
		
	}	
	
	public function DeleteBi($oBiId) {
		
		if ($oBiId instanceof ModuleBi_EntityBi) {
			$sBiId=$oBiId->getId();
			$this->Cache_Clean(Zend_Cache::CLEANING_MODE_MATCHING_TAG,array("bi_update_user_{$oBiId->getUserId()}"));
		} else {
			$sBiId=$oBiId;
		}

		$this->Cache_Clean(Zend_Cache::CLEANING_MODE_MATCHING_TAG,array('bi_update'));
		$this->Cache_Delete("bi_{$sBiId}");

		if($bResult=$this->oMapperBi->DeleteBi($sBiId)){
			return $this->DeleteBiAdditionalData($sBiId);
		}

		return false;
	}

	public function DeleteBiAdditionalData($iBiId) {
		
		$this->Cache_Clean(Zend_Cache::CLEANING_MODE_MATCHING_TAG,array('bi_update'));
		$this->Cache_Delete("bi_{$iBiId}");

		$this->Comment_DeleteCommentByTargetId($iBiId,'bi');

		$this->DeleteBiTagsByBiId($iBiId);
		
		return true;		
	}

	public function UpdateBi(ModuleBi_EntityBi $oBi) {
		
		$oBiOld=$this->GetBiById($oBi->getId());
		$oBi->setDateEdit(date("Y-m-d H:i:s"));
		if ($this->oMapperBi->UpdateBi($oBi)) {	

			$aTags=explode(',',$oBi->getTags());
			$this->DeleteBiTagsByBiId($oBi->getId());
				
			foreach ($aTags as $sTag) {
				$oTag=Engine::GetEntity('Bi_BiTag');
				$oTag->setBiId($oBi->getId());
				$oTag->setUserId($oBi->getUserId());
				$oTag->setText($sTag);
				$this->oMapperBi->AddBiTag($oTag);
			}

			$this->Cache_Clean(Zend_Cache::CLEANING_MODE_MATCHING_TAG,array('bi_update',"bi_update_user_{$oBi->getUserId()}",""));
			$this->Cache_Delete("bi_{$oBi->getId()}");
			return true;
		}
		return false;		
		
	}	
		
	public function GetBiById($sId) {		
		$aBis=$this->GetBisAdditionalData($sId);
		
		if (isset($aBis[$sId])) {
			return $aBis[$sId];
		}
		return null;		
	}	

	public function GetBisByArrayId($aBiId) {
		
		if (!$aBiId) {
			return array();
		}
		if (Config::Get('sys.cache.solid')) {
			return $this->GetBisByArrayIdSolid($aBiId);
		}
		
		if (!is_array($aBiId)) {
			$aBiId=array($aBiId);
		}
		$aBiId=array_unique($aBiId);
		$aBis=array();
		$aBiIdNotNeedQuery=array();

		$aCacheKeys=func_build_cache_keys($aBiId,'bi_');
		if (false !== ($data = $this->Cache_Get($aCacheKeys))) {			
			foreach ($aCacheKeys as $sValue => $sKey ) {
				if (array_key_exists($sKey,$data)) {	
					if ($data[$sKey]) {
						$aBis[$data[$sKey]->getId()]=$data[$sKey];
					} else {
						$aBiIdNotNeedQuery[]=$sValue;
					}
				} 
			}
		}

		$aBiIdNeedQuery=array_diff($aBiId,array_keys($aBis));		
		$aBiIdNeedQuery=array_diff($aBiIdNeedQuery,$aBiIdNotNeedQuery);		
		$aBiIdNeedStore=$aBiIdNeedQuery;
		if ($data = $this->oMapperBi->GetBisByArrayId($aBiIdNeedQuery)) {
			foreach ($data as $oBi) {
				$aBis[$oBi->getId()]=$oBi;
				$this->Cache_Set($oBi, "bi_{$oBi->getId()}", array(), 60*60*24*4);
				$aBiIdNeedStore=array_diff($aBiIdNeedStore,array($oBi->getId()));
			}
		}

		foreach ($aBiIdNeedStore as $sId) {
			$this->Cache_Set(null, "bi_{$sId}", array(), 60*60*24*4);
		}	

		$aBis=func_array_sort_by_keys($aBis,$aBiId);
		return $aBis;		
		
	}

	public function GetBisByArrayIdSolid($aBiId) {
		
		if (!is_array($aBiId)) {
			$aBiId=array($aBiId);
		}
		$aBiId=array_unique($aBiId);	
		$aBis=array();	
		$s=join(',',$aBiId);
		if (false === ($data = $this->Cache_Get("bi_id_{$s}"))) {			
			$data = $this->oMapperBi->GetBisByArrayId($aBiId);
			foreach ($data as $oBi) {
				$aBis[$oBi->getId()]=$oBi;
			}
			$this->Cache_Set($aBis, "bi_id_{$s}", array("bi_update"), 60*60*24*1);
			return $aBis;
		}		
		return $data;		
		
	}

	public function GetBisFavouriteByUserId($sUserId,$iCurrPage,$iPerPage) {		
		
		$data = $this->Favourite_GetFavouritesByUserId($sUserId,'bi',$iCurrPage,$iPerPage);
		$data['collection']=$this->GetBisAdditionalData($data['collection']);
		return $data;		
	}

	public function GetCountBisFavouriteByUserId($sUserId) {
	}

	public function GetBisByFilter($aFilter,$iPage=0,$iPerPage=0,$aAllowData=array('user'=>array(),'blog'=>array('owner'=>array(),'relation_user'),'vote','favourite','comment_new')) {
		
		//echo $iCount;
		$iCount = 0;
		
		$s=serialize($aFilter);
		if (false === ($data = $this->Cache_Get("bi_filter_{$s}_{$iPage}_{$iPerPage}"))) {			
			$data = ($iPage*$iPerPage!=0) 
				? array(
						'collection'=>$this->oMapperBi->GetBis($aFilter,$iCount,$iPage,$iPerPage),
						'count'=>$iCount
					)
				: array(
						'collection'=>$this->oMapperBi->GetAllBis($aFilter),
						'count'=>$this->GetCountBisByFilter($aFilter)
					);
			$this->Cache_Set($data, "bi_filter_{$s}_{$iPage}_{$iPerPage}", array('bi_update','bi_new'), 60*60*24*3);
		}
		$data['collection']=$this->GetBisAdditionalData($data['collection'],$aAllowData);
		return $data;		
		
	}

	public function GetCountBisByFilter($aFilter) {
	}

	public function GetBisGood($iPage,$iPerPage,$bAddAccessible=true) {
		
		$aFilter=array(
			'blog_type' => array(
				'personal',
				'open'
			),
			'topic_publish' => 1,
			'topic_rating'  => array(
				'value' => Config::Get('module.blog.index_good'),
				'type'  => 'top',
				'publish_index'  => 1,
			)
		);	

		return $this->GetBisByFilter($aFilter,$iPage,$iPerPage);		
		
	}

	public function GetBisNew($iPage,$iPerPage,$bAddAccessible=true) {
	}

	public function GetBisLast($iCount) {		
	}

	public function GetBisPersonal($iPage,$iPerPage,$sShowType='good') {
	}	

	public function GetCountBisPersonalNew() {
	}

	public function GetBisPersonalByUser($sUserId,$iPublish,$iPage,$iPerPage) {
		$aFilter=array(	'user_id' => $sUserId );
		
		return $this->GetBisByFilter($aFilter,$iPage,$iPerPage);		
	}
	
	public function GetCountBisPersonalByUser($sUserId,$iPublish) {
	}
	
	public function GetBisCloseByUser($sUserId=null) {
	}
	
	public function GetBisByBlogId($iBlogId,$iPage=0,$iPerPage=0,$aAllowData=array(),$bIdsOnly=true) {
	}
	
	public function GetBisCollective($iPage,$iPerPage,$sShowType='good') {
	}	

	public function GetCountBisCollectiveNew() {
	}

	public function GetBisRatingByDate($sDate,$iLimit=20) {
	}	

	public function GetBisByBlog($oBlog,$iPage,$iPerPage,$sShowType='good') {
	}
	
	public function GetCountBisByBlogNew($oBlog) {
	}

	public function GetBisByTag($sTag,$iPage,$iPerPage,$bAddAccessible=true) {
		
		$s = "";
			
		if (false === ($data = $this->Cache_Get("bi_tag_{$sTag}_{$iPage}_{$iPerPage}_{$s}"))) {			
			$data = array('collection'=>$this->oMapperBi->GetBisByTag($sTag,"",$iCount,$iPage,$iPerPage),'count'=>$iCount);
			$this->Cache_Set($data, "bi_tag_{$sTag}_{$iPage}_{$iPerPage}_{$s}", array('bi_update','bi_new'), 60*60*24*2);
		}
		$data['collection']=$this->GetBisAdditionalData($data['collection']);
		return $data;		
		
	}

	public function GetBiTags($iLimit,$aExcludeBi=array()) {
	}

	public function GetOpenBiTags($iLimit) {
		if (false === ($data = $this->Cache_Get("tag_bi_{$iLimit}_open"))) {			
			$data = $this->oMapperBi->GetOpenBiTags($iLimit);
			$this->Cache_Set($data, "tag_bi_{$iLimit}_open", array('bi_update','bi_new'), 60*60*24*3);
		}
		return $data;		
	}	

	public function GetFavouriteBi($sBiId,$sUserId) {
		return $this->Favourite_GetFavourite($sBiId,'bi',$sUserId);		
	}

	
	public function GetFavouriteBisByArraySolid($aBiId,$sUserId) {
	}

	public function AddFavouriteBi(ModuleFavourite_EntityFavourite $oFavouriteBi) {		
		return $this->Favourite_AddFavourite($oFavouriteBi);		
	}

	public function DeleteFavouriteBi(ModuleFavourite_EntityFavourite $oFavouriteBi) {	
	}

	public function SetFavouriteBiPublish($sBiId,$iPublish) {
	}	

	public function DeleteFavouriteBiByArrayId($aBiId) {
	}

	public function GetBiTagsByLike($sTag,$iLimit) {
	}

	public function SetBiRead(ModuleBi_EntityBiRead $oBiRead) {		
	}	
	
	public function GetBiRead($sBiId,$sUserId) {
	}

	public function DeleteBiReadByArrayId($aBiId) {
	}

	public function GetBisReadByArray($aBiId,$sUserId) {
	}

	public function GetBisReadByArraySolid($aBiId,$sUserId) {
	}

	public function GetBiQuestionVote($sBiId,$sUserId) {
	}

	public function GetBisQuestionVoteByArray($aBiId,$sUserId) {
	}

	public function GetBisQuestionVoteByArraySolid($aBiId,$sUserId) {
	}
	
	public function AddBiQuestionVote(ModuleBi_EntityBiQuestionVote $oBiQuestionVote) {
	}
	
	public function GetBiUnique($sUserId,$sHash) {
	}
	
	public function SendNotifyBiNew($oBlog,$oBi,$oUserBi) {
	}

	public function GetLastBisByUserId($sUserId,$iTimeLimit,$iCountLimit=1,$aAllowData=array()) {
	}
	
	public function MoveBisByArrayId($aBis,$sBlogId) {
	}
	
	public function MoveBis($sBlogId,$sBlogIdNew) {
	}	
	
	public function UploadBiImageFile($aFile,$oUser) {
	}

	public function UploadBiImageUrl($sUrl, $oUser) {
	}
}
?>