<?php

class ModuleBi_MapperBi extends Mapper {	

	public function GetBiRating(&$iCount,$iCurrPage,$iPerPage) {		
		$sql = "SELECT 
					b.bi_id													
				FROM 
					".Config::Get('db.table.bi')." as b 									 
				WHERE 1 									

				ORDER by b.bi_rating desc
				LIMIT ?d, ?d 	";		
		$aReturn=array();
		if ($aRows=$this->oDb->selectPage($iCount,$sql,($iCurrPage-1)*$iPerPage, $iPerPage)) {
			foreach ($aRows as $aRow) {
				$aReturn[]=$aRow['bi_id'];
			}
		}
		return $aReturn;
	}	
	
	public function AddBi(ModuleBi_EntityBi $oBi) {
		
		$sql = "INSERT INTO ".Config::Get('db.table.bi')." 
			(user_id,
			bi_title,			
			bi_tags,
			bi_date_add,
			bi_user_ip,
			bi_publish,
			bi_publish_draft,
			bi_publish_index,
			bi_cut_text,
			bi_forbid_comment,			
			bi_text_hash			
			)
			VALUES(?d,	?,	?,  ?, ?, ?d, ?d, ?d, ?, ?, ?)
		";			
		if ($iId=$this->oDb->query($sql,$oBi->getUserId(), $oBi->getTitle(),
			$oBi->getTags(),$oBi->getDateAdd(),$oBi->getUserIp(),$oBi->getPublish(),$oBi->getPublishDraft(),$oBi->getPublishIndex(),$oBi->getCutText(),$oBi->getForbidComment(),$oBi->getTextHash())) 
		{
			$oBi->setId($iId);
			$this->AddBiContent($oBi);
			return $iId;
		}		
		return false;
	}
	
	public function AddBiContent(ModuleBi_EntityBi $oBi) {
		$sql = "INSERT INTO ".Config::Get('db.table.bi_content')." 
			(bi_id,			
			bi_text,
			bi_text_short,
			bi_text_source,
			bi_extra			
			)
			VALUES(?d,  ?,	?,	?, ? )
		";			
		if ($iId=$this->oDb->query($sql,$oBi->getId(),$oBi->getText(),
			$oBi->getTextShort(),$oBi->getTextSource(),$oBi->getExtra())) 
		{
			return $iId;
		}		
		return false;
	}
	
	public function AddBiTag(ModuleBi_EntityBiTag $oBiTag) {
		$sql = "INSERT INTO ".Config::Get('db.table.bi_tag')." 
			(bi_id,
			user_id,
			bi_tag_text		
			)
			VALUES(?d,  ?d,	?)
		";			
		if ($iId=$this->oDb->query($sql,$oBiTag->getBiId(),$oBiTag->getUserId(),$oBiTag->getText())) 
		{
			return $iId;
		}		
		return false;
	}
	
	
	
	public function DeleteBiTagsByBiId($sBiId) {
		$sql = "DELETE FROM ".Config::Get('db.table.bi_tag')." 
			WHERE
				bi_id = ?d				
		";			
		if ($this->oDb->query($sql,$sBiId)) {
			return true;
		}		
		return false;
	}
	
	public function DeleteBi($sBiId) {
		$sql = "DELETE FROM ".Config::Get('db.table.bi')." 
			WHERE
				bi_id = ?d				
		";			
		if ($this->oDb->query($sql,$sBiId)) {
			return true;
		}		
		return false;
	}
	
		
	public function GetBiUnique($sUserId,$sHash) {
		$sql = "SELECT bi_id FROM ".Config::Get('db.table.bi')." 
			WHERE 				
				bi_text_hash =? 						
				AND
				user_id = ?d
			LIMIT 0,1
				";
		if ($aRow=$this->oDb->selectRow($sql,$sHash,$sUserId)) {
			return $aRow['bi_id'];
		}
		return null;
	}
	
	public function GetBisByArrayId($aArrayId) {
		if (!is_array($aArrayId) or count($aArrayId)==0) {
			return array();
		}
				
		$sql = "SELECT 
					t.*,
					tc.*							 
				FROM 
					".Config::Get('db.table.bi')." as t	
					JOIN  ".Config::Get('db.table.bi_content')." AS tc ON t.bi_id=tc.bi_id				
				WHERE 
					t.bi_id IN(?a) 									
				ORDER BY FIELD(t.bi_id,?a) ";
		$aBis=array();
		if ($aRows=$this->oDb->select($sql,$aArrayId,$aArrayId)) {
			foreach ($aRows as $aBi) {
				$aBis[]=Engine::GetEntity('Bi',$aBi);
			}
		}		
		return $aBis;
	}
	
	
	public function GetBis($aFilter,&$iCount,$iCurrPage,$iPerPage) {
		$sWhere = $this->buildFilter($aFilter);
		
		if(isset($aFilter['order']) and !is_array($aFilter['order'])) {
			$aFilter['order'] = array($aFilter['order']);
		} else {
			$aFilter['order'] = array('t.bi_date_add desc');
		}
		
		$sql = "SELECT 
						t.bi_id							
					FROM 
						".Config::Get('db.table.bi')." as t
					WHERE 
						1=1					
						".$sWhere." 
					ORDER BY ".
						implode(', ', $aFilter['order'])
				."
					LIMIT ?d, ?d";		
		$aBis=array();
		if ($aRows=$this->oDb->selectPage($iCount,$sql,($iCurrPage-1)*$iPerPage, $iPerPage)) {			
			foreach ($aRows as $aBi) {
				$aBis[]=$aBi['bi_id'];
			}
		}				
		return $aBis;
	}
	
	public function GetCountBis($aFilter) {		
		$sWhere=$this->buildFilter($aFilter);
		$sql = "SELECT 
					count(t.bi_id) as count									
				FROM 
					".Config::Get('db.table.bi')." as t 
				WHERE 
					1=1
					
					".$sWhere." ";		
		if ($aRow=$this->oDb->selectRow($sql)) {
			return $aRow['count'];
		}
		return false;
	}
	
	public function GetAllBis($aFilter) {
		$sWhere=$this->buildFilter($aFilter);
		
		$sql = "SELECT 
						t.bi_id							
					FROM 
						".Config::Get('db.table.bi')." as t
					WHERE 
						1=1					
						".$sWhere."
					ORDER by t.bi_id desc";		
		$aBis=array();
		if ($aRows=$this->oDb->select($sql)) {			
			foreach ($aRows as $aBi) {
				$aBis[]=$aBi['bi_id'];
			}
		}		

		return $aBis;		
	}
	
	public function GetBisByTag($sTag,$aExcludeBlog,&$iCount,$iCurrPage,$iPerPage) {		
		$sql = "				
							SELECT 		
								bi_id										
							FROM 
								".Config::Get('db.table.bi_tag')."								
							WHERE 
								bi_tag_text = ? 	
								{ AND blog_id NOT IN (?a) }
                            ORDER BY bi_id DESC	
                            LIMIT ?d, ?d ";
		
		$aBis=array();
		if ($aRows=$this->oDb->selectPage(
				$iCount,$sql,$sTag,
				(is_array($aExcludeBlog)&&count($aExcludeBlog)) ? $aExcludeBlog : DBSIMPLE_SKIP,
				($iCurrPage-1)*$iPerPage, $iPerPage
			)
		) {
			foreach ($aRows as $aBi) {
				$aBis[]=$aBi['bi_id'];
			}
		}
		return $aBis;
	}
	
	
	public function GetBisRatingByDate($sDate,$iLimit,$aExcludeBlog=array()) {
		$sql = "SELECT 
						t.bi_id										
					FROM 
						".Config::Get('db.table.bi')." as t
					WHERE 					
						t.bi_publish = 1
						AND
						t.bi_date_add >= ?
						AND
						t.bi_rating >= 0
						{ AND t.blog_id NOT IN(?a) } 																	
					ORDER by t.bi_rating desc, t.bi_id desc
					LIMIT 0, ?d ";		
		$aBis=array();
		if ($aRows=$this->oDb->select(
				$sql,$sDate,
				(is_array($aExcludeBlog)&&count($aExcludeBlog)) ? $aExcludeBlog : DBSIMPLE_SKIP,
				$iLimit
			)
		) {
			foreach ($aRows as $aBi) {
				$aBis[]=$aBi['bi_id'];
			}
		}
		return $aBis;
	}
	
	public function GetBiTags($iLimit,$aExcludeBi=array()) {
		$sql = "SELECT 
			tt.bi_tag_text,
			count(tt.bi_tag_text)	as count		 
			FROM 
				".Config::Get('db.table.bi_tag')." as tt
			WHERE 
				1=1
				{AND tt.bi_id NOT IN(?a) }		
			GROUP BY 
				tt.bi_tag_text
			ORDER BY 
				count desc		
			LIMIT 0, ?d
				";	
		$aReturn=array();
		$aReturnSort=array();
		if ($aRows=$this->oDb->select(
				$sql,
				(is_array($aExcludeBi)&&count($aExcludeBi)) ? $aExcludeBi : DBSIMPLE_SKIP,
				$iLimit
			)
		) {
			foreach ($aRows as $aRow) {				
				$aReturn[mb_strtolower($aRow['bi_tag_text'],'UTF-8')]=$aRow;
			}
			ksort($aReturn);
			foreach ($aReturn as $aRow) {
				$aReturnSort[]=Engine::GetEntity('Bi_BiTag',$aRow);				
			}
		}
		return $aReturnSort;
	}

	public function GetOpenBiTags($iLimit) {
		$sql = "
			SELECT 
				tt.bi_tag_text,
				count(tt.bi_tag_text)	as count		 
			FROM 
				".Config::Get('db.table.bi_tag')." as tt

			WHERE 1 

			GROUP BY  
				tt.bi_tag_text
			ORDER BY 
				count desc		
			LIMIT 0, ?d
				";	
		$aReturn=array();
		$aReturnSort=array();
		if ($aRows=$this->oDb->select($sql,$iLimit)) {
			foreach ($aRows as $aRow) {				
				$aReturn[mb_strtolower($aRow['bi_tag_text'],'UTF-8')]=$aRow;
			}
			ksort($aReturn);
			foreach ($aReturn as $aRow) {
				$aReturnSort[]=Engine::GetEntity('Bi_BiTag',$aRow);				
			}
		}
		return $aReturnSort;
	}

	public function increaseBiCountComment($sBiId) {
		
		$sql = "UPDATE ".Config::Get('db.table.bi')." SET bi_count_comment=bi_count_comment+1 WHERE bi_id= ? ";
		if ($this->oDb->query($sql,$sBiId)) {
			return true;
		} 
		return false;
	}
	
	public function UpdateBi(ModuleBi_EntityBi $oBi) {		
		$sql = "UPDATE ".Config::Get('db.table.bi')." 
			SET 
				bi_title= ?,				
				bi_tags= ?,
				bi_date_add = ?,
				bi_date_edit = ?,
				bi_user_ip= ?,
				bi_publish= ?d ,
				bi_publish_draft= ?d ,
				bi_publish_index= ?d,
				bi_rating= ?f,
				bi_count_vote= ?d,
				bi_count_read= ?d,
				bi_count_comment= ?d, 
				bi_cut_text = ? ,
				bi_forbid_comment = ? ,
				bi_text_hash = ? 
			WHERE
				bi_id = ?d
		";			
		if ($this->oDb->query($sql,$oBi->getTitle(),$oBi->getTags(),$oBi->getDateAdd(),$oBi->getDateEdit(),$oBi->getUserIp(),$oBi->getPublish(),$oBi->getPublishDraft(),$oBi->getPublishIndex(),$oBi->getRating(),$oBi->getCountVote(),$oBi->getCountRead(),$oBi->getCountComment(),$oBi->getCutText(),$oBi->getForbidComment(),$oBi->getTextHash(),$oBi->getId())) {
			$this->UpdateBiContent($oBi);
			return true;
		}		
		return false;
	}
	
	public function UpdateBiContent(ModuleBi_EntityBi $oBi) {		
		$sql = "UPDATE ".Config::Get('db.table.bi_content')." 
			SET 				
				bi_text= ?,
				bi_text_short= ?,
				bi_text_source= ?,
				bi_extra= ?
			WHERE
				bi_id = ?d
		";			
		if ($this->oDb->query($sql,$oBi->getText(),$oBi->getTextShort(),$oBi->getTextSource(),$oBi->getExtra(),$oBi->getId())) {
			return true;
		}		
		return false;
	}
	
	protected function buildFilter($aFilter) {
		$sWhere='';
		if (isset($aFilter['bi_publish'])) {
			$sWhere.=" AND t.bi_publish =  ".(int)$aFilter['bi_publish'];
		}	
		if (isset($aFilter['bi_rating']) and is_array($aFilter['bi_rating'])) {
			$sPublishIndex='';
			if (isset($aFilter['bi_rating']['publish_index']) and $aFilter['bi_rating']['publish_index']==1) {
				$sPublishIndex=" or bi_publish_index=1 ";
			}
			if ($aFilter['bi_rating']['type']=='top') {
				$sWhere.=" AND ( t.bi_rating >= ".(float)$aFilter['bi_rating']['value']." {$sPublishIndex} ) ";
			} else {
				$sWhere.=" AND ( t.bi_rating < ".(float)$aFilter['bi_rating']['value']."  ) ";
			}			
		}
		if (isset($aFilter['bi_new'])) {
			$sWhere.=" AND t.bi_date_add >=  '".$aFilter['bi_new']."'";
		}
		if (isset($aFilter['user_id'])) {
			$sWhere.=is_array($aFilter['user_id'])
				? " AND t.user_id IN(".implode(', ',$aFilter['user_id']).")"
				: " AND t.user_id =  ".(int)$aFilter['user_id'];
		}
		
		return $sWhere;
	}
	
	public function GetBiTagsByLike($sTag,$iLimit) {
		$sTag=mb_strtolower($sTag,"UTF-8");		
		$sql = "SELECT 
				bi_tag_text					 
			FROM 
				".Config::Get('db.table.bi_tag')."	
			WHERE
				bi_tag_text LIKE ?			
			GROUP BY 
				bi_tag_text					
			LIMIT 0, ?d		
				";	
		$aReturn=array();
		if ($aRows=$this->oDb->select($sql,$sTag.'%',$iLimit)) {
			foreach ($aRows as $aRow) {
				$aReturn[]=Engine::GetEntity('Bi_BiTag',$aRow);
			}
		}
		return $aReturn;
	}
	
	public function UpdateBiRead(ModuleBi_EntityBiRead $oBiRead) {		
		$sql = "UPDATE ".Config::Get('db.table.bi_read')." 
			SET 
				comment_count_last = ? ,
				comment_id_last = ? ,
				date_read = ? 
			WHERE
				bi_id = ? 
				AND				
				user_id = ? 
		";			
		return $this->oDb->query($sql,$oBiRead->getCommentCountLast(),$oBiRead->getCommentIdLast(),$oBiRead->getDateRead(),$oBiRead->getBiId(),$oBiRead->getUserId());
	}	

	public function AddBiRead(ModuleBi_EntityBiRead $oBiRead) {		
		$sql = "INSERT INTO ".Config::Get('db.table.bi_read')." 
			SET 
				comment_count_last = ? ,
				comment_id_last = ? ,
				date_read = ? ,
				bi_id = ? ,							
				user_id = ? 
		";			
		return $this->oDb->query($sql,$oBiRead->getCommentCountLast(),$oBiRead->getCommentIdLast(),$oBiRead->getDateRead(),$oBiRead->getBiId(),$oBiRead->getUserId());
	}
	/**
	 * Удаляет записи о чтении записей по списку идентификаторов
	 *
	 * @param  array $aBiId
	 * @return bool
	 */				
	public function DeleteBiReadByArrayId($aBiId) {
		$sql = "
			DELETE FROM ".Config::Get('db.table.bi_read')." 
			WHERE
				bi_id IN(?a)				
		";			
		if ($this->oDb->query($sql,$aBiId)) {
			return true;
		}
		return false;
	}
			
	public function GetBisReadByArray($aArrayId,$sUserId) {
		if (!is_array($aArrayId) or count($aArrayId)==0) {
			return array();
		}
				
		$sql = "SELECT 
					t.*							 
				FROM 
					".Config::Get('db.table.bi_read')." as t 
				WHERE 
					t.bi_id IN(?a)
					AND
					t.user_id = ?d 
				";
		$aReads=array();
		if ($aRows=$this->oDb->select($sql,$aArrayId,$sUserId)) {
			foreach ($aRows as $aRow) {
				$aReads[]=Engine::GetEntity('Bi_BiRead',$aRow);
			}
		}		
		return $aReads;
	}
	
	public function AddBiQuestionVote(ModuleBi_EntityBiQuestionVote $oBiQuestionVote) {
		$sql = "INSERT INTO ".Config::Get('db.table.bi_question_vote')." 
			(bi_id,
			user_voter_id,
			answer		
			)
			VALUES(?d,  ?d,	?f)
		";			
		if ($this->oDb->query($sql,$oBiQuestionVote->getBiId(),$oBiQuestionVote->getVoterId(),$oBiQuestionVote->getAnswer())===0) 
		{
			return true;
		}		
		return false;
	}
	
		
	public function GetBisQuestionVoteByArray($aArrayId,$sUserId) {
		if (!is_array($aArrayId) or count($aArrayId)==0) {
			return array();
		}
				
		$sql = "SELECT 
					v.*							 
				FROM 
					".Config::Get('db.table.bi_question_vote')." as v 
				WHERE 
					v.bi_id IN(?a)
					AND	
					v.user_voter_id = ?d 				 									
				";
		$aVotes=array();
		if ($aRows=$this->oDb->select($sql,$aArrayId,$sUserId)) {
			foreach ($aRows as $aRow) {
				$aVotes[]=Engine::GetEntity('Bi_BiQuestionVote',$aRow);
			}
		}		
		return $aVotes;
	}
	
	/**
	 * Перемещает топики в другой блог
	 *
	 * @param  array  $aBis
	 * @param  string $sBlogId
	 * @return bool
	 */	
	public function MoveBisByArrayId($aBis,$sBlogId) {
		if(!is_array($aBis)) $aBis = array($aBis);
		
		$sql = "UPDATE ".Config::Get('db.table.bi')."
			SET 
				blog_id= ?d
			WHERE
				bi_id IN(?a)
		";			
		if ($this->oDb->query($sql,$sBlogId,$aBis)) {
			return true;
		}		
		return false;
	}
	
	/**
	 * Перемещает топики в другой блог
	 *
	 * @param  string $sBlogId
	 * @param  string $sBlogIdNew
	 * @return bool
	 */	
	public function MoveBis($sBlogId,$sBlogIdNew) {
		$sql = "UPDATE ".Config::Get('db.table.bi')."
			SET 
				blog_id= ?d
			WHERE
				blog_id = ?d
		";			
		if ($this->oDb->query($sql,$sBlogIdNew,$sBlogId)) {
			return true;
		}		
		return false;
	}
	
	/**
	 * Перемещает теги топиков в другой блог
	 *
	 * @param string $sBlogId
	 * @param string $sBlogIdNew
	 * @return bool
	 */
	public function MoveBisTags($sBlogId,$sBlogIdNew) {
		$sql = "UPDATE ".Config::Get('db.table.bi_tag')."
			SET 
				blog_id= ?d
			WHERE
				blog_id = ?d
		";			
		if ($this->oDb->query($sql,$sBlogIdNew,$sBlogId)) {
			return true;
		}		
		return false;
	}
	
	/**
	 * Перемещает теги топиков в другой блог
	 *
	 * @param array $aBis
	 * @param string $sBlogId
	 * @return bool
	 */
	public function MoveBisTagsByArrayId($aBis,$sBlogId) {
		if(!is_array($aBis)) $aBis = array($aBis);
		
		$sql = "UPDATE ".Config::Get('db.table.bi_tag')."
			SET 
				blog_id= ?d
			WHERE
				bi_id IN(?a)
		";			
		if ($this->oDb->query($sql,$sBlogId,$aBis)) {
			return true;
		}		
		return false;
	}
	
}
?>