<?php
/*-------------------------------------------------------
*
*   LiveStreet Engine Social Networking
*   Copyright © 2008 Mzhelskiy Maxim
*
*--------------------------------------------------------
*
*   Official site: www.livestreet.ru
*   Contact e-mail: rus.engine@gmail.com
*
*   GNU General Public License, version 2:
*   http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
*
---------------------------------------------------------
*/

/**
 * Модуль управления рейтингами и силой 
 *
 */
class ModuleRating extends Module {

	/**
	 * �?нициализация модуля
	 *
	 */
	public function Init() {		
		
	}
	/**
	 * Расчет рейтинга при голосовании за комментарий
	 *
	 * @param ModuleUser_EntityUser $oUser
	 * @param CommentEntity_TopicComment $oComment
	 */
	public function VoteComment(ModuleUser_EntityUser $oUser, ModuleComment_EntityComment $oComment, $iValue) {
		/**
		 * Устанавливаем рейтинг комментария
		 */
		$oComment->setRating($oComment->getRating()+$iValue);
		/**
		 * Начисляем силу автору коммента, используя логарифмическое распределение
		 */		
		$skill=$oUser->getSkill();
		$iMinSize=0.004;
		$iMaxSize=0.5;
		$iSizeRange=$iMaxSize-$iMinSize;
		$iMinCount=log(0+1);
		$iMaxCount=log(500+1);
		$iCountRange=$iMaxCount-$iMinCount;
		if ($iCountRange==0) {
			$iCountRange=1;
		}		
		if ($skill>50 and $skill<200) {
			$skill_new=$skill/70;
		} elseif ($skill>=200) {
			$skill_new=$skill/10;
		} else {
			$skill_new=$skill/130;
		}
		$iDelta=$iMinSize+(log($skill_new+1)-$iMinCount)*($iSizeRange/$iCountRange);
		/**
		 * Сохраняем силу
		 */
		$oUserComment=$this->User_GetUserById($oComment->getUserId());
		$iSkillNew=$oUserComment->getSkill()+$iValue*$iDelta;
		$iSkillNew=($iSkillNew<0) ? 0 : $iSkillNew;
		$oUserComment->setSkill($iSkillNew);
		$this->User_Update($oUserComment);
		return $iValue;
	}
	
	public function VoteBb(ModuleUser_EntityUser $oUser, ModuleBb_EntityBb $oBb, $iValue) {
		
		$skill=$oUser->getSkill();

		$iDeltaRating=$iValue;
		if ($skill>=100 and $skill<250) {
			$iDeltaRating=$iValue*2;
		} elseif ($skill>=250 and $skill<400) {
			$iDeltaRating=$iValue*3;
		} elseif ($skill>=400) {
			$iDeltaRating=$iValue*4;
		}
		
		$oBb->setRating($oBb->getRating()+$iDeltaRating);

		$iMinSize=0.1;
		$iMaxSize=8;
		$iSizeRange=$iMaxSize-$iMinSize;
		$iMinCount=log(0+1);
		$iMaxCount=log(500+1);
		$iCountRange=$iMaxCount-$iMinCount;
		if ($iCountRange==0) {
			$iCountRange=1;
		}		
		if ($skill>50 and $skill<200) {
			$skill_new=$skill/70;
		} elseif ($skill>=200) {
			$skill_new=$skill/10;
		} else {
			$skill_new=$skill/100;
		}
		$iDelta=$iMinSize+(log($skill_new+1)-$iMinCount)*($iSizeRange/$iCountRange);

		$oUserBb=$this->User_GetUserById($oBb->getUserId());
		$iSkillNew=$oUserBb->getSkill()+$iValue*$iDelta;
		$iSkillNew=($iSkillNew<0) ? 0 : $iSkillNew;
		$oUserBb->setSkill($iSkillNew);
		$oUserBb->setRating($oUserBb->getRating()+$iValue*$iDelta/2.73);
		$this->User_Update($oUserBb);
		
		return $iDeltaRating;
	}	
	
	public function VoteBi(ModuleUser_EntityUser $oUser, ModuleBi_EntityBi $oBi, $iValue) {
		$skill=$oUser->getSkill();

		$iDeltaRating=$iValue;
		if ($skill>=100 and $skill<250) {
			$iDeltaRating=$iValue*2;
		} elseif ($skill>=250 and $skill<400) {
			$iDeltaRating=$iValue*3;
		} elseif ($skill>=400) {
			$iDeltaRating=$iValue*4;
		}
		$oBi->setRating($oBi->getRating()+$iDeltaRating);

		$iMinSize=0.1;
		$iMaxSize=8;
		$iSizeRange=$iMaxSize-$iMinSize;
		$iMinCount=log(0+1);
		$iMaxCount=log(500+1);
		$iCountRange=$iMaxCount-$iMinCount;
		if ($iCountRange==0) {
			$iCountRange=1;
		}		
		if ($skill>50 and $skill<200) {
			$skill_new=$skill/70;
		} elseif ($skill>=200) {
			$skill_new=$skill/10;
		} else {
			$skill_new=$skill/100;
		}
		$iDelta=$iMinSize+(log($skill_new+1)-$iMinCount)*($iSizeRange/$iCountRange);

		$oUserBi=$this->User_GetUserById($oBi->getUserId());
		$iSkillNew=$oUserBi->getSkill()+$iValue*$iDelta;
		$iSkillNew=($iSkillNew<0) ? 0 : $iSkillNew;
		$oUserBi->setSkill($iSkillNew);
		$oUserBi->setRating($oUserBi->getRating()+$iValue*$iDelta/2.73);
		$this->User_Update($oUserBi);
		return $iDeltaRating;
	}	
	
	public function VoteBs(ModuleUser_EntityUser $oUser, ModuleBs_EntityBs $oBs, $iValue) {
		$skill=$oUser->getSkill();

		$iDeltaRating=$iValue;
		if ($skill>=100 and $skill<250) {
			$iDeltaRating=$iValue*2;
		} elseif ($skill>=250 and $skill<400) {
			$iDeltaRating=$iValue*3;
		} elseif ($skill>=400) {
			$iDeltaRating=$iValue*4;
		}
		$oBs->setRating($oBs->getRating()+$iDeltaRating);

		$iMinSize=0.1;
		$iMaxSize=8;
		$iSizeRange=$iMaxSize-$iMinSize;
		$iMinCount=log(0+1);
		$iMaxCount=log(500+1);
		$iCountRange=$iMaxCount-$iMinCount;
		if ($iCountRange==0) {
			$iCountRange=1;
		}		
		if ($skill>50 and $skill<200) {
			$skill_new=$skill/70;
		} elseif ($skill>=200) {
			$skill_new=$skill/10;
		} else {
			$skill_new=$skill/100;
		}
		$iDelta=$iMinSize+(log($skill_new+1)-$iMinCount)*($iSizeRange/$iCountRange);

		$oUserBs=$this->User_GetUserById($oBs->getUserId());
		$iSkillNew=$oUserBs->getSkill()+$iValue*$iDelta;
		$iSkillNew=($iSkillNew<0) ? 0 : $iSkillNew;
		$oUserBs->setSkill($iSkillNew);
		$oUserBs->setRating($oUserBs->getRating()+$iValue*$iDelta/2.73);
		$this->User_Update($oUserBs);
		return $iDeltaRating;
	}	
	
	public function VoteBa(ModuleUser_EntityUser $oUser, ModuleBa_EntityBa $oBa, $iValue) {
		$skill=$oUser->getSkill();

		$iDeltaRating=$iValue;
		if ($skill>=100 and $skill<250) {
			$iDeltaRating=$iValue*2;
		} elseif ($skill>=250 and $skill<400) {
			$iDeltaRating=$iValue*3;
		} elseif ($skill>=400) {
			$iDeltaRating=$iValue*4;
		}
		$oBa->setRating($oBa->getRating()+$iDeltaRating);

		$iMinSize=0.1;
		$iMaxSize=8;
		$iSizeRange=$iMaxSize-$iMinSize;
		$iMinCount=log(0+1);
		$iMaxCount=log(500+1);
		$iCountRange=$iMaxCount-$iMinCount;
		if ($iCountRange==0) {
			$iCountRange=1;
		}		
		if ($skill>50 and $skill<200) {
			$skill_new=$skill/70;
		} elseif ($skill>=200) {
			$skill_new=$skill/10;
		} else {
			$skill_new=$skill/100;
		}
		$iDelta=$iMinSize+(log($skill_new+1)-$iMinCount)*($iSizeRange/$iCountRange);

		$oUserBa=$this->User_GetUserById($oBa->getUserId());
		$iSkillNew=$oUserBa->getSkill()+$iValue*$iDelta;
		$iSkillNew=($iSkillNew<0) ? 0 : $iSkillNew;
		$oUserBa->setSkill($iSkillNew);
		$oUserBa->setRating($oUserBa->getRating()+$iValue*$iDelta/2.73);
		$this->User_Update($oUserBa);
		return $iDeltaRating;
	}	
	
	
	public function VoteQa(ModuleUser_EntityUser $oUser, ModuleQa_EntityQa $oQa, $iValue) {
		$skill=$oUser->getSkill();
		/**
		 * Устанавливаем рейтинг топика
		 */
		$iDeltaRating=$iValue;
		if ($skill>=100 and $skill<250) {
			$iDeltaRating=$iValue*2;
		} elseif ($skill>=250 and $skill<400) {
			$iDeltaRating=$iValue*3;
		} elseif ($skill>=400) {
			$iDeltaRating=$iValue*4;
		}
		$oQa->setRating($oQa->getRating()+$iDeltaRating);
		/**
		 * Начисляем силу и рейтинг автору топика, используя логарифмическое распределение
		 */			
		$iMinSize=0.1;
		$iMaxSize=8;
		$iSizeRange=$iMaxSize-$iMinSize;
		$iMinCount=log(0+1);
		$iMaxCount=log(500+1);
		$iCountRange=$iMaxCount-$iMinCount;
		if ($iCountRange==0) {
			$iCountRange=1;
		}		
		if ($skill>50 and $skill<200) {
			$skill_new=$skill/70;
		} elseif ($skill>=200) {
			$skill_new=$skill/10;
		} else {
			$skill_new=$skill/100;
		}
		$iDelta=$iMinSize+(log($skill_new+1)-$iMinCount)*($iSizeRange/$iCountRange);
		/**
		 * Сохраняем силу и рейтинг
		 */
		$oUserQa=$this->User_GetUserById($oQa->getUserId());
		$iSkillNew=$oUserQa->getSkill()+$iValue*$iDelta;
		$iSkillNew=($iSkillNew<0) ? 0 : $iSkillNew;
		$oUserQa->setSkill($iSkillNew);
		$oUserQa->setRating($oUserQa->getRating()+$iValue*$iDelta/2.73);
		$this->User_Update($oUserQa);
		return $iDeltaRating;
	}	
	
	
	/**
	 * Расчет рейтинга и силы при гоосовании за топик
	 *
	 * @param ModuleUser_EntityUser $oUser
	 * @param ModuleTopic_EntityTopic $oTopic
	 * @param unknown_type $iValue
	 */
	public function VoteTopic(ModuleUser_EntityUser $oUser, ModuleTopic_EntityTopic $oTopic, $iValue) {
		$skill=$oUser->getSkill();
		/**
		 * Устанавливаем рейтинг топика
		 */
		$iDeltaRating=$iValue;
		if ($skill>=100 and $skill<250) {
			$iDeltaRating=$iValue*2;
		} elseif ($skill>=250 and $skill<400) {
			$iDeltaRating=$iValue*3;
		} elseif ($skill>=400) {
			$iDeltaRating=$iValue*4;
		}
		$oTopic->setRating($oTopic->getRating()+$iDeltaRating);
		/**
		 * Начисляем силу и рейтинг автору топика, используя логарифмическое распределение
		 */			
		$iMinSize=0.1;
		$iMaxSize=8;
		$iSizeRange=$iMaxSize-$iMinSize;
		$iMinCount=log(0+1);
		$iMaxCount=log(500+1);
		$iCountRange=$iMaxCount-$iMinCount;
		if ($iCountRange==0) {
			$iCountRange=1;
		}		
		if ($skill>50 and $skill<200) {
			$skill_new=$skill/70;
		} elseif ($skill>=200) {
			$skill_new=$skill/10;
		} else {
			$skill_new=$skill/100;
		}
		$iDelta=$iMinSize+(log($skill_new+1)-$iMinCount)*($iSizeRange/$iCountRange);
		/**
		 * Сохраняем силу и рейтинг
		 */
		$oUserTopic=$this->User_GetUserById($oTopic->getUserId());
		$iSkillNew=$oUserTopic->getSkill()+$iValue*$iDelta;
		$iSkillNew=($iSkillNew<0) ? 0 : $iSkillNew;
		$oUserTopic->setSkill($iSkillNew);
		$oUserTopic->setRating($oUserTopic->getRating()+$iValue*$iDelta/2.73);
		$this->User_Update($oUserTopic);
		return $iDeltaRating;
	}
	/**
	 * Расчет рейтинга и силы при голосовании за блог
	 *
	 * @param ModuleUser_EntityUser $oUser
	 * @param ModuleBlog_EntityBlog $oBlog
	 * @param unknown_type $iValue
	 */
	public function VoteBlog(ModuleUser_EntityUser $oUser, ModuleBlog_EntityBlog $oBlog, $iValue) {		
		/**
		 * Устанавливаем рейтинг блога, используя логарифмическое распределение
		 */		
		$skill=$oUser->getSkill();	
		$iMinSize=1.13;
		$iMaxSize=15;
		$iSizeRange=$iMaxSize-$iMinSize;
		$iMinCount=log(0+1);
		$iMaxCount=log(500+1);
		$iCountRange=$iMaxCount-$iMinCount;
		if ($iCountRange==0) {
			$iCountRange=1;
		}		
		if ($skill>50 and $skill<200) {
			$skill_new=$skill/20;
		} elseif ($skill>=200) {
			$skill_new=$skill/10;
		} else {
			$skill_new=$skill/50;
		}
		$iDelta=$iMinSize+(log($skill_new+1)-$iMinCount)*($iSizeRange/$iCountRange);
		/**
		 * Сохраняем рейтинг
		 */
		$oBlog->setRating($oBlog->getRating()+$iValue*$iDelta);
		return $iValue*$iDelta;
	}
	/**
	 * Расчет рейтинга и силы при голосовании за пользователя
	 *
	 * @param ModuleUser_EntityUser $oUser
	 * @param ModuleUser_EntityUser $oUserTarget
	 * @param unknown_type $iValue
	 */
	public function VoteUser(ModuleUser_EntityUser $oUser, ModuleUser_EntityUser $oUserTarget, $iValue) {		
		/**
		 * Начисляем силу и рейтинг юзеру, используя логарифмическое распределение
		 */			
		$skill=$oUser->getSkill();
		$iMinSize=0.42;
		$iMaxSize=3.2;
		$iSizeRange=$iMaxSize-$iMinSize;
		$iMinCount=log(0+1);
		$iMaxCount=log(500+1);
		$iCountRange=$iMaxCount-$iMinCount;
		if ($iCountRange==0) {
			$iCountRange=1;
		}		
		if ($skill>50 and $skill<200) {
			$skill_new=$skill/40;
		} elseif ($skill>=200) {
			$skill_new=$skill/2;
		} else {
			$skill_new=$skill/70;
		}
		$iDelta=$iMinSize+(log($skill_new+1)-$iMinCount)*($iSizeRange/$iCountRange);
		/**
		 * Сохраняем силу и рейтинг
		 */		
		$iRatingNew=$oUserTarget->getRating()+$iValue*$iDelta;		
		//$iSkillNew=$oUserTarget->getSkill()+$iValue*$iDelta/3.67;
		//$iSkillNew=($iSkillNew<0) ? 0 : $iSkillNew;		
		//$oUserTarget->setSkill($iSkillNew);
		$oUserTarget->setRating($iRatingNew);
		///$this->User_Update($oUserTarget);
		return $iValue*$iDelta;
	}
}
?>