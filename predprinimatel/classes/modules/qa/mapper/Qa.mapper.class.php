<?php

class ModuleQa_MapperQa extends Mapper {	
	protected $oUserCurrent=null;
	
	public function SetUserCurrent($oUserCurrent)  {
		$this->oUserCurrent=$oUserCurrent;
	}
	
	public function GetOpenQaTags($iLimit) {
		$sql = "
			SELECT 
				tt.qa_tag_text,
				count(tt.qa_tag_text)	as count		 
			FROM 
				".Config::Get('db.table.qa_tag')." as tt,
				".Config::Get('db.table.qa')." as b
			WHERE 
				tt.qa_id = b.qa_id 
			GROUP BY 
				tt.qa_tag_text
			ORDER BY 
				count desc		
			LIMIT 0, ?d
				";	
		$aReturn=array();
		$aReturnSort=array();
		if ($aRows=$this->oDb->select($sql,$iLimit)) {
			foreach ($aRows as $aRow) {				
				$aReturn[mb_strtolower($aRow['qa_tag_text'],'UTF-8')]=$aRow;
			}
			ksort($aReturn);
			foreach ($aReturn as $aRow) {
				$aReturnSort[]=Engine::GetEntity('Qa_QaTag',$aRow);				
			}
		}
		return $aReturnSort;
	}	
	
	public function increaseQaCountComment($sQaId) {
		
		
		$sql = "UPDATE ".Config::Get('db.table.qa')." 
			SET 
				qa_count_comment=qa_count_comment+1
			WHERE
				qa_id = ?
		";
		if ($this->oDb->query($sql,$sQaId)) {
			
			return true;
		}		
		return false;
	}	
	
	public function AddQa(ModuleQa_EntityQa $oQa) {
		$sql = "INSERT INTO ".Config::Get('db.table.qa')." 
			(user_owner_id,
			qa_title,
			qa_tags,
			qa_description,
			qa_type,			
			qa_date_add,
			qa_limit_rating_topic,
			qa_url,
			qa_avatar
			)
			VALUES(?d,  ?,	?, ?, ?,	?,	?, ?, ?)
		";			
		if ($iId=$this->oDb->query($sql,$oQa->getOwnerId(),$oQa->getTitle(),$oQa->getTags(),$oQa->getDescription(),$oQa->getType(),$oQa->getDateAdd(),$oQa->getLimitRatingTopic(),$oQa->getUrl(),$oQa->getAvatar())) {
			return $iId;
		}		
		return false;
	}
	
	public function GetCountQas($aFilter) {		
		
		$sWhere=$this->buildFilter($aFilter);
		$sql = "SELECT 
					count(t.qa_id) as count									
				FROM 
					".Config::Get('db.table.qa')." as t
				WHERE 
					1=1 ".$sWhere." ";
				
		if ($aRow=$this->oDb->selectRow($sql)) {
			return $aRow['count'];
		}
		return false;
	}	
	
	public function UpdateQa(ModuleQa_EntityQa $oQa) {		
		
		$sql = "UPDATE ".Config::Get('db.table.qa')." 
			SET 
				qa_title= ?,
				qa_tags= ?,
				qa_description= ?,
				qa_type= ?,
				qa_date_edit= ?,
				qa_rating= ?f,
				qa_count_vote = ?d,
				qa_count_user= ?d,
				qa_limit_rating_topic= ?f ,
				qa_url= ?,
				qa_avatar= ?
			WHERE
				qa_id = ?d
		";			
		if ($this->oDb->query($sql,$oQa->getTitle(),$oQa->getTags(),$oQa->getDescription(),$oQa->getType(),$oQa->getDateEdit(),$oQa->getRating(),$oQa->getCountVote(),$oQa->getCountUser(),$oQa->getLimitRatingTopic(),$oQa->getUrl(),$oQa->getAvatar(),$oQa->getId())) {
			return true;
		}		
		return false;
		
	}
	
	public function GetQasByArrayId($aArrayId) {
		if (!is_array($aArrayId) or count($aArrayId)==0) {
			return array();
		}
				
		$sql = "SELECT 
					b.*							 
				FROM 
					".Config::Get('db.table.qa')." as b					
				WHERE 
					b.qa_id IN(?a) 								
				ORDER BY FIELD(b.qa_id,?a) ";
		$aQas=array();
		if ($aRows=$this->oDb->select($sql,$aArrayId,$aArrayId)) {
			foreach ($aRows as $aQa) {
				$aQas[]=Engine::GetEntity('Qa',$aQa);
			}
		}
		return $aQas;
	}	
	
	public function AddRelationQaUser(ModuleQa_EntityQaUser $oQaUser) {
		$sql = "INSERT INTO ".Config::Get('db.table.qa_user')." 
			(qa_id,
			user_id,
			user_role
			)
			VALUES(?d,  ?d, ?d)
		";			
		if ($this->oDb->query($sql,$oQaUser->getQaId(),$oQaUser->getUserId(),$oQaUser->getUserRole())===0) {
			return true;
		}		
		return false;
	}
	
	public function DeleteRelationQaUser(ModuleQa_EntityQaUser $oQaUser) {
		$sql = "DELETE FROM ".Config::Get('db.table.qa_user')." 
			WHERE
				qa_id = ?d
				AND
				user_id = ?d
		";			
		if ($this->oDb->query($sql,$oQaUser->getQaId(),$oQaUser->getUserId())) {
			return true;
		}		
		return false;
	}
		
	public function UpdateRelationQaUser(ModuleQa_EntityQaUser $oQaUser) {		
		$sql = "UPDATE ".Config::Get('db.table.qa_user')." 
			SET 
				user_role = ?d			
			WHERE
				qa_id = ?d 
				AND
				user_id = ?d
		";			
		if ($this->oDb->query($sql,$oQaUser->getUserRole(),$oQaUser->getQaId(),$oQaUser->getUserId())) {
			return true;
		}		
		return false;
	}
	
	public function GetQaUsers($aFilter) {
		$sWhere=' 1=1 ';
		if (isset($aFilter['qa_id'])) {
			$sWhere.=" AND bu.qa_id =  ".(int)$aFilter['qa_id'];
		}
		if (isset($aFilter['user_id'])) {
			$sWhere.=" AND bu.user_id =  ".(int)$aFilter['user_id'];
		}
		if (isset($aFilter['user_role'])) {
			if(!is_array($aFilter['user_role'])) {
				$aFilter['user_role']=array($aFilter['user_role']);
			}
			$sWhere.=" AND bu.user_role IN ('".join("', '",$aFilter['user_role'])."')";		
		} else {
			$sWhere.=" AND bu.user_role>".ModuleQa::BLOG_USER_ROLE_GUEST;
		}
		
		$sql = "SELECT
					bu.*				
				FROM 
					".Config::Get('db.table.qa_user')." as bu
				WHERE 
					".$sWhere." 					
				;
					";		
		$aQaUsers=array();
		if ($aRows=$this->oDb->select($sql)) {
			foreach ($aRows as $aUser) {
				$aQaUsers[]=Engine::GetEntity('Qa_QaUser',$aUser);
			}
		}
		return $aQaUsers;
	}

	public function GetQaUsersByArrayQa($aArrayId,$sUserId) {	
		if (!is_array($aArrayId) or count($aArrayId)==0) {
			return array();
		}
			
		$sql = "SELECT 
					bu.*				
				FROM 
					".Config::Get('db.table.qa_user')." as bu
				WHERE 
					bu.qa_id IN(?a) 					
					AND
					bu.user_id = ?d ";		
		$aQaUsers=array();
		if ($aRows=$this->oDb->select($sql,$aArrayId,$sUserId)) {
			foreach ($aRows as $aUser) {
				$aQaUsers[]=Engine::GetEntity('Qa_QaUser',$aUser);
			}
		}
		return $aQaUsers;
	}
	
		
	public function GetPersonalQaByUserId($sUserId) {
		$sql = "SELECT qa_id FROM ".Config::Get('db.table.qa')." WHERE user_owner_id = ?d and qa_type='personal'";
		if ($aRow=$this->oDb->selectRow($sql,$sUserId)) {
			return $aRow['qa_id'];
		}
		return null;
	}
	
		
	public function GetQaByTitle($sTitle) {
		$sql = "SELECT qa_id FROM ".Config::Get('db.table.qa')." WHERE qa_title = ? ";
		if ($aRow=$this->oDb->selectRow($sql,$sTitle)) {
			return $aRow['qa_id'];
		}
		return null;
	}
	

	
	
	public function GetQaByUrl($sUrl) {		
		$sql = "SELECT 
				b.qa_id 
			FROM 
				".Config::Get('db.table.qa')." as b
			WHERE 
				b.qa_url = ? 		
				";
		if ($aRow=$this->oDb->selectRow($sql,$sUrl)) {
			return $aRow['qa_id'];
		}
		return null;
	}
	
	public function GetQasByOwnerId($sUserId) {
		$sql = "SELECT 
			b.qa_id			 
			FROM 
				".Config::Get('db.table.qa')." as b				
			WHERE 
				b.user_owner_id = ? 
				AND
				b.qa_type<>'personal'				
				";	
		$aQas=array();
		if ($aRows=$this->oDb->select($sql,$sUserId)) {
			foreach ($aRows as $aQa) {
				$aQas[]=$aQa['qa_id'];
			}
		}
		return $aQas;
	}
	
	public function GetQas($aFilter, &$iCount,$iCurrPage,$iPerPage) {
		$sWhere=$this->buildFilter($aFilter);
		$sql = "SELECT 
			t.qa_id			 
			FROM 
				".Config::Get('db.table.qa')." as t				
			WHERE 				
				1=1 ".$sWhere."				
				ORDER BY t.qa_date_add desc LIMIT ?d, ?d ";	
		
		$aQas=array();
		if ($aRows=$this->oDb->selectPage($iCount,$sql,($iCurrPage-1)*$iPerPage, $iPerPage)) {
			foreach ($aRows as $aQa) {
				$aQas[]=$aQa['qa_id'];
			}
		}
		return $aQas;
	}
		
	public function GetQaRating(&$iCount,$iCurrPage,$iPerPage) {		
		$sql = "SELECT 
					b.qa_id													
				FROM 
					".Config::Get('db.table.qa')." as b 									 
				WHERE 									
					b.qa_type<>'personal'									
				ORDER by b.qa_rating desc
				LIMIT ?d, ?d 	";		
		$aReturn=array();
		if ($aRows=$this->oDb->selectPage($iCount,$sql,($iCurrPage-1)*$iPerPage, $iPerPage)) {
			foreach ($aRows as $aRow) {
				$aReturn[]=$aRow['qa_id'];
			}
		}
		return $aReturn;
	}
	
	public function GetQasRatingJoin($sUserId,$iLimit) {		
		$sql = "SELECT 
					b.*													
				FROM 
					".Config::Get('db.table.qa_user')." as bu,
					".Config::Get('db.table.qa')." as b	
				WHERE 	
					bu.user_id = ?d
					AND
					bu.qa_id = b.qa_id
					AND				
					b.qa_type<>'personal'							
				ORDER by b.qa_rating desc
				LIMIT 0, ?d 
				;	
					";		
		$aReturn=array();
		if ($aRows=$this->oDb->select($sql,$sUserId,$iLimit)) {
			foreach ($aRows as $aRow) {
				$aReturn[]=Engine::GetEntity('Qa',$aRow);
			}
		}
		return $aReturn;
	}
	
	public function GetQasRatingSelf($sUserId,$iLimit) {		
		$sql = "SELECT 
					b.*													
				FROM 					
					".Config::Get('db.table.qa')." as b	
				WHERE 						
					b.user_owner_id = ?d
					AND				
					b.qa_type<>'personal'													
				ORDER by b.qa_rating desc
				LIMIT 0, ?d 
			;";		
		$aReturn=array();
		if ($aRows=$this->oDb->select($sql,$sUserId,$iLimit)) {
			foreach ($aRows as $aRow) {
				$aReturn[]=Engine::GetEntity('Qa',$aRow);
			}
		}
		return $aReturn;
	}
	
	public function GetCloseQas() {
		$sql = "SELECT b.qa_id										
				FROM ".Config::Get('db.table.qa')." as b					
				WHERE b.qa_type='close'
			;";
		$aReturn=array();
		if ($aRows=$this->oDb->select($sql)) {
			foreach ($aRows as $aRow) {
				$aReturn[]=$aRow['qa_id'];
			}
		}
		return $aReturn;
	}
	
	/**
	 * Удаление блога из базы данных
	 *
	 * @param  int  $iQaId
	 * @return bool	 
	 */
	public function DeleteQa($iQaId) {
		$sql = "
			DELETE FROM ".Config::Get('db.table.qa')." 
			WHERE qa_id = ?d				
		";			
		if ($this->oDb->query($sql,$iQaId)) {
			return true;
		}
		return false;
	}
	
	/**
	 * Удалить пользователей блога по идентификатору блога
	 *
	 * @param  int  $iQaId
	 * @return bool
	 */
	public function DeleteQaUsersByQaId($iQaId) {
		$sql = "
			DELETE FROM ".Config::Get('db.table.qa_user')." 
			WHERE qa_id = ?d
		";
		if ($this->oDb->query($sql,$iQaId)) {
			return true;
		}
		return false;
	}
	
	protected function buildFilter($aFilter) {
		$sWhere='';

		if (isset($aFilter['user_id'])) {
			$sWhere.=is_array($aFilter['user_id'])
				? " AND t.user_owner_id IN(".implode(', ',$aFilter['user_id']).")"
				: " AND t.user_owner_id =  ".(int)$aFilter['user_id'];
		}

		return $sWhere;
	}
	
	
	public function GetAllQas($aFilter) {
		
		$sWhere=$this->buildFilter($aFilter);
		
		$sql = "SELECT 
						t.qa_id							
					FROM 
						".Config::Get('db.table.qa')." as t
					WHERE 
						1=1			
						".$sWhere." 		
					ORDER by t.qa_id desc";		
		
		$aQas=array();
		if ($aRows=$this->oDb->select($sql)) {			
			foreach ($aRows as $aQa) {
				$aQas[]=$aQa['qa_id'];
			}
		}		

		return $aQas;		
	}	
	
	public function GetQasByTag($sTag,&$iCount,$iCurrPage,$iPerPage) {		
		
		$aQas = array();
		
		$sql = "				
							SELECT 		
								qa_id										
							FROM 
								".Config::Get('db.table.qa_tag')."								
							WHERE 
								qa_tag_text = ? 	
								
                            ORDER BY qa_id DESC	
                            LIMIT ?d, ?d ";
		
		$aTopics=array();
		if ($aRows=$this->oDb->selectPage(
				$iCount,$sql,$sTag,
				($iCurrPage-1)*$iPerPage, $iPerPage
			)
		) {
			foreach ($aRows as $aQa) {
				$aQas[]=$aQa['qa_id'];
			}
		}
		return $aQas;
	}	
	
	public function AddQaTag(ModuleQa_EntityQaTag $oQaTag) {
		$sql = "INSERT INTO ".Config::Get('db.table.qa_tag')." 
			(qa_id,
			user_id,
			qa_tag_text		
			)
			VALUES(?d,  ?d,  	?)
		";			
		if ($iId=$this->oDb->query($sql,$oQaTag->getQaId(),$oQaTag->getUserId(), $oQaTag->getText())) 
		{
			return $iId;
		}		
		return false;
	}

	public function DeleteQaTagsByQaId($sQaId) {
		$sql = "DELETE FROM ".Config::Get('db.table.qa_tag')." 
			WHERE
				qa_id = ?d				
		";			
		if ($this->oDb->query($sql,$sQaId)) {
			return true;
		}		
		return false;
	}	
	
	
}
?>