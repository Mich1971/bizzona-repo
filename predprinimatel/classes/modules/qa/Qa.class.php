<?php

class ModuleQa extends Module {
	
	protected $oMapperQa;	
	protected $oUserCurrent=null;	
	
	public function Init() {
		$this->oMapperQa=Engine::GetMapper(__CLASS__);
		$this->oMapperQa->SetUserCurrent($this->User_GetUserCurrent());
		$this->oUserCurrent=$this->User_GetUserCurrent();		
	}	

	public function GetOpenQaTags($iLimit) {
		if (false === ($data = $this->Cache_Get("tag_qa_{$iLimit}_open"))) {			
			$data = $this->oMapperQa->GetOpenQaTags($iLimit);
			$this->Cache_Set($data, "tag_qa_{$iLimit}_open", array('qa_update','qa_new'), 60*60*24*3);
		}
		return $data;
	}	
	
	public function GetQasPersonalByUser($sUserId,$iPublish,$iPage,$iPerPage) {
		
		$aFilter=array(	'user_id' => $sUserId );
		
		return $this->GetQasByFilter($aFilter,$iPage,$iPerPage);

	}	
	
	public function GetFavouriteQa($sQaId,$sUserId) {
		return $this->Favourite_GetFavourite($sQaId,'qa',$sUserId);
	}	
	
	public function AddFavouriteQa(ModuleFavourite_EntityFavourite $oFavouriteTopic) {		
		return $this->Favourite_AddFavourite($oFavouriteTopic);
	}	
	
	public function GetQaRating($iCurrPage,$iPerPage) {		
		
		if (false === ($data = $this->Cache_Get("qa_rating_{$iCurrPage}_{$iPerPage}"))) {				
			$data = array('collection'=>$this->oMapperQa->GetQaRating($iCount,$iCurrPage,$iPerPage),'count'=>$iCount);
			$this->Cache_Set($data, "qa_rating_{$iCurrPage}_{$iPerPage}", array("qa_update","qa_new"), 60*60*24*2);
		}
				 
		$data['collection'] = $this->GetQasAdditionalData($data['collection']);
		return $data; 
	}

	public function increaseQaCountComment($sQaId) {
		$this->Cache_Delete("qa_{$sQaId}");
		$this->Cache_Clean(Zend_Cache::CLEANING_MODE_MATCHING_TAG,array("qa_update"));
		return $this->oMapperQa->increaseQaCountComment($sQaId);		
		
	}	
	
	public function DeleteQaTagsByQaId($sQaId) {
		return $this->oMapperQa->DeleteQaTagsByQaId($sQaId);
	}	
	
	public function UpdateQa(ModuleQa_EntityQa $oQa) {
		
		$oQaOld=$this->GetQaById($oQa->getId());
		$oQa->setDateEdit(date("Y-m-d H:i:s"));
		if ($this->oMapperQa->UpdateQa($oQa)) {	

			$aTags=explode(',',$oQa->getTags());
			
			$this->DeleteQaTagsByQaId($oQa->getId());
			
				
			foreach ($aTags as $sTag) {
				$oTag=Engine::GetEntity('Qa_QaTag');
				$oTag->setQaId($oQa->getId());
				$oTag->setUserId($oQa->getUserId());
				$oTag->setText($sTag);
				$this->oMapperQa->AddQaTag($oTag);
			}
			
			
			$this->Cache_Delete("qa_{$oQa->getId()}");
			return true;
		}
		return false;
	}	
	
	public function GetQasByArrayIdSolid($aQaId) {
		
		if (!is_array($aQaId)) {
			$aQaId=array($aQaId);
		}
		$aQaId=array_unique($aQaId);	
		$aQas=array();	
		$s=join(',',$aQaId);
		if (false === ($data = $this->Cache_Get("qa_id_{$s}"))) {			
			$data = $this->oMapperQa->GetQasByArrayId($aQaId);
			foreach ($data as $oQa) {
				$aQas[$oQa->getId()]=$oQa;
			}
			$this->Cache_Set($aQas, "qa_id_{$s}", array("qa_update"), 60*60*24*1);
			return $aQas;
		}		
		return $data;
		
	}	
	
	public function GetQaById($sId) {		
		$aQas=$this->GetQasAdditionalData($sId);
		if (isset($aQas[$sId])) {
			return $aQas[$sId];
		}
		return null;
	}	
	
	public function GeQaByArrayId($aQaId) {
		
		
		if (!$aQaId) {
			return array();
		}
		if (Config::Get('sys.cache.solid')) {
			return $this->GetQasByArrayIdSolid($aQaId);
		}
		if (!is_array($aQaId)) {
			$aQaId=array($aQaId);
		}
		$aQaId=array_unique($aQaId);
		$aQas=array();
		$aQaIdNotNeedQuery=array();

		$aCacheKeys=func_build_cache_keys($aQaId,'qa_');
		if (false !== ($data = $this->Cache_Get($aCacheKeys))) {			
			foreach ($aCacheKeys as $sValue => $sKey ) {
				if (array_key_exists($sKey,$data)) {	
					if ($data[$sKey]) {
						$aQas[$data[$sKey]->getId()]=$data[$sKey];
					} else {
						$aQaIdNotNeedQuery[]=$sValue;
					}
				} 
			}
		}

		$aQaIdNeedQuery=array_diff($aQaId,array_keys($aQas));		
		$aQaIdNeedQuery=array_diff($aQaIdNeedQuery,$aQaIdNotNeedQuery);		
		$aQaIdNeedStore=$aQaIdNeedQuery;
		if ($data = $this->oMapperQa->GetQasByArrayId($aQaIdNeedQuery)) {
			foreach ($data as $oQa) {
				$aQas[$oQa->getId()]=$oQa;
				$this->Cache_Set($oQa, "qa_{$oQa->getId()}", array(), 60*60*24*4);
				$aQaIdNeedStore=array_diff($aQaIdNeedStore,array($oQa->getId()));
			}
		}

		foreach ($aQaIdNeedStore as $sId) {
			$this->Cache_Set(null, "qa_{$sId}", array(), 60*60*24*4);
		}		

		$aQas=func_array_sort_by_keys($aQas,$aQaId);
		return $aQas;		
		
	}	
	
	public function AddQa(ModuleQa_EntityQa $oQa) {
		
		if ($sId=$this->oMapperQa->AddQa($oQa)) {
			
			$oQa->setId($sId);
			
			$aTags=explode(',',$oQa->getTags());
			
			foreach ($aTags as $sTag) {
				$oTag=Engine::GetEntity('Qa_QaTag');
				$oTag->setQaId($oQa->getId());
				$oTag->setUserId($oQa->getUserId());
				$oTag->setText($sTag);
				$this->oMapperQa->AddQaTag($oTag);
			}			
 
			return $sId;
		}
		return false;
		
	}	
	
	public function SetQaRead(ModuleQa_EntityQaRead $oQaRead) {		
		/*
		if ($this->GetQaRead($oQaRead->getQaId(),$oQaRead->getUserId())) {
			$this->Cache_Delete("qa_read_{$oQaRead->getQaId()}_{$oQaRead->getUserId()}");
			$this->Cache_Clean(Zend_Cache::CLEANING_MODE_MATCHING_TAG,array("qa_read_user_{$oQaRead->getUserId()}"));
			$this->oMapperQa->UpdateQaRead($oQaRead);
		} else {
			$this->Cache_Delete("qa_read_{$oQaRead->getQaId()}_{$oQaRead->getUserId()}");
			$this->Cache_Clean(Zend_Cache::CLEANING_MODE_MATCHING_TAG,array("qa_read_user_{$oQaRead->getUserId()}"));
			$this->oMapperQa->AddQaRead($oQaRead);
		}
		*/
		return true;		
	}	
	
	public function GetQaRead($sQaId,$sUserId) {
		/*
		$data=$this->GetQasReadByArray($sQaId,$sUserId);
		if (isset($data[$sQaId])) {
			return $data[$sQaId];
		}
		return null;
		*/
	}	
	
	public function DeleteQa($oQaId) {
		if ($oQaId instanceof ModuleQa_EntityQa) {
			$sQaId=$oQaId->getId();
			$this->Cache_Clean(Zend_Cache::CLEANING_MODE_MATCHING_TAG,array("qa_update_user_{$oQaId->getUserId()}"));
		} else {
			$sQaId=$oQaId;
		}

		$this->Cache_Clean(Zend_Cache::CLEANING_MODE_MATCHING_TAG,array('qa_update'));
		$this->Cache_Delete("qa_{$sQaId}");

		if($bResult=$this->oMapperQa->DeleteQa($sQaId)){
			return $this->DeleteQaAdditionalData($sQaId);
		}

		return false;
	}	
	
	public function DeleteQaAdditionalData($iQaId) {

		$this->Cache_Clean(Zend_Cache::CLEANING_MODE_MATCHING_TAG,array('qa_update'));
		$this->Cache_Delete("topic_{$iQaId}");

		$this->Comment_DeleteCommentByTargetId($iQaId,'qa');
		
		return true;
	}	
	
	public function GetQasByFilter($aFilter, $iPage=0,$iPerPage=0, $aAllowData=array('user'=>array(),'blog'=>array('owner'=>array(),'relation_user'),'vote','favourite','comment_new')) {
		
		$s = serialize($aFilter);
		//echo $iPage*$iPerPage;
		//$iCount = 0;
		if (false === ($data = $this->Cache_Get("qa_filter_{$s}_{$iPage}_{$iPerPage}"))) {
			$data = ($iPage*$iPerPage!=0) 
				? array(
							'collection'=>$this->oMapperQa->GetQas($aFilter,$iCount,$iPage,$iPerPage),
							'count'=>$iCount
					 	)
				: array(
							'collection'=>$this->oMapperQa->GetAllQas($aFilter),
							'count'=>$this->GetCountQasByFilter($aFilter)
					 	);	 	
			$this->Cache_Set($data, "qa_filter_{$s}_{$iPage}_{$iPerPage}", array('qa_update','qa_new'), 60*60*24*3);
		}
		$data['collection']=$this->GetQasAdditionalData($data['collection'], $aAllowData);
		
		return $data;
	}	
	
	public function GetCountQasByFilter($aFilter) {
		$s=serialize($aFilter);;					
		if (false === ($data = $this->Cache_Get("qa_count_{$s}"))) {			
			$data = $this->oMapperQa->GetCountQas($aFilter);
			$this->Cache_Set($data, "qa_count_{$s}", array('qa_update','qa_new'), 60*60*24*1);
		}
		return 	$data;
	}	
	
	public function GetQasAdditionalData($aQaId, $aAllowData=array('user'=>array(),'qa'=>array('owner'=>array(),'relation_user'),'vote','favourite')) {
		
		$aAllowData['favourite'] = true;
		
		if (!is_array($aQaId)) {
			$aQaId=array($aQaId);
		}		
		
		$aQas=$this->GetQasByArrayId($aQaId);
		
		$aUserId=array();
		$aQasVote=array();
		$aFavouriteQas=array();
		
		foreach ($aQas as $oQa) {
			if (isset($aAllowData['user'])) {
				$aUserId[]=$oQa->getUserId();
			}
		}		
		
		$aUsers=isset($aAllowData['user']) && is_array($aAllowData['user']) ? $this->User_GetUsersAdditionalData($aUserId,$aAllowData['user']) : $this->User_GetUsersAdditionalData($aUserId);

		if (isset($aAllowData['vote']) and $this->oUserCurrent) {
			$aQasVote=$this->Vote_GetVoteByArray($aQaId,'qa',$this->oUserCurrent->getId());
		}		
		
		if (isset($aAllowData['favourite']) and $this->oUserCurrent) {
			$aFavouriteQas=$this->GetFavouriteQasByArray($aQaId,$this->oUserCurrent->getId());	
		}		

		
		foreach ($aQas as $oQa) {

			if (isset($aUsers[$oQa->getUserId()])) {
				$oQa->setUser($aUsers[$oQa->getUserId()]);
			} else {
				$oQa->setUser(null);
			}			
			
			if (isset($aQasVote[$oQa->getId()])) {
				$oQa->setVote($aQasVote[$oQa->getId()]);				
			} else {
				$oQa->setVote(null);
			}			
			
			if (isset($aFavouriteQas[$oQa->getId()])) {
				$oQa->setIsFavourite(true);
			} else {
				$oQa->setIsFavourite(false);
			}			
			
		}		
		
		return $aQas;
	}

	public function GetFavouriteQasByArray($aQaId,$sUserId) {
		return $this->Favourite_GetFavouritesByArray($aQaId,'qa',$sUserId);
	}	
	
	public function GetQasByArrayId($aQaId) {
		
		if (!$aQaId) {
			return array();
		}		
		
		$aQas=array();
		$aQaIdNotNeedQuery=array();
		$aCacheKeys=func_build_cache_keys($aQaId,'Qa_');
		if (false !== ($data = $this->Cache_Get($aCacheKeys))) {			
			/**
			 * проверяем что досталось из кеша
			 */
			foreach ($aCacheKeys as $sValue => $sKey ) {
				if (array_key_exists($sKey,$data)) {	
					if ($data[$sKey]) {
						$aQas[$data[$sKey]->getId()]=$data[$sKey];
					} else {
						$aQaIdNotNeedQuery[]=$sValue;
					}
				} 
			}
		}		
		
		$aQaIdNeedQuery=array_diff($aQaId,array_keys($aQas));		
		$aQaIdNeedQuery=array_diff($aQaIdNeedQuery,$aQaIdNotNeedQuery);		
		$aQaIdNeedStore=$aQaIdNeedQuery;		
		
		if ($data = $this->oMapperQa->GetQasByArrayId($aQaIdNeedQuery)) {
			foreach ($data as $oQa) {
				/**
				 * Добавляем к результату и сохраняем в кеш
				 */
				$aQas[$oQa->getId()]=$oQa;
				$this->Cache_Set($oQa, "Qa_{$oQa->getId()}", array(), 60*60*24*4);
				$aQaIdNeedStore=array_diff($aQaIdNeedStore,array($oQa->getId()));
			}
		}
		foreach ($aQaIdNeedStore as $sId) {
			$this->Cache_Set(null, "Qa_{$sId}", array(), 60*60*24*4);
		}				
		
		$aQas=func_array_sort_by_keys($aQas,$aQaId);
		
		return $aQas;
	}
	
	public function GetQasByTag($sTag,$iPage,$iPerPage,$bAddAccessible=true) {
		/*
		$aCloseBlogs = ($this->oUserCurrent && $bAddAccessible) 
			? $this->Blog_GetInaccessibleBlogsByUser($this->oUserCurrent)
			: $this->Blog_GetInaccessibleBlogsByUser();
		
		$s = serialize($aCloseBlogs);	
		*/
		$s = "";
		
		if (false === ($data = $this->Cache_Get("qa_tag_{$sTag}_{$iPage}_{$iPerPage}_{$s}"))) {			
			$data = array('collection'=>$this->oMapperQa->GetQasByTag($sTag,$iCount,$iPage,$iPerPage),'count'=>$iCount);
			$this->Cache_Set($data, "qa_tag_{$sTag}_{$iPage}_{$iPerPage}_{$s}", array('qa_update','qa_new'), 60*60*24*2);
		}
		$data['collection']=$this->GetQasAdditionalData($data['collection']);
		return $data;		
		
	}	
	
	public function GetQasFavouriteByUserId($sUserId,$iCurrPage,$iPerPage) {		

		$data = $this->Favourite_GetFavouritesByUserId($sUserId,'qa',$iCurrPage,$iPerPage);

		$data['collection']=$this->GetQasAdditionalData($data['collection']);
		return $data;		
	}	
	
}

?>