<?php

class ModuleBa extends Module {		
	protected $oMapperBa;
	protected $oUserCurrent=null;
		
	public function Init() {		
		$this->oMapperBa=Engine::GetMapper(__CLASS__);
		$this->oUserCurrent=$this->User_GetUserCurrent();
	}

	public function GetBasFavouriteByUserId($sUserId,$iCurrPage,$iPerPage) {		

		$data = $this->Favourite_GetFavouritesByUserId($sUserId,'ba',$iCurrPage,$iPerPage);

		$data['collection']=$this->GetBasAdditionalData($data['collection']);
		return $data;		
	}	
	
	
	public function increaseBaCountComment($sBaId) {
		
		$this->Cache_Delete("ba_{$sBaId}");
		$this->Cache_Clean(Zend_Cache::CLEANING_MODE_MATCHING_TAG,array("ba_update"));
		return $this->oMapperBa->increaseBaCountComment($sBaId);		
		
	}	
	
	public function GetBaRating($iCurrPage,$iPerPage) {		
		
		if (false === ($data = $this->Cache_Get("ba_rating_{$iCurrPage}_{$iPerPage}"))) {				
			$data = array('collection'=>$this->oMapperBa->GetBaRating($iCount,$iCurrPage,$iPerPage),'count'=>$iCount);
			$this->Cache_Set($data, "ba_rating_{$iCurrPage}_{$iPerPage}", array("ba_update","ba_new"), 60*60*24*2);
		}
				 
		$data['collection'] = $this->GetBasAdditionalData($data['collection']);
		return $data; 
	}	
	
	public function GetBasAdditionalData($aBaId,$aAllowData=array('user'=>array(),'blog'=>array('owner'=>array(),'relation_user'),'vote','favourite','comment_new')) {

		$aAllowData['favourite'] = true;
		
		func_array_simpleflip($aAllowData);
		if (!is_array($aBaId)) {
			$aBaId=array($aBaId);
		}

		$aBas=$this->GetBasByArrayId($aBaId);
		
		$aUserId=array();
		$aBasVote=array();
		$aFavouriteBas=array();

		foreach ($aBas as $oBa) {
			if (isset($aAllowData['user'])) {
				$aUserId[]=$oBa->getUserId();
			}			
		}
		
		$aUsers=isset($aAllowData['user']) && is_array($aAllowData['user']) ? $this->User_GetUsersAdditionalData($aUserId,$aAllowData['user']) : $this->User_GetUsersAdditionalData($aUserId);

		if (isset($aAllowData['vote']) and $this->oUserCurrent) {
			$aBasVote=$this->Vote_GetVoteByArray($aBaId,'ba',$this->oUserCurrent->getId());
		}		
		
		if (isset($aAllowData['favourite']) and $this->oUserCurrent) {
			$aFavouriteBas=$this->GetFavouriteBasByArray($aBaId,$this->oUserCurrent->getId());	
		}		
		
		foreach ($aBas as $oBa) {

			if (isset($aUsers[$oBa->getUserId()])) {
				$oBa->setUser($aUsers[$oBa->getUserId()]);
			} else {
				$oBa->setUser(null);
			}
						
			if (isset($aBasVote[$oBa->getId()])) {
				$oBa->setVote($aBasVote[$oBa->getId()]);				
			} else {
				$oBa->setVote(null);
			}			

			if (isset($aFavouriteBas[$oBa->getId()])) {
				$oBa->setIsFavourite(true);
			} else {
				$oBa->setIsFavourite(false);
			}			
			
		}		
		
		return $aBas;		
		
	}

	public function GetFavouriteBasByArray($aBaId,$sUserId) {
		return $this->Favourite_GetFavouritesByArray($aBaId,'ba',$sUserId);
	}	
	
	public function AddBa(ModuleBa_EntityBa $oBa) {
		
		if ($sId=$this->oMapperBa->AddBa($oBa)) {
			
			$oBa->setId($sId);
			
			$aTags=explode(',',$oBa->getTags());
			foreach ($aTags as $sTag) {
				$oTag=Engine::GetEntity('Ba_BaTag');
				$oTag->setBaId($oBa->getId());
				$oTag->setUserId($oBa->getUserId());
				$oTag->setText($sTag);
				$this->oMapperBa->AddBaTag($oTag);
			}
			
			$this->Cache_Clean(Zend_Cache::CLEANING_MODE_MATCHING_TAG,array('ba_new',"ba_update_user_{$oBa->getUserId()}",""));						
			return $oBa;
		}
		return false;
		
	}
	
	public function DeleteBaTagsByBaId($sBaId) {
		
		return $this->oMapperBa->DeleteBaTagsByBaId($sBaId);
		
	}	
	
	public function DeleteBa($oBaId) {
		
		if ($oBaId instanceof ModuleBa_EntityBa) {
			$sBaId=$oBaId->getId();
			$this->Cache_Clean(Zend_Cache::CLEANING_MODE_MATCHING_TAG,array("ba_update_user_{$oBaId->getUserId()}"));
		} else {
			$sBaId=$oBaId;
		}

		$this->Cache_Clean(Zend_Cache::CLEANING_MODE_MATCHING_TAG,array('ba_update'));
		$this->Cache_Delete("ba_{$sBaId}");

		if($bResult=$this->oMapperBa->DeleteBa($sBaId)){
			return $this->DeleteBaAdditionalData($sBaId);
		}

		return false;
		
	}

	public function DeleteBaAdditionalData($iBaId) {
		
		$this->Cache_Clean(Zend_Cache::CLEANING_MODE_MATCHING_TAG,array('ba_update'));
		$this->Cache_Delete("ba_{$iBaId}");

		$this->Comment_DeleteCommentByTargetId($iBaId,'ba');

		$this->DeleteBaTagsByBaId($iBaId);
		
		return true;		
		
	}

	public function UpdateBa(ModuleBa_EntityBa $oBa) {
		
		$oBaOld=$this->GetBaById($oBa->getId());
		$oBa->setDateEdit(date("Y-m-d H:i:s"));
		if ($this->oMapperBa->UpdateBa($oBa)) {	

			$aTags=explode(',',$oBa->getTags());
			$this->DeleteBaTagsByBaId($oBa->getId());
				
			foreach ($aTags as $sTag) {
				$oTag=Engine::GetEntity('Ba_BaTag');
				$oTag->setBaId($oBa->getId());
				$oTag->setUserId($oBa->getUserId());
				$oTag->setText($sTag);
				$this->oMapperBa->AddBaTag($oTag);
			}

			$this->Cache_Clean(Zend_Cache::CLEANING_MODE_MATCHING_TAG,array('ba_update',"ba_update_user_{$oBa->getUserId()}",""));
			$this->Cache_Delete("ba_{$oBa->getId()}");
			return true;
		}
		return false;		
		
	}	
		
	public function GetBaById($sId) {		
		
		$aBas=$this->GetBasAdditionalData($sId);
		
		if (isset($aBas[$sId])) {
			return $aBas[$sId];
		}
		return null;		
		
	}	

	public function GetBasByArrayId($aBaId) {
		
		if (!$aBaId) {
			return array();
		}
		if (Config::Get('sys.cache.solid')) {
			return $this->GetBasByArrayIdSolid($aBaId);
		}
		
		if (!is_array($aBaId)) {
			$aBaId=array($aBaId);
		}
		$aBaId=array_unique($aBaId);
		$aBas=array();
		$aBaIdNotNeedQuery=array();

		$aCacheKeys=func_build_cache_keys($aBaId,'ba_');
		if (false !== ($data = $this->Cache_Get($aCacheKeys))) {			
			foreach ($aCacheKeys as $sValue => $sKey ) {
				if (array_key_exists($sKey,$data)) {	
					if ($data[$sKey]) {
						$aBas[$data[$sKey]->getId()]=$data[$sKey];
					} else {
						$aBaIdNotNeedQuery[]=$sValue;
					}
				} 
			}
		}

		$aBaIdNeedQuery=array_diff($aBaId,array_keys($aBas));		
		$aBaIdNeedQuery=array_diff($aBaIdNeedQuery,$aBaIdNotNeedQuery);		
		$aBaIdNeedStore=$aBaIdNeedQuery;
		if ($data = $this->oMapperBa->GetBasByArrayId($aBaIdNeedQuery)) {
			foreach ($data as $oBa) {
				$aBas[$oBa->getId()]=$oBa;
				$this->Cache_Set($oBa, "ba_{$oBa->getId()}", array(), 60*60*24*4);
				$aBaIdNeedStore=array_diff($aBaIdNeedStore,array($oBa->getId()));
			}
		}

		foreach ($aBaIdNeedStore as $sId) {
			$this->Cache_Set(null, "ba_{$sId}", array(), 60*60*24*4);
		}	

		$aBas=func_array_sort_by_keys($aBas,$aBaId);
		return $aBas;		
		
	}

	public function GetBasByArrayIdSolid($aBaId) {
		
		if (!is_array($aBaId)) {
			$aBaId=array($aBaId);
		}
		$aBaId=array_unique($aBaId);	
		$aBas=array();	
		$s=join(',',$aBaId);
		if (false === ($data = $this->Cache_Get("ba_id_{$s}"))) {			
			$data = $this->oMapperBa->GetBasByArrayId($aBaId);
			foreach ($data as $oBa) {
				$aBas[$oBa->getId()]=$oBa;
			}
			$this->Cache_Set($aBas, "ba_id_{$s}", array("ba_update"), 60*60*24*1);
			return $aBas;
		}		
		return $data;		
		
	}

	public function GetCountBasFavouriteByUserId($sUserId) {
	}

	public function GetBasByFilter($aFilter,$iPage=0,$iPerPage=0,$aAllowData=array('user'=>array(),'blog'=>array('owner'=>array(),'relation_user'),'vote','favourite','comment_new')) {
				
		//$iCount = 0;
		
		$s=serialize($aFilter);
		if (false === ($data = $this->Cache_Get("ba_filter_{$s}_{$iPage}_{$iPerPage}"))) {			
			$data = ($iPage*$iPerPage!=0) 
				? array(
						'collection'=>$this->oMapperBa->GetBas($aFilter,$iCount,$iPage,$iPerPage),
						'count'=>$iCount
					)
				: array(
						'collection'=>$this->oMapperBa->GetAllBas($aFilter),
						'count'=>$this->GetCountBasByFilter($aFilter)
					);
			$this->Cache_Set($data, "ba_filter_{$s}_{$iPage}_{$iPerPage}", array('ba_update','ba_new'), 60*60*24*3);
		}
		$data['collection']=$this->GetBasAdditionalData($data['collection'],$aAllowData);
		return $data;		
		
	}

	public function GetCountBasByFilter($aFilter) {
		$s=serialize($aFilter);;					
		if (false === ($data = $this->Cache_Get("ba_count_{$s}"))) {			
			$data = $this->oMapperBa->GetCountBas($aFilter);
			$this->Cache_Set($data, "ba_count_{$s}", array('ba_update','qa_new'), 60*60*24*1);
		}
		return 	$data;		
	}

	public function GetBasGood($iPage,$iPerPage,$bAddAccessible=true) {
		
		
		$aFilter=array(
			'blog_type' => array(
				'personal',
				'open'
			),
			'topic_publish' => 1,
			'topic_rating'  => array(
				'value' => Config::Get('module.blog.index_good'),
				'type'  => 'top',
				'publish_index'  => 1,
			)
		);	

		return $this->GetBasByFilter($aFilter,$iPage,$iPerPage);		
		
	}

	public function GetBasNew($iPage,$iPerPage,$bAddAccessible=true) {
	}

	public function GetBasLast($iCount) {		
	}

	public function GetBasPersonal($iPage,$iPerPage,$sShowType='good') {
	}	

	public function GetCountBasPersonalNew() {
	}

	public function GetBasPersonalByUser($sUserId,$iPublish,$iPage,$iPerPage) {
		$aFilter=array(	'user_id' => $sUserId );
		
		return $this->GetBasByFilter($aFilter,$iPage,$iPerPage);
		
	}
	
	public function GetCountBasPersonalByUser($sUserId,$iPublish) {
	}
	
	public function GetBasCloseByUser($sUserId=null) {
	}
	
	public function GetBasByBlogId($iBlogId,$iPage=0,$iPerPage=0,$aAllowData=array(),$bIdsOnly=true) {
	}
	
	public function GetBasCollective($iPage,$iPerPage,$sShowType='good') {
	}	

	public function GetCountBasCollectiveNew() {
	}

	public function GetBasRatingByDate($sDate,$iLimit=20) {
	}	

	public function GetBasByBlog($oBlog,$iPage,$iPerPage,$sShowType='good') {
	}
	
	public function GetCountBasByBlogNew($oBlog) {
	}

	public function GetBasByTag($sTag,$iPage,$iPerPage,$bAddAccessible=true) {
		
		$s = "";
			
		if (false === ($data = $this->Cache_Get("ba_tag_{$sTag}_{$iPage}_{$iPerPage}_{$s}"))) {			
			$data = array('collection'=>$this->oMapperBa->GetBasByTag($sTag,"",$iCount,$iPage,$iPerPage),'count'=>$iCount);
			$this->Cache_Set($data, "ba_tag_{$sTag}_{$iPage}_{$iPerPage}_{$s}", array('ba_update','ba_new'), 60*60*24*2);
		}
		$data['collection']=$this->GetBasAdditionalData($data['collection']);
		return $data;		
		
	}

	public function GetBaTags($iLimit,$aExcludeBa=array()) {
	}

	public function GetOpenBaTags($iLimit) {
		if (false === ($data = $this->Cache_Get("tag_ba_{$iLimit}_open"))) {			
			$data = $this->oMapperBa->GetOpenBaTags($iLimit);
			$this->Cache_Set($data, "tag_ba_{$iLimit}_open", array('ba_update','ba_new'), 60*60*24*3);
		}
		return $data;		
	}	

	public function GetFavouriteBa($sBaId,$sUserId) {
		return $this->Favourite_GetFavourite($sBaId,'ba',$sUserId);
	}

	public function GetFavouriteBasByArraySolid($aBaId,$sUserId) {
	}

	public function AddFavouriteBa(ModuleFavourite_EntityFavourite $oFavouriteBa) {		
		return $this->Favourite_AddFavourite($oFavouriteBa);		
	}

	public function DeleteFavouriteBa(ModuleFavourite_EntityFavourite $oFavouriteBa) {	
	}

	public function SetFavouriteBaPublish($sBaId,$iPublish) {
	}	

	public function DeleteFavouriteBaByArrayId($aBaId) {
	}

	public function GetBaTagsByLike($sTag,$iLimit) {
	}

	public function SetBaRead(ModuleBa_EntityBaRead $oBaRead) {		
	}	
	
	public function GetBaRead($sBaId,$sUserId) {
	}

	public function DeleteBaReadByArrayId($aBaId) {
	}

	public function GetBasReadByArray($aBaId,$sUserId) {
	}

	public function GetBasReadByArraySolid($aBaId,$sUserId) {
	}

	public function GetBaQuestionVote($sBaId,$sUserId) {
	}

	public function GetBasQuestionVoteByArray($aBaId,$sUserId) {
	}

	public function GetBasQuestionVoteByArraySolid($aBaId,$sUserId) {
	}
	
	public function AddBaQuestionVote(ModuleBa_EntityBaQuestionVote $oBaQuestionVote) {
	}
	
	public function GetBaUnique($sUserId,$sHash) {
	}
	
	public function SendNotifyBaNew($oBlog,$oBa,$oUserBa) {
	}

	public function GetLastBasByUserId($sUserId,$iTimeLimit,$iCountLimit=1,$aAllowData=array()) {
	}
	
	public function MoveBasByArrayId($aBas,$sBlogId) {
	}
	
	public function MoveBas($sBlogId,$sBlogIdNew) {
	}	
	
	public function UploadBaImageFile($aFile,$oUser) {
	}

	public function UploadBaImageUrl($sUrl, $oUser) {
	}
}
?>