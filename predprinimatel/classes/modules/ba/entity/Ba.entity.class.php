<?php

class ModuleBa_EntityBa extends Entity 
{    
	protected $aExtra=null;
	
    public function getId() {
        return $this->_aData['ba_id'];
    }  
    public function getUserId() {
        return $this->_aData['user_owner_id'];
    }
    public function getText() {
        return $this->_aData['ba_text'];
    }    
    public function getTags() {
        return $this->_aData['ba_tags'];
    }
    public function getDateAdd() {
        return $this->_aData['ba_date_add'];
    }
    public function getDateEdit() {
        return $this->_aData['ba_date_edit'];
    }
    public function getUserIp() {
        return $this->_aData['ba_user_ip'];
    }
    public function getRating() {            
        return number_format(round($this->_aData['ba_rating'],2), 0, '.', '');
    }
    public function getCountVote() {
        return $this->_aData['ba_count_vote'];
    }
    public function getCountRead() {
        return $this->_aData['ba_count_read'];
    }
    public function getCountComment() {
        return $this->_aData['ba_count_comment'];
    }
    
    public function getTagsArray() {
    	return explode(',',$this->getTags());    	
    } 
    public function getCountCommentNew() {
        return $this->_aData['count_comment_new'];
    }  
    public function getDateRead() {
        return $this->_aData['date_read'];
    }  
    public function getUser() {
        return $this->_aData['user'];
    }
    
    public function getUrl() {
    	return Router::GetPath('ba').$this->getId().'.html';
    }
    
    public function getVote() {
        return $this->_aData['vote'];
    }    
    public function setVote($data) {
        $this->_aData['vote']=$data;
    }    
    public function getUserQuestionIsVote() {
        return $this->_aData['user_question_is_vote'];
    }	
    public function getIsFavourite() {
        return $this->_aData['ba_is_favourite'];
    }
    
    protected function extractExtra() {
    	if (is_null($this->aExtra)) {
    		$this->aExtra=unserialize($this->getExtra());
    	}
    }
    public function getLinkUrl($bShort=false) {
    }    
    public function setLinkUrl($data) {
    }
    public function getLinkCountJump() {
    }
    public function setLinkCountJump($data) {
    }
    
	public function setId($data) {
        $this->_aData['ba_id']=$data;
    }
    public function setUserId($data) {
        $this->_aData['user_owner_id']=$data;
    }
    public function setText($data) {
        $this->_aData['ba_text']=$data;
    }    
    public function setTags($data) {
        $this->_aData['ba_tags']=$data;
    }
    public function setDateAdd($data) {
        $this->_aData['ba_date_add']=$data;
    }
    public function setDateEdit($data) {
        $this->_aData['ba_date_edit']=$data;
    }
    public function setUserIp($data) {
        $this->_aData['ba_user_ip']=$data;
    }
    public function setRating($data) {
        $this->_aData['ba_rating']=$data;
    }
    public function setCountVote($data) {
        $this->_aData['ba_count_vote']=$data;
    }
    public function setCountRead($data) {
        $this->_aData['ba_count_read']=$data;
    }
    public function setCountComment($data) {
        $this->_aData['ba_count_comment']=$data;
    }
    public function setCountCommentNew($data) {
        $this->_aData['count_comment_new']=$data;
    }
    public function setDateRead($data) {
        $this->_aData['date_read']=$data;
    }
    
    public function getForbidComment() {
        return 0;
    }    
    public function setIsFavourite($data) {
        $this->_aData['ba_is_favourite']=$data;
    }    
}
?>