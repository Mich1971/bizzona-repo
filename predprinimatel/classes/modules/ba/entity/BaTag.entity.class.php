<?php

class ModuleBa_EntityBaTag extends Entity 
{    
    public function getId() {
        return $this->_aData['ba_tag_id'];
    }  
    public function getBaId() {
        return $this->_aData['ba_id'];
    }
    public function getUserId() {
        return $this->_aData['user_id'];
    }
    public function getText() {
        return $this->_aData['ba_tag_text'];
    }
    
    public function getCount() {
        return $this->_aData['count'];
    }
    public function getSize() {
        return $this->_aData['size'];
    }

  
    
	public function setId($data) {
        $this->_aData['ba_tag_id']=$data;
    }
    public function setBaId($data) {
        $this->_aData['ba_id']=$data;
    }
    public function setUserId($data) {
        $this->_aData['user_id']=$data;
    }
    public function setText($data) {
        $this->_aData['ba_tag_text']=$data;
    }
    
	public function setSize($data) {
        $this->_aData['size']=$data;
    }
}
?>