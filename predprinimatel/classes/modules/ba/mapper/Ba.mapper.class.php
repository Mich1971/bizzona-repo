<?php

class ModuleBa_MapperBa extends Mapper {	
		
	public function AddBa(ModuleBa_EntityBa $oBa) {
		
		$sql = "INSERT INTO ".Config::Get('db.table.ba')." 
			(user_owner_id,
			ba_text,			
			ba_tags,
			ba_date_add
			)
			VALUES(?d,	?,	?,  ?)
		";			
		if ($iId=$this->oDb->query($sql,$oBa->getUserId(), $oBa->getText(),	$oBa->getTags(),$oBa->getDateAdd())) {
			return $iId;
		}		
		return false;
	}
	
	public function AddBaTag(ModuleBa_EntityBaTag $oBaTag) {
		$sql = "INSERT INTO ".Config::Get('db.table.ba_tag')." 
			(ba_id,
			user_id,
			ba_tag_text		
			)
			VALUES(?d,  ?d,	?)
		";			
		if ($iId=$this->oDb->query($sql,$oBaTag->getBaId(),$oBaTag->getUserId(),$oBaTag->getText())) 
		{
			return $iId;
		}		
		return false;
	}
	
	
	
	public function DeleteBaTagsByBaId($sBaId) {
		$sql = "DELETE FROM ".Config::Get('db.table.ba_tag')." 
			WHERE
				ba_id = ?d				
		";			
		if ($this->oDb->query($sql,$sBaId)) {
			return true;
		}		
		return false;
	}
	
	public function DeleteBa($sBaId) {
		$sql = "DELETE FROM ".Config::Get('db.table.ba')." 
			WHERE
				ba_id = ?d				
		";			
		if ($this->oDb->query($sql,$sBaId)) {
			return true;
		}		
		return false;
	}
	
	public function GetBasByArrayId($aArrayId) {
		if (!is_array($aArrayId) or count($aArrayId)==0) {
			return array();
		}
				
		$sql = "SELECT 
					t.*
				FROM 
					".Config::Get('db.table.ba')." as t 		
				WHERE 
					t.ba_id IN(?a) 									
				ORDER BY FIELD(t.ba_id,?a) ";
		$aBas=array();
		if ($aRows=$this->oDb->select($sql,$aArrayId,$aArrayId)) {
			foreach ($aRows as $aBa) {
				$aBas[]=Engine::GetEntity('Ba',$aBa);
			}
		}		
		return $aBas;
	}
	
	
	public function GetBas($aFilter,&$iCount,$iCurrPage,$iPerPage) {
		$sWhere = $this->buildFilter($aFilter);
		
		if(isset($aFilter['order']) and !is_array($aFilter['order'])) {
			$aFilter['order'] = array($aFilter['order']);
		} else {
			$aFilter['order'] = array('t.ba_date_add desc');
		}
		
		$sql = "SELECT 
						t.ba_id							
					FROM 
						".Config::Get('db.table.ba')." as t
					WHERE 
						1=1					
						".$sWhere." 
					ORDER BY ".
						implode(', ', $aFilter['order'])
				."
					LIMIT ?d, ?d";		
		$aBas=array();
		if ($aRows=$this->oDb->selectPage($iCount,$sql,($iCurrPage-1)*$iPerPage, $iPerPage)) {			
			foreach ($aRows as $aBa) {
				$aBas[]=$aBa['ba_id'];
			}
		}				
		return $aBas;
	}
	
	public function GetCountBas($aFilter) {		
		$sWhere=$this->buildFilter($aFilter);
		$sql = "SELECT 
					count(t.ba_id) as count									
				FROM 
					".Config::Get('db.table.ba')." as t 
				WHERE 
					1=1
					
					".$sWhere." ";		
		if ($aRow=$this->oDb->selectRow($sql)) {
			return $aRow['count'];
		}
		return false;
	}
	
	public function GetAllBas($aFilter) {
		$sWhere=$this->buildFilter($aFilter);
		
		$sql = "SELECT 
						t.ba_id							
					FROM 
						".Config::Get('db.table.ba')." as t
					WHERE 
						1=1					
						".$sWhere."
					ORDER by t.ba_id desc";		
		$aBas=array();
		if ($aRows=$this->oDb->select($sql)) {			
			foreach ($aRows as $aBa) {
				$aBas[]=$aBa['ba_id'];
			}
		}		

		return $aBas;		
	}
	
	public function GetBasByTag($sTag,$aExcludeBlog,&$iCount,$iCurrPage,$iPerPage) {		
		$sql = "				
							SELECT 		
								ba_id										
							FROM 
								".Config::Get('db.table.ba_tag')."								
							WHERE 
								ba_tag_text = ? 	
								{ AND blog_id NOT IN (?a) }
                            ORDER BY ba_id DESC	
                            LIMIT ?d, ?d ";
		
		$aBas=array();
		if ($aRows=$this->oDb->selectPage(
				$iCount,$sql,$sTag,
				(is_array($aExcludeBlog)&&count($aExcludeBlog)) ? $aExcludeBlog : DBSIMPLE_SKIP,
				($iCurrPage-1)*$iPerPage, $iPerPage
			)
		) {
			foreach ($aRows as $aBa) {
				$aBas[]=$aBa['ba_id'];
			}
		}
		return $aBas;
	}
	
	
	public function GetBasRatingByDate($sDate,$iLimit,$aExcludeBlog=array()) {
		$sql = "SELECT 
						t.ba_id										
					FROM 
						".Config::Get('db.table.ba')." as t
					WHERE   		
						t.ba_date_add >= ?
						AND
						t.ba_rating >= 0 						
					ORDER by t.ba_rating desc, t.ba_id desc
					LIMIT 0, ?d ";		
		$aBas=array();
		if ($aRows=$this->oDb->select(
				$sql,$sDate,
				(is_array($aExcludeBlog)&&count($aExcludeBlog)) ? $aExcludeBlog : DBSIMPLE_SKIP,
				$iLimit
			)
		) {
			foreach ($aRows as $aBa) {
				$aBas[]=$aBa['ba_id'];
			}
		}
		return $aBas;
	}
	
	public function GetBaTags($iLimit,$aExcludeBa=array()) {
		$sql = "SELECT 
			tt.ba_tag_text,
			count(tt.ba_tag_text)	as count		 
			FROM 
				".Config::Get('db.table.ba_tag')." as tt
			WHERE 
				1=1
				{AND tt.ba_id NOT IN(?a) }		
			GROUP BY 
				tt.ba_tag_text
			ORDER BY 
				count desc		
			LIMIT 0, ?d
				";	
		$aReturn=array();
		$aReturnSort=array();
		if ($aRows=$this->oDb->select(
				$sql,
				(is_array($aExcludeBa)&&count($aExcludeBa)) ? $aExcludeBa : DBSIMPLE_SKIP,
				$iLimit
			)
		) {
			foreach ($aRows as $aRow) {				
				$aReturn[mb_strtolower($aRow['ba_tag_text'],'UTF-8')]=$aRow;
			}
			ksort($aReturn);
			foreach ($aReturn as $aRow) {
				$aReturnSort[]=Engine::GetEntity('Ba_BaTag',$aRow);				
			}
		}
		return $aReturnSort;
	}

	public function GetOpenBaTags($iLimit) {
		$sql = "
			SELECT 
				tt.ba_tag_text,
				count(tt.ba_tag_text)	as count		 
			FROM 
				".Config::Get('db.table.ba_tag')." as tt
			WHERE 1 
			GROUP BY 
				tt.ba_tag_text
			ORDER BY 
				count desc		
			LIMIT 0, ?d
				";	
		$aReturn=array();
		$aReturnSort=array();
		if ($aRows=$this->oDb->select($sql,$iLimit)) {
			foreach ($aRows as $aRow) {				
				$aReturn[mb_strtolower($aRow['ba_tag_text'],'UTF-8')]=$aRow;
			}
			ksort($aReturn);
			foreach ($aReturn as $aRow) {
				$aReturnSort[]=Engine::GetEntity('Ba_BaTag',$aRow);				
			}
		}
		return $aReturnSort;
	}

	public function increaseBaCountComment($sBaId) {
		
		$sql = "UPDATE ".Config::Get('db.table.ba')." SET ba_count_comment=ba_count_comment+1 WHERE ba_id= ? ";
		if ($this->oDb->query($sql,$sBaId)) {
			return true;
		} 
		return false;
	}
	
	public function UpdateBa(ModuleBa_EntityBa $oBa) {		
		$sql = "UPDATE ".Config::Get('db.table.ba')." 
			SET 
				ba_text= ?,				
				ba_tags= ?,
				ba_date_add = ?,
				ba_date_edit = ?,
				ba_rating= ?f,
				ba_count_vote= ?d,
				ba_count_comment= ?d
			WHERE
				ba_id = ?d
		";			
		if ($this->oDb->query($sql,$oBa->getText(),$oBa->getTags(),$oBa->getDateAdd(),$oBa->getDateEdit(),$oBa->getRating(),$oBa->getCountVote(),$oBa->getCountComment(), $oBa->getId())) {
			return true;
		}		
		return false;
	}
	
	protected function buildFilter($aFilter) {
		$sWhere='';
		if (isset($aFilter['ba_publish'])) {
			$sWhere.=" AND t.ba_publish =  ".(int)$aFilter['ba_publish'];
		}	
		if (isset($aFilter['ba_rating']) and is_array($aFilter['ba_rating'])) {
			$sPublishIndex='';
			if (isset($aFilter['ba_rating']['publish_index']) and $aFilter['ba_rating']['publish_index']==1) {
				$sPublishIndex=" or ba_publish_index=1 ";
			}
			if ($aFilter['ba_rating']['type']=='top') {
				$sWhere.=" AND ( t.ba_rating >= ".(float)$aFilter['ba_rating']['value']." {$sPublishIndex} ) ";
			} else {
				$sWhere.=" AND ( t.ba_rating < ".(float)$aFilter['ba_rating']['value']."  ) ";
			}			
		}
		if (isset($aFilter['ba_new'])) {
			$sWhere.=" AND t.ba_date_add >=  '".$aFilter['ba_new']."'";
		}
		if (isset($aFilter['user_id'])) {
			$sWhere.=is_array($aFilter['user_id'])
				? " AND t.user_owner_id IN(".implode(', ',$aFilter['user_id']).")"
				: " AND t.user_owner_id =  ".(int)$aFilter['user_id'];
		}
		
		return $sWhere;
	}
	
	public function GetBaTagsByLike($sTag,$iLimit) {
		$sTag=mb_strtolower($sTag,"UTF-8");		
		$sql = "SELECT 
				ba_tag_text					 
			FROM 
				".Config::Get('db.table.ba_tag')."	
			WHERE
				ba_tag_text LIKE ?			
			GROUP BY 
				ba_tag_text					
			LIMIT 0, ?d		
				";	
		$aReturn=array();
		if ($aRows=$this->oDb->select($sql,$sTag.'%',$iLimit)) {
			foreach ($aRows as $aRow) {
				$aReturn[]=Engine::GetEntity('Ba_BaTag',$aRow);
			}
		}
		return $aReturn;
	}
	
	public function UpdateBaRead(ModuleBa_EntityBaRead $oBaRead) {		
		$sql = "UPDATE ".Config::Get('db.table.ba_read')." 
			SET 
				comment_count_last = ? ,
				comment_id_last = ? ,
				date_read = ? 
			WHERE
				ba_id = ? 
				AND				
				user_id = ? 
		";			
		return $this->oDb->query($sql,$oBaRead->getCommentCountLast(),$oBaRead->getCommentIdLast(),$oBaRead->getDateRead(),$oBaRead->getBaId(),$oBaRead->getUserId());
	}	

	public function AddBaRead(ModuleBa_EntityBaRead $oBaRead) {		
		$sql = "INSERT INTO ".Config::Get('db.table.ba_read')." 
			SET 
				comment_count_last = ? ,
				comment_id_last = ? ,
				date_read = ? ,
				ba_id = ? ,							
				user_id = ? 
		";			
		return $this->oDb->query($sql,$oBaRead->getCommentCountLast(),$oBaRead->getCommentIdLast(),$oBaRead->getDateRead(),$oBaRead->getBaId(),$oBaRead->getUserId());
	}

	public function DeleteBaReadByArrayId($aBaId) {
		$sql = "
			DELETE FROM ".Config::Get('db.table.ba_read')." 
			WHERE
				ba_id IN(?a)				
		";			
		if ($this->oDb->query($sql,$aBaId)) {
			return true;
		}
		return false;
	}
			
	public function GetBasReadByArray($aArrayId,$sUserId) {
		if (!is_array($aArrayId) or count($aArrayId)==0) {
			return array();
		}
				
		$sql = "SELECT 
					t.*							 
				FROM 
					".Config::Get('db.table.ba_read')." as t 
				WHERE 
					t.ba_id IN(?a)
					AND
					t.user_id = ?d 
				";
		$aReads=array();
		if ($aRows=$this->oDb->select($sql,$aArrayId,$sUserId)) {
			foreach ($aRows as $aRow) {
				$aReads[]=Engine::GetEntity('Ba_BaRead',$aRow);
			}
		}		
		return $aReads;
	}
	
	public function AddBaQuestionVote(ModuleBa_EntityBaQuestionVote $oBaQuestionVote) {
		$sql = "INSERT INTO ".Config::Get('db.table.ba_question_vote')." 
			(ba_id,
			user_voter_id,
			answer		
			)
			VALUES(?d,  ?d,	?f)
		";			
		if ($this->oDb->query($sql,$oBaQuestionVote->getBaId(),$oBaQuestionVote->getVoterId(),$oBaQuestionVote->getAnswer())===0) 
		{
			return true;
		}		
		return false;
	}
	
		
	public function GetBasQuestionVoteByArray($aArrayId,$sUserId) {
		if (!is_array($aArrayId) or count($aArrayId)==0) {
			return array();
		}
				
		$sql = "SELECT 
					v.*							 
				FROM 
					".Config::Get('db.table.ba_question_vote')." as v 
				WHERE 
					v.ba_id IN(?a)
					AND	
					v.user_voter_id = ?d 				 									
				";
		$aVotes=array();
		if ($aRows=$this->oDb->select($sql,$aArrayId,$sUserId)) {
			foreach ($aRows as $aRow) {
				$aVotes[]=Engine::GetEntity('Ba_BaQuestionVote',$aRow);
			}
		}		
		return $aVotes;
	}
	
	public function MoveBasByArrayId($aBas,$sBlogId) {
		if(!is_array($aBas)) $aBas = array($aBas);
		
		$sql = "UPDATE ".Config::Get('db.table.ba')."
			SET 
				blog_id= ?d
			WHERE
				ba_id IN(?a)
		";			
		if ($this->oDb->query($sql,$sBlogId,$aBas)) {
			return true;
		}		
		return false;
	}
	
	public function MoveBas($sBlogId,$sBlogIdNew) {
		$sql = "UPDATE ".Config::Get('db.table.ba')."
			SET 
				blog_id= ?d
			WHERE
				blog_id = ?d
		";			
		if ($this->oDb->query($sql,$sBlogIdNew,$sBlogId)) {
			return true;
		}		
		return false;
	}
	
	public function MoveBasTags($sBlogId,$sBlogIdNew) {
		$sql = "UPDATE ".Config::Get('db.table.ba_tag')."
			SET 
				blog_id= ?d
			WHERE
				blog_id = ?d
		";			
		if ($this->oDb->query($sql,$sBlogIdNew,$sBlogId)) {
			return true;
		}		
		return false;
	}
	
	public function MoveBasTagsByArrayId($aBas,$sBlogId) {
		if(!is_array($aBas)) $aBas = array($aBas);
		
		$sql = "UPDATE ".Config::Get('db.table.ba_tag')."
			SET 
				blog_id= ?d
			WHERE
				ba_id IN(?a)
		";			
		if ($this->oDb->query($sql,$sBlogId,$aBas)) {
			return true;
		}		
		return false;
	}
	
	public function GetBaRating(&$iCount,$iCurrPage,$iPerPage) {		
		$sql = "SELECT 
					b.ba_id													
				FROM 
					".Config::Get('db.table.ba')." as b 									 
				WHERE 1  
				ORDER by b.ba_rating desc  
				LIMIT ?d, ?d 	";		
		$aReturn=array();
		if ($aRows=$this->oDb->selectPage($iCount,$sql,($iCurrPage-1)*$iPerPage, $iPerPage)) {
			foreach ($aRows as $aRow) {
				$aReturn[]=$aRow['ba_id'];
			}
		}
		return $aReturn;
	}
	
	
}
?>