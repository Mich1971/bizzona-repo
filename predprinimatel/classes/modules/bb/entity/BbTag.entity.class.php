<?php

class ModuleBb_EntityBbTag extends Entity 
{    
    public function getId() {
        return $this->_aData['bb_tag_id'];
    }  
    public function getBbId() {
        return $this->_aData['bb_id'];
    }
    public function getUserId() {
        return $this->_aData['user_id'];
    }

    public function getText() {
        return $this->_aData['bb_tag_text'];
    }
    
    public function getCount() {
        return $this->_aData['count'];
    }
    public function getSize() {
        return $this->_aData['size'];
    }

  
    
	public function setId($data) {
        $this->_aData['bb_tag_id']=$data;
    }
    public function setBbId($data) {
        $this->_aData['bb_id']=$data;
    }
    public function setUserId($data) {
        $this->_aData['user_id']=$data;
    }
    public function setBlogId($data) {
        $this->_aData['blog_id']=$data;
    }
    public function setText($data) {
        $this->_aData['bb_tag_text']=$data;
    }
    
	public function setSize($data) {
        $this->_aData['size']=$data;
    }
}
?>