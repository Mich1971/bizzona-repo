<?php

class ModuleBb extends Module {		
	protected $oMapperBb;
	protected $oUserCurrent=null;
		
	public function Init() {		
		$this->oMapperBb=Engine::GetMapper(__CLASS__);
		$this->oUserCurrent=$this->User_GetUserCurrent();
	}

	public function GetBbsFavouriteByUserId($sUserId,$iCurrPage,$iPerPage) {		

		$data = $this->Favourite_GetFavouritesByUserId($sUserId,'bb',$iCurrPage,$iPerPage);

		$data['collection']=$this->GetBbsAdditionalData($data['collection']);
		return $data;		
	}	
	
	
	public function GetBbsAdditionalData($aBbId,$aAllowData=array('user'=>array(),'blog'=>array('owner'=>array(),'relation_user'),'vote','favourite','comment_new')) {
		
		$aAllowData['favourite'] = true;
		
		func_array_simpleflip($aAllowData);
		if (!is_array($aBbId)) {
			$aBbId=array($aBbId);
		}

		$aBbs=$this->GetBbsByArrayId($aBbId);

		$aUserId=array();
		$aBlogId=array();
		$aBbIdQuestion=array();		
		
		foreach ($aBbs as $oBb) {
			if (isset($aAllowData['user'])) {
				$aUserId[]=$oBb->getUserId();
			}
			if (isset($aAllowData['blog'])) {
				$aBlogId[]=$oBb->getBlogId();
			}
			if ($oBb->getType()=='question')	{		
				$aBbIdQuestion[]=$oBb->getId();
			}
		}

		$aBbsVote=array();
		$aFavouriteBbs=array();
		$aBbsQuestionVote=array();
		$aBbsRead=array();
		$aUsers=isset($aAllowData['user']) && is_array($aAllowData['user']) ? $this->User_GetUsersAdditionalData($aUserId,$aAllowData['user']) : $this->User_GetUsersAdditionalData($aUserId);
		$aBlogs=isset($aAllowData['blog']) && is_array($aAllowData['blog']) ? $this->Blog_GetBlogsAdditionalData($aBlogId,$aAllowData['blog']) : $this->Blog_GetBlogsAdditionalData($aBlogId);		
		if (isset($aAllowData['vote']) and $this->oUserCurrent) {
			$aBbsVote=$this->Vote_GetVoteByArray($aBbId,'bb',$this->oUserCurrent->getId());
			$aBbsQuestionVote=$this->GetBbsQuestionVoteByArray($aBbIdQuestion,$this->oUserCurrent->getId());
		}	
		if (isset($aAllowData['favourite']) and $this->oUserCurrent) {
			$aFavouriteBbs=$this->GetFavouriteBbsByArray($aBbId,$this->oUserCurrent->getId());	
		}
		if (isset($aAllowData['comment_new']) and $this->oUserCurrent) {
			$aBbsRead=$this->GetBbsReadByArray($aBbId,$this->oUserCurrent->getId());	
		}

		if (isset($aAllowData['favourite']) and $this->oUserCurrent) {
			$aFavouriteBbs=$this->GetFavouriteBbsByArray($aBbId,$this->oUserCurrent->getId());	
		}		

		foreach ($aBbs as $oBb) {
			if (isset($aUsers[$oBb->getUserId()])) {
				$oBb->setUser($aUsers[$oBb->getUserId()]);
			} else {
				$oBb->setUser(null); // или $oBb->setUser(new ModuleUser_EntityUser());
			}
			if (isset($aBlogs[$oBb->getBlogId()])) {
				$oBb->setBlog($aBlogs[$oBb->getBlogId()]);
			} else {
				$oBb->setBlog(null); // или $oBb->setBlog(new ModuleBlog_EntityBlog());
			}
			if (isset($aBbsVote[$oBb->getId()])) {
				$oBb->setVote($aBbsVote[$oBb->getId()]);				
			} else {
				$oBb->setVote(null);
			}
			if (isset($aFavouriteBbs[$oBb->getId()])) {
				$oBb->setIsFavourite(true);
			} else {
				$oBb->setIsFavourite(false);
			}			
			if (isset($aBbsQuestionVote[$oBb->getId()])) {
				$oBb->setUserQuestionIsVote(true);
			} else {
				$oBb->setUserQuestionIsVote(false);
			}
			if (isset($aBbsRead[$oBb->getId()]))	{		
				$oBb->setCountCommentNew($oBb->getCountComment()-$aBbsRead[$oBb->getId()]->getCommentCountLast());
				$oBb->setDateRead($aBbsRead[$oBb->getId()]->getDateRead());
			} else {
				$oBb->setCountCommentNew(0);
				$oBb->setDateRead(date("Y-m-d H:i:s"));
			}						
		}
		return $aBbs;
	}

	public function AddBb(ModuleBb_EntityBb $oBb) {
		if ($sId=$this->oMapperBb->AddBb($oBb)) {
			$oBb->setId($sId);
			if ($oBb->getPublish()) {
				$aTags=explode(',',$oBb->getTags());
				foreach ($aTags as $sTag) {
					$oTag=Engine::GetEntity('Bb_BbTag');
					$oTag->setBbId($oBb->getId());
					$oTag->setUserId($oBb->getUserId());
					$oTag->setBlogId($oBb->getBlogId());
					$oTag->setText($sTag);
					$this->oMapperBb->AddBbTag($oTag);
				}
			}
			//чистим зависимые кеши
			$this->Cache_Clean(Zend_Cache::CLEANING_MODE_MATCHING_TAG,array('bb_new',"bb_update_user_{$oBb->getUserId()}","bb_new_blog_{$oBb->getBlogId()}"));						
			return $oBb;
		}
		return false;
	}
	
	public function DeleteBbTagsByBbId($sBbId) {
		return $this->oMapperBb->DeleteBbTagsByBbId($sBbId);
	}	

	public function DeleteBb($oBbId) {
		if ($oBbId instanceof ModuleBb_EntityBb) {
			$sBbId=$oBbId->getId();
			$this->Cache_Clean(Zend_Cache::CLEANING_MODE_MATCHING_TAG,array("bb_update_user_{$oBbId->getUserId()}"));
		} else {
			$sBbId=$oBbId;
		}

		$this->Cache_Clean(Zend_Cache::CLEANING_MODE_MATCHING_TAG,array('bb_update'));
		$this->Cache_Delete("bb_{$sBbId}");

		if($bResult=$this->oMapperBb->DeleteBb($sBbId)){
			return $this->DeleteBbAdditionalData($sBbId);
		}

		return false;
	}

	public function DeleteBbAdditionalData($iBbId) {

		$this->Cache_Clean(Zend_Cache::CLEANING_MODE_MATCHING_TAG,array('bb_update'));
		$this->Cache_Delete("bb_{$iBbId}");
		$this->Comment_DeleteCommentByTargetId($iBbId,'bb');
		$this->DeleteFavouriteBbByArrayId($iBbId);
		$this->DeleteBbReadByArrayId($iBbId);
		$this->Vote_DeleteVoteByTarget($iBbId,'bb');
		$this->DeleteBbTagsByBbId($iBbId);
		
		return true;
	}

	public function UpdateBb(ModuleBb_EntityBb $oBb) {
		$oBbOld=$this->GetBbById($oBb->getId());
		$oBb->setDateEdit(date("Y-m-d H:i:s"));
		if ($this->oMapperBb->UpdateBb($oBb)) {	
			if (($oBb->getPublish()!=$oBbOld->getPublish()) || ($oBb->getBlogId()!=$oBbOld->getBlogId()) || ($oBb->getTags()!=$oBbOld->getTags())) {
				$aTags=explode(',',$oBb->getTags());
				$this->DeleteBbTagsByBbId($oBb->getId());
				
				if ($oBb->getPublish()) {
					foreach ($aTags as $sTag) {
						$oTag=Engine::GetEntity('Bb_BbTag');
						$oTag->setBbId($oBb->getId());
						$oTag->setUserId($oBb->getUserId());
						$oTag->setBlogId($oBb->getBlogId());
						$oTag->setText($sTag);
						$this->oMapperBb->AddBbTag($oTag);
					}
				}
			}
			if ($oBb->getPublish()!=$oBbOld->getPublish()) {
				$this->SetFavouriteBbPublish($oBb->getId(),$oBb->getPublish());
				if ($oBb->getPublish()==0) {
					$this->Comment_DeleteCommentOnlineByTargetId($oBb->getId(),'bb');
				}
				$this->Comment_SetCommentsPublish($oBb->getId(),'bb',$oBb->getPublish());
			}
			$this->Cache_Clean(Zend_Cache::CLEANING_MODE_MATCHING_TAG,array('bb_update',"bb_update_user_{$oBb->getUserId()}","bb_update_blog_{$oBb->getBlogId()}"));
			$this->Cache_Delete("bb_{$oBb->getId()}");
			return true;
		}
		return false;
	}	
		
	public function GetBbById($sId) {		
		$aBbs=$this->GetBbsAdditionalData($sId);
		if (isset($aBbs[$sId])) {
			return $aBbs[$sId];
		}
		return null;
	}	

	public function GetBbRating($iCurrPage,$iPerPage) {		
		
		if (false === ($data = $this->Cache_Get("bb_rating_{$iCurrPage}_{$iPerPage}"))) {				
			$data = array('collection'=>$this->oMapperBb->GetBbRating($iCount,$iCurrPage,$iPerPage),'count'=>$iCount);
			$this->Cache_Set($data, "bb_rating_{$iCurrPage}_{$iPerPage}", array("bb_update","bb_new"), 60*60*24*2);
		}
				 
		$data['collection'] = $this->GetBbsAdditionalData($data['collection']);
		return $data; 
	}	
	
	public function GetBbsByArrayId($aBbId) {
		if (!$aBbId) {
			return array();
		}
		if (Config::Get('sys.cache.solid')) {
			return $this->GetBbsByArrayIdSolid($aBbId);
		}
		
		if (!is_array($aBbId)) {
			$aBbId=array($aBbId);
		}
		$aBbId=array_unique($aBbId);
		$aBbs=array();
		$aBbIdNotNeedQuery=array();
		$aCacheKeys=func_build_cache_keys($aBbId,'bb_');
		if (false !== ($data = $this->Cache_Get($aCacheKeys))) {			
			foreach ($aCacheKeys as $sValue => $sKey ) {
				if (array_key_exists($sKey,$data)) {	
					if ($data[$sKey]) {
						$aBbs[$data[$sKey]->getId()]=$data[$sKey];
					} else {
						$aBbIdNotNeedQuery[]=$sValue;
					}
				} 
			}
		}
		$aBbIdNeedQuery=array_diff($aBbId,array_keys($aBbs));		
		$aBbIdNeedQuery=array_diff($aBbIdNeedQuery,$aBbIdNotNeedQuery);		
		$aBbIdNeedStore=$aBbIdNeedQuery;
		if ($data = $this->oMapperBb->GetBbsByArrayId($aBbIdNeedQuery)) {
			foreach ($data as $oBb) {
				$aBbs[$oBb->getId()]=$oBb;
				$this->Cache_Set($oBb, "bb_{$oBb->getId()}", array(), 60*60*24*4);
				$aBbIdNeedStore=array_diff($aBbIdNeedStore,array($oBb->getId()));
			}
		}
		foreach ($aBbIdNeedStore as $sId) {
			$this->Cache_Set(null, "bb_{$sId}", array(), 60*60*24*4);
		}	
		$aBbs=func_array_sort_by_keys($aBbs,$aBbId);
		return $aBbs;		
	}

	public function GetBbsByArrayIdSolid($aBbId) {
		if (!is_array($aBbId)) {
			$aBbId=array($aBbId);
		}
		$aBbId=array_unique($aBbId);	
		$aBbs=array();	
		$s=join(',',$aBbId);
		if (false === ($data = $this->Cache_Get("bb_id_{$s}"))) {			
			$data = $this->oMapperBb->GetBbsByArrayId($aBbId);
			foreach ($data as $oBb) {
				$aBbs[$oBb->getId()]=$oBb;
			}
			$this->Cache_Set($aBbs, "bb_id_{$s}", array("bb_update"), 60*60*24*1);
			return $aBbs;
		}		
		return $data;
	}

	public function GetCountBbsFavouriteByUserId($sUserId) {
		$aCloseBbs = array();					
		return ($this->oUserCurrent && $sUserId==$this->oUserCurrent->getId()) 
			? $this->Favourite_GetCountFavouritesByUserId($sUserId,'bb',$aCloseBbs)
			: $this->Favourite_GetCountFavouriteOpenBbsByUserId($sUserId);	
	}

	public function GetBbsByFilter($aFilter,$iPage=0,$iPerPage=0,$aAllowData=array('user'=>array(),'blog'=>array('owner'=>array(),'relation_user'),'vote','favourite','comment_new')) {
		$s=serialize($aFilter);
		if (false === ($data = $this->Cache_Get("bb_filter_{$s}_{$iPage}_{$iPerPage}"))) {			
			$data = ($iPage*$iPerPage!=0) 
				? array(
						'collection'=>$this->oMapperBb->GetBbs($aFilter,$iCount,$iPage,$iPerPage),
						'count'=>$iCount
					)
				: array(
						'collection'=>$this->oMapperBb->GetAllBbs($aFilter),
						'count'=>$this->GetCountBbsByFilter($aFilter)
					);
			$this->Cache_Set($data, "bb_filter_{$s}_{$iPage}_{$iPerPage}", array('bb_update','bb_new'), 60*60*24*3);
		}
		$data['collection']=$this->GetBbsAdditionalData($data['collection'],$aAllowData);
		return $data;
	}

	public function GetCountBbsByFilter($aFilter) {
		$s=serialize($aFilter);					
		if (false === ($data = $this->Cache_Get("bb_count_{$s}"))) {			
			$data = $this->oMapperBb->GetCountBbs($aFilter);
			$this->Cache_Set($data, "bb_count_{$s}", array('bb_update','bb_new'), 60*60*24*1);
		}
		return 	$data;
	}

	public function GetBbsGood($iPage,$iPerPage,$bAddAccessible=true) {
		$aFilter=array(
			'blog_type' => array(
				'personal',
				'open'
			),
			'bb_publish' => 1,
			'bb_rating'  => array(
				'value' => Config::Get('module.blog.index_good'),
				'type'  => 'top',
				'publish_index'  => 1,
			)
		);	
		if($this->oUserCurrent && $bAddAccessible) {
			$aOpenBlogs = $this->Blog_GetAccessibleBlogsByUser($this->oUserCurrent);
			if(count($aOpenBlogs)) $aFilter['blog_type']['close'] = $aOpenBlogs;			
		}
		
		return $this->GetBbsByFilter($aFilter,$iPage,$iPerPage);
	}

	public function GetBbsNew($iPage,$iPerPage,$bAddAccessible=true) {
		$sDate=date("Y-m-d H:00:00",time()-Config::Get('module.bb.new_time'));
		$aFilter=array(
			'blog_type' => array(
				'personal',
				'open',
			),
			'bb_publish' => 1,
			'bb_new' => $sDate,
		);	
		if($this->oUserCurrent && $bAddAccessible) {
			$aOpenBlogs = $this->Blog_GetAccessibleBlogsByUser($this->oUserCurrent);
			if(count($aOpenBlogs)) $aFilter['blog_type']['close'] = $aOpenBlogs;
		}			
		return $this->GetBbsByFilter($aFilter,$iPage,$iPerPage);
	}

	public function GetBbsLast($iCount) {		
		$aFilter=array(
			'blog_type' => array(
				'personal',
				'open',
			),
			'bb_publish' => 1,			
		);
		if($this->oUserCurrent) {
			$aOpenBlogs = $this->Blog_GetAccessibleBlogsByUser($this->oUserCurrent);
			if(count($aOpenBlogs)) $aFilter['blog_type']['close'] = $aOpenBlogs;
		}	
		$aReturn=$this->GetBbsByFilter($aFilter,1,$iCount);
		if (isset($aReturn['collection'])) {
			return $aReturn['collection'];
		}
		return false;
	}

	public function GetBbsPersonal($iPage,$iPerPage,$sShowType='good') {
		$aFilter=array(
			'blog_type' => array(
				'personal',
			),
			'bb_publish' => 1,			
		);
		switch ($sShowType) {
			case 'good':
				$aFilter['bb_rating']=array(
					'value' => Config::Get('module.blog.personal_good'),
					'type'  => 'top',
				);			
				break;	
			case 'bad':
				$aFilter['bb_rating']=array(
					'value' => Config::Get('module.blog.personal_good'),
					'type'  => 'down',
				);			
				break;	
			case 'new':
				$aFilter['bb_new']=date("Y-m-d H:00:00",time()-Config::Get('module.bb.new_time'));							
				break;
			default:
				break;
		}
		return $this->GetBbsByFilter($aFilter,$iPage,$iPerPage);
	}	

	public function GetCountBbsPersonalNew() {
		$sDate=date("Y-m-d H:00:00",time()-Config::Get('module.bb.new_time'));
		$aFilter=array(
			'blog_type' => array(
				'personal',
			),
			'bb_publish' => 1,
			'bb_new' => $sDate,
		);				
		return $this->GetCountBbsByFilter($aFilter);
	}

	public function GetBbsPersonalByUser($sUserId,$iPublish,$iPage,$iPerPage) {
		$aFilter=array(			
			'bb_publish' => $iPublish,
			'user_id' => $sUserId,
			'blog_type' => array('open','personal'),
		);
		if($this->oUserCurrent && $this->oUserCurrent->getId()==$sUserId) {
			$aFilter['blog_type'][]='close';
		}		
		return $this->GetBbsByFilter($aFilter,$iPage,$iPerPage);
	}
	
	public function GetCountBbsPersonalByUser($sUserId,$iPublish) {
		$aFilter=array(			
			'bb_publish' => $iPublish,
			'user_id' => $sUserId,
			'blog_type' => array('open','personal'),
		);
		if($this->oUserCurrent && $this->oUserCurrent->getId()==$sUserId) {
			$aFilter['blog_type'][]='close';
		}		
		$s=serialize($aFilter);
		if (false === ($data = $this->Cache_Get("bb_count_user_{$s}"))) {			
			$data = $this->oMapperBb->GetCountBbs($aFilter);
			$this->Cache_Set($data, "bb_count_user_{$s}", array("bb_update_user_{$sUserId}"), 60*60*24);
		}
		return 	$data;		
	}
	
	public function GetBbsCloseByUser($sUserId=null) {
		if(!is_null($sUserId) && $oUser=$this->User_GetUserById($sUserId)) {
			$aCloseBlogs=$this->Blog_GetInaccessibleBlogsByUser($oUser);
			$aFilter=array(
				'bb_publish' => 1,
				'blog_id' => $aCloseBlogs,
			);
		} else {
			$aFilter=array(
				'bb_publish' => 1,
				'blog_type' => array('close'),
			);
		}
		
		$aBbs=$this->GetBbsByFilter($aFilter);
		return array_keys($aBbs['collection']);
	}
	
	public function GetBbsByBlogId($iBlogId,$iPage=0,$iPerPage=0,$aAllowData=array(),$bIdsOnly=true) {
		$aFilter=array('blog_id'=>$iBlogId);
		
		if(!$aBbs = $this->GetBbsByFilter($aFilter,$iPage,$iPerPage,$aAllowData) ) {
			return false;
		}
		
		return ($bIdsOnly) 
			? array_keys($aBbs['collection'])
			: $aBbs;		
	}
	
	public function GetBbsCollective($iPage,$iPerPage,$sShowType='good') {
		$aFilter=array(
			'blog_type' => array(
				'open',
			),
			'bb_publish' => 1,			
		);		
		switch ($sShowType) {
			case 'good':
				$aFilter['bb_rating']=array(
					'value' => Config::Get('module.blog.collective_good'),
					'type'  => 'top',
				);			
				break;	
			case 'bad':
				$aFilter['bb_rating']=array(
					'value' => Config::Get('module.blog.collective_good'),
					'type'  => 'down',
				);			
				break;	
			case 'new':
				$aFilter['bb_new']=date("Y-m-d H:00:00",time()-Config::Get('module.bb.new_time'));							
				break;
			default:
				break;
		}
		if($this->oUserCurrent) {
			$aOpenBlogs = $this->Blog_GetAccessibleBlogsByUser($this->oUserCurrent);
			if(count($aOpenBlogs)) $aFilter['blog_type']['close'] = $aOpenBlogs;
		}
		return $this->GetBbsByFilter($aFilter,$iPage,$iPerPage);
	}	

	public function GetCountBbsCollectiveNew() {
		$sDate=date("Y-m-d H:00:00",time()-Config::Get('module.bb.new_time'));
		$aFilter=array(
			'blog_type' => array(
				'open',
			),
			'bb_publish' => 1,
			'bb_new' => $sDate,
		);
		if($this->oUserCurrent) {
			$aOpenBlogs = $this->Blog_GetAccessibleBlogsByUser($this->oUserCurrent);
			if(count($aOpenBlogs)) $aFilter['blog_type']['close'] = $aOpenBlogs;
		}		
		return $this->GetCountBbsByFilter($aFilter);		
	}

	public function GetBbsRatingByDate($sDate,$iLimit=20) {
		$aCloseBlogs = ($this->oUserCurrent)
			? $this->Blog_GetInaccessibleBlogsByUser($this->oUserCurrent)
			: $this->Blog_GetInaccessibleBlogsByUser();	
		
		$s=serialize($aCloseBlogs);
		
		if (false === ($data = $this->Cache_Get("bb_rating_{$sDate}_{$iLimit}_{$s}"))) {
			$data = $this->oMapperBb->GetBbsRatingByDate($sDate,$iLimit,$aCloseBlogs);
			$this->Cache_Set($data, "bb_rating_{$sDate}_{$iLimit}_{$s}", array('bb_update'), 60*60*24*2);
		}
		$data=$this->GetBbsAdditionalData($data);
		return $data;
	}	

	public function GetBbsByBlog($oBlog,$iPage,$iPerPage,$sShowType='good') {
		$aFilter=array(
			'bb_publish' => 1,
			'blog_id' => $oBlog->getId(),			
		);
		switch ($sShowType) {
			case 'good':
				$aFilter['bb_rating']=array(
					'value' => Config::Get('module.blog.collective_good'),
					'type'  => 'top',
				);			
				break;	
			case 'bad':
				$aFilter['bb_rating']=array(
					'value' => Config::Get('module.blog.collective_good'),
					'type'  => 'down',
				);			
				break;	
			case 'new':
				$aFilter['bb_new']=date("Y-m-d H:00:00",time()-Config::Get('module.bb.new_time'));							
				break;
			default:
				break;
		}		
		return $this->GetBbsByFilter($aFilter,$iPage,$iPerPage);
	}
	
	public function GetCountBbsByBlogNew($oBlog) {
		$sDate=date("Y-m-d H:00:00",time()-Config::Get('module.bb.new_time'));
		$aFilter=array(			
			'bb_publish' => 1,
			'blog_id' => $oBlog->getId(),
			'bb_new' => $sDate,
			
		);	
		return $this->GetCountBbsByFilter($aFilter);		
	}

	public function GetBbsByTag($sTag,$iPage,$iPerPage,$bAddAccessible=true) {
		$aCloseBlogs = ($this->oUserCurrent && $bAddAccessible) 
			? $this->Blog_GetInaccessibleBlogsByUser($this->oUserCurrent)
			: $this->Blog_GetInaccessibleBlogsByUser();
		
		$s = serialize($aCloseBlogs);	
		if (false === ($data = $this->Cache_Get("bb_tag_{$sTag}_{$iPage}_{$iPerPage}_{$s}"))) {			
			$data = array('collection'=>$this->oMapperBb->GetBbsByTag($sTag,$aCloseBlogs,$iCount,$iPage,$iPerPage),'count'=>$iCount);
			$this->Cache_Set($data, "bb_tag_{$sTag}_{$iPage}_{$iPerPage}_{$s}", array('bb_update','bb_new'), 60*60*24*2);
		}
		$data['collection']=$this->GetBbsAdditionalData($data['collection']);
		return $data;		
	}

	public function GetBbTags($iLimit,$aExcludeBb=array()) {
		$s=serialize($aExcludeBb);
		if (false === ($data = $this->Cache_Get("tag_{$iLimit}_{$s}"))) {			
			$data = $this->oMapperBb->GetBbTags($iLimit,$aExcludeBb);
			$this->Cache_Set($data, "tag_{$iLimit}_{$s}", array('bb_update','bb_new'), 60*60*24*3);
		}
		return $data;
	}

	public function GetOpenBbTags($iLimit) {
		if (false === ($data = $this->Cache_Get("tag_{$iLimit}_open"))) {			
			$data = $this->oMapperBb->GetOpenBbTags($iLimit);
			$this->Cache_Set($data, "tag_{$iLimit}_open", array('bb_update','bb_new'), 60*60*24*3);
		}
		return $data;
	}	
	
	public function increaseBbCountComment($sBbId) {		
		$this->Cache_Delete("bb_{$sBbId}");
		$this->Cache_Clean(Zend_Cache::CLEANING_MODE_MATCHING_TAG,array("bb_update"));
		return $this->oMapperBb->increaseBbCountComment($sBbId);
	}

	public function GetFavouriteBb($sBbId,$sUserId) {
		return $this->Favourite_GetFavourite($sBbId,'bb',$sUserId);
	}

	public function GetFavouriteBbsByArray($aBbId,$sUserId) {
		return $this->Favourite_GetFavouritesByArray($aBbId,'bb',$sUserId);
	}

	public function GetFavouriteBbsByArraySolid($aBbId,$sUserId) {
		return $this->Favourite_GetFavouritesByArraySolid($aBbId,'bb',$sUserId);
	}

	public function AddFavouriteBb(ModuleFavourite_EntityFavourite $oFavouriteBb) {		
		return $this->Favourite_AddFavourite($oFavouriteBb);
	}

	public function DeleteFavouriteBb(ModuleFavourite_EntityFavourite $oFavouriteBb) {	
		return $this->Favourite_DeleteFavourite($oFavouriteBb);
	}

	public function SetFavouriteBbPublish($sBbId,$iPublish) {
		return $this->Favourite_SetFavouriteTargetPublish($sBbId,'bb',$iPublish);		
	}	

	public function DeleteFavouriteBbByArrayId($aBbId) {
		return $this->Favourite_DeleteFavouriteByTargetId($aBbId, 'bb');
	}

	public function GetBbTagsByLike($sTag,$iLimit) {
		if (false === ($data = $this->Cache_Get("tag_like_{$sTag}_{$iLimit}"))) {			
			$data = $this->oMapperBb->GetBbTagsByLike($sTag,$iLimit);
			$this->Cache_Set($data, "tag_like_{$sTag}_{$iLimit}", array("bb_update","bb_new"), 60*60*24*3);
		}
		return $data;		
	}

	public function SetBbRead(ModuleBb_EntityBbRead $oBbRead) {		
		if ($this->GetBbRead($oBbRead->getBbId(),$oBbRead->getUserId())) {
			$this->Cache_Delete("bb_read_{$oBbRead->getBbId()}_{$oBbRead->getUserId()}");
			$this->Cache_Clean(Zend_Cache::CLEANING_MODE_MATCHING_TAG,array("bb_read_user_{$oBbRead->getUserId()}"));
			$this->oMapperBb->UpdateBbRead($oBbRead);
		} else {
			$this->Cache_Delete("bb_read_{$oBbRead->getBbId()}_{$oBbRead->getUserId()}");
			$this->Cache_Clean(Zend_Cache::CLEANING_MODE_MATCHING_TAG,array("bb_read_user_{$oBbRead->getUserId()}"));
			$this->oMapperBb->AddBbRead($oBbRead);
		}
		return true;		
	}	

	public function GetBbRead($sBbId,$sUserId) {
		$data=$this->GetBbsReadByArray($sBbId,$sUserId);
		if (isset($data[$sBbId])) {
			return $data[$sBbId];
		}
		return null;
	}

	public function DeleteBbReadByArrayId($aBbId) {
		if(!is_array($aBbId)) $aBbId = array($aBbId);
		return $this->oMapperBb->DeleteBbReadByArrayId($aBbId);
	}

	public function GetBbsReadByArray($aBbId,$sUserId) {
		if (!$aBbId) {
			return array();
		}
		if (Config::Get('sys.cache.solid')) {
			return $this->GetBbsReadByArraySolid($aBbId,$sUserId);
		}
		if (!is_array($aBbId)) {
			$aBbId=array($aBbId);
		}
		$aBbId=array_unique($aBbId);
		$aBbsRead=array();
		$aBbIdNotNeedQuery=array();
		/**
		 * Делаем мульти-запрос к кешу
		 */
		$aCacheKeys=func_build_cache_keys($aBbId,'bb_read_','_'.$sUserId);
		if (false !== ($data = $this->Cache_Get($aCacheKeys))) {			
			/**
			 * проверяем что досталось из кеша
			 */
			foreach ($aCacheKeys as $sValue => $sKey ) {
				if (array_key_exists($sKey,$data)) {	
					if ($data[$sKey]) {
						$aBbsRead[$data[$sKey]->getBbId()]=$data[$sKey];
					} else {
						$aBbIdNotNeedQuery[]=$sValue;
					}
				} 
			}
		}
		/**
		 * Смотрим каких топиков не было в кеше и делаем запрос в БД
		 */		
		$aBbIdNeedQuery=array_diff($aBbId,array_keys($aBbsRead));		
		$aBbIdNeedQuery=array_diff($aBbIdNeedQuery,$aBbIdNotNeedQuery);		
		$aBbIdNeedStore=$aBbIdNeedQuery;
		if ($data = $this->oMapperBb->GetBbsReadByArray($aBbIdNeedQuery,$sUserId)) {
			foreach ($data as $oBbRead) {
				/**
				 * Добавляем к результату и сохраняем в кеш
				 */
				$aBbsRead[$oBbRead->getBbId()]=$oBbRead;
				$this->Cache_Set($oBbRead, "bb_read_{$oBbRead->getBbId()}_{$oBbRead->getUserId()}", array(), 60*60*24*4);
				$aBbIdNeedStore=array_diff($aBbIdNeedStore,array($oBbRead->getBbId()));
			}
		}
		/**
		 * Сохраняем в кеш запросы не вернувшие результата
		 */
		foreach ($aBbIdNeedStore as $sId) {
			$this->Cache_Set(null, "bb_read_{$sId}_{$sUserId}", array(), 60*60*24*4);
		}		
		/**
		 * Сортируем результат согласно входящему массиву
		 */
		$aBbsRead=func_array_sort_by_keys($aBbsRead,$aBbId);
		return $aBbsRead;		
	}

	public function GetBbsReadByArraySolid($aBbId,$sUserId) {
		/*
		if (!is_array($aBbId)) {
			$aBbId=array($aBbId);
		}
		$aBbId=array_unique($aBbId);	
		$aBbsRead=array();	
		$s=join(',',$aBbId);
		if (false === ($data = $this->Cache_Get("bb_read_{$sUserId}_id_{$s}"))) {			
			$data = $this->oMapperBb->GetBbsReadByArray($aBbId,$sUserId);
			foreach ($data as $oBbRead) {
				$aBbsRead[$oBbRead->getBbId()]=$oBbRead;
			}
			$this->Cache_Set($aBbsRead, "bb_read_{$sUserId}_id_{$s}", array("bb_read_user_{$sUserId}"), 60*60*24*1);
			return $aBbsRead;
		}		
		return $data;
		*/
	}

	public function GetBbQuestionVote($sBbId,$sUserId) {
		$data=$this->GetBbsQuestionVoteByArray($sBbId,$sUserId);
		if (isset($data[$sBbId])) {
			return $data[$sBbId];
		}
		return null;
	}

	public function GetBbsQuestionVoteByArray($aBbId,$sUserId) {
		if (!$aBbId) {
			return array();
		}
		if (Config::Get('sys.cache.solid')) {
			return $this->GetBbsQuestionVoteByArraySolid($aBbId,$sUserId);
		}
		if (!is_array($aBbId)) {
			$aBbId=array($aBbId);
		}
		$aBbId=array_unique($aBbId);
		$aBbsQuestionVote=array();
		$aBbIdNotNeedQuery=array();
		/**
		 * Делаем мульти-запрос к кешу
		 */
		$aCacheKeys=func_build_cache_keys($aBbId,'bb_question_vote_','_'.$sUserId);
		if (false !== ($data = $this->Cache_Get($aCacheKeys))) {			
			/**
			 * проверяем что досталось из кеша
			 */
			foreach ($aCacheKeys as $sValue => $sKey ) {
				if (array_key_exists($sKey,$data)) {	
					if ($data[$sKey]) {
						$aBbsQuestionVote[$data[$sKey]->getBbId()]=$data[$sKey];
					} else {
						$aBbIdNotNeedQuery[]=$sValue;
					}
				} 
			}
		}
		/**
		 * Смотрим каких топиков не было в кеше и делаем запрос в БД
		 */		
		$aBbIdNeedQuery=array_diff($aBbId,array_keys($aBbsQuestionVote));		
		$aBbIdNeedQuery=array_diff($aBbIdNeedQuery,$aBbIdNotNeedQuery);		
		$aBbIdNeedStore=$aBbIdNeedQuery;
		if ($data = $this->oMapperBb->GetBbsQuestionVoteByArray($aBbIdNeedQuery,$sUserId)) {
			foreach ($data as $oBbVote) {
				/**
				 * Добавляем к результату и сохраняем в кеш
				 */
				$aBbsQuestionVote[$oBbVote->getBbId()]=$oBbVote;
				$this->Cache_Set($oBbVote, "bb_question_vote_{$oBbVote->getBbId()}_{$oBbVote->getVoterId()}", array(), 60*60*24*4);
				$aBbIdNeedStore=array_diff($aBbIdNeedStore,array($oBbVote->getBbId()));
			}
		}
		/**
		 * Сохраняем в кеш запросы не вернувшие результата
		 */
		foreach ($aBbIdNeedStore as $sId) {
			$this->Cache_Set(null, "bb_question_vote_{$sId}_{$sUserId}", array(), 60*60*24*4);
		}		
		/**
		 * Сортируем результат согласно входящему массиву
		 */
		$aBbsQuestionVote=func_array_sort_by_keys($aBbsQuestionVote,$aBbId);
		return $aBbsQuestionVote;		
	}

	public function GetBbsQuestionVoteByArraySolid($aBbId,$sUserId) {
		if (!is_array($aBbId)) {
			$aBbId=array($aBbId);
		}
		$aBbId=array_unique($aBbId);	
		$aBbsQuestionVote=array();	
		$s=join(',',$aBbId);
		if (false === ($data = $this->Cache_Get("bb_question_vote_{$sUserId}_id_{$s}"))) {			
			$data = $this->oMapperBb->GetBbsQuestionVoteByArray($aBbId,$sUserId);
			foreach ($data as $oBbVote) {
				$aBbsQuestionVote[$oBbVote->getBbId()]=$oBbVote;
			}
			$this->Cache_Set($aBbsQuestionVote, "bb_question_vote_{$sUserId}_id_{$s}", array("bb_question_vote_user_{$sUserId}"), 60*60*24*1);
			return $aBbsQuestionVote;
		}		
		return $data;
	}

	public function AddBbQuestionVote(ModuleBb_EntityBbQuestionVote $oBbQuestionVote) {
		$this->Cache_Delete("bb_question_vote_{$oBbQuestionVote->getBbId()}_{$oBbQuestionVote->getVoterId()}");
		$this->Cache_Clean(Zend_Cache::CLEANING_MODE_MATCHING_TAG,array("bb_question_vote_user_{$oBbQuestionVote->getVoterId()}"));
		return $this->oMapperBb->AddBbQuestionVote($oBbQuestionVote);
	}

	public function GetBbUnique($sUserId,$sHash) {
		$sId=$this->oMapperBb->GetBbUnique($sUserId,$sHash);
		return $this->GetBbById($sId);
	}

	public function SendNotifyBbNew($oBlog,$oBb,$oUserBb) {
		$aBlogUsers=$this->Blog_GetBlogUsersByBlogId($oBlog->getId());
		foreach ($aBlogUsers as $oBlogUser) {
			if ($oBlogUser->getUserId()==$oUserBb->getId()) {
				continue;
			}
			$this->Notify_SendBbNewToSubscribeBlog($oBlogUser->getUser(),$oBb,$oBlog,$oUserBb);
		}
		//отправляем создателю блога
		if ($oBlog->getOwnerId()!=$oUserBb->getId()) {
			$this->Notify_SendBbNewToSubscribeBlog($oBlog->getOwner(),$oBb,$oBlog,$oUserBb);
		}	
	}

	public function GetLastBbsByUserId($sUserId,$iTimeLimit,$iCountLimit=1,$aAllowData=array()) {
		$aFilter = array(
			'bb_publish' => 1,
			'user_id' => $sUserId,
			'bb_new' => date("Y-m-d H:i:s",time()-$iTimeLimit),
		);
		$aBbs = $this->GetBbsByFilter($aFilter,1,$iCountLimit,$aAllowData);

		return $aBbs;
	}
	
	public function MoveBbsByArrayId($aBbs,$sBlogId) {
		$this->Cache_Clean(Zend_Cache::CLEANING_MODE_MATCHING_TAG,array("bb_update", "bb_new_blog_{$sBlogId}"));
		if ($res=$this->oMapperBb->MoveBbsByArrayId($aBbs,$sBlogId)) {
			// перемещаем теги
			$this->oMapperBb->MoveBbsTagsByArrayId($aBbs,$sBlogId);
			// меняем target parent у комментов
			$this->Comment_UpdateTargetParentByTargetId($sBlogId, 'bb', $aBbs);
			// меняем target parent у комментов в прямом эфире
			$this->Comment_UpdateTargetParentByTargetIdOnline($sBlogId, 'bb', $aBbs);
			return $res;
		}
		return false;
	}
	
	public function MoveBbs($sBlogId,$sBlogIdNew) {
		$this->Cache_Clean(Zend_Cache::CLEANING_MODE_MATCHING_TAG,array("bb_update", "bb_new_blog_{$sBlogId}", "bb_new_blog_{$sBlogIdNew}"));
		if ($res=$this->oMapperBb->MoveBbs($sBlogId,$sBlogIdNew)) {
			// перемещаем теги
			$this->oMapperBb->MoveBbsTags($sBlogId,$sBlogIdNew);
			// меняем target parent у комментов
			$this->Comment_MoveTargetParent($sBlogId, 'bb', $sBlogIdNew);
			// меняем target parent у комментов в прямом эфире
			$this->Comment_MoveTargetParentOnline($sBlogId, 'bb', $sBlogIdNew);
			return $res;
		}
		return false;
	}	
	
	public function UploadBbImageFile($aFile,$oUser) {
		if(!is_array($aFile) || !isset($aFile['tmp_name'])) {
			return false;
		}
							
		$sFileTmp=Config::Get('sys.cache.dir').func_generator();		
		if (!move_uploaded_file($aFile['tmp_name'],$sFileTmp)) {			
			return false;
		}
		$sDirUpload=$this->Image_GetIdDir($oUser->getId());
		$aParams=$this->Image_BuildParams('bb');
		
		if ($sFileImage=$this->Image_Resize($sFileTmp,$sDirUpload,func_generator(6),Config::Get('view.img_max_width'),Config::Get('view.img_max_height'),Config::Get('view.img_resize_width'),null,true,$aParams)) {
			@unlink($sFileTmp);
			return $this->Image_GetWebPath($sFileImage);
		}
		@unlink($sFileTmp);
		return false;
	}

	public function UploadBbImageUrl($sUrl, $oUser) {
		if(!@getimagesize($sUrl)) {
			return ModuleImage::UPLOAD_IMAGE_ERROR_TYPE;
		}
		$oFile=fopen($sUrl,'r');
		if(!$oFile) {
			return ModuleImage::UPLOAD_IMAGE_ERROR_READ;
		}
		
		$iMaxSizeKb=500;
		$iSizeKb=0;
		$sContent='';
		while (!feof($oFile) and $iSizeKb<$iMaxSizeKb) {
			$sContent.=fread($oFile ,1024*1);
			$iSizeKb++;
		}

		if(!feof($oFile)) {
			return ModuleImage::UPLOAD_IMAGE_ERROR_SIZE;
		}
		fclose($oFile);

		$sFileTmp=Config::Get('sys.cache.dir').func_generator();
		
		$fp=fopen($sFileTmp,'w');
		fwrite($fp,$sContent);
		fclose($fp);
		
		$sDirSave=$this->Image_GetIdDir($oUser->getId());
		$aParams=$this->Image_BuildParams('bb');
		
		if ($sFileImg=$this->Image_Resize($sFileTmp,$sDirSave,func_generator(),Config::Get('view.img_max_width'),Config::Get('view.img_max_height'),Config::Get('view.img_resize_width'),null,true,$aParams)) {
			@unlink($sFileTmp);
			return $this->Image_GetWebPath($sFileImg);
		} 		
		
		@unlink($sFileTmp);
		return ModuleImage::UPLOAD_IMAGE_ERROR;
	}
}
?>