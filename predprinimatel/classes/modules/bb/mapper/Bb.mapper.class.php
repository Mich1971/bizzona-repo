<?php
/*-------------------------------------------------------
*
*   LiveStreet Engine Social Networking
*   Copyright © 2008 Mzhelskiy Maxim
*
*--------------------------------------------------------
*
*   Official site: www.livestreet.ru
*   Contact e-mail: rus.engine@gmail.com
*
*   GNU General Public License, version 2:
*   http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
*
---------------------------------------------------------
*/

class ModuleBb_MapperBb extends Mapper {	
		
	public function AddBb(ModuleBb_EntityBb $oBb) {
		$sql = "INSERT INTO ".Config::Get('db.table.bb')." 
			(
			user_id,
			bb_title,			
			bb_tags,
			bb_date_add,
			bb_user_ip,
			bb_cut_text,
			bb_text_hash			
			)
			VALUES(?d,  ?,	?,	?,  ?, ?, ?)
		";			
		if ($iId=$this->oDb->query($sql,$oBb->getUserId(),$oBb->getTitle(),
			$oBb->getTags(),$oBb->getDateAdd(),$oBb->getUserIp(),$oBb->getCutText(),$oBb->getTextHash())) 
		{
			$oBb->setId($iId);
			$this->AddBbContent($oBb);
			return $iId;
		}		
		return false;
	}

	public function GetBbRating(&$iCount,$iCurrPage,$iPerPage) {		
		$sql = "SELECT 
					b.bb_id													
				FROM 
					".Config::Get('db.table.bb')." as b 									 
				WHERE 1 									

				ORDER by b.bb_rating desc
				LIMIT ?d, ?d 	";		
		$aReturn=array();
		if ($aRows=$this->oDb->selectPage($iCount,$sql,($iCurrPage-1)*$iPerPage, $iPerPage)) {
			foreach ($aRows as $aRow) {
				$aReturn[]=$aRow['bb_id'];
			}
		}
		return $aReturn;
	}	
	
	public function AddBbContent(ModuleBb_EntityBb $oBb) {
		$sql = "INSERT INTO ".Config::Get('db.table.bb_content')." 
			(bb_id,			
			bb_text,
			bb_text_short,
			bb_text_source,
			bb_extra			
			)
			VALUES(?d,  ?,	?,	?, ? )
		";			
		if ($iId=$this->oDb->query($sql,$oBb->getId(),$oBb->getText(),
			$oBb->getTextShort(),$oBb->getTextSource(),$oBb->getExtra())) 
		{
			return $iId;
		}		
		return false;
	}
	
	public function AddBbTag(ModuleBb_EntityBbTag $oBbTag) {
		$sql = "INSERT INTO ".Config::Get('db.table.bb_tag')." 
			(bb_id,
			user_id,
			bb_tag_text		
			)
			VALUES(?d,  ?d,  ?)
		";			
		if ($iId=$this->oDb->query($sql,$oBbTag->getBbId(),$oBbTag->getUserId(),$oBbTag->getText())) 
		{
			return $iId;
		}		
		return false;
	}
	
	
	
	public function DeleteBbTagsByBbId($sBbId) {
		$sql = "DELETE FROM ".Config::Get('db.table.bb_tag')." 
			WHERE
				bb_id = ?d				
		";			
		if ($this->oDb->query($sql,$sBbId)) {
			return true;
		}		
		return false;
	}
	
	public function DeleteBb($sBbId) {
		$sql = "DELETE FROM ".Config::Get('db.table.bb')." 
			WHERE
				bb_id = ?d				
		";			
		if ($this->oDb->query($sql,$sBbId)) {
			return true;
		}		
		return false;
	}
	
		
	public function GetBbUnique($sUserId,$sHash) {
		$sql = "SELECT bb_id FROM ".Config::Get('db.table.bb')." 
			WHERE 				
				bb_text_hash =? 						
				AND
				user_id = ?d
			LIMIT 0,1
				";
		if ($aRow=$this->oDb->selectRow($sql,$sHash,$sUserId)) {
			return $aRow['bb_id'];
		}
		return null;
	}
	
	public function GetBbsByArrayId($aArrayId) {
		if (!is_array($aArrayId) or count($aArrayId)==0) {
			return array();
		}
				
		$sql = "SELECT 
					t.*,
					tc.*							 
				FROM 
					".Config::Get('db.table.bb')." as t	
					JOIN  ".Config::Get('db.table.bb_content')." AS tc ON t.bb_id=tc.bb_id				
				WHERE 
					t.bb_id IN(?a) 									
				ORDER BY FIELD(t.bb_id,?a) ";
		$aBbs=array();
		if ($aRows=$this->oDb->select($sql,$aArrayId,$aArrayId)) {
			foreach ($aRows as $aBb) {
				$aBbs[]=Engine::GetEntity('Bb',$aBb);
			}
		}		
		return $aBbs;
	}
	
	
	public function GetBbs($aFilter,&$iCount,$iCurrPage,$iPerPage) {
		$sWhere=$this->buildFilter($aFilter);
		
		if(isset($aFilter['order']) and !is_array($aFilter['order'])) {
			$aFilter['order'] = array($aFilter['order']);
		} else {
			$aFilter['order'] = array('t.bb_date_add desc');
		}
		
		$sql = "SELECT 
						t.bb_id							
					FROM 
						".Config::Get('db.table.bb')." as t 			
					WHERE 
						1=1					
						".$sWhere." 
					ORDER BY ".
						implode(', ', $aFilter['order'])
				."
					LIMIT ?d, ?d";		
		$aBbs=array();
		if ($aRows=$this->oDb->selectPage($iCount,$sql,($iCurrPage-1)*$iPerPage, $iPerPage)) {			
			foreach ($aRows as $aBb) {
				$aBbs[]=$aBb['bb_id'];
			}
		}				
		return $aBbs;
	}
	
	public function GetCountBbs($aFilter) {		
		$sWhere=$this->buildFilter($aFilter);
		$sql = "SELECT 
					count(t.bb_id) as count									
				FROM 
					".Config::Get('db.table.bb')." as t,					
					".Config::Get('db.table.blog')." as b
				WHERE 
					1=1
					
					".$sWhere."								
					
					AND
					t.blog_id=b.blog_id;";		
		if ($aRow=$this->oDb->selectRow($sql)) {
			return $aRow['count'];
		}
		return false;
	}
	
	public function GetAllBbs($aFilter) {
		$sWhere=$this->buildFilter($aFilter);
		
		$sql = "SELECT 
						t.bb_id							
					FROM 
						".Config::Get('db.table.bb')." as t,	
						".Config::Get('db.table.blog')." as b			
					WHERE 
						1=1					
						".$sWhere."
						AND
						t.blog_id=b.blog_id										
					ORDER by t.bb_id desc";		
		$aBbs=array();
		if ($aRows=$this->oDb->select($sql)) {			
			foreach ($aRows as $aBb) {
				$aBbs[]=$aBb['bb_id'];
			}
		}		

		return $aBbs;		
	}
	
	public function GetBbsByTag($sTag,$aExcludeBlog,&$iCount,$iCurrPage,$iPerPage) {		
		$sql = "				
							SELECT 		
								bb_id										
							FROM 
								".Config::Get('db.table.bb_tag')."								
							WHERE 
								bb_tag_text = ? 	
								{ AND blog_id NOT IN (?a) }
                            ORDER BY bb_id DESC	
                            LIMIT ?d, ?d ";
		
		$aBbs=array();
		if ($aRows=$this->oDb->selectPage(
				$iCount,$sql,$sTag,
				(is_array($aExcludeBlog)&&count($aExcludeBlog)) ? $aExcludeBlog : DBSIMPLE_SKIP,
				($iCurrPage-1)*$iPerPage, $iPerPage
			)
		) {
			foreach ($aRows as $aBb) {
				$aBbs[]=$aBb['bb_id'];
			}
		}
		return $aBbs;
	}
	
	
	public function GetBbsRatingByDate($sDate,$iLimit,$aExcludeBlog=array()) {
		$sql = "SELECT 
						t.bb_id										
					FROM 
						".Config::Get('db.table.bb')." as t
					WHERE 					
						t.bb_publish = 1
						AND
						t.bb_date_add >= ?
						AND
						t.bb_rating >= 0
						{ AND t.blog_id NOT IN(?a) } 																	
					ORDER by t.bb_rating desc, t.bb_id desc
					LIMIT 0, ?d ";		
		$aBbs=array();
		if ($aRows=$this->oDb->select(
				$sql,$sDate,
				(is_array($aExcludeBlog)&&count($aExcludeBlog)) ? $aExcludeBlog : DBSIMPLE_SKIP,
				$iLimit
			)
		) {
			foreach ($aRows as $aBb) {
				$aBbs[]=$aBb['bb_id'];
			}
		}
		return $aBbs;
	}
	
	public function GetBbTags($iLimit,$aExcludeBb=array()) {
		$sql = "SELECT 
			tt.bb_tag_text,
			count(tt.bb_tag_text)	as count		 
			FROM 
				".Config::Get('db.table.bb_tag')." as tt
			WHERE 
				1=1
				{AND tt.bb_id NOT IN(?a) }		
			GROUP BY 
				tt.bb_tag_text
			ORDER BY 
				count desc		
			LIMIT 0, ?d
				";	
		$aReturn=array();
		$aReturnSort=array();
		if ($aRows=$this->oDb->select(
				$sql,
				(is_array($aExcludeBb)&&count($aExcludeBb)) ? $aExcludeBb : DBSIMPLE_SKIP,
				$iLimit
			)
		) {
			foreach ($aRows as $aRow) {				
				$aReturn[mb_strtolower($aRow['bb_tag_text'],'UTF-8')]=$aRow;
			}
			ksort($aReturn);
			foreach ($aReturn as $aRow) {
				$aReturnSort[]=Engine::GetEntity('Bb_BbTag',$aRow);				
			}
		}
		return $aReturnSort;
	}

	public function GetOpenBbTags($iLimit) {
		$sql = "
			SELECT 
				tt.bb_tag_text,
				count(tt.bb_tag_text)	as count		 
			FROM 
				".Config::Get('db.table.bb_tag')." as tt
			WHERE 
				1 
			GROUP BY 
				tt.bb_tag_text
			ORDER BY 
				count desc		
			LIMIT 0, ?d
				";	
		$aReturn=array();
		$aReturnSort=array();
		if ($aRows=$this->oDb->select($sql,$iLimit)) {
			foreach ($aRows as $aRow) {				
				$aReturn[mb_strtolower($aRow['bb_tag_text'],'UTF-8')]=$aRow;
			}
			ksort($aReturn);
			foreach ($aReturn as $aRow) {
				$aReturnSort[]=Engine::GetEntity('Bb_BbTag',$aRow);				
			}
		}
		return $aReturnSort;
	}

	
	public function increaseBbCountComment($sBbId) {
		$sql = "UPDATE ".Config::Get('db.table.bb')." 
			SET 
				bb_count_comment=bb_count_comment+1
			WHERE
				bb_id = ?
		";			
		if ($this->oDb->query($sql,$sBbId)) {
			return true;
		}		
		return false;
	}
	
	public function UpdateBb(ModuleBb_EntityBb $oBb) {		
		$sql = "UPDATE ".Config::Get('db.table.bb')." 
			SET 
				bb_title= ?,				
				bb_tags= ?,
				bb_date_add = ?,
				bb_date_edit = ?,
				bb_user_ip= ?,
				bb_rating= ?f,
				bb_count_vote= ?d,
				bb_count_read= ?d,
				bb_count_comment= ?d, 
				bb_cut_text = ? ,
				bb_text_hash = ? 
			WHERE
				bb_id = ?d
		";			
		if ($this->oDb->query($sql,$oBb->getTitle(),$oBb->getTags(),$oBb->getDateAdd(),$oBb->getDateEdit(),$oBb->getUserIp(),$oBb->getRating(),$oBb->getCountVote(),$oBb->getCountRead(),$oBb->getCountComment(),$oBb->getCutText(),$oBb->getTextHash(),$oBb->getId())) {
			$this->UpdateBbContent($oBb);
			return true;
		}		
		return false;
	}
	
	public function UpdateBbContent(ModuleBb_EntityBb $oBb) {		
		$sql = "UPDATE ".Config::Get('db.table.bb_content')." 
			SET 				
				bb_text= ?,
				bb_text_short= ?,
				bb_text_source= ?,
				bb_extra= ?
			WHERE
				bb_id = ?d
		";			
		if ($this->oDb->query($sql,$oBb->getText(),$oBb->getTextShort(),$oBb->getTextSource(),$oBb->getExtra(),$oBb->getId())) {
			return true;
		}		
		return false;
	}
	
	protected function buildFilter($aFilter) {
		$sWhere='';
		
		if (isset($aFilter['user_id'])) {
			$sWhere.=is_array($aFilter['user_id'])
				? " AND t.user_id IN(".implode(', ',$aFilter['user_id']).")"
				: " AND t.user_id =  ".(int)$aFilter['user_id'];
		}		
		
		return $sWhere;
	}
	
	public function GetBbTagsByLike($sTag,$iLimit) {
		$sTag=mb_strtolower($sTag,"UTF-8");		
		$sql = "SELECT 
				bb_tag_text					 
			FROM 
				".Config::Get('db.table.bb_tag')."	
			WHERE
				bb_tag_text LIKE ?			
			GROUP BY 
				bb_tag_text					
			LIMIT 0, ?d		
				";	
		$aReturn=array();
		if ($aRows=$this->oDb->select($sql,$sTag.'%',$iLimit)) {
			foreach ($aRows as $aRow) {
				$aReturn[]=Engine::GetEntity('Bb_BbTag',$aRow);
			}
		}
		return $aReturn;
	}
	
	public function UpdateBbRead(ModuleBb_EntityBbRead $oBbRead) {		
		$sql = "UPDATE ".Config::Get('db.table.bb_read')." 
			SET 
				comment_count_last = ? ,
				comment_id_last = ? ,
				date_read = ? 
			WHERE
				bb_id = ? 
				AND				
				user_id = ? 
		";			
		return $this->oDb->query($sql,$oBbRead->getCommentCountLast(),$oBbRead->getCommentIdLast(),$oBbRead->getDateRead(),$oBbRead->getBbId(),$oBbRead->getUserId());
	}	

	public function AddBbRead(ModuleBb_EntityBbRead $oBbRead) {		
		$sql = "INSERT INTO ".Config::Get('db.table.bb_read')." 
			SET 
				comment_count_last = ? ,
				comment_id_last = ? ,
				date_read = ? ,
				bb_id = ? ,							
				user_id = ? 
		";			
		return $this->oDb->query($sql,$oBbRead->getCommentCountLast(),$oBbRead->getCommentIdLast(),$oBbRead->getDateRead(),$oBbRead->getBbId(),$oBbRead->getUserId());
	}
	/**
	 * Удаляет записи о чтении записей по списку идентификаторов
	 *
	 * @param  array $aBbId
	 * @return bool
	 */				
	public function DeleteBbReadByArrayId($aBbId) {
		/*
		$sql = "
			DELETE FROM ".Config::Get('db.table.bb_read')." 
			WHERE
				bb_id IN(?a)				
		";			
		if ($this->oDb->query($sql,$aBbId)) {
			return true;
		}
		return false;
		*/
		return true;
	}
			
	public function GetBbsReadByArray($aArrayId,$sUserId) {
		/*
		if (!is_array($aArrayId) or count($aArrayId)==0) {
			return array();
		}
				
		$sql = "SELECT 
					t.*							 
				FROM 
					".Config::Get('db.table.bb_read')." as t 
				WHERE 
					t.bb_id IN(?a)
					AND
					t.user_id = ?d 
				";
		$aReads=array();
		if ($aRows=$this->oDb->select($sql,$aArrayId,$sUserId)) {
			foreach ($aRows as $aRow) {
				$aReads[]=Engine::GetEntity('Bb_BbRead',$aRow);
			}
		}		
		return $aReads;
		*/
	}
	
	public function AddBbQuestionVote(ModuleBb_EntityBbQuestionVote $oBbQuestionVote) {
		$sql = "INSERT INTO ".Config::Get('db.table.bb_question_vote')." 
			(bb_id,
			user_voter_id,
			answer		
			)
			VALUES(?d,  ?d,	?f)
		";			
		if ($this->oDb->query($sql,$oBbQuestionVote->getBbId(),$oBbQuestionVote->getVoterId(),$oBbQuestionVote->getAnswer())===0) 
		{
			return true;
		}		
		return false;
	}
	
		
	public function GetBbsQuestionVoteByArray($aArrayId,$sUserId) {
		if (!is_array($aArrayId) or count($aArrayId)==0) {
			return array();
		}
				
		$sql = "SELECT 
					v.*							 
				FROM 
					".Config::Get('db.table.bb_question_vote')." as v 
				WHERE 
					v.bb_id IN(?a)
					AND	
					v.user_voter_id = ?d 				 									
				";
		$aVotes=array();
		if ($aRows=$this->oDb->select($sql,$aArrayId,$sUserId)) {
			foreach ($aRows as $aRow) {
				$aVotes[]=Engine::GetEntity('Bb_BbQuestionVote',$aRow);
			}
		}		
		return $aVotes;
	}
	
	/**
	 * Перемещает топики в другой блог
	 *
	 * @param  array  $aBbs
	 * @param  string $sBlogId
	 * @return bool
	 */	
	public function MoveBbsByArrayId($aBbs,$sBlogId) {
		if(!is_array($aBbs)) $aBbs = array($aBbs);
		
		$sql = "UPDATE ".Config::Get('db.table.bb')."
			SET 
				blog_id= ?d
			WHERE
				bb_id IN(?a)
		";			
		if ($this->oDb->query($sql,$sBlogId,$aBbs)) {
			return true;
		}		
		return false;
	}
	
	/**
	 * Перемещает топики в другой блог
	 *
	 * @param  string $sBlogId
	 * @param  string $sBlogIdNew
	 * @return bool
	 */	
	public function MoveBbs($sBlogId,$sBlogIdNew) {
		$sql = "UPDATE ".Config::Get('db.table.bb')."
			SET 
				blog_id= ?d
			WHERE
				blog_id = ?d
		";			
		if ($this->oDb->query($sql,$sBlogIdNew,$sBlogId)) {
			return true;
		}		
		return false;
	}
	
	/**
	 * Перемещает теги топиков в другой блог
	 *
	 * @param string $sBlogId
	 * @param string $sBlogIdNew
	 * @return bool
	 */
	public function MoveBbsTags($sBlogId,$sBlogIdNew) {
		$sql = "UPDATE ".Config::Get('db.table.bb_tag')."
			SET 
				blog_id= ?d
			WHERE
				blog_id = ?d
		";			
		if ($this->oDb->query($sql,$sBlogIdNew,$sBlogId)) {
			return true;
		}		
		return false;
	}
	
	/**
	 * Перемещает теги топиков в другой блог
	 *
	 * @param array $aBbs
	 * @param string $sBlogId
	 * @return bool
	 */
	public function MoveBbsTagsByArrayId($aBbs,$sBlogId) {
		if(!is_array($aBbs)) $aBbs = array($aBbs);
		
		$sql = "UPDATE ".Config::Get('db.table.bb_tag')."
			SET 
				blog_id= ?d
			WHERE
				bb_id IN(?a)
		";			
		if ($this->oDb->query($sql,$sBlogId,$aBbs)) {
			return true;
		}		
		return false;
	}
	
}
?>