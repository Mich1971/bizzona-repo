<?php

class ModuleBs extends Module {
	
	protected $oMapperBs;	
	protected $oUserCurrent=null;	
	
	public function Init() {
		$this->oMapperBs=Engine::GetMapper(__CLASS__);
		$this->oMapperBs->SetUserCurrent($this->User_GetUserCurrent());
		$this->oUserCurrent=$this->User_GetUserCurrent();		
	}	
	
	public function GetBssPersonalByUser($sUserId,$iPublish,$iPage,$iPerPage) {
		$aFilter=array(	'user_id' => $sUserId );
		
		return $this->GetBssByFilter($aFilter,$iPage,$iPerPage);		
	}	
	
	public function GetBssFavouriteByUserId($sUserId,$iCurrPage,$iPerPage) {		

		$data = $this->Favourite_GetFavouritesByUserId($sUserId,'bs',$iCurrPage,$iPerPage);

		$data['collection']=$this->GetBssAdditionalData($data['collection']);
		return $data;		
	}	
	
	
	public function GetBsRating($iCurrPage,$iPerPage) {		
		
		if (false === ($data = $this->Cache_Get("bs_rating_{$iCurrPage}_{$iPerPage}"))) {				
			$data = array('collection'=>$this->oMapperBs->GetBsRating($iCount,$iCurrPage,$iPerPage),'count'=>$iCount);
			$this->Cache_Set($data, "bs_rating_{$iCurrPage}_{$iPerPage}", array("bs_update","bs_new"), 60*60*24*2);
		}
				 
		$data['collection'] = $this->GetBssAdditionalData($data['collection']);
		return $data; 
	}

	public function increaseBsCountComment($sBsId) {
		$this->Cache_Delete("bs_{$sBsId}");
		$this->Cache_Clean(Zend_Cache::CLEANING_MODE_MATCHING_TAG,array("bs_update"));
		return $this->oMapperBs->increaseBsCountComment($sBsId);		
		
	}	
	
	public function DeleteBsTagsByBsId($sBsId) {
		return $this->oMapperBs->DeleteBsTagsByBsId($sBsId);
	}	
	
	public function UpdateBs(ModuleBs_EntityBs $oBs) {
		
		$oBsOld=$this->GetBsById($oBs->getId());
		$oBs->setDateEdit(date("Y-m-d H:i:s"));
		if ($this->oMapperBs->UpdateBs($oBs)) {	
			
			$this->Cache_Delete("bs_{$oBs->getId()}");
			return true;
		}
		return false;
	}	
	
	public function GetBssByArrayIdSolid($aBsId) {
		
		if (!is_array($aBsId)) {
			$aBsId=array($aBsId);
		}
		$aBsId=array_unique($aBsId);	
		$aBss=array();	
		$s=join(',',$aBsId);
		if (false === ($data = $this->Cache_Get("bs_id_{$s}"))) {			
			$data = $this->oMapperBs->GetBssByArrayId($aBsId);
			foreach ($data as $oBs) {
				$aBss[$oBs->getId()]=$oBs;
			}
			$this->Cache_Set($aBss, "bs_id_{$s}", array("bs_update"), 60*60*24*1);
			return $aBss;
		}		
		return $data;
		
	}	
	
	public function GetBsById($sId) {		
		$aBss=$this->GetBssAdditionalData($sId);
		if (isset($aBss[$sId])) {
			return $aBss[$sId];
		}
		return null;
	}	
	
	public function GeBsByArrayId($aBsId) {
		
		
		if (!$aBsId) {
			return array();
		}
		if (Config::Get('sys.cache.solid')) {
			return $this->GetBssByArrayIdSolid($aBsId);
		}
		if (!is_array($aBsId)) {
			$aBsId=array($aBsId);
		}
		$aBsId=array_unique($aBsId);
		$aBss=array();
		$aBsIdNotNeedQuery=array();

		$aCacheKeys=func_build_cache_keys($aBsId,'bs_');
		if (false !== ($data = $this->Cache_Get($aCacheKeys))) {			
			foreach ($aCacheKeys as $sValue => $sKey ) {
				if (array_key_exists($sKey,$data)) {	
					if ($data[$sKey]) {
						$aBss[$data[$sKey]->getId()]=$data[$sKey];
					} else {
						$aBsIdNotNeedQuery[]=$sValue;
					}
				} 
			}
		}

		$aBsIdNeedQuery=array_diff($aBsId,array_keys($aBss));		
		$aBsIdNeedQuery=array_diff($aBsIdNeedQuery,$aBsIdNotNeedQuery);		
		$aBsIdNeedStore=$aBsIdNeedQuery;
		if ($data = $this->oMapperBs->GetBssByArrayId($aBsIdNeedQuery)) {
			foreach ($data as $oBs) {
				$aBss[$oBs->getId()]=$oBs;
				$this->Cache_Set($oBs, "bs_{$oBs->getId()}", array(), 60*60*24*4);
				$aBsIdNeedStore=array_diff($aBsIdNeedStore,array($oBs->getId()));
			}
		}

		foreach ($aBsIdNeedStore as $sId) {
			$this->Cache_Set(null, "bs_{$sId}", array(), 60*60*24*4);
		}		

		$aBss=func_array_sort_by_keys($aBss,$aBsId);
		return $aBss;		
		
	}	
	
	public function AddBs(ModuleBs_EntityBs $oBs) {
		
		if ($sId=$this->oMapperBs->AddBs($oBs)) {
			return $sId;
		}
		return false;
		
	}	
	
	public function SetBsRead(ModuleBs_EntityBsRead $oBsRead) {		
		/*
		if ($this->GetBsRead($oBsRead->getBsId(),$oBsRead->getUserId())) {
			$this->Cache_Delete("bs_read_{$oBsRead->getBsId()}_{$oBsRead->getUserId()}");
			$this->Cache_Clean(Zend_Cache::CLEANING_MODE_MATCHING_TAG,array("bs_read_user_{$oBsRead->getUserId()}"));
			$this->oMapperBs->UpdateBsRead($oBsRead);
		} else {
			$this->Cache_Delete("bs_read_{$oBsRead->getBsId()}_{$oBsRead->getUserId()}");
			$this->Cache_Clean(Zend_Cache::CLEANING_MODE_MATCHING_TAG,array("bs_read_user_{$oBsRead->getUserId()}"));
			$this->oMapperBs->AddBsRead($oBsRead);
		}
		*/
		return true;		
	}	
	
	public function GetBsRead($sBsId,$sUserId) {
		/*
		$data=$this->GetBssReadByArray($sBsId,$sUserId);
		if (isset($data[$sBsId])) {
			return $data[$sBsId];
		}
		return null;
		*/
	}	
	
	public function DeleteBs($oBsId) {
		if ($oBsId instanceof ModuleBs_EntityBs) {
			$sBsId=$oBsId->getId();
			$this->Cache_Clean(Zend_Cache::CLEANING_MODE_MATCHING_TAG,array("bs_update_user_{$oBsId->getUserId()}"));
		} else {
			$sBsId=$oBsId;
		}

		$this->Cache_Clean(Zend_Cache::CLEANING_MODE_MATCHING_TAG,array('bs_update'));
		$this->Cache_Delete("bs_{$sBsId}");

		if($bResult=$this->oMapperBs->DeleteBs($sBsId)){
			return $this->DeleteBsAdditionalData($sBsId);
		}

		return false;
	}	
	
	public function DeleteBsAdditionalData($iBsId) {

		$this->Cache_Clean(Zend_Cache::CLEANING_MODE_MATCHING_TAG,array('bs_update'));
		$this->Cache_Delete("bs_{$iBsId}");

		$this->Comment_DeleteCommentByTargetId($iBsId,'bs');
		
		return true;
	}	
	
	public function GetBssByFilter($aFilter, $iPage=0,$iPerPage=0) {
		$s=serialize($aFilter);
		//$iCount = 0;
		if (false === ($data = $this->Cache_Get("bs_filter_{$s}_{$iPage}_{$iPerPage}"))) {
			$data = ($iPage*$iPerPage!=0) 
				? array(
							'collection'=>$this->oMapperBs->GetBss($aFilter, $iCount,$iPage,$iPerPage),
							'count'=>$iCount
					 	)
				: array(
							'collection'=>$this->oMapperBs->GetAllBss($aFilter),
							'count'=>$this->GetCountBssByFilter()
					 	);	 	
			$this->Cache_Set($data, "bs_filter_{$s}_{$iPage}_{$iPerPage}", array('bs_update','bs_new'), 60*60*24*3);
		}
		$data['collection']=$this->GetBssAdditionalData($data['collection']);
		
		return $data;
	}	
	
	public function GetCountBssByFilter() {
		$s=1;					
		if (false === ($data = $this->Cache_Get("bs_count_{$s}"))) {			
			$data = $this->oMapperBs->GetCountBss();
			$this->Cache_Set($data, "bs_count_{$s}", array('bs_update','bs_new'), 60*60*24*1);
		}
		return 	$data;
	}	
	
	public function GetBssAdditionalData($aBsId, $aAllowData=array('user'=>array(),'bs'=>array('owner'=>array(),'relation_user'),'vote')) {
		if (!is_array($aBsId)) {
			$aBsId=array($aBsId);
		}		
		
		$aAllowData['favourite'] = true;		
		
		$aBss=$this->GetBssByArrayId($aBsId);
		
		$aUserId=array();
		$aBssVote=array();
		$aFavouriteBss=array();
		
		foreach ($aBss as $oBs) {
			if (isset($aAllowData['user'])) {
				$aUserId[]=$oBs->getUserId();
			}
		}		
		
		$aUsers=isset($aAllowData['user']) && is_array($aAllowData['user']) ? $this->User_GetUsersAdditionalData($aUserId,$aAllowData['user']) : $this->User_GetUsersAdditionalData($aUserId);

		if (isset($aAllowData['vote']) and $this->oUserCurrent) {
			$aBssVote=$this->Vote_GetVoteByArray($aBsId,'bs',$this->oUserCurrent->getId());
		}		
		
		if (isset($aAllowData['favourite']) and $this->oUserCurrent) {
			$aFavouriteBss=$this->GetFavouriteBssByArray($aBsId,$this->oUserCurrent->getId());	
		}		

		foreach ($aBss as $oBs) {

			if (isset($aUsers[$oBs->getUserId()])) {
				$oBs->setUser($aUsers[$oBs->getUserId()]);
			} else {
				$oBs->setUser(null);
			}			
			
			if (isset($aBssVote[$oBs->getId()])) {
				$oBs->setVote($aBssVote[$oBs->getId()]);				
			} else {
				$oBs->setVote(null);
			}			
			
			if (isset($aFavouriteBss[$oBs->getId()])) {
				$oBs->setIsFavourite(true);
			} else {
				$oBs->setIsFavourite(false);
			}			
			
		}		
		
		return $aBss;
	}

	public function GetFavouriteBssByArray($aBsId,$sUserId) {
		return $this->Favourite_GetFavouritesByArray($aBsId,'bs',$sUserId);
	}	
	
	public function GetFavouriteBs($sBsId,$sUserId) {
		return $this->Favourite_GetFavourite($sBsId,'bs',$sUserId);		
	}	
	
	public function AddFavouriteBs(ModuleFavourite_EntityFavourite $oFavouriteBs) {		
		return $this->Favourite_AddFavourite($oFavouriteBs);		
	}	
	
	public function GetBssByArrayId($aBsId) {
		
		if (!$aBsId) {
			return array();
		}		
		
		$aBss=array();
		$aBsIdNotNeedQuery=array();
		$aCacheKeys=func_build_cache_keys($aBsId,'Bs_');
		if (false !== ($data = $this->Cache_Get($aCacheKeys))) {			

			foreach ($aCacheKeys as $sValue => $sKey ) {
				if (array_key_exists($sKey,$data)) {	
					if ($data[$sKey]) {
						$aBss[$data[$sKey]->getId()]=$data[$sKey];
					} else {
						$aBsIdNotNeedQuery[]=$sValue;
					}
				} 
			}
		}		
		
		$aBsIdNeedQuery=array_diff($aBsId,array_keys($aBss));		
		$aBsIdNeedQuery=array_diff($aBsIdNeedQuery,$aBsIdNotNeedQuery);		
		$aBsIdNeedStore=$aBsIdNeedQuery;		
		
		if ($data = $this->oMapperBs->GetBssByArrayId($aBsIdNeedQuery)) {
			foreach ($data as $oBs) {
				$aBss[$oBs->getId()]=$oBs;
				$this->Cache_Set($oBs, "Bs_{$oBs->getId()}", array(), 60*60*24*4);
				$aBsIdNeedStore=array_diff($aBsIdNeedStore,array($oBs->getId()));
			}
		}
		foreach ($aBsIdNeedStore as $sId) {
			$this->Cache_Set(null, "Bs_{$sId}", array(), 60*60*24*4);
		}				
		
		$aBss=func_array_sort_by_keys($aBss,$aBsId);
		
		return $aBss;
	}
	
	public function GetBssByTag($sTag,$iPage,$iPerPage,$bAddAccessible=true) {
		$s = "";
		
		if (false === ($data = $this->Cache_Get("bs_tag_{$sTag}_{$iPage}_{$iPerPage}_{$s}"))) {			
			$data = array('collection'=>$this->oMapperBs->GetBssByTag($sTag,$iCount,$iPage,$iPerPage),'count'=>$iCount);
			$this->Cache_Set($data, "bs_tag_{$sTag}_{$iPage}_{$iPerPage}_{$s}", array('bs_update','bs_new'), 60*60*24*2);
		}
		$data['collection']=$this->GetBssAdditionalData($data['collection']);
		return $data;		
		
	}	
	
}

?>