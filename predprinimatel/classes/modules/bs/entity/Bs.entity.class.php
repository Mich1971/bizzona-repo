<?php

class ModuleBs_EntityBs extends Entity 
{    
    public function getBs() {
        return $this->_aData['bs'];
    }	
	
    public function setBs($data) {
        $this->_aData['bs']=$data;
    }    
    
    public function getId() {
        return $this->_aData['bs_id'];
    }  
    public function getOwnerId() {
        return $this->_aData['user_owner_id'];
    }

    public function getText() {
        return $this->_aData['bs_text'];
    }

    public function getDateAdd() {
        return $this->_aData['bs_date_add'];
    }
    public function getDateEdit() {
        return $this->_aData['bs_data_edit'];
    }
    public function getRating() {        
        return number_format(round($this->_aData['bs_rating'],2), 2, '.', '');
    }
    public function getCountVote() {
        return $this->_aData['bs_count_vote'];
    }
    public function getCountUser() {
        return $this->_aData['bs_count_user'];
    }
    
    public function getCountComment() {
        return $this->_aData['bs_count_comment'];
    }    

    public function getUserId() {
        return $this->_aData['user_owner_id'];
    }

    public function getIsFavourite() {
        return $this->_aData['bs_is_favourite'];
    }       
    
    public function setUserId($data) {
        $this->_aData['user_owner_id']=$data;
    }    
    
    public function setCountComment($data) {
        $this->_aData['bs_count_comment']=$data;
    }    
    
    
    public function getUrl() { 
    	return Router::GetPath('bs').$this->getId().'.html';
    }    
    
    public function getUser() {
        return $this->_aData['user'];
    }    
    
    public function setUser($data) {
        $this->_aData['user']=$data;
    }    
    
    public function getOwner() {
        return $this->_aData['owner'];
    }    
    public function getVote() {
        return $this->_aData['vote'];
    }

	public function setId($data) {
        $this->_aData['bs_id']=$data;
    }
    public function setOwnerId($data) {
        $this->_aData['user_owner_id']=$data;
    }
    public function setText($data) {
        $this->_aData['bs_text']=$data;
    }
    public function setDateAdd($data) {
        $this->_aData['bs_date_add']=$data;
    }   
    public function setDateEdit($data) {
        $this->_aData['bs_data_edit']=$data;
    } 
    public function setRating($data) {
        $this->_aData['bs_rating']=$data;
    }
    public function setCountVote($data) {
        $this->_aData['bs_count_vote']=$data;
    }
    public function setCountUser($data) {
        $this->_aData['bs_count_user']=$data;
    }
    public function setUrl($data) {
        $this->_aData['bs_url']=$data;
    }
    public function setOwner($data) {
        $this->_aData['owner']=$data;
    }
    public function setVote($data) {
        $this->_aData['vote']=$data;
    }
    public function setIsFavourite($data) {
        $this->_aData['bs_is_favourite']=$data;
    }    
}
?>