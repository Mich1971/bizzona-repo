<?php

class ModuleBs_MapperBs extends Mapper {	
	protected $oUserCurrent=null;
	
	public function SetUserCurrent($oUserCurrent)  {
		$this->oUserCurrent=$oUserCurrent;
	}
	
	public function increaseBsCountComment($sBsId) {
		
		
		$sql = "UPDATE ".Config::Get('db.table.bs')." 
			SET 
				bs_count_comment=bs_count_comment+1
			WHERE
				bs_id = ?
		";
		if ($this->oDb->query($sql,$sBsId)) {
			
			return true;
		}		
		return false;
	}	
	
	public function AddBs(ModuleBs_EntityBs $oBs) {
		$sql = "INSERT INTO ".Config::Get('db.table.bs')." 
			(user_owner_id,
			bs_text,
			bs_date_add,
			bs_url
			)
			VALUES(?d,  ?,	?, ?)
		";			
		if ($iId=$this->oDb->query($sql,$oBs->getOwnerId(),$oBs->getText(),$oBs->getDateAdd(),$oBs->getUrl())) {
			return $iId;
		}		
		return false;
	}
	
	public function GetCountBss() {		
		
		$sql = "SELECT 
					count(t.bs_id) as count									
				FROM 
					".Config::Get('db.table.bs')." as t
				WHERE 
					1=1";
				
		if ($aRow=$this->oDb->selectRow($sql)) {
			return $aRow['count'];
		}
		return false;
	}	
	
	public function UpdateBs(ModuleBs_EntityBs $oBs) {		
		
		$sql = "UPDATE ".Config::Get('db.table.bs')." 
			SET 
				bs_text= ?,
				bs_date_edit= ?,
				bs_rating= ?f,
				bs_count_vote = ?d,
				bs_count_user= ?d,
				bs_url= ?
			WHERE
				bs_id = ?d
		";			
		if ($this->oDb->query($sql,$oBs->getText(),$oBs->getDateEdit(),$oBs->getRating(),$oBs->getCountVote(),$oBs->getCountUser(),$oBs->getUrl(),$oBs->getId())) {
			return true;
		}		
		return false;
		
	}
	
	public function GetBssByArrayId($aArrayId) {
		if (!is_array($aArrayId) or count($aArrayId)==0) {
			return array();
		}
				
		$sql = "SELECT 
					b.*							 
				FROM 
					".Config::Get('db.table.bs')." as b					
				WHERE 
					b.bs_id IN(?a) 								
				ORDER BY FIELD(b.bs_id,?a) ";
		$aBss=array();
		if ($aRows=$this->oDb->select($sql,$aArrayId,$aArrayId)) {
			foreach ($aRows as $aBs) {
				$aBss[]=Engine::GetEntity('Bs',$aBs);
			}
		}
		return $aBss;
	}	
	
	public function AddRelationBsUser(ModuleBs_EntityBsUser $oBsUser) {
		$sql = "INSERT INTO ".Config::Get('db.table.bs_user')." 
			(bs_id,
			user_id,
			user_role
			)
			VALUES(?d,  ?d, ?d)
		";			
		if ($this->oDb->query($sql,$oBsUser->getBsId(),$oBsUser->getUserId(),$oBsUser->getUserRole())===0) {
			return true;
		}		
		return false;
	}
	
	public function DeleteRelationBsUser(ModuleBs_EntityBsUser $oBsUser) {
		$sql = "DELETE FROM ".Config::Get('db.table.bs_user')." 
			WHERE
				bs_id = ?d
				AND
				user_id = ?d
		";			
		if ($this->oDb->query($sql,$oBsUser->getBsId(),$oBsUser->getUserId())) {
			return true;
		}		
		return false;
	}
		
	public function UpdateRelationBsUser(ModuleBs_EntityBsUser $oBsUser) {		
		$sql = "UPDATE ".Config::Get('db.table.bs_user')." 
			SET 
				user_role = ?d			
			WHERE
				bs_id = ?d 
				AND
				user_id = ?d
		";			
		if ($this->oDb->query($sql,$oBsUser->getUserRole(),$oBsUser->getBsId(),$oBsUser->getUserId())) {
			return true;
		}		
		return false;
	}
	
	public function GetBsUsers($aFilter) {
		$sWhere=' 1=1 ';
		if (isset($aFilter['bs_id'])) {
			$sWhere.=" AND bu.bs_id =  ".(int)$aFilter['bs_id'];
		}
		if (isset($aFilter['user_id'])) {
			$sWhere.=" AND bu.user_id =  ".(int)$aFilter['user_id'];
		}
		if (isset($aFilter['user_role'])) {
			if(!is_array($aFilter['user_role'])) {
				$aFilter['user_role']=array($aFilter['user_role']);
			}
			$sWhere.=" AND bu.user_role IN ('".join("', '",$aFilter['user_role'])."')";		
		} else {
			$sWhere.=" AND bu.user_role>".ModuleBs::BLOG_USER_ROLE_GUEST;
		}
		
		$sql = "SELECT
					bu.*				
				FROM 
					".Config::Get('db.table.bs_user')." as bu
				WHERE 
					".$sWhere." 					
				;
					";		
		$aBsUsers=array();
		if ($aRows=$this->oDb->select($sql)) {
			foreach ($aRows as $aUser) {
				$aBsUsers[]=Engine::GetEntity('Bs_BsUser',$aUser);
			}
		}
		return $aBsUsers;
	}

	public function GetBsUsersByArrayBs($aArrayId,$sUserId) {	
		if (!is_array($aArrayId) or count($aArrayId)==0) {
			return array();
		}
			
		$sql = "SELECT 
					bu.*				
				FROM 
					".Config::Get('db.table.bs_user')." as bu
				WHERE 
					bu.bs_id IN(?a) 					
					AND
					bu.user_id = ?d ";		
		$aBsUsers=array();
		if ($aRows=$this->oDb->select($sql,$aArrayId,$sUserId)) {
			foreach ($aRows as $aUser) {
				$aBsUsers[]=Engine::GetEntity('Bs_BsUser',$aUser);
			}
		}
		return $aBsUsers;
	}
	
		
	public function GetPersonalBsByUserId($sUserId) {
		$sql = "SELECT bs_id FROM ".Config::Get('db.table.bs')." WHERE user_owner_id = ?d and bs_type='personal'";
		if ($aRow=$this->oDb->selectRow($sql,$sUserId)) {
			return $aRow['bs_id'];
		}
		return null;
	}
	
		
	public function GetBsByTitle($sTitle) {
		$sql = "SELECT bs_id FROM ".Config::Get('db.table.bs')." WHERE bs_title = ? ";
		if ($aRow=$this->oDb->selectRow($sql,$sTitle)) {
			return $aRow['bs_id'];
		}
		return null;
	}
	

	
	
	public function GetBsByUrl($sUrl) {		
		$sql = "SELECT 
				b.bs_id 
			FROM 
				".Config::Get('db.table.bs')." as b
			WHERE 
				b.bs_url = ? 		
				";
		if ($aRow=$this->oDb->selectRow($sql,$sUrl)) {
			return $aRow['bs_id'];
		}
		return null;
	}
	
	public function GetBssByOwnerId($sUserId) {
		$sql = "SELECT 
			b.bs_id			 
			FROM 
				".Config::Get('db.table.bs')." as b				
			WHERE 
				b.user_owner_id = ? 
				AND
				b.bs_type<>'personal'				
				";	
		$aBss=array();
		if ($aRows=$this->oDb->select($sql,$sUserId)) {
			foreach ($aRows as $aBs) {
				$aBss[]=$aBs['bs_id'];
			}
		}
		return $aBss;
	}
	
	public function GetBss($aFilter, &$iCount,$iCurrPage,$iPerPage) {
		$sWhere=$this->buildFilter($aFilter);
		$sql = "SELECT 
			b.bs_id			 
			FROM 
				".Config::Get('db.table.bs')." as b				
			WHERE 				
				1 ".$sWhere."
				ORDER BY b.bs_date_add desc LIMIT ?d, ?d ";	
		$aBss=array();
		if ($aRows=$this->oDb->selectPage($iCount,$sql,($iCurrPage-1)*$iPerPage, $iPerPage)) {
			foreach ($aRows as $aBs) {
				$aBss[]=$aBs['bs_id'];
			}
		}
		return $aBss;
	}
		
	protected function buildFilter($aFilter) {
		$sWhere='';

		if (isset($aFilter['user_id'])) {
			$sWhere.=is_array($aFilter['user_id'])
				? " AND b.user_owner_id IN(".implode(', ',$aFilter['user_id']).")"
				: " AND b.user_owner_id =  ".(int)$aFilter['user_id'];
		}

		return $sWhere;
	}
	
	
	public function GetBsRating(&$iCount,$iCurrPage,$iPerPage) {		
		$sql = "SELECT 
					b.bs_id													
				FROM 
					".Config::Get('db.table.bs')." as b 									 
				WHERE  1 
 				ORDER by b.bs_rating desc
				LIMIT ?d, ?d 	";		
		$aReturn=array();
		if ($aRows=$this->oDb->selectPage($iCount,$sql,($iCurrPage-1)*$iPerPage, $iPerPage)) {
			foreach ($aRows as $aRow) {
				$aReturn[]=$aRow['bs_id'];
			}
		}
		return $aReturn;
	}
	
	public function GetBssRatingJoin($sUserId,$iLimit) {		
		$sql = "SELECT 
					b.*													
				FROM 
					".Config::Get('db.table.bs_user')." as bu,
					".Config::Get('db.table.bs')." as b	
				WHERE 	
					bu.user_id = ?d
					AND
					bu.bs_id = b.bs_id 
				ORDER by b.bs_rating desc
				LIMIT 0, ?d 
				;	
					";		
		$aReturn=array();
		if ($aRows=$this->oDb->select($sql,$sUserId,$iLimit)) {
			foreach ($aRows as $aRow) {
				$aReturn[]=Engine::GetEntity('Bs',$aRow);
			}
		}
		return $aReturn;
	}
	
	public function GetBssRatingSelf($sUserId,$iLimit) {		
		$sql = "SELECT 
					b.*													
				FROM 					
					".Config::Get('db.table.bs')." as b	
				WHERE 						
					b.user_owner_id = ?d
					AND				
					b.bs_type<>'personal'													
				ORDER by b.bs_rating desc
				LIMIT 0, ?d 
			;";		
		$aReturn=array();
		if ($aRows=$this->oDb->select($sql,$sUserId,$iLimit)) {
			foreach ($aRows as $aRow) {
				$aReturn[]=Engine::GetEntity('Bs',$aRow);
			}
		}
		return $aReturn;
	}
	
	public function GetCloseBss() {
		$sql = "SELECT b.bs_id										
				FROM ".Config::Get('db.table.bs')." as b					
				WHERE b.bs_type='close'
			;";
		$aReturn=array();
		if ($aRows=$this->oDb->select($sql)) {
			foreach ($aRows as $aRow) {
				$aReturn[]=$aRow['bs_id'];
			}
		}
		return $aReturn;
	}
	
	/**
	 * Удаление блога из базы данных
	 *
	 * @param  int  $iBsId
	 * @return bool	 
	 */
	public function DeleteBs($iBsId) {
		$sql = "
			DELETE FROM ".Config::Get('db.table.bs')." 
			WHERE bs_id = ?d				
		";			
		if ($this->oDb->query($sql,$iBsId)) {
			return true;
		}
		return false;
	}
	
	/**
	 * Удалить пользователей блога по идентификатору блога
	 *
	 * @param  int  $iBsId
	 * @return bool
	 */
	public function DeleteBsUsersByBsId($iBsId) {
		$sql = "
			DELETE FROM ".Config::Get('db.table.bs_user')." 
			WHERE bs_id = ?d
		";
		if ($this->oDb->query($sql,$iBsId)) {
			return true;
		}
		return false;
	}
	
	public function GetAllBss($aFilter) {
		
		$sWhere=$this->buildFilter($aFilter);
		
		$sql = "SELECT 
						b.bs_id							
					FROM 
						".Config::Get('db.table.bs')." as b
					WHERE 
						1=1	".$sWhere."				
					ORDER by b.bs_id desc";		
		$aBss=array();
		if ($aRows=$this->oDb->select($sql)) {			
			foreach ($aRows as $aBs) {
				$aBss[]=$aBs['bs_id'];
			}
		}		

		return $aBss;		
	}	
	
	public function GetBssByTag($sTag,&$iCount,$iCurrPage,$iPerPage) {		
		
		$aBss = array();
		
		$sql = "				
							SELECT 		
								bs_id										
							FROM 
								".Config::Get('db.table.bs_tag')."								
							WHERE 
								bs_tag_text = ? 	
								
                            ORDER BY bs_id DESC	
                            LIMIT ?d, ?d ";
		
		$aTopics=array();
		if ($aRows=$this->oDb->selectPage(
				$iCount,$sql,$sTag,
				($iCurrPage-1)*$iPerPage, $iPerPage
			)
		) {
			foreach ($aRows as $aBs) {
				$aBss[]=$aBs['bs_id'];
			}
		}
		return $aBss;
	}	

}
?>