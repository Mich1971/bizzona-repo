<?php

set_include_path(get_include_path().PATH_SEPARATOR.dirname(dirname(dirname(__FILE__))));
$sDirRoot=dirname(dirname(dirname(__FILE__)));
require_once($sDirRoot."/config/config.ajax.php");

$iType=getRequest('type',null,'post');
$idBi=getRequest('idBi',null,'post');
$bStateError=true;
$sMsg='';
$sMsgTitle='';
$bState=false;
if ($oEngine->User_IsAuthorization()) {
	if (in_array($iType,array('1','0'))) {
		if ($oBi=$oEngine->Bi_GetBiById($idBi)) {
			$oUserCurrent=$oEngine->User_GetUserCurrent();
			$oFavouriteBi=$oEngine->Bi_GetFavouriteBi($oBi->getId(),$oUserCurrent->getId());
			if (!$oFavouriteBi and $iType) {
				$oFavouriteBiNew=Engine::GetEntity('Favourite',
					array(
						'target_id'      => $oBi->getId(),
						'user_id'        => $oUserCurrent->getId(),
						'target_type'    => 'bi',
						'target_publish' => $oBi->getPublish()
					)
				);
				if ($oEngine->Bi_AddFavouriteBi($oFavouriteBiNew)) {
					$bStateError=false;
					$sMsgTitle=$oEngine->Lang_Get('attention');
					$sMsg=$oEngine->Lang_Get('topic_favourite_add_ok');
					$bState=true;
				} else {
					$sMsgTitle=$oEngine->Lang_Get('error');
					$sMsg=$oEngine->Lang_Get('system_error');
				}
			}
			if (!$oFavouriteBi and !$iType) {
				$sMsgTitle=$oEngine->Lang_Get('error');
				$sMsg=$oEngine->Lang_Get('topic_favourite_add_no');
			}
			if ($oFavouriteBi and $iType) {
				$sMsgTitle=$oEngine->Lang_Get('error');
				$sMsg=$oEngine->Lang_Get('topic_favourite_add_already');
			}
			if ($oFavouriteBi and !$iType) {
				if ($oEngine->Bi_DeleteFavouriteBi($oFavouriteBi)) {
					$bStateError=false;
					$sMsgTitle=$oEngine->Lang_Get('attention');
					$sMsg=$oEngine->Lang_Get('topic_favourite_del_ok');
					$bState=false;
				} else {
					$sMsgTitle=$oEngine->Lang_Get('error');
					$sMsg=$oEngine->Lang_Get('system_error');
				}
			}
		} else {
			$sMsgTitle=$oEngine->Lang_Get('error');
			$sMsg=$oEngine->Lang_Get('system_error');
		}
	} else {
		$sMsgTitle=$oEngine->Lang_Get('error');
		$sMsg=$oEngine->Lang_Get('system_error');
	}
} else {
	$sMsgTitle=$oEngine->Lang_Get('error');
	$sMsg=$oEngine->Lang_Get('need_authorization');
}


$GLOBALS['_RESULT'] = array(
"bStateError"     => $bStateError,
"bState"   => $bState,
"sMsgTitle"   => $sMsgTitle,
"sMsg"   => $sMsg,
);

?>