<?php

set_include_path(get_include_path().PATH_SEPARATOR.dirname(dirname(dirname(__FILE__))));
$sDirRoot=dirname(dirname(dirname(__FILE__)));
require_once($sDirRoot."/config/config.ajax.php");

$iValue=getRequest('value',null,'post');
$bStateError=true;
$sMsg='';
$sMsgTitle=''; 
$iRating=0;
if ($oEngine->User_IsAuthorization()) {
	if ($oBs=$oEngine->Bs_GetBsById(getRequest('idBs',null,'post'))) {
		
		$oUserCurrent=$oEngine->User_GetUserCurrent();
		if ($oBs->getUserId()!=$oUserCurrent->getId()) {
			
			if (!($oBsVote=$oEngine->Vote_GetVote($oBs->getId(),'bs',$oUserCurrent->getId()))) {
				
				if (strtotime($oBs->getDateAdd())>time()-Config::Get('acl.vote.topic.limit_time')) {
					
					if ($oEngine->ACL_CanVoteBs($oUserCurrent,$oBs) or $iValue==0) {
						
						if (in_array($iValue,array('1','-1','0'))) {
					
							
									
							$oBsVote=Engine::GetEntity('Vote');
							$oBsVote->setTargetId($oBs->getId());
							$oBsVote->setTargetType('bs');
							$oBsVote->setVoterId($oUserCurrent->getId());
							$oBsVote->setDirection($iValue);
							$oBsVote->setDate(date("Y-m-d H:i:s"));
							$iVal=0;
							if ($iValue!=0) {
								$iVal=(float)$oEngine->Rating_VoteBs($oUserCurrent,$oBs,$iValue);
							}
							
							$oBsVote->setValue($iVal);
							$oBs->setCountVote($oBs->getCountVote()+1);
							if ($oEngine->Vote_AddVote($oBsVote) and $oEngine->Bs_UpdateBs($oBs)) {
								
								$bStateError=false;
								$sMsgTitle=$oEngine->Lang_Get('attention');
								$sMsg = $iValue==0 ? $oEngine->Lang_Get('topic_vote_ok_abstain') : $oEngine->Lang_Get('topic_vote_ok');
								$iRating=$oBs->getRating();
							} else {
								$sMsgTitle=$oEngine->Lang_Get('error');
								$sMsg=$oEngine->Lang_Get('system_error');
							}
						} else {
							$sMsgTitle=$oEngine->Lang_Get('attention');
							$sMsg=$oEngine->Lang_Get('system_error');
						}
					} else {
						$sMsgTitle=$oEngine->Lang_Get('attention');
						$sMsg=$oEngine->Lang_Get('topic_vote_error_acl');
					}
				} else {
					$sMsgTitle=$oEngine->Lang_Get('attention');
					$sMsg=$oEngine->Lang_Get('topic_vote_error_time');
				}
			} else {
				$sMsgTitle=$oEngine->Lang_Get('attention');
				$sMsg=$oEngine->Lang_Get('topic_vote_error_already');
			}
		} else {
			$sMsgTitle=$oEngine->Lang_Get('attention');
			$sMsg=$oEngine->Lang_Get('topic_vote_error_self');   
		}
	} else {
		$sMsgTitle=$oEngine->Lang_Get('error');
		$sMsg=$oEngine->Lang_Get('system_error');
	}
} else {
	$sMsgTitle=$oEngine->Lang_Get('error');
	$sMsg=$oEngine->Lang_Get('need_authorization');
}


$GLOBALS['_RESULT'] = array(
"bStateError"     => $bStateError,
"iRating"   => $iRating,
"sMsgTitle"   => $sMsgTitle,
"sMsg"   => $sMsg,
);

?>