<?php

set_include_path(get_include_path().PATH_SEPARATOR.dirname(dirname(dirname(__FILE__))));
$sDirRoot=dirname(dirname(dirname(__FILE__)));
require_once($sDirRoot."/config/config.ajax.php");

$iType=getRequest('type',null,'post');
$idBa=getRequest('idBa',null,'post');
$bStateError=true;
$sMsg='';
$sMsgTitle='';
$bState=false;
if ($oEngine->User_IsAuthorization()) {
	if (in_array($iType,array('1','0'))) {
		if ($oBa=$oEngine->Ba_GetBaById($idBa)) {
			$oUserCurrent=$oEngine->User_GetUserCurrent();
			$oFavouriteBa=$oEngine->Ba_GetFavouriteBa($oBa->getId(),$oUserCurrent->getId());
			if (!$oFavouriteBa and $iType) {
				$oFavouriteBaNew=Engine::GetEntity('Favourite',
					array(
						'target_id'      => $oBa->getId(),
						'user_id'        => $oUserCurrent->getId(),
						'target_type'    => 'ba',
						'target_publish' => $oBa->getPublish()
					)
				);
				if ($oEngine->Ba_AddFavouriteBa($oFavouriteBaNew)) {
					$bStateError=false;
					$sMsgTitle=$oEngine->Lang_Get('attention');
					$sMsg=$oEngine->Lang_Get('topic_favourite_add_ok');
					$bState=true;
				} else {
					$sMsgTitle=$oEngine->Lang_Get('error');
					$sMsg=$oEngine->Lang_Get('system_error');
				}
			}
			if (!$oFavouriteBa and !$iType) {
				$sMsgTitle=$oEngine->Lang_Get('error');
				$sMsg=$oEngine->Lang_Get('topic_favourite_add_no');
			}
			if ($oFavouriteBa and $iType) {
				$sMsgTitle=$oEngine->Lang_Get('error');
				$sMsg=$oEngine->Lang_Get('topic_favourite_add_already');
			}
			if ($oFavouriteBa and !$iType) {
				if ($oEngine->Ba_DeleteFavouriteBa($oFavouriteBa)) {
					$bStateError=false;
					$sMsgTitle=$oEngine->Lang_Get('attention');
					$sMsg=$oEngine->Lang_Get('topic_favourite_del_ok');
					$bState=false;
				} else {
					$sMsgTitle=$oEngine->Lang_Get('error');
					$sMsg=$oEngine->Lang_Get('system_error');
				}
			}
		} else {
			$sMsgTitle=$oEngine->Lang_Get('error');
			$sMsg=$oEngine->Lang_Get('system_error');
		}
	} else {
		$sMsgTitle=$oEngine->Lang_Get('error');
		$sMsg=$oEngine->Lang_Get('system_error');
	}
} else {
	$sMsgTitle=$oEngine->Lang_Get('error');
	$sMsg=$oEngine->Lang_Get('need_authorization');
}


$GLOBALS['_RESULT'] = array(
"bStateError"     => $bStateError,
"bState"   => $bState,
"sMsgTitle"   => $sMsgTitle,
"sMsg"   => $sMsg,
);

?>