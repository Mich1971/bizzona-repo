<?php

set_include_path(get_include_path().PATH_SEPARATOR.dirname(dirname(dirname(__FILE__))));
$sDirRoot=dirname(dirname(dirname(__FILE__)));
require_once($sDirRoot."/config/config.ajax.php");

$iValue=getRequest('value',null,'post');
$bStateError=true;
$sMsg='';
$sMsgTitle=''; 
$iRating=0;
if ($oEngine->User_IsAuthorization()) {
	if ($oBa=$oEngine->Ba_GetBaById(getRequest('idBa',null,'post'))) {
		
		$oUserCurrent=$oEngine->User_GetUserCurrent();
		if ($oBa->getUserId()!=$oUserCurrent->getId()) {
			
			if (!($oBaVote=$oEngine->Vote_GetVote($oBa->getId(),'ba',$oUserCurrent->getId()))) {
				
				if (strtotime($oBa->getDateAdd())>time()-Config::Get('acl.vote.topic.limit_time')) {
					
					if ($oEngine->ACL_CanVoteBa($oUserCurrent,$oBa) or $iValue==0) {
						
						if (in_array($iValue,array('1','-1','0'))) {
							
							$oBaVote=Engine::GetEntity('Vote');
							$oBaVote->setTargetId($oBa->getId());
							$oBaVote->setTargetType('ba');
							$oBaVote->setVoterId($oUserCurrent->getId());
							$oBaVote->setDirection($iValue);
							$oBaVote->setDate(date("Y-m-d H:i:s"));
							
							$iVal=0;
							if ($iValue!=0) {
								$iVal=(float)$oEngine->Rating_VoteBa($oUserCurrent,$oBa,$iValue);
							}

							$oBaVote->setValue($iVal);
							$oBa->setCountVote($oBa->getCountVote()+1);
							
							if ($oEngine->Vote_AddVote($oBaVote) and $oEngine->Ba_UpdateBa($oBa)) {
								
								$bStateError=false;
								$sMsgTitle=$oEngine->Lang_Get('attention');
								$sMsg = $iValue==0 ? $oEngine->Lang_Get('topic_vote_ok_abstain') : $oEngine->Lang_Get('topic_vote_ok');
								$iRating=$oBa->getRating();
								
							} else {
								$sMsgTitle=$oEngine->Lang_Get('error');
								$sMsg=$oEngine->Lang_Get('system_error');
							}
						} else {
							$sMsgTitle=$oEngine->Lang_Get('attention');
							$sMsg=$oEngine->Lang_Get('system_error');
						}
					} else {
						$sMsgTitle=$oEngine->Lang_Get('attention');
						$sMsg=$oEngine->Lang_Get('topic_vote_error_acl');
					}
				} else {
					$sMsgTitle=$oEngine->Lang_Get('attention');
					$sMsg=$oEngine->Lang_Get('topic_vote_error_time');
				}
			} else {
				$sMsgTitle=$oEngine->Lang_Get('attention');
				$sMsg=$oEngine->Lang_Get('ba_vote_error_already');
			}
		} else {
			$sMsgTitle=$oEngine->Lang_Get('attention');
			$sMsg=$oEngine->Lang_Get('qa_vote_error_self');   
		}
	} else {
		$sMsgTitle=$oEngine->Lang_Get('error');
		$sMsg=$oEngine->Lang_Get('system_error');
	}
} else {
	$sMsgTitle=$oEngine->Lang_Get('error');
	$sMsg=$oEngine->Lang_Get('need_authorization');
}


$GLOBALS['_RESULT'] = array(
"bStateError"     => $bStateError,
"iRating"   => $iRating,
"sMsgTitle"   => $sMsgTitle,
"sMsg"   => $sMsg,
);

?>