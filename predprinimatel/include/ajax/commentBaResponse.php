<?php
set_include_path(get_include_path().PATH_SEPARATOR.dirname(dirname(dirname(__FILE__))));
$sDirRoot=dirname(dirname(dirname(__FILE__)));
require_once($sDirRoot."/config/config.ajax.php");

//mail("eugenekurilov@gmail.com", "ok", "ok");

$idCommentLast=getRequest('idCommentLast',null,'post');
$idBa=getRequest('idTarget',null,'post');
$bStateError=true;
$sMsg='';
$sMsgTitle='';
$iMaxIdComment=0;
$aComments=array();

if ($oEngine->User_IsAuthorization()) {
	$oUserCurrent=$oEngine->User_GetUserCurrent();
	
	if ($oBa=$oEngine->Ba_GetBaById($idBa)) {		
		
		$aReturn=$oEngine->Comment_GetCommentsNewByTargetId($oBa->getId(),'ba',$idCommentLast);

		$iMaxIdComment=$aReturn['iMaxIdComment'];
		
		$oBaRead=Engine::GetEntity('Ba_BaRead');
		$oBaRead->setBaId($oBa->getId());
		$oBaRead->setUserId($oUserCurrent->getId());
		$oBaRead->setCommentCountLast($oBa->getCountComment());
		$oBaRead->setCommentIdLast($iMaxIdComment);
		$oBaRead->setDateRead(date("Y-m-d H:i:s"));
		$oEngine->Ba_SetBaRead($oBaRead);		
		
		$aCmts=$aReturn['comments'];
		
		if ($aCmts and is_array($aCmts)) {
			foreach ($aCmts as $aCmt) {
				
				$aComments[]=array(
					'html' => $aCmt['html'],
					'idParent' => $aCmt['obj']->getPid(),
					'id' => $aCmt['obj']->getId(),
				);
			}
		}
		
		$bStateError=false;		
		
	} else {
		$sMsgTitle=$oEngine->Lang_Get('error');
		$sMsg=$oEngine->Lang_Get('system_error');
	}

	
} else {
	$sMsgTitle=$oEngine->Lang_Get('error');
	$sMsg=$oEngine->Lang_Get('need_authorization');
}

$GLOBALS['_RESULT'] = array(
"bStateError"     => $bStateError,
"sMsgTitle"   => $sMsgTitle,
"sMsg"   => $sMsg,
"aComments" => $aComments,
"iMaxIdComment" => $iMaxIdComment
);
?>