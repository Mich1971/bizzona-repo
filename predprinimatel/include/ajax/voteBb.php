<?php

set_include_path(get_include_path().PATH_SEPARATOR.dirname(dirname(dirname(__FILE__))));
$sDirRoot=dirname(dirname(dirname(__FILE__)));
require_once($sDirRoot."/config/config.ajax.php");

$iValue=getRequest('value',null,'post');
$bStateError=true;
$sMsg='';
$sMsgTitle=''; 
$iRating=0;
if ($oEngine->User_IsAuthorization()) {
	if ($oBb=$oEngine->Bb_GetBbById(getRequest('idBb',null,'post'))) {

		$oUserCurrent=$oEngine->User_GetUserCurrent();
		if ($oBb->getUserId()!=$oUserCurrent->getId()) {
			
			if (!($oBbVote=$oEngine->Vote_GetVote($oBb->getId(),'bb',$oUserCurrent->getId()))) {
				
				if (strtotime($oBb->getDateAdd())>time()-Config::Get('acl.vote.topic.limit_time')) {
					
					if ($oEngine->ACL_CanVoteBb($oUserCurrent,$oBb) or $iValue==0) {
						
						if (in_array($iValue,array('1','-1','0'))) {
							
							$oBbVote=Engine::GetEntity('Vote');
							$oBbVote->setTargetId($oBb->getId());
							$oBbVote->setTargetType('bb');
							$oBbVote->setVoterId($oUserCurrent->getId());
							$oBbVote->setDirection($iValue);
							$oBbVote->setDate(date("Y-m-d H:i:s"));
							
							$iVal=0;
							if ($iValue!=0) {
								$iVal=(float)$oEngine->Rating_VoteBb($oUserCurrent,$oBb,$iValue);
							}
							
							$oBbVote->setValue($iVal);
							$oBb->setCountVote($oBb->getCountVote()+1);
							
							if ($oEngine->Vote_AddVote($oBbVote) and $oEngine->Bb_UpdateBb($oBb)) {
								
								$bStateError=false;
								$sMsgTitle=$oEngine->Lang_Get('attention');
								$sMsg = $iValue==0 ? $oEngine->Lang_Get('topic_vote_ok_abstain') : $oEngine->Lang_Get('topic_vote_ok');
								$iRating=$oBb->getRating();
								
							} else {
								$sMsgTitle=$oEngine->Lang_Get('error');
								$sMsg=$oEngine->Lang_Get('system_error');
							}
						} else {
							$sMsgTitle=$oEngine->Lang_Get('attention');
							$sMsg=$oEngine->Lang_Get('system_error');
						}
					} else {
						$sMsgTitle=$oEngine->Lang_Get('attention');
						$sMsg=$oEngine->Lang_Get('topic_vote_error_acl');
					}
				} else {
					$sMsgTitle=$oEngine->Lang_Get('attention');
					$sMsg=$oEngine->Lang_Get('topic_vote_error_time');
				}
			} else {
				$sMsgTitle=$oEngine->Lang_Get('attention');
				$sMsg=$oEngine->Lang_Get('bb_vote_error_already');
			}
		} else {
			$sMsgTitle=$oEngine->Lang_Get('attention');
			$sMsg=$oEngine->Lang_Get('qa_vote_error_self');   
		}
	} else {
		$sMsgTitle=$oEngine->Lang_Get('error');
		$sMsg=$oEngine->Lang_Get('system_error');
	}
} else {
	$sMsgTitle=$oEngine->Lang_Get('error');
	$sMsg=$oEngine->Lang_Get('need_authorization');
}


$GLOBALS['_RESULT'] = array(
"bStateError"     => $bStateError,
"iRating"   => $iRating,
"sMsgTitle"   => $sMsgTitle,
"sMsg"   => $sMsg,
);

?>