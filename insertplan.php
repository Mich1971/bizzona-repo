<?
  global $DATABASE;
  
  include_once("./phpset.inc");
  include_once("./engine/functions.inc"); 
  AssignDataBaseSetting();
  
  include_once("./engine/class.category.inc"); 
  $category = new Category();

  include_once("./engine/class.subcategory.inc"); 
  $subCategory = new SubCategory();
  
  include_once("./engine/class.country.inc"); 
  $country = new Country();

  include_once("./engine/class.city.inc"); 
  $city = new City();

  include_once("./engine/class.bizplan_company.inc"); 
  $bizplan_company = new BizPlanCompany();
  
  include_once("./engine/class.bizplan.inc");
  $bizplan = new BizPlan();

  include("./engine/FCKeditor/fckeditor.php"); 

  require_once("./libs/Smarty.class.php");
  $smarty = new Smarty;
  $smarty->template_dir = "./templates";
  $smarty->compile_dir  = "./templates_c";
  //$smarty->cache_dir = "./cache";
  $smarty->compile_check = true;
  $smarty->debugging     = false;

  
  $data = array();
  $data["offset"] = 0;
  $data["rowCount"] = 0;
  $data["sort"] = "ID";
    
  $listCategory = $category->Select($data);
  $smarty->assign("listCategory", $listCategory);
  
  $listSubCategory = $subCategory->Select($data);
  $smarty->assign("listSubCategory", $listSubCategory);

  $companylist = $bizplan_company->SelectByStatus(OPEN_BIZPLAN_COMPANY);
  $smarty->assign("companylist", $companylist);
  
  
  
  $data = array();
  $data["offset"] = 0;
  $data["rowCount"] = 0;
  $data["sort"] = "Count";
  $result = $category->Select($data);
  $smarty->assign("categorylist", $result);
  
  
  if (isset($_POST) && sizeof($_POST)) {
  	
  	$error = false;
  	$error_text = "";
  	
  	$t_description = htmlspecialchars(trim($_POST["description"]), ENT_QUOTES);
  	$_POST["description"] = $t_description;
  	
  	$t_shortDescription = htmlspecialchars(trim($_POST["shortDescription"]), ENT_QUOTES);
  	$_POST["shortDescription"] = $t_shortDescription;
  	
  	$t_name = htmlspecialchars(trim($_POST["name"]), ENT_QUOTES);
  	$_POST["name"] = $t_name;
  	
  	$t_cost = htmlspecialchars(trim($_POST["cost"]), ENT_QUOTES);
  	$_POST["cost"] = $t_cost;
  	
  	$t_count_site = htmlspecialchars(trim($_POST["count_site"]), ENT_QUOTES);
  	$_POST["count_site"] = $t_count_site;
  	
  	$t_company_id = htmlspecialchars(trim($_POST["company_id"]), ENT_QUOTES);
  	$_POST["company_id"] = $t_company_id;
  	
  	$t_category_id = htmlspecialchars(trim($_POST["category_id"]), ENT_QUOTES);
  	$_POST["category_id"] = $t_category_id;
  	
  	$t_company_text = htmlspecialchars(trim($_POST["company_text"]), ENT_QUOTES);
  	$_POST["company_text"] = $t_company_text;
  	
  	$t_company_name = htmlspecialchars(trim($_POST["company_name"]), ENT_QUOTES);
  	$_POST["company_name"] = $t_company_name;
  	
  	$t_company_email = htmlspecialchars(trim($_POST["company_email"]), ENT_QUOTES);
  	$_POST["company_email"] = $t_company_email;
  	
  	$t_company_phone = htmlspecialchars(trim($_POST["company_phone"]), ENT_QUOTES);
  	$_POST["company_phone"] = $t_company_phone;
  	
  	$t_company_fml = htmlspecialchars(trim($_POST["company_fml"]), ENT_QUOTES);
  	$_POST["company_fml"] = $t_company_fml;
  	
  	$t_display_id = htmlspecialchars(trim($_POST["display_id"]), ENT_QUOTES);
  	$_POST["display_id"] = $t_display_id;
  	
  	$t_language_id = htmlspecialchars(trim($_POST["language_id"]), ENT_QUOTES);
  	$_POST["language_id"] = $t_language_id;
  	

  	if (strlen(trim($t_name)) <= 0)
  	{
  		$error = true;	
  		$text_error = "������ ����� ������ \"�������� �����\"";
  		$smarty->assign("ErrorInput_name", true);
  		
  	}
  	else if(strlen(trim($t_cost)) <= 0)
  	{
  		$error = true;
  		$text_error = "������ ����� ������ \"���������\"";
  		$smarty->assign("ErrorInput_cost", true);
  	}
  	else if(strlen(trim($t_count_site)) <= 0)
  	{
  		$error = true;
  		$text_error = "������ ����� ������ \"���������� �������\"";
  		$smarty->assign("ErrorInput_count_site", true);
  	}
  	else if ($t_company_id == "0" && 
  	           (
  			     strlen(trim($t_company_text)) <= 0 ||
  			     strlen(trim($t_company_name)) <= 0 ||
  			     strlen(trim($t_company_email)) <= 0 ||
  			     strlen(trim($t_company_fml)) <= 0 ||
  			     strlen(trim($t_company_phone)) <= 0
  			   )
  			)
  	{
  		
  		$error = true;
  		$text_error = "������ ����� ������ \"�����������\"";
  		$smarty->assign("othercompanyerror", true);
  		
  		if(strlen(trim($t_company_name)) <= 0)
  		{
  			$smarty->assign("ErrorInput_company_name", true);
  		}
  		else if(strlen(trim($t_company_text)) <= 0)
  		{
  			$smarty->assign("ErrorInput_company_text", true);
  		}
  		else if(strlen(trim($t_company_email)) <= 0)
  		{
  			$smarty->assign("ErrorInput_company_email", true);
  		}
  		else if(strlen(trim($t_company_phone)) <= 0)
  		{
  			$smarty->assign("ErrorInput_company_phone", true);
  		}
  		else if(strlen(trim($t_company_fml)) <= 0)
  		{
  			$smarty->assign("ErrorInput_company_fml", true);
  		}
  		
  	}
  	else if(strlen(trim($t_shortDescription)) <= 0)
  	{
  		$error = true;
  		$text_error = "������ ����� ������ \"������� ��������\"";
  		$smarty->assign("ErrorInput_shortDescription", true);
  	} 
  	
  	else if(strlen(trim($t_description)) <= 0)
  	{
  		$error = true;
  		$text_error = "������ ����� ������ \"��������� ��������\"";
  		$smarty->assign("ErrorInput_description", true);
  	} 
  	
  	
  	if(!$error)
  	{
  		$bizplan->description = $t_description;
  		$bizplan->shortDescription = $t_shortDescription;
  		$bizplan->name = $t_name;
  		$bizplan->cost = $t_cost;
  		$bizplan->count_site = $t_count_site;
  		$bizplan->company_id = $t_company_id;
  		$bizplan->category_id = $t_category_id;
  		$bizplan->display_id = $t_display_id;
  		$bizplan->language_id = $t_language_id;
  		$bizplan->status_id = NEW_BIZPLAN;
  		$bizplan->Insert();
  		
  		$bizplan_company->name = $t_company_name;
  		$bizplan_company->description = $t_company_text;
  		$bizplan_company->email = $t_company_email;
  		$bizplan_company->fml = $t_company_fml;
  		$bizplan_company->phone = $t_company_phone;
  		$bizplan_company->Insert();
  		
  		unset($_POST);
  		$smarty->assign("text_success", "������ ���� ��� ��������. � ������� ���� �� ����� ���������� � �����������.");


                //$headers1 = 'BizZONA.RU: new looked' . "\r\n" . 'Reply-To:  ' . "\r\n" . 'X-Mailer: PHP/' . phpversion();
                //mail("staff@bizzona.ru", "Posted BizPLAN", "PHP_SELF: ".$_SERVER["PHP_SELF"]."\r\n"." HTTP_HOST: ".$_SERVER["HTTP_HOST"]."\r\n"."HTTP_REFERER: ".$_SERVER["HTTP_REFERER"]."\r\n"."REMOTE_ADDR: ".$_SERVER["REMOTE_ADDR"]."\r\n" , $headers1);


  	}
  	else
  	{
  		$smarty->assign("text_error", $text_error);
  	}
  	
  }
  
?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>

  <HEAD>
      <title>���������� ������ ���� - ������� � ������� �������. ������� ������ - ������ ����</title>		
	<META NAME="Description" CONTENT="������� �������, ������� �������, ������� ������, ������� �������, ������� �������� �������, ������� �������, ������� ��������,  ������� ��������, ������� ��������, ���� ������� ������?">
        <meta name="Keywords" content="������� �������, ������� �������, ������� ������, ������� �������, ������� �������� �������, ������� �������, ������� ��������,  ������� ��������, ������� ��������, ���� ������� ������?, Ready business, �����������, �������� � ��������, ���������, ����������, �����������"> 
	<meta http-equiv="content-type" content="text/html; charset=windows-1251"/>
        <LINK href="./general.css" type="text/css" rel="stylesheet">
  </HEAD>


<?
	$smarty->display("./site/header.tpl");
?>
  


<table class="w" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td width="100%">

         <table width='100%' height='100%' cellpadding="0" cellspacing="0">
           <tr>
             <td valign='top' width='60%' height='100%'>
               <?
                  $smarty->display("./site/insertplan1.tpl");
               ?>
             </td>
           </tr>
         </table>
        
        </td>
    </tr>
</table>

<style>
 td a 
 {
   color:black;
 }
</style>


<div style="padding-top:4pt;padding-bottom:5pt;padding-right:10pt;padding-left:10pt;font-size:10pt;font-family:arial;">
	<table cellpadding="0" cellspacing="0" border="0"  style="width:100%;padding-bottom:2pt;" bgcolor="#eeeee0">
    	<tr>
        	<td style="background-image:url(<?=$_SERVER["host_name"]?>images/d1.gif); background-repeat:no-repeat;width:5px;height:5px; background-position:left top;">&nbsp;</td>
            <td rowspan="2" valign="middle" align="center"  style="font-size:8pt; font-family:Arial; color:black;">
			<?
     			define('_SAPE_USER', 'b91a4da0b21c4d197163d613e54e8d28'); 
     			require_once($_SERVER['DOCUMENT_ROOT'].'/'._SAPE_USER.'/sape.php'); 
     			$sape = new SAPE_client();
     			echo $sape->return_links();
			?>
            </td>
            <td style="background-image:url(<?=$_SERVER["host_name"]?>images/d2.gif); background-repeat:no-repeat;width:5px;height:5px;background-position:right top;">&nbsp;</td>
        </tr>
        <tr>
            <td style="background-image:url(<?=$_SERVER["host_name"]?>images/d4.gif); background-repeat:no-repeat;width:5px;height:5px; background-position:left bottom;">&nbsp;</td>
            <td style="background-image:url(<?=$_SERVER["host_name"]?>images/d3.gif); background-repeat:no-repeat;width:5px;height:5px; background-position:right bottom;">&nbsp;</td>
        </tr>
    </table>
</div>


<?
	$smarty->display("./site/footer.tpl");
?>


</body>
</html>


