<?php
    //set_time_limit(300);
    //ini_set("memory_limit","92M");
    global $DATABASE;
    include_once("../phpset.inc"); 
    include_once("../engine/functions.inc");
    include_once("../engine/class.HistoryRequestBrokers.inc");
    
    ini_set("display_startup_errors", "on");
    ini_set("display_errors", "on");
    ini_set("register_globals", "on");

    AssignDataBaseSetting("../config.ini");

    include_once("../engine/class.deloshop.inc");
    include_once("../engine/class.company.inc");    
    
    $history = new HistoryRequestBrokers();
    $history->typeId = HistoryRequestBrokers::$TYPE_IMPORT;
    $history->statusId = 1;

    $deloshop = new Deloshop();
    
    $company = new Company();  
    $company->Id = $deloshop->brokerId;
    
    if($company->GetAccess() == 2) {
        echo "<h1>���������� �������� �������������</h1>";
        return;
    }
    
    
    
    try { 
        $history->brokerId = $deloshop->brokerId;
        $history->message = '������ �� �������';
        $deloshop->ImportData(); 
        $history->countProcessed = $deloshop->countProcessed;
        
    } catch (Exception  $e) {

        $history->statusId = -1;
        $history->message = $e->getMessage();
    }

    $history->Insert();    
    
    
?>