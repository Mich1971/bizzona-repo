<?php
	global $DATABASE;
	include_once("../phpset.inc");
	include_once("../engine/functions.inc");

    ini_set("display_startup_errors", "on");
    ini_set("display_errors", "on");
    ini_set("register_globals", "on");
 
	AssignDataBaseSetting("../config.ini");
	
	include_once("../engine/class.templateevent.inc");

	$templateEvent = new TemplateEvent();	

	
	$aParameters['typeEventId'] = '5';
	
	$listCountdays = $templateEvent->GetListCountdays($aParameters['typeEventId']);
	
	foreach ($listCountdays as $k => $v) {
		
		
		$aParameters['countdays'] = (int)$v['countdays'];
		$aParameters['cityListIds'] = $v['cityListIds'];
		$aParameters['minDays'] = $aParameters['countdays']; 
		$aParameters['maxDays'] = $aParameters['countdays'] + 1; 
		
	
		$aParameters['typeEventObjectId'] = 1; //sell
		$aParameters['isBrokerId'] = 0;
		$templateEvent->AutoInitEvent($aParameters);
		$aParameters['isBrokerId'] = 1;
		$templateEvent->AutoInitEvent($aParameters);	
	
		
		$aParameters['typeEventObjectId'] = 2; // buy
		$aParameters['isBrokerId'] = 0;
		$templateEvent->AutoInitEvent($aParameters);
		$aParameters['isBrokerId'] = 1;
		$templateEvent->AutoInitEvent($aParameters);	
	
		
		$aParameters['typeEventObjectId'] = 3; // invest
		$aParameters['isBrokerId'] = 0;
		$templateEvent->AutoInitEvent($aParameters);
		$aParameters['isBrokerId'] = 1;
		$templateEvent->AutoInitEvent($aParameters);	
	
		$aParameters['typeEventObjectId'] = 4; // equipment
		$aParameters['isBrokerId'] = 0;
		$templateEvent->AutoInitEvent($aParameters);
		$aParameters['isBrokerId'] = 1;
		$templateEvent->AutoInitEvent($aParameters);	

		$aParameters['typeEventObjectId'] = 5; // coopbiz
		$aParameters['isBrokerId'] = 0;
		$templateEvent->AutoInitEvent($aParameters);
		$aParameters['isBrokerId'] = 1;
		$templateEvent->AutoInitEvent($aParameters);	

		$aParameters['typeEventObjectId'] = 6; // room
		$aParameters['isBrokerId'] = 0;	
		$templateEvent->AutoInitEvent($aParameters);
		$aParameters['isBrokerId'] = 1;	
		$templateEvent->AutoInitEvent($aParameters);		
		
	}
?>