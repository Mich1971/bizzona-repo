<?php

    global $DATABASE;
    include_once("../phpset.inc");
	include_once("../engine/functions.inc");
	include_once("../engine/class.db.inc");

  	ini_set("display_startup_errors", "on");
  	ini_set("display_errors", "on");
  	ini_set("register_globals", "on");

  	AssignDataBaseSetting("../config.ini");

	$db = new DB($DATABASE["HOSTNAME"], $DATABASE["DATABASENAME"], $DATABASE["DATABASEUSER"], $DATABASE["DATABASEPASSWORD"]);
	
	$usd = 32;
	$aud = 26;
	$eur = 40;
	
	$result = $db->Select("update Sell set costseach=CostID*".$usd." where 1 and cCostID='usd';");
	$result = $db->Select("update Sell set costseach=CostID*".$aud." where 1 and cCostID='aud';");
	$result = $db->Select("update Sell set costseach=CostID*".$eur." where 1 and cCostID='eur';");
	$result = $db->Select("update Sell set costseach=CostID where 1 and cCostID='rub';");

	$result = $db->Select("update Sell set min_monthaveturnseacr=MonthAveTurnID*".$usd." where 1 and cMonthAveTurnID='usd';");
	$result = $db->Select("update Sell set min_monthaveturnseacr=MonthAveTurnID*".$aud." where 1 and cMonthAveTurnID='aud';");
	$result = $db->Select("update Sell set min_monthaveturnseacr=MonthAveTurnID*".$eur." where 1 and cMonthAveTurnID='eur';");
	$result = $db->Select("update Sell set min_monthaveturnseacr=MonthAveTurnID where 1 and cMonthAveTurnID='rub';");	
	
	$result = $db->Select("update Sell set max_monthaveturnseacr=MaxMonthAveTurnID*".$usd." where 1 and cMaxMonthAveTurnID='usd';");
	$result = $db->Select("update Sell set max_monthaveturnseacr=MaxMonthAveTurnID*".$aud." where 1 and cMaxMonthAveTurnID='aud';");
	$result = $db->Select("update Sell set max_monthaveturnseacr=MaxMonthAveTurnID*".$eur." where 1 and cMaxMonthAveTurnID='eur';");
	$result = $db->Select("update Sell set max_monthaveturnseacr=MaxMonthAveTurnID where 1 and cMaxMonthAveTurnID='rub';");	
	
	$result = $db->Select("update Sell set max_monthaveturnseacr=min_monthaveturnseacr where 1 and max_monthaveturnseacr <= 0;");	
	$result = $db->Select("update Sell set min_monthaveturnseacr=max_monthaveturnseacr where 1 and min_monthaveturnseacr <= 0;");	

	
	$result = $db->Select("update Sell set min_profitseacrh=ProfitPerMonthID*".$usd." where 1 and cProfitPerMonthID='usd';");
	$result = $db->Select("update Sell set min_profitseacrh=ProfitPerMonthID*".$aud." where 1 and cProfitPerMonthID='aud';");
	$result = $db->Select("update Sell set min_profitseacrh=ProfitPerMonthID*".$eur." where 1 and cProfitPerMonthID='eur';");
	$result = $db->Select("update Sell set min_profitseacrh=ProfitPerMonthID where 1 and cProfitPerMonthID='rub';");	
	
	$result = $db->Select("update Sell set max_profitseacrh=MaxProfitPerMonthID*".$usd." where 1 and cMaxProfitPerMonthID='usd';");
	$result = $db->Select("update Sell set max_profitseacrh=MaxProfitPerMonthID*".$aud." where 1 and cMaxProfitPerMonthID='aud';");
	$result = $db->Select("update Sell set max_profitseacrh=MaxProfitPerMonthID*".$eur." where 1 and cMaxProfitPerMonthID='eur';");
	$result = $db->Select("update Sell set max_profitseacrh=MaxProfitPerMonthID where 1 and cMaxProfitPerMonthID='rub';");	
	
	$result = $db->Select("update Sell set max_profitseacrh=min_profitseacrh where 1 and max_profitseacrh <= 0;");	
	$result = $db->Select("update Sell set min_profitseacrh=max_profitseacrh where 1 and min_profitseacrh <= 0;");	
	
	
	$result = $db->Select("update buyer set coststartseach=cost_start*".$usd." where 1 and currencyID='usd';");
	$result = $db->Select("update buyer set coststartseach=cost_start*".$aud." where 1 and currencyID='aud';");
	$result = $db->Select("update buyer set coststartseach=cost_start*".$eur." where 1 and currencyID='eur';");
	$result = $db->Select("update buyer set coststartseach=cost_start where 1 and currencyID='rub';");	
	
	$result = $db->Select("update buyer set coststopseach=cost_stop*".$usd." where 1 and currencyID='usd';");
	$result = $db->Select("update buyer set coststopseach=cost_stop*".$aud." where 1 and currencyID='aud';");
	$result = $db->Select("update buyer set coststopseach=cost_stop*".$eur." where 1 and currencyID='eur';");
	$result = $db->Select("update buyer set coststopseach=cost_stop where 1 and currencyID='rub';");	
	
	
	$result = $db->Select("update equipment set costseach=cost*".$usd." where 1 and ccost='usd';");
	$result = $db->Select("update equipment set costseach=cost*".$aud." where 1 and ccost='aud';");
	$result = $db->Select("update equipment set costseach=cost*".$eur." where 1 and ccost='eur';");
	$result = $db->Select("update equipment set costseach=cost where 1 and ccost='rub';");

	
	$result = $db->Select("update Room set costseach=cost*".$usd." where 1 and cCostId=1;");
	$result = $db->Select("update Room set costseach=cost*".$eur." where 1 and cCostId=3;");
	$result = $db->Select("update Room set costseach=cost where 1 and cCostId=2;");	
	
	
   	$result = $db->Select("update investments set costseach=cost*".$usd." where 1 and ccost='usd';");
    $result = $db->Select("update investments set costseach=cost*".$aud." where 1 and ccost='aud';");
    $result = $db->Select("update investments set costseach=cost*".$eur." where 1 and ccost='eur';");
    $result = $db->Select("update investments set costseach=cost where 1 and ccost='rub';");	
	
?>