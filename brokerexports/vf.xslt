<?xml version="1.0"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="xml" indent="yes"/>
    <xsl:template match="ads">
        <ads>
            <xsl:apply-templates/>
        </ads>
    </xsl:template>
    <xsl:template match="ad">
        <ad>
            <items>
            <item name='source-id'><xsl:value-of select="@source-id"/></item>  
            <xsl:apply-templates/>
            </items>  
        </ad>
    </xsl:template>
    <xsl:template match="items">
        
        
           <xsl:for-each select="item"> 
                <xsl:if test="@name = 'phone'">
                  <item name='phone'><xsl:value-of select="."/></item>
		</xsl:if>
                <xsl:if test="@name = 'contact'">
                  <item name='contact'><xsl:value-of select="."/></item>
		</xsl:if>
                <xsl:if test="@name = 'offer'">
                  <item name='offer'><xsl:value-of select="."/></item>
		</xsl:if>
                <xsl:if test="@name = 'title'">
                  <item name='title'><xsl:value-of select="."/></item>
		</xsl:if>
                <xsl:if test="@name = 'description'">
                  <item name='description'><xsl:value-of select="."/></item>
		</xsl:if>
           </xsl:for-each> 
        
    </xsl:template>

    <xsl:template match="price">

      <item currency='USD'><xsl:value-of select="."/></item>  
          
    </xsl:template>

    <xsl:template match="fotos">
    </xsl:template>

    <!-- xsl:template match="/ads">
       <item name='source-id'><xsl:value-of select="@source-id"/></item> 
    </xsl:template -->


</xsl:stylesheet>