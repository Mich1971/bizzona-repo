<?php
    global $DATABASE;
    include_once("../phpset.inc");
    include_once("../engine/functions.inc");
    include_once("../engine/class.HistoryRequestBrokers.inc");    
    
    ini_set("display_startup_errors", "off");
    ini_set("display_errors", "off");
    ini_set("register_globals", "on");

    AssignDataBaseSetting("../config.ini");

    include_once("../engine/class.ProdayBiznes.inc");
    
    $history = new HistoryRequestBrokers();
    $history->typeId = HistoryRequestBrokers::$TYPE_DELETE;
    $history->statusId = 1;
    
    try {
 
        $prodaybiznes = new ProdayBiznes();
        $prodaybiznes->DeleteData();

        $history->brokerId = $prodaybiznes->brokerId;
        $history->message = '������ �� �������';
        $history->countProcessed = $prodaybiznes->countProcessed;

    } catch (Exception  $e) {

        $history->statusId = -1;
        $history->message = $e->getMessage();
    }
    
    $history->Insert();    
    
    
    
    
?>