<?php
    //set_time_limit(300);
    //ini_set("memory_limit","92M");
    global $DATABASE;
    include_once("../phpset.inc");
    include_once("../engine/functions.inc");

    include_once("../engine/class.alertxmlpartner.inc");
    include_once("../engine/class.HistoryRequestBrokers.inc");
    
    ini_set("display_startup_errors", "off");
    ini_set("display_errors", "off");
    ini_set("register_globals", "off");
 
    AssignDataBaseSetting("../config.ini");

    include_once("../engine/class.ProdayBiznes.inc");
    include_once("../engine/class.company.inc");
    
    $history = new HistoryRequestBrokers();
    $history->typeId = HistoryRequestBrokers::$TYPE_IMPORT;
    $history->statusId = 1;

    $prodaybiznes = new ProdayBiznes();
    $company = new Company(); 
    $company->Id = $prodaybiznes->brokerId;
    
    if($company->GetAccess() == 2) {
        echo "<h1>���������� �������� �������������</h1>";
        return;
    }
    
    try {
        $prodaybiznes->ImportData();

        $history->brokerId = $prodaybiznes->brokerId;
        $history->message = '������ �� �������';

        $history->countProcessed = $prodaybiznes->countProcessed;

    } catch (Exception  $e) {

        $history->statusId = -1;
        $history->message = $e->getMessage();
        
        $_aEmails = AlertXmlPartner::$emails;
        $_aEmails[] = "vmelnikov@bk.ru";
        AlertXmlPartner::$emails = $_aEmails;   
        AlertXmlPartner::EmailAlert($e->getMessage());

    }
    
    $history->Insert();
    
?>