<?php
    //ini_set("memory_limit","1024M");	
    ini_set('max_execution_time', 300);
    ini_set('memory_limit', -1);
    
    //header('Content-Type: text/html; charset=windows-1251');
 
    global $DATABASE;
    include_once("../phpset.inc");
    include_once("../engine/functions.inc");
    include_once("../engine/class.alertxmlpartner.inc");
    include_once("../engine/class.HistoryRequestBrokers.inc");

    $count_sql_call = 0; 
    $start = get_formatted_microtime();   
    $base_memory_usage = memory_get_usage();	

    ini_set("display_startup_errors", "off");
    ini_set("display_errors", "off");
    ini_set("register_globals", "off");

    AssignDataBaseSetting("../config.ini");

    include_once("../engine/class.Skaniainvest.inc");
    include_once("../engine/class.company.inc");
    
    $history = new HistoryRequestBrokers();
    $history->typeId = HistoryRequestBrokers::$TYPE_IMPORT;
    $history->statusId = 1;

    $sk = new Skaniainvest();
    
    $company = new Company(); 
    $company->Id = $sk->brokerId;
    
    if($company->GetAccess() == 2) {
        echo "<h1>���������� �������� �������������</h1>";
        return;
    }
    
    try { 
        $history->brokerId = $sk->brokerId;
        $history->message = '������ �� �������';
        $sk->ImportData();
        $history->countProcessed = $sk->countProcessed;
        
    } catch (Exception  $e) {

        $history->statusId = -1;
        $history->message = $e->getMessage();
        
        //AlertXmlPartner::EmailAlert($e->getMessage());
        AlertXmlPartner::$emails = array("avk@skaniainvest.ru", "eugenekurilov@gmail.com", "bizzona.ru@gmail.com");
        AlertXmlPartner::EmailAlert($e->getMessage());
    }

    $history->Insert();
    
    $end = get_formatted_microtime(); 
    $total = $end - $start;
    echo "<center><span style='font-size:7pt;'>".round($total, 6)." : ".$count_sql_call." : ".$base_memory_usage." : ".memoryUsage(memory_get_usage(), $base_memory_usage)." </span><center>";	
?>
