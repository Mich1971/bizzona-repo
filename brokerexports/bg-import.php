<?php
	//set_time_limit(300);

	global $DATABASE;
	include_once("../phpset.inc");
	include_once("../engine/functions.inc");

    ini_set("display_startup_errors", "on");
    ini_set("display_errors", "on");
    ini_set("register_globals", "on");
 
	AssignDataBaseSetting("../config.ini");
	
	include_once("../engine/class.bg.inc");
	$bg = new BG();
	$bg->ImportData();
?>