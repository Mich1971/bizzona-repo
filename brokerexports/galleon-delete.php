<?php
    ini_set("memory_limit","92M");

    global $DATABASE;
    include_once("../phpset.inc");
    include_once("../engine/functions.inc");
    include_once("../engine/class.HistoryRequestBrokers.inc");
    
    ini_set("display_startup_errors", "off");
    ini_set("display_errors", "off");
    //ini_set("register_globals", "on");

    AssignDataBaseSetting("../config.ini");

    include_once("../engine/class.Galleon.inc");
    
    $history = new HistoryRequestBrokers();
    $history->typeId = HistoryRequestBrokers::$TYPE_DELETE;
    $history->statusId = 1;
     
    try {  

        $galleon = new Galleon();
        $history->brokerId = $galleon->brokerId;
        $history->message = '������ �� �������';
        $galleon->DeleteData(); 
        $history->countProcessed = $galleon->countProcessed;
        
    } catch (Exception  $e) {

        $history->statusId = -1;
        $history->message = $e->getMessage();
    }

    $history->Insert();
?>