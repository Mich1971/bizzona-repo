<?php
    //set_time_limit(300);
    //ini_set("memory_limit","92M");
    global $DATABASE;
    include_once("../phpset.inc");
    include_once("../engine/functions.inc");

    include_once("../engine/class.alertxmlpartner.inc");	
	
    ini_set("display_startup_errors", "on");
    ini_set("display_errors", "on");
    ini_set("register_globals", "on");
 
    AssignDataBaseSetting("../config.ini");
	
    include_once("../engine/class.deloprofit.inc");
    include_once("../engine/class.company.inc");

    $dp = new DeloProfit();
    $company = new Company(); 
    
    $company->Id = $dp->brokerId;
    
    if($company->GetAccess() == 2) {
        
        echo "<h1>���������� �������� �������������</h1>";
        
        return;
    }
    
    try {
	
        $dp->ImportData();
		
    } catch (Exception  $e) {
    	
    	AlertXmlPartner::EmailAlert($e->getMessage());
    	
    }
		
?>