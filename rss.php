<?

  header('Content-type: application/xml');
  echo "<?xml version=\"1.0\" encoding=\"windows-1251\" ?>\n"; 
 
   
  global $DATABASE;
  include_once("./phpset.inc");
  include_once("./engine/functions.inc"); 
  AssignDataBaseSetting();
  include_once("./engine/class.sell.inc"); 
  $sell = new Sell();
  include_once("./engine/class.city.inc"); 
  $city = new City();
  include_once("./engine/class.category.inc"); 
  $category = new Category();

  require_once("./libs/Smarty.class.php");
  $smarty = new Smarty;
  $smarty->template_dir = "./templates";
  $smarty->compile_dir  = "./templates_c";
  //$smarty->cache_dir = "./cache";
  $smarty->compile_check = true;
  $smarty->debugging     = false;
    

  //ini_set("display_startup_errors", "on");
  //ini_set("display_errors", "on");
  //ini_set("register_globals", "on");
  
 

?>
<?

  $aAction = array (1 => "category", 2 => "city", 3 => "hotbiz", 4 => "allbiz", 5 => "topbiz");

  $_GET["action"]  = trim($_GET["action"]);
  $_GET["ID"] = (isset($_GET["ID"]) ? intval($_GET["ID"]) : 0);

  $cacheCategoryObject = array();
  
  if (array_search($_GET["action"], $aAction)) {

     switch($_GET["action"]) { 
       case "city": {
         $dataCity["ID"] = $_GET["ID"];
         $objCity = $city->GetItem($dataCity);

         $info = new stdclass(); 
         $info->title         = "[BizZONA.RU] ������� ������� �������.";
         $info->link          = "http://www.bizzona.ru";
         $info->description   = "������� �������: ".$objCity->title." ";
         $info->category      = "Ready Business";
         $info->language      = "russia";
         $info->copyright     = "Copyright, BizZONA.ru, 2006 - 2011";
         $info->webmaster     = "webmaster@bizzona.ru";
         $info->generator     = "www.bizzona.ru";
         $info->url           = "http://www.bizzona.ru";
         $info->ttl           = "60";
         $info->imglink       = "http://www.bizzona.ru/images/ban.gif";
         $info->lastbuilddate = date("m.d.Y", time());

         $smarty->assign("info", $info);
         $inforss = $smarty->fetch('./site/inforss.tpl');
         $smarty->assign("inforss", $inforss);

         $select["CityID"] = $_GET["ID"];
         $select["offset"] = 0;
         $select["rowCount"] = 30;
         $select["sort"] = "ID";
         $select["StatusID"] = "-1";
         $result = $sell->Select($select);
         $content = '';
         if (is_array($result) && sizeof($result) > 0) {
           while(list($k, $v) = each($result)) {
             $dataCategory = array();
             $dataCategory["ID"] = $v->CategoryID;       
             $objCategory = $category->GetItem($dataCategory);
             $v->NameBizID    = htmlspecialchars($v->NameBizID);
             $v->SiteID       = htmlspecialchars($v->SiteID);
             $v->CityID       = $objCity->title; 
             $v->ShortDetailsID = strip_tags($v->ShortDetailsID); 
             //$v->ShortDetailsID = str_replace("&nbsp;", " ", $v->ShortDetailsID);
             //$v->ShortDetailsID = str_replace("&ndash;", " ", $v->ShortDetailsID);
             //$v->ShortDetailsID = str_replace("&laquo;", " ", $v->ShortDetailsID);
             //$v->ShortDetailsID = str_replace("&raquo;", " ", $v->ShortDetailsID);
             //$v->ShortDetailsID = str_replace("&sup2;", "2", $v->ShortDetailsID);
             //$v->ShortDetailsID = str_replace("&ldquo;", " ", $v->ShortDetailsID);
             //$v->ShortDetailsID = str_replace("&rdquo;", " ", $v->ShortDetailsID);
             //$v->ShortDetailsID = str_replace("&mdash;", " ", $v->ShortDetailsID);
             //$v->ShortDetailsID = str_replace("&sbquo;", " ", $v->ShortDetailsID);
             $v->ShortDetailsID = preg_replace("/&([a-zA-Z0-9]+;)/", " ", $v->ShortDetailsID);
             $v->category = $objCategory->Name;
             $v->link = ''.'http://www.bizzona.ru/detailsSell/'.$v->ID.'';
             $smarty->assign("row", $v);
             $content.=  $smarty->fetch('./site/itemrss.tpl');
           }
         }

         $smarty->assign("content", $content);
         $smarty->display("./site/rss.tpl");
         break;
       }
       case "allbiz": {

         $info = new stdclass(); 
         $info->title         = "[BizZONA.RU] ������� ������� �������.";
         $info->link          = "http://www.bizzona.ru";
         $info->description   = "������� ������. ������ ����";
         $info->category      = "Ready Business";
         $info->language      = "russia";
         $info->copyright     = "Copyright, BizZONA.ru, 2006 - 2011";
         $info->webmaster     = "webmaster@bizzona.ru";
         $info->generator     = "www.bizzona.ru";
         $info->url           = "http://www.bizzona.ru";
         $info->ttl           = "60";
         $info->imglink       = "http://www.bizzona.ru/images/ban.gif";
         $info->lastbuilddate = date("m.d.Y", time());
 
         $smarty->assign("info", $info);
         $inforss = $smarty->fetch('./site/inforss.tpl');
         $smarty->assign("inforss", $inforss);

         $select["offset"] = 0;
         $select["rowCount"] = 30;
         $select["sort"] = "ID";
         $select["StatusID"] = "-1";
         $result = $sell->Select($select);
         $content = '';
         if (is_array($result) && sizeof($result) > 0) {
           while(list($k, $v) = each($result)) {
             $dataCategory = array();
             $dataCategory["ID"] = $v->CategoryID;       
             $objCategory = $category->GetItem($dataCategory);
             $v->NameBizID    = htmlspecialchars($v->NameBizID);
             $v->SiteID       = htmlspecialchars($v->SiteID);
             $v->CityID       = $objCity->title; 
             $v->ShortDetailsID = strip_tags($v->ShortDetailsID); 
             //$v->ShortDetailsID = str_replace("&nbsp;", " ", $v->ShortDetailsID);
             //$v->ShortDetailsID = str_replace("&ndash;", " ", $v->ShortDetailsID);
             //$v->ShortDetailsID = str_replace("&laquo;", " ", $v->ShortDetailsID);
             //$v->ShortDetailsID = str_replace("&raquo;", " ", $v->ShortDetailsID);
             //$v->ShortDetailsID = str_replace("&reg;", " ", $v->ShortDetailsID);
             //$v->ShortDetailsID = str_replace("&sup2;", "2", $v->ShortDetailsID);
             //$v->ShortDetailsID = str_replace("&ldquo;", " ", $v->ShortDetailsID);
	     	 //$v->ShortDetailsID = str_replace("&rdquo;", " ", $v->ShortDetailsID);
	     	 //$v->ShortDetailsID = str_replace("&mdash;", " ", $v->ShortDetailsID);
	     	 //$v->ShortDetailsID = str_replace("&sbquo;", " ", $v->ShortDetailsID);
	     	 $v->ShortDetailsID = preg_replace("/&([a-zA-Z0-9]+;)/", " ", $v->ShortDetailsID);

             $v->category = $objCategory->Name;
             $v->link = ''.'http://www.bizzona.ru/detailsSell/'.$v->ID.'';
             $smarty->assign("row", $v);
             $content.=  $smarty->fetch('./site/itemrss.tpl');
           }
         }

         $smarty->assign("content", $content);
         $smarty->display("./site/rss.tpl");

         break;
       }

       case "category": {

          
         $dataCategory["ID"] = $_GET["ID"];
         $objCategory = $category->GetItem($dataCategory);
          
         
         $info = new stdclass(); 
         $info->title         = "[BizZONA.RU] ������� ������� �������.";
         $info->link          = "http://www.bizzona.ru";
         $info->description   = "������� �������: ".$objCategory->Name." ";
         $info->category      = "Ready Business";
         $info->language      = "russia";
         $info->copyright     = "Copyright, BizZONA.ru, 2006 - 2011";
         $info->webmaster     = "webmaster@bizzona.ru";
         $info->generator     = "www.bizzona.ru";
         $info->url           = "http://www.bizzona.ru";
         $info->ttl           = "60";
         $info->imglink       = "http://www.bizzona.ru/images/ban.gif";
         $info->lastbuilddate = date("m.d.Y", time());

         $smarty->assign("info", $info);
         $inforss = $smarty->fetch('./site/inforss.tpl');
         $smarty->assign("inforss", $inforss);

         $select["CategoryID"] = $_GET["ID"];
         $select["offset"] = 0;
         $select["rowCount"] = 30;
         $select["sort"] = "ID";
         $select["StatusID"] = "-1";
         $result = $sell->Select($select);
         $content = '';
         if (is_array($result) && sizeof($result) > 0) {
           while(list($k, $v) = each($result)) {
             $dataCity["ID"] = $v->CityID;
             $objCity = $city->GetItem($dataCity);
             $v->NameBizID    = htmlspecialchars($v->NameBizID);
             $v->SiteID       = htmlspecialchars($v->SiteID);
             $v->CityID       = $objCity->title; 
             $v->ShortDetailsID = strip_tags($v->ShortDetailsID); 
             //$v->ShortDetailsID = str_replace("&nbsp;", " ", $v->ShortDetailsID);
             //$v->ShortDetailsID = str_replace("&ndash;", " ", $v->ShortDetailsID);
             //$v->ShortDetailsID = str_replace("&laquo;", " ", $v->ShortDetailsID);
             //$v->ShortDetailsID = str_replace("&raquo;", " ", $v->ShortDetailsID);
             //$v->ShortDetailsID = str_replace("&reg;", " ", $v->ShortDetailsID);
             //$v->ShortDetailsID = str_replace("&sup2;", "2", $v->ShortDetailsID);
             //$v->ShortDetailsID = str_replace("&ldquo;", " ", $v->ShortDetailsID);
             //$v->ShortDetailsID = str_replace("&rdquo;", " ", $v->ShortDetailsID);
             //$v->ShortDetailsID = str_replace("&mdash;", " ", $v->ShortDetailsID);
             //$v->ShortDetailsID = str_replace("&sbquo;", " ", $v->ShortDetailsID);
             $v->ShortDetailsID = preg_replace("/&([a-zA-Z0-9]+;)/", " ", $v->ShortDetailsID);
             $v->category = $objCategory->Name;
             $v->link = ''.'http://www.bizzona.ru/detailsSell/'.$v->ID.'';
             $smarty->assign("row", $v);
             $content.=  $smarty->fetch('./site/itemrss.tpl');
           }
         }
         
         $smarty->assign("content", $content);
         $smarty->display("./site/rss.tpl");

         break;
         
       } 


       case "topbiz": {
         $info = new stdclass(); 
         $info->title         = "[BizZONA.RU] ������� ������� �������.";
         $info->link          = "http://www.bizzona.ru";
         $info->description   = "������� ������. ������ ����";
         $info->category      = "Ready Business";
         $info->language      = "russia";
         $info->copyright     = "Copyright, BizZONA.ru, 2006 - 2011";
         $info->webmaster     = "webmaster@bizzona.ru";
         $info->generator     = "www.bizzona.ru";
         $info->url           = "http://www.bizzona.ru";
         $info->ttl           = "60";
         $info->imglink       = "http://www.bizzona.ru/images/ban.gif";
         $info->lastbuilddate = date("m.d.Y", time());

         $smarty->assign("info", $info);
         $inforss = $smarty->fetch('./site/inforss.tpl');
         $smarty->assign("inforss", $inforss);

         $select["offset"] = 0;
         $select["rowCount"] = 30;
         $select["sort"] = "ID";
         $select["StatusID"] = "-1";
         $select["icon"] =   (isset($_GET["name"]) ? htmlspecialchars(trim($_GET["name"])).".gif" : "");
         
         $result = $sell->Select($select);
         $content = '';
         
         if (is_array($result) && sizeof($result) > 0) {
           while(list($k, $v) = each($result)) {
             $dataCategory = array();
             $dataCategory["ID"] = $v->CategoryID;       
             $objCategory = $category->GetItem($dataCategory);
             $v->NameBizID    = htmlspecialchars($v->NameBizID);
             $v->SiteID       = htmlspecialchars($v->SiteID);
             $v->CityID       = $objCity->title; 
             $v->ShortDetailsID = strip_tags($v->ShortDetailsID); 
             //$v->ShortDetailsID = str_replace("&nbsp;", " ", $v->ShortDetailsID);
             //$v->ShortDetailsID = str_replace("&ndash;", " ", $v->ShortDetailsID);
             //$v->ShortDetailsID = str_replace("&laquo;", " ", $v->ShortDetailsID);
             //$v->ShortDetailsID = str_replace("&raquo;", " ", $v->ShortDetailsID);
             //$v->ShortDetailsID = str_replace("&reg;", " ", $v->ShortDetailsID);
             //$v->ShortDetailsID = str_replace("&sup2;", "2", $v->ShortDetailsID);
             //$v->ShortDetailsID = str_replace("&ldquo;", " ", $v->ShortDetailsID);
             //$v->ShortDetailsID = str_replace("&rdquo;", " ", $v->ShortDetailsID);
             //$v->ShortDetailsID = str_replace("&mdash;", " ", $v->ShortDetailsID);
             //$v->ShortDetailsID = str_replace("&sbquo;", " ", $v->ShortDetailsID);
		     $v->ShortDetailsID = preg_replace("/&([a-zA-Z0-9]+;)/", " ", $v->ShortDetailsID);		
             $v->category = $objCategory->Name;
             $v->link = ''.'http://www.bizzona.ru/detailsSell/'.$v->ID.'';
             $smarty->assign("row", $v);
             $content.=  $smarty->fetch('./site/itemrss.tpl');
           }
         }


         $smarty->assign("content", $content);
         $output = $smarty->fetch('./site/rss.tpl'); 
         $smarty->display("./site/rss.tpl");

         break;
       } 


       default: {
         break; 
       }
     }

  } else {
  }

?>
