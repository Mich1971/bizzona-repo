<?
	global $DATABASE;

	include_once("./phpset.inc");
	include_once("./engine/functions.inc"); 

  	$count_sql_call = 0;
  	$start = get_formatted_microtime(); 
  	$base_memory_usage = memory_get_usage();

	AssignDataBaseSetting();
   
	include_once("./engine/class.category_lite.inc"); 
	$category = new Category_Lite();

	include_once("./engine/class.country_lite.inc"); 
	$country = new Country_Lite();

	include_once("./engine/class.city_lite.inc"); 
	$city = new City_Lite();

	include_once("./engine/class.sell_lite.inc"); 
	$sell = new Sell_Lite();

	include_once("./engine/class.buy_lite.inc"); 
	$buyer = new Buyer_Lite();
  
  	include_once("./engine/class.metro_lite.inc"); 
  	$metro = new Metro_Lite();
  
  	include_once("./engine/class.district_lite.inc"); 
  	$district = new District_Lite();

  	include_once("./engine/class.subcategory_lite.inc");
  	$subcategory = new SubCategory_Lite();

  	include_once("./engine/class.streets_lite.inc");
  	$streets = new Streets_Lite();

  	include_once("./engine/class.equipment_lite.inc"); 
  	$equipment = new Equipment_Lite();	
	
 	include_once("./engine/class.optimization_region_category.inc");
    $optimizationRegionCategory = new OptimizationRegionCategory();

	include_once("./engine/class.frontend.inc");
	
	require_once("./libs/Smarty.class.php");
	$smarty = new Smarty;
	$smarty->template_dir = "./templates";
	$smarty->compile_dir  = "./templates_c";
	//$smarty->cache_dir = "./cache";
	$smarty->compile_check = true;
	$smarty->debugging     = false;

	
	
	require_once("./regfuncsmarty.php");
  
	if(isset($_GET["ID"])) {
		$_GET["ID"] = intval($_GET["ID"]);
	}
  
	if(isset($_GET["rowCount"])) {
		$_GET["rowCount"] = intval($_GET["rowCount"]);
	}

	if(isset($_GET["offset"])) {
		$_GET["offset"] = intval($_GET["offset"]);
	}

	if(isset($_GET["streetID"])) {
		$_GET["streetID"] = intval($_GET["streetID"]);
	}
  
	$name_words = array("turagenstvo", "restoran", "parik", "krasota", "otel");
	if(isset($_GET["name"])) {
		if(in_array($_GET["name"], $name_words)) {
		} else {
			$_GET["name"] = "";
		}
	}
  
	$action_words = array("category", "city", "price", "topbiz");
	if(isset($_GET["action"])) {
		if(in_array($_GET["action"], $action_words)) {
		
		} else {
			$_GET["action"] = "";
		}
	}

	$_GET["subCityID"] = (isset($_GET["subCityID"]) ? intval($_GET["subCityID"]) : 77);
	$_GET["subCategoryID"] = (isset($_GET["subCategoryID"]) ? intval($_GET["subCategoryID"]) : 8);
	
	$_GET["subCatId"] = $_GET["subCategoryID"];
	$_GET["subCityId"] = $_GET["subCityID"]; 
	
	if(isset($_GET["subCityId"]) and intval($_GET["subCityId"]) > 0) {
        $objCity = $city->GetItem(array("ID" => intval($_GET["subCityId"])));
	}
	
	
    $optimizationRegionCategory->regionId = $_GET["subCityID"];
    $optimizationRegionCategory->categoryId = $_GET["subCategoryID"];
    $optimizationRegionCategory->adId = 1;
    $res_optimization_category = $optimizationRegionCategory->GetItem();
    $title  = "������� � ������� ������� ";
    $description = "������� � ������� ������� �� ������������. ����� 4 000 �������� ����������� �� ������� �������.";
    if(sizeof($res_optimization_category) > 0) {
    	$title  = $res_optimization_category->seotitle;
    	$description = $res_optimization_category->description;
    }
	
	$smarty->assign("headertitle", $title);	
	
  	$colorset = GetColorSet();
  	$smarty->assign("colorset", $colorset);  	
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
  <HEAD> 
    <title><?=$title;?> - ��� "������-����"</title>		
	<META NAME="Description" CONTENT="<?=$description;?>">
        <meta name="Keywords" content="<?=$title;?>"> 
        <LINK href="http://www.bizzona.ru/general.css" type="text/css" rel="stylesheet">
        <meta http-equiv="content-type" content="text/html; charset=windows-1251"/>
    </HEAD>
<body>

<?php
  $smarty->display("./site/headerbanner.tpl");
?>

<?
	$topmenu = GetMenu();
	$smarty->assign("topmenu", $topmenu);
		
	$link_country = GetSubMenu();	
	$smarty->assign("ad", $link_country);
	
	echo minimizer($smarty->fetch("./site/header.tpl"));
?>
<table class="w" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td width="30%" valign="top" style="padding-top:2pt;">
        <? 
          if (isset($_GET["action"]) && $_GET["action"]  == "category") {
            $_GET["ID"] = (isset($_GET["ID"])  ? intval($_GET["ID"]) : 0);
            $smarty->caching = true; 
            if (!$smarty->is_cached('./site/categorys.tpl', $_GET["ID"])) {
              $data = array();
              $data["offset"] = 0;
              $data["rowCount"] = 0;
              $data["sort"] = "Count";
              $result = $category->Select($data);
              $smarty->assign("data", $result);
            }
            
  			echo fminimizer($smarty->fetch("./site/categorys.tpl", $_GET["ID"]));
            $smarty->caching = false; 
          } else { 
            $smarty->caching = true; 
            if (!$smarty->is_cached('./site/categorys.tpl')) {
              $data = array();
              $data["offset"] = 0;
              $data["rowCount"] = 0;
              $data["sort"] = "Count";
              $result = $category->Select($data);
              $smarty->assign("data", $result);
            }
            
			echo fminimizer($smarty->fetch("./site/categorys.tpl"));
            
            $smarty->caching = false; 
          }
        ?>
            <?
            	echo fminimizer($smarty->fetch("./site/call.tpl"));
            
				$smarty->caching = true; 
				$smarty->cache_lifetime = 3600;
            	
            	if (!$smarty->is_cached("./site/iblockbuyer.tpl", "action=".(isset($_GET["action"]) ? $_GET["action"] : ""))) {
            	
            		$datahotbuyer = array();
            		$datahotbuyer["offset"] = 0;
            		$datahotbuyer["rowCount"] = 4;
            		$datahotbuyer["sort"] = "datecreate";
            		$datahotbuyer["StatusID"] = "-1";

                	if (isset($_GET["action"])) {
                   		switch(trim($_GET["action"])) {
	                   		case "category": {
    	                 		$datahotbuyer["typeBizID"] = intval($_GET["ID"]);
                       		} break;
                       		case "city": {
                         		$datahotbuyer["regionID"] = intval($_GET["ID"]);
                       		} break;                       
        	           		default: {
                       		}
                       		break;
                   		}            	
                	}		
            	
  	          		$res_datahotbuyer = $buyer->Select($datahotbuyer);
  	          		
    				$smarty->assign("databuyer", $res_datahotbuyer);        	
            	} 	
            	echo fminimizer($smarty->fetch("./site/iblockbuyer.tpl", "action=".(isset($_GET["action"]) ? $_GET["action"] : "")));
            	$smarty->caching = false; 
  	          	
  	          	
			echo fminimizer($smarty->fetch("./site/proposal.tpl"));
			echo fminimizer($smarty->fetch("./site/request.tpl"));
           	echo minimizer($smarty->fetch("./site/citydomains.tpl"));
        ?>              
        
        </td>
        <td width="70%" valign="top">

			<?
				if(isset($objCity) && strlen($objCity->phone) > 0) {
					$smarty->assign("phone", $objCity->phone);
					$smarty->assign("phonetitle", $objCity->phonetitle);
				} 
				
				$smarty->caching = false;
				echo minimizer($smarty->fetch("./site/phonecode.tpl"));						
				$smarty->caching = true;
			?>        
        
            <?
            	
            	$smarty->caching = true; 
            	$smarty->cache_lifetime = 3600;
				$city_cach = ((isset($_GET["action"]) && $_GET["action"] == "city") ? intval($_GET["ID"]) : 0 );
            	if (!$smarty->is_cached('./site/city.tpl', $city_cach)) {
            		$data = array();
            		$data["offset"] = 0;
            		$data["rowCount"] = 0;
            		$data["sort"] = "Count";
            		$result = $city->SelectActive($data);
                	$smarty->assign("data", $result);
                } 
                
				echo fminimizer($smarty->fetch("./site/city.tpl", $city_cach));
       			$smarty->caching = false; 

       			// up subcity show
				echo fminimizer($smarty->fetch("./site/subcity.tpl", $city_cach));
       			$smarty->caching = false;       			
       			// down subcity show
       			
       			
       			
				if(isset($_GET["action"]) && $_GET["action"]  == "category") {
					$sell->CategoryID = (isset($_GET["ID"])  ? intval($_GET["ID"]) : 0);
					$res_sub = $sell->SelectSubCategory();
					if(sizeof($res_sub) > 1)
					{
						$smarty->assign("subbizcategory", $res_sub);
						echo fminimizer($smarty->fetch("./site/subbiz.tpl"));
					} else {
						echo fminimizer($smarty->fetch("./site/poplistbiz.tpl"));
					}
				} else {
					echo fminimizer($smarty->fetch("./site/poplistbiz.tpl"));
				}
				
				
            ?>
            <?
				$output_navsearch = $smarty->fetch("./site/navsearch.tpl");
				echo fminimizer($output_navsearch);
            ?>
               <?
                  $smarty->caching = false; 
                  if (!$smarty->is_cached('./site/city.tpl', $_SERVER["REQUEST_URI"])) {
                    $data = array();
                    $dataPage = array(); 
                    $pagesplit = GetPageSplit();

                    if (isset($_GET["offset"])) {
                      $data["offset"] = intval($_GET["offset"]);
                    } else {
                      $data["offset"] = 0;
                    }
                    if (isset($_GET["rowCount"])) { 
                      $data["rowCount"] = intval($_GET["rowCount"]);
                    } else {
                      $data["rowCount"] = $pagesplit;
                    }

                    $data["sort"] = "ID";
                    
                    if (isset($_GET["streetID"]) && intval($_GET["streetID"]) > 0) {
                    	$data["streetID"] = intval($_GET["streetID"]);
                    	$dataPage["streetID"] = $data["streetID"];
                    }
                    
                    if(isset($_GET["subCityID"]) && intval($_GET["subCityID"]) > 0) {
                    	$data["subCityID"] = intval($_GET["subCityID"]);
                    	$dataPage["subCityID"] = intval($_GET["subCityID"]);
                    }
                    
                    if(isset($_GET["subCategoryID"]) && intval($_GET["subCategoryID"]) > 0) {
                    	$data["SubCategoryID"] = intval($_GET["subCategoryID"]);
                    	$dataPage["SubCategoryID"] = intval($_GET["subCategoryID"]);
                    }


                    $dataPage["offset"] = 0;
                    $dataPage["rowCount"] = 0;
                    $dataPage["sort"] = "ID";
                    $dataPage["StatusID"] = "-1";
                    
                    
                    $resultPage = $sell->CountSelect($dataPage);
                    
                    
                    
                    $smarty->assign("CountRecord",$resultPage);
                    $smarty->assign("CountSplit", $pagesplit);
                    $smarty->assign("CountPage", ceil($resultPage/5));

                    $data["StatusID"] = "-1";
                    $data["sort"] = "pay_datetime";

                    $result = $sell->Select($data);

                    $smarty->assign("data", $result);
                  }

				  $output_pagesplit = $smarty->fetch("./site/pagesplit_rc.tpl", $_SERVER["REQUEST_URI"]);
				  echo fminimizer($output_pagesplit);
                  
					echo $smarty->fetch("./site/saveme.tpl");
                 
				  $smarty->assign('colorsell', FrontEnd::ListColorSet());
 
				  echo fminimizer($smarty->fetch("./site/ilistsell.tpl", $_SERVER["REQUEST_URI"]));
                  
                  $smarty->caching = false; 
               ?>
            	<div class="lftpadding" style="width: auto;">
       				<div  style="background-image:url(<?=$_SERVER["HTTP_HOST"]?>/images/bl.gif); background-repeat:repeat-x;">&nbsp;</div>
            	</div>
            	<?
				  	echo fminimizer($output_pagesplit);
            	?> 
            	<?
				  	echo fminimizer($output_navsearch);
				?>
				
				<?
					include_once("./yadirect.php");
				?>				
				
                                    <? if (IsShowBlockSeoWords()):  ?>
					<div class="lftpadding_cnt" style="padding-top:10pt; padding-bottom: 5pt;padding-top:0pt; padding-bottom:0pt; width:auto;" >
                                            <?
                                                $smarty->caching = true; 
                                                if (!$smarty->is_cached("./site/keysell.tpl")) {
                                                    $datakeysell = $category->GetListSubCategoryActive();
                                                    $smarty->assign("datakeysell", $datakeysell);
                                                }
                                                echo fminimizer($smarty->fetch("./site/keysell.tpl"));
                
                                                $smarty->caching = false; 
                                            ?>
                                        </div>
                                    <? endif ?>    
            
				<?
				  if (isset($_GET["action"])) {
    				switch(trim($_GET["action"])) {
						case "city": {
							$parentID = (isset($_GET["ID"])  ? intval($_GET["ID"]) : 0);
							$seocity = $city->GetSeoParent($parentID);
							$dataseocity = $city->GetRegionbyParent($parentID);
							if(strlen(trim($seocity->seosell)) > 0 && sizeof($dataseocity) > 1) {
								$smarty->assign("seocity", $seocity);
								$smarty->assign("dataseocity", $dataseocity);
								$smarty->display("./site/hsubregion.tpl");
							}
						}	break;
    				}
				  }
     		   ?>            		

			  <div class="lftpadding_cnt" style="padding-top:10pt; padding-bottom: 5pt;padding-top:0pt; padding-bottom:0pt; width:auto;" >
			   <?
			   		
			        if(isset($_GET["subCityID"]) && intval($_GET["subCityID"]) > 0) {
						$pref_cach = intval($_GET["subCityID"]);
            			$smarty->caching = true; 
						if (!$smarty->is_cached('./site/streetssell.tpl', $pref_cach)) {
							$streets->subCityId = intval($_GET["subCityID"]);
							$res_streets = $streets->SelectStreetSell();
							//echo $streets->sql;
							if(sizeof($res_streets) > 0) {
								$smarty->assign("streets", $res_streets);
							}
						} 
						echo fminimizer($smarty->fetch("./site/streetssell.tpl", $pref_cach));
						$smarty->caching = false; 
						
			        } else if (isset($_GET["action"]) && intval($_GET["ID"]) > 0 && $_GET["action"] == "city") {
						$pref_cach = intval($_GET["ID"]);
            			$smarty->caching = true; 
						if (!$smarty->is_cached('./site/streetssell.tpl', $pref_cach)) {
							$streets->cityId = intval($_GET["ID"]);
							$res_streets = $streets->SelectStreetSell();
							//echo $streets->sql;
							if(sizeof($res_streets) > 0) {
								$smarty->assign("streets", $res_streets);
							}
						} 
						echo fminimizer($smarty->fetch("./site/streetssell.tpl", $pref_cach));
						$smarty->caching = false; 
					}
			   ?>
			   </div>
			   
				<div class="lftpadding_cnt" style="padding-top:10pt; padding-bottom: 5pt;padding-top:0pt; padding-bottom:0pt; width:auto;" >	    
					<table border="0" align="right" width="100%">
                		<tr>
                			<td width="30%"></td>
                			<td width="70%" align="right">
								<script type="text/javascript" src="//yandex.st/share/share.js" charset="utf-8"></script>
								<div class="yashare-auto-init" data-yashareType="button" data-yashareQuickServices="yaru,vkontakte,facebook,twitter,odnoklassniki,moimir,lj,friendfeed,moikrug"></div>
               				</td>			
							</tr>
						</table>
				</div>				
				
        </td>
    </tr>
    <tr>
    	<td colspan="2">
				<?
				
					$smarty->caching = true; 
					$smarty->cache_lifetime = 10500;
					
					if (!$smarty->is_cached('./site/sugequipment.tpl', 'prodam-biznes')) {
            
						$datasugequipment = array();
						$datasugequipment["offset"] = 0;
						$datasugequipment["rowCount"] = 4;
						$datasugequipment["sort"] = "datecreate";
						$datasugequipment["status_id"] = "-1";
            	 
						$res_datasugequipment = $equipment->SugSelect($datasugequipment);
  	          	
						$smarty->assign("datasugequipment", $res_datasugequipment);        	
					}				
				
					?><div class="lftpadding_cnt"><?
						echo fminimizer($smarty->fetch("./site/sugequipment.tpl", "prodam-biznes"));
					?></div><?
					
					$smarty->caching = false; 					
					
				?>			   
    	</td>
    </tr>
</table>
<style>
 td a 
 {
   color:black;
 }
</style>
<div style="padding-top:4pt;padding-bottom:5pt;padding-right:10pt;padding-left:10pt;font-size:10pt;font-family:arial;">
	<table cellpadding="0" cellspacing="0" border="0"  style="width:100%;padding-bottom:2pt;" bgcolor="#eeeee0">
    	<tr>
        	<td style="background-image:url(http://<?=$_SERVER["HTTP_HOST"]?>/images/d1.gif); background-repeat:no-repeat;width:5px;height:5px; background-position:left top;">&nbsp;</td>
            <td rowspan="2" valign="middle" align="center"  style="font-size:8pt; font-family:Arial; color:black;">
            
             <a href="http://www.bizzona.ru/prodazha-biznesa-avtomoika.php" title="������� ���������">������� ���������</a>&nbsp;
             <a href="http://www.bizzona.ru/prodazha-biznesa-avtoskola.php" title="������� ���������">������� ���������</a>&nbsp;
             <a href="http://www.bizzona.ru/prodazha-biznesa-employment-agency.php" title="������� ��������� ���������">������� ��������� ���������</a>&nbsp;
             <a href="http://www.bizzona.ru/prodazha-biznesa-building-company.php" title="������� ������������ �����">������� ������������ �����</a>&nbsp;
             <a href="http://www.bizzona.ru/prodazha-biznesa-car-care-center.php" title="������� �����������">������� �����������</a>&nbsp;
             <a href="http://www.bizzona.ru/prodazha-biznesa-car-magazin.php" title="������� ��������">������� ��������</a>&nbsp;
             
             
            </td>
            <td style="background-image:url(http://<?=$_SERVER["HTTP_HOST"]?>/images/d2.gif); background-repeat:no-repeat;width:5px;height:5px;background-position:right top;">&nbsp;</td>
        </tr>
        <tr>
            <td style="background-image:url(http://<?=$_SERVER["HTTP_HOST"]?>/images/d4.gif); background-repeat:no-repeat;width:5px;height:5px; background-position:left bottom;">&nbsp;</td>
            <td style="background-image:url(http://<?=$_SERVER["HTTP_HOST"]?>/images/d3.gif); background-repeat:no-repeat;width:5px;height:5px; background-position:right bottom;">&nbsp;</td>
        </tr>
    </table>
</div>

<?
	if (isset($_GET["action"])) {
		switch(trim($_GET["action"])) {
			case "city": {
				$cID = (isset($_GET["ID"])  ? intval($_GET["ID"]) : 0);
				
				if($cID == 928 || $cID == 23) {
					echo minimizer($smarty->fetch("./site/adv23.tpl"));
				} else if ($cID == 77 || $cID == 50) {
					echo minimizer($smarty->fetch("./site/adv77.tpl"));
                                } else if ($cID == 1310 || $cID == 54) {
					echo minimizer($smarty->fetch("./site/adv54.tpl"));
                                } else if ($cID == 1915 || $cID == 66) {
					echo minimizer($smarty->fetch("./site/adv66.tpl"));
                                } else if ($cID == 47 || $cID == 78) {
					echo minimizer($smarty->fetch("./site/adv78.tpl"));
                                } else if ($cID == 985 || $cID == 24) {
					echo minimizer($smarty->fetch("./site/adv24.tpl"));
                                } else if ($cID == 1441 || $cID == 59) {
					echo minimizer($smarty->fetch("./site/adv59.tpl"));
                                } else if ($cID == 1816 || $cID == 63) {
					echo minimizer($smarty->fetch("./site/adv63.tpl"));
                                } else if ($cID == 1264 || $cID == 52) {
					echo minimizer($smarty->fetch("./site/adv52.tpl"));
                                } else if ($cID == 304 || $cID == 2) {
					echo minimizer($smarty->fetch("./site/adv2.tpl"));
                                } else if ($cID == 634 || $cID == 39) {
					echo minimizer($smarty->fetch("./site/adv39.tpl"));
                                } else if ($cID == 1721 || $cID == 16) {
					echo minimizer($smarty->fetch("./site/adv16.tpl"));
                                } else if ($cID == 1791 || $cID == 61) {
					echo minimizer($smarty->fetch("./site/adv61.tpl"));
                                } else if ($cID == 1353 || $cID == 55) {
					echo minimizer($smarty->fetch("./site/adv55.tpl"));
                                } else if ($cID == 1100 || $cID == 48) {
					echo minimizer($smarty->fetch("./site/adv48.tpl"));
                                } else if ($cID == 2326 || $cID == 74) {
					echo minimizer($smarty->fetch("./site/adv74.tpl"));
                                } else if ($cID == 554 || $cID == 38) {
					echo minimizer($smarty->fetch("./site/adv38.tpl"));
                                } else if ($cID == 460 || $cID == 36) {
					echo minimizer($smarty->fetch("./site/adv36.tpl"));
                                }

			}
			case "category": {
				$subCatID = (isset($_GET["subcategory"])  ? intval($_GET["subcategory"]) : 0);
				$cID = (isset($_GET["ID"])  ? intval($_GET["ID"]) : 0);
				if($cID == 109) {
					echo minimizer($smarty->fetch("./site/adv-salon.tpl"));
				} else if($cID == 82) {
					echo minimizer($smarty->fetch("./site/advsub1.tpl"));
				} else if($cID == 115) {
					echo minimizer($smarty->fetch("./site/advsub156.tpl"));
				} else if($cID == 92) {
					echo minimizer($smarty->fetch("./site/advsub8.tpl"));
				} else if($cID == 88 && $subCatID == 22) {
					echo minimizer($smarty->fetch("./site/advsub22.tpl"));
				} else if($cID == 88 && $subCatID == 23) {
					echo minimizer($smarty->fetch("./site/advsub23.tpl"));
				} else if($cID == 90 && $subCatID == 41) {
					echo minimizer($smarty->fetch("./site/advsub41.tpl"));
				} else if($cID == 90 && $subCatID == 42) {
					echo minimizer($smarty->fetch("./site/advsub42.tpl"));
				} else if($cID == 91 && ($subCatID == 81 || $subCatID == 80)) {
					echo minimizer($smarty->fetch("./site/advsub81.tpl"));
				} else if($cID == 114 && ($subCatID == 157 || $subCatID == 51 || $subCatID == 52 || $subCatID == 118)) {
					echo minimizer($smarty->fetch("./site/advsub157.tpl"));
				} else if($cID == 93 && $subCatID == 18) {
					echo minimizer($smarty->fetch("./site/advsub18.tpl"));
				} else if($cID == 93 && $subCatID == 19) {
					echo minimizer($smarty->fetch("./site/advsub19.tpl"));
				} else if($cID == 93 && $subCatID == 20) {
					echo minimizer($smarty->fetch("./site/advsub20.tpl"));
				}

			} 
			case "topbiz": {
				if (isset($_GET["name"]) && ($_GET["name"] == "parik" || $_GET["name"] == "krasota"))	{
					echo minimizer($smarty->fetch("./site/adv-salon.tpl"));
				}
			}
		}
	} else {
		echo fminimizer($smarty->fetch("./site/adv.tpl"));
	}
?>

<?
	echo fminimizer($smarty->fetch("./site/site_city.tpl"));

	$smarty->display("./site/footer.tpl");
?>

<?
	$end = get_formatted_microtime(); 
	$total = $end - $start;
	echo "<center><span style='font-size:7pt;'>".round($total, 6)." : ".$count_sql_call." : ".memory_get_usage()." </span><center>";;
?>

<script type="text/javascript" src="http://www.bizzona.ru/ja.js"></script>

</body>
</html>
<?
	$sell->Close();
	$category->Close();
	$city->Close();
	$country->Close();
	$buyer->Close();
	$metro->Close();
	$district->Close();
	$subcategory->Close();
	$streets->Close();
    $optimizationRegionCategory->Close();	
?>
