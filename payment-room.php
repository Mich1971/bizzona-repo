<?php
	global 	$DATABASE,
			$DEFAULT_COUNTRY_ID,
			$DEFAULT_LANGUAGE_ID;
	
	include_once("./phpset.inc");
	include_once("./engine/functions.inc");

	AssignDataBaseSetting();

	ini_set("display_startup_errors", "off");
	ini_set("display_errors", "off");
	ini_set("register_globals", "on");
	
	include_once("./engine/class.Room.inc");
	$room = new Room();

    require_once("./libs/Smarty.class.php");
	$smarty = new Smarty;
	$smarty->template_dir = "./templates";
	$smarty->compile_dir  = "./templates_c";
	$smarty->cache_dir = "./cache";
	$smarty->compile_check = true;
	$smarty->debugging     = false;

	$room->Id = intval($_GET["Id"]);
	$res = $room->GetItem();
    $smarty->assign("Id", $data["ID"]);	
	
	$smarty->assign("fml", $res->fio); 
	$smarty->assign("email", $res->email); 
			
	$secret_code = "vbhjplfybt";
	$purse = "9192"; // sms:bank id        ������������� ���:�����
	$order_id = $res->Id; //  ������������� ��������
	$amount = "5.5"; // transaction sum  ����� ����������
	
	if(isset($_GET["cost"])) {
		$amount = intval($_GET["cost"]);
	}
	
	if(isset($_GET["operator"])) {
		//$amount = 4;
                $amount = "5.5";   
	}
	
	if(isset($_GET["country"]) && $_GET["country"]="ukraine") {
		$amount = 3;
	}

	if(isset($_GET["country"]) && $_GET["country"]="pt") {
		$amount = 2;
	}
	
	//$amount = "0.01";
	
	$clear_amount  = "0"; // billing algorithm  �������� �������� ���������
	$description  = "������ ���������� ������� ���������"; // operation desc �������� ��������
	$submit  = "���������"; // submit label ������� �� ������ submit
	
	$money_message = cp1251_to_utf8("������ ���������� ������� ��������� �� ����� bizzona.ru (".$order_id.") ");
	$money_message = base64_encode($money_message);
	
	
	$sign = ref_sign($purse, $order_id, $amount, $clear_amount, $description, $secret_code);
	$sign_ukraine = ref_sign($purse, $order_id, 3, $clear_amount, $description, $secret_code);
	
	$smarty->assign("purse", $purse); 
	$smarty->assign("order_id", $order_id); 
	$smarty->assign("rbk_order_id", $order_id); 
	$smarty->assign("amount", $amount); 
	$smarty->assign("amount_ukraine", 3); 
	$smarty->assign("clear_amount", $clear_amount);
	$smarty->assign("description", $description); 
	$smarty->assign("money_message", $money_message);

	$smarty->assign("submit", $submit);
	$smarty->assign("sign", $sign);
	$smarty->assign("sign_ukraine", $sign_ukraine);
			 
	$payment = $smarty->fetch("./site/payment-room.tpl");
	echo $payment;
?>
