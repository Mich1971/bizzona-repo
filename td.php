<?
	global $DATABASE;

	include_once("./phpset.inc");
	include_once("./engine/functions.inc"); 
	include_once("./engine/sell_functions.inc"); 
	require_once("./engine/Cache/Lite.php");

	$start = get_formatted_microtime(); 
  
	$count_sql_call = 0;

	AssignDataBaseSetting();
  
	//ini_set("display_startup_errors", "on");
	//ini_set("display_errors", "on");
	//ini_set("register_globals", "on");
 
	include_once("./engine/class.category.inc"); 
	$category = new Category();

	include_once("./engine/class.country.inc"); 
	$country = new Country();

	include_once("./engine/class.city.inc"); 
	$city = new City();

	include_once("./engine/class.sell.inc"); 
	$sell = new Sell();

	include_once("./engine/class.buy.inc"); 
	$buyer = new Buyer();

	include_once("./engine/class.district.inc"); 
	$district = new District();

	include_once("./engine/class.subcategory.inc");
	$subcategory = new SubCategory();

	include_once("./engine/class.geocode.inc"); 
	$geocode = new GeoCode();

	include_once("./engine/class.streets.inc");
	$streets = new streets();
	
  	include_once("./engine/class.metro.inc");
  	$metro = new Metro();

	include_once("./engine/class.optimization_region_category.inc");
	$optimizationRegionCategory = new OptimizationRegionCategory();
  
	require_once("./libs/Smarty.class.php");
	$smarty = new Smarty;
	$smarty->template_dir = "./templates";
	$smarty->compile_dir  = "./templates_c";
	$smarty->cache_dir = "./cache";
	$smarty->compile_check = true;
	$smarty->debugging     = false;
	//$smarty->caching = true; 

	require_once("./regfuncsmarty.php");
 
	$assignDescription = AssignDescription();

	$sell = new Sell();
	$dataSell = Array();
	$dataSell["ID"] = intval($_GET["ID"]);
  
	//$resultGetItem = $sell->GetItem($dataSell);
	
  	// up caching call details sell
  	$options = array(
  		'cacheDir' => "sellcache/",
   		'lifeTime' => 2592000
  	);
		
  	$cache = new Cache_Lite($options);
		
  	if ($data = $cache->get(''.$dataSell["ID"].'____sellitem')) {
		$resultGetItem = unserialize($data);
  	} else { 
		$resultGetItem = $sell->GetItem($dataSell);
		$cache->save(serialize($resultGetItem));
  	}
  	// donw caching call details sell	
	
	$smarty->assign("datasell", $resultGetItem);

  	$seoname = "";
  	
  	if (isset($resultGetItem->ID)) {
    	$resultGetItem->ProfitPerMonthID = trim(strrev(chunk_split (strrev($resultGetItem->ProfitPerMonthID), 3,' '))); 
    	$resultGetItem->MaxProfitPerMonthID = trim(strrev(chunk_split (strrev($resultGetItem->MaxProfitPerMonthID), 3,' '))); 
    	$resultGetItem->SumDebtsID = trim(strrev(chunk_split (strrev($resultGetItem->SumDebtsID), 3,' '))); 
    	$resultGetItem->WageFundID = trim(strrev(chunk_split (strrev($resultGetItem->WageFundID), 3,' ')));
    	$resultGetItem->ArendaCostID = trim(strrev(chunk_split (strrev($resultGetItem->ArendaCostID), 3,' ')));

    	$_SESSION["uCityID"] = $resultGetItem->CityID; 
    	$_SESSION["uCategoryID"] = $resultGetItem->CategoryID; 
    
    	$seoname = $resultGetItem->seoname;
    	if(strlen($seoname) > 0) {
    		$seoname =  $seoname.", ";
    	}
    
    	$optimizationRegionCategory->regionId = ($resultGetItem->subCityID > 0 ? $resultGetItem->subCityID : $resultGetItem->CityID);
    	$optimizationRegionCategory->categoryId = $resultGetItem->SubCategoryID;
    	$optimizationRegionCategory->adId = 1;
    	$res_optimization_category = $optimizationRegionCategory->GetItem();
    	$smarty->assign("optimization_category", $res_optimization_category);
  	} else {
  		header ("Location: http://www.bizzona.ru");
  	}
	
	if (isset($resultGetItem->ID)) {

		$datahotbuyer = array();
        $datahotbuyer["offset"] = 0;
        $datahotbuyer["rowCount"] = 3;
        $datahotbuyer["sort"] = "datecreate";
        $datahotbuyer["StatusID"] = "-1";

        $datahotbuyer["typeBizID"] = $resultGetItem->CategoryID;
        $datahotbuyer["regionID"] = $resultGetItem->CityID;
            	
        $res_datahotbuyer = $buyer->Select($datahotbuyer);
         
		$datasuggsell = array();
		$datasuggsell["offset"] = 0;
		$datasuggsell["rowCount"] = 5;
		$datasuggsell["subCatId"] = $resultGetItem->SubCategoryID;
		$datasuggsell["cityId"] = $resultGetItem->CityID;
		$datasuggsell["subCityId"] = $resultGetItem->SubCityID;
		$datasuggsell["StatusID"] = "-1";
		$datasuggsell["sort"] = "DataCreate";
		$datasuggsell["brokerId"] = $resultGetItem->brokerId;
         
		$resultsell = $sell->DetailsSelect($datasuggsell);
		
		$BizFormID =  NameBizFormByID($resultGetItem->BizFormID);
        $ReasonSellBizID = NameReasonSellBizByID($resultGetItem->ReasonSellBizID);
        $smarty->assign("BizFormID", $BizFormID); 
        $smarty->assign("ReasonSellBizID", $ReasonSellBizID);		
		
	}  	
  	
	$geocode->serviceId = intval($_GET["ID"]);
	$geocode->typeServiceId = 1;
	
	$objgeo = $geocode->GetData();
	
	if(intval($resultGetItem->latitude) > 0) {
		$objgeo->latitude = $resultGetItem->latitude;	
	}
	
	if(intval($resultGetItem->longitude) > 0) {
		$objgeo->longitude = $resultGetItem->longitude;	
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
	<HEAD>
  	  <?
  	     if(strlen($resultGetItem->subtitle) > 0) {
  	     	$smarty->assign("headertitle", $resultGetItem->subtitle);
			?>
				<title><?=$resultGetItem->subtitle;?> ��� "������-����"</title>
				<META NAME="Description" CONTENT="<?=$resultGetItem->subtitle;?> ��� "������-����">
			<?  	     	
  	     } else {
  	     	?>
  	     		<title>������� <?=$resultGetItem->NameBizID;?>. ������ <?=$resultGetItem->NameBizID;?>, ����� <?=$resultGetItem->NameBizID;?>.</title>
  	     		<META NAME="Description" CONTENT="������ <?=$resultGetItem->NameBizID;?> , ����� <?=$resultGetItem->NameBizID;?>">
  	     	<?
  	     }
      ?>	
        <meta name="Keywords" content="������ <?=$resultGetItem->NameBizID;?> , ����� <?=$resultGetItem->NameBizID;?>"> 
        <LINK href="http://www.bizzona.ru/general.css" type="text/css" rel="stylesheet">
        <meta http-equiv="content-type" content="text/html; charset=windows-1251"/>
		<link rel="shortcut icon" href="http://www.bizzona.ru/images/favicon.ico">
	</HEAD>
<?
	$smarty->display("./site/headerbanner.tpl");
?>
<?  
    $useMapGoogle = ( ($_SERVER["SERVER_NAME"] == "www.bizzona.ru" or $_SERVER["SERVER_NAME"] == "bizzona.ru")  ? true : false); 
    $useMapGoogle = (($resultGetItem->StatusID == 5 or $resultGetItem->StatusID == 12 or $resultGetItem->StatusID == 10) ? false : $useMapGoogle);
    
	if(isset($objgeo->status) && $objgeo->status == G_GEO_SUCCESS && $useMapGoogle ) {
		
		if($objgeo->accuracy >= 1) {
			$zoom = $objgeo->zoom;
			
?>
    			<script charset="utf-8" src="http://maps.google.com/maps?file=api&amp;v=2&amp;hl=ru&key=ABQIAAAAnavXIsM5jdni-p-JoKqdIxTPIkIa5BjXbMz0WvIzZffrVGiazxTRu-mHw0IpP9Ib5fP2129LbcNtTw"
      				type="text/javascript"></script>
    			<script type="text/javascript">
    			//<![CDATA[
    				function load() {
      					if (GBrowserIsCompatible()) {
        					var map = new GMap2(document.getElementById("map"));
        					//map.enableScrollWheelZoom(); 
        					map.setCenter(new GLatLng(<?=$objgeo->longitude;?>,<?=$objgeo->latitude;?>), <?=$zoom;?>);
        					map.addControl(new GMapTypeControl());
        					var bounds = map.getBounds();
          					var point = new GLatLng(<?=$objgeo->longitude;?>, <?=$objgeo->latitude;?>);
          					map.addOverlay(new GMarker(point));
        					map.addControl(new GSmallMapControl());
        					GEvent.addListener(map, "click", function(overlay,latlng) {
          						var lat = latlng.lat();
          						var lon = latlng.lng();
          						var latOffset = 0.01;
          						var lonOffset = 0.01;
       	  						var polygon = new GPolygon([
            					new GLatLng(lat, lon - lonOffset),
            					new GLatLng(lat + latOffset, lon),
            					new GLatLng(lat, lon + lonOffset),
            					new GLatLng(lat - latOffset, lon),
            					new GLatLng(lat, lon - lonOffset)
		  						], "#f33f00", 5, 1, "#ff0000", 0.2);
		  						map.addOverlay(polygon);
        					});
          					
          					
      					}
    				}
    			//]]>
    			</script>  
    			<body onload="load()" onunload="GUnload()">
<?
			$smarty->assign("googlemap", 1);
		}
	}
?> 
<table class="w" border="0" cellpadding="0" cellspacing="0" height="100%"  bgcolor="#eeeee0">
	<tr>
		<td colspan="2">
		<?
			$topmenu = GetMenu();
			$smarty->assign("topmenu", $topmenu);

			$link_country = GetSubMenu();	
			$smarty->assign("ad", $link_country);
	
			$smarty->caching = false;	
			$output_header = $smarty->fetch("./site/header.tpl");
			echo minimizer($output_header);
		?>
		</td>
	</tr>
	<tr>
		<td colspan="2" style="padding:2pt;">
			<?
				echo UpPositionAndShare_1(intval($_GET["ID"]));
			?>
		</td>
	</tr>
	<tr>
		<td colspan="2" bgcolor="#006699" id="menu_top" align="right" style="padding:3pt;">
			<?
				$output_linkcatdetsell = $smarty->fetch("./site/linkcatdetsell.tpl");
				echo minimizer($output_linkcatdetsell);
			?>
		</td>
	</tr>
    <tr>
		<td width="60%" valign="top" style="padding:2pt;">
			<?		
				echo SellHeaderShow_1($dataSell["ID"], $resultGetItem->DataCreate);
       		?>		
			<div style='font-family:arial;font-size:11pt;border-top: 1px; border-bottom: 1px; border-left:1px;  border-right:1px;  border-color:#C1C1A4;  border-style: solid;padding:5px;background-color:#ffffff;' >
       			<div class="w" style="padding-top:10pt; padding-bottom: 5pt;height:20pt; padding-top:0pt; padding-bottom:0pt;width:auto;" >
       				<table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#eeeee0" style="height:100%;">
            			<tr>
							<td valign="middle" align="left" style="padding-left:3pt;vertical-align:middle; background-image:url(http://www.bizzona.ru/images/disc.gif); background-repeat:no-repeat; background-position:center; background-position:left; padding-left:15pt;"><span onclick='window.open("http://www.bizzona.ru/messagesell/<?=$resultGetItem->ID;?>","","width=550,height=500,scrollbars=auto,resizable=yes")' class="type_a" style="text-decoration:underline;cursor:hand;cursor: pointer;"  target='_blank'>����������</span></td>
							<td valign="middle" align="left" style="padding-left:3pt;vertical-align:middle; background-image:url(http://www.bizzona.ru/images/print.gif); background-repeat:no-repeat; background-position:center; background-position:left; padding-left:15pt;"><a href="http://www.bizzona.ru/print/<?=$resultGetItem->ID;?>" class="type_a" style="font-weight:normal;" target="_blank">������ ��� ������</a></td>
                			<td width="50%" valign="middle" align="right" style="padding-right:3pt; padding-left:3pt; font-size:12pt;font-family:Arial;color:White;font-weight:bold; vertical-align:middle; padding-left:15pt;">
							<div style='display:block' id='SendBlockID'>
								<input type='hidden' id='idsell' value='<?=$dataSell["ID"];?>'>
								<div style="background-image:url(http://www.bizzona.ru/images/email.gif); display:inline; background-repeat:no-repeat; background-position:center; background-position:left; padding-right:15pt;">&nbsp;</div>&nbsp;<input type="text"  id='email' name='email' value="�������� �� email" style="font-size:9pt; font-family:Arial;" onClick='this.value="";'  onfocusout="Restore(this);" /><input type="button" value="&nbsp;Ok&nbsp;"  style="font-size:9pt; font-family:Arial;"  onclick='send();' />
							</div>
                			</td>
                		</tr>
            		</table>
       			</div>
			
				<?
					$output_detalssellleft = $smarty->fetch("./site/detalssellleft.tpl");
					echo minimizer($output_detalssellleft);
				?>

				<?
				if (isset($resultGetItem->ID)) {
					$sell->SubCategoryID = $resultGetItem->SubCategoryID;
					
					$smarty->assign("catId", $resultGetItem->CategoryID);
					$smarty->assign("subCatId", $resultGetItem->SubCategoryID);
					
           			$res_ListCityForSearch = $sell->ListCityForSearch();
           			$res_ListRegionForSearch = $sell->ListRegionForSearch();
           			while (list($k, $v) = each($res_ListCityForSearch)) {
           				if($v->CityID == 77) {
           					$smarty->assign("show_moscow_link", "1");
           				}
           				if($v->CityID == 78) {
           					$smarty->assign("show_spb_link", "1");
           				}
           			}
           			reset($res_ListCityForSearch);
           			while (list($k, $v) = each($res_ListRegionForSearch)) {
           				if($v->CityID == 50) {
           					$smarty->assign("show_moscow_region_link", "1");
           					break;
           				}
           			}
           			reset($res_ListRegionForSearch);
           			$smarty->assign("ListCityForSearch", $res_ListCityForSearch);
           			$smarty->assign("ListRegionForSearch", $res_ListRegionForSearch);
            
            		$output_SelectByRegionSubRegion = $smarty->fetch("./site/SelectByRegionSubRegion_1.tpl", $sell->SubCategoryID);
					echo fminimizer($output_SelectByRegionSubRegion);					
				}
				
				?>

				<?
					$smarty->caching = true; 
					if (!$smarty->is_cached("./site/keysell.tpl")) {
							$datakeysell = $category->GetListSubCategoryActive();
							$smarty->assign("datakeysell", $datakeysell);
					}
					$output_keysell = $smarty->fetch("./site/keysell.tpl");
					echo minimizer($output_keysell);
					$smarty->caching = false; 
				?>
				
				<?
					ShowDetailsSearchForm("");
				?>
			</div>		
        </td>
        <td width="40%" valign="top" style="padding:2pt;">
        	<div style='font-family:arial;font-size:11pt;border-top: 1px; border-bottom: 1px; border-left:1px;  border-right:1px;  border-color:#C1C1A4;  border-style: solid;padding:5px;background-color:#fbfafa;' >
        		<?

				$output_detalssellright = $smarty->fetch("./site/detailssellright.tpl");
				echo minimizer($output_detalssellright);
				
                $smarty->assign("data", $resultsell);
                if(isset($resultsell) && sizeof($resultsell) > 0) {
					$output_sugnavsearch = $smarty->fetch("./site/sugnavsearch_1.tpl");
					echo fminimizer($output_sugnavsearch);
                }
                    
				$saveme = $smarty->fetch("./site/saveme.tpl");
				echo $saveme;

				$output_ilistsell = $smarty->fetch("./site/ilistsell_1.tpl", $_SERVER["REQUEST_URI"]);
				echo fminimizer($output_ilistsell);
                    
                if(isset($resultsell) && sizeof($resultsell) > 0) {
					$output_sugnavsearch = $smarty->fetch("./site/sugnavsearch_1.tpl");
					echo fminimizer($output_sugnavsearch);                    	
                }				
				?>
        	</div>

			<?
				DetailsSuggestSubcategory($resultGetItem->SubCategoryID);  
			?>
        	
        	
				<?
           			if(isset($res_datahotbuyer) && sizeof($res_datahotbuyer) > 0) {
  						$smarty->assign("data", $res_datahotbuyer);        	
  						
                     	$output_sugnavsearchb = $smarty->fetch("./site/sugnavsearchb_1.tpl");
		             	echo fminimizer($output_sugnavsearchb);
  						
                     	$output_ilistsellb = $smarty->fetch("./site/ilistsellb_1.tpl", $_SERVER["REQUEST_URI"]);
		             	echo fminimizer($output_ilistsellb);
          				
                     	$output_sugnavsearchb = $smarty->fetch("./site/sugnavsearchb_1.tpl");
		             	echo fminimizer($output_sugnavsearchb);
           			}				
				?>
				<?
					if($resultGetItem->SubCategoryID > 0) {

						include_once("./engine/class.subcategory_article.inc"); 
						$subcategory_article = new subcategory_article();	
				
                		$smarty->caching = true; 
                		$smarty->cache_dir = "./persistentcache";
                		$smarty->cache_lifetime = 86400*30;

                		if (!$smarty->is_cached("./site/subcategory_articles.tpl", $resultGetItem->SubCategoryID)) {
							$subcategory_article->subcategoryId = $resultGetItem->SubCategoryID; 
							$result_subcategory_article = $subcategory_article->Select();
							$smarty->assign("subcategory_article", $result_subcategory_article);
                		}
				
						$output_subcategory_articles = $smarty->fetch("./site/subcategory_articles.tpl", $resultGetItem->SubCategoryID);
						echo minimizer($output_subcategory_articles);			
				
						$smarty->caching = false; 
					}				
				?>			
				
			<div style='font-family:arial;font-size:11pt;border-top: 1px; border-bottom: 1px; border-left:1px;  border-right:1px;  border-color:#C1C1A4;  border-style: solid;padding:5px;background-color:#ffffff;' >        	
        	<table class="w" bgcolor="#e4e4e2">
				<tr>
					<td>
						<?
       						if(sizeof($res_datahotbuyer) > 0) {	
								$smarty->caching = true; 
								if (!$smarty->is_cached("./site/keyselllist.tpl")) {
									$datakeysell = $category->GetListSubCategoryActive();
									$smarty->assign("datakeysell", $datakeysell);
								}
								$smarty->assign("countdatakeysell", 18);
								$output_keysell = $smarty->fetch("./site/keyselllist.tpl");
								echo fminimizer($output_keysell);
								$smarty->caching = false; 					
   							}
						?>							
					</td>
				</tr>
			</table>
			</div>

				
        </td>
    </tr>
	<tr>
		<td colspan="2" bgcolor="#006699" id="menu_top" align="right" style="padding:3pt;">
			<?
				$output_linkcatdetsell = $smarty->fetch("./site/linkcatdetsell.tpl");
				echo minimizer($output_linkcatdetsell);
			?>
		</td>
	</tr>    
	<tr>
		<td colspan="2" style="padding:2pt;">
			<?
				echo UpPositionAndShare_1(intval($_GET["ID"]));
			?>
		</td>
	</tr>    
	
	<tr>
		<td colspan="2" bgcolor="White">
			<style>
 				td a {    color:black; }
			</style> 
			<div style="padding-top:4pt;padding-bottom:5pt;padding-right:10pt;padding-left:10pt;font-size:10pt;font-family:arial;">
			<table cellpadding="0" cellspacing="0" border="0"  style="width:100%;" bgcolor="#eeeee0">
    			<tr>
        			<td style="background-image:url(http://<?=$_SERVER["HTTP_HOST"]?>/images/d1.gif); background-repeat:no-repeat;width:5px;height:5px; background-position:left top;">&nbsp;</td>
            		<td rowspan="2" valign="middle" align="center"  style="font-size:8pt; font-family:Arial; color:black;">
						<h1 style='margin:0pt;padding:0pt;color:black;font-family:arial;font-size:8pt;text-align:center;margin-bottom:0pt;' >��� "������-����" - <?=$seoname;?> �������� ����� ����������� ������� �� �������.</h1> 
            		</td>
            		<td style="background-image:url(http://<?=$_SERVER["HTTP_HOST"]?>/images/d2.gif); background-repeat:no-repeat;width:5px;height:5px;background-position:right top;">&nbsp;</td>
        		</tr>
        		<tr>
            		<td style="background-image:url(http://<?=$_SERVER["HTTP_HOST"]?>/images/d4.gif); background-repeat:no-repeat;width:5px;height:5px; background-position:left bottom;">&nbsp;</td>
            		<td style="background-image:url(http://<?=$_SERVER["HTTP_HOST"]?>/images/d3.gif); background-repeat:no-repeat;width:5px;height:5px; background-position:right bottom;">&nbsp;</td>
        		</tr>
    		</table>
			</div>
			<?
				$output_adv = $smarty->fetch("./site/selladvice.tpl");
				echo fminimizer($output_adv);

				$output_mework = $smarty->fetch("./site/sell-howmework.tpl");
				echo fminimizer($output_mework);
			?>
			<?
				$smarty->display("./site/footer.tpl");
			?>
		</td>
	</tr>	
</table>
<?
	$end = get_formatted_microtime();
	$total = $end - $start;
	echo "<center><span style='font-size:7pt;'>".round($total, 6)." : ".$count_sql_call." : ".memory_get_usage()." </span><center>";
?>
<script language='javascript'>
  function AddToSearch(obj) {
	var input_search = document.getElementById("searchtext");
	if(input_search != null) {
		if(obj.innerText != null) {
			input_search.value = obj.innerText;
		} else if (obj.innerHTML != null) {
			input_search.value = obj.innerHTML;
		}
	}
  }  
</script>
</body>
</html>
<?
	$dataSell["ID"] = $resultGetItem->ID;
    if(!isset($_GET["nocount"])) {
   		$sell->IncrementQView($dataSell);
    }

	$sell->Close();
	$category->Close();
	$city->Close();
	$country->Close();
	$buyer->Close();
	$district->Close();
	$subcategory->Close();
	$geocode->Close();
    $streets->Close();
    $optimizationRegionCategory->Close();
?>