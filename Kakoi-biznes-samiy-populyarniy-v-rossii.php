 <?
  global $DATABASE;
  include_once("./phpset.inc");
  include_once("./engine/functions.inc"); 
  
  	$count_sql_call = 0;
  	$start = get_formatted_microtime(); 
  	$base_memory_usage = memory_get_usage();  
  
  AssignDataBaseSetting();
  
  //ini_set("display_startup_errors", "on");
  //ini_set("display_errors", "on");
  //ini_set("register_globals", "on");
  
  include_once("./libtotalarticle.php"); 	
   
  require_once("./libs/Smarty.class.php");
  $smarty = new Smarty;
  $smarty->template_dir = "./templates";
  $smarty->compile_dir  = "./templates_c";
  //$smarty->cache_dir = "./cache";
  $smarty->compile_check = true;
  $smarty->debugging     = false;

  require_once("./regfuncsmarty.php");

  $assignDescription = AssignDescription();

  $smarty->assign("headertitle", "����� ������ ����� ���������� � ������?");
  
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>

  <HEAD>
      	<title>����� ������ ����� ���������� � ������? - ��� "������-����"</title>		
		<META NAME="Description" CONTENT="����� ������ ����� ���������� � ������?">
        <meta name="Keywords" content="����� ������ ����� ���������� � ������?"> 
        <LINK href="./general.css" type="text/css" rel="stylesheet">
        <meta http-equiv="content-type" content="text/html; charset=windows-1251"/>
		<script type="text/javascript" src="ja.js"></script>        
		<link rel="shortcut icon" href="./images/favicon.ico">
		
		<script src="http://www.bizzona.ru/js/jquery.min.js"></script>
		<script src="http://www.bizzona.ru/js/jquery.colorbox.js"></script> 
		<link media="screen" rel="stylesheet" href="http://www.bizzona.ru/css/colorbox.css" />        
		<script>
		$(document).ready(function(){
			$("a[rel='compliment']").colorbox({width:"60%", height:"80%", iframe:true});
			$("a[rel='scold']").colorbox({width:"60%", height:"80%", iframe:true});
			$("a[rel='advise']").colorbox({width:"60%", height:"80%", iframe:true});
			$("#click").click(function(){ 
				$('#click').css({"background-color":"#f00", "color":"#fff", "cursor":"inherit"}).text("");
				return false;
			});
		});
		</script>		
  </HEAD>

<?php
  $smarty->display("./site/headerbanner.tpl");
?>


<table class="w" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="2">
<?
	$topmenu = GetMenu();
	$smarty->assign("topmenu", $topmenu);
		
	$link_country = GetSubMenu();	
	$smarty->assign("ad", $link_country);

	$smarty->display("./site/header.tpl");
?>		
		</td>
	</tr>
    <tr>
        <td width="75%" valign="top">
<div style='height:95%;padding-left:5px;padding-right:5px;'>
<div class="adv">
<h1>����� ������ ����� ���������� � ������?</h1>

	<table>
		<tr>
			<td colspan="2">
				<div>
					<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;���� � ������� ����� ������� ���� ������ �� �������� ��������  ������� � ������ �� ������ ����������� ������ ������ � ����� ������ ������. �� ����  ����� �������� ������� �������� �������� ������� �������� � ����� �� ������. �� ������ ������ ������� ����� ����� ����� ���������  � ����� ���������� �������. ������ �������, ��� ��� ������ ������ � ��������� ���������, ��������������� � ���������� ���������.  �������� �� ��, ��� ������ ������ ������, ���������� ���������� ��������� ������������� ���������� ����������� ��� ������� ������ �� ������� ������ �������.  ���� �� �������� ������ �������, ������������ ���� ������� ������� �� ���������. �� ��������� ������� ���� � ���������, �� ����������� ������������ �����  �������.  ������� ����������� � ������� ����������  ���� ������ �������. �� ��������� ������� �����, ��� ������� ������� �������� ������ � ����� ��������������, ����� ��� ��������� ������� � ��������� ��������.</p>
				</div>
			</td>
		</tr>
	
		<tr>
			<td><img src="http://www.bizzona.ru/simg/top-biznes-rossiya-1.png"></td>
			<td valign="top">
				<div class="adv">
				<p>�� ������� 1 ������������ ������������� ���� �������  � ��������� ������������� �� ������� � ����������� �� ����� ������������. �� ������ ������� ����������� ������ ���� �������� ����� ������.</p>
				</div>
			</td>
		</tr>
		<tr>
			<td><img src="http://www.bizzona.ru/simg/top-biznes-rossiya-2.png"></td>
			<td valign="top">
				<div class="adv">
				<p>�� ������� 2 ������������ ������������� ���� �������  � ��������� ������������� �� ������� � ����������� �� ����� ������������. �� ������ ������� ����������� ������ ���� �������� � �������� ������ ��������.</p>
				</div>
			</td>
		</tr>		
		<tr>
			<td><img src="http://www.bizzona.ru/simg/top-biznes-rossiya-3.png"></td>
			<td valign="top">
				<div class="adv">
				<p>�� ������� 3 ������������ ������������� ���� �������  � ��������� ������������� �� ������� � ����������� �� ����� ������������. �� ������ ������� ����������� ������ ���� �������� � �������� 1%-3%.</p>
				<div class="adv">
			</td>
		</tr>
		<tr>
			<td><img src="http://www.bizzona.ru/simg/top-biznes-rossiya-4.png"></td>
			<td valign="top">
				<div class="adv">
				<p>�� ������� 4 ������������ ������������� ���� �������  � ��������� ������������� �� ������� � ����������� �� ����� ������������. �� ������ ������� ����������� ������ ���� �������� ����������� ������������ ������ �������� �������.</p>
				<div class="adv">
			</td>
		</tr>		
		<tr>
			<td colspan="2">
				<div >
				<p>
				&nbsp;&nbsp;&nbsp;&nbsp;����� �������, �� ���������� ������ ����� �������, ��� �������� ���������� ������ � ������ ��� �������������� (������, �������), ��������� ������� (���� �� ����� � ��� ��� ����� ���� �������) � ��������� ��������.
				</p>
				</p>
			</td>
		</tr>
	</table>
	
	<p>����� ������������� ���������� ����������� ������ � ����������� �������� ��� �������-�����.</p>
<?
	$smarty->display("./site/review.tpl");
?>
	
<?
	$sociallinks = $smarty->fetch("./site/sociallinks.tpl");
	echo fminimizer($sociallinks);
?>	
 </div>
</div>

<?
	$output_articles = $smarty->fetch("./site/in_articles.tpl");
    echo fminimizer($output_articles);
?>

<?
    if(IsShowBlockSeoWords()) {
	$output_seoprodazha = $smarty->fetch("./site/seo_prodazha_19.tpl");
        echo fminimizer($output_seoprodazha);
    }
?>


        </td>
        <td width="25%" valign="top" style="padding-top:2pt;">
        
			<?
				$output_banner3 = $smarty->fetch("./site/banner3.tpl");
				echo fminimizer($output_banner3);
			?>        
            
            <?
            	$smarty->display("./site/request.tpl");
            ?>                      
        
            <?
              /*
              $smarty->caching = true; 
              $smarty->cache_lifetime = 3600;
              if (!$smarty->is_cached('./site/hotsell.tpl')) {
                $data = array();
                $data["offset"] = 0;
                $data["rowCount"] = 0;
                $data["sort"] = "DateStart";
                $data["StatusID"] = "2";              
                $result = $sell->Select($data);
                $smarty->assign("data", $result);


                if (isset($_SESSION["listID"])) { 
                   $dataPage["rowCount"] = 0;
                   $dataPage["offset"] = 0;
                   unset($_SESSION["listID"]);
                }
                while (list($ld, $lv) = each($result)) {
                   $_SESSION["listID"][] = $lv->ID; 
                }

              }

	      $hotselljs = $smarty->fetch("./site/hotselljs.tpl");
	      echo minimizer($hotselljs);


              $smarty->display("./site/hotsell.tpl");
              */
            RightBlockTopSell();  
            
            ?>            
        </td>
    </tr>
	<tr>
		<td colspan="2">
<div style="padding-top:4pt;padding-bottom:5pt;padding-right:10pt;padding-left:10pt;font-size:10pt;font-family:arial;">
	<table cellpadding="0" cellspacing="0" border="0"  style="width:100%;padding-bottom:2pt;" bgcolor="#eeeee0">
    	<tr>
        	<td style="background-image:url(<?=$_SERVER["host_name"]?>images/d1.gif); background-repeat:no-repeat;width:5px;height:5px; background-position:left top;">&nbsp;</td>
            <td rowspan="2" valign="middle" align="center"  style="font-size:8pt; font-family:Arial; color:black;">
            </td>
            <td style="background-image:url(<?=$_SERVER["host_name"]?>images/d2.gif); background-repeat:no-repeat;width:5px;height:5px;background-position:right top;">&nbsp;</td>
        </tr>
        <tr>
            <td style="background-image:url(<?=$_SERVER["host_name"]?>images/d4.gif); background-repeat:no-repeat;width:5px;height:5px; background-position:left bottom;">&nbsp;</td>
            <td style="background-image:url(<?=$_SERVER["host_name"]?>images/d3.gif); background-repeat:no-repeat;width:5px;height:5px; background-position:right bottom;">&nbsp;</td>
        </tr>
    </table>
</div>
<?
	$smarty->display("./site/footer.tpl");
?>
		</td>
	</tr>
</table>	
<?
	$end = get_formatted_microtime(); 
	$total = $end - $start;
	echo "<center><span style='font-size:7pt;'>".round($total, 6)." : ".$count_sql_call." : ".memory_get_usage()." </span><center>";
?>

</body>
</html>
