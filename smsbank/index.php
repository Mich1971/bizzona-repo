<?php
	// the function returns an MD5 of parameters passed
	// ������� ���������� MD5 ���������� �� ����������
	function ref_sign() {
		$params = func_get_args();
		$prehash = implode("::", $params);
		return md5($prehash);
	}
	
	// the function prints a request form
	// ������� �������� ����� �������
	function print_form($purse, $order_id, $amount, $clear_amount, $description, $secret_code, $submit) {
		// making signature
		// ������� �������
		$sign = ref_sign($purse, $order_id, $amount, $clear_amount, $description, $secret_code);
		
		// printing the form
		// �������� �����
		echo <<<Form
		<form action="http://service.smscoin.com/bank/" method="post">
			<p>
				<input name="s_purse" type="hidden" value="$purse" />
				<input name="s_order_id" type="hidden" value="$order_id" />
				<input name="s_amount" type="hidden" value="$amount" />
				<input name="s_clear_amount" type="hidden" value="$clear_amount" />
				<input name="s_description" type="hidden" value="$description" />
				<input name="s_sign" type="hidden" value="$sign" />
				<input type="submit" value="$submit" />
			</p>
		</form>

Form;
	}
	
	// printing headers
	// �������� ���������
	header("Content-Type: text/html; charset=windows-1251");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ru">
	<head>
		<title>
			sms:bank - demo // ���:���� - ������������ // index.php
		</title>
	</head>
	<body>
<?php
	// service secret code
	// ��������� ��� �������
	$secret_code = "vbhjplfybt";
	
	// initializing variables
	// �������������� ����������
	$purse        = 2621;              // sms:bank id        ������������� ���:�����
	$order_id     = 1234;           // operation id       ������������� ��������
	$amount       = 0.1;            // transaction sum    ����� ����������
	$clear_amount = 0;              // billing algorithm  �������� �������� ���������
	$description  = "demo payment"; // operation desc     �������� ��������
	$submit       = "���������";    // submit label       ������� �� ������ submit
	
	// printing the form
	// �������� �����
	print_form($purse, $order_id, $amount, $clear_amount, $description, $secret_code, $submit);
?>
	</body>
</html>
