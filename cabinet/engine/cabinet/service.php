<?php
    header("Content-Type: text/xml; charset=utf-8");

	global $DATABASE;

	include_once("../../../phpset.inc");
	
	include_once("../../../engine/functions.inc"); 

	AssignDataBaseSetting("../../../config.ini");

	include_once("../../../engine/class.services.inc"); 
  	$service = new Services(); 
	
  	$_POST = array_merge($_POST, $_GET);
  	
  	if (isset($_POST) && $_POST["action"] == "get") {
  	
  		$array = $service->SelectForCabinet();
  	
		$dom = new DOMDocument();

  		$n_servicies = $dom->createElement("servicies");
  	
  		while (list($k, $v) = each($array)) {
  	
  			$n_item = $dom->createElement("item", cp1251_to_utf8($v['shortName']));	
			$n_item->setAttribute("id", $v['id']);  		
  			$n_servicies->appendChild($n_item);
  			
  		}
  	
    	$dom->appendChild($n_servicies);
  		echo $dom->saveXML();
  		return;
  	}
?>	