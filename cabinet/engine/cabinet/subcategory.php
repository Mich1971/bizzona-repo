<?php
    header("Content-Type: text/xml; charset=utf-8");

	global $DATABASE;

	include_once("../../../phpset.inc");
	
	include_once("../../../engine/functions.inc"); 

	AssignDataBaseSetting("../../../config.ini");

	include_once("../../../engine/class.subcategory.inc");
	$subCategory = new SubCategory();


  	$dataCategory = array();
  	$dataCategory["offset"] = 0;
  	$dataCategory["rowCount"] = 0;
  	$dataCategory["sort"] = "ID";
  	$listSubCategory = $subCategory->Select($dataCategory);

  	
  	$dom = new DOMDocument();

  	$n_subcategory = $dom->createElement("subcategorylist");

	while (list($k,$v) = each($listSubCategory)) {
		$n_categoryitem = $dom->createElement("item", cp1251_to_utf8($v->Name));
		$n_categoryitem->setAttribute("id", $v->ID);
		$n_categoryitem->setAttribute("parentid", $v->CategoryID);
		$n_subcategory->appendChild($n_categoryitem);
	}

  	$dom->appendChild($n_subcategory);
  	echo $dom->saveXML();
?>