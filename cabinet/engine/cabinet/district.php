<?php
    header("Content-Type: text/xml; charset=utf-8");

	global $DATABASE;

	include_once("../../../phpset.inc");
	
	include_once("../../../engine/functions.inc"); 

	AssignDataBaseSetting("../../../config.ini");

	include_once("../../../engine/class.district.inc");
	$district = new District();

  	if(!isset($_GET)) {
  		$_GET = array();
  	}
  	
  	$id = 0;
  	
  	$_POST = array_merge($_POST, $_GET);

	if (isset($_POST) && $_POST["action"] == "get") {
  		$id = intval($_POST["id"]);
	}  	
	
	$district->regionID = $id;
	$listDistrict = $district->SelectByRegion();
  	
  	$dom = new DOMDocument();

  	$n_districtlist = $dom->createElement("districtlist");

	while (list($k,$v) = each($listDistrict)) {
		$n_districtitem = $dom->createElement("item", cp1251_to_utf8($v->name));
		$n_districtitem->setAttribute("id", $v->ID);
		$n_districtlist->appendChild($n_districtitem);
	}

  	$dom->appendChild($n_districtlist);
  	echo $dom->saveXML();
?>