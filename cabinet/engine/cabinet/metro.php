<?php
    header("Content-Type: text/xml; charset=utf-8");

	global $DATABASE;

	include_once("../../../phpset.inc");
	
	include_once("../../../engine/functions.inc"); 

	AssignDataBaseSetting("../../../config.ini");

	include_once("../../../engine/class.metro.inc");
	$metro = new Metro();

  	if(!isset($_GET)) {
  		$_GET = array();
  	}
  	
  	$id = 0;
  	
  	$_POST = array_merge($_POST, $_GET);

	if (isset($_POST) && $_POST["action"] == "get") {
  		$id = intval($_POST["id"]);
	}  	

	$metro->regionID = $id;
	
  	$listMetro = $metro->SelectByRegion();
  	 
  	$dom = new DOMDocument();

  	$n_metro = $dom->createElement("metrolist");

	while (list($k,$v) = each($listMetro)) {
		$n_metroitem = $dom->createElement("item", cp1251_to_utf8($v->name));
		$n_metroitem->setAttribute("id", $v->ID);
		$n_metro->appendChild($n_metroitem);
	}

  	$dom->appendChild($n_metro);
  	echo $dom->saveXML();
?>