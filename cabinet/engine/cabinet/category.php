<?php
    header("Content-Type: text/xml; charset=utf-8");

	global $DATABASE;

	include_once("../../../phpset.inc");
	
	include_once("../../.../engine/functions.inc"); 

	AssignDataBaseSetting("../../../config.ini");

	include_once("../../../engine/class.category.inc"); 
	$category = new Category();

  	$dataCategory = array();
  	$dataCategory["offset"] = 0;
  	$dataCategory["rowCount"] = 0;
  	$dataCategory["sort"] = "ID";
  	$listCategory = $category->Select($dataCategory);

  	$dom = new DOMDocument();

  	$n_category = $dom->createElement("categorylist");
  	
	while (list($k,$v) = each($listCategory)) {
		$n_categoryitem = $dom->createElement("item", cp1251_to_utf8($v->Name));
		$n_categoryitem->setAttribute("id", $v->ID);
		$n_category->appendChild($n_categoryitem);
	}
	
  	$dom->appendChild($n_category);
  	echo $dom->saveXML();
?>