<?php
    header("Content-Type: text/xml; charset=utf-8");

	global $DATABASE;

	include_once("../../../phpset.inc");
	
	include_once("../../../engine/functions.inc"); 

	AssignDataBaseSetting("../../../config.ini");

	include_once("../../../engine/class.city.inc");
	$city = new City();

  	if(!isset($_GET)) {
  		$_GET = array();
  	}
  	
  	$parentId = 0;
  	
  	$_POST = array_merge($_POST, $_GET);

	if (isset($_POST) && $_POST["action"] == "get") {
  		$parentId = intval($_POST["id"]);
	}  	
	
  	$listSubRegion = $city->GetRegionbyParent($parentId, false, false);

  	
  	$dom = new DOMDocument();

  	$n_subregion = $dom->createElement("subregionlist");

	while (list($k,$v) = each($listSubRegion)) {
		$n_regionitem = $dom->createElement("item", cp1251_to_utf8($v->title));
		$n_regionitem->setAttribute("id", $v->ID);
		$n_subregion->appendChild($n_regionitem);
	}

  	$dom->appendChild($n_subregion);
  	echo $dom->saveXML();
?>