<?php
    header("Content-Type: text/xml; charset=utf-8");

	global $DATABASE;

	include_once("../../../phpset.inc");
	
	include_once("../../../engine/functions.inc"); 
	require_once("../../../engine/Cache/Lite.php");

	AssignDataBaseSetting("../../../config.ini");

	require_once("../../../libs/Smarty.class.php");
	$smarty = new Smarty;
	$smarty->template_dir = "../../../templates";
	$smarty->compile_dir  = "../../../templates_c";
	$smarty->cache_dir = "../../../sellcache";
	$smarty->compile_check = false; 
	$smarty->debugging     = false;	
	
	include_once("../../../engine/class.sell.inc"); 
  	$sell = new Sell();

  	if(!isset($_GET)) {
  		$_GET = array();
  	}
  	
  	$_POST = array_merge($_POST, $_GET);
  	
	if (isset($_POST) && $_POST["action"] == "get") {
	
  		$dataSell = Array();
  		$dataSell["ID"] = intval($_POST["id"]);
  		$resultGetItem = $sell->GetItem($dataSell);
  
  		$dom = new DOMDocument();

  		$n_sell = $dom->createElement("sell");

  		$n_fml = $dom->createElement("fml", cp1251_to_utf8($resultGetItem->FML));
  		$n_sell->appendChild($n_fml);

  		$n_email = $dom->createElement("email", cp1251_to_utf8($resultGetItem->EmailID));
  		$n_sell->appendChild($n_email);
  	
  		$n_fax = $dom->createElement("fax", cp1251_to_utf8($resultGetItem->FaxID));
  		$n_sell->appendChild($n_fax);

  		$n_phone = $dom->createElement("phone", cp1251_to_utf8($resultGetItem->PhoneID));
  		$n_sell->appendChild($n_phone);
  	
  		$n_cost = $dom->createElement("cost", cp1251_to_utf8($resultGetItem->CostID));
  		$n_sell->appendChild($n_cost);

  		$n_ccost = $dom->createElement("ccost", cp1251_to_utf8($resultGetItem->cCostID));
  		$n_sell->appendChild($n_ccost);
  	
  		$n_profitpermonth = $dom->createElement("profitpermonth", cp1251_to_utf8($resultGetItem->ProfitPerMonthID));
  		$n_sell->appendChild($n_profitpermonth);
  	
  		$n_cprofitpermonth = $dom->createElement("cprofitpermonth", cp1251_to_utf8($resultGetItem->cProfitPerMonthID));
  		$n_sell->appendChild($n_cprofitpermonth);
  	
  		$n_maxprofitpermonth = $dom->createElement("maxprofitpermonth", cp1251_to_utf8($resultGetItem->MaxProfitPerMonthID));
  		$n_sell->appendChild($n_maxprofitpermonth);

  		$n_cmaxprofitpermonth = $dom->createElement("cmaxprofitpermonth", cp1251_to_utf8($resultGetItem->cMaxProfitPerMonthID));
  		$n_sell->appendChild($n_cmaxprofitpermonth);

  		$n_timeinvest = $dom->createElement("timeinvest", cp1251_to_utf8($resultGetItem->TimeInvestID));
  		$n_sell->appendChild($n_timeinvest);
	
  		$n_monthaveturn = $dom->createElement("monthaveturn", cp1251_to_utf8($resultGetItem->MonthAveTurnID));
  		$n_sell->appendChild($n_monthaveturn);

  		$n_cmonthaveturn = $dom->createElement("cmonthaveturn", cp1251_to_utf8($resultGetItem->cMonthAveTurnID));
  		$n_sell->appendChild($n_cmonthaveturn);

  		$n_maxmonthaveturn = $dom->createElement("maxmonthaveturn", cp1251_to_utf8($resultGetItem->MaxMonthAveTurnID));
  		$n_sell->appendChild($n_maxmonthaveturn);

  		$n_cmaxmonthaveturn = $dom->createElement("cmaxmonthaveturn", cp1251_to_utf8($resultGetItem->cMaxMonthAveTurnID));
  		$n_sell->appendChild($n_cmaxmonthaveturn);

  		$n_monthexpemse = $dom->createElement("monthexpemse", cp1251_to_utf8($resultGetItem->MonthExpemseID));
  		$n_sell->appendChild($n_monthexpemse);

  		$n_cmonthexpemse = $dom->createElement("cmonthexpemse", cp1251_to_utf8($resultGetItem->cMonthExpemseID));
  		$n_sell->appendChild($n_cmonthexpemse);

  		$n_sumdebts = $dom->createElement("sumdebts", cp1251_to_utf8($resultGetItem->SumDebtsID));
  		$n_sell->appendChild($n_sumdebts);

  		$n_csumdebts = $dom->createElement("csumdebts", cp1251_to_utf8($resultGetItem->cSumDebtsID));
  		$n_sell->appendChild($n_csumdebts);
  	
  		$n_wagefund = $dom->createElement("wagefund", cp1251_to_utf8($resultGetItem->WageFundID));
  		$n_sell->appendChild($n_wagefund);
  	
  		$n_cwagefund = $dom->createElement("cwagefund", cp1251_to_utf8($resultGetItem->cWageFundID));
  		$n_sell->appendChild($n_cwagefund);
  	
  		$n_countempl = $dom->createElement("countempl", cp1251_to_utf8($resultGetItem->CountEmplID));
  		$n_sell->appendChild($n_countempl);

  		$n_countmanempl = $dom->createElement("countmanempl", cp1251_to_utf8($resultGetItem->CountManEmplID));
  		$n_sell->appendChild($n_countmanempl);
  	
  		$n_termbiz = $dom->createElement("termbiz", cp1251_to_utf8($resultGetItem->TermBizID));
  		$n_sell->appendChild($n_termbiz);
  	
  		$n_sharesale = $dom->createElement("sharesale", cp1251_to_utf8($resultGetItem->ShareSaleID));
  		$n_sell->appendChild($n_sharesale);
  		
  		$n_maxsharesale = $dom->createElement("maxsharesale", cp1251_to_utf8($resultGetItem->maxShareSaleID));
  		$n_sell->appendChild($n_maxsharesale);  		
  	
  		$n_reasonsellbiz = $dom->createElement("reasonsellbiz", cp1251_to_utf8($resultGetItem->ReasonSellBizID));
  		$n_sell->appendChild($n_reasonsellbiz);
  	
  		$n_bizform = $dom->createElement("bizform", cp1251_to_utf8($resultGetItem->BizFormID));
  		$n_sell->appendChild($n_bizform);
	
  		$n_category = $dom->createElement("category", cp1251_to_utf8($resultGetItem->CategoryID));
  		$n_sell->appendChild($n_category);

  		$n_subcategory = $dom->createElement("subcategory", cp1251_to_utf8($resultGetItem->SubCategoryID));
  		$n_sell->appendChild($n_subcategory);

  		$n_agreementcost = $dom->createElement("agreementcost", cp1251_to_utf8($resultGetItem->AgreementCostID));
  		$n_sell->appendChild($n_agreementcost);
  	
  		$n_statusId = $dom->createElement("statusId", cp1251_to_utf8($resultGetItem->StatusID));
  		$n_sell->appendChild($n_statusId);  		

  		$n_owncomment = $dom->createElement("owncomment", cp1251_to_utf8($resultGetItem->owncomment));
  		$n_sell->appendChild($n_owncomment);
  		
  		
  		$temp_NameBizID = str_replace("�","\"",$resultGetItem->NameBizID);
  		$temp_NameBizID = str_replace("�","\"",$temp_NameBizID);
  		
  		$n_namebiz = $dom->createElement("namebiz", cp1251_to_utf8($temp_NameBizID));
  		//$n_namebiz = $dom->createElement("namebiz", cp1251_to_utf8($resultGetItem->NameBizID));
  		$n_sell->appendChild($n_namebiz);
  		
  	
  		$dom->appendChild($n_sell);
  		echo $dom->saveXML();
  		return;
	}
	
	if (isset($_POST) && $_POST["action"] == "set") {
		
  		$dataSell = Array();
  		$dataSell["ID"] = intval($_POST["id"]);
  		$resultGetItem = $sell->GetItem($dataSell);
  		
		$data['FML'] = $resultGetItem->FML;
		$data['EmailID'] = $resultGetItem->EmailID;
		$data['Email2ID'] = $resultGetItem->Email2ID;
		$data['PhoneID'] = $resultGetItem->PhoneID;
		$data['NameBizID'] = $resultGetItem->NameBizID;
		$data['BizFormID'] = $resultGetItem->BizFormID;
		$data['CityID'] = $resultGetItem->CityID;
		$data['subRegionID'] = $resultGetItem->subCityID;
		$data['SiteID'] = $resultGetItem->SiteID; 
		$data['CostID'] = $resultGetItem->CostID;
		$data['cCostID'] = $resultGetItem->cCostID;
		$data['AgreementCostID'] = $resultGetItem->AgreementCostID;
		$data['ProfitPerMonthID'] = $resultGetItem->ProfitPerMonthID;
		$data['cProfitPerMonthID'] = $resultGetItem->cProfitPerMonthID;
        $data['MaxProfitPerMonthID'] = $resultGetItem->MaxProfitPerMonthID;
		$data['cMaxProfitPerMonthID'] = $resultGetItem->cMaxProfitPerMonthID;
		$data['TimeInvestID'] = $resultGetItem->TimeInvestID;
		$data['ListObjectID'] = $resultGetItem->ListObjectID;
		$data['ContactID'] = $resultGetItem->ContactID;
		$data['MatPropertyID'] = $resultGetItem->MatPropertyID;
		$data['MonthAveTurnID'] = $resultGetItem->MonthAveTurnID;
		$data['cMonthAveTurnID'] = $resultGetItem->cMonthAveTurnID;
		$data['MaxMonthAveTurnID'] = $resultGetItem->MaxMonthAveTurnID;
		$data['cMaxMonthAveTurnID'] = $resultGetItem->cMaxMonthAveTurnID;
        $data['MonthExpemseID'] = $resultGetItem->MonthExpemseID;
		$data['cMonthExpemseID'] = $resultGetItem->cMonthExpemseID;
		$data['SumDebtsID'] = $resultGetItem->SumDebtsID;
		$data['cSumDebtsID'] = $resultGetItem->cSumDebtsID;
		$data['WageFundID'] = $resultGetItem->WageFundID;
		$data['cWageFundID'] = $resultGetItem->cWageFundID;
		$data['ObligationID'] = $resultGetItem->ObligationID;
		$data['CountEmplID'] = $resultGetItem->CountEmplID;
		$data['CountManEmplID'] = $resultGetItem->CountManEmplID;
		$data['TermBizID'] = $resultGetItem->TermBizID;
		$data['ShortDetailsID'] = $resultGetItem->ShortDetailsID;
		$data['DetailsID'] = $resultGetItem->DetailsID;
		$data['FaxID'] = $resultGetItem->FaxID;
		$data['ShareSaleID'] = $resultGetItem->ShareSaleID;
		$data['maxShareSaleID'] = $resultGetItem->maxShareSaleID;
		$data['ReasonSellBizID'] = $resultGetItem->ReasonSellBizID;
		$data['TarifID'] = $resultGetItem->TarifID;
		$data['StatusID'] = $resultGetItem->StatusID;
		$data['CategoryID'] = $resultGetItem->CategoryID;
		$data['SubCategoryID'] = $resultGetItem->SubCategoryID;
		$data['Agent'] = $resultGetItem->Agent;
		$data['districtID'] = $resultGetItem->districtID;
		$data['Icon'] = $resultGetItem->Icon;
		$data['metroID'] = $resultGetItem->metroID;
		$data['streetID'] = $resultGetItem->streetID;
		$data['ID'] = $resultGetItem->ID;

    	$data['youtubeURL'] = $resultGetItem->youtubeURL;
    	$data['youtubeShortURL'] = $resultGetItem->youtubeShortURL;
    	$data['subtitle'] = $resultGetItem->subtitle;
    	$data['brokerId'] = $resultGetItem->brokerId;
    	
    	$data['HotShortDetailsID'] = $resultGetItem->HotShortDetailsID;
    	$data['typeSellID'] = $resultGetItem->typeSellID;
    	$data['arendaStatusID'] = $resultGetItem->arendaStatusID;
    	$data['torgStatusID'] = $resultGetItem->torgStatusID;
    	$data['getseo'] = $resultGetItem->getseo;

        $data['ArendaCostID'] = $resultGetItem->ArendaCostID;
		$data['cArendaCostID'] = $resultGetItem->cArendaCostID;    	
		$data['ArendaCommAl'] = $resultGetItem->ArendaCommAl;  
    	
    	
    	$data['owncomment'] = $resultGetItem->owncomment;
		
		if(isset($_POST["OwncommentID"])) {
			$data['owncomment'] = utf8_to_cp1251($_POST["OwncommentID"]);
			$data['owncomment'] = htmlspecialchars(trim($data['owncomment']), ENT_QUOTES);
		}		
    	
    	
		if(isset($_POST["StatusID"])) {
			
			$data['StatusID'] = utf8_to_cp1251($_POST["StatusID"]);
			$data['StatusID'] = intval($data['StatusID']);
			
			//mail("eugenekurilov@gmail.com",$data['StatusID'], $data['StatusID']);
		}    	
    	
		if(isset($_POST["FML"])) {
			$data['FML'] = utf8_to_cp1251($_POST["FML"]);
			$data['FML'] = htmlspecialchars(trim($data['FML']), ENT_QUOTES);
		}
		
		if(isset($_POST["PhoneID"])) {
			$data['PhoneID'] = utf8_to_cp1251($_POST["PhoneID"]);
			$data['PhoneID'] = htmlspecialchars(trim($data['PhoneID']), ENT_QUOTES);
		}
		
		if(isset($_POST["FaxID"])) {
			$data['FaxID'] = utf8_to_cp1251($_POST["FaxID"]);
			$data['FaxID'] = htmlspecialchars(trim($data['FaxID']), ENT_QUOTES);
		}
		
		if(isset($_POST["EmailID"])) {
			$data['EmailID'] = utf8_to_cp1251($_POST["EmailID"]);
			$data['EmailID'] = htmlspecialchars(trim($data['EmailID']), ENT_QUOTES);
		}
		
		if(isset($_POST["CostID"])) {
			$data['CostID'] = utf8_to_cp1251($_POST["CostID"]);
			$data['CostID'] = htmlspecialchars(trim($data['CostID']), ENT_QUOTES);
			$data['CostID'] = str_replace(" ", "", $data['CostID']);
		}
		
		if(isset($_POST["cCostID"])) {
			$data['cCostID'] = utf8_to_cp1251($_POST["cCostID"]);
			$data['cCostID'] = htmlspecialchars(trim($data['cCostID']), ENT_QUOTES);
		}
		
		if(isset($_POST["ProfitPerMonthID"])) {
			
			$data['ProfitPerMonthID'] = utf8_to_cp1251($_POST["ProfitPerMonthID"]);
			$data['ProfitPerMonthID'] = htmlspecialchars(trim($data['ProfitPerMonthID']), ENT_QUOTES);
			$data['ProfitPerMonthID'] = str_replace(" ", "", $data['ProfitPerMonthID']);
		}
		
		if(isset($_POST["cProfitPerMonthID"])) {
			$data['cProfitPerMonthID'] = utf8_to_cp1251($_POST["cProfitPerMonthID"]);
			$data['cProfitPerMonthID'] = htmlspecialchars(trim($data['cProfitPerMonthID']), ENT_QUOTES);
		}
		
		if(isset($_POST["MaxProfitPerMonthID"])) {
			$data['MaxProfitPerMonthID'] = utf8_to_cp1251($_POST["MaxProfitPerMonthID"]);
			$data['MaxProfitPerMonthID'] = htmlspecialchars(trim($data['MaxProfitPerMonthID']), ENT_QUOTES);
			$data['MaxProfitPerMonthID'] = str_replace(" ", "", $data['MaxProfitPerMonthID']);
		}
		
		if(isset($_POST["cMaxProfitPerMonthID"])) {
			$data['cMaxProfitPerMonthID'] = utf8_to_cp1251($_POST["cMaxProfitPerMonthID"]);
			$data['cMaxProfitPerMonthID'] = htmlspecialchars(trim($data['cMaxProfitPerMonthID']), ENT_QUOTES);
		}
		
		if(isset($_POST["TimeInvestID"])) {
			$data['TimeInvestID'] = utf8_to_cp1251($_POST["TimeInvestID"]);
			$data['TimeInvestID'] = htmlspecialchars(trim($data['TimeInvestID']), ENT_QUOTES);
		}

		if(isset($_POST["MonthAveTurnID"])) {
			$data['MonthAveTurnID'] = utf8_to_cp1251($_POST["MonthAveTurnID"]);
			$data['MonthAveTurnID'] = htmlspecialchars(trim($data['MonthAveTurnID']), ENT_QUOTES);
			$data['MonthAveTurnID'] = str_replace(" ", "", $data['MonthAveTurnID']);
		}
		
		if(isset($_POST["cMonthAveTurnID"])) {
			$data['cMonthAveTurnID'] = utf8_to_cp1251($_POST["cMonthAveTurnID"]);
			$data['cMonthAveTurnID'] = htmlspecialchars(trim($data['cMonthAveTurnID']), ENT_QUOTES);
		}

		if(isset($_POST["MaxMonthAveTurnID"])) {
			$data['MaxMonthAveTurnID'] = utf8_to_cp1251($_POST["MaxMonthAveTurnID"]);
			$data['MaxMonthAveTurnID'] = htmlspecialchars(trim($data['MaxMonthAveTurnID']), ENT_QUOTES);
			$data['MaxMonthAveTurnID'] = str_replace(" ", "", $data['MaxMonthAveTurnID']);
		}
		
		if(isset($_POST["cMaxMonthAveTurnID"])) {
			$data['cMaxMonthAveTurnID'] = utf8_to_cp1251($_POST["cMaxMonthAveTurnID"]);
			$data['cMaxMonthAveTurnID'] = htmlspecialchars(trim($data['cMaxMonthAveTurnID']), ENT_QUOTES);
		}

		if(isset($_POST["MonthExpemseID"])) {
			$data['MonthExpemseID'] = utf8_to_cp1251($_POST["MonthExpemseID"]);
			$data['MonthExpemseID'] = htmlspecialchars(trim($data['MonthExpemseID']), ENT_QUOTES);
			$data['MonthExpemseID'] = str_replace(" ", "", $data['MonthExpemseID']);
		}

		if(isset($_POST["cMonthExpemseID"])) {
			$data['cMonthExpemseID'] = utf8_to_cp1251($_POST["cMonthExpemseID"]);
			$data['cMonthExpemseID'] = htmlspecialchars(trim($data['cMonthExpemseID']), ENT_QUOTES);
		}
		
		if(isset($_POST["SumDebtsID"])) {
			$data['SumDebtsID'] = utf8_to_cp1251($_POST["SumDebtsID"]);
			$data['SumDebtsID'] = htmlspecialchars(trim($data['SumDebtsID']), ENT_QUOTES);
			$data['SumDebtsID'] = str_replace(" ", "", $data['SumDebtsID']);
		}

		if(isset($_POST["cSumDebtsID"])) {
			$data['cSumDebtsID'] = utf8_to_cp1251($_POST["cSumDebtsID"]);
			$data['cSumDebtsID'] = htmlspecialchars(trim($data['cSumDebtsID']), ENT_QUOTES);
		}
		
		if(isset($_POST["WageFundID"])) {
			$data['WageFundID'] = utf8_to_cp1251($_POST["WageFundID"]);
			$data['WageFundID'] = htmlspecialchars(trim($data['WageFundID']), ENT_QUOTES);
			$data['WageFundID'] = str_replace(" ", "", $data['WageFundID']);
		}

		if(isset($_POST["cWageFundID"])) {
			$data['cWageFundID'] = utf8_to_cp1251($_POST["cWageFundID"]);
			$data['cWageFundID'] = htmlspecialchars(trim($data['cWageFundID']), ENT_QUOTES);
		}
		
		if(isset($_POST["AgreementCostID"]) && $_POST["AgreementCostID"] == "true") {
			$data['AgreementCostID'] = 1;
		} else {
			$data['AgreementCostID'] = 0;
		}
		
		if(isset($_POST["CountEmplID"])) {
			$data['CountEmplID'] = utf8_to_cp1251($_POST["CountEmplID"]);
			$data['CountEmplID'] = htmlspecialchars(trim($data['CountEmplID']), ENT_QUOTES);
			$data['CountEmplID'] = intval($data['CountEmplID']);
		}
		
		if(isset($_POST["CountManEmplID"])) {
			$data['CountManEmplID'] = utf8_to_cp1251($_POST["CountManEmplID"]);
			$data['CountManEmplID'] = htmlspecialchars(trim($data['CountManEmplID']), ENT_QUOTES);
			$data['CountManEmplID'] = intval($data['CountManEmplID']);
		}
		
		if(isset($_POST["TermBizID"])) {
			$data['TermBizID'] = utf8_to_cp1251($_POST["TermBizID"]);
			$data['TermBizID'] = htmlspecialchars(trim($data['TermBizID']), ENT_QUOTES);
		}
		
		if(isset($_POST["ShareSaleID"])) {
			$data['ShareSaleID'] = utf8_to_cp1251($_POST["ShareSaleID"]);
			$data['ShareSaleID'] = htmlspecialchars(trim($data['ShareSaleID']), ENT_QUOTES);
			$data['ShareSaleID'] = intval($data['ShareSaleID']);
		}

		if(isset($_POST["maxShareSaleID"])) {
			$data['maxShareSaleID'] = utf8_to_cp1251($_POST["maxShareSaleID"]);
			$data['maxShareSaleID'] = htmlspecialchars(trim($data['maxShareSaleID']), ENT_QUOTES);
			$data['maxShareSaleID'] = intval($data['maxShareSaleID']);
		}
		
		
		if(isset($_POST["ReasonSellBizID"])) {
			$data['ReasonSellBizID'] = utf8_to_cp1251($_POST["ReasonSellBizID"]);
			$data['ReasonSellBizID'] = htmlspecialchars(trim($data['ReasonSellBizID']), ENT_QUOTES);
		}
		
		if(isset($_POST["CategoryID"])) {
			$aCategoryID = split("[:]",$_POST["CategoryID"]);
  			$data["CategoryID"] = $aCategoryID[0];
  			$data["SubCategoryID"] = $aCategoryID[1];
		}

		if(isset($_POST["BizFormID"])) {
			$data['BizFormID'] = utf8_to_cp1251($_POST["BizFormID"]);
			$data['BizFormID'] = htmlspecialchars(trim($data['BizFormID']), ENT_QUOTES);
		}
		
		if(isset($_SESSION['LoginID'])) {
			$data['loginID'] = $_SESSION['LoginID'];
		} else {
			$data['loginID'] = "none";
		}

		if(isset($_SESSION['PasswordID'])) {
			$data['passwordID'] = $_SESSION['PasswordID'];
		} else {
			$data['passwordID'] = "none";
		}
		
		$error = "";
		if(strlen($data['FML']) <= 0) {
			$error = "error_fmlId";
		}
		
		if(strlen($data['PhoneID']) <= 0 && strlen($data['EmailID']) <= 0) {
			$error = "error_phoneId_emailId";
		}
		
		if(strlen($error) <= 0) {
			$sell->Update($data);
  			$useMapGoogle = true;
  			$smarty->clear_cache("./site/detailsSell.tpl", intval($data['ID'])."-".$useMapGoogle);
  			$useMapGoogle = false;
  			$smarty->clear_cache("./site/detailsSell.tpl", intval($data['ID'])."-".$useMapGoogle);			
  			
  			$options = array(
  				'cacheDir' => "../../../sellcache/",
   				'lifeTime' => 1
  			); 
  			$cache = new Cache_Lite($options);
  			//if($dc = $cache->get(''.intval($data['ID']).'____sellitem')) {
  			$cache->remove(''.intval($data['ID']).'____sellitem');
  			//}
		}

  		$dom = new DOMDocument();
  		$n_result = $dom->createElement("result");
  		$n_result->setAttribute("error", cp1251_to_utf8($error));
  		$dom->appendChild($n_result);
  		echo $dom->saveXML();
		
		return;
	}
	
?>