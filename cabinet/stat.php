<?
    global $DATABASE;
    include_once("../phpset.inc");
    include_once("../engine/functions.inc");
    
    AssignDataBaseSetting("../config.ini");

    include_once("../engine/class.StatCabinet.inc");
    $statCabinet = new StatCabinet();
  
    $count = 30;
    $type_sql = 1;
    if(isset($_GET['period'])) {
        
        $periond = (int)$_GET['period'];
        
        if($periond == 1) {
            $count = 30;
        } else if ($periond == 2) {
            $count = 60;
        } else if ($periond == 3) {
            $count = 90;
        } else if ($periond == 4) {
            $type_sql = 2;
        }
        
    } else {
        $_GET['period'] = 1;
    }
    
    $result = $statCabinet->Select(array(
        'type_sql' => $type_sql, 
        'typeId' => '1',
        'objectId' => '45916', 
        'count' => $count, 
    ));
    
?>

<html>
    <HEAD>
        <title>���������� ����������</title>		
        <meta http-equiv="content-type" content="text/html; charset=windows-1251"/>
        <link rel="stylesheet" href="http://cdn.oesmith.co.uk/morris-0.4.3.min.css">
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
        <script src="http://cdn.oesmith.co.uk/morris-0.4.3.min.js"></script>
    </HEAD>

    <body bgcolor="#fffcee">

    <div id="myfirstchart" style="height: 450px;"></div>

        <script language="javascript">

        new Morris.Area({
          // ID of the element in which to draw the chart.
          element: 'myfirstchart',
          // Chart data records -- each entry in this array corresponds to a point on
          // the chart.
          data: [
        <?
            foreach ($result as $k => $v) {
                ?>
                    { year: '<?=$v['gdate'];?>', ryear: '<?=$v['date'];?>', value: <?=$v['count'];?> },            
                <?            
            }
        ?>
          ],
          // The name of the data record attribute that contains x-values.
          xkey: 'year',
          // A list of names of data record attributes that contain y-values.
          ykeys: ['value'],
          // Labels for the ykeys -- will be displayed when you hover over the
          // chart.
          labels: ['�����������'], 
          behaveLikeLine:true, 
          
        });    

        </script>    

        <form method="get">
            <table width="100%" style="background-color:#599a7b;color:white;">
                <tr>
                    <td><input type="radio" name="period" value="1" <?=((int)$_GET['period'] == 1 ? 'checked' : '')?> onclick="this.form.submit();">��������� 30 ����</td>
                    <td><input type="radio" name="period" value="2" <?=((int)$_GET['period'] == 2 ? 'checked' : '')?> onclick="this.form.submit();">��������� 60 ����</td>
                    <td><input type="radio" name="period" value="3" <?=((int)$_GET['period'] == 3 ? 'checked' : '')?> onclick="this.form.submit();">��������� 90 ����</td>
                    <td><input type="radio" name="period" value="4" <?=((int)$_GET['period'] == 4 ? 'checked' : '')?> onclick="this.form.submit();">���������� ���������</td>
                </tr>
            </table>
        </form>    
        
    <body>
</html>        