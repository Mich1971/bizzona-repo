<?php
    //ini_set('xdebug.show_mem_delta', 'On');
    //ini_set('xdebug.collect_return', 'On');
    //ini_set('xdebug.collect_params', '4');

    ini_set('display_errors', 1);
    error_reporting(E_ALL);

    session_start();
    
    global $DATABASE;
    include_once("../phpset.inc");
    include_once("../engine/functions.inc");
    
    AssignDataBaseSetting("../config.ini");

    include_once("./libcabinet.php");
    
    include_once("../engine/class.sell.inc");
    $sell = new Sell();
    
    include_once("../engine/class.ApprovingSell.inc");
    $aSell = new ApprovingSell();
    
    include_once("../engine/class.ContactInfo_Lite.inc");
    $contact = new ContactInfo_Lite();  

    include_once("../engine/class.category.inc"); 
    $category = new Category();

    include_once("../engine/class.subcategory.inc"); 
    $subCategory = new SubCategory();
    
    include_once("../engine/class.ControllerWallet.inc");
    $cwallet = new ControllerWallet();
    
    include_once("../engine/class.services.inc"); 
    $service = new Services();
    
   include_once("../engine/class.ProcessingActions.inc");
   $processingActions = new ProcessingActions();
    
    
    require_once("../libs/Smarty.class.php");
    $smarty = new Smarty;
    $smarty->template_dir = "../templates";
    $smarty->compile_dir  = "../templates_c";
    $smarty->cache_dir = "../cache";
    $smarty->compile_check = true;
    $smarty->debugging     = false;

    $listCategory = $category->Select(array(
        "offset" => 0, 
        "rowCount" => 0, 
        "sort" => 'Count',
    )); 
    $smarty->assign("listCategory", $listCategory);    
    $listSubCategory = $subCategory->Select(array(
        "offset" => 0, 
        "rowCount" => 0, 
    ));
    $smarty->assign("listSubCategory", $listSubCategory);

    
    $assignDescription = AssignDescription("../config.ini");
    
    if(isset($_GET['exit'])) {
        unset($_SESSION['sellId']);
        unset($_SESSION['LoginID']);
        unset($_SESSION['PasswordID']);
    }
    
    if(isset($_POST['auth'])) {

        $sell->LoginID = htmlspecialchars(trim($_POST["loginID"]), ENT_QUOTES);
        $sell->PasswordID = htmlspecialchars(trim($_POST["passwordID"]), ENT_QUOTES);
        
        $sellId = $sell->GetID();
        if(intval($sellId) > 0) {
        
            $_SESSION['sellId'] = $sellId;
            $_SESSION['LoginID'] = $sell->LoginID;
            $_SESSION['PasswordID'] = $sell->PasswordID;
            
        }
        
    }
    
    if(!isset($_SESSION['sellId'])) {
        
        ?>
            <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
            <html>
            <head>
                    <title>���� � ������ �������</title>
                    <script src="../js/jquery.js"></script>
                    <link rel="stylesheet" href="../js/ui.tabs.css" type="text/css" media="screen">
                    <LINK href="../general.css" type="text/css" rel="stylesheet">
                    <script type="text/javascript" src="../js/ui.base.js"></script>
            </head>
            <body bgcolor="#d9d8b9"> 
            <?
                    $output_cabinet = $smarty->fetch("./site/cabinet/login1.tpl");
                    echo minimizer($output_cabinet);
            ?>
            </body>
            </html>
        <?
        
    } else {

    	$cwallet->_codeId = $_SESSION["sellId"];
    	$cwallet->_typeId = 'sell';
        
	global $typeQueryInit;
	
        $smarty->assign("typeQueryInit", $typeQueryInit);
        
        
        
	if(count($_POST) && isset($_POST['addservice']) && intval($_POST['service']) > 0) {
            
                
            
                $aEnabledParams = array();
                $aEnabledActionsTotal = array(); 
                
                foreach($_POST['serviceaction'][$_POST['service']] as $k => $v) {
                    
                    $_temp = split(':', $v);
                    
                    $aEnabledParams[] = array(
                        'serviceId' => (int)$_temp[0],
                        'actionId' => (int)$_temp[1],
                        'statusId' => 1,
                        'summ' => (int)$_temp[2],
                        'codeId' => (int)$_SESSION["sellId"],
                    ); 
                    
                    $aEnabledActionsTotal[] = (int)$_temp[1];
                }

                $balance = $cwallet->GetBalance();
                
                $price = $processingActions->GetTotalPriceActions($aEnabledActionsTotal);
                
                
                if($balance > $price) {
                    
                    $aParams = array();
                    $aParams['serviceId'] = (int)$_POST['service'];
                    $aParams['codeId'] =  (int)$_SESSION["sellId"];
                
                    
                    $processingActions->InsertService($aParams);
                
                    $processingActions->UpdateActionFromCabinet($aEnabledParams);
                    
                    $aPriceActions = $processingActions->GetPriceActions($aEnabledActionsTotal);

                    $cwallet->_codeId = $_SESSION["sellId"];
                    $cwallet->_typeId = 'sell';
                    $cwallet->_serviceId = (int)$_POST['service'];
                    
                    foreach ($aPriceActions as $k => $v) {

                        $cwallet->_summ = $v;
                        $cwallet->_serviceActionId = $k;
                        $cwallet->BuyService();	
                        
                    }
                  
                } else {
                    
                    $smarty->assign("balance_error", "������: ������������ ����� (<span style='color:black;'> - ".($cwallet->_summ - $balance)." ���.</span>) ��� ����������� ���������� ������!");	                    
                    
                }
		
                $balance = $cwallet->GetBalance();
                $smarty->assign("balance", $balance);
                 
	}
        
        
        $aParams =  array(
                        'codeId' => $_SESSION['sellId'], 
                        'typeId' => 'sell', 
                    );
        
        $contact->GetContactInfo();
        $smarty->assign("contact", $contact);

        $isExists  = false;
        
        if($aSell->IsExists($aParams)) {
            $dataSell = $aSell->Select($aParams);
            $smarty->assign('waiting', true);
            $isExists = true;
            
            $comment = $aSell->GetCooment($aParams);
            $smarty->assign('comment', $comment);
            
        } else {  
            $dataSell = $sell->GetItemAdmin(array('ID' => $_SESSION['sellId']));
        }

        if(isset($_POST['updatestatus'])) {
            
            $aStatus = array(
                'StatusID' => (int)$_POST['status'],
                'ID' => $_SESSION['sellId'], 
            );

            if($isExists) {
                $dataSell->StatusID = $aStatus['StatusID'];
                $aSell->data = $dataSell;
                $aSell->Save($aParams);
            } else {
                $dataSell = $sell->GetItemAdmin(array('ID' => $_SESSION['sellId']));
            }
            
            $sell->UpdateStatusID($aStatus);
            
        }
        
        if(isset($_GET['dimg'])) {
            
            if($_GET['dimg'] == 'img_1') {
                
                $dataSell->sImg1ID = '';
                $dataSell->Img1ID = '';
                
            } else if ($_GET['dimg'] == 'img_2') {

                $dataSell->sImg2ID = '';
                $dataSell->Img2ID = '';
                
            } else if ($_GET['dimg'] == 'img_3') {

                $dataSell->sImg3ID = '';
                $dataSell->Img3ID = '';
                
            } else if ($_GET['dimg'] == 'img_4') {

                $dataSell->sImg4ID = '';
                $dataSell->Img4ID = '';
                
            } else if ($_GET['dimg'] == 'img') {

                $dataSell->sImgID = '';
                $dataSell->ImgID = '';
                
            }
            
            $aSell->data = $dataSell;
            $aSell->Save($aParams);
            
            header ("Location: http://www.bizzona.ru/cabinet/index.php");
	    exit();
            
            $smarty->assign('waiting', true);            
        }
        
        if(isset($_FILES['img_1']) && strlen($_FILES["img_1"]["name"]) > 0) {

            $newname_ImgID = LoadClientImg("img_1");
            $dataSell->Img1ID = $newname_ImgID;
            $sImgFile = RatioResizeImg300("../simg/".$dataSell->Img1ID, "../simg/".uniqid("sell_"));
            $splImgFile= split("\/", $sImgFile);
            $dataSell->sImg1ID = $splImgFile[sizeof($splImgFile)-1];
            
            $aSell->data = $dataSell;
            $aSell->Save($aParams);
            
            $smarty->assign('waiting', true);            
        }

        if(isset($_FILES['img_2']) && strlen($_FILES["img_2"]["name"]) > 0) {

            $newname_ImgID = LoadClientImg("img_2");
            $dataSell->Img2ID = $newname_ImgID;
            $sImgFile = RatioResizeImg300("../simg/".$dataSell->Img2ID, "../simg/".uniqid("sell_"));
            $splImgFile= split("\/", $sImgFile);
            $dataSell->sImg2ID = $splImgFile[sizeof($splImgFile)-1];
            
            $aSell->data = $dataSell;
            $aSell->Save($aParams);
            
            $smarty->assign('waiting', true);            
        }
        
        if(isset($_FILES['img_3']) && strlen($_FILES["img_3"]["name"]) > 0) {

            $newname_ImgID = LoadClientImg("img_3");
            $dataSell->Img3ID = $newname_ImgID;
            $sImgFile = RatioResizeImg300("../simg/".$dataSell->Img3ID, "../simg/".uniqid("sell_"));
            $splImgFile= split("\/", $sImgFile);
            $dataSell->sImg3ID = $splImgFile[sizeof($splImgFile)-1];
            
            $aSell->data = $dataSell;
            $aSell->Save($aParams);
            
            $smarty->assign('waiting', true);            
        }

        if(isset($_FILES['img_4']) && strlen($_FILES["img_4"]["name"]) > 0) {

            $newname_ImgID = LoadClientImg("img_4");
            $dataSell->Img4ID = $newname_ImgID;
            $sImgFile = RatioResizeImg300("../simg/".$dataSell->Img4ID, "../simg/".uniqid("sell_"));
            $splImgFile= split("\/", $sImgFile);
            $dataSell->sImg4ID = $splImgFile[sizeof($splImgFile)-1];
            
            $aSell->data = $dataSell;
            $aSell->Save($aParams);
            
            $smarty->assign('waiting', true);            
        }
        

        if(isset($_FILES['img']) && strlen($_FILES["img"]["name"]) > 0) {

            $newname_ImgID = LoadClientImg("img");
            $dataSell->ImgID = $newname_ImgID;
            $sImgFile = RatioResizeImg("../simg/".$dataSell->ImgID, "../simg/".uniqid("sell_"));
            $splImgFile= split("\/", $sImgFile);
            $dataSell->sImgID = $splImgFile[sizeof($splImgFile)-1];
            
            $aSell->data = $dataSell;
            $aSell->Save($aParams);
            
            $smarty->assign('waiting', true);            
        }
        
        
        
        
        if(isset($_POST['sell'])) { 
  
            $dataSell->CostID = addslashes(str_replace(' ', '', trim($_POST['CostID'])));
            $dataSell->cCostID = addslashes($_POST['cCostID']);
            $dataSell->NameBizID = addslashes($_POST['NameBizID']);
            $dataSell->ShortDetailsID = addslashes($_POST['ShortDetailsID']);
            $dataSell->ProfitPerMonthID = addslashes(str_replace(' ', '', trim($_POST['ProfitPerMonthID'])));
            $dataSell->MaxProfitPerMonthID = addslashes(str_replace(' ', '', trim($_POST['MaxProfitPerMonthID'])));
            $dataSell->cProfitPerMonthID = addslashes($_POST['cProfitPerMonthID']);
            $dataSell->cMaxProfitPerMonthID = addslashes($_POST['cMaxProfitPerMonthID']);
            
            $dataSell->MonthAveTurnID = addslashes(str_replace(' ', '', trim($_POST['MonthAveTurnID'])));
            $dataSell->MaxMonthAveTurnID = addslashes(str_replace(' ', '', trim($_POST['MaxMonthAveTurnID'])));
            $dataSell->cMonthAveTurnID = addslashes($_POST['cMonthAveTurnID']);
            $dataSell->cMaxMonthAveTurnID = addslashes($_POST['cMaxMonthAveTurnID']);
            
            $dataSell->MonthExpemseID = addslashes(str_replace(' ', '', trim($_POST['MonthExpemseID'])));
            $dataSell->cMonthExpemseID = addslashes($_POST['cMonthExpemseID']);
            
            $dataSell->TimeInvestID = addslashes($_POST['TimeInvestID']);
            
            $dataSell->WageFundID = addslashes(str_replace(' ', '', trim($_POST['WageFundID'])));
            $dataSell->cWageFundID = addslashes($_POST['cWageFundID']);
            
            $dataSell->ArendaCostID = addslashes(str_replace(' ', '', trim($_POST['ArendaCostID'])));
            $dataSell->cArendaCostID = addslashes($_POST['cArendaCostID']);
            
            $dataSell->ShareSaleID = addslashes($_POST['ShareSaleID']);
            $dataSell->maxShareSaleID = addslashes($_POST['maxShareSaleID']);
            
            $dataSell->SumDebtsID = addslashes(str_replace(' ', '', trim($_POST['SumDebtsID'])));
            $dataSell->cSumDebtsID = addslashes($_POST['cSumDebtsID']);
            
            $dataSell->TermBizID = addslashes($_POST['TermBizID']);
            $dataSell->CountEmplID = addslashes($_POST['CountEmplID']);
            $dataSell->CountManEmplID = addslashes($_POST['CountManEmplID']);
            $dataSell->ContactID = addslashes($_POST['ContactID']);
            $dataSell->DetailsID = addslashes($_POST['DetailsID']);
            $dataSell->MatPropertyID = addslashes($_POST['MatPropertyID']);
            $dataSell->ListObjectID = addslashes($_POST['ListObjectID']);
            
            $dataSell->FML = addslashes($_POST['FML']);
            $dataSell->EmailID = addslashes($_POST['EmailID']);
            $dataSell->PhoneID = addslashes($_POST['PhoneID']);
            $dataSell->FaxID = addslashes($_POST['FaxID']);
            $dataSell->SiteID = addslashes($_POST['SiteID']);
            
            $dataSell->AgreementCostID = (isset($_POST['AgreementCostID']) ? 1 : 0);
            $dataSell->torgStatusID = (isset($_POST['torgStatusID']) ? 1 : 0);
            $dataSell->BizFormID = addslashes($_POST['BizFormID']);
            $dataSell->ReasonSellBizID = addslashes($_POST['ReasonSellBizID']);
    
            $aCategoryID = split("[:]",$_POST["CategoryID"]);
            $dataSell->CategoryID = (isset($aCategoryID[0]) ? $aCategoryID[0] : 0);
            $dataSell->SubCategoryID = (isset($aCategoryID[1]) ? $aCategoryID[1] : 0);
            
            $aSell->data = $dataSell;
            //xdebug_start_trace();
            $aSell->Save($aParams);
            //xdebug_stop_trace();    
            
            $smarty->assign('waiting', true);
        }

    	$balance = $cwallet->GetBalance();
    	$smarty->assign("balance", $balance);
        
        $smarty->assign("aListIncome", $cwallet->ListIncome());    
        $smarty->assign("aListOutcome", $cwallet->ListOutcome());    
        
	$smarty->assign("aService", $service->SelectForCabinet2());        
        
        $smarty->assign("datasell", $dataSell);
        
        ?>
            <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">            
            <html>
            <head>
                <LINK href="http://www.bizzona.ru/general.css" type="text/css" rel="stylesheet">
                <meta http-equiv="content-type" content="text/html; charset=windows-1251"/>
                <title>������ �������</title>
            </head>
            
            
        <?    
            $smarty->display("./site/cabinet/content.tpl");
        ?>
            </html>
        <?    
        
    }


?>
