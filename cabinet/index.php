<?php
    session_start();

    function PureHtmlSimple($str) {
        return strip_tags($str, '<ul><li><ol><p><strong><b><a><span><em><table><tr><td><th><sub><sup>');        
    }

    //ini_set('xdebug.show_mem_delta', 'On');
    //ini_set('xdebug.collect_return', 'On');
    //ini_set('xdebug.collect_params', '4');

    //ini_set('display_errors', 1);
    //error_reporting(E_ALL);
    
    global $DATABASE;
    include_once("../phpset.inc");
    include_once("../engine/functions.inc");
    include_once("../engine/CreateSubDirImg.php"); 
    
    AssignDataBaseSetting("../config.ini");

    include_once("./libcabinet.php");
    
    include_once("../engine/class.sell.inc");
    $sell = new Sell();
    
    include_once("../engine/class.ApprovingSell.inc");
    $aSell = new ApprovingSell();
    
    include_once("../engine/class.ContactInfo_Lite.inc");
    $contact = new ContactInfo_Lite();  

    include_once("../engine/class.category.inc"); 
    $category = new Category();

    include_once("../engine/class.subcategory.inc"); 
    $subCategory = new SubCategory();
    
    include_once("../engine/class.ControllerWallet.inc");
    $cwallet = new ControllerWallet();
    
    include_once("../engine/class.services.inc"); 
    $service = new Services();
    
    include_once("../engine/class.ProcessingActions.inc");
    $processingActions = new ProcessingActions();
    
    include_once("../engine/class.StatCabinet.inc");
    $statCabinet = new StatCabinet();
    
    include_once("../engine/class.communication.inc");
    $communication = new Communication();
    
    include_once("../engine/class.review.inc");
    $review = new Review();
    
    require_once("../libs/Smarty.class.php");
    $smarty = new Smarty;
    $smarty->template_dir = "../templates";
    $smarty->compile_dir  = "../templates_c";
    $smarty->cache_dir = "../cache";
    $smarty->compile_check = true;
    $smarty->debugging     = false;

    $listCategory = $category->Select(array(
        "offset" => 0, 
        "rowCount" => 0, 
        "sort" => 'Count',
    )); 
    $smarty->assign("listCategory", $listCategory);    
    $listSubCategory = $subCategory->Select(array(
        "offset" => 0, 
        "rowCount" => 0, 
    ));
    $smarty->assign("listSubCategory", $listSubCategory);

    
    $assignDescription = AssignDescription("../config.ini");
    
    if(isset($_GET['exit'])) {
        unset($_SESSION['sellId']);
        unset($_SESSION['LoginID']);
        unset($_SESSION['PasswordID']);
    }
    
    if(isset($_POST['auth'])) {

        $sell->LoginID = htmlspecialchars(trim($_POST["loginID"]), ENT_QUOTES);
        $sell->PasswordID = htmlspecialchars(trim($_POST["passwordID"]), ENT_QUOTES);
        
        $sellId = $sell->GetID();
        if(intval($sellId) > 0) {
        
            $_SESSION['sellId'] = $sellId;
            $_SESSION['LoginID'] = $sell->LoginID;
            $_SESSION['PasswordID'] = $sell->PasswordID;
            
        } else {
            $error_auth = "�� ����� ������� ������";
            $smarty->assign("error_auth", $error_auth);
        }
        
        /*
        $sell->Loging(array(
            'filename' => '../engine/log/login.txt',
            'message' => $_SERVER["REMOTE_ADDR"]." ".date("F j, Y, g:i a")." login: ".$_POST["loginID"]." password: ".$_POST["passwordID"]." => id = ".$sellId."\r\n", 
        ));
        */
        
    }
    
    // up auto auth by get parameters
    
    if(isset($_GET['auth'])) {    

        unset($_SESSION['sellId']);
        unset($_SESSION['LoginID']);
        unset($_SESSION['PasswordID']);

        $sell->LoginID = htmlspecialchars(trim($_GET["loginID"]), ENT_QUOTES);
        $sell->PasswordID = htmlspecialchars(trim($_GET["passwordID"]), ENT_QUOTES);
        
        $sellId = $sell->GetID();
        if(intval($sellId) > 0) {
        
            $_SESSION['sellId'] = $sellId;
            $_SESSION['LoginID'] = $sell->LoginID;
            $_SESSION['PasswordID'] = $sell->PasswordID;
            
        } else {
            $error_auth = "�� ����� ������� ������";
            $smarty->assign("error_auth", $error_auth);
        }
        
        /*
        $sell->Loging(array(
            'filename' => '../engine/log/login.txt',
            'message' => $_SERVER["REMOTE_ADDR"]." ".date("F j, Y, g:i a")." login: ".$_POST["loginID"]." password: ".$_POST["passwordID"]." => id = ".$sellId."\r\n", 
        ));
        */
         
        $act = '';
         
        if(isset($_GET['act'])) {
            $act .= '?act='.trim($_GET['act']);
        }
         
        session_regenerate_id(true);
        header ('Location: ../cabinet/index.php'.$act); 
        session_write_close(); 
        //echo '<script>window.location.href = "http://www.bizzona.ru/cabinet/index.php'.$act.'";</script>';
        exit();
        
    }
    
    // down auto auth by get parameters
    
    if(!isset($_SESSION['sellId'])) {
        
        ?>
            <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
            <html>
            <head>
                    <title>���� � ������ �������</title>
                    <script src="../js/jquery.js"></script>
                    <link rel="stylesheet" href="../js/ui.tabs.css" type="text/css" media="screen">
                    <LINK href="../general.css" type="text/css" rel="stylesheet">
                    <script type="text/javascript" src="../js/ui.base.js"></script>
            </head>
            <body bgcolor="#d9d8b9"> 
            <?
                    $output_cabinet = $smarty->fetch("./site/cabinet/login1.tpl");
                    echo minimizer($output_cabinet);
            ?>
            </body>
            </html>
        <?php
        
    } else {

    	$cwallet->_codeId = $_SESSION["sellId"];
    	$cwallet->_typeId = 'sell';
        
	global $typeQueryInit;
	
        $smarty->assign("typeQueryInit", $typeQueryInit);
        
        if(isset($_GET['act']) && $_GET['act'] == 'statautoupdate') {

            $result = $statCabinet->SelectStatUpdate(array(
                'typeId' => 'sell',
                'codeId' => (int)$_SESSION["sellId"], 
            ));
            
            $extResult = $statCabinet->SelectStatUpdateExt(array(
                'typeId' => 'sell',
                'codeId' => (int)$_SESSION["sellId"], 
            ));
            
            $smarty->assign("statautoupdateview", $result);
            $smarty->assign("extstatautoupdateview", $extResult);
        }

        if(isset($_GET['act']) && $_GET['act'] == 'statview') {

            
            $count = 30;
            $type_sql = 1;
            if(isset($_POST['period'])) {

                $periond = (int)$_POST['period'];

                if($periond == 1) {
                    $count = 30;
                } else if ($periond == 2) {
                    $count = 60;
                } else if ($periond == 3) {
                    $count = 90;
                } else if ($periond == 4) {
                    $type_sql = 2;
                }

            } else {
                $_POST['period'] = 1;
            }

            $result = $statCabinet->Select(array(
                'type_sql' => $type_sql, 
                'typeId' => '1',
                'objectId' => (int)$_SESSION["sellId"], 
                'count' => $count, 
            ));
            
            $smarty->assign("statview", $result);
            $smarty->assign('aPeriod', array(
                '1' => '��������� 30 ����',
                '2' => '��������� 60 ����',
                '3' => '��������� 90 ����',
                '4' => '���������� ���������'
            ));
        }
        
        
	if(count($_POST) && isset($_POST['addservice']) && intval($_POST['service']) > 0) {
            
                $aEnabledParams = array();
                $aEnabledActionsTotal = array(); 
                
                foreach($_POST['serviceaction'][$_POST['service']] as $k => $v) {
                    
                    $_temp = split(':', $v);
                    
                    $aEnabledParams[] = array(
                        'serviceId' => (int)$_temp[0],
                        'actionId' => (int)$_temp[1],
                        'statusId' => 1,
                        'summ' => (int)$_temp[2],
                        'codeId' => (int)$_SESSION["sellId"],
                    ); 
                    
                    $aEnabledActionsTotal[] = (int)$_temp[1];
                }

                $balance = $cwallet->GetBalance();
                
                $price = $processingActions->GetTotalPriceActions($aEnabledActionsTotal);
                
                
                if($balance > $price) {
                    
                    $aParams = array();
                    $aParams['serviceId'] = (int)$_POST['service'];
                    $aParams['codeId'] =  (int)$_SESSION["sellId"];
                
                    
                    $processingActions->InsertService($aParams);
                
                    $processingActions->UpdateActionFromCabinet($aEnabledParams);
                    
                    $aPriceActions = $processingActions->GetPriceActions($aEnabledActionsTotal);

                    $cwallet->_codeId = $_SESSION["sellId"];
                    $cwallet->_typeId = 'sell';
                    $cwallet->_serviceId = (int)$_POST['service'];
                    
                    foreach ($aPriceActions as $k => $v) {

                        $cwallet->_summ = $v;
                        $cwallet->_serviceActionId = $k;
                        $cwallet->BuyService();	
                        
                    }
                  
                } else {
                    
                    $smarty->assign("balance_error", "������: ������������ ����� (<span style='color:black;'> - ".($cwallet->_summ - $balance)." ���.</span>) ��� ����������� ���������� ������!");	                    
                    
                }
		
                $balance = $cwallet->GetBalance();
                $smarty->assign("balance", $balance);
                 
	}
        
        
        $aParams =  array(
                        'codeId' => $_SESSION['sellId'], 
                        'typeId' => 'sell', 
                    );
        
        $contact->GetContactInfo();
        $smarty->assign("contact", $contact);

        $isExists  = false;
        
        if($aSell->IsExists($aParams)) {
            $dataSell = $aSell->Select($aParams);
            $smarty->assign('waiting', true);
            $isExists = true;
            
            $comment = $aSell->GetCooment($aParams);
            $smarty->assign('comment', $comment);
            
        } else {  
            $dataSell = $sell->GetItemAdmin(array('ID' => $_SESSION['sellId']));
        }

        if(isset($_POST['updatestatus'])) {
             
            if($_POST['status'] == 12 && strlen(trim($_POST["review"])) <= 0 && $dataSell->brokerId <= 0) {
                
                $smarty->assign('error', "����������, �������� ��� ������������� ���� ����� � ���������� ���������� ����������.");
                
            } else {
            
                $aStatus = array(
                    'StatusID' => (int)$_POST['status'],
                    'ID' => $_SESSION['sellId'], 
                );

                if($isExists) {
                    $dataSell->StatusID = $aStatus['StatusID'];
                    $aSell->data = $dataSell;
                    $aSell->Save($aParams);
                } else {
                    $dataSell = $sell->GetItemAdmin(array('ID' => $_SESSION['sellId']));
                }
            
                $sell->UpdateStatusID($aStatus);
            
                if(isset($_POST["review"]) && strlen(trim($_POST["review"])) > 0) {
               
                    $review->_comment = addslashes(htmlspecialchars(trim($_POST["review"]), ENT_QUOTES));
                    $review->_id = $_SESSION['sellId'];
                    $review->_type = Review::SELL_TYPE;
                    $review->_fio = $dataSell->FML;  
                    $review->_name = $dataSell->NameBizID;
                
                    $review->Insert();
                }
            
                $dataSell = $sell->GetItemAdmin(array('ID' => $_SESSION['sellId']));
            }
        }
        
        if(isset($_GET['dimg'])) {
            
            if($_GET['dimg'] == 'img_1') {
                
                $dataSell->sImg1ID = '';
                $dataSell->Img1ID = '';
                
            } else if ($_GET['dimg'] == 'img_2') {

                $dataSell->sImg2ID = '';
                $dataSell->Img2ID = '';
                
            } else if ($_GET['dimg'] == 'img_3') {

                $dataSell->sImg3ID = '';
                $dataSell->Img3ID = '';
                
            } else if ($_GET['dimg'] == 'img_4') {

                $dataSell->sImg4ID = '';
                $dataSell->Img4ID = '';
                
            } else if ($_GET['dimg'] == 'img') {

                $dataSell->sImgID = '';
                $dataSell->ImgID = '';
                
            }
            
            $aSell->data = $dataSell;
            $aSell->Save($aParams);
            
            header ("Location: http://www.bizzona.ru/cabinet/index.php");
	    exit();
            
            $smarty->assign('waiting', true);            
        }
        
        if(isset($_FILES['img_1']) && strlen($_FILES["img_1"]["name"]) > 0) {

            $auto_create_path = CreateSubDirImg();
            
            $newname_ImgID = LoadClientImg("img_1");
            $dataSell->Img1ID = $newname_ImgID;
            $sImgFile = RatioResizeImg300("../simg/".$dataSell->Img1ID, "../simg/".$auto_create_path.'/'.uniqid("sell_"));
            $splImgFile= split("\/", $sImgFile);
            $dataSell->sImg1ID = $auto_create_path.'/'.$splImgFile[sizeof($splImgFile)-1];
            
            $aSell->data = $dataSell;
            $aSell->Save($aParams);
            
            $smarty->assign('waiting', true);            
        }

        if(isset($_FILES['img_2']) && strlen($_FILES["img_2"]["name"]) > 0) {

            $auto_create_path = CreateSubDirImg();
            
            $newname_ImgID = LoadClientImg("img_2");
            $dataSell->Img2ID = $newname_ImgID;
            $sImgFile = RatioResizeImg300("../simg/".$dataSell->Img2ID, "../simg/".$auto_create_path.'/'.uniqid("sell_"));
            $splImgFile= split("\/", $sImgFile);
            $dataSell->sImg2ID = $auto_create_path.'/'.$splImgFile[sizeof($splImgFile)-1];
            
            $aSell->data = $dataSell;
            $aSell->Save($aParams);
            
            $smarty->assign('waiting', true);            
        }
        
        if(isset($_FILES['img_3']) && strlen($_FILES["img_3"]["name"]) > 0) {

            $auto_create_path = CreateSubDirImg();
            
            $newname_ImgID = LoadClientImg("img_3");
            $dataSell->Img3ID = $newname_ImgID;
            $sImgFile = RatioResizeImg300("../simg/".$dataSell->Img3ID, "../simg/".$auto_create_path.'/'.uniqid("sell_"));
            $splImgFile= split("\/", $sImgFile);
            $dataSell->sImg3ID = $auto_create_path.'/'.$splImgFile[sizeof($splImgFile)-1];
            
            $aSell->data = $dataSell;
            $aSell->Save($aParams);
            
            $smarty->assign('waiting', true);            
        }

        if(isset($_FILES['img_4']) && strlen($_FILES["img_4"]["name"]) > 0) {

            $auto_create_path = CreateSubDirImg();
            
            $newname_ImgID = LoadClientImg("img_4");
            $dataSell->Img4ID = $newname_ImgID;
            $sImgFile = RatioResizeImg300("../simg/".$dataSell->Img4ID, "../simg/".$auto_create_path.'/'.uniqid("sell_"));
            $splImgFile= split("\/", $sImgFile);
            $dataSell->sImg4ID = $auto_create_path.'/'.$splImgFile[sizeof($splImgFile)-1];
            
            $aSell->data = $dataSell;
            $aSell->Save($aParams);
            
            $smarty->assign('waiting', true);            
        }
        

        if(isset($_FILES['img']) && strlen($_FILES["img"]["name"]) > 0) {

            $auto_create_path = CreateSubDirImg();
            
            $newname_ImgID = LoadClientImg("img");
            $dataSell->ImgID = $newname_ImgID;
            $sImgFile = RatioResizeImg("../simg/".$dataSell->ImgID, "../simg/".$auto_create_path.'/'.uniqid("sell_"));
            $splImgFile= split("\/", $sImgFile);
            $dataSell->sImgID = $auto_create_path.'/'.$splImgFile[sizeof($splImgFile)-1];
            
            $aSell->data = $dataSell;
            $aSell->Save($aParams);
            
            $smarty->assign('waiting', true);            
        }
        
        
        
        
        if(isset($_POST['sell'])) { 
                
            $dataSell->CostID = addslashes(str_replace(' ', '', trim($_POST['CostID'])));
            $dataSell->cCostID = addslashes($_POST['cCostID']);
            $dataSell->NameBizID = addslashes($_POST['NameBizID']);
            $dataSell->ShortDetailsID = addslashes(PureHtmlSimple($_POST['ShortDetailsID']));
            $dataSell->ProfitPerMonthID = addslashes(str_replace(' ', '', trim($_POST['ProfitPerMonthID'])));
            $dataSell->MaxProfitPerMonthID = addslashes(str_replace(' ', '', trim($_POST['MaxProfitPerMonthID'])));
            $dataSell->cProfitPerMonthID = addslashes($_POST['cProfitPerMonthID']);
            $dataSell->cMaxProfitPerMonthID = addslashes($_POST['cMaxProfitPerMonthID']);
            
            $dataSell->MonthAveTurnID = addslashes(str_replace(' ', '', trim($_POST['MonthAveTurnID'])));
            $dataSell->MaxMonthAveTurnID = addslashes(str_replace(' ', '', trim($_POST['MaxMonthAveTurnID'])));
            $dataSell->cMonthAveTurnID = addslashes($_POST['cMonthAveTurnID']);
            $dataSell->cMaxMonthAveTurnID = addslashes($_POST['cMaxMonthAveTurnID']);
            
            $dataSell->MonthExpemseID = addslashes(str_replace(' ', '', trim($_POST['MonthExpemseID'])));
            $dataSell->cMonthExpemseID = addslashes($_POST['cMonthExpemseID']);
            
            $dataSell->TimeInvestID = addslashes($_POST['TimeInvestID']);
            
            $dataSell->WageFundID = addslashes(str_replace(' ', '', trim($_POST['WageFundID'])));
            $dataSell->cWageFundID = addslashes($_POST['cWageFundID']);
            
            $dataSell->ArendaCostID = addslashes(str_replace(' ', '', trim($_POST['ArendaCostID'])));
            $dataSell->cArendaCostID = addslashes($_POST['cArendaCostID']);
            
            $dataSell->ShareSaleID = addslashes($_POST['ShareSaleID']);
            $dataSell->maxShareSaleID = addslashes($_POST['maxShareSaleID']);
            
            $dataSell->SumDebtsID = addslashes(str_replace(' ', '', trim($_POST['SumDebtsID'])));
            $dataSell->cSumDebtsID = addslashes($_POST['cSumDebtsID']);
            
            $dataSell->TermBizID = addslashes($_POST['TermBizID']);
            $dataSell->CountEmplID = addslashes($_POST['CountEmplID']);
            $dataSell->CountManEmplID = addslashes($_POST['CountManEmplID']);
            $dataSell->ContactID = addslashes($_POST['ContactID']);
            
            
            $dataSell->DetailsID = addslashes(PureHtmlSimple($_POST['DetailsID'])); 
            
            $dataSell->MatPropertyID = addslashes(PureHtmlSimple($_POST['MatPropertyID']));
            $dataSell->ListObjectID = addslashes(PureHtmlSimple($_POST['ListObjectID']));
            
            $dataSell->FML = addslashes($_POST['FML']);
            $dataSell->EmailID = addslashes($_POST['EmailID']);
            $dataSell->PhoneID = addslashes($_POST['PhoneID']);
            $dataSell->FaxID = addslashes($_POST['FaxID']);
            $dataSell->SiteID = addslashes($_POST['SiteID']);
            
            $dataSell->AgreementCostID = (isset($_POST['AgreementCostID']) ? 1 : 0);
            $dataSell->torgStatusID = (isset($_POST['torgStatusID']) ? 1 : 0);
            $dataSell->BizFormID = addslashes($_POST['BizFormID']);
            $dataSell->ReasonSellBizID = addslashes($_POST['ReasonSellBizID']);
    
            $aCategoryID = split("[:]",$_POST["CategoryID"]);
            $dataSell->CategoryID = (isset($aCategoryID[0]) ? $aCategoryID[0] : 0);
            $dataSell->SubCategoryID = (isset($aCategoryID[1]) ? $aCategoryID[1] : 0);
            
            $aSell->data = $dataSell;
            //xdebug_start_trace();
            
            if($dataSell->isPromo && $dataSell->pay_summ < 5) { 
                $smarty->assign('comment', '�� ��������� �� ������ "������������ ������"');
            } else {
                $aSell->Save($aParams);
                $smarty->assign('waiting', true);
            }
            //xdebug_stop_trace();    
            
            
        }

    	$balance = $cwallet->GetBalance();
    	$smarty->assign("balance", $balance);
        
        $smarty->assign("aListIncome", $cwallet->ListIncome());    
        $smarty->assign("aListOutcome", $cwallet->ListOutcome());    
        
	$smarty->assign("aService", $service->SelectForCabinet2());        
        
        $smarty->assign("datasell", $dataSell);
        
        
        $aComm = $communication->Select(array(
            'typeId' => 1,
            'objectId' =>  (int)$_SESSION["sellId"],
        ));
        
        $smarty->assign("aComm", $aComm);
        
        //var_dump($aResComm);        
        
        ?> 
            <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">            
            <html>
            <head>
                <LINK href="http://www.bizzona.ru/general.css" type="text/css" rel="stylesheet">
                <meta http-equiv="content-type" content="text/html; charset=windows-1251"/>
                <link rel="stylesheet" href="http://cdn.oesmith.co.uk/morris-0.4.3.min.css">
                <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
                <script src="//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
                <script src="http://cdn.oesmith.co.uk/morris-0.4.3.min.js"></script>
                <title>������ �������</title>
            </head>
            <body>
             
        <?    
            $smarty->display("./site/cabinet/content.tpl");
        ?>
            <body>    
            </html>
        <?    
        
    }


?>
