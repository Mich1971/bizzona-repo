<?
	global $DATABASE;

	include_once("./phpset.inc");
	include_once("./engine/functions.inc"); 

  	$count_sql_call = 0;
  	$start = get_formatted_microtime(); 
  	$base_memory_usage = memory_get_usage();	

	AssignDataBaseSetting();
	
  	//ini_set("display_startup_errors", "on");
  	//ini_set("display_errors", "on");
  	//ini_set("register_globals", "on");  	
	
	include_once("./engine/class.category_lite.inc"); 
	$category = new Category_Lite();

	include_once("./engine/class.country_lite.inc"); 
	$country = new Country_Lite();

	include_once("./engine/class.city_lite.inc"); 
	$city = new City_Lite();

	include_once("./engine/class.sell_lite.inc"); 
	$sell = new Sell_Lite();

	include_once("./engine/class.buy_lite.inc"); 
	$buyer = new Buyer_Lite();

	include_once("./engine/class.coopbiz_lite.inc"); 
	$coopbiz = new CoopBiz_Lite();

  	include_once("./engine/class.district_lite.inc"); 
  	$district = new District_Lite();

  	include_once("./engine/class.subcategory_lite.inc");
  	$subcategory = new SubCategory_Lite();

  	include_once("./engine/class.streets_lite.inc");
  	$streets = new Streets_Lite();
	
  	include_once("./engine/class.ad.inc");
  	$ad = new Ad();   	
	
	require_once("./libs/Smarty.class.php");
	$smarty = new Smarty;
	$smarty->template_dir = "./templates";
	$smarty->compile_dir  = "./templates_c";
	//$smarty->cache_dir = "./cache";
	$smarty->compile_check = true;
	$smarty->debugging     = false;

	require_once("./regfuncsmarty.php");

  
	$action_words = array("category", "city", "price");
	if(isset($_GET["action"])) {
		if(in_array($_GET["action"], $action_words)) {
		} else {
			$_GET["action"] = "";
		}
	} 

  	if(isset($_GET["ID"])) {
  		$_GET["ID"] = intval($_GET["ID"]);
  	}

  	$ad->InitAd($smarty, $_GET);
  	
  	
	if(isset($_GET['aCategoryID'])) {
		$_GET['CategoryID'] = explode(',', $_GET['aCategoryID']);
	}
  
	if(isset($_GET['aCityID'])) {
		$_GET['CityID'] = explode(',', $_GET['aCityID']);
	}  	
  	
  	
	if(isset($_GET['CategoryID']) && is_array($_GET['CategoryID'])) {
  		$aCategoryID = $_GET['CategoryID'];
		array_walk($aCategoryID, create_function('&$v', 'return $v=intval($v);'));  		
	}
  
  	if(isset($_GET['CityID']) && is_array($_GET['CityID'])) {
  		$aCityID = $_GET['CityID'];
		array_walk($aCityID, create_function('&$v', 'return $v=intval($v);'));   	
  	}   	
  	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
  <HEAD>
<?
  if (isset($_GET["action"])) {
    switch(trim($_GET["action"])) {
      case "category": {
        ?>
          <title>���������� ������, ��� "������ ����"</title>		
        <?
      } break;
      case "city": {
        
      	if(isset($_GET["subRegion_id"]) && intval($_GET["subRegion_id"]) > 0) {
      		$dataCity["ID"] = intval($_GET["subRegion_id"]); 
      	} else {
      		$dataCity["ID"] = intval($_GET["ID"]);	
      	}
        
        $objCity = $city->GetItem($dataCity);
        ?>
          <title>���������� ������ - <?=$objCity->title;?>  - ��� "������ ����"</title>		
        <?
      } break;
      case "price": {
        ?>
          <title>���������� ������, ��� "������ ����"</title>
        <?
      } break;
      default: {
        ?>
          <title>���������� ������, ��� "������ ����"</title>
        <?
      }   
    }
  } else {
    ?>
      <title>���������� ������, ��� "������ ����"</title>
    <?
  } 
?>
	<META NAME="Description" CONTENT="���������� ������, ��� ���������� ������, ��� �������� ��� ����������� �������, ���������� ������� �������, ����������� �� ����������� �������">
        <meta name="Keywords" content="���������� ������, ��� ���������� ������, ��� �������� ��� ����������� �������, ���������� ������� �������, ����������� �� ����������� �������"> 
        <LINK href="http://www.bizzona.ru/general.css" type="text/css" rel="stylesheet">
        <meta http-equiv="content-type" content="text/html; charset=windows-1251"/>
	</HEAD>
<body>

<? 
	$headertitle = '���������� ������ '.$objCity->title.'';
	$smarty->assign("headertitle", $headertitle);
?>

<?
  $smarty->display("./site/headerbanner.tpl");
?>

<table class="w" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="2">
			<?
				$topmenu = GetMenu();
				$smarty->assign("topmenu", $topmenu);

				$link_country = GetSubMenu();
				$smarty->assign("ad", $link_country);

				$smarty->caching = false;
				$output_header = $smarty->fetch("./site/headercoop.tpl");
				echo minimizer($output_header);
			?>
		</td>
	</tr>
	<tr>
        <td width="30%" valign="top">
        
        <?
        	$smarty->caching = false; 
			echo fminimizer($smarty->fetch("./site/inner_bannertop_left.tpl"));
		?>        
        
        <?
          $smarty->cache_dir = "./persistentcache"; 
          $smarty->cache_lifetime = 10800;        
        
          if (isset($_GET["action"]) && $_GET["action"]  == "category") {
            $_GET["ID"] = (isset($_GET["ID"])  ? intval($_GET["ID"]) : 0);
            $smarty->caching = true; 
            if (!$smarty->is_cached('./site/categoryc.tpl', $_GET["ID"])) {
              $data = array();
              $data["offset"] = 0;
              $data["rowCount"] = 0;
              $data["sort"] = "CountC";
              $result = $category->Select($data);
              $smarty->assign("data", $result);
            }
            
  			$output_categorys = $smarty->fetch("./site/categoryc.tpl", $_GET["ID"]);
  			echo minimizer($output_categorys);
            $smarty->caching = false; 
          } else { 
            $smarty->caching = true; 
            if (!$smarty->is_cached('./site/categoryc.tpl')) {
              $data = array();
              $data["offset"] = 0;
              $data["rowCount"] = 0;
              $data["sort"] = "CountC";
              $result = $category->Select($data);
              $smarty->assign("data", $result);
            }
            
			$output_categorys = $smarty->fetch("./site/categoryc.tpl");
			echo minimizer($output_categorys);
            
            $smarty->caching = false; 
          }
          
            $smarty->cache_dir = "./cache";
            $smarty->cache_lifetime = 3600;            
          
			$output_call = $smarty->fetch("./site/call.tpl");
			echo minimizer($output_call);
    	?>
		<div style="padding-top:2pt;"></div>                    
        <?
			$output_proposal = $smarty->fetch("./site/proposal.tpl");
			echo minimizer($output_proposal);
        	
			$output_request = $smarty->fetch("./site/request.tpl");
			echo minimizer($output_request);
        ?>              
        </td>
        <td width="70%" valign="top">
			<?
				$smarty->caching = true; 
          		$smarty->cache_dir = "./persistentcache"; 
          		$smarty->cache_lifetime = 10800;				
				if (!$smarty->is_cached('./site/cityc.tpl')) {
					$data = array();
					$data["offset"] = 0;
					$data["rowCount"] = 0;
					$data["sort"] = "CountC";
					$result = $city->SelectActiveC($data);
					$smarty->assign("data", $result);
				} 
				$output_city = $smarty->fetch("./site/cityc.tpl");
				echo minimizer($output_city);
				$smarty->caching = false; 

				// up subcity show
            	$smarty->caching = true; 
            	$smarty->cache_lifetime = 10800;
				$city_cach = ((isset($_GET["action"]) && $_GET["action"] == "city") ? intval($_GET["ID"]) : 0 );
            	if (!$smarty->is_cached('./site/subcityc.tpl', $city_cach)) {
            		$data = array();
            		$data["offset"] = 0;
            		$data["rowCount"] = 0;
					$city->CityID = intval($_GET["ID"]);            		
            		$result = $city->SelectChildActive2Coop($data);
            		$smarty->assign("items", sizeof($result)/3);
                	$smarty->assign("datasubcity", $result);
                	
    				$dataCity["ID"] = intval($_GET["ID"]);	
        			$objCity = $city->GetItem($dataCity);                	
        			$smarty->assign("citytitleparent", $objCity->titleParentSelect);
                } 
                
				$output_city = $smarty->fetch("./site/subcityc.tpl", $city_cach);
				echo minimizer($output_city);
				
				$smarty->cache_dir = "./cache";
				$smarty->cache_lifetime = 3600;
       			$smarty->caching = false;       			
       			// down subcity show           			
				
            	
				if(isset($_GET["action"]) && $_GET["action"]  == "category") {
					$coopbiz->category_id = (isset($_GET["ID"])  ? intval($_GET["ID"]) : 0);
					$res_sub = $coopbiz->SelectSubCategory();
					if(sizeof($res_sub) > 1) {
						$smarty->assign("subbizcategory", $res_sub);
						$output_subbiz = $smarty->fetch("./site/subbizc.tpl");
						echo minimizer($output_subbiz);
					}
				}
				
				$output_navsearch = $smarty->fetch("./site/navsearchc.tpl");
				echo minimizer($output_navsearch);

				$smarty->caching = true; 
				if (!$smarty->is_cached('./site/cityc.tpl', $_SERVER["REQUEST_URI"])) {
					$data = array();
					$dataPage = array(); 
					$pagesplit = GetPageSplit();

					if (isset($_GET["offset"])) {
						$data["offset"] = intval($_GET["offset"]);
					} else {
						$data["offset"] = 0;
					}
					if (isset($_GET["rowCount"])) { 
						$data["rowCount"] = intval($_GET["rowCount"]);
					} else {
						$data["rowCount"] = $pagesplit;
					}

					$data["sort"] = "ID";

                    if(isset($_GET["subRegion_id"]) && intval($_GET["subRegion_id"]) > 0) {
                    	$data["subRegion_id"] = intval($_GET["subRegion_id"]);
                    	$dataPage["subRegion_id"] = intval($_GET["subRegion_id"]);
                    }
					
					if (isset($_GET["action"])) {
                       switch(trim($_GET["action"])) {
                         case "category": {
                           $_GET["ID"] = intval($_GET["ID"]); 
                           $data["category_id"] = $_GET["ID"];
                           $dataPage["category_id"] = $_GET["ID"];
                           
                           if(isset($_GET["subcategory_id"]) && intval($_GET["subcategory_id"]) > 0) {
                             $_GET["subcategory_id"] = intval($_GET["subcategory_id"]);	
                             $data["subcategory_id"] = $_GET["subcategory_id"];
                             $dataPage["subcategory_id"] = $_GET["subcategory_id"];
                           }

                         } break;
                         case "city": {
                           $_GET["ID"] = intval($_GET["ID"]);
                           $data["region_id"] = $_GET["ID"];
                           $dataPage["region_id"] = $_GET["ID"];
                         } break;
                         default: {
                         }   
                       }
                    }

                    if(isset($aCategoryID)) {
                    	$dataPage["aCategoryID"] = $aCategoryID;
                    	$data["aCategoryID"] = $aCategoryID;
                    }
                    if(isset($aCityID)) {
                    	$dataPage["aCityID"] = $aCityID;
                    	$data["aCityID"] = $aCityID;
                    }                    

                    $dataPage["offset"] = 0;
                    $dataPage["rowCount"] = 0;
                    $dataPage["sort"] = "id";
                    $dataPage["status_id"] = "-1";
                    $resultPage = $coopbiz->Select($dataPage);
                    $smarty->assign("CountRecord", sizeof($resultPage));
                    $smarty->assign("CountSplit", $pagesplit);
                    $smarty->assign("CountPage", ceil(sizeof($resultPage)/5));

                    $data["status_id"] = "-1";
                    $data["sort"] = "datecreate";

                    $result = $coopbiz->Select($data);

                    $smarty->assign("data", $result);

                    if (isset($_SESSION["listID"])) { 
                      $dataPage["rowCount"] = 0;
                      $dataPage["offset"] = 0;
                      $resultPage = $coopbiz->Select($dataPage); 
                      unset($_SESSION["listID"]);
                    }
                    while (list($ld, $lv) = each($resultPage)) {
                      $_SESSION["listID"][] = $lv->ID; 
                    }

                  }

				  $output_pagesplit = $smarty->fetch("./site/pagesplitc.tpl", $_SERVER["REQUEST_URI"]);
				  echo minimizer($output_pagesplit);
                  
				  $output_ilistsell = $smarty->fetch("./site/listsellc.tpl", $_SERVER["REQUEST_URI"]);
				  echo minimizer($output_ilistsell);
                  
                  $smarty->caching = false; 
               ?>
            	<div class="lftpadding_cnt" style="width: auto;">
       				<div  style="background-image:url(<?=$_SERVER["HTTP_HOST"]?>images/bl.gif); background-repeat:repeat-x;">&nbsp;</div>
            	</div>
            	<?
               		//$smarty->display("./site/pagesplit.tpl");
    			  	$output_pagesplit = $smarty->fetch("./site/pagesplitc.tpl");
				  	echo minimizer($output_pagesplit);
            	?> 
            	<?
               		//$smarty->display("./site/navsearch.tpl");
				  	$output_navsearch = $smarty->fetch("./site/navsearchc.tpl");
				  	echo minimizer($output_navsearch);
            	?>
            	
				<div class="lftpadding_cnt" style="padding-top:10pt; padding-bottom: 5pt;padding-top:0pt; padding-bottom:0pt; width:auto;" >	    
					<table border="0" align="right" width="100%">
                		<tr>
                			<td align="right">
								<script type="text/javascript" src="//yandex.st/share/share.js" charset="utf-8"></script>
								<div class="yashare-auto-init" data-yashareType="button" data-yashareQuickServices="yaru,vkontakte,facebook,twitter,odnoklassniki,moimir,lj,friendfeed,moikrug"></div>
               				</td>			
						</tr>
					</table>
				</div>            	
            	
        </td>
    </tr>
    <tr>
    	<td colspan="2">
<div style="padding-top:4pt;padding-bottom:5pt;padding-right:10pt;padding-left:10pt;font-size:10pt;font-family:arial;">
	<table cellpadding="0" cellspacing="0" border="0"  style="width:100%;padding-bottom:2pt;" bgcolor="#eeeee0">
    	<tr>
        	<td style="background-image:url(http://<?=$_SERVER["HTTP_HOST"]?>/images/d1.gif); background-repeat:no-repeat;width:5px;height:5px; background-position:left top;">&nbsp;</td>
            <td rowspan="2" valign="middle" align="center"  style="font-size:8pt; font-family:Arial; color:black;">
			<?
			?>
            </td>
            <td style="background-image:url(http://<?=$_SERVER["HTTP_HOST"]?>/images/d2.gif); background-repeat:no-repeat;width:5px;height:5px;background-position:right top;">&nbsp;</td>
        </tr>
        <tr>
            <td style="background-image:url(http://<?=$_SERVER["HTTP_HOST"]?>/images/d4.gif); background-repeat:no-repeat;width:5px;height:5px; background-position:left bottom;">&nbsp;</td>
            <td style="background-image:url(http://<?=$_SERVER["HTTP_HOST"]?>/images/d3.gif); background-repeat:no-repeat;width:5px;height:5px; background-position:right bottom;">&nbsp;</td>
        </tr>
    </table>
</div>
<?
	$smarty->display("./site/footer.tpl");
?>
    	</td>
    </tr>
</table>
<?
	$end = get_formatted_microtime(); 
	$total = $end - $start;
	echo "<center><span style='font-size:7pt;'>".round($total, 6)." : ".$count_sql_call." : ".memory_get_usage()." </span><center>";
?>
</body>
</html>