<?
	global $DATABASE;
	include_once("../phpset.inc");
	include_once("../engine/functions.inc");
	include_once("../engine/valuation_functions.inc");	
	include_once("../engine/analitics_functions.inc");

	AssignDataBaseSetting("../config.ini");

	//ini_set("display_startup_errors", "on");
	//ini_set("display_errors", "on");  
	//ini_set("register_globals", "on");	

  	require_once("../libs/Smarty.class.php");
  	$smarty = new Smarty;
  	$smarty->template_dir = "../templates";
  	$smarty->compile_dir  = "../templates_c";
  	$smarty->cache_dir = "../cache";
  	$smarty->compile_check = true;
  	$smarty->debugging     = false;
	
	include_once("../engine/businessvaluation/class.valuation_barber.inc");
	$valuation_barber = new Valuation_Barber();
	
	if(isset($_POST) && sizeof($_POST) > 0 && !isset($_POST["order"])) {	
		
		$_POST["profit"] = intval(trim(str_replace(" ","",$_POST["profit"])));
		$_POST["area"] = intval($_POST["area"]);
		$_POST["arendatype"] = intval($_POST["arendatype"]);
		$_POST["cityId"] = intval($_POST["cityId"]);
		$_POST["countplace"] = intval($_POST["countplace"]);
		$_POST["locaton"] = intval($_POST["locaton"]);
		
		$valuation_barber->profit = $_POST["profit"];
		$valuation_barber->area = $_POST["area"];
		$valuation_barber->arendatype = $_POST["arendatype"];
		$valuation_barber->cityId = $_POST["cityId"];
		$valuation_barber->countplace = $_POST["countplace"];
		$valuation_barber->locaton = $_POST["locaton"];

		if($valuation_barber->profit > 0) {
		
			$smarty->assign("businessvaluation", $valuation_barber->GetBusinessValuation());
		}
		
		$smarty->assign("valuation_barber", $valuation_barber);
		
	} else if (isset($_POST) && sizeof($_POST) > 0 && isset($_POST["order"])) {
		$res_order = SendOrderValuation();
		$smarty->assign("res_order", $res_order);
	}
	
	$array_city = $valuation_barber->GetCity();
	$smarty->assign("array_city", $array_city);
?>
	
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
  <HEAD>
   <title>������ ��������� �������: �������������� - ��� "������-����"</title>		
  
	<META NAME="Description" CONTENT="������ ��������� �������: ��������������">
        <meta name="Keywords" content="������ �������, ������ ��������� �������,������ ��������� ��������������"> 
        <LINK href="http://www.bizzona.ru/general.css" type="text/css" rel="stylesheet">
        <meta http-equiv="content-type" content="text/html; charset=windows-1251"/>
        <link rel="shortcut icon" href="http://www.bizzona.ru/images/favicon.ico">
    </HEAD>
<body bgcolor="#7c3535">

<?
	$topmenu = GetMenu();
	$smarty->assign("topmenu", $topmenu);
	
	$link_country = GetSubMenuRightAnalitics();
	$smarty->assign("ad", $link_country);
?>
<table class="w" border="0" cellpadding="0" cellspacing="0" bgcolor="#eeeee0">
	<tr>
		<td colspan="2">
			<?
				$output_header = $smarty->fetch("./site/headervaluation.tpl");
				echo minimizer($output_header);			
			?>	
		</td>
	</tr>
</table>	
<table class="w" border="0" cellpadding="0" cellspacing="0" bgcolor="#eeeee0">	
    <tr>
        <td valign="top" style="padding:2pt;" width="50%" align="left">
        	<?
				$output_header = $smarty->fetch("./site/businessvaluation/valuation_barber.tpl");
				echo minimizer($output_header);	        	
			?>
			
			<?
				$output_order = $smarty->fetch("./site/businessvaluation/order.tpl");
				echo minimizer($output_order);	
			?>			
        </td>
        <td width="50%" valign="top" style="padding:10px;" >
			<div style='font-family:arial;font-size:11pt;border-top: 1px; border-bottom: 1px; border-left:1px;  border-right:1px;  border-color:#C1C1A4;  border-style: solid;padding:5px;background-color:#ffffff;' >
				<img src="http://www.bizzona.ru/iconbiz/osenka-biznesa-parikmaherskyaya.png"  align="right" alt="������ ������� ��������������" title="������ ������� ��������������">
				�������� ��� �������-�����  �����������  <b>������ � ��������� ����-��������� ������� ������ ��������� �������</b> (��������������)  ������ �� �������� ����������� �����������  ������ ������ � ��� ������� ������������� � �����. ��� ��� ������� ������������, �� �� ��� � �������� ��������� ����������� ����� ��� ����, ����� �������� � ���������� ����������� ������. �� ��������  ���������������� ������� �������������  �����  ��������� ����� ������ ��� ������ �� ���� ������ ��������. 
				<br><br>
				<b><i>� ��������� ����� ����������� ������ ������� ��������� � �������� ������.</i></b>
				<div align="right" style="color:#737474;font-size:10pt;">
					���� ��������: <span style="color:#3b3b3b;">12 ���� 2011 ���</span>
				</div>
			</div>		
			
			<div style="padding-top:10pt; padding-bottom: 5pt;padding-top:0pt; padding-bottom:0pt; width:auto;" >	    
				<table border="0" align="right" width="100%">
               		<tr>
               			<td align="right">
							<script type="text/javascript" src="//yandex.st/share/share.js" charset="utf-8"></script>
							<div class="yashare-auto-init" data-yashareType="button" data-yashareQuickServices="yaru,vkontakte,facebook,twitter,odnoklassniki,moimir,lj,friendfeed,moikrug"></div>
           				</td>			
					</tr>
				</table>
			</div>
        </td>
    </tr>

	<tr>
		<td colspan="2">
		<?
			$subdomain_city = $smarty->fetch("./site/site_city.tpl");
			echo fminimizer($subdomain_city);

			$smarty->display("./site/footer.tpl");
		?>
		</td>
	</tr>

	
	
</table>

<script language='javascript'>
  function SellPreviewAdd() {
  	
  }
  function AddToSearch(obj) {
	var input_search = document.getElementById("searchtext");
	if(input_search != null) {
		if(obj.innerText != null) {
			input_search.value = obj.innerText;
		} else if (obj.innerHTML != null) {
			input_search.value = obj.innerHTML;
		}
	}
  }  
</script>

</body>
</html>	