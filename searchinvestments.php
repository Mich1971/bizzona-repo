<?
  global $DATABASE;

  include_once("./phpset.inc");
  include_once("./engine/functions.inc"); 

  $count_sql_call = 0;
  $start = get_formatted_microtime(); 
  $base_memory_usage = memory_get_usage();	
  
  AssignDataBaseSetting();
  
	include_once("./engine/class.category_lite.inc"); 
	$category = new Category_Lite();

	include_once("./engine/class.country_lite.inc"); 
	$country = new Country_Lite();	

	include_once("./engine/class.city_lite.inc"); 
	$city = new City_Lite();

	include_once("./engine/class.sell_lite.inc"); 
	$sell = new Sell_Lite();
	
	include_once("./engine/class.buy_lite.inc"); 
	$buyer = new Buyer_Lite();

  	include_once("./engine/class.district_lite.inc"); 
  	$district = new District_Lite();

  	include_once("./engine/class.subcategory_lite.inc");
  	$subcategory = new SubCategory_Lite();

  include_once("./engine/class.investments_lite.inc"); 
  $investments = new Investments_Lite();

  include_once("./engine/class.streets_lite.inc");
  $streets = new Streets_Lite();

  	include_once("./engine/class.ad.inc");
  	$ad = new Ad();  
  
  require_once("./libs/Smarty.class.php");
  $smarty = new Smarty;
  $smarty->template_dir = "./templates";
  $smarty->compile_dir  = "./templates_c";
  //$smarty->cache_dir = "./cache";
  $smarty->compile_check = true;
  $smarty->debugging     = false;

  require_once("./regfuncsmarty.php");

  if(isset($_GET["ID"])) {
  	$_GET["ID"] = intval($_GET["ID"]);
  }
  
	$action_words = array("category", "city", "price", "typeBizID");
	if(isset($_GET["action"])) {
		if(in_array($_GET["action"], $action_words)) {
		} else {
			$_GET["action"] = "";
		}
	} 
	
  $ad->InitAd($smarty, $_GET);  		
  
  
	if(isset($_GET['aCategoryID'])) {
		$_GET['CategoryID'] = explode(',', $_GET['aCategoryID']);
	}
  
	if(isset($_GET['aCityID'])) {
		$_GET['CityID'] = explode(',', $_GET['aCityID']);
	}  
  
  
	if(isset($_GET['CategoryID']) && is_array($_GET['CategoryID'])) {
  		$aCategoryID = $_GET['CategoryID'];
		array_walk($aCategoryID, create_function('&$v', 'return $v=intval($v);'));  		
	}
  
  	if(isset($_GET['CityID']) && is_array($_GET['CityID'])) {
  		$aCityID = $_GET['CityID'];
		array_walk($aCityID, create_function('&$v', 'return $v=intval($v);'));   	
  	}  
  

  	if(isset($_GET["startcostf"])) {
  		$_GET["startcostf"] = intval($_GET["startcostf"]);
  	} else {
  		$_GET["startcostf"] = 0;
  	}
  
  	if(isset($_GET["stopcostf"])) {
  		$_GET["stopcostf"] = intval($_GET["stopcostf"]);
  	} else {
  		$_GET["stopcostf"] = 0;
  	}

  	if((intval($_GET["startcostf"]) > intval($_GET["stopcostf"])) && (intval($_GET["startcostf"]) > 0 && intval($_GET["stopcostf"]) > 0) ) {
  		$temp_c  = $_GET["stopcostf"];
  		$_GET["stopcostf"] = $_GET["startcostf"];
  		$_GET["startcostf"] = $temp_c;
  	}   	
  	
  	
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
  <HEAD>
<?
  if (isset($_GET["action"])) {
    switch(trim($_GET["action"])) {
      case "category": {
        ?><title>������� �������������� - ��� "������ ����"</title><?
      } break;
      case "city": {
      	if(isset($_GET["subRegion_id"]) && intval($_GET["subRegion_id"]) > 0) {
      		$dataCity["ID"] = intval($_GET["subRegion_id"]); 
      	} else {
      		$dataCity["ID"] = intval($_GET["ID"]);	
      	}
      	
        $objCity = $city->GetItem($dataCity);
        ?><title>������� �������������� <?=$objCity->title;?> - ��� "������ ����"</title><?

      } break;
      case "price": {
        ?><title>������� �������������� - ��� "������ ����"</title><?
      } break;
      default: {
        ?><title>������� �������������� - ��� "������ ����"</title><?
      }   
    }
  } else {
    ?><title>������� �������������� - ��� "������ ����"</title><?
  } 
?>
	<META NAME="Description" CONTENT="������� ��������������, �������������� �������, ��������������, �������������� ��������, ���������� ��������������, �������������� � ������">
        <meta name="Keywords" content="������� ��������������, �������������� �������, ��������������, �������������� ��������, ���������� ��������������, �������������� � ������"> 
        <LINK href="http://www.bizzona.ru/general.css" type="text/css" rel="stylesheet">
        <meta http-equiv="content-type" content="text/html; charset=windows-1251"/>
   </HEAD>
<body>
<?
  $smarty->display("./site/headerbanner.tpl");
  $smarty->assign("headertitle", "������� ��������������");
?>
<table class="w" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="2">
			<?
				$topmenu = GetMenu();
				$smarty->assign("topmenu", $topmenu);

				$link_country = GetSubMenu();
				$smarty->assign("ad", $link_country);

				$smarty->caching = false;
				$output_header = $smarty->fetch("./site/headeri.tpl");
				echo minimizer($output_header);			
			?>
		</td>
	</tr>
    <tr>
        <td width="30%" valign="top">
        
        <?
        	$smarty->caching = false; 
			echo fminimizer($smarty->fetch("./site/inner_bannertop_left.tpl"));
		?>        
        
        <?
          $smarty->cache_dir = "./persistentcache"; 
          $smarty->cache_lifetime = 10800;
                  
          if (isset($_GET["action"]) && $_GET["action"]  == "category") {
            $_GET["ID"] = (isset($_GET["ID"])  ? intval($_GET["ID"]) : 0);
            $smarty->caching = true; 
            if (!$smarty->is_cached('./site/categorysi.tpl', $_GET["ID"])) {
              $data = array();
              $data["offset"] = 0;
              $data["rowCount"] = 0;
              $data["sort"] = "CountI";
              $result = $category->Select($data);
              $smarty->assign("data", $result);
            }
            
  			$output_categorysi = $smarty->fetch("./site/categorysi.tpl", $_GET["ID"]);
  			echo minimizer($output_categorysi);            
            
            $smarty->caching = false; 
          } else { 
            $smarty->caching = true; 
            if (!$smarty->is_cached('./site/categorysi.tpl')) {
              $data = array();
              $data["offset"] = 0;
              $data["rowCount"] = 0;
              $data["sort"] = "CountI";
              $result = $category->Select($data);
              $smarty->assign("data", $result);
            }

  			$output_categorysi = $smarty->fetch("./site/categorysi.tpl");
  			echo minimizer($output_categorysi);            
            
            $smarty->caching = false; 
          }
          
          $smarty->cache_dir = "./cache";
          $smarty->cache_lifetime = 3600;          
            
         	$datahotbuyer = array();
           	$datahotbuyer["offset"] = 0;
           	$datahotbuyer["rowCount"] = 4;
           	$datahotbuyer["sort"] = "datecreate";
           	$datahotbuyer["StatusID"] = "-1";
            	
                if (isset($_GET["action"])) {
                   switch(trim($_GET["action"])) {
	                   case "category": {
    	                 $datahotbuyer["typeBizID"] = intval($_GET["ID"]);
                       } break;
                       case "city": {
                         $datahotbuyer["regionID"] = intval($_GET["ID"]);
                       } break;                       
        	           default: {
                         		
                       }
                       break;
                   }            	
                }		
            	
                $smarty->display("./site/call.tpl");
			?>
			
		<div style="padding-top:2pt;"></div>
		<?            
			$output_proposal = $smarty->fetch("./site/proposal.tpl");
			echo minimizer($output_proposal);
        	
			$output_request = $smarty->fetch("./site/request.tpl");
			echo minimizer($output_request);            
        ?>    
        </td>
        <td width="70%" valign="top">
            
			<?
            	$smarty->caching = true; 
          		$smarty->cache_dir = "./persistentcache"; 
          		$smarty->cache_lifetime = 10800;
          		
                if (!$smarty->is_cached('./site/cityi.tpl')) {
                	$data = array();
                    $data["offset"] = 0;
                    $data["rowCount"] = 0;
                    $data["sort"] = "CountI";
                    $result = $city->SelectActiveI($data);
                    $smarty->assign("data", $result);
                } 

                $output_cityi = $smarty->fetch("./site/cityi.tpl");
				echo minimizer($output_cityi);
                
          		$smarty->cache_dir = "./cache";
          		$smarty->cache_lifetime = 3600;                
                $smarty->caching = false; 
                
				// up subcity show
            	$smarty->caching = true; 
          		$smarty->cache_dir = "./persistentcache"; 
          		$smarty->cache_lifetime = 10800;            	
				$city_cach = ((isset($_GET["action"]) && $_GET["action"] == "city") ? intval($_GET["ID"]) : 0 );
            	if (!$smarty->is_cached('./site/subcityi.tpl', $city_cach)) {
            		$data = array();
            		$data["offset"] = 0;
            		$data["rowCount"] = 0;
					$city->CityID = intval($_GET["ID"]);            		
            		$result = $city->SelectChildActive2Investments($data);
            		$smarty->assign("items", sizeof($result)/3);
                	$smarty->assign("datasubcity", $result);
                	
    				$dataCity["ID"] = intval($_GET["ID"]);	
        			$objCity = $city->GetItem($dataCity);                	
        			$smarty->assign("citytitleparent", $objCity->titleParentSelect);
                } 
                
				$output_city = $smarty->fetch("./site/subcityi.tpl", $city_cach);
				echo minimizer($output_city);
				
          		$smarty->cache_dir = "./cache";
          		$smarty->cache_lifetime = 3600;				
       			$smarty->caching = false;       			
       			// down subcity show           			
            ?>
           
            <?
				if(isset($_GET["action"]) && $_GET["action"]  == "typeBizID") {
					$investments->category_id = (isset($_GET["ID"])  ? intval($_GET["ID"]) : 0);
					$res_sub = $investments->SelectSubCategory();
					if(sizeof($res_sub) > 1) {
						$smarty->assign("subbizcategory", $res_sub);
						$output_subbiz = $smarty->fetch("./site/subbizi.tpl");
						echo minimizer($output_subbiz);
					}
				}
            ?>
            <?
				$output_navsearchi = $smarty->fetch("./site/navsearchi.tpl");
				echo minimizer($output_navsearchi);
            ?>
               <?
                    $data = array();
                    $dataPage = array(); 
                    $pagesplit = GetPageSplit();

                    if (isset($_GET["offset"])) {
                      $data["offset"] = intval($_GET["offset"]);
                    } else {
                      $data["offset"] = 0;
                    }
                    if (isset($_GET["rowCount"])) { 
                      $data["rowCount"] = intval($_GET["rowCount"]);
                    } else {
                      $data["rowCount"] = $pagesplit;
                    }

                    $data["sort"] = "ID";

                    if (isset($_GET["action"])) {
                       switch(trim($_GET["action"])) {
                         case "typeBizID": {
                           $_GET["ID"] = intval($_GET["ID"]); 
                           $data["category_id"] = $_GET["ID"];
                           $dataPage["category_id"] = $_GET["ID"];
                           
                           if(isset($_GET["subCategory_id"])) {
						     $data["subCategory_id"] = intval($_GET["subCategory_id"]);
                             $dataPage["subCategory_id"] = intval($_GET["subCategory_id"]);
                           }
                           
                         } break;
                         case "city": {
                           $_GET["ID"] = intval($_GET["ID"]);
                           $data["region_id"] = $_GET["ID"];
                           $dataPage["region_id"] = $_GET["ID"];
                         } break;
                         default: {
                         }   
                       }
                    }

                    if(isset($aCategoryID)) {
                    	$dataPage["aCategoryID"] = $aCategoryID;
                    	$data["aCategoryID"] = $aCategoryID;
                    }
                    if(isset($aCityID)) {
                    	$dataPage["aCityID"] = $aCityID;
                    	$data["aCityID"] = $aCityID;
                    }                    

                    
  					$data["stopcostf"] = intval($_GET["stopcostf"]);
  					$data["startcostf"] = intval($_GET["startcostf"]);			
  					$dataPage["stopcostf"] = $data["stopcostf"];
					$dataPage["startcostf"] = $data["startcostf"];                    
                         
                     
                    $dataPage["offset"] = 0;
                    $dataPage["rowCount"] = 0;
                    $dataPage["sort"] = "ID";
                    $dataPage["status_id"] = "-1";
                    $resultPage = $investments->Select($dataPage);
                    
                    $smarty->assign("CountRecord", sizeof($resultPage));
                    $smarty->assign("CountSplit", $pagesplit);
                    $smarty->assign("CountPage", ceil(sizeof($resultPage)/5));

                    $data["status_id"] = "-1";
                    $data["sort"] = "datecreate";

                    $result = $investments->Select($data);
                    
                    $smarty->assign("data", $result);

					$output_pagespliti = $smarty->fetch("./site/pagespliti.tpl", $_SERVER["REQUEST_URI"]);
					echo minimizer($output_pagespliti);                  	
                  	
					$output_ilistselli = $smarty->fetch("./site/ilistselli.tpl", $_SERVER["REQUEST_URI"]);
					echo minimizer($output_ilistselli);                  	
               ?>
            	<div class="lftpadding" style="width: auto;">
       				<div  style="background-image:url(<?=$_SERVER["HTTP_HOST"]?>images/bl.gif); background-repeat:repeat-x;">&nbsp;</div>
            	</div>
            	<?
					$output_pagespliti = $smarty->fetch("./site/pagespliti.tpl", $_SERVER["REQUEST_URI"]);
					echo minimizer($output_pagespliti);             	
            	
					$output_navsearchi = $smarty->fetch("./site/navsearchi.tpl");
					echo minimizer($output_navsearchi);
            	?>
            	
				<div class="lftpadding_cnt" style="padding-top:10pt; padding-bottom: 5pt;padding-top:0pt; padding-bottom:0pt; width:auto;" >	    
					<table border="0" align="right" width="100%">
                		<tr>
                			<td align="right">
								<script type="text/javascript" src="//yandex.st/share/share.js" charset="utf-8"></script>
								<div class="yashare-auto-init" data-yashareType="button" data-yashareQuickServices="yaru,vkontakte,facebook,twitter,odnoklassniki,moimir,lj,friendfeed,moikrug"></div>
               				</td>			
						</tr>
					</table>
				</div>            	
            	
        </td>
    </tr>
    <tr>
    	<td colspan="2">
			<div style="padding-top:4pt;padding-bottom:5pt;padding-right:10pt;padding-left:10pt;font-size:10pt;font-family:arial;">
				<table cellpadding="0" cellspacing="0" border="0"  style="width:100%;padding-bottom:2pt;" bgcolor="#eeeee0">
    				<tr>
        				<td style="background-image:url(http://<?=$_SERVER["HTTP_HOST"]?>/images/d1.gif); background-repeat:no-repeat;width:5px;height:5px; background-position:left top;">&nbsp;</td>
            			<td rowspan="2" valign="middle" align="center"  style="font-size:8pt; font-family:Arial; color:black;">
            			</td>
            			<td style="background-image:url(http://<?=$_SERVER["HTTP_HOST"]?>/images/d2.gif); background-repeat:no-repeat;width:5px;height:5px;background-position:right top;">&nbsp;</td>
        			</tr>
        			<tr>
            			<td style="background-image:url(http://<?=$_SERVER["HTTP_HOST"]?>/images/d4.gif); background-repeat:no-repeat;width:5px;height:5px; background-position:left bottom;">&nbsp;</td>
            			<td style="background-image:url(http://<?=$_SERVER["HTTP_HOST"]?>/images/d3.gif); background-repeat:no-repeat;width:5px;height:5px; background-position:right bottom;">&nbsp;</td>
        			</tr>
    			</table>
			</div>
			<?
				$smarty->display("./site/footer.tpl");
			?>
    	</td>
    </tr>
</table>
<?
  $end = get_formatted_microtime(); 
  $total = $end - $start;
  echo "<center><span style='font-size:7pt;'>".round($total, 6)." : ".$count_sql_call." : ".$base_memory_usage." : ".memoryUsage(memory_get_usage(), $base_memory_usage)." </span><center>";
?>
</body>
</html>
<?
	$sell->Close();
	$category->Close();
	$city->Close();
	$country->Close();
	$buyer->Close();
	$investments->Close();
	$district->Close();
	$subcategory->Close();
	$streets->Close();
?>