<?
  global $DATABASE;

  include_once("./phpset.inc");
  include_once("./engine/functions.inc"); 

  $count_sql_call = 0;
  $start = get_formatted_microtime(); 
  $base_memory_usage = memory_get_usage();	

  AssignDataBaseSetting();
  
  include_once("./engine/class.category_lite.inc"); 
  $category = new Category_Lite();

  include_once("./engine/class.country_lite.inc"); 
  $country = new Country_Lite();

  include_once("./engine/class.city_lite.inc");  
  $city = new City_Lite();

  include_once("./engine/class.sell_lite.inc"); 
  $sell = new Sell_Lite();

  include_once("./engine/class.buy_lite.inc"); 
  $buyer = new Buyer_Lite();

  include_once("./engine/class.district_lite.inc"); 
  $district = new District_Lite();

  include_once("./engine/class.subcategory_lite.inc");
  $subcategory = new SubCategory_Lite();

  include_once("./engine/class.streets_lite.inc");
  $streets = new Streets_Lite();

  require_once("./libs/Smarty.class.php");
  $smarty = new Smarty;
  $smarty->template_dir = "./templates";
  $smarty->compile_dir  = "./templates_c";
  //$smarty->cache_dir = "./cache";
  $smarty->compile_check = true;
  $smarty->debugging     = false;

  require_once("./regfuncsmarty.php");
  
  if(isset($_GET["ID"])) {
  	$_GET["ID"] = intval($_GET["ID"]);
  }
  
  $name_words = array("turagenstvo", "restoran", "parik", "krasota", "otel");
  if(isset($_GET["name"])) {
	if(in_array($_GET["name"], $name_words)) {
	} else {
		$_GET["name"] = "";
	}
  }

  if(isset($_GET["MaxCostID"])) {
  	$_GET["MaxCostID"] = intval($_GET["MaxCostID"]);
  }
  
  if(isset($_GET["MinCostID"])) {
  	$_GET["MinCostID"] = intval($_GET["MinCostID"]);
  }

	$action_words = array("typeBizID", "city", "price");
	if(isset($_GET["action"])) {
		if(in_array($_GET["action"], $action_words)) {
		
		} else {
			$_GET["action"] = "";
		}
	} 

  if(isset($_GET["regionID"])) {
  	$_GET["regionID"] = intval($_GET["regionID"]);
  }
	
  if(isset($_GET["districtID"])) {
  	$_GET["districtID"] = intval($_GET["districtID"]);
  }

	$title = "������� � ������� �������. ������� ������ - ������-����";  
    $seodistrict = "";
  
	if(isset($_GET["districtID"])) {
  		$_GET["districtID"] = intval($_GET["districtID"]);
  		if($_GET["districtID"] > 0)
  		{
  			$district->ID = $_GET["districtID"];
  			$textseosell = $district->GetSeoSell();
  			$textseobuy = $district->GetSeoBuy();
  			if(strlen(trim($textseosell)) > 0 && $textseobuy) {
  				$title = $textseobuy." | ".$textseosell." | ������� ������ - ������-����";  
  				$seodistrict = $textseosell.",".$textseobuy.",";
  			}
  		}
	}
  
  
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
  <HEAD>

<?

  if (isset($_GET["action"])) {
    switch(trim($_GET["action"])) {
      case "typeBizID": {
        ?>
          <title>����� ������� ������. ��� "������-����"</title>		
        <?
        $seokey = $category->GetKeyBuy(intval($_GET["ID"]));
        if(strlen($seokey) > 0) {
        	$seokey = $seokey.", ";
        }        
      } break;
      case "city": {
        $dataCity["ID"] = intval($_GET["ID"]);
        $objCity = $city->GetItem($dataCity);
        $seocity = $objCity->seo;
        if(strlen($seocity) > 0) {
        	$seocity = $seocity.", ";
        }
        ?>
          <title>������� ������� <?=$objCity->title;?> | ������� ������� <?=$objCity->title;?> | ������� ������ - ������-����</title>		
        <?

      } break;
      case "price": {
        ?>
          <title>����� ������� ������. ��� "������-����"</title>
        <?
      } break;
      default: {
        ?>
          <title>����� ������� ������. ��� "������-����"</title>		
        <?
      }   
    }
  } else {
    ?>
      <title><?=$title;?></title>
    <?
  } 

	$headertitle = "����� ������� ������";
?>

	<META NAME="Description" CONTENT="������� �������, ������� �������, ������� ������, ������� �������, ������� �������� �������, ������� �������, ������� ��������,  ������� ��������, ������� ��������, ���� ������� ������?">
        <meta name="Keywords" content="<?=$seodistrict;?><?=$seocity;?><?=$seokey;?>������� �������, ������� �������, ������� ������, ������� �������, ������� �������� �������, ������� �������, ������� ��������,  ������� ��������, ������� ��������, ����� ������� ������"> 
        <LINK href="http://www.bizzona.ru/general.css" type="text/css" rel="stylesheet">
        <meta http-equiv="content-type" content="text/html; charset=windows-1251"/>
  </HEAD>

<body>

<?php
  $smarty->display("./site/headerbanner.tpl");
?>

<table class="w" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="2">
<?
	$topmenu = GetMenu();
	$smarty->assign("topmenu", $topmenu);			
	$link_country = GetSubMenuBuyer();
	$smarty->caching = false; 
	$smarty->assign("headertitle", $headertitle);
	$smarty->assign("ad", $link_country);
	echo minimizer($smarty->fetch("./site/headerbuy.tpl"));
	$smarty->caching = true;
?>		
		</td>
	</tr>
    <tr>
        <td width="30%" valign="top">
         
        <?
          if (isset($_GET["action"]) && $_GET["action"]  == "category") {
            $_GET["ID"] = (isset($_GET["ID"])  ? intval($_GET["ID"]) : 0);
            $smarty->caching = true; 
            if (!$smarty->is_cached('./site/categorysb.tpl', $_GET["ID"])) {
              $data = array();
              $data["offset"] = 0;
              $data["rowCount"] = 0;
              $data["sort"] = "CountB";
              $result = $category->Select($data);
              $smarty->assign("data", $result);
            }
            
   			echo minimizer($smarty->fetch("./site/categorysb.tpl", $_GET["ID"]));
            
            $smarty->caching = false; 
          } else { 
            $smarty->caching = true; 
            if (!$smarty->is_cached('./site/categorysb.tpl')) {
              $data = array();
              $data["offset"] = 0;
              $data["rowCount"] = 0;
              $data["sort"] = "CountB";
              $result = $category->Select($data);
              $smarty->assign("data", $result);
            }
            
   			echo minimizer($smarty->fetch("./site/categorysb.tpl"));
            
            $smarty->caching = false; 
          }
        ?>
            
            <?
            
            	$datahotsell = array();
            	$datahotsell["offset"] = 0;
            	$datahotsell["rowCount"] = 4;
            	$datahotsell["sort"] = "DataCreate";
            	$datahotsell["StatusID"] = "-1";

            	
                if (isset($_GET["action"])) {
                   switch(trim($_GET["action"])) {
	                   case "typeBizID": {
    	                 $datahotsell["CategoryID"] = intval($_GET["ID"]);
                       } break;
                       case "city": {
                         $datahotsell["CityID"] = intval($_GET["ID"]);
                       } break;                       
        	           default: {
                         		
                       }
                       break;
                   }            	
                }		
            	
                //$smarty->display("./site/call.tpl");

   				echo minimizer($smarty->fetch("./site/call.tpl"));
                
                
  	          	$res_datahotsell = $sell->Select($datahotsell);
  	          	
  	          	if(sizeof($res_datahotsell) > 0) {
    				$smarty->assign("datasell", $res_datahotsell);        	
            		
   					echo minimizer($smarty->fetch("./site/iblocksell.tpl"));
  	          	}
			?>
        
            <?
				echo minimizer($smarty->fetch("./site/request.tpl"));
            ?>  
        
        </td>
        <td width="70%" valign="top">
            
       			<?
           			$smarty->caching = true; 
           			$smarty->cache_lifetime = 3600;
           			if (!$smarty->is_cached('./site/cityb.tpl')) {
               			$data = array();
               			$data["offset"] = 0;
               			$data["rowCount"] = 0;
               			$data["sort"] = "CountB";
               			$result = $city->SelectActiveB($data);
               			$smarty->assign("data", $result);
           			} 
           			
					echo minimizer($smarty->fetch("./site/cityb.tpl"));
           			
           			//}
           			$smarty->caching = false; 
           			
           			
					if(isset($_GET["regionID"]) && intval($_GET["regionID"]) > 0)
					{
						if(!$smarty->is_cached('./site/districtbuyer.tpl', intval($_GET["regionID"]))) {
							$district->regionID =  intval($_GET["regionID"]);
							$district_list = $district->SelectActiveBuyer();
							$smarty->assign("districtdata", $district_list);
						}

						echo minimizer($smarty->fetch("./site/districtbuyer.tpl", intval($_GET["regionID"])));
					}
				
       				$smarty->caching = false; 
           			
					if(isset($_GET["action"]) && $_GET["action"]  == "typeBizID") {
						$buyer->typeBizID = (isset($_GET["ID"])  ? intval($_GET["ID"]) : 0);
						$res_sub = $buyer->SelectSubCategory();
						
						if(sizeof($res_sub) > 0)
						{
							$smarty->assign("subbizcategory", $res_sub);
							echo minimizer($smarty->fetch("./site/subbizbuy.tpl"));
						} 
					}
            	?>
            
               	<?
                    $pagesplit = GetPageSplit();

                    if (isset($_GET["offset"])) {
                      $buyer->offset = intval($_GET["offset"]);
                    } else {
                      $buyer->offset = 0;
                    }
                    if (isset($_GET["rowCount"])) { 
                      $buyer->rowCount = intval($_GET["rowCount"]);
                    } else {
                      $buyer->rowCount = $pagesplit;
                    }               

               		$buyer->districtID = (isset($_GET["districtID"]) ? intval($_GET["districtID"]) : "null");
               		$buyer->typeBizID = (isset($_GET["CategoryID"]) ? intval($_GET["CategoryID"]) : "null");
					$buyer->subTypeBizID = (isset($_GET["SubCategoryID"]) ? intval($_GET["SubCategoryID"]) : "null");
               		$buyer->regionID = (isset($_GET["regionID"]) ? intval($_GET["regionID"]) : "null");
                    
                    $resultPage = $buyer->CountSelectDistrict();
                    
                    $smarty->assign("CountRecord", $resultPage);
                    $smarty->assign("CountSplit", $pagesplit);
                    $smarty->assign("CountPage", ceil($resultPage/5));
               		
               		
					$result = $buyer->SelectDistrict();
                    $smarty->assign("data", $result);

                  	//$smarty->display("./site/pagesplitb.tpl", $_SERVER["REQUEST_URI"]);
                  
				  	echo minimizer($smarty->fetch("./site/navsearchbdistrict.tpl", $_SERVER["REQUEST_URI"]));

				    $output_pagesplitb = $smarty->fetch("./site/pagesplitbdistrict.tpl");
				    echo minimizer($output_pagesplitb);
				  	
				  	
                  	$smarty->display("./site/ilistsellb.tpl", $_SERVER["REQUEST_URI"]);
                  
				  	echo $output_ilistsellb;
                  
                  	$smarty->caching = false; 
               ?>

               
            	<div class="lftpadding" style="width: auto;">
       				<div  style="background-image:url(<?=$_SERVER["HTTP_HOST"]?>images/bl.gif); background-repeat:repeat-x;">&nbsp;</div>
            	</div>
               
            	<?
				    echo minimizer($smarty->fetch("./site/pagesplitbdistrict.tpl"));
            	?> 

            	<?
				    echo minimizer($smarty->fetch("./site/navsearchbdistrict.tpl"));
            	?>
         
				<?
					include_once("./yadirect.php");
				?>
            	
				<div class="lftpadding_cnt" style="padding-top:10pt; padding-bottom: 5pt;padding-top:0pt; padding-bottom:0pt; width:auto;" >
				<?
				  	  	
			    $smarty->caching = true; 
            	if (!$smarty->is_cached("./site/keybuy.tpl")) {
					$datakeysell = $category->GetListSubCategoryActiveBuy();
					$smarty->assign("datakeysell", $datakeysell);
            	}
                echo minimizer($smarty->fetch("./site/keybuy.tpl"));
                
                $smarty->caching = false; 
            	?>
           		</div>

				<div class="lftpadding_cnt" style="padding-top:10pt; padding-bottom: 5pt;padding-top:0pt; padding-bottom:0pt; width:auto;" >	    
					<table border="0" align="right" width="100%">
                		<tr>
                			<td width="30%"></td>
                			<td width="70%" align="right">
								<script type="text/javascript" src="//yandex.st/share/share.js" charset="utf-8"></script>
								<div class="yashare-auto-init" data-yashareType="button" data-yashareQuickServices="yaru,vkontakte,facebook,twitter,odnoklassniki,moimir,lj,friendfeed,moikrug"></div>
               				</td>			
						</tr>
					</table>
				</div>           		
           		
				<?
				  if (isset($_GET["action"])) {
    				switch(trim($_GET["action"])) {
						case "city": {
							$parentID = (isset($_GET["ID"])  ? intval($_GET["ID"]) : 0);
							$seocity = $city->GetSeoParent($parentID);
							$dataseocity = $city->GetRegionbyParentForBuyer($parentID);
							if(strlen(trim($seocity->seosell)) > 0 && sizeof($dataseocity) > 1) {
								$smarty->assign("seocity", $seocity);
								$smarty->assign("dataseocity", $dataseocity);
								$smarty->display("./site/hsubregionbuyer.tpl");
							}
						}	break;
    				}
				  }
				?>            		
            
        </td>
    </tr>
	<tr>
		<td colspan="2">

<div style="padding-top:4pt;padding-bottom:5pt;padding-right:10pt;padding-left:10pt;font-size:10pt;font-family:arial;">
	<table cellpadding="0" cellspacing="0" border="0"  style="width:100%;padding-bottom:2pt;" bgcolor="#eeeee0">
    	<tr>
        	<td style="background-image:url(http://<?=$_SERVER["HTTP_HOST"]?>/images/d1.gif); background-repeat:no-repeat;width:5px;height:5px; background-position:left top;">&nbsp;</td>
            <td rowspan="2" valign="middle" align="center"  style="font-size:8pt; font-family:Arial; color:black;">
            </td>
            <td style="background-image:url(http://<?=$_SERVER["HTTP_HOST"]?>/images/d2.gif); background-repeat:no-repeat;width:5px;height:5px;background-position:right top;">&nbsp;</td>
        </tr>
        <tr>
            <td style="background-image:url(http://<?=$_SERVER["HTTP_HOST"]?>/images/d4.gif); background-repeat:no-repeat;width:5px;height:5px; background-position:left bottom;">&nbsp;</td>
            <td style="background-image:url(http://<?=$_SERVER["HTTP_HOST"]?>/images/d3.gif); background-repeat:no-repeat;width:5px;height:5px; background-position:right bottom;">&nbsp;</td>
        </tr>
    </table>
</div>
<?
	$smarty->display("./site/footer.tpl");
?>
		</td>
	</tr>
</table>	
<?
  $end = get_formatted_microtime(); 
  $total = $end - $start;
  echo "<center><span style='font-size:7pt;'>".round($total, 6)." : ".$count_sql_call." : ".$base_memory_usage." : ".memoryUsage(memory_get_usage(), $base_memory_usage)." </span><center>";
?>
</body>
</html>
<?
	$sell->Close();
	$category->Close();
	$city->Close();
	$country->Close();
	$buyer->Close();
	$district->Close();
	$subcategory->Close();
	$streets->Close();
?>