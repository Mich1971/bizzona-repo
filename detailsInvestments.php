<?
  global $DATABASE;

  include_once("./phpset.inc");
  include_once("./engine/functions.inc"); 
  include_once("./engine/investments_functions.inc"); 
  require_once("./engine/Cache/Lite.php");

  $count_sql_call = 0;
  $start = get_formatted_microtime();   
  $base_memory_usage = memory_get_usage();	

  AssignDataBaseSetting();
  
  include_once("./engine/class.category_lite.inc"); 
  $category = new Category_Lite();

  include_once("./engine/class.country_lite.inc"); 
  $country = new Country_Lite();
 
  include_once("./engine/class.city_lite.inc");  
  $city = new City_Lite();

  include_once("./engine/class.buy_lite.inc"); 
  $buyer = new Buyer_Lite();
  
  include_once("./engine/class.equipment_lite.inc"); 
  $equipment = new Equipment_Lite();

  include_once("./engine/class.investments_lite.inc");  
  $investments = new Investments_Lite();

  include_once("./engine/class.sell_lite.inc"); 
  $sell = new Sell_Lite();
  
  include_once("./engine/class.district_lite.inc"); 
  $district = new District_Lite();

  include_once("./engine/class.subcategory_lite.inc");
  $subcategory = new SubCategory_Lite();

  include_once("./engine/class.streets_lite.inc");
  $streets = new Streets_Lite();
  
  	include_once("./engine/class.ad.inc");
  	$ad = new Ad();   
  
  require_once("./libs/Smarty.class.php");
  $smarty = new Smarty;
  $smarty->template_dir = "./templates";
  $smarty->compile_dir  = "./templates_c";
  $smarty->cache_dir = "./cache";
  $smarty->compile_check = true;
  $smarty->debugging     = false;
  //$smarty->caching = true; 

  require_once("./regfuncsmarty.php");
  
  $assignDescription = AssignDescription();

  $investments->id  = intval($_GET["ID"]);

  //$resultGetItem = $investments->GetItem();
  
  // up caching call details investments
  
  $options = array(
  		'cacheDir' => "investmentscache/",
   		'lifeTime' => 2592000
  );
		
  $cache = new Cache_Lite($options);
		
  if ($data = $cache->get(''.$investments->id.'____investmentsitem')) {
	$resultGetItem = unserialize($data);
  } else { 
	$resultGetItem = $investments->GetItem();
	$cache->save(serialize($resultGetItem));
  }

  if($resultGetItem->status_id == 17) {
    header ("Location: http://www.bizzona.ru");  
  }

  
  // donw caching call details investments  
  
	$ad->InitAd($smarty, $_GET);    
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
  <HEAD>
      <title>������� ��������������: <?=$resultGetItem->name;?>,  ��� "������ ����" </title>		
		<META NAME="Description" CONTENT="������� ��������������, <?=$resultGetItem->name;?>, �������������� �������, ��������������, �������������� ��������, ���������� ��������������, �������������� � ������">
        <meta name="Keywords" content="������� ��������������, <?=$resultGetItem->name;?>, �������������� �������, ��������������, �������������� ��������, ���������� ��������������, �������������� � ������"> 
        <LINK href="http://www.bizzona.ru/general.css" type="text/css" rel="stylesheet">
        <meta http-equiv="content-type" content="text/html; charset=windows-1251"/>
  </HEAD>
<?
	$smarty->display("./site/headerbanner.tpl");
	$smarty->assign("headertitle", $resultGetItem->name);
?>  
<table class="w" border="0" cellpadding="0" cellspacing="0" height="100%">
	<tr>
		<td colspan="2">
		<?
			$topmenu = GetMenu();
			$smarty->assign("topmenu", $topmenu);

			$link_country = GetSubMenu();
			$smarty->assign("ad", $link_country);

			$smarty->caching = false;
			$output_header = $smarty->fetch("./site/headeri.tpl");
			echo minimizer($output_header);		
		?>
		</td>
	</tr>
    <tr>
        <td width="30%" valign="top"  height="100%">
        <?
          $smarty->cache_dir = "./persistentcache"; 
          $smarty->cache_lifetime = 10800;
                  
          if (isset($_GET["action"]) && $_GET["action"]  == "category") {
            $_GET["ID"] = (isset($_GET["ID"])  ? intval($_GET["ID"]) : 0);
            $smarty->caching = true; 
            if (!$smarty->is_cached('./site/categorysi.tpl', $_GET["ID"])) {
              $data = array();
              $data["offset"] = 0;
              $data["rowCount"] = 0;
              $data["sort"] = "CountI";
              $result = $category->Select($data);
              $smarty->assign("data", $result);
            }
  			$output_categorysi = $smarty->fetch("./site/categorysi.tpl", $_GET["ID"]);
  			echo minimizer($output_categorysi);             
            $smarty->caching = false; 
          } else { 
            $smarty->caching = true; 
            if (!$smarty->is_cached('./site/categorysi.tpl')) {
              $data = array();
              $data["offset"] = 0;
              $data["rowCount"] = 0;
              $data["sort"] = "CountI";
              $result = $category->Select($data);
              $smarty->assign("data", $result);
            }
  			$output_categorysi = $smarty->fetch("./site/categorysi.tpl");
  			echo minimizer($output_categorysi);             
            $smarty->caching = false; 
          }

          $smarty->cache_dir = "./cache";
          $smarty->cache_lifetime = 3600;          
          
          $smarty->display("./site/call.tpl");
        ?>
		<div style="padding-top:2pt;"></div>
		<?            
			$output_proposal = $smarty->fetch("./site/proposal.tpl");
			echo minimizer($output_proposal);
        	
			$output_request = $smarty->fetch("./site/request.tpl");
			echo minimizer($output_request);            
        ?>    
        </td>
        <td width="70%" valign="top">
			<div style="width:auto;height:100%;" >

       			<?
       				echo UpPositionAndShare(intval($_GET["ID"]));
				?>
			
			
       			<?
          			$smarty->cache_dir = "./persistentcache"; 
          			$smarty->cache_lifetime = 10800;
           			if (!$smarty->is_cached('./site/cityi.tpl')) {
               			$data = array();
               			$data["offset"] = 0;
               			$data["rowCount"] = 0;
               			$data["sort"] = "CountI";
               			$result = $city->SelectActiveI($data);
               			$smarty->assign("data", $result);
           			} 
           			
                	$output_cityi = $smarty->fetch("./site/cityi.tpl");
					echo minimizer($output_cityi);
           			
           			$smarty->caching = false; 
           			$smarty->cache_dir = "./cache";
          			$smarty->cache_lifetime = 3600;
       			?>

   				<?
   					echo InvestmentHeaderShow($resultGetItem->id, $resultGetItem->datecreate);
				?>
       			
               <?
                 if (isset($resultGetItem->id)) {
                     $smarty->caching = true; 
                     $smarty->cache_dir = "./investmentscache";
                     $smarty->cache_lifetime = 86400*30;                     

                     if (!$smarty->is_cached("./site/detailsInvestments.tpl", $resultGetItem->id)) {
                     	$smarty->assign("data", $resultGetItem);
                     }

                     $smarty->display("./site/detailsInvestments.tpl", $resultGetItem->id);
                     
                     $smarty->cache_dir = "./cache";
                     $smarty->caching = false;  

                     if(!isset($_GET["nocount"])) {
                     	$investments->IncrementQView();
                     }
                 }
               ?>
				<br>
				<?
   					echo InvestmentHeaderShow($resultGetItem->id, $resultGetItem->datecreate);
   					echo UpPositionAndShare(intval($_GET["ID"]));
				?>

               
            </div>
            
            
        </td>
    </tr>
    <tr>
    	<td colspan="2">
			<div style="padding-top:4pt;padding-bottom:5pt;padding-right:10pt;padding-left:10pt;font-size:10pt;font-family:arial;">
				<table cellpadding="0" cellspacing="0" border="0"  style="width:100%;" bgcolor="#eeeee0">
    				<tr>
        				<td style="background-image:url(http://<?=$_SERVER["HTTP_HOST"]?>/images/d1.gif); background-repeat:no-repeat;width:5px;height:5px; background-position:left top;">&nbsp;</td>
            			<td rowspan="2" valign="middle" align="center"  style="font-size:8pt; font-family:Arial; color:black;">
            			</td>
            			<td style="background-image:url(http://<?=$_SERVER["HTTP_HOST"]?>/images/d2.gif); background-repeat:no-repeat;width:5px;height:5px;background-position:right top;">&nbsp;</td>
        			</tr>
        			<tr>
            			<td style="background-image:url(http://<?=$_SERVER["HTTP_HOST"]?>/images/d4.gif); background-repeat:no-repeat;width:5px;height:5px; background-position:left bottom;">&nbsp;</td>
            			<td style="background-image:url(http://<?=$_SERVER["HTTP_HOST"]?>/images/d3.gif); background-repeat:no-repeat;width:5px;height:5px; background-position:right bottom;">&nbsp;</td>
        			</tr>
    			</table>
			</div>
			<?
				$smarty->display("./site/footer.tpl");
			?>
    	</td>
    </tr>
</table>
<?
  $end = get_formatted_microtime(); 
  $total = $end - $start;
  echo "<center><span style='font-size:7pt;'>".round($total, 6)." : ".$count_sql_call." : ".$base_memory_usage." : ".memoryUsage(memory_get_usage(), $base_memory_usage)." </span><center>";
?>
</body>
</html>
<?
	$sell->Close();
	$category->Close();
	$city->Close();
	$country->Close();
	$buyer->Close();
	$investments->Close();
	$equipment->Close();
	$district->Close();
	$subcategory->Close();
    $streets->Close();
?>
