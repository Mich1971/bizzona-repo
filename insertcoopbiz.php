<?
    global $DATABASE;

    include_once("./phpset.inc");
    include_once("./engine/functions.inc"); 
    AssignDataBaseSetting();
    include_once("./engine/class.category.inc"); 
    $category = new Category();

    include_once("./engine/class.subcategory.inc"); 
    $subCategory = new SubCategory();

    include_once("./engine/class.coopbiz.inc"); 
    $coopbiz = new CoopBiz();

    include_once("./engine/class.BlockIP.inc"); 
    $blockIp = new BlockIP();

    $blocking  = $blockIp->YouAreBlocking(array(
        'ip' => $_SERVER['REMOTE_ADDR']
    ));  

    include_once("./engine/class.insert_settings.inc"); 
    $insertSettings = new InsertSettings();  

    $insertSettings->type_insert = ConstInsertSettings::COOPBIZ;
    $insertSettingsData = $insertSettings->GetItem();    
    
    require_once("./libs/Smarty.class.php");
    $smarty = new Smarty;
    $smarty->template_dir = "./templates";
    $smarty->compile_dir  = "./templates_c";
    $smarty->compile_check = true;
    $smarty->debugging     = false;

    require_once("./regfuncsmarty.php");

    $data = array();
    $data["offset"] = 0;
    $data["rowCount"] = 0;
    $data["sort"] = "ID";
     
    $listCategory = $category->Select($data);   
    $smarty->assign("listCategory", $listCategory);

    $listSubCategory = $subCategory->Select($data);
    $smarty->assign("listSubCategory", $listSubCategory);

  
  if (isset($_POST) && sizeof($_POST) && !$blocking) {
  	
    $status_error = false;
    $text_error = "";

    $t_name = htmlspecialchars(trim($_POST["name"]), ENT_QUOTES);
    $_POST["name"] = $t_name;
  	
    $t_shortdescription = htmlspecialchars(trim($_POST["shortdescription"]), ENT_QUOTES);
    $_POST["shortdescription"] = $t_shortdescription;
    
    $t_description = htmlspecialchars(trim($_POST["description"]), ENT_QUOTES);
    $_POST["description"] = $t_description;
    
    $t_region_id = "";
    $t_region_text = htmlspecialchars(trim($_POST["region_text"]), ENT_QUOTES);
    $_POST["region_text"] = $t_region_text;
    
    $t_duration_id = htmlspecialchars(trim($_POST["duration_id"]), ENT_QUOTES);
    $_POST["duration_id"] = $t_duration_id;
    
    $t_datecreate = date();
    $t_qview = 0;
    $t_status_id = NEW_COOPBIZ;
    $t_contact_face = htmlspecialchars(trim($_POST["contact_face"]), ENT_QUOTES);
    $_POST["contact_face"] = $t_contact_face;
    
    $t_contact_phone = htmlspecialchars(trim($_POST["contact_phone"]), ENT_QUOTES);
    $_POST["contact_phone"] = $t_contact_phone;
    
    $t_contact_email = htmlspecialchars(trim(strtolower($_POST["contact_email"])), ENT_QUOTES);
    $_POST["contact_email"] = $t_contact_email;
    
    $t_contact_site = htmlspecialchars(trim($_POST["contact_site"]), ENT_QUOTES);
    $_POST["contact_site"] = $t_contact_site;
  	
  	$total_typeBizID = htmlspecialchars(trim($_POST["category"]), ENT_QUOTES);
  	$_POST["category"] = $total_typeBizID;
  	
  	$arr_typeBizID = split("[:]",$total_typeBizID);
  	if(sizeof($arr_typeBizID) == 2)
  	{
  		$t_category_id =intval($arr_typeBizID[0]);
  		$t_subcategory_id = intval($arr_typeBizID[1]);
  	}
  	else 
  	{
  		$t_category_id = 0;
  		$t_subcategory_id = 0;
  	}
  	
  	if (strlen($t_name) <= 0) {
  		
  		$status_error = true;	
  		$text_error = "������ ����� \"�������� �������\"";
  		$smarty->assign("ErrorInput_name", true);
  		
  	} else if(strlen($t_region_text) <= 0)	{
  		
  		$status_error = true;	
  		$text_error = "������ ����� \"������������\"";
  		$smarty->assign("ErrorInput_region_text", true);
  		  		
  	} else if(strlen($t_shortdescription) <= 0) {
  		
  		$status_error = true;	
  		$text_error = "������ ����� \"������� ��������\"";
  		$smarty->assign("ErrorInput_shortdescription", true);
  		  		
  	} else if(strlen($t_description) <= 0) {
  		
  		$status_error = true;	
  		$text_error = "������ ����� \"������ ��������\"";
  		$smarty->assign("ErrorInput_description", true);  		
  		
  	} else if(strlen($t_contact_face) <= 0) {
  		
  		$status_error = true;	
  		$text_error = "������ ����� \"���������� ����\"";
  		$smarty->assign("ErrorInput_contact_face", true);  		  		
  		
  	} else if(strlen($t_contact_phone) <= 0) {
  		
  		$status_error = true;	
  		$text_error = "������ ����� \"��������\"";
  		$smarty->assign("ErrorInput_contact_phone", true);  		
  		
  	} else if (strlen($t_contact_email) <= 0) {
  		
  		$status_error = true;	
  		$text_error = "������ ����� \"Email\"";
  		$smarty->assign("ErrorInput_contact_email", true);
  	}
  	
  	
  	
  	if(!$status_error)
  	{
		
    	$coopbiz->name = $t_name;
    	$coopbiz->shortdescription = $t_shortdescription;
    	$coopbiz->description = $t_description;
    	$coopbiz->category_id = $t_category_id;
    	$coopbiz->subcategory_id = $t_subcategory_id;
    	$coopbiz->region_id = $t_region_id;
    	$coopbiz->region_text = $t_region_text;
    	$coopbiz->duration_id = $t_duration_id;
    	$coopbiz->datecreate = $t_datecreate;
    	$coopbiz->qview = $t_qview;
    	$coopbiz->status_id = $t_status_id;
    	$coopbiz->contact_face = $t_contact_face;
    	$coopbiz->contact_phone = $t_contact_phone;
    	$coopbiz->contact_email = $t_contact_email;
    	$coopbiz->contact_site = $t_contact_site;
    	
		if(isset($_SESSION["partnerid"])) {
			$coopbiz->partnerid = intval($_SESSION["partnerid"]);
		}
    	
    	
		$code = $coopbiz->Insert();
                $_SESSION['timepost'] = time();
		
		$secret_code = "vbhjplfybt";
		$purse = "4146"; // sms:bank id        ������������� ���:�����
		$order_id = intval($code) + 10000000; //  ������������� ��������
		$amount = "3"; // transaction sum  ����� ����������
		$clear_amount  = "0"; // billing algorithm  �������� �������� ���������
		$description  = "������ ���������� ���������� - ���������� ������"; // operation desc  �������� ��������
  		$submit  = "���������"; // submit label    ������� �� ������ submit

		$sign = ref_sign($purse, $order_id, $amount, $clear_amount, $description, $secret_code);

		$m_description = cp1251_to_utf8($description."  (".$order_id.") ");
		$m_description = base64_encode($m_description);		
		
		
       	$smarty->assign("purse", $purse);
       	$smarty->assign("order_id", $order_id);
       	$smarty->assign("rbk_order_id", $order_id - 10000000);
       	$smarty->assign("amount", $amount);
       	$smarty->assign("clear_amount", $clear_amount);
       	$smarty->assign("description", $description);
       	$smarty->assign("m_description", $m_description);
       	$smarty->assign("submit", $submit);
       	$smarty->assign("sign", $sign);

       	$smarty->assign("email", $_POST["contact_email"]);
       	
		unset($_POST);
		
		$text_success = "������ ���� ������� �������� �� ������";
		$smarty->assign("text_success", $text_success);
		
		// up send to email - dublicate
        $to      = 'bizzona.ru@gmail.com';
        $subject = 'NEW COOPBIZ: '.$data["FML"].'';
        
        $message = "�������� �������: ".$coopbiz->name."\n";
        $message.= "��� �������: ".$coopbiz->category_id."\n";
        $message.= "������������: ".$coopbiz->region_text."\n";
        $message.= "������� ��������: ".$coopbiz->shortdescription."\n";
        $message.= "������ ��������: ".$coopbiz->description."\n";
        $message.= "���������� ����: ".$coopbiz->contact_face."\n";
        $message.= "��������: ".$coopbiz->contact_phone."\n";
        $message.= "Email: ".$coopbiz->contact_email."\n";
        $message.= "����: ".$coopbiz->contact_site."\n";
        $message.= "������������ ����������: ".$coopbiz->duration_id."\n";

		$from = "eugenekurilov@gmail.com";        
         
    	$headers = "From: BizZONA.ru  <".$from.">\r\n".               
    				"Reply-To: BizZONA.ru <".$from.">\r\n".
    				"Content-type: text/plain; charset=windows-1251;\r\n".
    				"Return-Path: ".$from."\r\n";
        
        mail($to, $subject, $message, $headers);
		// down send to email - dublicate
		
		
		
  	}
  	else 
  	{
  		$smarty->assign("text_error", $text_error);
  	}
  }
  
    include_once("./engine/class.ContactInfo_Lite.inc");
    $contact = new ContactInfo_Lite();  

    $contact->GetContactInfo();  
    $smarty->assign("contact", $contact);

    $smarty->assign("insertsettings", $insertSettingsData);      
?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>

  <HEAD>
      <title>���������� ������ - ������ ����</title>		
	<META NAME="Description" CONTENT="���������� ������, ������� �������, ������� �������, ������� ������, ������� �������, ������� �������� �������, ������� �������, ������� ��������,  ������� ��������, ������� ��������, ���� ������� ������?">
        <meta name="Keywords" content="���������� ������, ������� �������, ������� �������, ������� ������, ������� �������, ������� �������� �������, ������� �������, ������� ��������,  ������� ��������, ������� ��������, ���� ������� ������?, Ready business, �����������, �������� � ��������, ���������, ����������, �����������"> 
	<meta http-equiv="content-type" content="text/html; charset=windows-1251"/>
        <LINK href="./general.css" type="text/css" rel="stylesheet">
  </HEAD>

<?
    $topmenu = GetMenu();
    $smarty->assign("topmenu", $topmenu);

    $link_country.= "&nbsp;&nbsp;<a href='".NAMESERVER."help.php' class='city_a'  style='color:white;font-size:10pt;color:yellow;' title='������ - ������� �������' target='_blank'>������</a>&nbsp;&nbsp;";	
    $smarty->assign("ad", $link_country);

    $smarty->display("./site/headercoop.tpl");
?>
  

<table class="w" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td width="100%">
       		<?
                    if (isset($code) && $code > 0 && (int)$insertSettingsData['type_show'] == ConstInsertSettings::TYPE_SHOW_PAY) {
                        
                        $pay = $smarty->fetch("./site/confirmpaymentcoop.tpl");
			echo $pay;
                        
		}  else if (isset($code) && $code > 0 && in_array($insertSettingsData['type_show'], array(ConstInsertSettings::TYPE_SHOW_NO_PAY, ConstInsertSettings::TYPE_SHOW_PROMO)) ) {
                  ?>
                    <table width='100%' align='center' bgcolor='#EDEDCC' style='border-top: 1px; border-bottom: 1px; border-left:1px;  border-right:1px;  border-color:#C1C1A4;  border-style: solid;' cellpadding="0" cellspacing="0" >
                        <tr>
                          <td valign='top' style='padding-left:5pt;padding-right:5pt;'>
                            <h2>���� ���������� �� ����������� ������� �������!</h2>
                          </td>
                        </tr>
                    </table>            
                  <? 
                } else {
         	?>
                    <table width='100%' align='center' bgcolor='#EDEDCC' style='border-top: 1px; border-bottom: 1px; border-left:1px;  border-right:1px;  border-color:#C1C1A4;  border-style: solid;' cellpadding="0" cellspacing="0" >
                        <tr>
                             <td  valign='top' style='padding-left:5pt;padding-right:5pt;'>
                                    <div align="center" style="padding-top:5pt;padding-bottom:5pt;font-size:14pt;font-weight:bold;">���������� ���������� �� ���������� ������</div>
                                    <?
                                        echo $insertSettingsData['content'];
                                        
                                        if($insertSettingsData['use_promo']) {
                                            $promo = $smarty->fetch("./site/promo.tpl");
                                            echo $promo;
                                        }                                        
                                    ?>
                             </td>
                             <td width="25%" valign="top" style="padding-top:5pt;padding-right:5pt;">
                                <?
                                   $out_req = $smarty->fetch("./site/requestmin.tpl");
                                   echo $out_req;
                                ?>
                             </td>
                         </tr>
                    </table>

                     <table width='100%' height='100%'>
                       <?
                          if (strlen($error) > 0) { 
                       ?> 
                       <tr  valign='top' width='100%' height='100%'>
                         <td align='center'><span style="color:red;"><b><?=$error;?></b></span></td> 
                       </tr>
                       <?
                          }
                       ?> 
                       <tr>
                         <td valign='top' width='60%' height='100%'>
                           <?
                              if($blocking) {
                                $smarty->display("./site/blockip.tpl");  
                              } else { 
                                $smarty->display("./site/insertcoopbiz.tpl");
                              }
                           ?>
                         </td>
                       </tr>
                     </table>
            
            
         	<?
         }
       ?>
        </td>
    </tr>
</table>

<style>
 td a 
 {
   color:black;
 }
</style>


<div style="padding-top:4pt;padding-bottom:5pt;padding-right:10pt;padding-left:10pt;font-size:10pt;font-family:arial;">
	<table cellpadding="0" cellspacing="0" border="0"  style="width:100%;padding-bottom:2pt;" bgcolor="#eeeee0">
    	<tr>
        	<td style="background-image:url(<?=$_SERVER["host_name"]?>images/d1.gif); background-repeat:no-repeat;width:5px;height:5px; background-position:left top;">&nbsp;</td>
            <td rowspan="2" valign="middle" align="center"  style="font-size:8pt; font-family:Arial; color:black;">
            </td>
            <td style="background-image:url(<?=$_SERVER["host_name"]?>images/d2.gif); background-repeat:no-repeat;width:5px;height:5px;background-position:right top;">&nbsp;</td>
        </tr>
        <tr>
            <td style="background-image:url(<?=$_SERVER["host_name"]?>images/d4.gif); background-repeat:no-repeat;width:5px;height:5px; background-position:left bottom;">&nbsp;</td>
            <td style="background-image:url(<?=$_SERVER["host_name"]?>images/d3.gif); background-repeat:no-repeat;width:5px;height:5px; background-position:right bottom;">&nbsp;</td>
        </tr>
    </table>
</div>


<?
	$smarty->display("./site/footer.tpl");
?>


</body>
</html>


