<?php
/*-------------------------------------------------------
*
*   LiveStreet Engine Social Networking
*   Copyright © 2008 Mzhelskiy Maxim
*
*--------------------------------------------------------
*
*   Official site: www.livestreet.ru
*   Contact e-mail: rus.engine@gmail.com
*
*   GNU General Public License, version 2:
*   http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
*
---------------------------------------------------------
*/
/**
 * Настройки для локального сервера.
 * Для использования - переименовать файл в config.local.php
 */

/**
 * Настройка базы данных
 */
$config['db']['params']['host'] = 'localhost';
$config['db']['params']['port'] = '3306';
$config['db']['params']['user'] = 'root';
$config['db']['params']['pass'] = 'KUgj346h';
$config['db']['params']['type']   = 'mysql';
$config['db']['params']['dbname'] = 'social';
$config['db']['table']['prefix'] = 'prefix_';

$config['path']['root']['web'] = 'http://www.bizzona.ru/seminars';
$config['path']['root']['server'] = '/var/www/mastermind/data/www/bizzona.ru/seminars';
$config['path']['offset_request_url'] = '1';
$config['db']['tables']['engine'] = 'MyISAM';
$config['view']['name'] = 'Семинары и тренинги - ООО "Бизнес-ЗОНА"';
$config['view']['description'] = 'Семинары, тренинги,  ООО "Бизнес-ЗОНА"';
$config['view']['keywords'] = 'Семинары, тренинги,  ООО "Бизнес-ЗОНА"';
$config['view']['skin'] = 'new';
$config['sys']['mail']['from_email'] = 'bizzona.ru@gmail.com';
$config['sys']['mail']['from_name'] = 'Почтовик Бизнес-ЗОНА';
$config['general']['close'] = false;
$config['general']['reg']['activation'] = true;
$config['general']['reg']['invite'] = false;
$config['lang']['current'] = 'russian';
$config['lang']['default'] = 'russian';
return $config;
?>
