<?php

set_include_path(get_include_path().PATH_SEPARATOR.dirname(dirname(dirname(__FILE__))));
$sDirRoot=dirname(dirname(dirname(__FILE__)));
require_once($sDirRoot."/config/config.ajax.php");

$iType=getRequest('type',null,'post');
$idQa=getRequest('idQa',null,'post');
$bStateError=true;
$sMsg='';
$sMsgTitle='';
$bState=false;
if ($oEngine->User_IsAuthorization()) {
	if (in_array($iType,array('1','0'))) {
		if ($oQa=$oEngine->Qa_GetQaById($idQa)) {
			$oUserCurrent=$oEngine->User_GetUserCurrent();
			$oFavouriteQa=$oEngine->Qa_GetFavouriteQa($oQa->getId(),$oUserCurrent->getId());
			if (!$oFavouriteQa and $iType) {
				$oFavouriteQaNew=Engine::GetEntity('Favourite',
					array(
						'target_id'      => $oQa->getId(),
						'user_id'        => $oUserCurrent->getId(),
						'target_type'    => 'qa',
						'target_publish' => $oQa->getPublish()
					)
				);
				if ($oEngine->Qa_AddFavouriteQa($oFavouriteQaNew)) {
					$bStateError=false;
					$sMsgTitle=$oEngine->Lang_Get('attention');
					$sMsg=$oEngine->Lang_Get('topic_favourite_add_ok');
					$bState=true;
				} else {
					$sMsgTitle=$oEngine->Lang_Get('error');
					$sMsg=$oEngine->Lang_Get('system_error');
				}
			}
			if (!$oFavouriteQa and !$iType) {
				$sMsgTitle=$oEngine->Lang_Get('error');
				$sMsg=$oEngine->Lang_Get('topic_favourite_add_no');
			}
			if ($oFavouriteQa and $iType) {
				$sMsgTitle=$oEngine->Lang_Get('error');
				$sMsg=$oEngine->Lang_Get('topic_favourite_add_already');
			}
			if ($oFavouriteQa and !$iType) {
				if ($oEngine->Qa_DeleteFavouriteQa($oFavouriteQa)) {
					$bStateError=false;
					$sMsgTitle=$oEngine->Lang_Get('attention');
					$sMsg=$oEngine->Lang_Get('topic_favourite_del_ok');
					$bState=false;
				} else {
					$sMsgTitle=$oEngine->Lang_Get('error');
					$sMsg=$oEngine->Lang_Get('system_error');
				}
			}
		} else {
			$sMsgTitle=$oEngine->Lang_Get('error');
			$sMsg=$oEngine->Lang_Get('system_error');
		}
	} else {
		$sMsgTitle=$oEngine->Lang_Get('error');
		$sMsg=$oEngine->Lang_Get('system_error');
	}
} else {
	$sMsgTitle=$oEngine->Lang_Get('error');
	$sMsg=$oEngine->Lang_Get('need_authorization');
}


$GLOBALS['_RESULT'] = array(
"bStateError"     => $bStateError,
"bState"   => $bState,
"sMsgTitle"   => $sMsgTitle,
"sMsg"   => $sMsg,
);

?>