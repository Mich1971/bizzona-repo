<?php
set_include_path(get_include_path().PATH_SEPARATOR.dirname(dirname(dirname(__FILE__))));
$sDirRoot=dirname(dirname(dirname(__FILE__)));
require_once($sDirRoot."/config/config.ajax.php");

$idCommentLast=getRequest('idCommentLast',null,'post');
$idBb=getRequest('idTarget',null,'post');
$bStateError=true;
$sMsg='';
$sMsgTitle='';
$iMaxIdComment=0;
$aComments=array();

if ($oEngine->User_IsAuthorization()) {
	$oUserCurrent=$oEngine->User_GetUserCurrent();
	
	if ($oBb=$oEngine->Bb_GetBbById($idBb)) {		
		
		$aReturn=$oEngine->Comment_GetCommentsNewByTargetId($oBb->getId(),'bb',$idCommentLast);

		$iMaxIdComment=$aReturn['iMaxIdComment'];
		
		/*
		$oBbRead=Engine::GetEntity('Bb_BbRead');
		$oBbRead->setBbId($oBb->getId());
		$oBbRead->setUserId($oUserCurrent->getId());
		$oBbRead->setCommentCountLast($oBb->getCountComment());
		$oBbRead->setCommentIdLast($iMaxIdComment);
		$oBbRead->setDateRead(date("Y-m-d H:i:s"));
		$oEngine->Bb_SetBbRead($oBbRead);		
		*/
		
		$aCmts=$aReturn['comments'];
		
		if ($aCmts and is_array($aCmts)) {
			foreach ($aCmts as $aCmt) {
				
				$aComments[]=array( 
					'html' => $aCmt['html'],
					'idParent' => $aCmt['obj']->getPid(),
					'id' => $aCmt['obj']->getId(),
				);
			}
		}
		
		$bStateError=false;		
		
	} else {
		$sMsgTitle=$oEngine->Lang_Get('error');
		$sMsg=$oEngine->Lang_Get('system_error');
	}

	
} else {
	$sMsgTitle=$oEngine->Lang_Get('error');
	$sMsg=$oEngine->Lang_Get('need_authorization');
}

$GLOBALS['_RESULT'] = array(
"bStateError"     => $bStateError,
"sMsgTitle"   => $sMsgTitle,
"sMsg"   => $sMsg,
"aComments" => $aComments,
"iMaxIdComment" => $iMaxIdComment
);
?>