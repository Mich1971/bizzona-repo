<?php
set_include_path(get_include_path().PATH_SEPARATOR.dirname(dirname(dirname(__FILE__))));
$sDirRoot=dirname(dirname(dirname(__FILE__)));
require_once($sDirRoot."/config/config.ajax.php");

$idCommentLast=getRequest('idCommentLast',null,'post');
$idBi=getRequest('idTarget',null,'post');
$bStateError=true;
$sMsg='';
$sMsgTitle='';
$iMaxIdComment=0;
$aComments=array();

if ($oEngine->User_IsAuthorization()) {
	$oUserCurrent=$oEngine->User_GetUserCurrent();
	
	
	
	if ($oBi=$oEngine->Bi_GetBiById($idBi)) {		
		
		//mail("eugenekurilov@gmail.com", "find", "find");
		
		$aReturn=$oEngine->Comment_GetCommentsNewByTargetId($oBi->getId(),'bi',$idCommentLast);
		
		//mail("eugenekurilov@gmail.com", "find1", "find1");
		
		$iMaxIdComment=$aReturn['iMaxIdComment'];
		
		$oBiRead=Engine::GetEntity('Bi_BiRead');
		$oBiRead->setBiId($oBi->getId());
		$oBiRead->setUserId($oUserCurrent->getId());
		$oBiRead->setCommentCountLast($oBi->getCountComment());
		$oBiRead->setCommentIdLast($iMaxIdComment);
		$oBiRead->setDateRead(date("Y-m-d H:i:s"));
		$oEngine->Bi_SetBiRead($oBiRead);		
		
		$aCmts=$aReturn['comments'];
		
		//mail("eugenekurilov@gmail.com", "find3", "find3");
		
		if ($aCmts and is_array($aCmts)) {
			foreach ($aCmts as $aCmt) {
				
				$aComments[]=array(
					'html' => $aCmt['html'],
					'idParent' => $aCmt['obj']->getPid(),
					'id' => $aCmt['obj']->getId(),
				);
			}
		}
		
		$bStateError=false;		
		
	} else {
		$sMsgTitle=$oEngine->Lang_Get('error');
		$sMsg=$oEngine->Lang_Get('system_error');
	}

	
} else {
	$sMsgTitle=$oEngine->Lang_Get('error');
	$sMsg=$oEngine->Lang_Get('need_authorization');
}

//$bStateError=false;

$GLOBALS['_RESULT'] = array(
"bStateError"     => $bStateError,
"sMsgTitle"   => $sMsgTitle,
"sMsg"   => $sMsg,
"aComments" => $aComments,
"iMaxIdComment" => $iMaxIdComment
);

?>