<?php

set_include_path(get_include_path().PATH_SEPARATOR.dirname(dirname(dirname(__FILE__))));
$sDirRoot=dirname(dirname(dirname(__FILE__)));
require_once($sDirRoot."/config/config.ajax.php");

$idCommentLast=getRequest('idCommentLast',null,'post');
$idQa=getRequest('idTarget',null,'post');
$bStateError=true;
$sMsg='';
$sMsgTitle='';
$iMaxIdComment=0;
$aComments=array();

if ($oEngine->User_IsAuthorization()) {
	$oUserCurrent=$oEngine->User_GetUserCurrent();
	
	if ($oQa=$oEngine->Qa_GetQaById($idQa)) {		
		
		$aReturn=$oEngine->Comment_GetCommentsNewByTargetId($oQa->getId(),'qa',$idCommentLast);
		$iMaxIdComment=$aReturn['iMaxIdComment'];
		
		$oQaRead=Engine::GetEntity('Qa_QaRead');
		$oQaRead->setQaId($oQa->getId());
		$oQaRead->setUserId($oUserCurrent->getId());
		$oQaRead->setCommentCountLast($oQa->getCountComment());
		$oQaRead->setCommentIdLast($iMaxIdComment);
		$oQaRead->setDateRead(date("Y-m-d H:i:s"));
		$oEngine->Qa_SetQaRead($oQaRead);		
		
		$aCmts=$aReturn['comments'];
		
		if ($aCmts and is_array($aCmts)) {
			foreach ($aCmts as $aCmt) {
				
				$aComments[]=array(
					'html' => $aCmt['html'],
					'idParent' => $aCmt['obj']->getPid(),
					'id' => $aCmt['obj']->getId(),
				);
			}
		}
	
		$bStateError=false;		
		
	} else {
		$sMsgTitle=$oEngine->Lang_Get('error');
		$sMsg=$oEngine->Lang_Get('system_error');
	}

	
} else {
	$sMsgTitle=$oEngine->Lang_Get('error');
	$sMsg=$oEngine->Lang_Get('need_authorization');
}

//$bStateError=false;

$GLOBALS['_RESULT'] = array(
"bStateError"     => $bStateError,
"sMsgTitle"   => $sMsgTitle,
"sMsg"   => $sMsg,
"aComments" => $aComments,
"iMaxIdComment" => $iMaxIdComment
);

?>