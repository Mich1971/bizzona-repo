<?php

set_include_path(get_include_path().PATH_SEPARATOR.dirname(dirname(dirname(__FILE__))));
$sDirRoot=dirname(dirname(dirname(__FILE__)));
require_once($sDirRoot."/config/config.ajax.php");

$idCommentLast=getRequest('idCommentLast',null,'post');
$idBs=getRequest('idTarget',null,'post');
$bStateError=true;
$sMsg='';
$sMsgTitle='';
$iMaxIdComment=0;
$aComments=array();

if ($oEngine->User_IsAuthorization()) {
	$oUserCurrent=$oEngine->User_GetUserCurrent();
	
	if ($oBs=$oEngine->Bs_GetBsById($idBs)) {		
		
		$aReturn=$oEngine->Comment_GetCommentsNewByTargetId($oBs->getId(),'bs',$idCommentLast);
		$iMaxIdComment=$aReturn['iMaxIdComment'];
		
		$aCmts=$aReturn['comments'];
		
		if ($aCmts and is_array($aCmts)) {
			foreach ($aCmts as $aCmt) {
				
				$aComments[]=array(
					'html' => $aCmt['html'],
					'idParent' => $aCmt['obj']->getPid(),
					'id' => $aCmt['obj']->getId(),
				);
			}
		}
	
		$bStateError=false;		
		
	} else {
		$sMsgTitle=$oEngine->Lang_Get('error');
		$sMsg=$oEngine->Lang_Get('system_error');
	}

	
} else {
	$sMsgTitle=$oEngine->Lang_Get('error');
	$sMsg=$oEngine->Lang_Get('need_authorization');
}

//$bStateError=false;

$GLOBALS['_RESULT'] = array(
"bStateError"     => $bStateError,
"sMsgTitle"   => $sMsgTitle,
"sMsg"   => $sMsg,
"aComments" => $aComments,
"iMaxIdComment" => $iMaxIdComment
);

?>