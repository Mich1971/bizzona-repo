<?php

set_include_path(get_include_path().PATH_SEPARATOR.dirname(dirname(dirname(__FILE__))));
$sDirRoot=dirname(dirname(dirname(__FILE__)));
require_once($sDirRoot."/config/config.ajax.php");

$iValue=getRequest('value',null,'post');
$bStateError=true;
$sMsg='';
$sMsgTitle=''; 
$iRating=0;
if ($oEngine->User_IsAuthorization()) {
	if ($oBi=$oEngine->Bi_GetBiById(getRequest('idBi',null,'post'))) {
		
		$oUserCurrent=$oEngine->User_GetUserCurrent();
		if ($oBi->getUserId()!=$oUserCurrent->getId()) {
			
			if (!($oBiVote=$oEngine->Vote_GetVote($oBi->getId(),'bi',$oUserCurrent->getId()))) {
				
				if (strtotime($oBi->getDateAdd())>time()-Config::Get('acl.vote.topic.limit_time')) {
					
					if ($oEngine->ACL_CanVoteBi($oUserCurrent,$oBi) or $iValue==0) {
						
						if (in_array($iValue,array('1','-1','0'))) {
							
							$oBiVote=Engine::GetEntity('Vote');
							$oBiVote->setTargetId($oBi->getId());
							$oBiVote->setTargetType('bi');
							$oBiVote->setVoterId($oUserCurrent->getId());
							$oBiVote->setDirection($iValue);
							$oBiVote->setDate(date("Y-m-d H:i:s"));
							
							$iVal=0;
							if ($iValue!=0) {
								$iVal=(float)$oEngine->Rating_VoteBi($oUserCurrent,$oBi,$iValue);
							}

							$oBiVote->setValue($iVal);
							$oBi->setCountVote($oBi->getCountVote()+1);
							
							if ($oEngine->Vote_AddVote($oBiVote) and $oEngine->Bi_UpdateBi($oBi)) {
								
								$bStateError=false;
								$sMsgTitle=$oEngine->Lang_Get('attention');
								$sMsg = $iValue==0 ? $oEngine->Lang_Get('topic_vote_ok_abstain') : $oEngine->Lang_Get('topic_vote_ok');
								$iRating=$oBi->getRating();
								
							} else {
								$sMsgTitle=$oEngine->Lang_Get('error');
								$sMsg=$oEngine->Lang_Get('system_error');
							}
						} else {
							$sMsgTitle=$oEngine->Lang_Get('attention');
							$sMsg=$oEngine->Lang_Get('system_error');
						}
					} else {
						$sMsgTitle=$oEngine->Lang_Get('attention');
						$sMsg=$oEngine->Lang_Get('topic_vote_error_acl');
					}
				} else {
					$sMsgTitle=$oEngine->Lang_Get('attention');
					$sMsg=$oEngine->Lang_Get('topic_vote_error_time');
				}
			} else {
				$sMsgTitle=$oEngine->Lang_Get('attention');
				$sMsg=$oEngine->Lang_Get('bi_vote_error_already');
			}
		} else {
			$sMsgTitle=$oEngine->Lang_Get('attention');
			$sMsg=$oEngine->Lang_Get('qa_vote_error_self');   
		}
	} else {
		$sMsgTitle=$oEngine->Lang_Get('error');
		$sMsg=$oEngine->Lang_Get('system_error');
	}
} else {
	$sMsgTitle=$oEngine->Lang_Get('error');
	$sMsg=$oEngine->Lang_Get('need_authorization');
}


$GLOBALS['_RESULT'] = array(
"bStateError"     => $bStateError,
"iRating"   => $iRating,
"sMsgTitle"   => $sMsgTitle,
"sMsg"   => $sMsg,
);

?>