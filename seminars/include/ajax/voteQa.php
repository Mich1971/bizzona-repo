<?php

set_include_path(get_include_path().PATH_SEPARATOR.dirname(dirname(dirname(__FILE__))));
$sDirRoot=dirname(dirname(dirname(__FILE__)));
require_once($sDirRoot."/config/config.ajax.php");

//mail("eugenekurilov@gmail.com", "test", "test");

$iValue=getRequest('value',null,'post');
$bStateError=true;
$sMsg='';
$sMsgTitle=''; 
$iRating=0;
if ($oEngine->User_IsAuthorization()) {
	if ($oQa=$oEngine->Qa_GetQaById(getRequest('idQa',null,'post'))) {
		
		$oUserCurrent=$oEngine->User_GetUserCurrent();
		if ($oQa->getUserId()!=$oUserCurrent->getId()) {
			
			if (!($oQaVote=$oEngine->Vote_GetVote($oQa->getId(),'qa',$oUserCurrent->getId()))) {
				
				if (strtotime($oQa->getDateAdd())>time()-Config::Get('acl.vote.topic.limit_time')) {
					
					//mail("eugenekurilov@gmail.com", "test4", "test4");
					
					if ($oEngine->ACL_CanVoteQa($oUserCurrent,$oQa) or $iValue==0) {
						
						//mail("eugenekurilov@gmail.com", "test5", "test5");
						
						if (in_array($iValue,array('1','-1','0'))) {
							
							//mail("eugenekurilov@gmail.com", "test6", "test6");
							
							$oQaVote=Engine::GetEntity('Vote');
							$oQaVote->setTargetId($oQa->getId());
							$oQaVote->setTargetType('qa');
							$oQaVote->setVoterId($oUserCurrent->getId());
							$oQaVote->setDirection($iValue);
							$oQaVote->setDate(date("Y-m-d H:i:s"));
							$iVal=0;
							if ($iValue!=0) {
								$iVal=(float)$oEngine->Rating_VoteQa($oUserCurrent,$oQa,$iValue);
							}
							//mail("eugenekurilov@gmail.com", "test60", "test60");
							$oQaVote->setValue($iVal);
							$oQa->setCountVote($oQa->getCountVote()+1);
							if ($oEngine->Vote_AddVote($oQaVote) and $oEngine->Qa_UpdateQa($oQa)) {
								
								//mail("eugenekurilov@gmail.com", "test7", "test7");
								
								$bStateError=false;
								$sMsgTitle=$oEngine->Lang_Get('attention');
								$sMsg = $iValue==0 ? $oEngine->Lang_Get('topic_vote_ok_abstain') : $oEngine->Lang_Get('topic_vote_ok');
								$iRating=$oQa->getRating();
							} else {
								$sMsgTitle=$oEngine->Lang_Get('error');
								$sMsg=$oEngine->Lang_Get('system_error');
							}
						} else {
							$sMsgTitle=$oEngine->Lang_Get('attention');
							$sMsg=$oEngine->Lang_Get('system_error');
						}
					} else {
						$sMsgTitle=$oEngine->Lang_Get('attention');
						$sMsg=$oEngine->Lang_Get('topic_vote_error_acl');
					}
				} else {
					$sMsgTitle=$oEngine->Lang_Get('attention');
					$sMsg=$oEngine->Lang_Get('topic_vote_error_time');
				}
			} else {
				$sMsgTitle=$oEngine->Lang_Get('attention');
				$sMsg=$oEngine->Lang_Get('qa_vote_error_already');
			}
		} else {
			$sMsgTitle=$oEngine->Lang_Get('attention');
			$sMsg=$oEngine->Lang_Get('qa_vote_error_self');   
		}
	} else {
		$sMsgTitle=$oEngine->Lang_Get('error');
		$sMsg=$oEngine->Lang_Get('system_error');
	}
} else {
	$sMsgTitle=$oEngine->Lang_Get('error');
	$sMsg=$oEngine->Lang_Get('need_authorization');
}


$GLOBALS['_RESULT'] = array(
"bStateError"     => $bStateError,
"iRating"   => $iRating,
"sMsgTitle"   => $sMsgTitle,
"sMsg"   => $sMsg,
);

?>