<?php

class ActionCompany extends Action {

	protected $sMenuHeadItemSelect='company';

	protected $sMenuItemSelect='company';

	protected $sMenuSubItemSelect='good';

	protected $sMenuSubCompanyUrl;

	protected $oUserCurrent=null;
	
	protected $iCountSeminarsCollectiveNew=0;

	protected $iCountSeminarsPersonalNew=0;

	protected $iCountSeminarsCompanyNew=0;

	protected $iCountSeminarsNew=0;

	protected $aBadCompanyUrl=array('new','good','bad','edit','add','admin','delete','invite','ajaxaddcomment','ajaxaddcompanyinvite');
	
	public function Init() {
		$this->SetDefaultEvent('good');
		$this->sMenuSubCompanyUrl=Router::GetPath('company');
		$this->oUserCurrent=$this->User_GetUserCurrent();	
			
		$this->iCountSeminarsCollectiveNew=$this->Seminar_GetCountSeminarsCollectiveNew();
		$this->iCountSeminarsPersonalNew=$this->Seminar_GetCountSeminarsPersonalNew();	
		$this->iCountSeminarsCompanyNew=$this->iCountSeminarsCollectiveNew;
		$this->iCountSeminarsNew=$this->iCountSeminarsCollectiveNew+$this->iCountSeminarsPersonalNew;
		
		Router::SetIsShowStats(false);
	}
	
	protected function RegisterEvent() {			
		$this->AddEventPreg('/^good$/i','/^(page(\d+))?$/i','EventSeminars');
		$this->AddEvent('good','EventSeminars');
		$this->AddEventPreg('/^bad$/i','/^(page(\d+))?$/i','EventSeminars');
		$this->AddEventPreg('/^new$/i','/^(page(\d+))?$/i','EventSeminars');
		
		$this->AddEvent('add','EventAddCompany');
		$this->AddEvent('edit','EventEditCompany');
		$this->AddEvent('delete','EventDeleteCompany');
		$this->AddEvent('admin','EventAdminCompany');
		$this->AddEvent('invite','EventInviteCompany');
		
		$this->AddEvent('ajaxaddcomment','AjaxAddComment');
		$this->AddEvent('ajaxaddcompanyinvite', 'AjaxAddCompanyInvite');
		$this->AddEvent('ajaxrecompanyinvite', 'AjaxReCompanyInvite');
		
		$this->AddEventPreg('/^(\d+)\.html$/i','/^$/i','EventShowSeminar');
		$this->AddEventPreg('/^[\w\-\_]+$/i','/^(\d+)\.html$/i','EventShowSeminar');
				
		$this->AddEventPreg('/^[\w\-\_]+$/i','/^(page(\d+))?$/i','EventShowCompany');
		$this->AddEventPreg('/^[\w\-\_]+$/i','/^bad$/i','/^(page(\d+))?$/i','EventShowCompany');
		$this->AddEventPreg('/^[\w\-\_]+$/i','/^new$/i','/^(page(\d+))?$/i','EventShowCompany');			
	}
		
	protected function EventAddCompany() {
		$this->Viewer_AddHtmlTitle($this->Lang_Get('company_create'));
		$this->sMenuSubItemSelect='add';
		$this->sMenuItemSelect='add_company';				
		if (!$this->User_IsAuthorization()) {
			$this->Message_AddErrorSingle($this->Lang_Get('not_access'),$this->Lang_Get('error'));
			return Router::Action('error');
		}		
		if (!$this->ACL_CanCreateCompany($this->oUserCurrent) and !$this->oUserCurrent->isAdministrator()) {
			$this->Message_AddErrorSingle($this->Lang_Get('company_create_acl'),$this->Lang_Get('error'));
			return Router::Action('error');
		}		
		if (!$this->checkCompanyFields()) {
			return false;	
		}	
		$oCompany=Engine::GetEntity('Company');
		$oCompany->setOwnerId($this->oUserCurrent->getId());
		$oCompany->setTitle(getRequest('company_title'));

		$sText=$this->Text_Parser(getRequest('company_description'));
		$oCompany->setDescription($sText);
		$oCompany->setType(getRequest('company_type'));
		$oCompany->setDateAdd(date("Y-m-d H:i:s"));
		$oCompany->setLimitRatingSeminar(getRequest('company_limit_rating_seminar'));
		$oCompany->setUrl(getRequest('company_url'));
		$oCompany->setAvatar(null);

		if (isset($_FILES['avatar']) and is_uploaded_file($_FILES['avatar']['tmp_name'])) {
			if ($sPath=$this->Company_UploadCompanyAvatar($_FILES['avatar'],$oCompany)) {
				$oCompany->setAvatar($sPath);
			} else {
				$this->Message_AddError($this->Lang_Get('company_create_avatar_error'),$this->Lang_Get('error'));
				return false;
			}
		}

		$this->Hook_Run('company_add_before', array('oCompany'=>$oCompany));
		if ($this->Company_AddCompany($oCompany)) {
			$this->Hook_Run('company_add_after', array('oCompany'=>$oCompany));
			$oCompany->Company_GetCompanyById($oCompany->getId());
			Router::Location($oCompany->getUrlFull());
		} else {
			$this->Message_AddError($this->Lang_Get('system_error'),$this->Lang_Get('error'));
		}
	}
	
	protected function EventEditCompany() {

		$this->sMenuSubItemSelect='';
		$this->sMenuItemSelect='profile';
		
		$sCompanyId=$this->GetParam(0);
		if (!$oCompany=$this->Company_GetCompanyById($sCompanyId)) {
			return parent::EventNotFound();
		}

		if ($oCompany->getType()=='personal') {
			return parent::EventNotFound();
		}

		if (!$this->User_IsAuthorization()) {
			$this->Message_AddErrorSingle($this->Lang_Get('not_access'),$this->Lang_Get('error'));
			return Router::Action('error');
		}

		$oCompanyUser=$this->Company_GetCompanyUserByCompanyIdAndUserId($oCompany->getId(),$this->oUserCurrent->getId());		
		$bIsAdministratorCompany=$oCompanyUser ? $oCompanyUser->getIsAdministrator() : false;
		if ($oCompany->getOwnerId()!=$this->oUserCurrent->getId()  and !$this->oUserCurrent->isAdministrator() and !$bIsAdministratorCompany) {
			return parent::EventNotFound();
		}			
		
		$this->Viewer_AddHtmlTitle($oCompany->getTitle());
		$this->Viewer_AddHtmlTitle($this->Lang_Get('company_edit'));
		
		$this->Viewer_Assign('oCompanyEdit',$oCompany);

		$this->SetTemplateAction('add');

		if (isPost('submit_company_add')) {

			if (!$this->checkCompanyFields($oCompany)) {
				return false;
			}
			$oCompany->setTitle(getRequest('company_title'));

			$sText=$this->Text_Parser(getRequest('company_description'));			
			$oCompany->setDescription($sText);
			$oCompany->setType(getRequest('company_type'));
			$oCompany->setLimitRatingSeminar(getRequest('company_limit_rating_seminar'));

			if (isset($_FILES['avatar']) and is_uploaded_file($_FILES['avatar']['tmp_name'])) {
				if ($sPath=$this->Company_UploadCompanyAvatar($_FILES['avatar'],$oCompany)) {
					$oCompany->setAvatar($sPath);
				} else {
					$this->Message_AddError($this->Lang_Get('company_create_avatar_error'),$this->Lang_Get('error'));
					return false;
				}
			}

			if (isset($_REQUEST['avatar_delete'])) {
				$this->Company_DeleteCompanyAvatar($oCompany);
				$oCompany->setAvatar(null);
			}

			$this->Hook_Run('company_edit_before', array('oCompany'=>$oCompany));
			if ($this->Company_UpdateCompany($oCompany)) {
				$this->Hook_Run('company_edit_after', array('oCompany'=>$oCompany));
				Router::Location($oCompany->getUrlFull());
			} else {
				$this->Message_AddErrorSingle($this->Lang_Get('system_error'),$this->Lang_Get('error'));
				return Router::Action('error');
			}
		} else {

			$_REQUEST['company_title']=$oCompany->getTitle();
			$_REQUEST['company_url']=$oCompany->getUrl();
			$_REQUEST['company_type']=$oCompany->getType();
			$_REQUEST['company_description']=$oCompany->getDescription();
			$_REQUEST['company_limit_rating_seminar']=$oCompany->getLimitRatingSeminar();
			$_REQUEST['company_id']=$oCompany->getId();
		}
		
		
	}

	protected function EventAdminCompany() {		
		$this->sMenuItemSelect='admin';
		$this->sMenuSubItemSelect='';

		$sCompanyId=$this->GetParam(0);
		if (!$oCompany=$this->Company_GetCompanyById($sCompanyId)) {
			return parent::EventNotFound();
		}
		if (!$this->User_IsAuthorization()) {
			$this->Message_AddErrorSingle($this->Lang_Get('not_access'),$this->Lang_Get('error'));
			return Router::Action('error');
		}

		$oCompanyUser=$this->Company_GetCompanyUserByCompanyIdAndUserId($oCompany->getId(),$this->oUserCurrent->getId());		
		$bIsAdministratorCompany=$oCompanyUser ? $oCompanyUser->getIsAdministrator() : false;
		if ($oCompany->getOwnerId()!=$this->oUserCurrent->getId()  and !$this->oUserCurrent->isAdministrator() and !$bIsAdministratorCompany) {
			return parent::EventNotFound();
		}					

		if (isPost('submit_company_admin')) {
			$this->Security_ValidateSendForm();
			
			$aUserRank=getRequest('user_rank',array());
			if (!is_array($aUserRank)) {
				$aUserRank=array();
			}
			foreach ($aUserRank as $sUserId => $sRank) {
				if (!($oCompanyUser=$this->Company_GetCompanyUserByCompanyIdAndUserId($oCompany->getId(),$sUserId))) {
					$this->Message_AddError($this->Lang_Get('system_error'),$this->Lang_Get('error'));
					break;
				}
				if (in_array($sRank,array('administrator','moderator','reader')) and $oCompanyUser->getUserRole()==ModuleCompany::COMPANY_USER_ROLE_BAN) {					
					$oCompany->setCountUser($oCompany->getCountUser()+1);					
				}
				
				switch ($sRank) {
					case 'administrator':
						$oCompanyUser->setUserRole(ModuleCompany::COMPANY_USER_ROLE_ADMINISTRATOR);
						break;
					case 'moderator':
						$oCompanyUser->setUserRole(ModuleCompany::COMPANY_USER_ROLE_MODERATOR);
						break;
					case 'reader':
						$oCompanyUser->setUserRole(ModuleCompany::COMPANY_USER_ROLE_USER);
						break;
					case 'ban':
						if ($oCompanyUser->getUserRole()!=ModuleCompany::COMPANY_USER_ROLE_BAN) {
							$oCompany->setCountUser($oCompany->getCountUser()-1);							
						}
						$oCompanyUser->setUserRole(ModuleCompany::COMPANY_USER_ROLE_BAN);
						break;
					default:
						$oCompanyUser->setUserRole(ModuleCompany::COMPANY_USER_ROLE_GUEST);						
				}
				$this->Company_UpdateRelationCompanyUser($oCompanyUser);
				$this->Message_AddNoticeSingle($this->Lang_Get('company_admin_users_submit_ok'));
			}
			$this->Company_UpdateCompany($oCompany);
		}

		$aCompanyUsers=$this->Company_GetCompanyUsersByCompanyId(
			$oCompany->getId(),
			array(
				ModuleCompany::COMPANY_USER_ROLE_BAN,
				ModuleCompany::COMPANY_USER_ROLE_USER,
				ModuleCompany::COMPANY_USER_ROLE_MODERATOR,
				ModuleCompany::COMPANY_USER_ROLE_ADMINISTRATOR
			)
		);

		$this->Viewer_AddHtmlTitle($oCompany->getTitle());
		$this->Viewer_AddHtmlTitle($this->Lang_Get('company_admin'));
		
		$this->Viewer_Assign('oCompanyEdit',$oCompany);
		$this->Viewer_Assign('aCompanyUsers',$aCompanyUsers);

		$this->SetTemplateAction('admin');
		
		if($oCompany->getType()=='close') {
			$aCompanyUsersInvited=$this->Company_GetCompanyUsersByCompanyId($oCompany->getId(),ModuleCompany::COMPANY_USER_ROLE_INVITE);				
			$this->Viewer_Assign('aCompanyUsersInvited',$aCompanyUsersInvited);
			$this->Viewer_AddBlock('right','actions/ActionCompany/invited.tpl');
		}
	}
	
	protected function checkCompanyFields($oCompany=null) {

		if (!isPost('submit_company_add')) {
			$_REQUEST['company_limit_rating_seminar']=0;
			return false;
		}
		$this->Security_ValidateSendForm();
		
		$bOk=true;
		if (!func_check(getRequest('company_title'),'text',2,200)) {
			$this->Message_AddError($this->Lang_Get('company_create_title_error'),$this->Lang_Get('error'));
			$bOk=false;
		}
		if ($oCompanyExists=$this->Company_GetCompanyByTitle(getRequest('company_title'))) {
			if (!$oCompany or $oCompany->getId()!=$oCompanyExists->getId()) {
				$this->Message_AddError($this->Lang_Get('company_create_title_error_unique'),$this->Lang_Get('error'));
				$bOk=false;
			}
		}
		
		if (!$oCompany) {
			$companyUrl=preg_replace("/\s+/",'_',getRequest('company_url'));
			$_REQUEST['company_url']=$companyUrl;
			if (!func_check(getRequest('company_url'),'login',2,50)) {
				$this->Message_AddError($this->Lang_Get('company_create_url_error'),$this->Lang_Get('error'));
				$bOk=false;
			}
		}

		if (in_array(getRequest('company_url'),$this->aBadCompanyUrl)) {
			$this->Message_AddError($this->Lang_Get('company_create_url_error_badword').' '.join(',',$this->aBadCompanyUrl),$this->Lang_Get('error'));
			$bOk=false;
		}

		if ($oCompanyExists=$this->Company_GetCompanyByUrl(getRequest('company_url'))) {
			if (!$oCompany or $oCompany->getId()!=$oCompanyExists->getId()) {
				$this->Message_AddError($this->Lang_Get('company_create_url_error_unique'),$this->Lang_Get('error'));
				$bOk=false;
			}
		}

		if (!func_check(getRequest('company_description'),'text',10,3000)) {
			$this->Message_AddError($this->Lang_Get('company_create_description_error'),$this->Lang_Get('error'));
			$bOk=false;
		}

		if (!in_array(getRequest('company_type'),array('open','close'))) {
			$this->Message_AddError($this->Lang_Get('company_create_type_error'),$this->Lang_Get('error'));
			$bOk=false;
		}

		if (!func_check(getRequest('company_limit_rating_seminar'),'float')) {
			$this->Message_AddError($this->Lang_Get('company_create_rating_error'),$this->Lang_Get('error'));
			$bOk=false;
		}
		
		$this->Hook_Run('check_company_fields', array('bOk'=>&$bOk));
			
		return $bOk;
	}
	
	protected function EventSeminars() {
		$sShowType=$this->sCurrentEvent;
		$this->sMenuSubItemSelect=$sShowType;
		$iPage=$this->GetParamEventMatch(0,2) ? $this->GetParamEventMatch(0,2) : 1;			
		$aResult=$this->Seminar_GetSeminarsCollective($iPage,Config::Get('module.seminar.per_page'),$sShowType);
		$aSeminars=$aResult['collection'];	
		$aPaging=$this->Viewer_MakePaging($aResult['count'],$iPage,Config::Get('module.seminar.per_page'),4,Router::GetPath('company').$sShowType);
		$this->Hook_Run('company_show',array('sShowType'=>$sShowType));
		$this->Viewer_Assign('aSeminars',$aSeminars);
		$this->Viewer_Assign('aPaging',$aPaging);		
		$this->SetTemplateAction('index');
	}	
	/**
	 * Показ топика
	 *
	 * @param unknown_type $sCompanyUrl
	 * @param unknown_type $iSeminarId
	 * @return unknown
	 */
	protected function EventShowSeminar() {
		$sCompanyUrl='';	
		if ($this->GetParamEventMatch(0,1)) {
			// из коллективного блога
			$sCompanyUrl=$this->sCurrentEvent;
			$iSeminarId=$this->GetParamEventMatch(0,1);
			$this->sMenuItemSelect='company';			
		} else {
			// из персонального блога
			$iSeminarId=$this->GetEventMatch(1);
			$this->sMenuItemSelect='log';			
		}
		$this->sMenuSubItemSelect='';					
		/**
		 * Проверяем есть ли такой топик
		 */
		if (!($oSeminar=$this->Seminar_GetSeminarById($iSeminarId))) {
			return parent::EventNotFound();
		}
		/**
		 * Проверяем права на просмотр топика
		 */
		if (!$oSeminar->getPublish() and (!$this->oUserCurrent or ($this->oUserCurrent->getId()!=$oSeminar->getUserId() and !$this->oUserCurrent->isAdministrator()))) {
			return parent::EventNotFound();
		}

		/**
		 * Определяем права на отображение записи из закрытого блога
		 */
		if($oSeminar->getCompany()->getType()=='close' 
			and (!$this->oUserCurrent 
				|| !in_array(
						$oSeminar->getCompany()->getId(),
						$this->Company_GetAccessibleCompanysByUser($this->oUserCurrent)
					)
				)
			) {
			return parent::EventNotFound();
		}
		
		/**
		 * Если запросили топик из персонального блога то перенаправляем на страницу вывода коллективного топика
		 */
		if ($sCompanyUrl!='' and $oSeminar->getCompany()->getType()=='personal') {
			Router::Location($oSeminar->getUrl());
		}
		/**
		 * Если запросили не персональный топик то перенаправляем на страницу для вывода коллективного топика
		 */
		if ($sCompanyUrl=='' and $oSeminar->getCompany()->getType()!='personal') {
			Router::Location($oSeminar->getUrl());
		}
		/**
		 * Если номер топика правильный но УРЛ блога косяный то корректируем его и перенаправляем на нужный адрес
		 */
		if ($sCompanyUrl!='' and $oSeminar->getCompany()->getUrl()!=$sCompanyUrl) {
			Router::Location($oSeminar->getUrl());
		}
		/**
		 * Обрабатываем добавление коммента
		 */
		if (isset($_REQUEST['submit_comment'])) {
			$this->SubmitComment();
		}
		/**
		 * Достаём комменты к топику
		 */		
		$aReturn=$this->Comment_GetCommentsByTargetId($oSeminar->getId(),'seminar');
		$iMaxIdComment=$aReturn['iMaxIdComment'];	
		$aComments=$aReturn['comments'];

		/**
		 * Отмечаем дату прочтения топика
		 */
		if ($this->oUserCurrent) {
			$oSeminarRead=Engine::GetEntity('Seminar_SeminarRead');
			$oSeminarRead->setSeminarId($oSeminar->getId());
			$oSeminarRead->setUserId($this->oUserCurrent->getId());
			$oSeminarRead->setCommentCountLast($oSeminar->getCountComment());
			$oSeminarRead->setCommentIdLast($iMaxIdComment);
			$oSeminarRead->setDateRead(date("Y-m-d H:i:s"));
			$this->Seminar_SetSeminarRead($oSeminarRead);
		}
		/**
		 * Вызов хуков
		 */
		$this->Hook_Run('seminar_show',array("oSeminar"=>$oSeminar));		
		/**
		 * Выставляем SEO данные
		 */
		$sTextSeo=preg_replace("/<.*>/Ui",' ',$oSeminar->getText());
		$this->Viewer_SetHtmlDescription(func_text_words($sTextSeo,20));
		$this->Viewer_SetHtmlKeywords($oSeminar->getTags());
		/**
		 * Загружаем переменные в шаблон
		 */				
		$this->Viewer_Assign('oSeminar',$oSeminar);
		$this->Viewer_Assign('aComments',$aComments);		
		$this->Viewer_Assign('iMaxIdComment',$iMaxIdComment);
		$this->Viewer_AddHtmlTitle($oSeminar->getCompany()->getTitle());
		$this->Viewer_AddHtmlTitle($oSeminar->getTitle());
		$this->Viewer_SetHtmlRssAlternate(Router::GetPath('rss').'comments/'.$oSeminar->getId().'/',$oSeminar->getTitle());
		/**
		 * Устанавливаем шаблон вывода
		 */	
		$this->SetTemplateAction('seminar');
	}
	
	protected function EventShowCompany() {
		
		$sCompanyUrl=$this->sCurrentEvent;
		
		$oUser = $this->User_GetUserByLogin($sCompanyUrl);
		
		$oCompanyList = $this->Company_GetCompanysByOwnerId($oUser->getId());
		
		if (!($oCompany = array_pop($oCompanyList))) {
			return parent::EventNotFound();
		}
		  
		$this->sMenuSubCompanyUrl=$oCompany->getUrlFull();

		$aCompanyUsers=$this->Company_GetCompanyUsersByCompanyId($oCompany->getId(),ModuleCompany::COMPANY_USER_ROLE_USER);		
		$aCompanyModerators=$this->Company_GetCompanyUsersByCompanyId($oCompany->getId(),ModuleCompany::COMPANY_USER_ROLE_MODERATOR);
		$aCompanyAdministrators=$this->Company_GetCompanyUsersByCompanyId($oCompany->getId(),ModuleCompany::COMPANY_USER_ROLE_ADMINISTRATOR);
		
		$sShowType = "good";
		$iPage= $this->GetParamEventMatch(($sShowType=='good')?0:1,2) ? $this->GetParamEventMatch(($sShowType=='good')?0:1,2) : 1;	
		$aResult=$this->Seminar_GetSeminarsByCompany($oCompany,$iPage,Config::Get('module.seminar.per_page'),$sShowType);		
		
		$aSeminars=$aResult['collection'];
		$this->Viewer_Assign('aSeminars',$aSeminars);
		
		$sTextSeo=preg_replace("/<.*>/Ui",' ',$oCompany->getDescription());
		$this->Viewer_SetHtmlDescription(func_text_words($sTextSeo,20));	
		
		$this->Viewer_SetHtmlRssAlternate(Router::GetPath('rss').'company/'.$oCompany->getUrl().'/',$oCompany->getTitle());
		$this->Viewer_AddHtmlTitle($oCompany->getTitle());
		
		$this->Viewer_Assign('aCompanyUsers',$aCompanyUsers);
		$this->Viewer_Assign('iCountCompanyUsers',count($aCompanyUsers));
		
		$this->Viewer_Assign('aCompanyModerators',$aCompanyModerators);
		$this->Viewer_Assign('iCountCompanyModerators',count($aCompanyModerators));
		
		$this->Viewer_Assign('aCompanyAdministrators',$aCompanyAdministrators);
		$this->Viewer_Assign('iCountCompanyAdministrators',count($aCompanyAdministrators)+1);						
		
		$this->Viewer_Assign('oCompany', $oCompany);
		
		$this->SetTemplateAction('company');
	}
	/**
	 * Обработка добавление комментария к топику через ajax
	 *
	 */
	protected function AjaxAddComment() {
		$this->Viewer_SetResponseAjax();
		$this->SubmitComment();
	}	
	/**
	 * Обработка добавление комментария к топику
	 *	 
	 * @return bool
	 */
	protected function SubmitComment() {
		/**
		 * Проверям авторизован ли пользователь
		 */
		if (!$this->User_IsAuthorization()) {
			$this->Message_AddErrorSingle($this->Lang_Get('need_authorization'),$this->Lang_Get('error'));
			return;
		}
		/**
		 * Проверяем топик
		 */
		if (!($oSeminar=$this->Seminar_GetSeminarById(getRequest('cmt_target_id')))) {
			$this->Message_AddErrorSingle($this->Lang_Get('system_error'),$this->Lang_Get('error'));
			return;
		}
		/**
		 * Возможность постить коммент в топик в черновиках
		 */
		if (!$oSeminar->getPublish() and $this->oUserCurrent->getId()!=$oSeminar->getUserId() and !$this->oUserCurrent->isAdministrator()) {
			$this->Message_AddErrorSingle($this->Lang_Get('system_error'),$this->Lang_Get('error'));
			return;
		}
		
		/**
		* Проверяем разрешено ли постить комменты
		*/
		if (!$this->ACL_CanPostComment($this->oUserCurrent) and !$this->oUserCurrent->isAdministrator()) {			
			$this->Message_AddErrorSingle($this->Lang_Get('seminar_comment_acl'),$this->Lang_Get('error'));
			return;
		}
		/**
		* Проверяем разрешено ли постить комменты по времени
		*/
		if (!$this->ACL_CanPostCommentTime($this->oUserCurrent) and !$this->oUserCurrent->isAdministrator()) {			
			$this->Message_AddErrorSingle($this->Lang_Get('seminar_comment_limit'),$this->Lang_Get('error'));
			return;
		}
		/**
		* Проверяем запрет на добавления коммента автором топика
		*/
		if ($oSeminar->getForbidComment()) {
			$this->Message_AddErrorSingle($this->Lang_Get('seminar_comment_notallow'),$this->Lang_Get('error'));
			return;
		}	
		/**
		* Проверяем текст комментария
		*/
		$sText=$this->Text_Parser(getRequest('comment_text'));
		if (!func_check($sText,'text',2,10000)) {			
			$this->Message_AddErrorSingle($this->Lang_Get('seminar_comment_add_text_error'),$this->Lang_Get('error'));
			return;
		}
		/**
		* Проверям на какой коммент отвечаем
		*/
		$sParentId=(int)getRequest('reply');
		if (!func_check($sParentId,'id')) {			
			$this->Message_AddErrorSingle($this->Lang_Get('system_error'),$this->Lang_Get('error'));
			return;
		}
		$oCommentParent=null;
		if ($sParentId!=0) {
			/**
			* Проверяем существует ли комментарий на который отвечаем
			*/
			if (!($oCommentParent=$this->Comment_GetCommentById($sParentId))) {				
				$this->Message_AddErrorSingle($this->Lang_Get('system_error'),$this->Lang_Get('error'));
				return;
			}
			/**
			* Проверяем из одного топика ли новый коммент и тот на который отвечаем
			*/
			if ($oCommentParent->getTargetId()!=$oSeminar->getId()) {
				$this->Message_AddErrorSingle($this->Lang_Get('system_error'),$this->Lang_Get('error'));
				return;
			}
		} else {
			/**
			* Корневой комментарий
			*/
			$sParentId=null;
		}
		/**
		* Проверка на дублирующий коммент
		*/
		if ($this->Comment_GetCommentUnique($oSeminar->getId(),'seminar',$this->oUserCurrent->getId(),$sParentId,md5($sText))) {			
			$this->Message_AddErrorSingle($this->Lang_Get('seminar_comment_spam'),$this->Lang_Get('error'));
			return;
		}
		/**
		* Создаём коммент
		*/
		$oCommentNew=Engine::GetEntity('Comment');
		$oCommentNew->setTargetId($oSeminar->getId());
		$oCommentNew->setTargetType('seminar');
		$oCommentNew->setTargetParentId($oSeminar->getCompany()->getId());
		$oCommentNew->setUserId($this->oUserCurrent->getId());		
		$oCommentNew->setText($sText);
		$oCommentNew->setDate(date("Y-m-d H:i:s"));
		$oCommentNew->setUserIp(func_getIp());
		$oCommentNew->setPid($sParentId);
		$oCommentNew->setTextHash(md5($sText));
			
		/**
		* Добавляем коммент
		*/
		$this->Hook_Run('comment_add_before', array('oCommentNew'=>$oCommentNew,'oCommentParent'=>$oCommentParent,'oSeminar'=>$oSeminar));
		if ($this->Comment_AddComment($oCommentNew)) {
			$this->Hook_Run('comment_add_after', array('oCommentNew'=>$oCommentNew,'oCommentParent'=>$oCommentParent,'oSeminar'=>$oSeminar));
			
			$this->Viewer_AssignAjax('sCommentId',$oCommentNew->getId());
			if ($oSeminar->getPublish()) {
				/**
			 	* Добавляем коммент в прямой эфир если топик не в черновиках
			 	*/					
				$oCommentOnline=Engine::GetEntity('Comment_CommentOnline');
				$oCommentOnline->setTargetId($oCommentNew->getTargetId());
				$oCommentOnline->setTargetType($oCommentNew->getTargetType());
				$oCommentOnline->setTargetParentId($oCommentNew->getTargetParentId());
				$oCommentOnline->setCommentId($oCommentNew->getId());
				
				$this->Comment_AddCommentOnline($oCommentOnline);
			}
			/**
			* Сохраняем дату последнего коммента для юзера
			*/
			$this->oUserCurrent->setDateCommentLast(date("Y-m-d H:i:s"));
			$this->User_Update($this->oUserCurrent);
			/**
			* Отправка уведомления автору топика
			*/
			$oUserSeminar=$oSeminar->getUser();
			if ($oCommentNew->getUserId()!=$oUserSeminar->getId()) {
				$this->Notify_SendCommentNewToAuthorSeminar($oUserSeminar,$oSeminar,$oCommentNew,$this->oUserCurrent);
			}
			/**
			* Отправляем уведомление тому на чей коммент ответили
			*/
			if ($oCommentParent and $oCommentParent->getUserId()!=$oSeminar->getUserId() and $oCommentNew->getUserId()!=$oCommentParent->getUserId()) {
				$oUserAuthorComment=$oCommentParent->getUser();
				$this->Notify_SendCommentReplyToAuthorParentComment($oUserAuthorComment,$oSeminar,$oCommentNew,$this->oUserCurrent);
			}
		} else {
			$this->Message_AddErrorSingle($this->Lang_Get('system_error'),$this->Lang_Get('error'));
		}
	}	
	
	/**
	 * Обработка ajax запроса на отправку 
	 * пользователям приглашения вступить в закрытый блог
	 */
	protected function AjaxAddCompanyInvite() {
		$this->Viewer_SetResponseAjax();
		$sUsers=getRequest('users',null,'post');
		$sCompanyId=getRequest('idCompany',null,'post');
		
		/**
		 * Если пользователь не авторизирован, возвращаем ошибку
		 */
		if (!$this->User_IsAuthorization()) {	
			$this->Message_AddErrorSingle($this->Lang_Get('need_authorization'),$this->Lang_Get('error'));
			return;
		}
		$this->oUserCurrent=$this->User_GetUserCurrent();
		/**
		 * Проверяем существование блога
		 */
		if(!$oCompany=$this->Company_GetCompanyById($sCompanyId)) {
			$this->Message_AddErrorSingle($this->Lang_Get('system_error'),$this->Lang_Get('error'));
			return;			
		}
		/**
		 * Проверяем, имеет ли право текущий пользователь добавлять invite в company
		 */
		$oCompanyUser=$this->Company_GetCompanyUserByCompanyIdAndUserId($oCompany->getId(),$this->oUserCurrent->getId());		
		$bIsAdministratorCompany=$oCompanyUser ? $oCompanyUser->getIsAdministrator() : false;
		if ($oCompany->getOwnerId()!=$this->oUserCurrent->getId()  and !$this->oUserCurrent->isAdministrator() and !$bIsAdministratorCompany) {
			$this->Message_AddErrorSingle($this->Lang_Get('system_error'),$this->Lang_Get('error'));
			return;	
		}		
		
		/**
		 * Получаем список пользователей блога (любого статуса)		 
		 */
		$aCompanyUsers = $this->Company_GetCompanyUsersByCompanyId(
			$oCompany->getId(),
			array(
				ModuleCompany::COMPANY_USER_ROLE_BAN,
				ModuleCompany::COMPANY_USER_ROLE_REJECT,
				ModuleCompany::COMPANY_USER_ROLE_INVITE,
				ModuleCompany::COMPANY_USER_ROLE_USER,
				ModuleCompany::COMPANY_USER_ROLE_MODERATOR,
				ModuleCompany::COMPANY_USER_ROLE_ADMINISTRATOR
			)
		);
		$aUsers=explode(',',$sUsers);

		$aResult=array();
		/**
		 * Обрабатываем добавление по каждому из переданных логинов
		 */
		foreach ($aUsers as $sUser) {
			$sUser=trim($sUser);
			if ($sUser=='') {
				continue;
			}
			/**
			 * Если пользователь пытается добавить инвайт 
			 * самому себе, возвращаем ошибку
			 */
			if(strtolower($sUser)==strtolower($this->oUserCurrent->getLogin())) {
				$aResult[]=array(
					'bStateError'=>true,
					'sMsgTitle'=>$this->Lang_Get('error'),
					'sMsg'=>$this->Lang_Get('company_user_invite_add_self')
				);										
				continue;
			}
			
			/**
			 * Если пользователь не найден или неактивен,
			 * возвращаем ошибку
			 */
			if (!$oUser=$this->User_GetUserByLogin($sUser) or $oUser->getActivate()!=1) {
				$aResult[]=array(
					'bStateError'=>true,
					'sMsgTitle'=>$this->Lang_Get('error'),
					'sMsg'=>$this->Lang_Get('user_not_found',array('login'=>$sUser)),
					'sUserLogin'=>$sUser
				);
				continue;
			}

			if(!isset($aCompanyUsers[$oUser->getId()])) {
				/**
				 * Создаем нового блог-пользователя со статусом INVITED
				 */
				$oCompanyUserNew=Engine::GetEntity('Company_CompanyUser');
				$oCompanyUserNew->setCompanyId($oCompany->getId());
				$oCompanyUserNew->setUserId($oUser->getId());
				$oCompanyUserNew->setUserRole(ModuleCompany::COMPANY_USER_ROLE_INVITE);
				
				if($this->Company_AddRelationCompanyUser($oCompanyUserNew)) {
					$aResult[]=array(
						'bStateError'=>false,
						'sMsgTitle'=>$this->Lang_Get('attention'),
						'sMsg'=>$this->Lang_Get('company_user_invite_add_ok',array('login'=>$sUser)),
						'sUserLogin'=>$sUser,
						'sUserWebPath'=>$oUser->getUserWebPath()
					);
					$this->SendCompanyInvite($oCompany,$oUser);
				} else {
					$aResult[]=array(
						'bStateError'=>true,
						'sMsgTitle'=>$this->Lang_Get('error'),
						'sMsg'=>$this->Lang_Get('system_error'),
						'sUserLogin'=>$sUser
					);					
				}
			} else {
				/**
				 * Попытка добавить приглашение уже существующему пользователю,
				 * возвращаем ошибку (сначала определяя ее точный текст)
				 */
				switch (true) {
					case ($aCompanyUsers[$oUser->getId()]->getUserRole()==ModuleCompany::COMPANY_USER_ROLE_INVITE):
						$sErrorMessage=$this->Lang_Get('company_user_already_invited',array('login'=>$sUser));
						break;
					case ($aCompanyUsers[$oUser->getId()]->getUserRole()>ModuleCompany::COMPANY_USER_ROLE_GUEST):
						$sErrorMessage=$this->Lang_Get('company_user_already_exists',array('login'=>$sUser));						
						break;
					case ($aCompanyUsers[$oUser->getId()]->getUserRole()==ModuleCompany::COMPANY_USER_ROLE_REJECT):
						$sErrorMessage=$this->Lang_Get('company_user_already_reject',array('login'=>$sUser));						
						break;
					default:
						$sErrorMessage=$this->Lang_Get('system_error');
				}
				$aResult[]=array(
					'bStateError'=>true,
					'sMsgTitle'=>$this->Lang_Get('error'),
					'sMsg'=>$sErrorMessage,
					'sUserLogin'=>$sUser
				);
				continue;
			}		
		}
		
		/**
		 * Передаем во вьевер массив с результатами обработки по каждому пользователю
		 */
		$this->Viewer_AssignAjax('aUsers',$aResult);
	}

	/**
	 * Обработка ajax запроса на отправку 
	 * повторного приглашения вступить в закрытый блог
	 */
	protected function AjaxReCompanyInvite() {
		$this->Viewer_SetResponseAjax();
		$sUserId=getRequest('idUser',null,'post');
		$sCompanyId=getRequest('idCompany',null,'post');
		
		/**
		 * Если пользователь не авторизирован, возвращаем ошибку
		 */
		if (!$this->User_IsAuthorization()) {	
			$this->Message_AddErrorSingle($this->Lang_Get('need_authorization'),$this->Lang_Get('error'));
			return;
		}
		$this->oUserCurrent=$this->User_GetUserCurrent();
		/**
		 * Проверяем существование блога
		 */
		if(!$oCompany=$this->Company_GetCompanyById($sCompanyId)) {
			$this->Message_AddErrorSingle($this->Lang_Get('system_error'),$this->Lang_Get('error'));
			return;			
		}
		if (!$oUser=$this->User_GetUserById($sUserId) or $oUser->getActivate()!=1) {
			$this->Message_AddErrorSingle($this->Lang_Get('system_error'),$this->Lang_Get('error'));
			return;
		}
		/**
		 * Проверяем, имеет ли право текущий пользователь добавлять invite в company
		 */
		$oCompanyUser=$this->Company_GetCompanyUserByCompanyIdAndUserId($oCompany->getId(),$this->oUserCurrent->getId());		
		$bIsAdministratorCompany=$oCompanyUser ? $oCompanyUser->getIsAdministrator() : false;
		if ($oCompany->getOwnerId()!=$this->oUserCurrent->getId()  and !$this->oUserCurrent->isAdministrator() and !$bIsAdministratorCompany) {
			$this->Message_AddErrorSingle($this->Lang_Get('system_error'),$this->Lang_Get('error'));
			return;	
		}
				
		$oCompanyUser=$this->Company_GetCompanyUserByCompanyIdAndUserId($oCompany->getId(),$oUser->getId());
		if ($oCompanyUser->getUserRole()==ModuleCompany::COMPANY_USER_ROLE_INVITE) {
			$this->SendCompanyInvite($oCompany,$oUser);
			$this->Message_AddNoticeSingle($this->Lang_Get('company_user_invite_add_ok',array('login'=>$oUser->getLogin())),$this->Lang_Get('attention'));
		} else {
			$this->Message_AddErrorSingle($this->Lang_Get('system_error'),$this->Lang_Get('error'));
		}			
	}
	/**
	 * Выполняет отправку приглашения в блог 
	 * (по внутренней почте и на email)
	 *
	 * @param ModuleCompany_EntityCompany $oCompany
	 * @param ModuleUser_EntityUser $oUser
	 */
	protected function SendCompanyInvite($oCompany,$oUser) {
		$sTitle=$this->Lang_Get(
			'company_user_invite_title',
			array(
				'company_title'=>$oCompany->getTitle()
			)
		);

		require_once Config::Get('path.root.engine').'/lib/external/XXTEA/encrypt.php';
		$sCode=$oCompany->getId().'_'.$oUser->getId();
		$sCode=rawurlencode(base64_encode(xxtea_encrypt($sCode, Config::Get('module.company.encrypt'))));

		$aPath=array(
			'accept'=>Router::GetPath('company').'invite/accept/?code='.$sCode,
			'reject'=>Router::GetPath('company').'invite/reject/?code='.$sCode
		);

		$sText=$this->Lang_Get(
			'company_user_invite_text',
			array(
				'login'=>$this->oUserCurrent->getLogin(),
				'accept_path'=>$aPath['accept'],
				'reject_path'=>$aPath['reject'],
				'company_title'=>$oCompany->getTitle()
			)
		);
		$oTalk=$this->Talk_SendTalk($sTitle,$sText,$this->oUserCurrent,array($oUser),false,false);
		/**
		 * Отправляем пользователю заявку
		 */
		$this->Notify_SendCompanyUserInvite(
			$oUser,$this->oUserCurrent,$oCompany,
			Router::GetPath('talk').'read/'.$oTalk->getId().'/'
		);
		/**
		 * Удаляем отправляющего юзера из переписки
		 */	
		$this->Talk_DeleteTalkUserByArray($oTalk->getId(),$this->oUserCurrent->getId());
	}
	
	/**
	 * Обработка отправленого пользователю приглашения вступить в блог
	 */
	protected function EventInviteCompany() {	
		require_once Config::Get('path.root.engine').'/lib/external/XXTEA/encrypt.php';
		$sCode=xxtea_decrypt(base64_decode(rawurldecode(getRequest('code'))), Config::Get('module.company.encrypt'));

		if (!$sCode) {
			return $this->EventNotFound();
		}
		
		list($sCompanyId,$sUserId)=explode('_',$sCode,2);
		
		$sAction=$this->GetParam(0);
		
		/**
		 * Получаем текущего пользователя
		 */
		if(!$this->User_IsAuthorization()) {
			return $this->EventNotFound();
		}
		$this->oUserCurrent = $this->User_GetUserCurrent();
		/**
		 * Если приглашенный пользователь не является авторизированным
		 */
		if($this->oUserCurrent->getId()!=$sUserId) {
			return $this->EventNotFound();
		}
		/**
		 * Получаем указанный блог
		 */
		if((!$oCompany=$this->Company_GetCompanyById($sCompanyId)) || $oCompany->getType()!='close') {
			return $this->EventNotFound();
		}
				
		/**
		 * Получаем связь "блог-пользователь" и проверяем,
		 * чтобы ее тип был INVITE или REJECT
		 */
		if(!$oCompanyUser=$this->Company_GetCompanyUserByCompanyIdAndUserId($oCompany->getId(),$this->oUserCurrent->getId())) {
			return $this->EventNotFound();
		}
		if($oCompanyUser->getUserRole()>ModuleCompany::COMPANY_USER_ROLE_GUEST) {
			$sMessage=$this->Lang_Get('company_user_invite_already_done');
			$this->Message_AddError($sMessage,$this->Lang_Get('error'),true);
			Router::Location(Router::GetPath('talk'));
			return ;						
		}
		if(!in_array($oCompanyUser->getUserRole(),array(ModuleCompany::COMPANY_USER_ROLE_INVITE,ModuleCompany::COMPANY_USER_ROLE_REJECT))) {
			$this->Message_AddError($this->Lang_Get('system_error'),$this->Lang_Get('error'),true);
			Router::Location(Router::GetPath('talk'));
			return ;
		}
		
		/**
		 * Обновляем роль пользователя до читателя
		 */
		$oCompanyUser->setUserRole(($sAction=='accept')?ModuleCompany::COMPANY_USER_ROLE_USER:ModuleCompany::COMPANY_USER_ROLE_REJECT);
		if(!$this->Company_UpdateRelationCompanyUser($oCompanyUser)) {
			$this->Message_AddError($this->Lang_Get('system_error'),$this->Lang_Get('error'),true);
			Router::Location(Router::GetPath('talk'));
			return ;						
		}
		if ($sAction=='accept') {
			/**
			 * Увеличиваем число читателей блога
			 */
			$oCompany->setCountUser($oCompany->getCountUser()+1);
			$this->Company_UpdateCompany($oCompany);
			$sMessage=$this->Lang_Get('company_user_invite_accept');
		} else {
			$sMessage=$this->Lang_Get('company_user_invite_reject');
		}		
		$this->Message_AddNotice($sMessage,$this->Lang_Get('attention'),true);
		
		Router::Location(Router::GetPath('talk'));		
	}

	/**
	 * Удаление блога
	 *
	 * @return bool
	 */
	protected function EventDeleteCompany() {
		$this->Security_ValidateSendForm();
		/**
		 * Проверяем передан ли в УРЛе номер блога
		 */
		$sCompanyId=$this->GetParam(0);
		if (!$oCompany=$this->Company_GetCompanyById($sCompanyId)) {
			return parent::EventNotFound();
		}
		/**
		 * Проверям авторизован ли пользователь
		 */
		if (!$this->User_IsAuthorization()) {
			$this->Message_AddErrorSingle($this->Lang_Get('not_access'),$this->Lang_Get('error'));
			return Router::Action('error');
		}
		/**
		 * проверяем есть ли право на удаление топика
		 */
		if (!$bAccess=$this->ACL_IsAllowDeleteCompany($oCompany,$this->oUserCurrent)) {
			return parent::EventNotFound();
		}
		$aSeminars =  $this->Seminar_GetSeminarsByCompanyId($sCompanyId);
		switch ($bAccess) {
			case ModuleACL::CAN_DELETE_COMPANY_EMPTY_ONLY :
				if(is_array($aSeminars) and count($aSeminars)) {
					$this->Message_AddErrorSingle($this->Lang_Get('company_admin_delete_not_empty'),$this->Lang_Get('error'),true);
					Router::Location($oCompany()->getUrlFull());
				}
				break;
			case ModuleACL::CAN_DELETE_COMPANY_WITH_TOPICS :
				/**
				 * Если указан идентификатор блога для перемещения,
				 * то делаем попытку переместить топики.
				 * 
				 * (-1) - выбран пункт меню "удалить топики".
				 */
				if($sCompanyIdNew=getRequest('seminar_move_to') and ($sCompanyIdNew!=-1) and is_array($aSeminars) and count($aSeminars)) {
					if(!$oCompanyNew = $this->Company_GetCompanyById($sCompanyIdNew)){
						$this->Message_AddErrorSingle($this->Lang_Get('company_admin_delete_move_error'),$this->Lang_Get('error'),true);
						Router::Location($oCompany()->getUrlFull());
					}
					/**
					 * Если выбранный блог является персональным, возвращаем ошибку
					 */
					if($oCompanyNew->getType()=='personal') {
						$this->Message_AddErrorSingle($this->Lang_Get('company_admin_delete_move_personal'),$this->Lang_Get('error'),true);
						Router::Location($oCompany()->getUrlFull());					
					}
					/**
					 * Перемещаем топики
					 */
					$this->Seminar_MoveSeminars($sCompanyId,$sCompanyIdNew);
				}
				break;
			default:
				return parent::EventNotFound();	
		}
		/**
		 * Удаляяем блог и перенаправляем пользователя к списку блогов
		 */
		$this->Hook_Run('company_delete_before', array('sCompanyId'=>$sCompanyId));		
		if($this->Company_DeleteCompany($sCompanyId)) {
			$this->Hook_Run('company_delete_after', array('sCompanyId'=>$sCompanyId));			
			$this->Message_AddNoticeSingle($this->Lang_Get('company_admin_delete_success'),$this->Lang_Get('attention'),true);
			Router::Location(Router::GetPath('companys'));
		} else {
			Router::Location($oCompany()->getUrlFull());
		}
	}
	
	/**
	 * Выполняется при завершении работы экшена
	 *
	 */
	public function EventShutdown() {		
		/**
		 * Загружаем в шаблон необходимые переменные
		 */
		$this->Viewer_Assign('sMenuHeadItemSelect',$this->sMenuHeadItemSelect);
		$this->Viewer_Assign('sMenuItemSelect',$this->sMenuItemSelect);
		$this->Viewer_Assign('sMenuSubItemSelect',$this->sMenuSubItemSelect);
		$this->Viewer_Assign('sMenuSubCompanyUrl',$this->sMenuSubCompanyUrl);
		$this->Viewer_Assign('iCountSeminarsCollectiveNew',$this->iCountSeminarsCollectiveNew);
		$this->Viewer_Assign('iCountSeminarsPersonalNew',$this->iCountSeminarsPersonalNew);
		$this->Viewer_Assign('iCountSeminarsCompanyNew',$this->iCountSeminarsCompanyNew);
		$this->Viewer_Assign('iCountSeminarsNew',$this->iCountSeminarsNew);
		
		$this->Viewer_Assign('COMPANY_USER_ROLE_GUEST', ModuleCompany::COMPANY_USER_ROLE_GUEST);
		$this->Viewer_Assign('COMPANY_USER_ROLE_USER', ModuleCompany::COMPANY_USER_ROLE_USER);
		$this->Viewer_Assign('COMPANY_USER_ROLE_MODERATOR', ModuleCompany::COMPANY_USER_ROLE_MODERATOR);
		$this->Viewer_Assign('COMPANY_USER_ROLE_ADMINISTRATOR', ModuleCompany::COMPANY_USER_ROLE_ADMINISTRATOR);
		$this->Viewer_Assign('COMPANY_USER_ROLE_INVITE', ModuleCompany::COMPANY_USER_ROLE_INVITE);
		$this->Viewer_Assign('COMPANY_USER_ROLE_REJECT', ModuleCompany::COMPANY_USER_ROLE_REJECT);
		$this->Viewer_Assign('COMPANY_USER_ROLE_BAN', ModuleCompany::COMPANY_USER_ROLE_BAN);
	}
}
?>