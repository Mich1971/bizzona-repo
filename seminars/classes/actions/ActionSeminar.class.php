<?php
class ActionSeminar extends Action {

	protected $sMenuHeadItemSelect='company';

	protected $sMenuItemSelect='seminar';

	protected $sMenuSubItemSelect='add';

	protected $oUserCurrent=null;
	
	public function Init() {
		if (!$this->User_IsAuthorization()) {
			return parent::EventNotFound();
		}
		$this->oUserCurrent=$this->User_GetUserCurrent();
		$this->SetDefaultEvent('add');		
		$this->Viewer_AddHtmlTitle($this->Lang_Get('seminar_title'));
		
		Router::SetIsShowStats(false);
	}

	protected function RegisterEvent() {		
		$this->AddEvent('add','EventAdd');			
		$this->AddEventPreg('/^published$/i','/^(page(\d+))?$/i','EventShowSeminars');	
		$this->AddEventPreg('/^saved$/i','/^(page(\d+))?$/i','EventShowSeminars');	
		$this->AddEvent('edit','EventEdit');	
		$this->AddEvent('delete','EventDelete');
	}
		
	protected function EventEdit() {
		$this->sMenuSubItemSelect='';
		$this->sMenuItemSelect='seminar';

		$sSeminarId=$this->GetParam(0);
		if (!($oSeminar=$this->Seminar_GetSeminarById($sSeminarId))) {
			return parent::EventNotFound();
		}

		if (!$this->ACL_IsAllowEditSeminar($oSeminar,$this->oUserCurrent)) {
			return parent::EventNotFound();
		}	

		$this->Hook_Run('seminar_edit_show',array('oSeminar'=>$oSeminar));

		$aCompanyModerators=$this->Company_GetCompanyUsersByCompanyId($oSeminar->getCompanyId(),ModuleCompany::COMPANY_USER_ROLE_MODERATOR);		
		
		$this->Viewer_Assign('aCompanyModerators',$aCompanyModerators);
		$this->Viewer_Assign('aCompanysAllow',$this->Company_GetCompanysAllowByUser($this->oUserCurrent));
		$this->Viewer_AddHtmlTitle($this->Lang_Get('seminar_seminar_edit'));

		$this->SetTemplateAction('add');		

		if (isset($_REQUEST['submit_seminar_publish']) or isset($_REQUEST['submit_seminar_save'])) {
			return $this->SubmitEdit($oSeminar);
		} else {
			$_REQUEST['seminar_title']=$oSeminar->getTitle();
			$_REQUEST['seminar_text']=$oSeminar->getTextSource();
			$_REQUEST['seminar_tags']=$oSeminar->getTags();
			$_REQUEST['company_id']=$oSeminar->getCompanyId();
			$_REQUEST['seminar_id']=$oSeminar->getId();
			$_REQUEST['seminar_publish_index']=$oSeminar->getPublishIndex();
			$_REQUEST['seminar_forbid_comment']=$oSeminar->getForbidComment();
		}	

		
		
	}

	protected function EventDelete() {
		$this->Security_ValidateSendForm();
		$sSeminarId=$this->GetParam(0);
		if (!($oSeminar=$this->Seminar_GetSeminarById($sSeminarId))) {
			return parent::EventNotFound();
		}
		if (!$this->ACL_IsAllowDeleteSeminar($oSeminar,$this->oUserCurrent)) {
			return parent::EventNotFound();
		}
		$this->Hook_Run('seminar_delete_before', array('oSeminar'=>$oSeminar));
		$this->Seminar_DeleteSeminar($oSeminar);
		$this->Hook_Run('seminar_delete_after', array('oSeminar'=>$oSeminar));
		Router::Location($oSeminar->getCompany()->getUrlFull());
	}

	protected function EventAdd() {
		
		$this->sMenuSubItemSelect='add';	
		$this->Hook_Run('seminar_add_show');
		$this->Viewer_Assign('aCompanysAllow',$this->Company_GetCompanysAllowByUser($this->oUserCurrent));				
		$this->Viewer_AddHtmlTitle($this->Lang_Get('seminar_seminar_create'));
		return $this->SubmitAdd();		
	}	

	protected function EventShowSeminars() {		
		$this->sMenuSubItemSelect=$this->sCurrentEvent;
		$iPage=$this->GetParamEventMatch(0,2) ? $this->GetParamEventMatch(0,2) : 1;				
		$aResult=$this->Seminar_GetSeminarsPersonalByUser($this->oUserCurrent->getId(),$this->sCurrentEvent=='published' ? 1 : 0,$iPage,Config::Get('module.seminar.per_page'));	
		$aSeminars=$aResult['collection'];
		$aPaging=$this->Viewer_MakePaging($aResult['count'],$iPage,Config::Get('module.seminar.per_page'),4,Router::GetPath('seminar').$this->sCurrentEvent);
		$this->Viewer_Assign('aPaging',$aPaging);						
		$this->Viewer_Assign('aSeminars',$aSeminars);
		$this->Viewer_AddHtmlTitle($this->Lang_Get('seminar_menu_'.$this->sCurrentEvent));
	}

	protected function SubmitAdd() {
		if (!isPost('submit_seminar_publish') and !isPost('submit_seminar_save')) {
			return false;
		}	
		if (!$this->checkSeminarFields()) {
			return false;	
		}		
		$iCompanyId=getRequest('company_id');	
		
		if ($iCompanyId==0) {
			$oCompany=$this->Company_GetPersonalCompanyByUserId($this->oUserCurrent->getId());
		} else {
			$oCompany=$this->Company_GetCompanyById($iCompanyId);
		}	
		if (!$oCompany) {
			$this->Message_AddErrorSingle($this->Lang_Get('seminar_create_company_error_unknown'),$this->Lang_Get('error'));
			return false;
		}		
		if (!$this->ACL_IsAllowCompany($oCompany,$this->oUserCurrent)) {
			$this->Message_AddErrorSingle($this->Lang_Get('seminar_create_company_error_noallow'),$this->Lang_Get('error'));
			return false;
		}					
		if ($oSeminarEquivalent=$this->Seminar_GetSeminarUnique($this->oUserCurrent->getId(),md5(getRequest('seminar_text')))) {			
			$this->Message_AddErrorSingle($this->Lang_Get('seminar_create_text_error_unique'),$this->Lang_Get('error'));
			return false;
		}	
		if (isPost('submit_seminar_publish') and !$this->ACL_CanPostSeminarTime($this->oUserCurrent)) {			
			$this->Message_AddErrorSingle($this->Lang_Get('seminar_time_limit'),$this->Lang_Get('error'));
			return;
		}
		$oSeminar=Engine::GetEntity('Seminar');
		$oSeminar->setCompanyId($oCompany->getId());
		$oSeminar->setUserId($this->oUserCurrent->getId());
		$oSeminar->setType('seminar');
		$oSeminar->setTitle(getRequest('seminar_title'));
		$oSeminar->setTextHash(md5(getRequest('seminar_text')));
		list($sTextShort,$sTextNew,$sTextCut) = $this->Text_Cut(getRequest('seminar_text'));
		
		$oSeminar->setCutText($sTextCut);
		$oSeminar->setText($this->Text_Parser($sTextNew));
		$oSeminar->setTextShort($this->Text_Parser($sTextShort));
		
		$oSeminar->setTextSource(getRequest('seminar_text'));
		$oSeminar->setTags(getRequest('seminar_tags'));
		$oSeminar->setDateAdd(date("Y-m-d H:i:s"));
		$oSeminar->setUserIp(func_getIp());
		if (isset($_REQUEST['submit_seminar_publish'])) {
			$oSeminar->setPublish(1);
			$oSeminar->setPublishDraft(1);
		} else {
			$oSeminar->setPublish(0);
			$oSeminar->setPublishDraft(0);
		}		
		$oSeminar->setPublishIndex(0);
		if ($this->oUserCurrent->isAdministrator())	{
			if (getRequest('seminar_publish_index')) {
				$oSeminar->setPublishIndex(1);
			} 
		}	
		$oSeminar->setForbidComment(0);
		if (getRequest('seminar_forbid_comment')) {
			$oSeminar->setForbidComment(1);
		}

		$this->Hook_Run('seminar_add_before', array('oSeminar'=>$oSeminar,'oCompany'=>$oCompany));
		if ($this->Seminar_AddSeminar($oSeminar)) {
			$this->Hook_Run('seminar_add_after', array('oSeminar'=>$oSeminar,'oCompany'=>$oCompany));
			$oSeminar=$this->Seminar_GetSeminarById($oSeminar->getId());
			if ($oSeminar->getPublish()==1 and $oCompany->getType()!='personal') {
				$this->Seminar_SendNotifySeminarNew($oCompany,$oSeminar,$this->oUserCurrent);				
			}	
			Router::Location($oSeminar->getUrl());
		} else {
			$this->Message_AddErrorSingle($this->Lang_Get('system_error'));
			return Router::Action('error');
		}		
	}

	protected function SubmitEdit($oSeminar) {				
		if (!$this->checkSeminarFields()) {
			return false;	
		}	
		$iCompanyId=getRequest('company_id');	
		if ($iCompanyId==0) {
			$oCompany=$this->Company_GetPersonalCompanyByUserId($oSeminar->getUserId());
		} else {
			$oCompany=$this->Company_GetCompanyById($iCompanyId);
		}
		if (!$oCompany) {
			$this->Message_AddErrorSingle($this->Lang_Get('seminar_create_company_error_unknown'),$this->Lang_Get('error'));
			return false;
		}
		if (!$this->ACL_IsAllowCompany($oCompany,$this->oUserCurrent)) {
			$this->Message_AddErrorSingle($this->Lang_Get('seminar_create_company_error_noallow'),$this->Lang_Get('error'));
			return false;
		}
		if ($oSeminarEquivalent=$this->Seminar_GetSeminarUnique($oSeminar->getUserId(),md5(getRequest('seminar_text')))) {			
			if ($oSeminarEquivalent->getId()!=$oSeminar->getId()) {
				$this->Message_AddErrorSingle($this->Lang_Get('seminar_create_text_error_unique'),$this->Lang_Get('error'));
				return false;
			}
		}
		if (isPost('submit_seminar_publish') and !$oSeminar->getPublishDraft() and !$this->ACL_CanPostSeminarTime($this->oUserCurrent)) {			
			$this->Message_AddErrorSingle($this->Lang_Get('seminar_time_limit'),$this->Lang_Get('error'));
			return;
		}
		$sCompanyIdOld = $oSeminar->getCompanyId();
		$oSeminar->setCompanyId($oCompany->getId());
		$oSeminar->setTitle(getRequest('seminar_title'));
		$oSeminar->setTextHash(md5(getRequest('seminar_text')));
		list($sTextShort,$sTextNew,$sTextCut) = $this->Text_Cut(getRequest('seminar_text'));

		$oSeminar->setCutText($sTextCut);
		$oSeminar->setText($this->Text_Parser($sTextNew));
		$oSeminar->setTextShort($this->Text_Parser($sTextShort));
		
		$oSeminar->setTextSource(getRequest('seminar_text'));
		$oSeminar->setTags(getRequest('seminar_tags'));
		$oSeminar->setUserIp(func_getIp());
		$bSendNotify=false;
		if (isset($_REQUEST['submit_seminar_publish'])) {
			$oSeminar->setPublish(1);
			if ($oSeminar->getPublishDraft()==0) {
				$oSeminar->setPublishDraft(1);
				$oSeminar->setDateAdd(date("Y-m-d H:i:s"));
				$bSendNotify=true;
			}
		} else {
			$oSeminar->setPublish(0);
		}
		if ($this->oUserCurrent->isAdministrator())	{
			if (getRequest('seminar_publish_index')) {
				$oSeminar->setPublishIndex(1);
			} else {
				$oSeminar->setPublishIndex(0);
			}
		}
		$oSeminar->setForbidComment(0);
		if (getRequest('seminar_forbid_comment')) {
			$oSeminar->setForbidComment(1);
		}
		$this->Hook_Run('seminar_edit_before', array('oSeminar'=>$oSeminar,'oCompany'=>$oCompany));
		if ($this->Seminar_UpdateSeminar($oSeminar)) {
			$this->Hook_Run('seminar_edit_after', array('oSeminar'=>$oSeminar,'oCompany'=>$oCompany,'bSendNotify'=>&$bSendNotify));
			if($sCompanyIdOld!=$oSeminar->getCompanyId()) {
				$this->Comment_UpdateTargetParentByTargetId($oSeminar->getCompanyId(), 'seminar', $oSeminar->getId());
				$this->Comment_UpdateTargetParentByTargetIdOnline($oSeminar->getCompanyId(), 'seminar', $oSeminar->getId());
			}
			if ($bSendNotify)	 {
				$this->Seminar_SendNotifySeminarNew($oCompany,$oSeminar,$oSeminar->getUser());
			}
			if (!$oSeminar->getPublish() and !$this->oUserCurrent->isAdministrator() and $this->oUserCurrent->getId()!=$oSeminar->getUserId()) {
				Router::Location($oCompany->getUrlFull());
			}
			Router::Location($oSeminar->getUrl());
		} else {
			$this->Message_AddErrorSingle($this->Lang_Get('system_error'));
			return Router::Action('error');
		}		
	}

	protected function checkSeminarFields() {
		$this->Security_ValidateSendForm();
		
		$bOk=true;
		if (!func_check(getRequest('company_id',null,'post'),'id')) {
			$this->Message_AddError($this->Lang_Get('seminar_create_company_error_unknown'),$this->Lang_Get('error'));
			$bOk=false;
		}
		if (!func_check(getRequest('seminar_title',null,'post'),'text',2,200)) {
			$this->Message_AddError($this->Lang_Get('seminar_create_title_error'),$this->Lang_Get('error'));
			$bOk=false;
		}
		if (!func_check(getRequest('seminar_text',null,'post'),'text',2,Config::Get('module.seminar.max_length'))) {
			$this->Message_AddError($this->Lang_Get('seminar_create_text_error'),$this->Lang_Get('error'));
			$bOk=false;
		}
		if (!func_check(getRequest('seminar_tags',null,'post'),'text',2,500)) {
			$this->Message_AddError($this->Lang_Get('seminar_create_tags_error'),$this->Lang_Get('error'));
			$bOk=false;
		}
		$sTags=getRequest('seminar_tags',null,'post');
		$aTags=explode(',',$sTags);
		$aTagsNew=array();
		$aTagsNewLow=array();
		foreach ($aTags as $sTag) {
			$sTag=trim($sTag);
			if (func_check($sTag,'text',2,50) and !in_array(mb_strtolower($sTag,'UTF-8'),$aTagsNewLow)) {
				$aTagsNew[]=$sTag;
				$aTagsNewLow[]=mb_strtolower($sTag,'UTF-8');
			}
		}
		if (!count($aTagsNew)) {
			$this->Message_AddError($this->Lang_Get('seminar_create_tags_error_bad'),$this->Lang_Get('error'));
			$bOk=false;
		} else {
			$_REQUEST['seminar_tags']=join(',',$aTagsNew);
		}
		$this->Hook_Run('check_seminar_fields', array('bOk'=>&$bOk));
		
		return $bOk;
	}

	public function EventShutdown() {
		$this->Viewer_Assign('sMenuHeadItemSelect',$this->sMenuHeadItemSelect);
		$this->Viewer_Assign('sMenuItemSelect',$this->sMenuItemSelect);
		$this->Viewer_Assign('sMenuSubItemSelect',$this->sMenuSubItemSelect);
	}
}
?>