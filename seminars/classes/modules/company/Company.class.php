<?php
/*-------------------------------------------------------
*
*   LiveStreet Engine Social Networking
*   Copyright © 2008 Mzhelskiy Maxim
*
*--------------------------------------------------------
*
*   Official site: www.livestreet.ru
*   Contact e-mail: rus.engine@gmail.com
*
*   GNU General Public License, version 2:
*   http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
*
---------------------------------------------------------
*/

/**
 * Модуль для работы с блогами
 *
 */
class ModuleCompany extends Module {
	/**
	 * Возможные роли пользователя в блоге
	 */
	const COMPANY_USER_ROLE_GUEST         = 0;
	const COMPANY_USER_ROLE_USER          = 1;
	const COMPANY_USER_ROLE_MODERATOR     = 2;
	const COMPANY_USER_ROLE_ADMINISTRATOR = 4;
	/**
	 * Пользователь, приглашенный админом блога в блог
	 */
	const COMPANY_USER_ROLE_INVITE        = -1;
	/**
	 * Пользователь, отклонивший приглашение админа
	 */
	const COMPANY_USER_ROLE_REJECT        = -2;
	/**
	 * Забаненный в блоге пользователь
	 */
	const COMPANY_USER_ROLE_BAN           = -4;
	
	protected $oMapperCompany;	
	protected $oUserCurrent=null;
		
	/**
	 * �?нициализация
	 *
	 */
	public function Init() {
		$this->oMapperCompany=Engine::GetMapper(__CLASS__);
		$this->oMapperCompany->SetUserCurrent($this->User_GetUserCurrent());
		$this->oUserCurrent=$this->User_GetUserCurrent();		
	}
	/**
	 * Получает дополнительные данные(объекты) для блогов по их ID
	 *
	 */
	public function GetCompanysAdditionalData($aCompanyId,$aAllowData=array('vote','owner'=>array(),'relation_user')) {
		func_array_simpleflip($aAllowData);
		if (!is_array($aCompanyId)) {
			$aCompanyId=array($aCompanyId);
		}
		/**
		 * Получаем блоги
		 */
		$aCompanys=$this->GetCompanysByArrayId($aCompanyId);
		/**
		 * Формируем ID дополнительных данных, которые нужно получить
		 */
		$aUserId=array();		
		foreach ($aCompanys as $oCompany) {
			if (isset($aAllowData['owner'])) {
				$aUserId[]=$oCompany->getOwnerId();
			}						
		}
		/**
		 * Получаем дополнительные данные
		 */
		$aCompanyUsers=array();
		$aCompanysVote=array();
		$aUsers=isset($aAllowData['owner']) && is_array($aAllowData['owner']) ? $this->User_GetUsersAdditionalData($aUserId,$aAllowData['owner']) : $this->User_GetUsersAdditionalData($aUserId);				
		if (isset($aAllowData['relation_user']) and $this->oUserCurrent) {
			$aCompanyUsers=$this->GetCompanyUsersByArrayCompany($aCompanyId,$this->oUserCurrent->getId());	
		}
		if (isset($aAllowData['vote']) and $this->oUserCurrent) {			
			$aCompanysVote=$this->Vote_GetVoteByArray($aCompanyId,'company',$this->oUserCurrent->getId());
		}
		/**
		 * Добавляем данные к результату - списку блогов
		 */
		foreach ($aCompanys as $oCompany) {
			if (isset($aUsers[$oCompany->getOwnerId()])) {
				$oCompany->setOwner($aUsers[$oCompany->getOwnerId()]);
			} else {
				$oCompany->setOwner(null); // или $oCompany->setOwner(new ModuleUser_EntityUser());
			}
			if (isset($aCompanyUsers[$oCompany->getId()])) {
				$oCompany->setUserIsJoin(true);
				$oCompany->setUserIsAdministrator($aCompanyUsers[$oCompany->getId()]->getIsAdministrator());
				$oCompany->setUserIsModerator($aCompanyUsers[$oCompany->getId()]->getIsModerator());
			} else {
				$oCompany->setUserIsJoin(false);
				$oCompany->setUserIsAdministrator(false);
				$oCompany->setUserIsModerator(false);
			}
			if (isset($aCompanysVote[$oCompany->getId()])) {
				$oCompany->setVote($aCompanysVote[$oCompany->getId()]);				
			} else {
				$oCompany->setVote(null);
			}
		}
		
		return $aCompanys;
	}
	/**
	 * Список блогов по ID
	 *
	 * @param array $aUserId
	 */
	public function GetCompanysByArrayId($aCompanyId) {
		if (!$aCompanyId) {
			return array();
		}
		if (Config::Get('sys.cache.solid')) {
			return $this->GetCompanysByArrayIdSolid($aCompanyId);
		}
		if (!is_array($aCompanyId)) {
			$aCompanyId=array($aCompanyId);
		}
		$aCompanyId=array_unique($aCompanyId);
		$aCompanys=array();
		$aCompanyIdNotNeedQuery=array();
		/**
		 * Делаем мульти-запрос к кешу
		 */
		$aCacheKeys=func_build_cache_keys($aCompanyId,'company_');
		if (false !== ($data = $this->Cache_Get($aCacheKeys))) {			
			/**
			 * проверяем что досталось из кеша
			 */
			foreach ($aCacheKeys as $sValue => $sKey ) {
				if (array_key_exists($sKey,$data)) {	
					if ($data[$sKey]) {
						$aCompanys[$data[$sKey]->getId()]=$data[$sKey];
					} else {
						$aCompanyIdNotNeedQuery[]=$sValue;
					}
				} 
			}
		}
		/**
		 * Смотрим каких блогов не было в кеше и делаем запрос в БД
		 */		
		$aCompanyIdNeedQuery=array_diff($aCompanyId,array_keys($aCompanys));		
		$aCompanyIdNeedQuery=array_diff($aCompanyIdNeedQuery,$aCompanyIdNotNeedQuery);		
		$aCompanyIdNeedStore=$aCompanyIdNeedQuery;
		if ($data = $this->oMapperCompany->GetCompanysByArrayId($aCompanyIdNeedQuery)) {
			foreach ($data as $oCompany) {
				/**
				 * Добавляем к результату и сохраняем в кеш
				 */
				$aCompanys[$oCompany->getId()]=$oCompany;
				$this->Cache_Set($oCompany, "company_{$oCompany->getId()}", array(), 60*60*24*4);
				$aCompanyIdNeedStore=array_diff($aCompanyIdNeedStore,array($oCompany->getId()));
			}
		}
		/**
		 * Сохраняем в кеш запросы не вернувшие результата
		 */
		foreach ($aCompanyIdNeedStore as $sId) {
			$this->Cache_Set(null, "company_{$sId}", array(), 60*60*24*4);
		}		
		/**
		 * Сортируем результат согласно входящему массиву
		 */
		$aCompanys=func_array_sort_by_keys($aCompanys,$aCompanyId);
		return $aCompanys;		
	}
	/**
	 * Список блогов по ID, но используя единый кеш
	 *
	 * @param unknown_type $aCompanyId
	 * @return unknown
	 */
	public function GetCompanysByArrayIdSolid($aCompanyId) {
		if (!is_array($aCompanyId)) {
			$aCompanyId=array($aCompanyId);
		}
		$aCompanyId=array_unique($aCompanyId);	
		$aCompanys=array();	
		$s=join(',',$aCompanyId);
		if (false === ($data = $this->Cache_Get("company_id_{$s}"))) {			
			$data = $this->oMapperCompany->GetCompanysByArrayId($aCompanyId);
			foreach ($data as $oCompany) {
				$aCompanys[$oCompany->getId()]=$oCompany;
			}
			$this->Cache_Set($aCompanys, "company_id_{$s}", array("company_update"), 60*60*24*1);
			return $aCompanys;
		}		
		return $data;
	}
	/**
	 * Получить персональный блог юзера
	 *
	 * @param Entity_User $oUser
	 * @return unknown
	 */
	public function GetPersonalCompanyByUserId($sUserId) {
		$id=$this->oMapperCompany->GetPersonalCompanyByUserId($sUserId);
		return $this->GetCompanyById($id);
	}
	/**
	 * Получить блог по айдишнику(номеру)
	 *
	 * @param unknown_type $sCompanyId
	 * @return unknown
	 */
	public function GetCompanyById($sCompanyId) {
		$aCompanys=$this->GetCompanysAdditionalData($sCompanyId);
		if (isset($aCompanys[$sCompanyId])) {
			return $aCompanys[$sCompanyId];
		}
		return null;		
	}
	/**
	 * Получить блог по УРЛу
	 *
	 * @param unknown_type $sCompanyUrl
	 * @return unknown
	 */
	public function GetCompanyByUrl($sCompanyUrl) {		
		if (false === ($id = $this->Cache_Get("company_url_{$sCompanyUrl}"))) {						
			if ($id = $this->oMapperCompany->GetCompanyByUrl($sCompanyUrl)) {				
				$this->Cache_Set($id, "company_url_{$sCompanyUrl}", array("company_update_{$id}"), 60*60*24*2);				
			} else {
				$this->Cache_Set(null, "company_url_{$sCompanyUrl}", array('company_update','company_new'), 60*60);
			}
		}		
		return $this->GetCompanyById($id);		
	}
	/**
	 * Получить блог по названию
	 *
	 * @param unknown_type $sTitle
	 * @return unknown
	 */
	public function GetCompanyByTitle($sTitle) {		
		if (false === ($id = $this->Cache_Get("company_title_{$sTitle}"))) {						
			if ($id = $this->oMapperCompany->GetCompanyByTitle($sTitle)) {				
				$this->Cache_Set($id, "company_title_{$sTitle}", array("company_update_{$id}",'company_new'), 60*60*24*2);				
			} else {
				$this->Cache_Set(null, "company_title_{$sTitle}", array('company_update','company_new'), 60*60);
			}
		}
		return $this->GetCompanyById($id);		
	}
	/**
	 * Создаёт персональный блог
	 *
	 * @param Entity_User $oUser
	 * @return unknown
	 */
	public function CreatePersonalCompany(ModuleUser_EntityUser $oUser) {
		$oCompany=Engine::GetEntity('Company');
		$oCompany->setOwnerId($oUser->getId());
		$oCompany->setTitle($this->Lang_Get('companys_personal_title').' '.$oUser->getLogin());
		$oCompany->setType('personal');
		$oCompany->setDescription($this->Lang_Get('companys_personal_description'));
		$oCompany->setDateAdd(date("Y-m-d H:i:s")); 
		$oCompany->setLimitRatingSeminar(-1000);
		$oCompany->setUrl(null);	
		$oCompany->setAvatar(null);
		return $this->AddCompany($oCompany);		
	}
	/**
	 * Добавляет блог
	 *
	 * @param ModuleCompany_EntityCompany $oCompany
	 * @return unknown
	 */
	public function AddCompany(ModuleCompany_EntityCompany $oCompany) {		
		if ($sId=$this->oMapperCompany->AddCompany($oCompany)) {
			$oCompany->setId($sId);
			//чистим зависимые кеши
			$this->Cache_Clean(Zend_Cache::CLEANING_MODE_MATCHING_TAG,array('company_new',"company_new_user_{$oCompany->getOwnerId()}"));						
			return $oCompany;
		}
		return false;
	}
	/**
	 * Обновляет блог
	 *
	 * @param ModuleCompany_EntityCompany $oCompany
	 * @return unknown
	 */
	public function UpdateCompany(ModuleCompany_EntityCompany $oCompany) {
		$oCompany->setDateEdit(date("Y-m-d H:i:s"));
		$res=$this->oMapperCompany->UpdateCompany($oCompany);		
		if ($res) {			
			//чистим зависимые кеши
			$this->Cache_Clean(Zend_Cache::CLEANING_MODE_MATCHING_TAG,array('company_update',"company_update_{$oCompany->getId()}","seminar_update"));
			$this->Cache_Delete("company_{$oCompany->getId()}");
			return true;
		}
		return false;
	}	
	/**
	 * Добавляет отношение юзера к блогу, по сути присоединяет к блогу
	 *
	 * @param ModuleCompany_EntityCompanyUser $oCompanyUser
	 * @return unknown
	 */
	public function AddRelationCompanyUser(ModuleCompany_EntityCompanyUser $oCompanyUser) {
		if ($this->oMapperCompany->AddRelationCompanyUser($oCompanyUser)) {		
			$this->Cache_Clean(Zend_Cache::CLEANING_MODE_MATCHING_TAG,array("company_relation_change_{$oCompanyUser->getUserId()}","company_relation_change_company_{$oCompanyUser->getCompanyId()}"));	
			$this->Cache_Delete("company_relation_user_{$oCompanyUser->getCompanyId()}_{$oCompanyUser->getUserId()}");	
			return true;
		}
		return false;
	}
	/**
	 * Удалет отношение юзера к блогу, по сути отключает от блога
	 *
	 * @param ModuleCompany_EntityCompanyUser $oCompanyUser
	 * @return unknown
	 */
	public function DeleteRelationCompanyUser(ModuleCompany_EntityCompanyUser $oCompanyUser) {
		if ($this->oMapperCompany->DeleteRelationCompanyUser($oCompanyUser)) {
			$this->Cache_Clean(Zend_Cache::CLEANING_MODE_MATCHING_TAG,array("company_relation_change_{$oCompanyUser->getUserId()}","company_relation_change_company_{$oCompanyUser->getCompanyId()}"));		
			$this->Cache_Delete("company_relation_user_{$oCompanyUser->getCompanyId()}_{$oCompanyUser->getUserId()}");
			return true;
		}
		return false;
	}
	/**
	 * Получает список блогов по хозяину
	 *
	 * @param unknown_type $sUserId
	 * @return unknown
	 */
	public function GetCompanysByOwnerId($sUserId,$bReturnIdOnly=false) {
		$data=$this->oMapperCompany->GetCompanysByOwnerId($sUserId);
		/**
		 * Возвращаем только иденитификаторы
		 */
		if($bReturnIdOnly) return $data;
		
		$data=$this->GetCompanysAdditionalData($data);
		return $data;
	}
	/**
	 * Получает список всех НЕ персональных блогов
	 *
	 * @return unknown
	 */
	public function GetCompanys($bReturnIdOnly=false) {
		$data=$this->oMapperCompany->GetCompanys();
		/**
		 * Возвращаем только иденитификаторы
		 */
		if($bReturnIdOnly) return $data;
				
		$data=$this->GetCompanysAdditionalData($data);
		return $data;
	}
		
	/**
	 * Получает список пользователей блога.
	 * Если роль не указана, то считаем что 
	 * поиск производиться по положительным значениям
	 * (статусом выше GUEST).
	 *
	 * @param  string           $sCompanyId
	 * @param  (null|int|array) $iRole
	 * @return array
	 */
	public function GetCompanyUsersByCompanyId($sCompanyId,$iRole=null) {
		$aFilter=array(
			'company_id'=> $sCompanyId,			
		);
		if($iRole!==null) {
			$aFilter['user_role']=$iRole;	
		}
		$s=serialize($aFilter);
		if (false === ($data = $this->Cache_Get("company_relation_user_by_filter_$s"))) {				
			$data = $this->oMapperCompany->GetCompanyUsers($aFilter);
			$this->Cache_Set($data, "company_relation_user_by_filter_$s", array("company_relation_change_company_{$sCompanyId}"), 60*60*24*3);
		}
		/**
		 * Достаем дополнительные данные, для этого формируем список юзеров и делаем мульти-запрос
		 */
		if ($data) {
			$aUserId=array();
			foreach ($data as $oCompanyUser) {
				$aUserId[]=$oCompanyUser->getUserId();
			}
			$aUsers=$this->User_GetUsersAdditionalData($aUserId);
			$aCompanys=$this->Company_GetCompanysAdditionalData($sCompanyId);
			
			$aResults=array();
			foreach ($data as $oCompanyUser) {
				if (isset($aUsers[$oCompanyUser->getUserId()])) {
					$oCompanyUser->setUser($aUsers[$oCompanyUser->getUserId()]);
				} else {
					$oCompanyUser->setUser(null);
				}
				if (isset($aCompanys[$oCompanyUser->getCompanyId()])) {
					$oCompanyUser->setCompany($aCompanys[$oCompanyUser->getCompanyId()]);
				} else {
					$oCompanyUser->setCompany(null);
				}
				$aResults[$oCompanyUser->getUserId()]=$oCompanyUser;
			}
			$data=$aResults;
		}
		return $data;		
	}
	/**
	 * Получает отношения юзера к блогам(состоит в блоге или нет)
	 *
	 * @param unknown_type $sUserId
	 * @return unknown
	 */
	public function GetCompanyUsersByUserId($sUserId,$iRole=null,$bReturnIdOnly=false) {
		$aFilter=array(
			'user_id'=> $sUserId
		);
		if($iRole!==null) {
			$aFilter['user_role']=$iRole;
		}
		$s=serialize($aFilter);
		if (false === ($data = $this->Cache_Get("company_relation_user_by_filter_$s"))) {				
			$data = $this->oMapperCompany->GetCompanyUsers($aFilter);
			$this->Cache_Set($data, "company_relation_user_by_filter_$s", array("company_relation_change_{$sUserId}"), 60*60*24*3);
		}
		/**
		 * Достаем дополнительные данные, для этого формируем список блогов и делаем мульти-запрос
		 */
		$aCompanyId=array();		
		if ($data) {
			foreach ($data as $oCompanyUser) {
				$aCompanyId[]=$oCompanyUser->getCompanyId();
			}
			
			/**
			 * Если указано возвращать полные объекты
			 */
			if(!$bReturnIdOnly) {
				$aUsers=$this->User_GetUsersAdditionalData($sUserId);
				$aCompanys=$this->Company_GetCompanysAdditionalData($aCompanyId);
				foreach ($data as $oCompanyUser) {
					if (isset($aUsers[$oCompanyUser->getUserId()])) {
						$oCompanyUser->setUser($aUsers[$oCompanyUser->getUserId()]);
					} else {
						$oCompanyUser->setUser(null);
					}
					if (isset($aCompanys[$oCompanyUser->getCompanyId()])) {
						$oCompanyUser->setCompany($aCompanys[$oCompanyUser->getCompanyId()]);
					} else {
						$oCompanyUser->setCompany(null);
					}
				}				
			}
		}
		return ($bReturnIdOnly) ? $aCompanyId : $data;
	}
	/**
	 * Состоит ли юзер в конкретном блоге
	 *
	 * @param unknown_type $sCompanyId
	 * @param unknown_type $sUserId
	 * @return unknown
	 */
	public function GetCompanyUserByCompanyIdAndUserId($sCompanyId,$sUserId) {		
		if ($aCompanyUser=$this->GetCompanyUsersByArrayCompany($sCompanyId,$sUserId)) {
			if (isset($aCompanyUser[$sCompanyId])) {
				return $aCompanyUser[$sCompanyId];
			}
		}
		return null;
	}
	/**
	 * Получить список отношений блог-юзер по списку айдишников
	 *
	 * @param unknown_type $aSeminarId
	 */
	public function GetCompanyUsersByArrayCompany($aCompanyId,$sUserId) {
		if (!$aCompanyId) {
			return array();
		}
		if (Config::Get('sys.cache.solid')) {
			return $this->GetCompanyUsersByArrayCompanySolid($aCompanyId,$sUserId);
		}
		if (!is_array($aCompanyId)) {
			$aCompanyId=array($aCompanyId);
		}
		$aCompanyId=array_unique($aCompanyId);
		$aCompanyUsers=array();
		$aCompanyIdNotNeedQuery=array();
		/**
		 * Делаем мульти-запрос к кешу
		 */
		$aCacheKeys=func_build_cache_keys($aCompanyId,'company_relation_user_','_'.$sUserId);
		if (false !== ($data = $this->Cache_Get($aCacheKeys))) {			
			/**
			 * проверяем что досталось из кеша
			 */
			foreach ($aCacheKeys as $sValue => $sKey ) {
				if (array_key_exists($sKey,$data)) {	
					if ($data[$sKey]) {
						$aCompanyUsers[$data[$sKey]->getCompanyId()]=$data[$sKey];
					} else {
						$aCompanyIdNotNeedQuery[]=$sValue;
					}
				} 
			}
		}
		/**
		 * Смотрим каких блогов не было в кеше и делаем запрос в БД
		 */		
		$aCompanyIdNeedQuery=array_diff($aCompanyId,array_keys($aCompanyUsers));		
		$aCompanyIdNeedQuery=array_diff($aCompanyIdNeedQuery,$aCompanyIdNotNeedQuery);		
		$aCompanyIdNeedStore=$aCompanyIdNeedQuery;
		if ($data = $this->oMapperCompany->GetCompanyUsersByArrayCompany($aCompanyIdNeedQuery,$sUserId)) {
			foreach ($data as $oCompanyUser) {
				/**
				 * Добавляем к результату и сохраняем в кеш
				 */
				$aCompanyUsers[$oCompanyUser->getCompanyId()]=$oCompanyUser;
				$this->Cache_Set($oCompanyUser, "company_relation_user_{$oCompanyUser->getCompanyId()}_{$oCompanyUser->getUserId()}", array(), 60*60*24*4);
				$aCompanyIdNeedStore=array_diff($aCompanyIdNeedStore,array($oCompanyUser->getCompanyId()));
			}
		}
		/**
		 * Сохраняем в кеш запросы не вернувшие результата
		 */
		foreach ($aCompanyIdNeedStore as $sId) {
			$this->Cache_Set(null, "company_relation_user_{$sId}_{$sUserId}", array(), 60*60*24*4);
		}		
		/**
		 * Сортируем результат согласно входящему массиву
		 */
		$aCompanyUsers=func_array_sort_by_keys($aCompanyUsers,$aCompanyId);
		return $aCompanyUsers;		
	}	
	public function GetCompanyUsersByArrayCompanySolid($aCompanyId,$sUserId) {
		if (!is_array($aCompanyId)) {
			$aCompanyId=array($aCompanyId);
		}
		$aCompanyId=array_unique($aCompanyId);	
		$aCompanyUsers=array();	
		$s=join(',',$aCompanyId);
		if (false === ($data = $this->Cache_Get("company_relation_user_{$sUserId}_id_{$s}"))) {			
			$data = $this->oMapperCompany->GetCompanyUsersByArrayCompany($aCompanyId,$sUserId);
			foreach ($data as $oCompanyUser) {
				$aCompanyUsers[$oCompanyUser->getCompanyId()]=$oCompanyUser;
			}
			$this->Cache_Set($aCompanyUsers, "company_relation_user_{$sUserId}_id_{$s}", array("company_relation_change_{$sUserId}"), 60*60*24*1);
			return $aCompanyUsers;
		}		
		return $data;
	}
	public function UpdateRelationCompanyUser(ModuleCompany_EntityCompanyUser $oCompanyUser) {
		$this->Cache_Clean(Zend_Cache::CLEANING_MODE_MATCHING_TAG,array("company_relation_change_{$oCompanyUser->getUserId()}","company_relation_change_company_{$oCompanyUser->getCompanyId()}"));		
		$this->Cache_Delete("company_relation_user_{$oCompanyUser->getCompanyId()}_{$oCompanyUser->getUserId()}");
		return $this->oMapperCompany->UpdateRelationCompanyUser($oCompanyUser);
	}
	/**
	 * Получает список блогов по рейтингу
	 *
	 * @param unknown_type $iLimit
	 * @return unknown
	 */
	public function GetCompanysRating($iCurrPage,$iPerPage) {		
		if (false === ($data = $this->Cache_Get("company_rating_{$iCurrPage}_{$iPerPage}"))) {				
			$data = array('collection'=>$this->oMapperCompany->GetCompanysRating($iCount,$iCurrPage,$iPerPage),'count'=>$iCount);
			$this->Cache_Set($data, "company_rating_{$iCurrPage}_{$iPerPage}", array("company_update","company_new"), 60*60*24*2);
		}
		$data['collection']=$this->GetCompanysAdditionalData($data['collection'],array('owner'=>array(),'relation_user'));
		return $data;
	}
	/**
	 * Список подключенных блогов по рейтингу
	 *
	 * @param unknown_type $sUserId
	 * @param unknown_type $iLimit
	 * @return unknown
	 */
	public function GetCompanysRatingJoin($sUserId,$iLimit) { 		
		if (false === ($data = $this->Cache_Get("company_rating_join_{$sUserId}_{$iLimit}"))) {				
			$data = $this->oMapperCompany->GetCompanysRatingJoin($sUserId,$iLimit);			
			$this->Cache_Set($data, "company_rating_join_{$sUserId}_{$iLimit}", array('company_update',"company_relation_change_{$sUserId}"), 60*60*24);
		}
		return $data;		
	}
	/**
	 * Список сових блогов по рейтингу
	 *
	 * @param unknown_type $sUserId
	 * @param unknown_type $iLimit
	 * @return unknown
	 */
	public function GetCompanysRatingSelf($sUserId,$iLimit) { 		
		if (false === ($data = $this->Cache_Get("company_rating_self_{$sUserId}_{$iLimit}"))) {				
			$data = $this->oMapperCompany->GetCompanysRatingSelf($sUserId,$iLimit);			
			$this->Cache_Set($data, "company_rating_self_{$sUserId}_{$iLimit}", array('company_update',"company_new_user_{$sUserId}"), 60*60*24);
		}
		return $data;		
	}	
	/**
	 * Получает список блогов в которые может постить юзер
	 *
	 * @param unknown_type $oUser	 
	 * @return unknown
	 */
	public function GetCompanysAllowByUser($oUser) {		
		if ($oUser->isAdministrator()) {
			return $this->GetCompanys();
		} else {						
			$aAllowCompanysUser=$this->GetCompanysByOwnerId($oUser->getId());
			$aCompanyUsers=$this->GetCompanyUsersByUserId($oUser->getId());			
			foreach ($aCompanyUsers as $oCompanyUser) {
				$oCompany=$oCompanyUser->getCompany();
				if ($this->ACL_CanAddSeminar($oUser,$oCompany) or $oCompanyUser->getIsAdministrator() or $oCompanyUser->getIsModerator()) {
					$aAllowCompanysUser[$oCompany->getId()]=$oCompany;
				}
			}
			return 	$aAllowCompanysUser;
		}		
	}	

	/**
	 * Получаем массив блогов, 
	 * которые являются открытыми для пользователя
	 *
	 * @param  ModuleUser_EntityUser $oUser
	 * @return array
	 */
	public function GetAccessibleCompanysByUser($oUser) {
		if ($oUser->isAdministrator()) {
			return $this->GetCompanys(true);
		}
		
		if (false === ($aOpenCompanysUser = $this->Cache_Get("company_accessible_user_{$oUser->getId()}"))) {
			/**
		 	* Заносим блоги, созданные пользователем
		 	*/
			$aOpenCompanysUser=array();
			$aOpenCompanysUser=$this->GetCompanysByOwnerId($oUser->getId(),true);
			/**
		 	* Добавляем блоги, в которых состоит пользователь
		 	* (читателем, модератором, или администратором)
		 	*/
			$aOpenCompanysUser=array_merge($aOpenCompanysUser,$this->GetCompanyUsersByUserId($oUser->getId(),null,true));
			$this->Cache_Set($aOpenCompanysUser, "company_accessible_user_{$oUser->getId()}", array('company_new','company_update',"company_relation_change_{$oUser->getId()}"), 60*60*24);
		}
		return $aOpenCompanysUser;
	}

	/**
	 * Получаем массив идентификаторов блогов, 
	 * которые являются закрытыми для пользователя
	 *
	 * @param  ModuleUser_EntityUser $oUser
	 * @return array
	 */	
	public function GetInaccessibleCompanysByUser($oUser=null) {
		if ($oUser&&$oUser->isAdministrator()) {
			return array();
		}
		
		$sUserId=$oUser ? $oUser->getId() : 'quest';		
		if (false === ($aCloseCompanys = $this->Cache_Get("company_inaccessible_user_{$sUserId}"))) {
			$aCloseCompanys=array();
			$aCloseCompanys = $this->oMapperCompany->GetCloseCompanys();

			if($oUser) {
				/**
		 		* Получаем массив идентификаторов блогов, 
		 		* которые являются откытыми для данного пользователя
		 		*/
				$aOpenCompanys = array();
				$aOpenCompanys=$this->GetCompanyUsersByUserId($oUser->getId(),null,true);
				
				$aCloseCompanys=array_diff($aCloseCompanys,$aOpenCompanys);
			}

			// Сохраняем в кеш
			if ($oUser) {
				$this->Cache_Set($aCloseCompanys, "company_inaccessible_user_{$sUserId}", array('company_new','company_update',"company_relation_change_{$oUser->getId()}"), 60*60*24);		
			} else {
				$this->Cache_Set($aCloseCompanys, "company_inaccessible_user_{$sUserId}", array('company_new','company_update'), 60*60*24*3);
			}
		}
		return $aCloseCompanys;
	}
	
	/**
	 * Удаляет блог
	 *
	 * @param  int $iCompanyId
	 * @return bool
	 */
	public function DeleteCompany($iCompanyId) {
		if($iCompanyId instanceof ModuleCompany_EntityCompany){
			$iCompanyId = $iCompanyId->getId();
		}
		/**
		 * Получаем идентификаторы топиков блога. Удаляем топики блога. 
		 * При удалении топиков удаляются комментарии к ним и голоса.
		 */
		$aSeminarIds = $this->Seminar_GetSeminarsByCompanyId($iCompanyId);		
		/**
		 * Если блог не удален, возвращаем false
		 */
		if(!$this->oMapperCompany->DeleteCompany($iCompanyId)) { return false; }
		/**
		 * Чистим кеш
		 */
		$this->Cache_Clean(
			Zend_Cache::CLEANING_MODE_MATCHING_TAG,
			array(
				"company_update", "company_relation_change_company_{$iCompanyId}",
				"seminar_update", "comment_online_update_seminar", "comment_update"
			)
		);
		$this->Cache_Delete("company_{$iCompanyId}");
		
		if(is_array($aSeminarIds) and count($aSeminarIds)) {
			/**
			 * Удаляем топики
			 */
			foreach ($aSeminarIds as $iSeminarId) {
				$this->Cache_Delete("seminar_{$iSeminarId}");
				if(Config::Get('db.tables.engine')=="InnoDB") {
					$this->Seminar_DeleteSeminarAdditionalData($iSeminarId);
				} else {
					$this->Seminar_DeleteSeminar($iSeminarId);
				}
			}

		}
		
		/**
		 * Удаляем связи пользователей блога.
		 */
		if(Config::Get('db.tables.engine')!="InnoDB"){ 
			$this->oMapperCompany->DeleteCompanyUsersByCompanyId($iCompanyId);
		}
		/**
		 * Удаляем голосование за блог
		 */
		$this->Vote_DeleteVoteByTarget($iCompanyId, 'company');
		/**
		 * Удаляем комментарии к записям из блога и метки прямого эфира
		 */
		
		return true;
	}
	/**
	 * Upload company avatar on server
	 * Make resized images
	 *
	 * @param  array           $aFile
	 * @param  ModuleCompany_EntityCompany $oUser
	 * @return (string|bool)
	 */
	public function UploadCompanyAvatar($aFile,$oCompany) {
		if(!is_array($aFile) || !isset($aFile['tmp_name'])) {
			return false;
		}
		
		$sFileTmp=Config::Get('sys.cache.dir').func_generator();		
		if (!move_uploaded_file($aFile['tmp_name'],$sFileTmp)) {
			return false;
		}
	
		$sPath=$this->Image_GetIdDir($oCompany->getOwnerId());
		$aParams=$this->Image_BuildParams('avatar');

		$oImage=new LiveImage($sFileTmp);
		/**
		 * Если объект изображения не создан, 
		 * возвращаем ошибку
		 */
		if($sError=$oImage->get_last_error()) {
			// Вывод сообщения об ошибки, произошедшей при создании объекта изображения
			// $this->Message_AddError($sError,$this->Lang_Get('error'));
			@unlink($sFileTmp);
			return false;
		}		
		/**
		 * Срезаем квадрат
		 */
		$oImage = $this->Image_CropSquare($oImage);
		
		if ($oImage && $sFileAvatar=$this->Image_Resize($sFileTmp,$sPath,"avatar_company_{$oCompany->getUrl()}_48x48",Config::Get('view.img_max_width'),Config::Get('view.img_max_height'),48,48,false,$aParams,$oImage)) {
			$aSize=Config::Get('module.company.avatar_size');
			foreach ($aSize as $iSize) {
				if ($iSize==0) {
					$this->Image_Resize($sFileTmp,$sPath,"avatar_company_{$oCompany->getUrl()}",Config::Get('view.img_max_width'),Config::Get('view.img_max_height'),null,null,false,$aParams,$oImage);
				} else {
					$this->Image_Resize($sFileTmp,$sPath,"avatar_company_{$oCompany->getUrl()}_{$iSize}x{$iSize}",Config::Get('view.img_max_width'),Config::Get('view.img_max_height'),$iSize,$iSize,false,$aParams,$oImage);
				}
			}
			@unlink($sFileTmp);
			/**
			 * Если все нормально, возвращаем расширение загруженного аватара
			 */
			return $this->Image_GetWebPath($sFileAvatar);
		}
		@unlink($sFileTmp);
		/**
		 * В случае ошибки, возвращаем false
		 */
		return false;
	}
	/**
	 * Delete company avatar from server
	 *
	 * @param ModuleCompany_EntityCompany $oUser
	 */
	public function DeleteCompanyAvatar($oCompany) {
		/**
		 * Если аватар есть, удаляем его и его рейсайзы
		 */
		if($oCompany->getAvatar()) {		
			$aSize=array_merge(Config::Get('module.company.avatar_size'),array(48));
			foreach ($aSize as $iSize) {
				@unlink($this->Image_GetServerPath($oCompany->getAvatarPath($iSize)));
			}		
		}
	}	
}
?>