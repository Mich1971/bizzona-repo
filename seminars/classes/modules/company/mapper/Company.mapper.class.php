<?php
/*-------------------------------------------------------
*
*   LiveStreet Engine Social Networking
*   Copyright © 2008 Mzhelskiy Maxim
*
*--------------------------------------------------------
*
*   Official site: www.livestreet.ru
*   Contact e-mail: rus.engine@gmail.com
*
*   GNU General Public License, version 2:
*   http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
*
---------------------------------------------------------
*/

class ModuleCompany_MapperCompany extends Mapper {	
	protected $oUserCurrent=null;
	
	public function SetUserCurrent($oUserCurrent)  {
		$this->oUserCurrent=$oUserCurrent;
	}
	
	public function AddCompany(ModuleCompany_EntityCompany $oCompany) {
		$sql = "INSERT INTO ".Config::Get('db.table.company')." 
			(user_owner_id,
			company_title,
			company_description,
			company_type,			
			company_date_add,
			company_limit_rating_seminar,
			company_url,
			company_avatar
			)
			VALUES(?d,  ?,	?,	?,	?,	?, ?, ?)
		";			
		if ($iId=$this->oDb->query($sql,$oCompany->getOwnerId(),$oCompany->getTitle(),$oCompany->getDescription(),$oCompany->getType(),$oCompany->getDateAdd(),$oCompany->getLimitRatingSeminar(),$oCompany->getUrl(),$oCompany->getAvatar())) {
			return $iId;
		}		
		return false;
	}
	
	public function UpdateCompany(ModuleCompany_EntityCompany $oCompany) {		
		$sql = "UPDATE ".Config::Get('db.table.company')." 
			SET 
				company_title= ?,
				company_description= ?,
				company_type= ?,
				company_date_edit= ?,
				company_rating= ?f,
				company_count_vote = ?d,
				company_count_user= ?d,
				company_limit_rating_seminar= ?f ,
				company_url= ?,
				company_avatar= ?
			WHERE
				company_id = ?d
		";			
		if ($this->oDb->query($sql,$oCompany->getTitle(),$oCompany->getDescription(),$oCompany->getType(),$oCompany->getDateEdit(),$oCompany->getRating(),$oCompany->getCountVote(),$oCompany->getCountUser(),$oCompany->getLimitRatingSeminar(),$oCompany->getUrl(),$oCompany->getAvatar(),$oCompany->getId())) {
			return true;
		}		
		return false;
	}
	
	public function GetCompanysByArrayId($aArrayId) {
		if (!is_array($aArrayId) or count($aArrayId)==0) {
			return array();
		}
				
		$sql = "SELECT 
					b.*							 
				FROM 
					".Config::Get('db.table.company')." as b					
				WHERE 
					b.company_id IN(?a) 								
				ORDER BY FIELD(b.company_id,?a) ";
		$aCompanys=array();
		if ($aRows=$this->oDb->select($sql,$aArrayId,$aArrayId)) {
			foreach ($aRows as $aCompany) {
				$aCompanys[]=Engine::GetEntity('Company',$aCompany);
			}
		}
		return $aCompanys;
	}	
	
	public function AddRelationCompanyUser(ModuleCompany_EntityCompanyUser $oCompanyUser) {
		$sql = "INSERT INTO ".Config::Get('db.table.company_user')." 
			(company_id,
			user_id,
			user_role
			)
			VALUES(?d,  ?d, ?d)
		";			
		if ($this->oDb->query($sql,$oCompanyUser->getCompanyId(),$oCompanyUser->getUserId(),$oCompanyUser->getUserRole())===0) {
			return true;
		}		
		return false;
	}
	
	public function DeleteRelationCompanyUser(ModuleCompany_EntityCompanyUser $oCompanyUser) {
		$sql = "DELETE FROM ".Config::Get('db.table.company_user')." 
			WHERE
				company_id = ?d
				AND
				user_id = ?d
		";			
		if ($this->oDb->query($sql,$oCompanyUser->getCompanyId(),$oCompanyUser->getUserId())) {
			return true;
		}		
		return false;
	}
		
	public function UpdateRelationCompanyUser(ModuleCompany_EntityCompanyUser $oCompanyUser) {		
		$sql = "UPDATE ".Config::Get('db.table.company_user')." 
			SET 
				user_role = ?d			
			WHERE
				company_id = ?d 
				AND
				user_id = ?d
		";			
		if ($this->oDb->query($sql,$oCompanyUser->getUserRole(),$oCompanyUser->getCompanyId(),$oCompanyUser->getUserId())) {
			return true;
		}		
		return false;
	}
	
	public function GetCompanyUsers($aFilter) {
		$sWhere=' 1=1 ';
		if (isset($aFilter['company_id'])) {
			$sWhere.=" AND bu.company_id =  ".(int)$aFilter['company_id'];
		}
		if (isset($aFilter['user_id'])) {
			$sWhere.=" AND bu.user_id =  ".(int)$aFilter['user_id'];
		}
		if (isset($aFilter['user_role'])) {
			if(!is_array($aFilter['user_role'])) {
				$aFilter['user_role']=array($aFilter['user_role']);
			}
			$sWhere.=" AND bu.user_role IN ('".join("', '",$aFilter['user_role'])."')";		
		} else {
			$sWhere.=" AND bu.user_role>".ModuleCompany::COMPANY_USER_ROLE_GUEST;
		}
		
		$sql = "SELECT
					bu.*				
				FROM 
					".Config::Get('db.table.company_user')." as bu
				WHERE 
					".$sWhere." 					
				;
					";		
		$aCompanyUsers=array();
		if ($aRows=$this->oDb->select($sql)) {
			foreach ($aRows as $aUser) {
				$aCompanyUsers[]=Engine::GetEntity('Company_CompanyUser',$aUser);
			}
		}
		return $aCompanyUsers;
	}

	public function GetCompanyUsersByArrayCompany($aArrayId,$sUserId) {	
		if (!is_array($aArrayId) or count($aArrayId)==0) {
			return array();
		}
			
		$sql = "SELECT 
					bu.*				
				FROM 
					".Config::Get('db.table.company_user')." as bu
				WHERE 
					bu.company_id IN(?a) 					
					AND
					bu.user_id = ?d ";		
		$aCompanyUsers=array();
		if ($aRows=$this->oDb->select($sql,$aArrayId,$sUserId)) {
			foreach ($aRows as $aUser) {
				$aCompanyUsers[]=Engine::GetEntity('Company_CompanyUser',$aUser);
			}
		}
		return $aCompanyUsers;
	}
	
		
	public function GetPersonalCompanyByUserId($sUserId) {
		$sql = "SELECT company_id FROM ".Config::Get('db.table.company')." WHERE user_owner_id = ?d and company_type='personal'";
		if ($aRow=$this->oDb->selectRow($sql,$sUserId)) {
			return $aRow['company_id'];
		}
		return null;
	}
	
		
	public function GetCompanyByTitle($sTitle) {
		$sql = "SELECT company_id FROM ".Config::Get('db.table.company')." WHERE company_title = ? ";
		if ($aRow=$this->oDb->selectRow($sql,$sTitle)) {
			return $aRow['company_id'];
		}
		return null;
	}
	

	
	
	public function GetCompanyByUrl($sUrl) {		
		$sql = "SELECT 
				b.company_id 
			FROM 
				".Config::Get('db.table.company')." as b
			WHERE 
				b.company_url = ? 		
				";
		if ($aRow=$this->oDb->selectRow($sql,$sUrl)) {
			return $aRow['company_id'];
		}
		return null;
	}
	
	public function GetCompanysByOwnerId($sUserId) {
		$sql = "SELECT 
			b.company_id			 
			FROM 
				".Config::Get('db.table.company')." as b				
			WHERE 
				b.user_owner_id = ?  ";	
		$aCompanys=array();
		if ($aRows=$this->oDb->select($sql,$sUserId)) {
			foreach ($aRows as $aCompany) {
				$aCompanys[]=$aCompany['company_id'];
			}
		}
		return $aCompanys;
	}
	
	public function GetCompanys() {
		$sql = "SELECT 
			b.company_id			 
			FROM 
				".Config::Get('db.table.company')." as b				
			WHERE 				
				b.company_type<>'personal'				
				";	
		$aCompanys=array();
		if ($aRows=$this->oDb->select($sql)) {
			foreach ($aRows as $aCompany) {
				$aCompanys[]=$aCompany['company_id'];
			}
		}
		return $aCompanys;
	}
		
	public function GetCompanysRating(&$iCount,$iCurrPage,$iPerPage) {		
		$sql = "SELECT 
					b.company_id													
				FROM 
					".Config::Get('db.table.company')." as b 									 
				WHERE 									
					b.company_type<>'personal'									
				ORDER by b.company_rating desc
				LIMIT ?d, ?d 	";		
		$aReturn=array();
		if ($aRows=$this->oDb->selectPage($iCount,$sql,($iCurrPage-1)*$iPerPage, $iPerPage)) {
			foreach ($aRows as $aRow) {
				$aReturn[]=$aRow['company_id'];
			}
		}
		return $aReturn;
	}
	
	public function GetCompanysRatingJoin($sUserId,$iLimit) {		
		$sql = "SELECT 
					b.*													
				FROM 
					".Config::Get('db.table.company_user')." as bu,
					".Config::Get('db.table.company')." as b	
				WHERE 	
					bu.user_id = ?d
					AND
					bu.company_id = b.company_id
					AND				
					b.company_type<>'personal'							
				ORDER by b.company_rating desc
				LIMIT 0, ?d 
				;	
					";		
		$aReturn=array();
		if ($aRows=$this->oDb->select($sql,$sUserId,$iLimit)) {
			foreach ($aRows as $aRow) {
				$aReturn[]=Engine::GetEntity('Company',$aRow);
			}
		}
		return $aReturn;
	}
	
	public function GetCompanysRatingSelf($sUserId,$iLimit) {		
		$sql = "SELECT 
					b.*													
				FROM 					
					".Config::Get('db.table.company')." as b	
				WHERE 						
					b.user_owner_id = ?d
					AND				
					b.company_type<>'personal'													
				ORDER by b.company_rating desc
				LIMIT 0, ?d 
			;";		
		$aReturn=array();
		if ($aRows=$this->oDb->select($sql,$sUserId,$iLimit)) {
			foreach ($aRows as $aRow) {
				$aReturn[]=Engine::GetEntity('Company',$aRow);
			}
		}
		return $aReturn;
	}
	
	public function GetCloseCompanys() {
		$sql = "SELECT b.company_id										
				FROM ".Config::Get('db.table.company')." as b					
				WHERE b.company_type='close'
			;";
		$aReturn=array();
		if ($aRows=$this->oDb->select($sql)) {
			foreach ($aRows as $aRow) {
				$aReturn[]=$aRow['company_id'];
			}
		}
		return $aReturn;
	}
	
	/**
	 * Удаление блога из базы данных
	 *
	 * @param  int  $iCompanyId
	 * @return bool	 
	 */
	public function DeleteCompany($iCompanyId) {
		$sql = "
			DELETE FROM ".Config::Get('db.table.company')." 
			WHERE company_id = ?d				
		";			
		if ($this->oDb->query($sql,$iCompanyId)) {
			return true;
		}
		return false;
	}
	
	/**
	 * Удалить пользователей блога по идентификатору блога
	 *
	 * @param  int  $iCompanyId
	 * @return bool
	 */
	public function DeleteCompanyUsersByCompanyId($iCompanyId) {
		$sql = "
			DELETE FROM ".Config::Get('db.table.company_user')." 
			WHERE company_id = ?d
		";
		if ($this->oDb->query($sql,$iCompanyId)) {
			return true;
		}
		return false;
	}
}
?>