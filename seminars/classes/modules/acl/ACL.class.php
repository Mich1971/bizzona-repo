<?php
class ModuleACL extends Module {

	const CAN_VOTE_BLOG_FALSE = 0;	
	const CAN_VOTE_BLOG_TRUE = 1;
	const CAN_VOTE_BLOG_ERROR_CLOSE = 2;

	const CAN_DELETE_BLOG_EMPTY_ONLY  = 1;
	const CAN_DELETE_BLOG_WITH_TOPICS = 2;
	
	public function Init() {		
		
	}
		
	public function CanCreateCompany(ModuleUser_EntityUser $oUser) {
		if ($oUser->getRating()>=Config::Get('acl.create.company.rating')) {
			return true;
		}
		return false;
	}
	
	public function CanAddSeminar(ModuleUser_EntityUser $oUser, ModuleCompany_EntityCompany $oCompany) {
		if ($oUser->getId()==$oCompany->getOwnerId()) {
			return true;
		}
		if ($oUser->getRating()>=$oCompany->getLimitRatingSeminar()) {
			return true;
		}
		return false;
	}

	public function CanPostComment(ModuleUser_EntityUser $oUser) {
		if ($oUser->getRating()>=Config::Get('acl.create.comment.rating')) {
			return true;
		}
		return false;
	}
	
	public function CanPostCommentTime(ModuleUser_EntityUser $oUser) {
		if (Config::Get('acl.create.comment.limit_time')>0 and $oUser->getDateCommentLast()) {
			$sDateCommentLast=strtotime($oUser->getDateCommentLast());
			if ($oUser->getRating()<Config::Get('acl.create.comment.limit_time_rating') and ((time()-$sDateCommentLast)<Config::Get('acl.create.comment.limit_time'))) {
				return false;
			}
		}
		return true;
	}

	public function CanPostSeminarTime(ModuleUser_EntityUser $oUser) {
		if($oUser->isAdministrator() 
			or Config::Get('acl.create.seminar.limit_time')==0 
				or $oUser->getRating()>=Config::Get('acl.create.seminar.limit_time_rating')) 
					return true;
		
		$aSeminars=$this->Seminar_GetLastSeminarsByUserId($oUser->getId(),Config::Get('acl.create.seminar.limit_time'));
		
		if(isset($aSeminars['count']) and $aSeminars['count']>0){ 
			return false;
		}
		return true;
	}

	public function CanSendTalkTime(ModuleUser_EntityUser $oUser) {
		if($oUser->isAdministrator() 
			or Config::Get('acl.create.talk.limit_time')==0 
				or $oUser->getRating()>=Config::Get('acl.create.talk.limit_time_rating')) 
					return true;
		
		$aTalks=$this->Talk_GetLastTalksByUserId($oUser->getId(),Config::Get('acl.create.talk.limit_time'));
		
		if(isset($aTalks['count']) and $aTalks['count']>0){ 
			return false;
		}
		return true;
	}	

	public function CanPostTalkCommentTime(ModuleUser_EntityUser $oUser) {
		if($oUser->isAdministrator() 
			or Config::Get('acl.create.talk_comment.limit_time')==0 
				or $oUser->getRating()>=Config::Get('acl.create.talk_comment.limit_time_rating')) 
					return true;
		$aTalkComments=$this->Comment_GetCommentsByUserId($oUser->getId(),'talk',1,1);
		if(!is_array($aTalkComments) or $aTalkComments['count']==0){ 
			return true;
		}

		$oComment = array_shift($aTalkComments['collection']);
		$sDate = strtotime($oComment->getDate());
		
		if($sDate and ((time()-$sDate)<Config::Get('acl.create.talk_comment.limit_time'))) {
			return false;
		}
		return true;
	}
	
	public function CanUseHtmlInComment(ModuleUser_EntityUser $oUser) {
		return true;
	}
	
	public function CanVoteComment(ModuleUser_EntityUser $oUser, ModuleComment_EntityComment $oComment) {
		if ($oUser->getRating()>=Config::Get('acl.vote.comment.rating')) {
			return true;
		}
		return false;
	}
	
	public function CanVoteCompany(ModuleUser_EntityUser $oUser, ModuleCompany_EntityCompany $oCompany) {
		if($oCompany->getType()=='close') {
			$oCompanyUser = $this->Company_GetCompanyUserByCompanyIdAndUserId($oCompany->getId(),$oUser->getId());
			if(!$oCompanyUser || $oCompanyUser->getUserRole()<ModuleCompany::BLOG_USER_ROLE_GUEST) {
				return self::CAN_VOTE_BLOG_ERROR_CLOSE;
			}
		}
		
		if ($oUser->getRating()>=Config::Get('acl.vote.company.rating')) {
			return self::CAN_VOTE_BLOG_TRUE;
		}
		return self::CAN_VOTE_BLOG_FALSE;
	}
	
	public function CanVoteSeminar(ModuleUser_EntityUser $oUser, ModuleSeminar_EntitySeminar $oSeminar) {
		if ($oUser->getRating()>=Config::Get('acl.vote.seminar.rating')) {
			return true;
		}
		return false;
	}
	
	public function CanVoteUser(ModuleUser_EntityUser $oUser, ModuleUser_EntityUser $oUserTarget) {
		if ($oUser->getRating()>=Config::Get('acl.vote.user.rating')) {
			return true;
		}
		return false;
	}

	public function CanSendInvite(ModuleUser_EntityUser $oUser) {
		if ($this->User_GetCountInviteAvailable($oUser)==0) {
			return false;
		}
		return true;
	}
	
	public function IsAllowCompany($oCompany,$oUser) {		
		if ($oUser->isAdministrator()) {
			return true;
		}
		if ($oCompany->getOwnerId()==$oUser->getId()) {
			return true;
		}
		if ($oCompanyUser=$this->Company_GetCompanyUserByCompanyIdAndUserId($oCompany->getId(),$oUser->getId())) {
			if ($this->ACL_CanAddSeminar($oUser,$oCompany) or $oCompanyUser->getIsAdministrator() or $oCompanyUser->getIsModerator()) {
				return true;
			}
		}
		return false;
	}	
	
	
	public function IsAllowEditSeminar($oSeminar,$oUser) {
		if ($oSeminar->getUserId()==$oUser->getId() or $oUser->isAdministrator()) {
			return true;
		}
		if ($oSeminar->getCompany()->getOwnerId()==$oUser->getId()) {
			return true;
		}
		$oCompanyUser=$this->Company_GetCompanyUserByCompanyIdAndUserId($oSeminar->getCompanyId(),$oUser->getId());
		if ($oCompanyUser and ($oCompanyUser->getIsModerator() or $oCompanyUser->getIsAdministrator())) {
			return true;
		}
		return false;
	}	
	
	public function IsAllowDeleteSeminar($oSeminar,$oUser) {		
		if ($oSeminar->getUserId()==$oUser->getId() or $oUser->isAdministrator()) {			
			return true;
		}
		if ($oSeminar->getCompany()->getOwnerId()==$oUser->getId()) {
			return true;
		}
		$oCompanyUser=$this->Company_GetCompanyUserByCompanyIdAndUserId($oSeminar->getCompanyId(),$oUser->getId());
		if ($oCompanyUser and ($oCompanyUser->getIsModerator() or $oCompanyUser->getIsAdministrator())) {
			return true;
		}
		return false;
	}
	
	public function IsAllowDeleteCompany($oCompany,$oUser) {		
		if ($oUser->isAdministrator()) {
			return self::CAN_DELETE_BLOG_WITH_TOPICS;
		}
		if($oCompany->getOwnerId()==$oUser->getId()) {
			return self::CAN_DELETE_BLOG_EMPTY_ONLY;
		}
		
		$oCompanyUser=$this->Company_GetCompanyUserByCompanyIdAndUserId($oCompany->getId(),$this->oUserCurrent->getId());		
		if($oCompanyUser and $oCompanyUser->getIsAdministrator()) {
			return self::CAN_DELETE_BLOG_EMPTY_ONLY;			
		}
		
		return false;
	}		
}
?>