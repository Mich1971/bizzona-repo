<?php
/*-------------------------------------------------------
*
*   LiveStreet Engine Social Networking
*   Copyright © 2008 Mzhelskiy Maxim
*
*--------------------------------------------------------
*
*   Official site: www.livestreet.ru
*   Contact e-mail: rus.engine@gmail.com
*
*   GNU General Public License, version 2:
*   http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
*
---------------------------------------------------------
*/

class ModuleSeminar_MapperSeminar extends Mapper {	
		
	public function AddSeminar(ModuleSeminar_EntitySeminar $oSeminar) {
		$sql = "INSERT INTO ".Config::Get('db.table.seminar')." 
			(company_id,
			user_id,
			seminar_type,
			seminar_title,			
			seminar_tags,
			seminar_date_add,
			seminar_user_ip,
			seminar_publish,
			seminar_publish_draft,
			seminar_publish_index,
			seminar_cut_text,
			seminar_forbid_comment,			
			seminar_text_hash			
			)
			VALUES(?d,  ?d,	?,	?,	?,  ?, ?, ?d, ?d, ?d, ?, ?, ?)
		";			
		if ($iId=$this->oDb->query($sql,$oSeminar->getCompanyId(),$oSeminar->getUserId(),$oSeminar->getType(),$oSeminar->getTitle(),
			$oSeminar->getTags(),$oSeminar->getDateAdd(),$oSeminar->getUserIp(),$oSeminar->getPublish(),$oSeminar->getPublishDraft(),$oSeminar->getPublishIndex(),$oSeminar->getCutText(),$oSeminar->getForbidComment(),$oSeminar->getTextHash())) 
		{
			$oSeminar->setId($iId);
			$this->AddSeminarContent($oSeminar);
			return $iId;
		}		
		return false;
	}
	
	public function AddSeminarContent(ModuleSeminar_EntitySeminar $oSeminar) {
		$sql = "INSERT INTO ".Config::Get('db.table.seminar_content')." 
			(seminar_id,			
			seminar_text,
			seminar_text_short,
			seminar_text_source,
			seminar_extra			
			)
			VALUES(?d,  ?,	?,	?, ? )
		";			
		if ($iId=$this->oDb->query($sql,$oSeminar->getId(),$oSeminar->getText(),
			$oSeminar->getTextShort(),$oSeminar->getTextSource(),$oSeminar->getExtra())) 
		{
			return $iId;
		}		
		return false;
	}
	
	public function AddSeminarTag(ModuleSeminar_EntitySeminarTag $oSeminarTag) {
		$sql = "INSERT INTO ".Config::Get('db.table.seminar_tag')." 
			(seminar_id,
			user_id,
			company_id,
			seminar_tag_text		
			)
			VALUES(?d,  ?d,  ?d,	?)
		";			
		if ($iId=$this->oDb->query($sql,$oSeminarTag->getSeminarId(),$oSeminarTag->getUserId(),$oSeminarTag->getCompanyId(),$oSeminarTag->getText())) 
		{
			return $iId;
		}		
		return false;
	}
	
	
	
	public function DeleteSeminarTagsBySeminarId($sSeminarId) {
		$sql = "DELETE FROM ".Config::Get('db.table.seminar_tag')." 
			WHERE
				seminar_id = ?d				
		";			
		if ($this->oDb->query($sql,$sSeminarId)) {
			return true;
		}		
		return false;
	}
	
	public function DeleteSeminar($sSeminarId) {
		$sql = "DELETE FROM ".Config::Get('db.table.seminar')." 
			WHERE
				seminar_id = ?d				
		";			
		if ($this->oDb->query($sql,$sSeminarId)) {
			return true;
		}		
		return false;
	}
	
		
	public function GetSeminarUnique($sUserId,$sHash) {
		$sql = "SELECT seminar_id FROM ".Config::Get('db.table.seminar')." 
			WHERE 				
				seminar_text_hash =? 						
				AND
				user_id = ?d
			LIMIT 0,1
				";
		if ($aRow=$this->oDb->selectRow($sql,$sHash,$sUserId)) {
			return $aRow['seminar_id'];
		}
		return null;
	}
	
	public function GetSeminarsByArrayId($aArrayId) {
		if (!is_array($aArrayId) or count($aArrayId)==0) {
			return array();
		}
				
		$sql = "SELECT 
					t.*,
					tc.*							 
				FROM 
					".Config::Get('db.table.seminar')." as t	
					JOIN  ".Config::Get('db.table.seminar_content')." AS tc ON t.seminar_id=tc.seminar_id				
				WHERE 
					t.seminar_id IN(?a) 									
				ORDER BY FIELD(t.seminar_id,?a) ";
		$aSeminars=array();
		if ($aRows=$this->oDb->select($sql,$aArrayId,$aArrayId)) {
			foreach ($aRows as $aSeminar) {
				$aSeminars[]=Engine::GetEntity('Seminar',$aSeminar);
			}
		}		
		return $aSeminars;
	}
	
	
	public function GetSeminars($aFilter,&$iCount,$iCurrPage,$iPerPage) {
		$sWhere=$this->buildFilter($aFilter);
		
		if(isset($aFilter['order']) and !is_array($aFilter['order'])) {
			$aFilter['order'] = array($aFilter['order']);
		} else {
			$aFilter['order'] = array('t.seminar_date_add desc');
		}
		
		$sql = "SELECT 
						t.seminar_id							
					FROM 
						".Config::Get('db.table.seminar')." as t,	
						".Config::Get('db.table.company')." as b			
					WHERE 
						1=1					
						".$sWhere."
						AND
						t.company_id=b.company_id										
					ORDER BY ".
						implode(', ', $aFilter['order'])
				."
					LIMIT ?d, ?d";		
		$aSeminars=array();
		if ($aRows=$this->oDb->selectPage($iCount,$sql,($iCurrPage-1)*$iPerPage, $iPerPage)) {			
			foreach ($aRows as $aSeminar) {
				$aSeminars[]=$aSeminar['seminar_id'];
			}
		}				
		return $aSeminars;
	}
	
	public function GetCountSeminars($aFilter) {		
		$sWhere=$this->buildFilter($aFilter);
		$sql = "SELECT 
					count(t.seminar_id) as count									
				FROM 
					".Config::Get('db.table.seminar')." as t,					
					".Config::Get('db.table.company')." as b
				WHERE 
					1=1
					
					".$sWhere."								
					
					AND
					t.company_id=b.company_id;";		
		if ($aRow=$this->oDb->selectRow($sql)) {
			return $aRow['count'];
		}
		return false;
	}
	
	public function GetAllSeminars($aFilter) {
		$sWhere=$this->buildFilter($aFilter);
		
		$sql = "SELECT 
						t.seminar_id							
					FROM 
						".Config::Get('db.table.seminar')." as t,	
						".Config::Get('db.table.company')." as b			
					WHERE 
						1=1					
						".$sWhere."
						AND
						t.company_id=b.company_id										
					ORDER by t.seminar_id desc";		
		$aSeminars=array();
		if ($aRows=$this->oDb->select($sql)) {			
			foreach ($aRows as $aSeminar) {
				$aSeminars[]=$aSeminar['seminar_id'];
			}
		}		

		return $aSeminars;		
	}
	
	public function GetSeminarsByTag($sTag,$aExcludeCompany,&$iCount,$iCurrPage,$iPerPage) {		
		$sql = "				
							SELECT 		
								seminar_id										
							FROM 
								".Config::Get('db.table.seminar_tag')."								
							WHERE 
								seminar_tag_text = ? 	
								{ AND company_id NOT IN (?a) }
                            ORDER BY seminar_id DESC	
                            LIMIT ?d, ?d ";
		
		$aSeminars=array();
		if ($aRows=$this->oDb->selectPage(
				$iCount,$sql,$sTag,
				(is_array($aExcludeCompany)&&count($aExcludeCompany)) ? $aExcludeCompany : DBSIMPLE_SKIP,
				($iCurrPage-1)*$iPerPage, $iPerPage
			)
		) {
			foreach ($aRows as $aSeminar) {
				$aSeminars[]=$aSeminar['seminar_id'];
			}
		}
		return $aSeminars;
	}
	
	
	public function GetSeminarsRatingByDate($sDate,$iLimit,$aExcludeCompany=array()) {
		$sql = "SELECT 
						t.seminar_id										
					FROM 
						".Config::Get('db.table.seminar')." as t
					WHERE 					
						t.seminar_publish = 1
						AND
						t.seminar_date_add >= ?
						AND
						t.seminar_rating >= 0
						{ AND t.company_id NOT IN(?a) } 																	
					ORDER by t.seminar_rating desc, t.seminar_id desc
					LIMIT 0, ?d ";		
		$aSeminars=array();
		if ($aRows=$this->oDb->select(
				$sql,$sDate,
				(is_array($aExcludeCompany)&&count($aExcludeCompany)) ? $aExcludeCompany : DBSIMPLE_SKIP,
				$iLimit
			)
		) {
			foreach ($aRows as $aSeminar) {
				$aSeminars[]=$aSeminar['seminar_id'];
			}
		}
		return $aSeminars;
	}
	
	public function GetSeminarTags($iLimit,$aExcludeSeminar=array()) {
		$sql = "SELECT 
			tt.seminar_tag_text,
			count(tt.seminar_tag_text)	as count		 
			FROM 
				".Config::Get('db.table.seminar_tag')." as tt
			WHERE 
				1=1
				{AND tt.seminar_id NOT IN(?a) }		
			GROUP BY 
				tt.seminar_tag_text
			ORDER BY 
				count desc		
			LIMIT 0, ?d
				";	
		$aReturn=array();
		$aReturnSort=array();
		if ($aRows=$this->oDb->select(
				$sql,
				(is_array($aExcludeSeminar)&&count($aExcludeSeminar)) ? $aExcludeSeminar : DBSIMPLE_SKIP,
				$iLimit
			)
		) {
			foreach ($aRows as $aRow) {				
				$aReturn[mb_strtolower($aRow['seminar_tag_text'],'UTF-8')]=$aRow;
			}
			ksort($aReturn);
			foreach ($aReturn as $aRow) {
				$aReturnSort[]=Engine::GetEntity('Seminar_SeminarTag',$aRow);				
			}
		}
		return $aReturnSort;
	}

	public function GetOpenSeminarTags($iLimit) {
		$sql = "
			SELECT 
				tt.seminar_tag_text,
				count(tt.seminar_tag_text)	as count		 
			FROM 
				".Config::Get('db.table.seminar_tag')." as tt,
				".Config::Get('db.table.company')." as b
			WHERE 
				tt.company_id = b.company_id
				AND
				b.company_type IN ('open','personal')
			GROUP BY 
				tt.seminar_tag_text
			ORDER BY 
				count desc		
			LIMIT 0, ?d
				";	
		$aReturn=array();
		$aReturnSort=array();
		if ($aRows=$this->oDb->select($sql,$iLimit)) {
			foreach ($aRows as $aRow) {				
				$aReturn[mb_strtolower($aRow['seminar_tag_text'],'UTF-8')]=$aRow;
			}
			ksort($aReturn);
			foreach ($aReturn as $aRow) {
				$aReturnSort[]=Engine::GetEntity('Seminar_SeminarTag',$aRow);				
			}
		}
		return $aReturnSort;
	}

	
	public function increaseSeminarCountComment($sSeminarId) {
		$sql = "UPDATE ".Config::Get('db.table.seminar')." 
			SET 
				seminar_count_comment=seminar_count_comment+1
			WHERE
				seminar_id = ?
		";			
		if ($this->oDb->query($sql,$sSeminarId)) {
			return true;
		}		
		return false;
	}
	
	public function UpdateSeminar(ModuleSeminar_EntitySeminar $oSeminar) {		
		$sql = "UPDATE ".Config::Get('db.table.seminar')." 
			SET 
				company_id= ?d,
				seminar_title= ?,				
				seminar_tags= ?,
				seminar_date_add = ?,
				seminar_date_edit = ?,
				seminar_user_ip= ?,
				seminar_publish= ?d ,
				seminar_publish_draft= ?d ,
				seminar_publish_index= ?d,
				seminar_rating= ?f,
				seminar_count_vote= ?d,
				seminar_count_read= ?d,
				seminar_count_comment= ?d, 
				seminar_cut_text = ? ,
				seminar_forbid_comment = ? ,
				seminar_text_hash = ? 
			WHERE
				seminar_id = ?d
		";			
		if ($this->oDb->query($sql,$oSeminar->getCompanyId(),$oSeminar->getTitle(),$oSeminar->getTags(),$oSeminar->getDateAdd(),$oSeminar->getDateEdit(),$oSeminar->getUserIp(),$oSeminar->getPublish(),$oSeminar->getPublishDraft(),$oSeminar->getPublishIndex(),$oSeminar->getRating(),$oSeminar->getCountVote(),$oSeminar->getCountRead(),$oSeminar->getCountComment(),$oSeminar->getCutText(),$oSeminar->getForbidComment(),$oSeminar->getTextHash(),$oSeminar->getId())) {
			$this->UpdateSeminarContent($oSeminar);
			return true;
		}		
		return false;
	}
	
	public function UpdateSeminarContent(ModuleSeminar_EntitySeminar $oSeminar) {		
		$sql = "UPDATE ".Config::Get('db.table.seminar_content')." 
			SET 				
				seminar_text= ?,
				seminar_text_short= ?,
				seminar_text_source= ?,
				seminar_extra= ?
			WHERE
				seminar_id = ?d
		";			
		if ($this->oDb->query($sql,$oSeminar->getText(),$oSeminar->getTextShort(),$oSeminar->getTextSource(),$oSeminar->getExtra(),$oSeminar->getId())) {
			return true;
		}		
		return false;
	}
	
	protected function buildFilter($aFilter) {
		$sWhere='';
		if (isset($aFilter['seminar_publish'])) {
			$sWhere.=" AND t.seminar_publish =  ".(int)$aFilter['seminar_publish'];
		}	
		if (isset($aFilter['seminar_rating']) and is_array($aFilter['seminar_rating'])) {
			$sPublishIndex='';
			if (isset($aFilter['seminar_rating']['publish_index']) and $aFilter['seminar_rating']['publish_index']==1) {
				$sPublishIndex=" or seminar_publish_index=1 ";
			}
			if ($aFilter['seminar_rating']['type']=='top') {
				$sWhere.=" AND ( t.seminar_rating >= ".(float)$aFilter['seminar_rating']['value']." {$sPublishIndex} ) ";
			} else {
				$sWhere.=" AND ( t.seminar_rating < ".(float)$aFilter['seminar_rating']['value']."  ) ";
			}			
		}
		if (isset($aFilter['seminar_new'])) {
			$sWhere.=" AND t.seminar_date_add >=  '".$aFilter['seminar_new']."'";
		}
		if (isset($aFilter['user_id'])) {
			$sWhere.=is_array($aFilter['user_id'])
				? " AND t.user_id IN(".implode(', ',$aFilter['user_id']).")"
				: " AND t.user_id =  ".(int)$aFilter['user_id'];
		}
		if (isset($aFilter['company_id'])) {
			if(!is_array($aFilter['company_id'])) {
				$aFilter['company_id']=array($aFilter['company_id']);
			}
			$sWhere.=" AND t.company_id IN ('".join("','",$aFilter['company_id'])."')";
		}
		if (isset($aFilter['company_type']) and is_array($aFilter['company_type'])) {
			$aCompanyTypes = array();
			foreach ($aFilter['company_type'] as $sType=>$aCompanyId) {
				/**
				 * Позиция вида 'type'=>array('id1', 'id2')
				 */
				if(!is_array($aCompanyId) && is_string($sType)){
					$aCompanyId=array($aCompanyId);
				}
				/**
				 * Позиция вида 'type'
				 */
				if(is_string($aCompanyId) && is_int($sType)) {
					$sType=$aCompanyId;
					$aCompanyId=array();
				}
				
				$aCompanyTypes[] = (count($aCompanyId)==0) 
					? "(b.company_type='".$sType."')"
					: "(b.company_type='".$sType."' AND t.company_id IN ('".join("','",$aCompanyId)."'))";
			}
			$sWhere.=" AND (".join(" OR ",(array)$aCompanyTypes).")";
		}
		return $sWhere;
	}
	
	public function GetSeminarTagsByLike($sTag,$iLimit) {
		$sTag=mb_strtolower($sTag,"UTF-8");		
		$sql = "SELECT 
				seminar_tag_text					 
			FROM 
				".Config::Get('db.table.seminar_tag')."	
			WHERE
				seminar_tag_text LIKE ?			
			GROUP BY 
				seminar_tag_text					
			LIMIT 0, ?d		
				";	
		$aReturn=array();
		if ($aRows=$this->oDb->select($sql,$sTag.'%',$iLimit)) {
			foreach ($aRows as $aRow) {
				$aReturn[]=Engine::GetEntity('Seminar_SeminarTag',$aRow);
			}
		}
		return $aReturn;
	}
	
	public function UpdateSeminarRead(ModuleSeminar_EntitySeminarRead $oSeminarRead) {		
		$sql = "UPDATE ".Config::Get('db.table.seminar_read')." 
			SET 
				comment_count_last = ? ,
				comment_id_last = ? ,
				date_read = ? 
			WHERE
				seminar_id = ? 
				AND				
				user_id = ? 
		";			
		return $this->oDb->query($sql,$oSeminarRead->getCommentCountLast(),$oSeminarRead->getCommentIdLast(),$oSeminarRead->getDateRead(),$oSeminarRead->getSeminarId(),$oSeminarRead->getUserId());
	}	

	public function AddSeminarRead(ModuleSeminar_EntitySeminarRead $oSeminarRead) {		
		$sql = "INSERT INTO ".Config::Get('db.table.seminar_read')." 
			SET 
				comment_count_last = ? ,
				comment_id_last = ? ,
				date_read = ? ,
				seminar_id = ? ,							
				user_id = ? 
		";			
		return $this->oDb->query($sql,$oSeminarRead->getCommentCountLast(),$oSeminarRead->getCommentIdLast(),$oSeminarRead->getDateRead(),$oSeminarRead->getSeminarId(),$oSeminarRead->getUserId());
	}
	/**
	 * Удаляет записи о чтении записей по списку идентификаторов
	 *
	 * @param  array $aSeminarId
	 * @return bool
	 */				
	public function DeleteSeminarReadByArrayId($aSeminarId) {
		$sql = "
			DELETE FROM ".Config::Get('db.table.seminar_read')." 
			WHERE
				seminar_id IN(?a)				
		";			
		if ($this->oDb->query($sql,$aSeminarId)) {
			return true;
		}
		return false;
	}
			
	public function GetSeminarsReadByArray($aArrayId,$sUserId) {
		if (!is_array($aArrayId) or count($aArrayId)==0) {
			return array();
		}
				
		$sql = "SELECT 
					t.*							 
				FROM 
					".Config::Get('db.table.seminar_read')." as t 
				WHERE 
					t.seminar_id IN(?a)
					AND
					t.user_id = ?d 
				";
		$aReads=array();
		if ($aRows=$this->oDb->select($sql,$aArrayId,$sUserId)) {
			foreach ($aRows as $aRow) {
				$aReads[]=Engine::GetEntity('Seminar_SeminarRead',$aRow);
			}
		}		
		return $aReads;
	}
	
	public function AddSeminarQuestionVote(ModuleSeminar_EntitySeminarQuestionVote $oSeminarQuestionVote) {
		$sql = "INSERT INTO ".Config::Get('db.table.seminar_question_vote')." 
			(seminar_id,
			user_voter_id,
			answer		
			)
			VALUES(?d,  ?d,	?f)
		";			
		if ($this->oDb->query($sql,$oSeminarQuestionVote->getSeminarId(),$oSeminarQuestionVote->getVoterId(),$oSeminarQuestionVote->getAnswer())===0) 
		{
			return true;
		}		
		return false;
	}
	
		
	public function GetSeminarsQuestionVoteByArray($aArrayId,$sUserId) {
		if (!is_array($aArrayId) or count($aArrayId)==0) {
			return array();
		}
				
		$sql = "SELECT 
					v.*							 
				FROM 
					".Config::Get('db.table.seminar_question_vote')." as v 
				WHERE 
					v.seminar_id IN(?a)
					AND	
					v.user_voter_id = ?d 				 									
				";
		$aVotes=array();
		if ($aRows=$this->oDb->select($sql,$aArrayId,$sUserId)) {
			foreach ($aRows as $aRow) {
				$aVotes[]=Engine::GetEntity('Seminar_SeminarQuestionVote',$aRow);
			}
		}		
		return $aVotes;
	}
	
	/**
	 * Перемещает топики в другой блог
	 *
	 * @param  array  $aSeminars
	 * @param  string $sCompanyId
	 * @return bool
	 */	
	public function MoveSeminarsByArrayId($aSeminars,$sCompanyId) {
		if(!is_array($aSeminars)) $aSeminars = array($aSeminars);
		
		$sql = "UPDATE ".Config::Get('db.table.seminar')."
			SET 
				company_id= ?d
			WHERE
				seminar_id IN(?a)
		";			
		if ($this->oDb->query($sql,$sCompanyId,$aSeminars)) {
			return true;
		}		
		return false;
	}
	
	/**
	 * Перемещает топики в другой блог
	 *
	 * @param  string $sCompanyId
	 * @param  string $sCompanyIdNew
	 * @return bool
	 */	
	public function MoveSeminars($sCompanyId,$sCompanyIdNew) {
		$sql = "UPDATE ".Config::Get('db.table.seminar')."
			SET 
				company_id= ?d
			WHERE
				company_id = ?d
		";			
		if ($this->oDb->query($sql,$sCompanyIdNew,$sCompanyId)) {
			return true;
		}		
		return false;
	}
	
	/**
	 * Перемещает теги топиков в другой блог
	 *
	 * @param string $sCompanyId
	 * @param string $sCompanyIdNew
	 * @return bool
	 */
	public function MoveSeminarsTags($sCompanyId,$sCompanyIdNew) {
		$sql = "UPDATE ".Config::Get('db.table.seminar_tag')."
			SET 
				company_id= ?d
			WHERE
				company_id = ?d
		";			
		if ($this->oDb->query($sql,$sCompanyIdNew,$sCompanyId)) {
			return true;
		}		
		return false;
	}
	
	/**
	 * Перемещает теги топиков в другой блог
	 *
	 * @param array $aSeminars
	 * @param string $sCompanyId
	 * @return bool
	 */
	public function MoveSeminarsTagsByArrayId($aSeminars,$sCompanyId) {
		if(!is_array($aSeminars)) $aSeminars = array($aSeminars);
		
		$sql = "UPDATE ".Config::Get('db.table.seminar_tag')."
			SET 
				company_id= ?d
			WHERE
				seminar_id IN(?a)
		";			
		if ($this->oDb->query($sql,$sCompanyId,$aSeminars)) {
			return true;
		}		
		return false;
	}
	
}
?>