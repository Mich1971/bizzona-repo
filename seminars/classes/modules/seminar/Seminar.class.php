<?php
/*-------------------------------------------------------
*
*   LiveStreet Engine Social Networking
*   Copyright © 2008 Mzhelskiy Maxim
*
*--------------------------------------------------------
*
*   Official site: www.livestreet.ru
*   Contact e-mail: rus.engine@gmail.com
*
*   GNU General Public License, version 2:
*   http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
*
---------------------------------------------------------
*/

/**
 * Модуль для работы с топиками
 *
 */
class ModuleSeminar extends Module {		
	protected $oMapperSeminar;
	protected $oUserCurrent=null;
		
	/**
	 * �?нициализация
	 *
	 */
	public function Init() {		
		$this->oMapperSeminar=Engine::GetMapper(__CLASS__);
		$this->oUserCurrent=$this->User_GetUserCurrent();
	}
	/**
	 * Получает дополнительные данные(объекты) для топиков по их ID
	 *
	 */
	public function GetSeminarsAdditionalData($aSeminarId,$aAllowData=array('user'=>array(),'company'=>array('owner'=>array(),'relation_user'),'vote','favourite','comment_new')) {
		func_array_simpleflip($aAllowData);
		if (!is_array($aSeminarId)) {
			$aSeminarId=array($aSeminarId);
		}
		/**
		 * Получаем "голые" топики
		 */
		$aSeminars=$this->GetSeminarsByArrayId($aSeminarId);
		/**
		 * Формируем ID дополнительных данных, которые нужно получить
		 */
		$aUserId=array();
		$aCompanyId=array();
		$aSeminarIdQuestion=array();		
		foreach ($aSeminars as $oSeminar) {
			if (isset($aAllowData['user'])) {
				$aUserId[]=$oSeminar->getUserId();
			}
			if (isset($aAllowData['company'])) {
				$aCompanyId[]=$oSeminar->getCompanyId();
			}
			if ($oSeminar->getType()=='question')	{		
				$aSeminarIdQuestion[]=$oSeminar->getId();
			}
		}
		/**
		 * Получаем дополнительные данные
		 */
		$aSeminarsVote=array();
		$aFavouriteSeminars=array();
		$aSeminarsQuestionVote=array();
		$aSeminarsRead=array();
		$aUsers=isset($aAllowData['user']) && is_array($aAllowData['user']) ? $this->User_GetUsersAdditionalData($aUserId,$aAllowData['user']) : $this->User_GetUsersAdditionalData($aUserId);
		$aCompanys=isset($aAllowData['company']) && is_array($aAllowData['company']) ? $this->Company_GetCompanysAdditionalData($aCompanyId,$aAllowData['company']) : $this->Company_GetCompanysAdditionalData($aCompanyId);		
		if (isset($aAllowData['vote']) and $this->oUserCurrent) {
			$aSeminarsVote=$this->Vote_GetVoteByArray($aSeminarId,'seminar',$this->oUserCurrent->getId());
			$aSeminarsQuestionVote=$this->GetSeminarsQuestionVoteByArray($aSeminarIdQuestion,$this->oUserCurrent->getId());
		}	
		if (isset($aAllowData['favourite']) and $this->oUserCurrent) {
			$aFavouriteSeminars=$this->GetFavouriteSeminarsByArray($aSeminarId,$this->oUserCurrent->getId());	
		}
		if (isset($aAllowData['comment_new']) and $this->oUserCurrent) {
			$aSeminarsRead=$this->GetSeminarsReadByArray($aSeminarId,$this->oUserCurrent->getId());	
		}
		/**
		 * Добавляем данные к результату - списку топиков
		 */
		foreach ($aSeminars as $oSeminar) {
			if (isset($aUsers[$oSeminar->getUserId()])) {
				$oSeminar->setUser($aUsers[$oSeminar->getUserId()]);
			} else {
				$oSeminar->setUser(null); // или $oSeminar->setUser(new ModuleUser_EntityUser());
			}
			if (isset($aCompanys[$oSeminar->getCompanyId()])) {
				$oSeminar->setCompany($aCompanys[$oSeminar->getCompanyId()]);
			} else {
				$oSeminar->setCompany(null); // или $oSeminar->setCompany(new ModuleCompany_EntityCompany());
			}
			if (isset($aSeminarsVote[$oSeminar->getId()])) {
				$oSeminar->setVote($aSeminarsVote[$oSeminar->getId()]);				
			} else {
				$oSeminar->setVote(null);
			}
			if (isset($aFavouriteSeminars[$oSeminar->getId()])) {
				$oSeminar->setIsFavourite(true);
			} else {
				$oSeminar->setIsFavourite(false);
			}			
			if (isset($aSeminarsQuestionVote[$oSeminar->getId()])) {
				$oSeminar->setUserQuestionIsVote(true);
			} else {
				$oSeminar->setUserQuestionIsVote(false);
			}
			if (isset($aSeminarsRead[$oSeminar->getId()]))	{		
				$oSeminar->setCountCommentNew($oSeminar->getCountComment()-$aSeminarsRead[$oSeminar->getId()]->getCommentCountLast());
				$oSeminar->setDateRead($aSeminarsRead[$oSeminar->getId()]->getDateRead());
			} else {
				$oSeminar->setCountCommentNew(0);
				$oSeminar->setDateRead(date("Y-m-d H:i:s"));
			}						
		}
		return $aSeminars;
	}
	/**
	 * Добавляет топик
	 *
	 * @param ModuleSeminar_EntitySeminar $oSeminar
	 * @return unknown
	 */
	public function AddSeminar(ModuleSeminar_EntitySeminar $oSeminar) {
		if ($sId=$this->oMapperSeminar->AddSeminar($oSeminar)) {
			$oSeminar->setId($sId);
			if ($oSeminar->getPublish()) {
				$aTags=explode(',',$oSeminar->getTags());
				foreach ($aTags as $sTag) {
					$oTag=Engine::GetEntity('Seminar_SeminarTag');
					$oTag->setSeminarId($oSeminar->getId());
					$oTag->setUserId($oSeminar->getUserId());
					$oTag->setCompanyId($oSeminar->getCompanyId());
					$oTag->setText($sTag);
					$this->oMapperSeminar->AddSeminarTag($oTag);
				}
			}
			//чистим зависимые кеши
			$this->Cache_Clean(Zend_Cache::CLEANING_MODE_MATCHING_TAG,array('seminar_new',"seminar_update_user_{$oSeminar->getUserId()}","seminar_new_company_{$oSeminar->getCompanyId()}"));						
			return $oSeminar;
		}
		return false;
	}
	
	/**
	 * Удаляет теги у топика
	 *
	 * @param unknown_type $sSeminarId
	 * @return unknown
	 */
	public function DeleteSeminarTagsBySeminarId($sSeminarId) {
		return $this->oMapperSeminar->DeleteSeminarTagsBySeminarId($sSeminarId);
	}	
	/**
	 * Удаляет топик.
	 * Если тип таблиц в БД InnoDB, то удалятся всё связи по топику(комменты,голосования,избранное)
	 *
	 * @param unknown_type $oSeminarId|$sSeminarId
	 * @return unknown
	 */
	public function DeleteSeminar($oSeminarId) {
		if ($oSeminarId instanceof ModuleSeminar_EntitySeminar) {
			$sSeminarId=$oSeminarId->getId();
			$this->Cache_Clean(Zend_Cache::CLEANING_MODE_MATCHING_TAG,array("seminar_update_user_{$oSeminarId->getUserId()}"));
		} else {
			$sSeminarId=$oSeminarId;
		}
		/**
		 * Чистим зависимые кеши
		 */
		$this->Cache_Clean(Zend_Cache::CLEANING_MODE_MATCHING_TAG,array('seminar_update'));
		$this->Cache_Delete("seminar_{$sSeminarId}");
		/**
		 * Если топик успешно удален, удаляем связанные данные
		 */
		if($bResult=$this->oMapperSeminar->DeleteSeminar($sSeminarId)){
			return $this->DeleteSeminarAdditionalData($sSeminarId);
		}

		return false;
	}
	/**
	 * Удаляет свзяанные с топика данные
	 *
	 * @param  int  $iSeminarId
	 * @return bool
	 */
	public function DeleteSeminarAdditionalData($iSeminarId) {
		/**
		 * Чистим зависимые кеши
		 */
		$this->Cache_Clean(Zend_Cache::CLEANING_MODE_MATCHING_TAG,array('seminar_update'));
		$this->Cache_Delete("seminar_{$iSeminarId}");
		/**
		 * Удаляем комментарии к топику. 
		 * При удалении комментариев они удаляются из избранного,прямого эфира и голоса за них
		 */
		$this->Comment_DeleteCommentByTargetId($iSeminarId,'seminar');
		/**
		 * Удаляем топик из избранного
		 */
		$this->DeleteFavouriteSeminarByArrayId($iSeminarId);
		/**
		 * Удаляем топик из прочитанного
		 */
		$this->DeleteSeminarReadByArrayId($iSeminarId);
		/**
		 * Удаляем голосование к топику
		 */
		$this->Vote_DeleteVoteByTarget($iSeminarId,'seminar');
		/**
		 * Удаляем теги
		 */
		$this->DeleteSeminarTagsBySeminarId($iSeminarId);
		
		return true;
	}
	/**
	 * Обновляет топик
	 *
	 * @param ModuleSeminar_EntitySeminar $oSeminar
	 * @return unknown
	 */
	public function UpdateSeminar(ModuleSeminar_EntitySeminar $oSeminar) {
		/**
		 * Получаем топик ДО изменения
		 */
		$oSeminarOld=$this->GetSeminarById($oSeminar->getId());
		$oSeminar->setDateEdit(date("Y-m-d H:i:s"));
		if ($this->oMapperSeminar->UpdateSeminar($oSeminar)) {	
			/**
			 * Если топик изменил видимость(publish) или локацию (CompanyId) или список тегов
			 */
			if (($oSeminar->getPublish()!=$oSeminarOld->getPublish()) || ($oSeminar->getCompanyId()!=$oSeminarOld->getCompanyId()) || ($oSeminar->getTags()!=$oSeminarOld->getTags())) {
				/**
				 * Обновляем теги
				 */
				$aTags=explode(',',$oSeminar->getTags());
				$this->DeleteSeminarTagsBySeminarId($oSeminar->getId());
				
				if ($oSeminar->getPublish()) {
					foreach ($aTags as $sTag) {
						$oTag=Engine::GetEntity('Seminar_SeminarTag');
						$oTag->setSeminarId($oSeminar->getId());
						$oTag->setUserId($oSeminar->getUserId());
						$oTag->setCompanyId($oSeminar->getCompanyId());
						$oTag->setText($sTag);
						$this->oMapperSeminar->AddSeminarTag($oTag);
					}
				}
			}
			if ($oSeminar->getPublish()!=$oSeminarOld->getPublish()) {
				/**
			 	* Обновляем избранное
			 	*/
				$this->SetFavouriteSeminarPublish($oSeminar->getId(),$oSeminar->getPublish());
				/**
			 	* Удаляем комментарий топика из прямого эфира
			 	*/
				if ($oSeminar->getPublish()==0) {
					$this->Comment_DeleteCommentOnlineByTargetId($oSeminar->getId(),'seminar');
				}
				/**
				 * �?зменяем видимость комментов
				 */
				$this->Comment_SetCommentsPublish($oSeminar->getId(),'seminar',$oSeminar->getPublish());
			}
			//чистим зависимые кеши			
			$this->Cache_Clean(Zend_Cache::CLEANING_MODE_MATCHING_TAG,array('seminar_update',"seminar_update_user_{$oSeminar->getUserId()}","seminar_update_company_{$oSeminar->getCompanyId()}"));
			$this->Cache_Delete("seminar_{$oSeminar->getId()}");
			return true;
		}
		return false;
	}	
		
	/**
	 * Получить топик по айдишнику
	 *
	 * @param unknown_type $sId
	 * @return unknown
	 */
	public function GetSeminarById($sId) {		
		$aSeminars=$this->GetSeminarsAdditionalData($sId);
		if (isset($aSeminars[$sId])) {
			return $aSeminars[$sId];
		}
		return null;
	}	
	/**
	 * Получить список топиков по списку айдишников
	 *
	 * @param unknown_type $aSeminarId
	 */
	public function GetSeminarsByArrayId($aSeminarId) {
		if (!$aSeminarId) {
			return array();
		}
		if (Config::Get('sys.cache.solid')) {
			return $this->GetSeminarsByArrayIdSolid($aSeminarId);
		}
		
		if (!is_array($aSeminarId)) {
			$aSeminarId=array($aSeminarId);
		}
		$aSeminarId=array_unique($aSeminarId);
		$aSeminars=array();
		$aSeminarIdNotNeedQuery=array();
		/**
		 * Делаем мульти-запрос к кешу
		 */
		$aCacheKeys=func_build_cache_keys($aSeminarId,'seminar_');
		if (false !== ($data = $this->Cache_Get($aCacheKeys))) {			
			/**
			 * проверяем что досталось из кеша
			 */
			foreach ($aCacheKeys as $sValue => $sKey ) {
				if (array_key_exists($sKey,$data)) {	
					if ($data[$sKey]) {
						$aSeminars[$data[$sKey]->getId()]=$data[$sKey];
					} else {
						$aSeminarIdNotNeedQuery[]=$sValue;
					}
				} 
			}
		}
		/**
		 * Смотрим каких топиков не было в кеше и делаем запрос в БД
		 */		
		$aSeminarIdNeedQuery=array_diff($aSeminarId,array_keys($aSeminars));		
		$aSeminarIdNeedQuery=array_diff($aSeminarIdNeedQuery,$aSeminarIdNotNeedQuery);		
		$aSeminarIdNeedStore=$aSeminarIdNeedQuery;
		if ($data = $this->oMapperSeminar->GetSeminarsByArrayId($aSeminarIdNeedQuery)) {
			foreach ($data as $oSeminar) {
				/**
				 * Добавляем к результату и сохраняем в кеш
				 */
				$aSeminars[$oSeminar->getId()]=$oSeminar;
				$this->Cache_Set($oSeminar, "seminar_{$oSeminar->getId()}", array(), 60*60*24*4);
				$aSeminarIdNeedStore=array_diff($aSeminarIdNeedStore,array($oSeminar->getId()));
			}
		}
		/**
		 * Сохраняем в кеш запросы не вернувшие результата
		 */
		foreach ($aSeminarIdNeedStore as $sId) {
			$this->Cache_Set(null, "seminar_{$sId}", array(), 60*60*24*4);
		}	
		/**
		 * Сортируем результат согласно входящему массиву
		 */
		$aSeminars=func_array_sort_by_keys($aSeminars,$aSeminarId);
		return $aSeminars;		
	}
	/**
	 * Получить список топиков по списку айдишников, но используя единый кеш
	 *
	 * @param unknown_type $aSeminarId
	 * @return unknown
	 */
	public function GetSeminarsByArrayIdSolid($aSeminarId) {
		if (!is_array($aSeminarId)) {
			$aSeminarId=array($aSeminarId);
		}
		$aSeminarId=array_unique($aSeminarId);	
		$aSeminars=array();	
		$s=join(',',$aSeminarId);
		if (false === ($data = $this->Cache_Get("seminar_id_{$s}"))) {			
			$data = $this->oMapperSeminar->GetSeminarsByArrayId($aSeminarId);
			foreach ($data as $oSeminar) {
				$aSeminars[$oSeminar->getId()]=$oSeminar;
			}
			$this->Cache_Set($aSeminars, "seminar_id_{$s}", array("seminar_update"), 60*60*24*1);
			return $aSeminars;
		}		
		return $data;
	}
	/**
	 * Получает список топиков из избранного
	 *
	 * @param  string $sUserId
	 * @param  int    $iCount
	 * @param  int    $iCurrPage
	 * @param  int    $iPerPage
	 * @return array
	 */
	public function GetSeminarsFavouriteByUserId($sUserId,$iCurrPage,$iPerPage) {		
		$aCloseSeminars =array();							
		/**
		 * Получаем список идентификаторов избранных записей
		 */
		$data = ($this->oUserCurrent && $sUserId==$this->oUserCurrent->getId())
			? $this->Favourite_GetFavouritesByUserId($sUserId,'seminar',$iCurrPage,$iPerPage,$aCloseSeminars)
			: $this->Favourite_GetFavouriteOpenSeminarsByUserId($sUserId,$iCurrPage,$iPerPage);
		/**
		 * Получаем записи по переданому массиву айдишников
		 */
		$data['collection']=$this->GetSeminarsAdditionalData($data['collection']);		
		return $data;
	}
	/**
	 * Возвращает число топиков в избранном
	 *
	 * @param  string $sUserId
	 * @return int
	 */
	public function GetCountSeminarsFavouriteByUserId($sUserId) {
		$aCloseSeminars = array();					
		return ($this->oUserCurrent && $sUserId==$this->oUserCurrent->getId()) 
			? $this->Favourite_GetCountFavouritesByUserId($sUserId,'seminar',$aCloseSeminars)
			: $this->Favourite_GetCountFavouriteOpenSeminarsByUserId($sUserId);	
	}
	/**
	 * Список топиков по фильтру
	 *
	 * @param  array $aFilter
	 * @param  int   $iPage
	 * @param  int   $iPerPage
	 * @return array
	 */
	public function GetSeminarsByFilter($aFilter,$iPage=0,$iPerPage=0,$aAllowData=array('user'=>array(),'company'=>array('owner'=>array(),'relation_user'),'vote','favourite','comment_new')) {
		$s=serialize($aFilter);
		if (false === ($data = $this->Cache_Get("seminar_filter_{$s}_{$iPage}_{$iPerPage}"))) {			
			$data = ($iPage*$iPerPage!=0) 
				? array(
						'collection'=>$this->oMapperSeminar->GetSeminars($aFilter,$iCount,$iPage,$iPerPage),
						'count'=>$iCount
					)
				: array(
						'collection'=>$this->oMapperSeminar->GetAllSeminars($aFilter),
						'count'=>$this->GetCountSeminarsByFilter($aFilter)
					);
			$this->Cache_Set($data, "seminar_filter_{$s}_{$iPage}_{$iPerPage}", array('seminar_update','seminar_new'), 60*60*24*3);
		}
		$data['collection']=$this->GetSeminarsAdditionalData($data['collection'],$aAllowData);
		return $data;
	}
	/**
	 * Количество топиков по фильтру
	 *
	 * @param unknown_type $aFilter
	 * @return unknown
	 */
	public function GetCountSeminarsByFilter($aFilter) {
		$s=serialize($aFilter);					
		if (false === ($data = $this->Cache_Get("seminar_count_{$s}"))) {			
			$data = $this->oMapperSeminar->GetCountSeminars($aFilter);
			$this->Cache_Set($data, "seminar_count_{$s}", array('seminar_update','seminar_new'), 60*60*24*1);
		}
		return 	$data;
	}
	/**
	 * Получает список хороших топиков для вывода на главную страницу(из всех блогов, как коллективных так и персональных)
	 *
	 * @param  int    $iPage
	 * @param  int    $iPerPage
	 * @param  bool   $bAddAccessible Указывает на необходимость добавить в выдачу топики, 
	 *                                из блогов доступных пользователю. При указании false,
	 *                                в выдачу будут переданы только топики из общедоступных блогов.	 
	 * @return array
	 */
	public function GetSeminarsGood($iPage,$iPerPage,$bAddAccessible=true) {
		$aFilter=array(
			'company_type' => array(
				'personal',
				'open'
			),
			'seminar_publish' => 1,
			'seminar_rating'  => array(
				'value' => Config::Get('module.company.index_good'),
				'type'  => 'top',
				'publish_index'  => 1,
			)
		);	
		/**
		 * Если пользователь авторизирован, то добавляем в выдачу
		 * закрытые блоги в которых он состоит
		 */
		if($this->oUserCurrent && $bAddAccessible) {
			$aOpenCompanys = $this->Company_GetAccessibleCompanysByUser($this->oUserCurrent);
			if(count($aOpenCompanys)) $aFilter['company_type']['close'] = $aOpenCompanys;			
		}
		
		return $this->GetSeminarsByFilter($aFilter,$iPage,$iPerPage);
	}
	/**
	 * Получает список ВСЕХ новых топиков
	 *
	 * @param  int    $iPage
	 * @param  int    $iPerPage
	 * @param  bool   $bAddAccessible Указывает на необходимость добавить в выдачу топики, 
	 *                                из блогов доступных пользователю. При указании false,
	 *                                в выдачу будут переданы только топики из общедоступных блогов.
	 * @return array
	 */
	public function GetSeminarsNew($iPage,$iPerPage,$bAddAccessible=true) {
		$sDate=date("Y-m-d H:00:00",time()-Config::Get('module.seminar.new_time'));
		$aFilter=array(
			'company_type' => array(
				'personal',
				'open',
			),
			'seminar_publish' => 1,
			'seminar_new' => $sDate,
		);	
		/**
		 * Если пользователь авторизирован, то добавляем в выдачу
		 * закрытые блоги в которых он состоит
		 */
		if($this->oUserCurrent && $bAddAccessible) {
			$aOpenCompanys = $this->Company_GetAccessibleCompanysByUser($this->oUserCurrent);
			if(count($aOpenCompanys)) $aFilter['company_type']['close'] = $aOpenCompanys;
		}			
		return $this->GetSeminarsByFilter($aFilter,$iPage,$iPerPage);
	}
	/**
	 * Получает заданое число последних топиков
	 *
	 * @param unknown_type $iCount
	 * @return unknown
	 */
	public function GetSeminarsLast($iCount) {		
		$aFilter=array(
			'company_type' => array(
				'personal',
				'open',
			),
			'seminar_publish' => 1,			
		);
		/**
		 * Если пользователь авторизирован, то добавляем в выдачу
		 * закрытые блоги в которых он состоит
		 */
		if($this->oUserCurrent) {
			$aOpenCompanys = $this->Company_GetAccessibleCompanysByUser($this->oUserCurrent);
			if(count($aOpenCompanys)) $aFilter['company_type']['close'] = $aOpenCompanys;
		}	
		$aReturn=$this->GetSeminarsByFilter($aFilter,1,$iCount);
		if (isset($aReturn['collection'])) {
			return $aReturn['collection'];
		}
		return false;
	}
	/**
	 * список топиков из персональных блогов
	 *
	 * @param unknown_type $iPage
	 * @param unknown_type $iPerPage
	 * @param unknown_type $sShowType
	 * @return unknown
	 */
	public function GetSeminarsPersonal($iPage,$iPerPage,$sShowType='good') {
		$aFilter=array(
			'company_type' => array(
				'personal',
			),
			'seminar_publish' => 1,			
		);
		switch ($sShowType) {
			case 'good':
				$aFilter['seminar_rating']=array(
					'value' => Config::Get('module.company.personal_good'),
					'type'  => 'top',
				);			
				break;	
			case 'bad':
				$aFilter['seminar_rating']=array(
					'value' => Config::Get('module.company.personal_good'),
					'type'  => 'down',
				);			
				break;	
			case 'new':
				$aFilter['seminar_new']=date("Y-m-d H:00:00",time()-Config::Get('module.seminar.new_time'));							
				break;
			default:
				break;
		}
		return $this->GetSeminarsByFilter($aFilter,$iPage,$iPerPage);
	}	
	/**
	 * Получает число новых топиков в персональных блогах
	 *
	 * @return unknown
	 */
	public function GetCountSeminarsPersonalNew() {
		$sDate=date("Y-m-d H:00:00",time()-Config::Get('module.seminar.new_time'));
		$aFilter=array(
			'company_type' => array(
				'personal',
			),
			'seminar_publish' => 1,
			'seminar_new' => $sDate,
		);				
		return $this->GetCountSeminarsByFilter($aFilter);
	}
	/**
	 * Получает список топиков по юзеру
	 *
	 * @param unknown_type $sUserId
	 * @param unknown_type $iPublish
	 * @param unknown_type $iPage
	 * @param unknown_type $iPerPage
	 * @return unknown
	 */
	public function GetSeminarsPersonalByUser($sUserId,$iPublish,$iPage,$iPerPage) {
		$aFilter=array(			
			'seminar_publish' => $iPublish,
			'user_id' => $sUserId,
			'company_type' => array('open','personal'),
		);
		/**
		 * Если пользователь смотрит свой профиль, то добавляем в выдачу
		 * закрытые блоги в которых он состоит
		 */
		if($this->oUserCurrent && $this->oUserCurrent->getId()==$sUserId) {
			$aFilter['company_type'][]='close';
		}		
		return $this->GetSeminarsByFilter($aFilter,$iPage,$iPerPage);
	}
	
	/**
	 * Возвращает количество топиков которые создал юзер
	 *
	 * @param unknown_type $sUserId
	 * @param unknown_type $iPublish
	 * @return unknown
	 */
	public function GetCountSeminarsPersonalByUser($sUserId,$iPublish) {
		$aFilter=array(			
			'seminar_publish' => $iPublish,
			'user_id' => $sUserId,
			'company_type' => array('open','personal'),
		);
		/**
		 * Если пользователь смотрит свой профиль, то добавляем в выдачу
		 * закрытые блоги в которых он состоит
		 */
		if($this->oUserCurrent && $this->oUserCurrent->getId()==$sUserId) {
			$aFilter['company_type'][]='close';
		}		
		$s=serialize($aFilter);
		if (false === ($data = $this->Cache_Get("seminar_count_user_{$s}"))) {			
			$data = $this->oMapperSeminar->GetCountSeminars($aFilter);
			$this->Cache_Set($data, "seminar_count_user_{$s}", array("seminar_update_user_{$sUserId}"), 60*60*24);
		}
		return 	$data;		
	}
	
	/**
	 * Получает список идентификаторов топиков 
	 * из закрытых блогов по юзеру
	 *
	 * @param  string $sUserId
	 * @return array
	 */
	public function GetSeminarsCloseByUser($sUserId=null) {
		if(!is_null($sUserId) && $oUser=$this->User_GetUserById($sUserId)) {
			$aCloseCompanys=$this->Company_GetInaccessibleCompanysByUser($oUser);
			$aFilter=array(
				'seminar_publish' => 1,
				'company_id' => $aCloseCompanys,
			);
		} else {
			$aFilter=array(
				'seminar_publish' => 1,
				'company_type' => array('close'),
			);
		}
		
		$aSeminars=$this->GetSeminarsByFilter($aFilter);
		return array_keys($aSeminars['collection']);
	}
	
	/**
	 * Получает список топиков из указанного блога
	 *
	 * @param  int   $iCompanyId
	 * @param  int   $iPage
	 * @param  int   $iPerPage
	 * @param  array $aAllowData
	 * @param  bool  $bIdsOnly
	 * @return array
	 */
	public function GetSeminarsByCompanyId($iCompanyId,$iPage=0,$iPerPage=0,$aAllowData=array(),$bIdsOnly=true) {
		$aFilter=array('company_id'=>$iCompanyId);
		
		if(!$aSeminars = $this->GetSeminarsByFilter($aFilter,$iPage,$iPerPage,$aAllowData) ) {
			return false;
		}
		
		return ($bIdsOnly) 
			? array_keys($aSeminars['collection'])
			: $aSeminars;		
	}
	
	/**
	 * список топиков из коллективных блогов
	 *
	 * @param unknown_type $iPage
	 * @param unknown_type $iPerPage
	 * @param unknown_type $sShowType
	 * @return unknown
	 */
	public function GetSeminarsCollective($iPage,$iPerPage,$sShowType='good') {
		$aFilter=array(
			'company_type' => array(
				'open',
			),
			'seminar_publish' => 1,			
		);		
		switch ($sShowType) {
			case 'good':
				$aFilter['seminar_rating']=array(
					'value' => Config::Get('module.company.collective_good'),
					'type'  => 'top',
				);			
				break;	
			case 'bad':
				$aFilter['seminar_rating']=array(
					'value' => Config::Get('module.company.collective_good'),
					'type'  => 'down',
				);			
				break;	
			case 'new':
				$aFilter['seminar_new']=date("Y-m-d H:00:00",time()-Config::Get('module.seminar.new_time'));							
				break;
			default:
				break;
		}
		/**
		 * Если пользователь авторизирован, то добавляем в выдачу
		 * закрытые блоги в которых он состоит
		 */
		if($this->oUserCurrent) {
			$aOpenCompanys = $this->Company_GetAccessibleCompanysByUser($this->oUserCurrent);
			if(count($aOpenCompanys)) $aFilter['company_type']['close'] = $aOpenCompanys;
		}
		return $this->GetSeminarsByFilter($aFilter,$iPage,$iPerPage);
	}	
	/**
	 * Получает число новых топиков в коллективных блогах
	 *
	 * @return unknown
	 */
	public function GetCountSeminarsCollectiveNew() {
		$sDate=date("Y-m-d H:00:00",time()-Config::Get('module.seminar.new_time'));
		$aFilter=array(
			'company_type' => array(
				'open',
			),
			'seminar_publish' => 1,
			'seminar_new' => $sDate,
		);
		/**
		 * Если пользователь авторизирован, то добавляем в выдачу
		 * закрытые блоги в которых он состоит
		 */
		if($this->oUserCurrent) {
			$aOpenCompanys = $this->Company_GetAccessibleCompanysByUser($this->oUserCurrent);
			if(count($aOpenCompanys)) $aFilter['company_type']['close'] = $aOpenCompanys;
		}		
		return $this->GetCountSeminarsByFilter($aFilter);		
	}
	/**
	 * Получает топики по рейтингу и дате
	 *
	 * @param unknown_type $sDate
	 * @param unknown_type $iLimit
	 * @return unknown
	 */
	public function GetSeminarsRatingByDate($sDate,$iLimit=20) {
		/**
		 * Получаем список блогов, топики которых нужно исключить из выдачи
		 */
		$aCloseCompanys = ($this->oUserCurrent)
			? $this->Company_GetInaccessibleCompanysByUser($this->oUserCurrent)
			: $this->Company_GetInaccessibleCompanysByUser();	
		
		$s=serialize($aCloseCompanys);
		
		if (false === ($data = $this->Cache_Get("seminar_rating_{$sDate}_{$iLimit}_{$s}"))) {
			$data = $this->oMapperSeminar->GetSeminarsRatingByDate($sDate,$iLimit,$aCloseCompanys);
			$this->Cache_Set($data, "seminar_rating_{$sDate}_{$iLimit}_{$s}", array('seminar_update'), 60*60*24*2);
		}
		$data=$this->GetSeminarsAdditionalData($data);
		return $data;
	}	
	/**
	 * Список топиков из блога
	 *
	 * @param unknown_type $oCompany
	 * @param unknown_type $iPage
	 * @param unknown_type $iPerPage
	 * @param unknown_type $sShowType
	 * @return unknown
	 */
	public function GetSeminarsByCompany($oCompany,$iPage,$iPerPage,$sShowType='good') {
		$aFilter=array(
			'seminar_publish' => 1,
			'company_id' => $oCompany->getId(),			
		);
		switch ($sShowType) {
			case 'good':
				$aFilter['seminar_rating']=array(
					'value' => Config::Get('module.company.collective_good'),
					'type'  => 'top',
				);			
				break;	
			case 'bad':
				$aFilter['seminar_rating']=array(
					'value' => Config::Get('module.company.collective_good'),
					'type'  => 'down',
				);			
				break;	
			case 'new':
				$aFilter['seminar_new']=date("Y-m-d H:00:00",time()-Config::Get('module.seminar.new_time'));							
				break;
			default:
				break;
		}		
		return $this->GetSeminarsByFilter($aFilter,$iPage,$iPerPage);
	}
	
	/**
	 * Получает число новых топиков из блога
	 *
	 * @param unknown_type $oCompany
	 * @return unknown
	 */
	public function GetCountSeminarsByCompanyNew($oCompany) {
		$sDate=date("Y-m-d H:00:00",time()-Config::Get('module.seminar.new_time'));
		$aFilter=array(			
			'seminar_publish' => 1,
			'company_id' => $oCompany->getId(),
			'seminar_new' => $sDate,
			
		);	
		return $this->GetCountSeminarsByFilter($aFilter);		
	}
	/**
	 * Получает список топиков по тегу
	 *
	 * @param  string $sTag
	 * @param  int    $iPage
	 * @param  int    $iPerPage
	 * @param  bool   $bAddAccessible Указывает на необходимость добавить в выдачу топики, 
	 *                                из блогов доступных пользователю. При указании false,
	 *                                в выдачу будут переданы только топики из общедоступных блогов.
	 * @return array
	 */
	public function GetSeminarsByTag($sTag,$iPage,$iPerPage,$bAddAccessible=true) {
		$aCloseCompanys = ($this->oUserCurrent && $bAddAccessible) 
			? $this->Company_GetInaccessibleCompanysByUser($this->oUserCurrent)
			: $this->Company_GetInaccessibleCompanysByUser();
		
		$s = serialize($aCloseCompanys);	
		if (false === ($data = $this->Cache_Get("seminar_tag_{$sTag}_{$iPage}_{$iPerPage}_{$s}"))) {			
			$data = array('collection'=>$this->oMapperSeminar->GetSeminarsByTag($sTag,$aCloseCompanys,$iCount,$iPage,$iPerPage),'count'=>$iCount);
			$this->Cache_Set($data, "seminar_tag_{$sTag}_{$iPage}_{$iPerPage}_{$s}", array('seminar_update','seminar_new'), 60*60*24*2);
		}
		$data['collection']=$this->GetSeminarsAdditionalData($data['collection']);
		return $data;		
	}
	/**
	 * Получает список тегов топиков
	 *
	 * @param unknown_type $iLimit
	 * @return unknown
	 */
	public function GetSeminarTags($iLimit,$aExcludeSeminar=array()) {
		$s=serialize($aExcludeSeminar);
		if (false === ($data = $this->Cache_Get("tag_{$iLimit}_{$s}"))) {			
			$data = $this->oMapperSeminar->GetSeminarTags($iLimit,$aExcludeSeminar);
			$this->Cache_Set($data, "tag_{$iLimit}_{$s}", array('seminar_update','seminar_new'), 60*60*24*3);
		}
		return $data;
	}
	/**
	 * Получает список тегов из топиков открытых блогов (open,personal)
	 *
	 * @param  int $iLimit
	 * @return array
	 */
	public function GetOpenSeminarTags($iLimit) {
		if (false === ($data = $this->Cache_Get("tag_{$iLimit}_open"))) {			
			$data = $this->oMapperSeminar->GetOpenSeminarTags($iLimit);
			$this->Cache_Set($data, "tag_{$iLimit}_open", array('seminar_update','seminar_new'), 60*60*24*3);
		}
		return $data;
	}	
	
	/**
	 * Увеличивает у топика число комментов
	 *
	 * @param unknown_type $sSeminarId
	 * @return unknown
	 */
	public function increaseSeminarCountComment($sSeminarId) {		
		$this->Cache_Delete("seminar_{$sSeminarId}");
		$this->Cache_Clean(Zend_Cache::CLEANING_MODE_MATCHING_TAG,array("seminar_update"));
		return $this->oMapperSeminar->increaseSeminarCountComment($sSeminarId);
	}
	/**
	 * Получает привязку топика к ибранному(добавлен ли топик в избранное у юзера)
	 *
	 * @param unknown_type $sSeminarId
	 * @param unknown_type $sUserId
	 * @return unknown
	 */
	public function GetFavouriteSeminar($sSeminarId,$sUserId) {
		return $this->Favourite_GetFavourite($sSeminarId,'seminar',$sUserId);
	}
	/**
	 * Получить список избранного по списку айдишников
	 *
	 * @param unknown_type $aSeminarId
	 */
	public function GetFavouriteSeminarsByArray($aSeminarId,$sUserId) {
		return $this->Favourite_GetFavouritesByArray($aSeminarId,'seminar',$sUserId);
	}
	/**
	 * Получить список избранного по списку айдишников, но используя единый кеш
	 *
	 * @param array $aSeminarId
	 * @param int $sUserId
	 * @return array
	 */
	public function GetFavouriteSeminarsByArraySolid($aSeminarId,$sUserId) {
		return $this->Favourite_GetFavouritesByArraySolid($aSeminarId,'seminar',$sUserId);
	}
	/**
	 * Добавляет топик в избранное
	 *
	 * @param ModuleFavourite_EntityFavourite $oFavouriteSeminar
	 * @return unknown
	 */
	public function AddFavouriteSeminar(ModuleFavourite_EntityFavourite $oFavouriteSeminar) {		
		return $this->Favourite_AddFavourite($oFavouriteSeminar);
	}
	/**
	 * Удаляет топик из избранного
	 *
	 * @param ModuleFavourite_EntityFavourite $oFavouriteSeminar
	 * @return unknown
	 */
	public function DeleteFavouriteSeminar(ModuleFavourite_EntityFavourite $oFavouriteSeminar) {	
		return $this->Favourite_DeleteFavourite($oFavouriteSeminar);
	}
	/**
	 * Устанавливает переданный параметр публикации таргета (топика)
	 *
	 * @param  string $sSeminarId
	 * @param  int    $iPublish
	 * @return bool
	 */
	public function SetFavouriteSeminarPublish($sSeminarId,$iPublish) {
		return $this->Favourite_SetFavouriteTargetPublish($sSeminarId,'seminar',$iPublish);		
	}	
	/**
	 * Удаляет топики из избранного по списку 
	 *
	 * @param  array $aSeminarId
	 * @return bool
	 */
	public function DeleteFavouriteSeminarByArrayId($aSeminarId) {
		return $this->Favourite_DeleteFavouriteByTargetId($aSeminarId, 'seminar');
	}
	/**
	 * Получает список тегов по первым буквам тега
	 *
	 * @param unknown_type $sTag
	 * @param unknown_type $iLimit
	 */
	public function GetSeminarTagsByLike($sTag,$iLimit) {
		if (false === ($data = $this->Cache_Get("tag_like_{$sTag}_{$iLimit}"))) {			
			$data = $this->oMapperSeminar->GetSeminarTagsByLike($sTag,$iLimit);
			$this->Cache_Set($data, "tag_like_{$sTag}_{$iLimit}", array("seminar_update","seminar_new"), 60*60*24*3);
		}
		return $data;		
	}
	/**
	 * Обновляем/устанавливаем дату прочтения топика, если читаем его первый раз то добавляем
	 *
	 * @param ModuleSeminar_EntitySeminarRead $oSeminarRead	 
	 */
	public function SetSeminarRead(ModuleSeminar_EntitySeminarRead $oSeminarRead) {		
		if ($this->GetSeminarRead($oSeminarRead->getSeminarId(),$oSeminarRead->getUserId())) {
			$this->Cache_Delete("seminar_read_{$oSeminarRead->getSeminarId()}_{$oSeminarRead->getUserId()}");
			$this->Cache_Clean(Zend_Cache::CLEANING_MODE_MATCHING_TAG,array("seminar_read_user_{$oSeminarRead->getUserId()}"));
			$this->oMapperSeminar->UpdateSeminarRead($oSeminarRead);
		} else {
			$this->Cache_Delete("seminar_read_{$oSeminarRead->getSeminarId()}_{$oSeminarRead->getUserId()}");
			$this->Cache_Clean(Zend_Cache::CLEANING_MODE_MATCHING_TAG,array("seminar_read_user_{$oSeminarRead->getUserId()}"));
			$this->oMapperSeminar->AddSeminarRead($oSeminarRead);
		}
		return true;		
	}	
	/**
	 * Получаем дату прочтения топика юзером
	 *
	 * @param unknown_type $sSeminarId
	 * @param unknown_type $sUserId
	 * @return unknown
	 */
	public function GetSeminarRead($sSeminarId,$sUserId) {
		$data=$this->GetSeminarsReadByArray($sSeminarId,$sUserId);
		if (isset($data[$sSeminarId])) {
			return $data[$sSeminarId];
		}
		return null;
	}
	/**
	 * Удаляет записи о чтении записей по списку идентификаторов
	 *
	 * @param  array|int $aSeminarId
	 * @return bool
	 */
	public function DeleteSeminarReadByArrayId($aSeminarId) {
		if(!is_array($aSeminarId)) $aSeminarId = array($aSeminarId);
		return $this->oMapperSeminar->DeleteSeminarReadByArrayId($aSeminarId);
	}
	/**
	 * Получить список просмотром/чтения топиков по списку айдишников
	 *
	 * @param unknown_type $aSeminarId
	 */
	public function GetSeminarsReadByArray($aSeminarId,$sUserId) {
		if (!$aSeminarId) {
			return array();
		}
		if (Config::Get('sys.cache.solid')) {
			return $this->GetSeminarsReadByArraySolid($aSeminarId,$sUserId);
		}
		if (!is_array($aSeminarId)) {
			$aSeminarId=array($aSeminarId);
		}
		$aSeminarId=array_unique($aSeminarId);
		$aSeminarsRead=array();
		$aSeminarIdNotNeedQuery=array();
		/**
		 * Делаем мульти-запрос к кешу
		 */
		$aCacheKeys=func_build_cache_keys($aSeminarId,'seminar_read_','_'.$sUserId);
		if (false !== ($data = $this->Cache_Get($aCacheKeys))) {			
			/**
			 * проверяем что досталось из кеша
			 */
			foreach ($aCacheKeys as $sValue => $sKey ) {
				if (array_key_exists($sKey,$data)) {	
					if ($data[$sKey]) {
						$aSeminarsRead[$data[$sKey]->getSeminarId()]=$data[$sKey];
					} else {
						$aSeminarIdNotNeedQuery[]=$sValue;
					}
				} 
			}
		}
		/**
		 * Смотрим каких топиков не было в кеше и делаем запрос в БД
		 */		
		$aSeminarIdNeedQuery=array_diff($aSeminarId,array_keys($aSeminarsRead));		
		$aSeminarIdNeedQuery=array_diff($aSeminarIdNeedQuery,$aSeminarIdNotNeedQuery);		
		$aSeminarIdNeedStore=$aSeminarIdNeedQuery;
		if ($data = $this->oMapperSeminar->GetSeminarsReadByArray($aSeminarIdNeedQuery,$sUserId)) {
			foreach ($data as $oSeminarRead) {
				/**
				 * Добавляем к результату и сохраняем в кеш
				 */
				$aSeminarsRead[$oSeminarRead->getSeminarId()]=$oSeminarRead;
				$this->Cache_Set($oSeminarRead, "seminar_read_{$oSeminarRead->getSeminarId()}_{$oSeminarRead->getUserId()}", array(), 60*60*24*4);
				$aSeminarIdNeedStore=array_diff($aSeminarIdNeedStore,array($oSeminarRead->getSeminarId()));
			}
		}
		/**
		 * Сохраняем в кеш запросы не вернувшие результата
		 */
		foreach ($aSeminarIdNeedStore as $sId) {
			$this->Cache_Set(null, "seminar_read_{$sId}_{$sUserId}", array(), 60*60*24*4);
		}		
		/**
		 * Сортируем результат согласно входящему массиву
		 */
		$aSeminarsRead=func_array_sort_by_keys($aSeminarsRead,$aSeminarId);
		return $aSeminarsRead;		
	}
	/**
	 * Получить список просмотром/чтения топиков по списку айдишников, но используя единый кеш
	 *
	 * @param unknown_type $aSeminarId
	 * @param unknown_type $sUserId
	 * @return unknown
	 */
	public function GetSeminarsReadByArraySolid($aSeminarId,$sUserId) {
		if (!is_array($aSeminarId)) {
			$aSeminarId=array($aSeminarId);
		}
		$aSeminarId=array_unique($aSeminarId);	
		$aSeminarsRead=array();	
		$s=join(',',$aSeminarId);
		if (false === ($data = $this->Cache_Get("seminar_read_{$sUserId}_id_{$s}"))) {			
			$data = $this->oMapperSeminar->GetSeminarsReadByArray($aSeminarId,$sUserId);
			foreach ($data as $oSeminarRead) {
				$aSeminarsRead[$oSeminarRead->getSeminarId()]=$oSeminarRead;
			}
			$this->Cache_Set($aSeminarsRead, "seminar_read_{$sUserId}_id_{$s}", array("seminar_read_user_{$sUserId}"), 60*60*24*1);
			return $aSeminarsRead;
		}		
		return $data;
	}
	/**
	 * Проверяет голосовал ли юзер за топик-вопрос
	 *
	 * @param unknown_type $sSeminarId
	 * @param unknown_type $sUserId
	 * @return unknown
	 */
	public function GetSeminarQuestionVote($sSeminarId,$sUserId) {
		$data=$this->GetSeminarsQuestionVoteByArray($sSeminarId,$sUserId);
		if (isset($data[$sSeminarId])) {
			return $data[$sSeminarId];
		}
		return null;
	}
	/**
	 * Получить список голосований в топике-опросе по списку айдишников
	 *
	 * @param unknown_type $aSeminarId
	 */
	public function GetSeminarsQuestionVoteByArray($aSeminarId,$sUserId) {
		if (!$aSeminarId) {
			return array();
		}
		if (Config::Get('sys.cache.solid')) {
			return $this->GetSeminarsQuestionVoteByArraySolid($aSeminarId,$sUserId);
		}
		if (!is_array($aSeminarId)) {
			$aSeminarId=array($aSeminarId);
		}
		$aSeminarId=array_unique($aSeminarId);
		$aSeminarsQuestionVote=array();
		$aSeminarIdNotNeedQuery=array();
		/**
		 * Делаем мульти-запрос к кешу
		 */
		$aCacheKeys=func_build_cache_keys($aSeminarId,'seminar_question_vote_','_'.$sUserId);
		if (false !== ($data = $this->Cache_Get($aCacheKeys))) {			
			/**
			 * проверяем что досталось из кеша
			 */
			foreach ($aCacheKeys as $sValue => $sKey ) {
				if (array_key_exists($sKey,$data)) {	
					if ($data[$sKey]) {
						$aSeminarsQuestionVote[$data[$sKey]->getSeminarId()]=$data[$sKey];
					} else {
						$aSeminarIdNotNeedQuery[]=$sValue;
					}
				} 
			}
		}
		/**
		 * Смотрим каких топиков не было в кеше и делаем запрос в БД
		 */		
		$aSeminarIdNeedQuery=array_diff($aSeminarId,array_keys($aSeminarsQuestionVote));		
		$aSeminarIdNeedQuery=array_diff($aSeminarIdNeedQuery,$aSeminarIdNotNeedQuery);		
		$aSeminarIdNeedStore=$aSeminarIdNeedQuery;
		if ($data = $this->oMapperSeminar->GetSeminarsQuestionVoteByArray($aSeminarIdNeedQuery,$sUserId)) {
			foreach ($data as $oSeminarVote) {
				/**
				 * Добавляем к результату и сохраняем в кеш
				 */
				$aSeminarsQuestionVote[$oSeminarVote->getSeminarId()]=$oSeminarVote;
				$this->Cache_Set($oSeminarVote, "seminar_question_vote_{$oSeminarVote->getSeminarId()}_{$oSeminarVote->getVoterId()}", array(), 60*60*24*4);
				$aSeminarIdNeedStore=array_diff($aSeminarIdNeedStore,array($oSeminarVote->getSeminarId()));
			}
		}
		/**
		 * Сохраняем в кеш запросы не вернувшие результата
		 */
		foreach ($aSeminarIdNeedStore as $sId) {
			$this->Cache_Set(null, "seminar_question_vote_{$sId}_{$sUserId}", array(), 60*60*24*4);
		}		
		/**
		 * Сортируем результат согласно входящему массиву
		 */
		$aSeminarsQuestionVote=func_array_sort_by_keys($aSeminarsQuestionVote,$aSeminarId);
		return $aSeminarsQuestionVote;		
	}
	/**
	 * Получить список голосований в топике-опросе по списку айдишников, но используя единый кеш
	 *
	 * @param unknown_type $aSeminarId
	 * @param unknown_type $sUserId
	 * @return unknown
	 */
	public function GetSeminarsQuestionVoteByArraySolid($aSeminarId,$sUserId) {
		if (!is_array($aSeminarId)) {
			$aSeminarId=array($aSeminarId);
		}
		$aSeminarId=array_unique($aSeminarId);	
		$aSeminarsQuestionVote=array();	
		$s=join(',',$aSeminarId);
		if (false === ($data = $this->Cache_Get("seminar_question_vote_{$sUserId}_id_{$s}"))) {			
			$data = $this->oMapperSeminar->GetSeminarsQuestionVoteByArray($aSeminarId,$sUserId);
			foreach ($data as $oSeminarVote) {
				$aSeminarsQuestionVote[$oSeminarVote->getSeminarId()]=$oSeminarVote;
			}
			$this->Cache_Set($aSeminarsQuestionVote, "seminar_question_vote_{$sUserId}_id_{$s}", array("seminar_question_vote_user_{$sUserId}"), 60*60*24*1);
			return $aSeminarsQuestionVote;
		}		
		return $data;
	}
	/**
	 * Добавляет факт голосования за топик-вопрос
	 *
	 * @param ModuleSeminar_EntitySeminarQuestionVote $oSeminarQuestionVote
	 */
	public function AddSeminarQuestionVote(ModuleSeminar_EntitySeminarQuestionVote $oSeminarQuestionVote) {
		$this->Cache_Delete("seminar_question_vote_{$oSeminarQuestionVote->getSeminarId()}_{$oSeminarQuestionVote->getVoterId()}");
		$this->Cache_Clean(Zend_Cache::CLEANING_MODE_MATCHING_TAG,array("seminar_question_vote_user_{$oSeminarQuestionVote->getVoterId()}"));
		return $this->oMapperSeminar->AddSeminarQuestionVote($oSeminarQuestionVote);
	}
	/**
	 * Получает топик по уникальному хешу(текст топика)
	 *
	 * @param unknown_type $sUserId
	 * @param unknown_type $sHash
	 * @return unknown
	 */
	public function GetSeminarUnique($sUserId,$sHash) {
		$sId=$this->oMapperSeminar->GetSeminarUnique($sUserId,$sHash);
		return $this->GetSeminarById($sId);
	}
	/**
	 * Рассылает уведомления о новом топике подписчикам блога
	 *
	 * @param unknown_type $oCompany
	 * @param unknown_type $oSeminar
	 * @param unknown_type $oUserSeminar
	 */
	public function SendNotifySeminarNew($oCompany,$oSeminar,$oUserSeminar) {
		$aCompanyUsers=$this->Company_GetCompanyUsersByCompanyId($oCompany->getId());
		foreach ($aCompanyUsers as $oCompanyUser) {
			if ($oCompanyUser->getUserId()==$oUserSeminar->getId()) {
				continue;
			}
			$this->Notify_SendSeminarNewToSubscribeCompany($oCompanyUser->getUser(),$oSeminar,$oCompany,$oUserSeminar);
		}
		//отправляем создателю блога
		if ($oCompany->getOwnerId()!=$oUserSeminar->getId()) {
			$this->Notify_SendSeminarNewToSubscribeCompany($oCompany->getOwner(),$oSeminar,$oCompany,$oUserSeminar);
		}	
	}
	/**
	 * Возвращает список последних топиков пользователя,
	 * опубликованных не более чем $iTimeLimit секунд назад
	 *
	 * @param  string $sUserId
	 * @param  int    $iTimeLimit
	 * @param  int    $iCountLimit
	 * @return array
	 */
	public function GetLastSeminarsByUserId($sUserId,$iTimeLimit,$iCountLimit=1,$aAllowData=array()) {
		$aFilter = array(
			'seminar_publish' => 1,
			'user_id' => $sUserId,
			'seminar_new' => date("Y-m-d H:i:s",time()-$iTimeLimit),
		);
		$aSeminars = $this->GetSeminarsByFilter($aFilter,1,$iCountLimit,$aAllowData);

		return $aSeminars;
	}
	
	/**
	 * Перемещает топики в другой блог
	 *
	 * @param  array  $aSeminars
	 * @param  string $sCompanyId
	 * @return bool
	 */
	public function MoveSeminarsByArrayId($aSeminars,$sCompanyId) {
		$this->Cache_Clean(Zend_Cache::CLEANING_MODE_MATCHING_TAG,array("seminar_update", "seminar_new_company_{$sCompanyId}"));
		if ($res=$this->oMapperSeminar->MoveSeminarsByArrayId($aSeminars,$sCompanyId)) {
			// перемещаем теги
			$this->oMapperSeminar->MoveSeminarsTagsByArrayId($aSeminars,$sCompanyId);
			// меняем target parent у комментов
			$this->Comment_UpdateTargetParentByTargetId($sCompanyId, 'seminar', $aSeminars);
			// меняем target parent у комментов в прямом эфире
			$this->Comment_UpdateTargetParentByTargetIdOnline($sCompanyId, 'seminar', $aSeminars);
			return $res;
		}
		return false;
	}
	
	/**
	 * Перемещает топики в другой блог
	 *
	 * @param  string $sCompanyId
	 * @param  string $sCompanyIdNew
	 * @return bool
	 */
	public function MoveSeminars($sCompanyId,$sCompanyIdNew) {
		$this->Cache_Clean(Zend_Cache::CLEANING_MODE_MATCHING_TAG,array("seminar_update", "seminar_new_company_{$sCompanyId}", "seminar_new_company_{$sCompanyIdNew}"));
		if ($res=$this->oMapperSeminar->MoveSeminars($sCompanyId,$sCompanyIdNew)) {
			// перемещаем теги
			$this->oMapperSeminar->MoveSeminarsTags($sCompanyId,$sCompanyIdNew);
			// меняем target parent у комментов
			$this->Comment_MoveTargetParent($sCompanyId, 'seminar', $sCompanyIdNew);
			// меняем target parent у комментов в прямом эфире
			$this->Comment_MoveTargetParentOnline($sCompanyId, 'seminar', $sCompanyIdNew);
			return $res;
		}
		return false;
	}	
	
	/**
	 * Заргузка изображений при написании топика
	 *
	 * @param  array           $aFile
	 * @param  ModuleUser_EntityUser $oUser
	 * @return string|bool
	 */
	public function UploadSeminarImageFile($aFile,$oUser) {
		if(!is_array($aFile) || !isset($aFile['tmp_name'])) {
			return false;
		}
							
		$sFileTmp=Config::Get('sys.cache.dir').func_generator();		
		if (!move_uploaded_file($aFile['tmp_name'],$sFileTmp)) {			
			return false;
		}
		$sDirUpload=$this->Image_GetIdDir($oUser->getId());
		$aParams=$this->Image_BuildParams('seminar');
		
		if ($sFileImage=$this->Image_Resize($sFileTmp,$sDirUpload,func_generator(6),Config::Get('view.img_max_width'),Config::Get('view.img_max_height'),Config::Get('view.img_resize_width'),null,true,$aParams)) {
			@unlink($sFileTmp);
			return $this->Image_GetWebPath($sFileImage);
		}
		@unlink($sFileTmp);
		return false;
	}
	/**
	 * Загрузка изображений по переданному URL
	 *
	 * @param  string          $sUrl
	 * @param  ModuleUser_EntityUser $oUser
	 * @return (string|bool)
	 */
	public function UploadSeminarImageUrl($sUrl, $oUser) {
		/**
		 * Проверяем, является ли файл изображением
		 */
		if(!@getimagesize($sUrl)) {
			return ModuleImage::UPLOAD_IMAGE_ERROR_TYPE;
		}
		/**
		 * Открываем файловый поток и считываем файл поблочно,
		 * контролируя максимальный размер изображения
		 */
		$oFile=fopen($sUrl,'r');
		if(!$oFile) {
			return ModuleImage::UPLOAD_IMAGE_ERROR_READ;
		}
		
		$iMaxSizeKb=500;
		$iSizeKb=0;
		$sContent='';
		while (!feof($oFile) and $iSizeKb<$iMaxSizeKb) {
			$sContent.=fread($oFile ,1024*1);
			$iSizeKb++;
		}

		/**
		 * Если конец файла не достигнут,
		 * значит файл имеет недопустимый размер
		 */
		if(!feof($oFile)) {
			return ModuleImage::UPLOAD_IMAGE_ERROR_SIZE;
		}
		fclose($oFile);

		/**
		 * Создаем tmp-файл, для временного хранения изображения
		 */
		$sFileTmp=Config::Get('sys.cache.dir').func_generator();
		
		$fp=fopen($sFileTmp,'w');
		fwrite($fp,$sContent);
		fclose($fp);
		
		$sDirSave=$this->Image_GetIdDir($oUser->getId());
		$aParams=$this->Image_BuildParams('seminar');
		
		/**
		 * Передаем изображение на обработку
		 */
		if ($sFileImg=$this->Image_Resize($sFileTmp,$sDirSave,func_generator(),Config::Get('view.img_max_width'),Config::Get('view.img_max_height'),Config::Get('view.img_resize_width'),null,true,$aParams)) {
			@unlink($sFileTmp);
			return $this->Image_GetWebPath($sFileImg);
		} 		
		
		@unlink($sFileTmp);
		return ModuleImage::UPLOAD_IMAGE_ERROR;
	}
}
?>