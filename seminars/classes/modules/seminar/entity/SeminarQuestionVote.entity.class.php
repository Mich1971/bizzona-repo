<?php
/*-------------------------------------------------------
*
*   LiveStreet Engine Social Networking
*   Copyright © 2008 Mzhelskiy Maxim
*
*--------------------------------------------------------
*
*   Official site: www.livestreet.ru
*   Contact e-mail: rus.engine@gmail.com
*
*   GNU General Public License, version 2:
*   http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
*
---------------------------------------------------------
*/

class ModuleSeminar_EntitySeminarQuestionVote extends Entity 
{    
    public function getSeminarId() {
        return $this->_aData['seminar_id'];
    }  
    public function getVoterId() {
        return $this->_aData['user_voter_id'];
    }
	public function getAnswer() {
        return $this->_aData['answer'];
    }
    
    
	public function setSeminarId($data) {
        $this->_aData['seminar_id']=$data;
    }
    public function setVoterId($data) {
        $this->_aData['user_voter_id']=$data;
    }
    public function setAnswer($data) {
        $this->_aData['answer']=$data;
    }
}
?>