		{hook run='content_end'}
		</div>
		<!-- /Content -->
		{if !$bNoSidebar}
			{include file='sidebar.tpl'}
		{/if}
		
	</div>

	<!-- Footer -->
	<div id="footer">
		<div class="right">
			© Powered by <a href="http://livestreetcms.ru" title="Free social engine">«LiveStreet»</a><br />
			<a href="{router page='page'}about/">{$aLang.page_about}</a>
		</div>
		Design by — <a href="http://www.xeoart.com/">Студия XeoArt</a>&nbsp;<img src="{cfg name='path.static.skin'}/images/xeoart.gif" border="0">
	</div>
	<!-- /Footer -->

</div>
{hook run='body_end'}

{literal}
<script src="http://www.google-analytics.com/urchin.js" type="text/javascript">
</script>
<script type="text/javascript">
_uacct = "UA-2346556-1";
urchinTracker();
</script>
{/literal}

{literal}
<!-- Yandex.Metrika -->
<script src="//mc.yandex.ru/metrika/watch.js" type="text/javascript"></script>
<div style="display:none;"><script type="text/javascript">
try { var yaCounter1598515 = new Ya.Metrika(1598515);
yaCounter1598515.clickmap();
yaCounter1598515.trackLinks({external: true});
} catch(e){}
</script></div>
<noscript><div style="position:absolute"><img src="//mc.yandex.ru/watch/1598515" alt="" /></div></noscript>
<!-- /Yandex.Metrika -->
{/literal}


</body>
</html>
