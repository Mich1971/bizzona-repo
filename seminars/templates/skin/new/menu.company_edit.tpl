
		<ul class="menu">
			<li class="active"><font color="#333333">{$aLang.company_admin}</font>
				<ul class="sub-menu">					
					<li {if $sMenuItemSelect=='profile'}class="active"{/if}><div><a href="{router page='company'}edit/{$oCompanyEdit->getId()}/">{$aLang.company_admin_profile}</a></div></li>
					<li {if $sMenuItemSelect=='admin'}class="active"{/if}><div><a href="{router page='company'}admin/{$oCompanyEdit->getId()}/">{$aLang.company_admin_users}</a></div></li>
					{hook run='menu_company_edit_admin_item'}
				</ul>
			</li>
			{hook run='menu_company_edit'}
		</ul>