		<ul class="menu">
		
			<li {if $sMenuItemSelect=='index'}class="active"{/if}>
				<a href="{cfg name='path.root.web'}/">{$aLang.company_menu_all}</a> {if $iCountSeminarsNew>0}+{$iCountSeminarsNew}{/if}
				{if $sMenuItemSelect=='index'}
					<ul class="sub-menu" >
						<li {if $sMenuSubItemSelect=='good'}class="active"{/if}><div><a href="{cfg name='path.root.web'}/">{$aLang.company_menu_all_good}</a></div></li>						
						{if $iCountSeminarsNew>0}<li {if $sMenuSubItemSelect=='new'}class="active"{/if}><div><a href="{router page='new'}">{$aLang.company_menu_all_new}</a> +{$iCountSeminarsNew}</div></li>{/if}
						{hook run='menu_company_index_item'}
					</ul>
				{/if}
			</li>
			
			<li {if $sMenuItemSelect=='company'}class="active"{/if}>
				<a href="{router page='company'}">{$aLang.company_menu_collective}</a> {if $iCountSeminarsCollectiveNew>0}+{$iCountSeminarsCollectiveNew}{/if}
				{if $sMenuItemSelect=='company'}
					<ul class="sub-menu" >											
						<li {if $sMenuSubItemSelect=='good'}class="active"{/if}><div><a href="{$sMenuSubCompanyUrl}">{$aLang.company_menu_collective_good}</a></div></li>
						{if $iCountSeminarsCompanyNew>0}<li {if $sMenuSubItemSelect=='new'}class="active"{/if}><div><a href="{$sMenuSubCompanyUrl}new/">{$aLang.company_menu_collective_new}</a> +{$iCountSeminarsCompanyNew}</div></li>{/if}
						<li {if $sMenuSubItemSelect=='bad'}class="active"{/if}><div><a href="{$sMenuSubCompanyUrl}bad/">{$aLang.company_menu_collective_bad}</a></div></li>
						{hook run='menu_company_company_item'}
					</ul>
				{/if}
			</li>
			
			<li {if $sMenuItemSelect=='log'}class="active"{/if}>
				<a href="{router page='personal_company'}">{$aLang.company_menu_personal}</a> {if $iCountSeminarsPersonalNew>0}+{$iCountSeminarsPersonalNew}{/if}
				{if $sMenuItemSelect=='log'}
					<ul class="sub-menu" style="left: -50px;">											
						<li {if $sMenuSubItemSelect=='good'}class="active"{/if}><div><a href="{router page='personal_company'}">{$aLang.company_menu_personal_good}</a></div></li>
						{if $iCountSeminarsPersonalNew>0}<li {if $sMenuSubItemSelect=='new'}class="active"{/if}><div><a href="{router page='personal_company'}new/">{$aLang.company_menu_personal_new}</a> +{$iCountSeminarsPersonalNew}</div></li>{/if}
						<li {if $sMenuSubItemSelect=='bad'}class="active"{/if}><div><a href="{router page='personal_company'}bad/">{$aLang.company_menu_personal_bad}</a></div></li>
						{hook run='menu_company_log_item'}
					</ul>
				{/if}
			</li>
			
			<li {if $sMenuItemSelect=='top'}class="active"{/if}>
				<a href="{router page='top'}">{$aLang.company_menu_top}</a>
				{if $sMenuItemSelect=='top'}
					<ul class="sub-menu" style="left: -80px;">											
						<li {if $sMenuSubItemSelect=='company'}class="active"{/if}><div><a href="{router page='top'}company/">{$aLang.company_menu_top_company}</a></div></li>
						<li {if $sMenuSubItemSelect=='seminar'}class="active"{/if}><div><a href="{router page='top'}seminar/">{$aLang.company_menu_top_seminar}</a></div></li>
						<li {if $sMenuSubItemSelect=='comment'}class="active"{/if}><div><a href="{router page='top'}comment/">{$aLang.company_menu_top_comment}</a></div></li>
						{hook run='menu_company_top_item'}
					</ul>
				{/if}
			</li>
			{hook run='menu_company'}
		</ul>
		
		
		

