				<table>
					<thead>
						<tr>
							<td class="user">{$aLang.companys_title}</td>
							{if $oUserCurrent}
							<td class="join-head"><img src="{cfg name='path.static.skin'}/images/join-head.gif" alt="" /></td>
							{/if}
							<td class="readers">{$aLang.companys_readers}</td>														
							<td class="rating">{$aLang.companys_rating}</td>
						</tr>
					</thead>
					
					<tbody>
						{foreach from=$aCompanys item=oCompany}
						{assign var="oUserOwner" value=$oCompany->getOwner()}
						<tr>
							<td class="name">
								<a href="{router page='company'}{$oCompany->getUrl()}/"><img src="{$oCompany->getAvatarPath(24)}" alt="" /></a>
								<a href="{router page='company'}{$oCompany->getUrl()}/" class="title {if $oCompany->getType()=='close'}close{/if}">{$oCompany->getTitle()|escape:'html'}</a><br />
								{$aLang.companys_owner}: <a href="{router page='profile'}{$oUserOwner->getLogin()}/" class="author">{$oUserOwner->getLogin()}</a>
							</td>
							{if $oUserCurrent}
							<td class="join {if $oCompany->getUserIsJoin()}active{/if}">
								{if $oUserCurrent->getId()!=$oCompany->getOwnerId() and $oCompany->getType()=='open'}
									<a href="#" onclick="ajaxJoinLeaveCompany(this,{$oCompany->getId()}); return false;"></a>
								{/if}
							</td>
							{/if}
							<td id="company_user_count_{$oCompany->getId()}" class="readers">{$oCompany->getCountUser()}</td>													
							<td class="rating"><strong>{$oCompany->getRating()}</strong></td>
						</tr>
						{/foreach}
					</tbody>
				</table>