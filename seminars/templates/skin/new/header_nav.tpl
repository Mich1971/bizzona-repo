	<!-- Navigation -->
	<div id="nav">
		<div class="left"></div>
		{if $oUserCurrent and ($sAction=='blog' or $sAction=='index' or $sAction=='new' or $sAction=='personal_blog')}
			<div class="write">
				<a href="{router page='seminar'}add/" alt="{$aLang.seminar_create}" title="{$aLang.seminar_create}" class="button small">
					<span><em>{$aLang.seminar_create}</em></span>
				</a>
			</div>
		{/if}
		
		{if $menu}
			{if in_array($menu,$aMenuContainers)}{$aMenuFetch.$menu}{else}{include file=menu.$menu.tpl}{/if}
		{/if}
				
		<div class="right"></div>
		<!--<a href="#" class="rss" onclick="return false;"></a>-->
	</div>
	<!-- /Navigation -->