				<ul class="poll">				
				{foreach from=$oSeminar->getQuestionAnswers() key=key item=aAnswer}	
				<li {if $oSeminar->getQuestionAnswerMax()==$aAnswer.count}class="most"{/if}>		
					<dl>
					<dt><span>{$oSeminar->getQuestionAnswerPercent($key)}%</span><br />({$aAnswer.count})</dt>
					<dd>{$aAnswer.text|escape:'html'}<br /><div style="width: {$oSeminar->getQuestionAnswerPercent($key)}%;" ><span></span></div></dd>
					</dl>
				</li>
				{/foreach}	
				</ul>						
				<span>{$aLang.seminar_question_vote_result}: {$oSeminar->getQuestionCountVote()}. {$aLang.seminar_question_abstain_result}: {$oSeminar->getQuestionCountVoteAbstain()}</span><br>
			