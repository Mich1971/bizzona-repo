{if count($aSeminars)>0}
	{foreach from=$aSeminars item=oSeminar}
			{assign var="oCompany" value=$oSeminar->getCompany()}
			{assign var="oUser" value=$oSeminar->getUser()}
			{assign var="oVote" value=$oSeminar->getVote()} 
			<!-- Seminar -->			
			<div class="topic">
				
				<div class="favorite {if $oUserCurrent}{if $oSeminar->getIsFavourite()}active{/if}{else}fav-guest{/if}"><a href="#" onclick="lsFavourite.toggle({$oSeminar->getId()},this,'seminar'); return false;"></a></div>
				
				<h1 class="title">		
					{if $oSeminar->getPublish()==0}	
						<img src="{cfg name='path.static.skin'}/images/seminar_unpublish.gif" border="0" title="{$aLang.seminar_unpublish}" width="16" height="16" alt="{$aLang.seminar_unpublish}">
					{/if}			
					<a href="{if $oSeminar->getType()=='link'}{router page='link'}go/{$oSeminar->getId()}/{else}{$oSeminar->getUrl()}{/if}">{$oSeminar->getTitle()|escape:'html'}</a>
					{if $oSeminar->getType()=='link'}
  						<img src="{cfg name='path.static.skin'}/images/link_url_big.gif" border="0" title="{$aLang.seminar_link}" width="16" height="16" alt="{$aLang.seminar_link}">
  					{/if}
				</h1>
				<ul class="action">
					<li><a href="{$oCompany->getUrlFull()}">{$oCompany->getTitle()|escape:'html'}</a>&nbsp;&nbsp;</li>
					{if $oUserCurrent and ($oUserCurrent->getId()==$oSeminar->getUserId() or $oUserCurrent->isAdministrator() or $oCompany->getUserIsAdministrator() or $oCompany->getUserIsModerator() or $oCompany->getOwnerId()==$oUserCurrent->getId())}
  						<li class="edit"><a href="{cfg name='path.root.web'}/{$oSeminar->getType()}/edit/{$oSeminar->getId()}/" title="{$aLang.seminar_edit}">{$aLang.seminar_edit}</a></li>
  					{/if}
					{if $oUserCurrent and ($oUserCurrent->isAdministrator() or $oCompany->getUserIsAdministrator() or $oCompany->getOwnerId()==$oUserCurrent->getId())}
							<li class="delete"><a href="{router page='seminar'}delete/{$oSeminar->getId()}/?security_ls_key={$LIVESTREET_SECURITY_KEY}" title="{$aLang.seminar_delete}" onclick="return confirm('{$aLang.seminar_delete_confirm}');">{$aLang.seminar_delete}</a></li>
					{/if}
				</ul>				
				<div class="content">
				
			{if $oSeminar->getType()=='question'}   
    		
    		<div id="seminar_question_area_{$oSeminar->getId()}">
    		{if !$oSeminar->getUserQuestionIsVote()} 		
    			<ul class="poll-new">	
				{foreach from=$oSeminar->getQuestionAnswers() key=key item=aAnswer}				
					<li><label for="seminar_answer_{$oSeminar->getId()}_{$key}"><input type="radio" id="seminar_answer_{$oSeminar->getId()}_{$key}" name="seminar_answer_{$oSeminar->getId()}"  value="{$key}" onchange="$('seminar_answer_{$oSeminar->getId()}_value').setProperty('value',this.value);"/> {$aAnswer.text|escape:'html'}</label></li>				
				{/foreach}
					<li>
					<input type="submit"  value="{$aLang.seminar_question_vote}" onclick="ajaxQuestionVote({$oSeminar->getId()},$('seminar_answer_{$oSeminar->getId()}_value').getProperty('value'));">
					<input type="submit"  value="{$aLang.seminar_question_abstain}"  onclick="ajaxQuestionVote({$oSeminar->getId()},-1)">
					</li>				
					<input type="hidden" id="seminar_answer_{$oSeminar->getId()}_value" value="-1">				
				</ul>				
				<span>{$aLang.seminar_question_vote_result}: {$oSeminar->getQuestionCountVote()}. {$aLang.seminar_question_abstain_result}: {$oSeminar->getQuestionCountVoteAbstain()}</span><br>			
			{else}			
				{include file='seminar_question.tpl'}
			{/if}
			</div>
			<br>	
						
    		{/if}
				
					{$oSeminar->getTextShort()}
					{if $oSeminar->getTextShort()!=$oSeminar->getText()}
      					<br><br>( <a href="{$oSeminar->getUrl()}" title="{$aLang.seminar_read_more}">
      					{if $oSeminar->getCutText()}
      						{$oSeminar->getCutText()}
      					{else}
      						{$aLang.seminar_read_more}
      					{/if}      			
      					</a> )
      				{/if}
				</div>				
				<ul class="tags">
					{foreach from=$oSeminar->getTagsArray() item=sTag name=tags_list}
						<li><a href="{router page='tag'}{$sTag|escape:'html'}/">{$sTag|escape:'html'}</a>{if !$smarty.foreach.tags_list.last}, {/if}</li>
					{/foreach}								
				</ul>				
				<ul class="voting {if $oVote || ($oUserCurrent && $oSeminar->getUserId()==$oUserCurrent->getId()) || strtotime($oSeminar->getDateAdd())<$smarty.now-$oConfig->GetValue('acl.vote.seminar.limit_time')}{if $oSeminar->getRating()>0}positive{elseif $oSeminar->getRating()<0}negative{/if}{/if} {if !$oUserCurrent || $oSeminar->getUserId()==$oUserCurrent->getId() || strtotime($oSeminar->getDateAdd())<$smarty.now-$oConfig->GetValue('acl.vote.seminar.limit_time')}guest{/if} {if $oVote} voted {if $oVote->getDirection()>0}plus{elseif $oVote->getDirection()<0}minus{/if}{/if}">
					<li class="plus"><a href="#" onclick="lsVote.vote({$oSeminar->getId()},this,1,'seminar'); return false;"></a></li>
					<li class="total" title="{$aLang.seminar_vote_count}: {$oSeminar->getCountVote()}">{if $oVote || ($oUserCurrent && $oSeminar->getUserId()==$oUserCurrent->getId()) || strtotime($oSeminar->getDateAdd())<$smarty.now-$oConfig->GetValue('acl.vote.seminar.limit_time')} {if $oSeminar->getRating()>0}+{/if}{$oSeminar->getRating()} {else} <a href="#" onclick="lsVote.vote({$oSeminar->getId()},this,0,'seminar'); return false;">&mdash;</a> {/if}</li>
					<li class="minus"><a href="#" onclick="lsVote.vote({$oSeminar->getId()},this,-1,'seminar'); return false;"></a></li>
					<li class="date">{date_format date=$oSeminar->getDateAdd()}</li>
					{if $oSeminar->getType()=='link'}
						<li class="link"><a href="{router page='link'}go/{$oSeminar->getId()}/" title="{$aLang.seminar_link_count_jump}: {$oSeminar->getLinkCountJump()}">{$oSeminar->getLinkUrl(true)}</a></li>
					{/if}
					<li class="author"><a href="{$oUser->getUserWebPath()}">{$oUser->getLogin()}</a></li>		
					<li class="comments-total">
						{if $oSeminar->getCountComment()>0}
							<a href="{$oSeminar->getUrl()}#comments" title="{$aLang.seminar_comment_read}"><span class="red">{$oSeminar->getCountComment()}</span>{if $oSeminar->getCountCommentNew()}<span class="green">+{$oSeminar->getCountCommentNew()}</span>{/if}</a>
						{else}
							<a href="{$oSeminar->getUrl()}#comments" title="{$aLang.seminar_comment_add}"><span class="red">{$aLang.seminar_comment_add}</span></a>
						{/if}
					</li>
					{hook run='seminar_show_info' seminar=$oSeminar}
				</ul>
			</div>
			<!-- /Seminar -->
	{/foreach}	
		
    {include file='paging.tpl' aPaging=`$aPaging`}
	
{else}
{$aLang.company_no_seminar}
{/if}