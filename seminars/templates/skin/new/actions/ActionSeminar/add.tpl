{include file='header.tpl' menu='seminar_action' showWhiteBack=true}


{literal}
<script language="JavaScript" type="text/javascript">
document.addEvent('domready', function() {	
	new Autocompleter.Request.HTML($('seminar_tags'), DIR_WEB_ROOT+'/include/ajax/tagAutocompleter.php?security_ls_key='+LIVESTREET_SECURITY_KEY, {
		'indicatorClass': 'autocompleter-loading', // class added to the input during request
		'minLength': 2, // We need at least 1 character
		'selectMode': 'pick', // Instant completion
		'multiple': true // Tag support, by default comma separated
	}); 
});
</script>
{/literal}


{if $oConfig->GetValue('view.tinymce')}
<script type="text/javascript" src="{cfg name='path.root.engine_lib'}/external/tinymce_3.2.7/tiny_mce.js"></script>

<script type="text/javascript">
{literal}
tinyMCE.init({
	mode : "textareas",
	theme : "advanced",
	theme_advanced_toolbar_location : "top",
	theme_advanced_toolbar_align : "left",
	theme_advanced_buttons1 : "lshselect,bold,italic,underline,strikethrough,|,bullist,numlist,|,undo,redo,|,lslink,unlink,lsvideo,lsimage,pagebreak,code",
	theme_advanced_buttons2 : "",
	theme_advanced_buttons3 : "",
	theme_advanced_statusbar_location : "bottom",
	theme_advanced_resizing : true,
	theme_advanced_resize_horizontal : 0,
	theme_advanced_resizing_use_cookie : 0,
	theme_advanced_path : false,
	object_resizing : true,
	force_br_newlines : true,
    forced_root_block : '', // Needed for 3.x
    force_p_newlines : false,    
    plugins : "lseditor,safari,inlinepopups,media,pagebreak",
    convert_urls : false,
    extended_valid_elements : "embed[src|type|allowscriptaccess|allowfullscreen|width|height]",
    pagebreak_separator :"<cut>",
    media_strict : false,
    language : TINYMCE_LANG
});
{/literal}
</script>

{else}
	{include file='window_load_img.tpl' sToLoad='seminar_text'}
{/if}


			<div class="seminar" style="display: none;">
				<div class="content" id="text_preview"></div>
			</div>

			<div class="profile-user">
				{if $sEvent=='add'}
					<h1>{$aLang.seminar_seminar_create}</h1>
				{else}
					<h1>{$aLang.seminar_seminar_edit}</h1>
				{/if}
				<form action="" method="POST" enctype="multipart/form-data">
					{hook run='form_add_seminar_seminar_begin'}
					<input type="hidden" name="security_ls_key" value="{$LIVESTREET_SECURITY_KEY}" /> 
					
					<p><label for="company_id">{$aLang.seminar_create_company}</label>
					<select name="company_id" id="company_id" onChange="ajaxCompanyInfo(this.value);">
     					<option value="0">{$aLang.seminar_create_company_personal}</option>
     					{foreach from=$aCompanysAllow item=oCompany}
     						<option value="{$oCompany->getId()}" {if $_aRequest.company_id==$oCompany->getId()}selected{/if}>{$oCompany->getTitle()}</option>
     					{/foreach}     					
     				</select></p>
					
     				<script language="JavaScript" type="text/javascript">
     					ajaxCompanyInfo($('company_id').value);
     				</script>
					
					<p><label for="seminar_title">{$aLang.seminar_create_title}:</label><br />
					<input type="text" id="seminar_title" name="seminar_title" value="{$_aRequest.seminar_title}" class="w100p" /><br />
					</p>

					<p>{if !$oConfig->GetValue('view.tinymce')}<div class="note">{$aLang.seminar_create_text_notice}</div>{/if}<label for="seminar_text">{$aLang.seminar_create_text}:</label>
					{if !$oConfig->GetValue('view.tinymce')}
            			<div class="panel_form">
							<select onchange="lsPanel.putTagAround('seminar_text',this.value); this.selectedIndex=0; return false;" style="width: 91px;">
            					<option value="">{$aLang.panel_title}</option>
            					<option value="h4">{$aLang.panel_title_h4}</option>
            					<option value="h5">{$aLang.panel_title_h5}</option>
            					<option value="h6">{$aLang.panel_title_h6}</option>
            				</select>            			
            				<select onchange="lsPanel.putList('seminar_text',this); return false;">
            					<option value="">{$aLang.panel_list}</option>
            					<option value="ul">{$aLang.panel_list_ul}</option>
            					<option value="ol">{$aLang.panel_list_ol}</option>
            				</select>
	 						<a href="#" onclick="lsPanel.putTagAround('seminar_text','b'); return false;" class="button"><img src="{cfg name='path.static.skin'}/images/panel/bold_ru.gif" width="20" height="20" title="{$aLang.panel_b}"></a>
	 						<a href="#" onclick="lsPanel.putTagAround('seminar_text','i'); return false;" class="button"><img src="{cfg name='path.static.skin'}/images/panel/italic_ru.gif" width="20" height="20" title="{$aLang.panel_i}"></a>	 			
	 						<a href="#" onclick="lsPanel.putTagAround('seminar_text','u'); return false;" class="button"><img src="{cfg name='path.static.skin'}/images/panel/underline_ru.gif" width="20" height="20" title="{$aLang.panel_u}"></a>	 			
	 						<a href="#" onclick="lsPanel.putTagAround('seminar_text','s'); return false;" class="button"><img src="{cfg name='path.static.skin'}/images/panel/strikethrough.gif" width="20" height="20" title="{$aLang.panel_s}"></a>	 			
	 						&nbsp;
	 						<a href="#" onclick="lsPanel.putTagUrl('seminar_text','{$aLang.panel_url_promt}'); return false;" class="button"><img src="{cfg name='path.static.skin'}/images/panel/link.gif" width="20" height="20"  title="{$aLang.panel_url}"></a>
	 						<a href="#" onclick="lsPanel.putQuote('seminar_text'); return false;" class="button"><img src="{cfg name='path.static.skin'}/images/panel/quote.gif" width="20" height="20" title="{$aLang.panel_quote}"></a>
	 						<a href="#" onclick="lsPanel.putTagAround('seminar_text','code'); return false;" class="button"><img src="{cfg name='path.static.skin'}/images/panel/code.gif" width="30" height="20" title="{$aLang.panel_code}"></a>
	 						<a href="#" onclick="lsPanel.putTagAround('seminar_text','video'); return false;" class="button"><img src="{cfg name='path.static.skin'}/images/panel/video.gif" width="20" height="20" title="{$aLang.panel_video}"></a>
	 				
	 						<a href="#" onclick="showImgUploadForm(); return false;" class="button"><img src="{cfg name='path.static.skin'}/images/panel/img.gif" width="20" height="20" title="{$aLang.panel_image}"></a> 			
	 						<a href="#" onclick="lsPanel.putText('seminar_text','<cut>'); return false;" class="button"><img src="{cfg name='path.static.skin'}/images/panel/cut.gif" width="20" height="20" title="{$aLang.panel_cut}"></a>	
	 					</div>
	 				{/if}
					<textarea name="seminar_text" id="seminar_text" rows="20">{$_aRequest.seminar_text}</textarea></p>
					
					<p><label for="company_id">{$aLang.seminar_lectors}</label>
					<select name="lectors_id" id="lectors_id" multiple='multiple'>
					{foreach from=$aCompanyModerators item=oCompanyUser}  
 						{assign var="oUser" value=$oCompanyUser->getUser()}					
						<option>{$oUser->getLogin()}</option>
					{/foreach}
     				</select></p>					
					
					<p><label for="seminar_tags">{$aLang.seminar_create_tags}:</label><br />
					<input type="text" id="seminar_tags" name="seminar_tags" value="{$_aRequest.seminar_tags}" class="w100p" /><br />
       				<span class="form_note">{$aLang.seminar_create_tags_notice}</span></p>
												
					<p><label for="seminar_forbid_comment"><input type="checkbox" id="seminar_forbid_comment" name="seminar_forbid_comment" class="checkbox" value="1" {if $_aRequest.seminar_forbid_comment==1}checked{/if}/> 
					&mdash; {$aLang.seminar_create_forbid_comment}</label><br />
					<span class="form_note">{$aLang.seminar_create_forbid_comment_notice}</span></p>

					{if $oUserCurrent->isAdministrator()}
						<p><label for="seminar_publish_index"><input type="checkbox" id="seminar_publish_index" name="seminar_publish_index" class="checkbox" value="1" {if $_aRequest.seminar_publish_index==1}checked{/if}/> 
						&mdash; {$aLang.seminar_create_publish_index}</label><br />
						<span class="form_note">{$aLang.seminar_create_publish_index_notice}</span></p>
					{/if}
					
					{hook run='form_add_seminar_seminar_end'}					
					<p class="buttons">
					<input type="submit" name="submit_seminar_publish" value="{$aLang.seminar_create_submit_publish}" class="right" />
					<input type="submit" name="submit_preview" value="{$aLang.seminar_create_submit_preview}" onclick="$('text_preview').getParent('div').setStyle('display','block'); ajaxTextPreview('seminar_text',false); return false;" />&nbsp;
					<input type="submit" name="submit_seminar_save" value="{$aLang.seminar_create_submit_save}" />
					</p>
				</form>

			</div>


{include file='footer.tpl'}

