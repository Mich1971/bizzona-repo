{include file='header.tpl' menu='company_edit' showWhiteBack=true}



			<h1>{$aLang.company_admin}: <a href="{router page='company'}{$oCompanyEdit->getUrl()}/">{$oCompanyEdit->getTitle()}</a></h1>

		{if $aCompanyUsers}
			<form action="" method="POST" enctype="multipart/form-data">
				<input type="hidden" name="security_ls_key" value="{$LIVESTREET_SECURITY_KEY}" />
				<table class="table-company-users">
					<thead>
						<tr>
							<td></td>
							<td width="10%">{$aLang.company_admin_users_administrator}</td>
							<td width="10%">{$aLang.company_admin_users_moderator}</td>
							<td width="10%">{$aLang.company_admin_users_reader}</td>
							<td width="10%">{$aLang.company_admin_users_bun}</td>
						</tr>
					</thead>
					<tbody>
						{foreach from=$aCompanyUsers item=oCompanyUser}
						{assign var="oUser" value=$oCompanyUser->getUser()}
						<tr>
							<td class="username"><a href="{router page='profile'}{$oUser->getLogin()}/">{$oUser->getLogin()}</a></td>
							{if $oUser->getId()==$oUserCurrent->getId()}
							<td colspan="3" align="center">{$aLang.company_admin_users_current_administrator}</td>
							{else}
								<td><input type="radio" name="user_rank[{$oUser->getId()}]"  value="administrator" {if $oCompanyUser->getIsAdministrator()}checked{/if}/></td>
								<td><input type="radio" name="user_rank[{$oUser->getId()}]"  value="moderator" {if $oCompanyUser->getIsModerator()}checked{/if}/></td>
								<td><input type="radio" name="user_rank[{$oUser->getId()}]"  value="reader" {if $oCompanyUser->getUserRole()==$BLOG_USER_ROLE_USER}checked{/if}/></td>
								<td><input type="radio" name="user_rank[{$oUser->getId()}]"  value="ban" {if $oCompanyUser->getUserRole()==$BLOG_USER_ROLE_BAN}checked{/if}/></td>						
							{/if}
						</tr>
						{/foreach}						
					</tbody>
				</table>
								
				<input type="submit" name="submit_company_admin" value="{$aLang.company_admin_users_submit}">
			</form>
		{else}
			{$aLang.company_admin_users_empty} 
		{/if}

{include file='footer.tpl'}