{include file='header.tpl' menu='company'}

 
{assign var="oUserOwner" value=$oCompany->getOwner()}



{assign var="oVote" value=$oCompany->getVote()}

{literal}
<script language="JavaScript" type="text/javascript">
function toggleCompanyInfo(id,link) {
	link=$(link);
	var obj=$(id);	
	var slideObj = new Fx.Slide(obj);
	if (obj.getStyle('display')=='none') {
		slideObj.hide();
		obj.setStyle('display','block');		
	}	
	link.toggleClass('inactive');
	slideObj.toggle();
}

function toggleCompanyDeleteForm(id,link) {
	link=$(link);
	var obj=$(id);	
	var slideObj = new Fx.Slide(obj);
	if (obj.getStyle('display')=='none') {
		slideObj.hide();
		obj.setStyle('display','block');
	}	
	link.toggleClass('inactive');
	slideObj.toggle();
	
}
</script>
{/literal}


			<div class="profile-blog">							
				<div class="voting {if $oCompany->getRating()>=0}positive{else}negative{/if} {if !$oUserCurrent || $oCompany->getOwnerId()==$oUserCurrent->getId()}guest{/if} {if $oVote} voted {if $oVote->getDirection()>0}plus{elseif $oVote->getDirection()<0}minus{/if}{/if}">
					<div class="clear">{$aLang.company_rating}</div>
					
					<a href="#" class="plus" onclick="lsVote.vote({$oCompany->getId()},this,1,'company'); return false;"></a>
					<div class="total">{if $oCompany->getRating()>0}+{/if}{$oCompany->getRating()}</div>
					<a href="#" class="minus" onclick="lsVote.vote({$oCompany->getId()},this,-1,'company'); return false;"></a>
					
					<div class="clear"></div>
					<div class="text">{$aLang.company_vote_count}:</div><div class="count">{$oCompany->getCountVote()}</div>
				</div>

				<img src="{$oCompany->getAvatarPath(24)}" alt="avatar" class="avatar" />
				<h1 class="title"><a href="#" class="title-link" onclick="toggleCompanyInfo('company_about_{$oCompany->getId()}',this); return false;"><span>{$oCompany->getTitle()|escape:'html'}</span><strong>&nbsp;&nbsp;</strong></a></h1>
				<ul class="action">
					<li class="rss"><a href="{router page='rss'}company/{$oCompany->getUrl()}/"></a></li>					
					{if $oUserCurrent and $oUserCurrent->getId()!=$oCompany->getOwnerId()}
						<li class="join {if $oCompany->getUserIsJoin()}active{/if}">
							<a href="#" onclick="ajaxJoinLeaveCompany(this,{$oCompany->getId()}); return false;"></a>
						</li>
					{/if}
					{if $oUserCurrent and ($oUserCurrent->getId()==$oCompany->getOwnerId() or $oUserCurrent->isAdministrator() or $oCompany->getUserIsAdministrator() )}
  						<li class="edit"><a href="{router page='company'}edit/{$oCompany->getId()}/" title="{$aLang.company_edit}">{$aLang.company_edit}</a></li>
 						{if $oUserCurrent->isAdministrator()}
							<li class="delete">
								<a href="#" title="{$aLang.company_delete}" onclick="toggleCompanyDeleteForm('company_delete_form',this); return false;">{$aLang.company_delete}</a> 
								<form id="company_delete_form" class="hidden" action="{router page='company'}delete/{$oCompany->getId()}/" method="POST">
									<input type="hidden" value="{$LIVESTREET_SECURITY_KEY}" name="security_ls_key" /> 
									{$aLang.company_admin_delete_move}:<br /> 
									<select name="seminar_move_to">
										<option value="-1">{$aLang.company_delete_clear}</option>
										{if $aCompanys} 
											<option disabled="disabled">-------------</option>
											{foreach from=$aCompanys item=oCompanyDelete}
												<option value="{$oCompanyDelete->getId()}">{$oCompanyDelete->getTitle()}</option>											
											{/foreach}
										{/if}
									</select>
									<input type="submit" value="{$aLang.company_delete}" />
								</form></li>						
						{else} 						
  							<li class="delete"><a href="{router page='company'}delete/{$oCompany->getId()}/?security_ls_key={$LIVESTREET_SECURITY_KEY}" title="{$aLang.company_delete}" onclick="return confirm('{$aLang.company_admin_delete_confirm}');" >{$aLang.company_delete}</a></li>
  						{/if}
  					{/if}
				</ul>
				<div class="about" id="company_about_{$oCompany->getId()}" style="display: none;" >
					<div class="tl"><div class="tr"></div></div>

					<div class="content">
					
						<h1>{$aLang.company_about}</h1>
						<p>
						{$oCompany->getDescription()|nl2br}
						</p>					
						
						<div class="line"></div>
						
						<div class="admins">
							<h1>{$aLang.company_user_administrators} ({$iCountCompanyAdministrators})</h1>							
							
							<ul class="admin-list">				
								<li>
									<dl>
										<dt>
											<a href="{$oUserOwner->getUserWebPath()}"><img src="{$oUserOwner->getProfileAvatarPath(48)}" alt=""  title="{$oUserOwner->getLogin()}"/></a>
										</dt>
										<dd>
											<a href="{$oUserOwner->getUserWebPath()}">{$oUserOwner->getLogin()}</a>
										</dd>
									</dl>
								</li>
								{if $aCompanyAdministrators}			
 								{foreach from=$aCompanyAdministrators item=oCompanyUser}
 								{assign var="oUser" value=$oCompanyUser->getUser()}  									
								<li>
									<dl>
										<dt>
											<a href="{$oUser->getUserWebPath()}"><img src="{$oUser->getProfileAvatarPath(48)}" alt=""  title="{$oUser->getLogin()}"/></a>
										</dt>
										<dd>
											<a href="{$oUser->getUserWebPath()}">{$oUser->getLogin()}</a>
										</dd>
									</dl>
								</li>
								{/foreach}	
								{/if}						
							</ul>
							
						</div>

						
						<div class="moderators">
							<h1>{$aLang.company_user_moderators} ({$iCountCompanyModerators})</h1>
							{if $aCompanyModerators}
							<ul class="admin-list">							
 								{foreach from=$aCompanyModerators item=oCompanyUser}  
 								{assign var="oUser" value=$oCompanyUser->getUser()}									
								<li>
									<dl>
										<dt>
											<a href="{$oUser->getUserWebPath()}"><img src="{$oUser->getProfileAvatarPath(48)}" alt="" title="{$oUser->getLogin()}" /></a>
										</dt>
										<dd>
											<a href="{$oUser->getUserWebPath()}">{$oUser->getLogin()}</a>
										</dd>
									</dl>
								</li>
								{/foreach}							
							</ul>
							{else}
   	 							{$aLang.company_user_moderators_empty}
							{/if}
						</div>
						
						<h1 class="readers">{$aLang.company_user_readers} ({$iCountCompanyUsers})</h1>
						{if $aCompanyUsers}
						<ul class="reader-list">
							{foreach from=$aCompanyUsers item=oCompanyUser}
							{assign var="oUser" value=$oCompanyUser->getUser()}
								<li><a href="{$oUser->getUserWebPath()}">{$oUser->getLogin()}</a></li>
							{/foreach}
						</ul>
						{else}
   	 						{$aLang.company_user_readers_empty}
    					{/if}
					</div>
					<div class="bl"><div class="br"></div></div>
				</div>				
			</div>


{if $bCloseCompany}
	<div class="topic">
		{$aLang.company_close_show}
	</div>
{else}
	{include file='seminar_list.tpl'}
{/if}

{include file='footer.tpl'}