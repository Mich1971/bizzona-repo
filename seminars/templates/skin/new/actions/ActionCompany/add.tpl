{if $sEvent=='add'}
	{include file='header.tpl' menu='seminar_action' showWhiteBack=true}
{else}
	{include file='header.tpl' menu='company_edit' showWhiteBack=true}
{/if}

		{if $sEvent=='add'}
			<h1>{$aLang.company_create}</h1>
		{else}
			<h1>{$aLang.company_admin}: <a href="{router page='company'}{$oCompanyEdit->getUrl()}/">{$oCompanyEdit->getTitle()}</a></h1>
		{/if}
		<form action="" method="POST" enctype="multipart/form-data">
			{hook run='form_add_company_begin'}
			<input type="hidden" name="security_ls_key" value="{$LIVESTREET_SECURITY_KEY}" /> 
				
			<p><label for="company_title">{$aLang.company_create_title}:</label><br />
			<input type="text" id="company_title" name="company_title" value="{$_aRequest.company_title}" class="w100p" /><br />
			<span class="form_note">{$aLang.company_create_title_notice}</span></p>

			<p><label for="company_url">{$aLang.company_create_url}:</label><br />
			<input type="text" id="company_url" name="company_url" value="{$_aRequest.company_url}" class="w100p"  {if $_aRequest.company_id}disabled{/if} /><br />
			<span class="form_note">{$aLang.company_create_url_notice}</span></p>
			
			<p><label for="company_type">{$aLang.company_create_type}:</label><br />
			<select name="company_type" id="company_type" onChange="">
				<option value="open" {if $_aRequest.company_type=='open'}selected{/if}>{$aLang.company_create_type_open}</option>
				<option value="close" {if $_aRequest.company_type=='close'}selected{/if}>{$aLang.company_create_type_close}</option>
			</select><br />
			<span class="form_note">{$aLang.company_create_type_open_notice}</span></p>

			<p><label for="company_description">{$aLang.company_create_description}:</label><br />
			<textarea name="company_description" id="company_description" rows="20">{$_aRequest.company_description}</textarea><br />
			<span class="form_note">{$aLang.company_create_description_notice}</span></p>
			
			<p><label for="company_limit_rating_seminar">{$aLang.company_create_rating}:</label><br />
			<input type="text" id="company_limit_rating_seminar" name="company_limit_rating_seminar" value="{$_aRequest.company_limit_rating_seminar}" class="w100p" /><br />
			<span class="form_note">{$aLang.company_create_rating_notice}</span></p>
				
			<p>
			{if $oCompanyEdit and $oCompanyEdit->getAvatar()}
				<img src="{$oCompanyEdit->getAvatarPath(48)}" />
				<img src="{$oCompanyEdit->getAvatarPath(24)}" />
				<label for="avatar_delete"><input type="checkbox" id="avatar_delete" name="avatar_delete" value="on"> &mdash; {$aLang.company_create_avatar_delete}</label><br /><br />
			{/if}
			<label for="avatar">{$aLang.company_create_avatar}:</label><br />
			<input type="file" name="avatar" id="avatar"></p>
					
			{hook run='form_add_company_end'}		
			<p><input type="submit" name="submit_company_add" value="{$aLang.company_create_submit}">						
			<input type="hidden" name="company_id" value="{$_aRequest.company_id}"></p>
			
		</form>

{include file='footer.tpl'}