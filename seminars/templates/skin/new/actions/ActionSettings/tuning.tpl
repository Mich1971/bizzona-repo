{include file='header.tpl' menu='settings' showWhiteBack=true}

			<h1>{$aLang.settings_tuning}</h1>
			<strong>{$aLang.settings_tuning_notice}</strong>
			<form action="{router page='settings'}tuning/" method="POST" enctype="multipart/form-data">
				{hook run='form_settings_tuning_begin'}
				<input type="hidden" name="security_ls_key" value="{$LIVESTREET_SECURITY_KEY}" /> 
				<p>
					<label for="settings_notice_new_topic"><input {if $oUserCurrent->getSettingsNoticeNewTopic()}checked{/if}  type="checkbox" id="settings_notice_new_topic" name="settings_notice_new_topic" value="1" class="checkbox" /> &mdash; {$aLang.settings_tuning_notice_new_topic}</label><br />
					<label for="settings_notice_new_comment"><input {if $oUserCurrent->getSettingsNoticeNewComment()}checked{/if} type="checkbox"   id="settings_notice_new_comment" name="settings_notice_new_comment" value="1" class="checkbox" /> &mdash; {$aLang.settings_tuning_notice_new_comment}</label><br />
					<label for="settings_notice_new_talk"><input {if $oUserCurrent->getSettingsNoticeNewTalk()}checked{/if} type="checkbox" id="settings_notice_new_talk" name="settings_notice_new_talk" value="1" class="checkbox" /> &mdash; {$aLang.settings_tuning_notice_new_talk}</label><br />
					<label for="settings_notice_reply_comment"><input {if $oUserCurrent->getSettingsNoticeReplyComment()}checked{/if} type="checkbox" id="settings_notice_reply_comment" name="settings_notice_reply_comment" value="1" class="checkbox" /> &mdash; {$aLang.settings_tuning_notice_reply_comment}</label><br />
					<label for="settings_notice_new_friend"><input {if $oUserCurrent->getSettingsNoticeNewFriend()}checked{/if} type="checkbox" id="settings_notice_new_friend" name="settings_notice_new_friend" value="1" class="checkbox" /> &mdash; {$aLang.settings_tuning_notice_new_friend}</label><br />
					<label for="settings_notice_new_qacomment"><input {if $oUserCurrent->getSettingsNoticeNewQaComment()}checked{/if} type="checkbox" id="settings_notice_new_qacomment" name="settings_notice_new_qacomment" value="1" class="checkbox" /> &mdash; {$aLang.settings_tuning_notice_new_qacomment}</label><br />
					<label for="settings_notice_new_bicomment"><input {if $oUserCurrent->getSettingsNoticeNewBiComment()}checked{/if} type="checkbox" id="settings_notice_new_bicomment" name="settings_notice_new_bicomment" value="1" class="checkbox" /> &mdash; {$aLang.settings_tuning_notice_new_bicomment}</label><br />
					<label for="settings_notice_new_bacomment"><input {if $oUserCurrent->getSettingsNoticeNewBaComment()}checked{/if} type="checkbox" id="settings_notice_new_bacomment" name="settings_notice_new_bacomment" value="1" class="checkbox" /> &mdash; {$aLang.settings_tuning_notice_new_bacomment}</label><br />
					<label for="settings_notice_new_bbcomment"><input {if $oUserCurrent->getSettingsNoticeNewBbComment()}checked{/if} type="checkbox" id="settings_notice_new_bbcomment" name="settings_notice_new_bbcomment" value="1" class="checkbox" /> &mdash; {$aLang.settings_tuning_notice_new_bbcomment}</label><br />
					<label for="settings_notice_new_bscomment"><input {if $oUserCurrent->getSettingsNoticeNewBsComment()}checked{/if} type="checkbox" id="settings_notice_new_bscomment" name="settings_notice_new_bscomment" value="1" class="checkbox" /> &mdash; {$aLang.settings_tuning_notice_new_bscomment}</label>
				</p>
				{hook run='form_settings_tuning_end'}
				<p><input type="submit" name="submit_settings_tuning" value="{$aLang.settings_tuning_submit}" /></p>				
			</form>

{include file='footer.tpl'}