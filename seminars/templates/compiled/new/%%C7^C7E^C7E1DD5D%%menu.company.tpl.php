<?php /* Smarty version 2.6.19, created on 2011-02-03 18:46:16
         compiled from menu.company.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'cfg', 'menu.company.tpl', 4, false),array('function', 'router', 'menu.company.tpl', 8, false),array('function', 'hook', 'menu.company.tpl', 9, false),)), $this); ?>
		<ul class="menu">
		
			<li <?php if ($this->_tpl_vars['sMenuItemSelect'] == 'index'): ?>class="active"<?php endif; ?>>
				<a href="<?php echo smarty_function_cfg(array('name' => 'path.root.web'), $this);?>
/"><?php echo $this->_tpl_vars['aLang']['company_menu_all']; ?>
</a> <?php if ($this->_tpl_vars['iCountSeminarsNew'] > 0): ?>+<?php echo $this->_tpl_vars['iCountSeminarsNew']; ?>
<?php endif; ?>
				<?php if ($this->_tpl_vars['sMenuItemSelect'] == 'index'): ?>
					<ul class="sub-menu" >
						<li <?php if ($this->_tpl_vars['sMenuSubItemSelect'] == 'good'): ?>class="active"<?php endif; ?>><div><a href="<?php echo smarty_function_cfg(array('name' => 'path.root.web'), $this);?>
/"><?php echo $this->_tpl_vars['aLang']['company_menu_all_good']; ?>
</a></div></li>						
						<?php if ($this->_tpl_vars['iCountSeminarsNew'] > 0): ?><li <?php if ($this->_tpl_vars['sMenuSubItemSelect'] == 'new'): ?>class="active"<?php endif; ?>><div><a href="<?php echo smarty_function_router(array('page' => 'new'), $this);?>
"><?php echo $this->_tpl_vars['aLang']['company_menu_all_new']; ?>
</a> +<?php echo $this->_tpl_vars['iCountSeminarsNew']; ?>
</div></li><?php endif; ?>
						<?php echo smarty_function_hook(array('run' => 'menu_company_index_item'), $this);?>

					</ul>
				<?php endif; ?>
			</li>
			
			<li <?php if ($this->_tpl_vars['sMenuItemSelect'] == 'company'): ?>class="active"<?php endif; ?>>
				<a href="<?php echo smarty_function_router(array('page' => 'company'), $this);?>
"><?php echo $this->_tpl_vars['aLang']['company_menu_collective']; ?>
</a> <?php if ($this->_tpl_vars['iCountSeminarsCollectiveNew'] > 0): ?>+<?php echo $this->_tpl_vars['iCountSeminarsCollectiveNew']; ?>
<?php endif; ?>
				<?php if ($this->_tpl_vars['sMenuItemSelect'] == 'company'): ?>
					<ul class="sub-menu" >											
						<li <?php if ($this->_tpl_vars['sMenuSubItemSelect'] == 'good'): ?>class="active"<?php endif; ?>><div><a href="<?php echo $this->_tpl_vars['sMenuSubCompanyUrl']; ?>
"><?php echo $this->_tpl_vars['aLang']['company_menu_collective_good']; ?>
</a></div></li>
						<?php if ($this->_tpl_vars['iCountSeminarsCompanyNew'] > 0): ?><li <?php if ($this->_tpl_vars['sMenuSubItemSelect'] == 'new'): ?>class="active"<?php endif; ?>><div><a href="<?php echo $this->_tpl_vars['sMenuSubCompanyUrl']; ?>
new/"><?php echo $this->_tpl_vars['aLang']['company_menu_collective_new']; ?>
</a> +<?php echo $this->_tpl_vars['iCountSeminarsCompanyNew']; ?>
</div></li><?php endif; ?>
						<li <?php if ($this->_tpl_vars['sMenuSubItemSelect'] == 'bad'): ?>class="active"<?php endif; ?>><div><a href="<?php echo $this->_tpl_vars['sMenuSubCompanyUrl']; ?>
bad/"><?php echo $this->_tpl_vars['aLang']['company_menu_collective_bad']; ?>
</a></div></li>
						<?php echo smarty_function_hook(array('run' => 'menu_company_company_item'), $this);?>

					</ul>
				<?php endif; ?>
			</li>
			
			<li <?php if ($this->_tpl_vars['sMenuItemSelect'] == 'log'): ?>class="active"<?php endif; ?>>
				<a href="<?php echo smarty_function_router(array('page' => 'personal_company'), $this);?>
"><?php echo $this->_tpl_vars['aLang']['company_menu_personal']; ?>
</a> <?php if ($this->_tpl_vars['iCountSeminarsPersonalNew'] > 0): ?>+<?php echo $this->_tpl_vars['iCountSeminarsPersonalNew']; ?>
<?php endif; ?>
				<?php if ($this->_tpl_vars['sMenuItemSelect'] == 'log'): ?>
					<ul class="sub-menu" style="left: -50px;">											
						<li <?php if ($this->_tpl_vars['sMenuSubItemSelect'] == 'good'): ?>class="active"<?php endif; ?>><div><a href="<?php echo smarty_function_router(array('page' => 'personal_company'), $this);?>
"><?php echo $this->_tpl_vars['aLang']['company_menu_personal_good']; ?>
</a></div></li>
						<?php if ($this->_tpl_vars['iCountSeminarsPersonalNew'] > 0): ?><li <?php if ($this->_tpl_vars['sMenuSubItemSelect'] == 'new'): ?>class="active"<?php endif; ?>><div><a href="<?php echo smarty_function_router(array('page' => 'personal_company'), $this);?>
new/"><?php echo $this->_tpl_vars['aLang']['company_menu_personal_new']; ?>
</a> +<?php echo $this->_tpl_vars['iCountSeminarsPersonalNew']; ?>
</div></li><?php endif; ?>
						<li <?php if ($this->_tpl_vars['sMenuSubItemSelect'] == 'bad'): ?>class="active"<?php endif; ?>><div><a href="<?php echo smarty_function_router(array('page' => 'personal_company'), $this);?>
bad/"><?php echo $this->_tpl_vars['aLang']['company_menu_personal_bad']; ?>
</a></div></li>
						<?php echo smarty_function_hook(array('run' => 'menu_company_log_item'), $this);?>

					</ul>
				<?php endif; ?>
			</li>
			
			<li <?php if ($this->_tpl_vars['sMenuItemSelect'] == 'top'): ?>class="active"<?php endif; ?>>
				<a href="<?php echo smarty_function_router(array('page' => 'top'), $this);?>
"><?php echo $this->_tpl_vars['aLang']['company_menu_top']; ?>
</a>
				<?php if ($this->_tpl_vars['sMenuItemSelect'] == 'top'): ?>
					<ul class="sub-menu" style="left: -80px;">											
						<li <?php if ($this->_tpl_vars['sMenuSubItemSelect'] == 'company'): ?>class="active"<?php endif; ?>><div><a href="<?php echo smarty_function_router(array('page' => 'top'), $this);?>
company/"><?php echo $this->_tpl_vars['aLang']['company_menu_top_company']; ?>
</a></div></li>
						<li <?php if ($this->_tpl_vars['sMenuSubItemSelect'] == 'seminar'): ?>class="active"<?php endif; ?>><div><a href="<?php echo smarty_function_router(array('page' => 'top'), $this);?>
seminar/"><?php echo $this->_tpl_vars['aLang']['company_menu_top_seminar']; ?>
</a></div></li>
						<li <?php if ($this->_tpl_vars['sMenuSubItemSelect'] == 'comment'): ?>class="active"<?php endif; ?>><div><a href="<?php echo smarty_function_router(array('page' => 'top'), $this);?>
comment/"><?php echo $this->_tpl_vars['aLang']['company_menu_top_comment']; ?>
</a></div></li>
						<?php echo smarty_function_hook(array('run' => 'menu_company_top_item'), $this);?>

					</ul>
				<?php endif; ?>
			</li>
			<?php echo smarty_function_hook(array('run' => 'menu_company'), $this);?>

		</ul>
		
		
		
