<?php /* Smarty version 2.6.19, created on 2011-01-27 17:33:58
         compiled from block.bs.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'truncate', 'block.bs.tpl', 12, false),array('modifier', 'escape', 'block.bs.tpl', 12, false),array('function', 'router', 'block.bs.tpl', 19, false),)), $this); ?>
			<div class="block stream">
				<div class="tl"><div class="tr"></div></div>
				<div class="cl">
					<div class="cr">
						<h1><?php echo $this->_tpl_vars['aLang']['block_bs']; ?>
</h1>
<ul class="stream-content">

	<?php $_from = $this->_tpl_vars['aBs']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['cmt'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['cmt']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['oBs']):
        $this->_foreach['cmt']['iteration']++;
?>
		<?php $this->assign('oUser', $this->_tpl_vars['oBs']->getUser()); ?>
		<li <?php if ($this->_foreach['cmt']['iteration'] % 2 == 1): ?>class="even"<?php endif; ?>>
			<a href="<?php echo $this->_tpl_vars['oUser']->getUserWebPath(); ?>
" class="stream-author"><?php echo $this->_tpl_vars['oUser']->getLogin(); ?>
</a>&nbsp;&#8594;
								<span class="stream-topic-icon"></span><a href="<?php echo $this->_tpl_vars['oBs']->getUrl(); ?>
" class="stream-topic"><?php echo ((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['oBs']->getText())) ? $this->_run_mod_handler('truncate', true, $_tmp) : smarty_modifier_truncate($_tmp)))) ? $this->_run_mod_handler('escape', true, $_tmp, 'html') : smarty_modifier_escape($_tmp, 'html')); ?>
</a>
			<span><?php echo $this->_tpl_vars['oBs']->getCountComment(); ?>
</span>
		</li>		
	<?php endforeach; endif; unset($_from); ?>
	
</ul>

<div class="right"><a href="<?php echo smarty_function_router(array('page' => 'bs'), $this);?>
"><?php echo $this->_tpl_vars['aLang']['block_bss_all']; ?>
</a></div>

					</div>
				</div>
				<div class="bl"><div class="br"></div></div>
			</div>