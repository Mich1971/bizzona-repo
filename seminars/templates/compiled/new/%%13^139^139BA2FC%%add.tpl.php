<?php /* Smarty version 2.6.19, created on 2011-02-05 17:23:08
         compiled from actions/ActionCompany/add.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'router', 'actions/ActionCompany/add.tpl', 10, false),array('function', 'hook', 'actions/ActionCompany/add.tpl', 13, false),)), $this); ?>
<?php if ($this->_tpl_vars['sEvent'] == 'add'): ?>
	<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'header.tpl', 'smarty_include_vars' => array('menu' => 'seminar_action','showWhiteBack' => true)));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php else: ?>
	<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'header.tpl', 'smarty_include_vars' => array('menu' => 'company_edit','showWhiteBack' => true)));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php endif; ?>

		<?php if ($this->_tpl_vars['sEvent'] == 'add'): ?>
			<h1><?php echo $this->_tpl_vars['aLang']['company_create']; ?>
</h1>
		<?php else: ?>
			<h1><?php echo $this->_tpl_vars['aLang']['company_admin']; ?>
: <a href="<?php echo smarty_function_router(array('page' => 'company'), $this);?>
<?php echo $this->_tpl_vars['oCompanyEdit']->getUrl(); ?>
/"><?php echo $this->_tpl_vars['oCompanyEdit']->getTitle(); ?>
</a></h1>
		<?php endif; ?>
		<form action="" method="POST" enctype="multipart/form-data">
			<?php echo smarty_function_hook(array('run' => 'form_add_company_begin'), $this);?>

			<input type="hidden" name="security_ls_key" value="<?php echo $this->_tpl_vars['LIVESTREET_SECURITY_KEY']; ?>
" /> 
				
			<p><label for="company_title"><?php echo $this->_tpl_vars['aLang']['company_create_title']; ?>
:</label><br />
			<input type="text" id="company_title" name="company_title" value="<?php echo $this->_tpl_vars['_aRequest']['company_title']; ?>
" class="w100p" /><br />
			<span class="form_note"><?php echo $this->_tpl_vars['aLang']['company_create_title_notice']; ?>
</span></p>

			<p><label for="company_url"><?php echo $this->_tpl_vars['aLang']['company_create_url']; ?>
:</label><br />
			<input type="text" id="company_url" name="company_url" value="<?php echo $this->_tpl_vars['_aRequest']['company_url']; ?>
" class="w100p"  <?php if ($this->_tpl_vars['_aRequest']['company_id']): ?>disabled<?php endif; ?> /><br />
			<span class="form_note"><?php echo $this->_tpl_vars['aLang']['company_create_url_notice']; ?>
</span></p>
			
			<p><label for="company_type"><?php echo $this->_tpl_vars['aLang']['company_create_type']; ?>
:</label><br />
			<select name="company_type" id="company_type" onChange="">
				<option value="open" <?php if ($this->_tpl_vars['_aRequest']['company_type'] == 'open'): ?>selected<?php endif; ?>><?php echo $this->_tpl_vars['aLang']['company_create_type_open']; ?>
</option>
				<option value="close" <?php if ($this->_tpl_vars['_aRequest']['company_type'] == 'close'): ?>selected<?php endif; ?>><?php echo $this->_tpl_vars['aLang']['company_create_type_close']; ?>
</option>
			</select><br />
			<span class="form_note"><?php echo $this->_tpl_vars['aLang']['company_create_type_open_notice']; ?>
</span></p>

			<p><label for="company_description"><?php echo $this->_tpl_vars['aLang']['company_create_description']; ?>
:</label><br />
			<textarea name="company_description" id="company_description" rows="20"><?php echo $this->_tpl_vars['_aRequest']['company_description']; ?>
</textarea><br />
			<span class="form_note"><?php echo $this->_tpl_vars['aLang']['company_create_description_notice']; ?>
</span></p>
			
			<p><label for="company_limit_rating_seminar"><?php echo $this->_tpl_vars['aLang']['company_create_rating']; ?>
:</label><br />
			<input type="text" id="company_limit_rating_seminar" name="company_limit_rating_seminar" value="<?php echo $this->_tpl_vars['_aRequest']['company_limit_rating_seminar']; ?>
" class="w100p" /><br />
			<span class="form_note"><?php echo $this->_tpl_vars['aLang']['company_create_rating_notice']; ?>
</span></p>
				
			<p>
			<?php if ($this->_tpl_vars['oCompanyEdit'] && $this->_tpl_vars['oCompanyEdit']->getAvatar()): ?>
				<img src="<?php echo $this->_tpl_vars['oCompanyEdit']->getAvatarPath(48); ?>
" />
				<img src="<?php echo $this->_tpl_vars['oCompanyEdit']->getAvatarPath(24); ?>
" />
				<label for="avatar_delete"><input type="checkbox" id="avatar_delete" name="avatar_delete" value="on"> &mdash; <?php echo $this->_tpl_vars['aLang']['company_create_avatar_delete']; ?>
</label><br /><br />
			<?php endif; ?>
			<label for="avatar"><?php echo $this->_tpl_vars['aLang']['company_create_avatar']; ?>
:</label><br />
			<input type="file" name="avatar" id="avatar"></p>
					
			<?php echo smarty_function_hook(array('run' => 'form_add_company_end'), $this);?>
		
			<p><input type="submit" name="submit_company_add" value="<?php echo $this->_tpl_vars['aLang']['company_create_submit']; ?>
">						
			<input type="hidden" name="company_id" value="<?php echo $this->_tpl_vars['_aRequest']['company_id']; ?>
"></p>
			
		</form>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'footer.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>