<?php /* Smarty version 2.6.19, created on 2011-02-05 16:41:16
         compiled from seminar.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'cfg', 'seminar.tpl', 10, false),array('function', 'router', 'seminar.tpl', 23, false),array('function', 'date_format', 'seminar.tpl', 63, false),array('function', 'hook', 'seminar.tpl', 68, false),array('modifier', 'escape', 'seminar.tpl', 12, false),)), $this); ?>
			
			<?php $this->assign('oCompany', $this->_tpl_vars['oSeminar']->getCompany()); ?> 
			<?php $this->assign('oUser', $this->_tpl_vars['oSeminar']->getUser()); ?>
			<?php $this->assign('oVote', $this->_tpl_vars['oSeminar']->getVote()); ?> 
			<!-- Seminar -->			
			<div class="seminar">
				<div class="favorite <?php if ($this->_tpl_vars['oUserCurrent']): ?><?php if ($this->_tpl_vars['oSeminar']->getIsFavourite()): ?>active<?php endif; ?><?php else: ?>fav-guest<?php endif; ?>"><a href="#" onclick="lsFavourite.toggle(<?php echo $this->_tpl_vars['oSeminar']->getId(); ?>
,this,'seminar'); return false;"></a></div>
				<h1 class="title">
					<?php if ($this->_tpl_vars['oSeminar']->getPublish() == 0): ?>	
						<img src="<?php echo smarty_function_cfg(array('name' => 'path.static.skin'), $this);?>
/images/seminar_unpublish.gif" border="0" title="<?php echo $this->_tpl_vars['aLang']['seminar_unpublish']; ?>
" width="16" height="16" alt="<?php echo $this->_tpl_vars['aLang']['seminar_unpublish']; ?>
">
					<?php endif; ?>
					<?php echo ((is_array($_tmp=$this->_tpl_vars['oSeminar']->getTitle())) ? $this->_run_mod_handler('escape', true, $_tmp, 'html') : smarty_modifier_escape($_tmp, 'html')); ?>

					<?php if ($this->_tpl_vars['oSeminar']->getType() == 'link'): ?>
  						<img src="<?php echo smarty_function_cfg(array('name' => 'path.static.skin'), $this);?>
/images/link_url_big.gif" border="0" title="<?php echo $this->_tpl_vars['aLang']['seminar_link']; ?>
" width="16" height="16" alt="<?php echo $this->_tpl_vars['aLang']['seminar_link']; ?>
">
  					<?php endif; ?>
				</h1>
				<ul class="action">					
					<li><a href="<?php echo $this->_tpl_vars['oCompany']->getUrlFull(); ?>
"><?php echo ((is_array($_tmp=$this->_tpl_vars['oCompany']->getTitle())) ? $this->_run_mod_handler('escape', true, $_tmp, 'html') : smarty_modifier_escape($_tmp, 'html')); ?>
</a>&nbsp;&nbsp;</li>						
					<?php if ($this->_tpl_vars['oUserCurrent'] && ( $this->_tpl_vars['oUserCurrent']->getId() == $this->_tpl_vars['oSeminar']->getUserId() || $this->_tpl_vars['oUserCurrent']->isAdministrator() || $this->_tpl_vars['oCompany']->getUserIsAdministrator() || $this->_tpl_vars['oCompany']->getUserIsModerator() || $this->_tpl_vars['oCompany']->getOwnerId() == $this->_tpl_vars['oUserCurrent']->getId() )): ?>
  						<li class="edit"><a href="<?php echo smarty_function_cfg(array('name' => 'path.root.web'), $this);?>
/<?php echo $this->_tpl_vars['oSeminar']->getType(); ?>
/edit/<?php echo $this->_tpl_vars['oSeminar']->getId(); ?>
/" title="<?php echo $this->_tpl_vars['aLang']['seminar_edit']; ?>
"><?php echo $this->_tpl_vars['aLang']['seminar_edit']; ?>
</a></li>
  					<?php endif; ?>
					<?php if ($this->_tpl_vars['oUserCurrent'] && ( $this->_tpl_vars['oUserCurrent']->isAdministrator() || $this->_tpl_vars['oCompany']->getUserIsAdministrator() || $this->_tpl_vars['oCompany']->getOwnerId() == $this->_tpl_vars['oUserCurrent']->getId() )): ?>
  						<li class="delete"><a href="<?php echo smarty_function_router(array('page' => 'seminar'), $this);?>
delete/<?php echo $this->_tpl_vars['oSeminar']->getId(); ?>
/?security_ls_key=<?php echo $this->_tpl_vars['LIVESTREET_SECURITY_KEY']; ?>
" title="<?php echo $this->_tpl_vars['aLang']['seminar_delete']; ?>
" onclick="return confirm('<?php echo $this->_tpl_vars['aLang']['seminar_delete_confirm']; ?>
');"><?php echo $this->_tpl_vars['aLang']['seminar_delete']; ?>
</a></li>
  					<?php endif; ?>
				</ul>				
				<div class="content">
				
				
			<?php if ($this->_tpl_vars['oSeminar']->getType() == 'question'): ?>   
    		
    		<div id="seminar_question_area_<?php echo $this->_tpl_vars['oSeminar']->getId(); ?>
">
    		<?php if (! $this->_tpl_vars['oSeminar']->getUserQuestionIsVote()): ?> 		
    			<ul class="poll-new">	
				<?php $_from = $this->_tpl_vars['oSeminar']->getQuestionAnswers(); if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['aAnswer']):
?>				
					<li><label for="seminar_answer_<?php echo $this->_tpl_vars['oSeminar']->getId(); ?>
_<?php echo $this->_tpl_vars['key']; ?>
"><input type="radio" id="seminar_answer_<?php echo $this->_tpl_vars['oSeminar']->getId(); ?>
_<?php echo $this->_tpl_vars['key']; ?>
" name="seminar_answer_<?php echo $this->_tpl_vars['oSeminar']->getId(); ?>
"  value="<?php echo $this->_tpl_vars['key']; ?>
" onchange="$('seminar_answer_<?php echo $this->_tpl_vars['oSeminar']->getId(); ?>
_value').setProperty('value',this.value);"/> <?php echo ((is_array($_tmp=$this->_tpl_vars['aAnswer']['text'])) ? $this->_run_mod_handler('escape', true, $_tmp, 'html') : smarty_modifier_escape($_tmp, 'html')); ?>
</label></li>				
				<?php endforeach; endif; unset($_from); ?>
					<li>
					<input type="submit"  value="<?php echo $this->_tpl_vars['aLang']['seminar_question_vote']; ?>
" onclick="ajaxQuestionVote(<?php echo $this->_tpl_vars['oSeminar']->getId(); ?>
,$('seminar_answer_<?php echo $this->_tpl_vars['oSeminar']->getId(); ?>
_value').getProperty('value'));">
					<input type="submit"  value="<?php echo $this->_tpl_vars['aLang']['seminar_question_abstain']; ?>
"  onclick="ajaxQuestionVote(<?php echo $this->_tpl_vars['oSeminar']->getId(); ?>
,-1)">
					</li>				
					<input type="hidden" id="seminar_answer_<?php echo $this->_tpl_vars['oSeminar']->getId(); ?>
_value" value="-1">				
				</ul>				
				<span><?php echo $this->_tpl_vars['aLang']['seminar_question_vote_result']; ?>
: <?php echo $this->_tpl_vars['oSeminar']->getQuestionCountVote(); ?>
. <?php echo $this->_tpl_vars['aLang']['seminar_question_abstain_result']; ?>
: <?php echo $this->_tpl_vars['oSeminar']->getQuestionCountVoteAbstain(); ?>
</span><br>			
			<?php else: ?>			
				<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'seminar_question.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
			<?php endif; ?>
			</div>
			<br>	
						
    		<?php endif; ?>
				
					<?php echo $this->_tpl_vars['oSeminar']->getText(); ?>

				</div>				
				<ul class="tags">
					<?php $_from = $this->_tpl_vars['oSeminar']->getTagsArray(); if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['tags_list'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['tags_list']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['sTag']):
        $this->_foreach['tags_list']['iteration']++;
?>
						<li><a href="<?php echo smarty_function_router(array('page' => 'tag'), $this);?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['sTag'])) ? $this->_run_mod_handler('escape', true, $_tmp, 'html') : smarty_modifier_escape($_tmp, 'html')); ?>
/"><?php echo ((is_array($_tmp=$this->_tpl_vars['sTag'])) ? $this->_run_mod_handler('escape', true, $_tmp, 'html') : smarty_modifier_escape($_tmp, 'html')); ?>
</a><?php if (! ($this->_foreach['tags_list']['iteration'] == $this->_foreach['tags_list']['total'])): ?>, <?php endif; ?></li>
					<?php endforeach; endif; unset($_from); ?>									
				</ul>				
				<ul class="voting <?php if ($this->_tpl_vars['oVote'] || ( $this->_tpl_vars['oUserCurrent'] && $this->_tpl_vars['oSeminar']->getUserId() == $this->_tpl_vars['oUserCurrent']->getId() ) || strtotime ( $this->_tpl_vars['oSeminar']->getDateAdd() ) < time()-$this->_tpl_vars['oConfig']->GetValue('acl.vote.seminar.limit_time')): ?><?php if ($this->_tpl_vars['oSeminar']->getRating() > 0): ?>positive<?php elseif ($this->_tpl_vars['oSeminar']->getRating() < 0): ?>negative<?php endif; ?><?php endif; ?> <?php if (! $this->_tpl_vars['oUserCurrent'] || $this->_tpl_vars['oSeminar']->getUserId() == $this->_tpl_vars['oUserCurrent']->getId() || strtotime ( $this->_tpl_vars['oSeminar']->getDateAdd() ) < time()-$this->_tpl_vars['oConfig']->GetValue('acl.vote.seminar.limit_time')): ?>guest<?php endif; ?> <?php if ($this->_tpl_vars['oVote']): ?> voted <?php if ($this->_tpl_vars['oVote']->getDirection() > 0): ?>plus<?php elseif ($this->_tpl_vars['oVote']->getDirection() < 0): ?>minus<?php endif; ?><?php endif; ?>">
					<li class="plus"><a href="#" onclick="lsVote.vote(<?php echo $this->_tpl_vars['oSeminar']->getId(); ?>
,this,1,'seminar'); return false;"></a></li>
					<li class="total" title="<?php echo $this->_tpl_vars['aLang']['seminar_vote_count']; ?>
: <?php echo $this->_tpl_vars['oSeminar']->getCountVote(); ?>
"><?php if ($this->_tpl_vars['oVote'] || ( $this->_tpl_vars['oUserCurrent'] && $this->_tpl_vars['oSeminar']->getUserId() == $this->_tpl_vars['oUserCurrent']->getId() ) || strtotime ( $this->_tpl_vars['oSeminar']->getDateAdd() ) < time()-$this->_tpl_vars['oConfig']->GetValue('acl.vote.seminar.limit_time')): ?> <?php if ($this->_tpl_vars['oSeminar']->getRating() > 0): ?>+<?php endif; ?><?php echo $this->_tpl_vars['oSeminar']->getRating(); ?>
 <?php else: ?> <a href="#" onclick="lsVote.vote(<?php echo $this->_tpl_vars['oSeminar']->getId(); ?>
,this,0,'seminar'); return false;">&mdash;</a> <?php endif; ?></li>
					<li class="minus"><a href="#" onclick="lsVote.vote(<?php echo $this->_tpl_vars['oSeminar']->getId(); ?>
,this,-1,'seminar'); return false;"></a></li>
					<li class="date"><?php echo smarty_function_date_format(array('date' => $this->_tpl_vars['oSeminar']->getDateAdd()), $this);?>
</li>
					<?php if ($this->_tpl_vars['oSeminar']->getType() == 'link'): ?>
						<li class="link"><a href="<?php echo smarty_function_router(array('page' => 'link'), $this);?>
go/<?php echo $this->_tpl_vars['oSeminar']->getId(); ?>
/" title="<?php echo $this->_tpl_vars['aLang']['seminar_link_count_jump']; ?>
: <?php echo $this->_tpl_vars['oSeminar']->getLinkCountJump(); ?>
"><?php echo $this->_tpl_vars['oSeminar']->getLinkUrl(true); ?>
</a></li>						
					<?php endif; ?>
					<li class="author"><a href="<?php echo $this->_tpl_vars['oUser']->getUserWebPath(); ?>
"><?php echo $this->_tpl_vars['oUser']->getLogin(); ?>
</a></li>
					<?php echo smarty_function_hook(array('run' => 'seminar_show_info','seminar' => $this->_tpl_vars['oSeminar']), $this);?>

				</ul>
			<?php echo smarty_function_hook(array('run' => 'seminar_show_end','seminar' => $this->_tpl_vars['oSeminar']), $this);?>

			</div>
			<!-- /Seminar -->