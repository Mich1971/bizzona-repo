<?php /* Smarty version 2.6.19, created on 2011-02-03 19:13:10
         compiled from menu.seminar_action.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'cfg', 'menu.seminar_action.tpl', 4, false),array('function', 'router', 'menu.seminar_action.tpl', 7, false),array('function', 'hook', 'menu.seminar_action.tpl', 10, false),)), $this); ?>
		<ul class="menu">
		
			<li <?php if ($this->_tpl_vars['sMenuSubItemSelect'] == 'add'): ?>class="active"<?php endif; ?>>
				<a href="<?php echo smarty_function_cfg(array('name' => 'path.root.web'), $this);?>
/<?php if ($this->_tpl_vars['sMenuItemSelect'] == 'add_company'): ?>seminar<?php else: ?><?php echo $this->_tpl_vars['sMenuItemSelect']; ?>
<?php endif; ?>/add/"><?php echo $this->_tpl_vars['aLang']['seminar_menu_add']; ?>
</a>
				<?php if ($this->_tpl_vars['sMenuSubItemSelect'] == 'add'): ?>
					<ul class="sub-menu" >
						<li <?php if ($this->_tpl_vars['sMenuItemSelect'] == 'seminar'): ?>class="active"<?php endif; ?>><div><a href="<?php echo smarty_function_router(array('page' => 'seminar'), $this);?>
<?php echo $this->_tpl_vars['sMenuSubItemSelect']; ?>
/"><?php echo $this->_tpl_vars['aLang']['seminar_menu_add_seminar']; ?>
</a></div></li>						
						<li <?php if ($this->_tpl_vars['sMenuItemSelect'] == 'question'): ?>class="active"<?php endif; ?>><div><a href="<?php echo smarty_function_router(array('page' => 'question'), $this);?>
<?php echo $this->_tpl_vars['sMenuSubItemSelect']; ?>
/"><?php echo $this->_tpl_vars['aLang']['seminar_menu_add_question']; ?>
</a></div></li>
						<li <?php if ($this->_tpl_vars['sMenuItemSelect'] == 'link'): ?>class="active"<?php endif; ?>><div><a href="<?php echo smarty_function_router(array('page' => 'link'), $this);?>
<?php echo $this->_tpl_vars['sMenuSubItemSelect']; ?>
/"><?php echo $this->_tpl_vars['aLang']['seminar_menu_add_link']; ?>
</a></div></li>
						<?php echo smarty_function_hook(array('run' => 'menu_seminar_action_add_item'), $this);?>

						<li ><div><a href="<?php echo smarty_function_router(array('page' => 'company'), $this);?>
add/"><font color="Red"><?php echo $this->_tpl_vars['aLang']['company_menu_create']; ?>
</font></a></div></li>
					</ul>
				<?php endif; ?>
			</li>
			
			<li <?php if ($this->_tpl_vars['sMenuSubItemSelect'] == 'saved'): ?>class="active"<?php endif; ?>>
				<a href="<?php echo smarty_function_router(array('page' => 'seminar'), $this);?>
saved/"><?php echo $this->_tpl_vars['aLang']['seminar_menu_saved']; ?>
</a> 
				<?php if ($this->_tpl_vars['sMenuSubItemSelect'] == 'saved'): ?>
					<ul class="sub-menu" >
						<?php echo smarty_function_hook(array('run' => 'menu_seminar_action_saved_item'), $this);?>

					</ul>
				<?php endif; ?>				
			</li>
			
			<li <?php if ($this->_tpl_vars['sMenuSubItemSelect'] == 'published'): ?>class="active"<?php endif; ?>>
				<a href="<?php echo smarty_function_router(array('page' => 'seminar'), $this);?>
published/"><?php echo $this->_tpl_vars['aLang']['seminar_menu_published']; ?>
</a>	
				<?php if ($this->_tpl_vars['sMenuSubItemSelect'] == 'published'): ?>
					<ul class="sub-menu" >
						<?php echo smarty_function_hook(array('run' => 'menu_seminar_action_published_item'), $this);?>

					</ul>
				<?php endif; ?>		
			</li>		
			<?php echo smarty_function_hook(array('run' => 'menu_seminar_action'), $this);?>

		</ul>
		
		
		
