<?php /* Smarty version 2.6.19, created on 2011-01-30 11:36:55
         compiled from menu.bi.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'cfg', 'menu.bi.tpl', 3, false),)), $this); ?>
<ul class="menu">
	<li>
		<a href="<?php echo smarty_function_cfg(array('name' => 'path.root.web'), $this);?>
/bi/"><?php echo $this->_tpl_vars['aLang']['bi']; ?>
</a>
	</li>	
<?php if ($this->_tpl_vars['oUserCurrent']): ?>		
	<li <?php if ($this->_tpl_vars['sMenuSubItemSelect'] == 'add'): ?>class="active"<?php endif; ?>>
		<a href="<?php echo smarty_function_cfg(array('name' => 'path.root.web'), $this);?>
/bi/add/"><?php echo $this->_tpl_vars['aLang']['bi_menu_add']; ?>
</a>
	</li>
	<li>
		<a href="<?php echo smarty_function_cfg(array('name' => 'path.root.web'), $this);?>
/mybi/<?php echo $this->_tpl_vars['oUserCurrent']->getLogin(); ?>
/"><?php echo $this->_tpl_vars['aLang']['bi_my']; ?>
</a>
	</li>	
	<li>
		<a href="<?php echo smarty_function_cfg(array('name' => 'path.root.web'), $this);?>
/mybi/<?php echo $this->_tpl_vars['oUserCurrent']->getLogin(); ?>
/comment"><?php echo $this->_tpl_vars['aLang']['bi_mycomment']; ?>
</a>
	</li>	
	<li>
		<a href="<?php echo smarty_function_cfg(array('name' => 'path.root.web'), $this);?>
/bi/favourites/"><?php echo $this->_tpl_vars['aLang']['bi_fav']; ?>
</a>
	</li>		
<?php endif; ?>		
</ul>