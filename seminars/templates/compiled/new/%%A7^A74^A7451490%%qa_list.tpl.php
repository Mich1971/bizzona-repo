<?php /* Smarty version 2.6.19, created on 2011-01-27 17:45:54
         compiled from qa_list.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'escape', 'qa_list.tpl', 11, false),array('function', 'router', 'qa_list.tpl', 16, false),array('function', 'date_format', 'qa_list.tpl', 39, false),)), $this); ?>
<?php if (count ( $this->_tpl_vars['aQas'] ) > 0): ?> 
	<?php $_from = $this->_tpl_vars['aQas']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['oQa']):
?>
			<!-- Qa -->			
			<?php $this->assign('oUser', $this->_tpl_vars['oQa']->getUser()); ?> 
			<?php $this->assign('oVote', $this->_tpl_vars['oQa']->getVote()); ?> 
			
			<div class="topic">

				<div class="favorite <?php if ($this->_tpl_vars['oUserCurrent']): ?><?php if ($this->_tpl_vars['oQa']->getIsFavourite()): ?>active<?php endif; ?><?php else: ?>fav-guest<?php endif; ?>"><a href="#" onclick="lsFavourite.toggle(<?php echo $this->_tpl_vars['oQa']->getId(); ?>
,this,'qa'); return false;"></a></div>			
			
				<h1 class="title"><a href="<?php echo $this->_tpl_vars['oQa']->getUrl(); ?>
"><?php echo ((is_array($_tmp=$this->_tpl_vars['oQa']->getTitle())) ? $this->_run_mod_handler('escape', true, $_tmp, 'html') : smarty_modifier_escape($_tmp, 'html')); ?>
</a></h1>
			
				<ul class="action"> 
				
					<?php if ($this->_tpl_vars['oUserCurrent'] && ( $this->_tpl_vars['oUserCurrent']->getId() == $this->_tpl_vars['oQa']->getUserId() || $this->_tpl_vars['oUserCurrent']->isAdministrator() )): ?>
  						<li class="edit"><a href="<?php echo smarty_function_router(array('page' => 'qa'), $this);?>
edit/<?php echo $this->_tpl_vars['oQa']->getId(); ?>
/" title="<?php echo $this->_tpl_vars['aLang']['topic_edit']; ?>
"><?php echo $this->_tpl_vars['aLang']['topic_edit']; ?>
</a></li>
  					<?php endif; ?>
					<?php if ($this->_tpl_vars['oUserCurrent'] && $this->_tpl_vars['oUserCurrent']->isAdministrator()): ?>
							<li class="delete"><a href="<?php echo smarty_function_router(array('page' => 'qa'), $this);?>
delete/<?php echo $this->_tpl_vars['oQa']->getId(); ?>
/?security_ls_key=<?php echo $this->_tpl_vars['LIVESTREET_SECURITY_KEY']; ?>
" title="<?php echo $this->_tpl_vars['aLang']['topic_delete']; ?>
" onclick="return confirm('<?php echo $this->_tpl_vars['aLang']['qa_delete_confirm']; ?>
');"><?php echo $this->_tpl_vars['aLang']['topic_delete']; ?>
</a></li> 
					<?php endif; ?>
					
				</ul>				
				
			<div class="content">
				<?php echo $this->_tpl_vars['oQa']->getDescription(); ?>

				<br><br>( <a href="<?php echo $this->_tpl_vars['oQa']->getUrl(); ?>
" title="<?php echo $this->_tpl_vars['aLang']['topic_read_more']; ?>
"><?php echo $this->_tpl_vars['aLang']['qa_read_more']; ?>
</a> )
			</div>
			
			<ul class="tags">
				<?php $_from = $this->_tpl_vars['oQa']->getTagsArray(); if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['tags_list'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['tags_list']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['sTag']):
        $this->_foreach['tags_list']['iteration']++;
?>
					<li><a href="<?php echo smarty_function_router(array('page' => 'qatag'), $this);?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['sTag'])) ? $this->_run_mod_handler('escape', true, $_tmp, 'html') : smarty_modifier_escape($_tmp, 'html')); ?>
/"><?php echo ((is_array($_tmp=$this->_tpl_vars['sTag'])) ? $this->_run_mod_handler('escape', true, $_tmp, 'html') : smarty_modifier_escape($_tmp, 'html')); ?>
</a><?php if (! ($this->_foreach['tags_list']['iteration'] == $this->_foreach['tags_list']['total'])): ?>, <?php endif; ?></li>
				<?php endforeach; endif; unset($_from); ?>								
			</ul>
							
			<ul class="voting  <?php if ($this->_tpl_vars['oVote'] || ( $this->_tpl_vars['oUserCurrent'] && $this->_tpl_vars['oQa']->getUserId() == $this->_tpl_vars['oUserCurrent']->getId() ) || strtotime ( $this->_tpl_vars['oQa']->getDateAdd() ) < time()-$this->_tpl_vars['oConfig']->GetValue('acl.vote.topic.limit_time')): ?><?php if ($this->_tpl_vars['oQa']->getRating() > 0): ?>positive<?php elseif ($this->_tpl_vars['oQa']->getRating() < 0): ?>negative<?php endif; ?><?php endif; ?> <?php if (! $this->_tpl_vars['oUserCurrent'] || $this->_tpl_vars['oQa']->getUserId() == $this->_tpl_vars['oUserCurrent']->getId() || strtotime ( $this->_tpl_vars['oQa']->getDateAdd() ) < time()-$this->_tpl_vars['oConfig']->GetValue('acl.vote.topic.limit_time')): ?>guest<?php endif; ?> <?php if ($this->_tpl_vars['oVote']): ?> voted <?php if ($this->_tpl_vars['oVote']->getDirection() > 0): ?>plus<?php elseif ($this->_tpl_vars['oVote']->getDirection() < 0): ?>minus<?php endif; ?><?php endif; ?>">
				<li class="plus"><a href="#" onclick="lsVote.vote(<?php echo $this->_tpl_vars['oQa']->getId(); ?>
,this,1,'qa'); return false;"></a></li>
				<li class="total" title="<?php echo $this->_tpl_vars['aLang']['topic_vote_count']; ?>
: <?php echo $this->_tpl_vars['oQa']->getCountVote(); ?>
"><?php if ($this->_tpl_vars['oVote'] || ( $this->_tpl_vars['oUserCurrent'] && $this->_tpl_vars['oQa']->getUserId() == $this->_tpl_vars['oUserCurrent']->getId() ) || strtotime ( $this->_tpl_vars['oQa']->getDateAdd() ) < time()-$this->_tpl_vars['oConfig']->GetValue('acl.vote.topic.limit_time')): ?> <?php if ($this->_tpl_vars['oQa']->getRating() > 0): ?>+<?php endif; ?><?php echo $this->_tpl_vars['oQa']->getRating(); ?>
 <?php else: ?> <a href="#" onclick="lsVote.vote(<?php echo $this->_tpl_vars['oQa']->getId(); ?>
,this,0,'qa'); return false;">&mdash;</a> <?php endif; ?></li>
				<li class="minus"><a href="#" onclick="lsVote.vote(<?php echo $this->_tpl_vars['oQa']->getId(); ?>
,this,-1,'qa'); return false;"></a></li>
				<li class="date"><?php echo smarty_function_date_format(array('date' => $this->_tpl_vars['oQa']->getDateAdd()), $this);?>
</li>
				<li class="author"><a href="<?php echo $this->_tpl_vars['oUser']->getUserWebPath(); ?>
"><?php echo $this->_tpl_vars['oUser']->getLogin(); ?>
</a></li>
					<li class="comments-total">
						<?php if ($this->_tpl_vars['oQa']->getCountComment() > 0): ?>
							<a href="<?php echo $this->_tpl_vars['oQa']->getUrl(); ?>
#comments" title="<?php echo $this->_tpl_vars['aLang']['topic_comment_read']; ?>
"><span class="green"><?php echo $this->_tpl_vars['oQa']->getCountComment(); ?>
</span></a>
						<?php endif; ?>
						<a href="<?php echo $this->_tpl_vars['oQa']->getUrl(); ?>
#comments" title="<?php echo $this->_tpl_vars['aLang']['qa_comment_add']; ?>
"><span class="red"><?php echo $this->_tpl_vars['aLang']['qa_comment_add']; ?>
</span></a>
					</li>				
			</ul>
			
			</div>
			<!-- /Qa -->
	<?php endforeach; endif; unset($_from); ?>	
	
	<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'paging.tpl', 'smarty_include_vars' => array('aPaging' => ($this->_tpl_vars['aPaging']))));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
	
<?php else: ?>
<?php echo $this->_tpl_vars['aLang']['blog_no_topic']; ?>

<?php endif; ?>