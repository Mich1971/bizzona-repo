<?
  global $DATABASE;
  
  include_once("./phpset.inc");
  include_once("./engine/functions.inc"); 
  AssignDataBaseSetting();
  
  include_once("./engine/class.category.inc"); 
  $category = new Category();

  include_once("./engine/class.subcategory.inc"); 
  $subCategory = new SubCategory();
  
  include_once("./engine/class.country.inc"); 
  $country = new Country();

  include_once("./engine/class.city.inc"); 
  $city = new City();

  include_once("./engine/class.bizplan_company.inc"); 
  $bizplan_company = new BizPlanCompany();
  
  include_once("./engine/class.bizplan.inc");
  $bizplan = new BizPlan();

  include("./engine/FCKeditor/fckeditor.php"); 

  require_once("./libs/Smarty.class.php");
  $smarty = new Smarty;
  $smarty->template_dir = "./templates";
  $smarty->compile_dir  = "./templates_c";
  //$smarty->cache_dir = "./cache";
  $smarty->compile_check = true;
  $smarty->debugging     = false;

  
  $data = array();
  $data["offset"] = 0;
  $data["rowCount"] = 0;
  $data["sort"] = "ID";
    
  $listCategory = $category->Select($data);
  $smarty->assign("listCategory", $listCategory);
  
  $listSubCategory = $subCategory->Select($data);
  $smarty->assign("listSubCategory", $listSubCategory);

  $companylist = $bizplan_company->SelectByStatus(OPEN_BIZPLAN_COMPANY);
  $smarty->assign("companylist", $companylist);
  
  
  
  $data = array();
  $data["offset"] = 0;
  $data["rowCount"] = 0;
  $data["sort"] = "Count";
  $result = $category->Select($data);
  $smarty->assign("categorylist", $result);
  
  
  if (isset($_POST) && sizeof($_POST)) {
  	
  	$error = false;
  	$error_text = "";
  	
  	$t_description = htmlspecialchars(trim($_POST["description"]), ENT_QUOTES);
  	$t_shortDescription = htmlspecialchars(trim($_POST["shortDescription"]), ENT_QUOTES);
  	$t_name = htmlspecialchars(trim($_POST["name"]), ENT_QUOTES);
  	$t_cost = htmlspecialchars(trim($_POST["cost"]), ENT_QUOTES);
  	$t_count_site = htmlspecialchars(trim($_POST["count_site"]), ENT_QUOTES);
  	$t_company_id = htmlspecialchars(trim($_POST["company_id"]), ENT_QUOTES);
  	$t_category_id = htmlspecialchars(trim($_POST["category_id"]), ENT_QUOTES);
  	$t_company_text = htmlspecialchars(trim($_POST["company_text"]), ENT_QUOTES);
  	$t_company_name = htmlspecialchars(trim($_POST["company_name"]), ENT_QUOTES);
  	$t_company_email = htmlspecialchars(trim($_POST["company_email"]), ENT_QUOTES);
  	$t_company_phone = htmlspecialchars(trim($_POST["company_phone"]), ENT_QUOTES);
  	$t_company_fml = htmlspecialchars(trim($_POST["company_fml"]), ENT_QUOTES);
  	$t_display_id = htmlspecialchars(trim($_POST["display_id"]), ENT_QUOTES);
  	$t_language_id = htmlspecialchars(trim($_POST["language_id"]), ENT_QUOTES);

  	if (strlen(trim($t_name)) <= 0)
  	{
  		$error = true;	
  		$text_error = "������ ����� ������ \"�������� �����\"";
  		$smarty->assign("ErrorInput_name", true);
  		
  	}
  	else if(strlen(trim($t_cost)) <= 0)
  	{
  		$error = true;
  		$text_error = "������ ����� ������ \"���������\"";
  		$smarty->assign("ErrorInput_cost", true);
  	}
  	else if(strlen(trim($t_count_site)) <= 0)
  	{
  		$error = true;
  		$text_error = "������ ����� ������ \"���������� �������\"";
  		$smarty->assign("ErrorInput_count_site", true);
  	}
  	else if ($t_company_id == "0" && 
  	           (
  			     strlen(trim($t_company_text)) <= 0 ||
  			     strlen(trim($t_company_name)) <= 0 ||
  			     strlen(trim($t_company_email)) <= 0 ||
  			     strlen(trim($t_company_fml)) <= 0 ||
  			     strlen(trim($t_company_phone)) <= 0
  			   )
  			)
  	{
  		
  		$error = true;
  		$text_error = "������ ����� ������ \"�����������\"";
  		$smarty->assign("othercompanyerror", true);
  		
  		if(strlen(trim($t_company_name)) <= 0)
  		{
  			$smarty->assign("ErrorInput_company_name", true);
  		}
  		else if(strlen(trim($t_company_text)) <= 0)
  		{
  			$smarty->assign("ErrorInput_company_text", true);
  		}
  		else if(strlen(trim($t_company_email)) <= 0)
  		{
  			$smarty->assign("ErrorInput_company_email", true);
  		}
  		else if(strlen(trim($t_company_phone)) <= 0)
  		{
  			$smarty->assign("ErrorInput_company_phone", true);
  		}
  		else if(strlen(trim($t_company_fml)) <= 0)
  		{
  			$smarty->assign("ErrorInput_company_fml", true);
  		}
  		
  	}
  	else if(strlen(trim($t_shortDescription)) <= 0)
  	{
  		$error = true;
  		$text_error = "������ ����� ������ \"������� ��������\"";
  		$smarty->assign("ErrorInput_shortDescription", true);
  	} 
  	
  	else if(strlen(trim($t_description)) <= 0)
  	{
  		$error = true;
  		$text_error = "������ ����� ������ \"��������� ��������\"";
  		$smarty->assign("ErrorInput_description", true);
  	} 
  	
  	
  	if(!$error)
  	{
  		$bizplan->description = $t_description;
  		$bizplan->shortDescription = $t_shortDescription;
  		$bizplan->name = $t_name;
  		$bizplan->cost = $t_cost;
  		$bizplan->count_site = $t_count_site;
  		$bizplan->company_id = $t_company_id;
  		$bizplan->category_id = $t_category_id;
  		$bizplan->display_id = $t_display_id;
  		$bizplan->language_id = $t_language_id;
  		$bizplan->status_id = NEW_BIZPLAN;
  		$bizplan->Insert();
  		
  		$bizplan_company->name = $t_company_name;
  		$bizplan_company->description = $t_company_text;
  		$bizplan_company->email = $t_company_email;
  		$bizplan_company->fml = $t_company_fml;
  		$bizplan_company->phone = $t_company_phone;
  		$bizplan_company->Insert();
  		
  		unset($_POST);
  		$smarty->assign("text_success", "������ ���� ��� ��������. � ������� ���� �� ����� ���������� � �����������.");


                $headers1 = 'BizZONA.RU: new looked' . "\r\n" . 'Reply-To:  ' . "\r\n" . 'X-Mailer: PHP/' . phpversion();
                mail("bizzona.ru@gmail.com", "Posted BizPLAN", "PHP_SELF: ".$_SERVER["PHP_SELF"]."\r\n"." HTTP_HOST: ".$_SERVER["HTTP_HOST"]."\r\n"."HTTP_REFERER: ".$_SERVER["HTTP_REFERER"]."\r\n"."REMOTE_ADDR: ".$_SERVER["REMOTE_ADDR"]."\r\n" , $headers1);


  	}
  	else
  	{
  		$smarty->assign("text_error", $text_error);
  	}
  	
  }
  
?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>

  <HEAD id="Head">
      <title>���������� ������ ���� - ������� � ������� �������. ������� ������ - ������ ����</title>		
	<META NAME="Description" CONTENT="������� �������, ������� �������, ������� ������, ������� �������, ������� �������� �������, ������� �������, ������� ��������,  ������� ��������, ������� ��������, ���� ������� ������?">
        <meta name="Keywords" content="������� �������, ������� �������, ������� ������, ������� �������, ������� �������� �������, ������� �������, ������� ��������,  ������� ��������, ������� ��������, ���� ������� ������?, Ready business, �����������, �������� � ��������, ���������, ����������, �����������"> 
        <LINK href="./general.css" type="text/css" rel="stylesheet">
        <script src="http://tools.spylog.ru/counter.js" type="text/javascript"> </script>
  </HEAD>


<?
	$smarty->display("./site/header.tpl");
?>
  


<table class="w" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td width="100%">

         <table width='100%' height='100%' cellpadding="0" cellspacing="0">
           <tr>
             <td valign='top' width='60%' height='100%'>
               <?
                  $smarty->display("./site/insertplan1.tpl");
               ?>
             </td>
           </tr>
         </table>
        
        </td>
    </tr>
</table>


<?
	$smarty->display("./site/footer.tpl");
?>


</body>
</html>


