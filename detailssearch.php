<?php
	global $DATABASE;
 
	include_once("./phpset.inc");
	include_once("./engine/functions.inc"); 

	$count_sql_call = 0;
	$start = get_formatted_microtime(); 
	$base_memory_usage = memory_get_usage();	

	AssignDataBaseSetting();
  
	include_once("./engine/class.category_lite.inc"); 
	$category = new Category_Lite();
	
	include_once("./engine/class.country_lite.inc"); 
	$country = new Country_Lite();	

	include_once("./engine/class.city_lite.inc"); 
	$city = new City_Lite();
	
	include_once("./engine/class.sell_lite.inc"); 
	$sell = new Sell_Lite();	

	include_once("./engine/class.buy_lite.inc"); 
	$buyer = new Buyer_Lite();	
  
  	include_once("./engine/class.metro_lite.inc"); 
  	$metro = new Metro_Lite();	
  
  	include_once("./engine/class.district_lite.inc"); 
  	$district = new District_Lite();	

  	include_once("./engine/class.subcategory_lite.inc");
  	$subcategory = new SubCategory_Lite();	

  	include_once("./engine/class.streets_lite.inc");
  	$streets = new Streets_Lite();	

	include_once("./engine/class.topparameters_lite.inc"); 
	$topparameters = new TopParameters_Lite();

  	include_once("./engine/class.equipment_lite.inc"); 
  	$equipment = new Equipment_Lite();  	
  	
  	include_once("./engine/class.ad.inc");
  	$ad = new Ad();  	
  	
  	include_once("./engine/class.company.inc"); 
  	$company = new Company();

        include_once("./engine/class.review.inc"); 
        $review = new Review();        
        
    include_once("./engine/class.frontend.inc");
	
	if(isset($_GET["brokerId"]) && intval($_GET["brokerId"]) > 0) {
		
		$_GET["brokerId"] = intval($_GET["brokerId"]);
		
		include_once("./engine/class.broker.inc"); 
		$broker = new Broker();
		
		$broker->Id = $_GET["brokerId"];
		$broker_getitem  = $broker->GetItem();
	}
	
	require_once("./libs/Smarty.class.php");
	$smarty = new Smarty;
	$smarty->template_dir = "./templates";
	$smarty->compile_dir  = "./templates_c";
	//$smarty->cache_dir = "./cache";
	$smarty->compile_check = true;
	$smarty->debugging     = false;

  	$colorset = GetColorSet();
        
  	$smarty->assign("colorset", $colorset);	

  	$smarty->assign("listcompany", $company->ListCompanyInList());  	
  	
	require_once("./regfuncsmarty.php");
  
	if(isset($_GET["ID"])) {
		$_GET["ID"] = intval($_GET["ID"]);
	}
  
	if(isset($_GET["rowCount"])) {
		$_GET["rowCount"] = intval($_GET["rowCount"]);
	}

	if(isset($_GET["offset"])) {
		$_GET["offset"] = intval($_GET["offset"]);
	}

	if(isset($_GET["streetID"])) {
		$_GET["streetID"] = intval($_GET["streetID"]);
	}
  
	if(isset($_GET["cityId"])) {
		$_GET["cityId"] = intval($_GET["cityId"]);
		$sell->CityID = $_GET["cityId"];
	}	

	if(isset($_GET["subCityId"])) {
		$_GET["subCityId"] = intval($_GET["subCityId"]);
		$sell->subCityID = $_GET["subCityId"];
	}	
	
	if(isset($_GET["catId"])) {
		$_GET["catId"] = intval($_GET["catId"]);
		$sell->CategoryID = $_GET["catId"];
	}	
	
	if(isset($_GET["subCatId"])) {
		$_GET["subCatId"] = intval($_GET["subCatId"]);
		$sell->SubCategoryID = $_GET["subCatId"];
	}	

        
        $_GET["startcostf"] = (isset($_GET["startcostf"]) ? intval($_GET["startcostf"]) : 0);
        $_GET["stopcostf"] = (isset($_GET["stopcostf"]) ? intval($_GET["stopcostf"]) : 0);
        
 	 if((intval($_GET["startcostf"]) > intval($_GET["stopcostf"])) && (intval($_GET["startcostf"]) > 0 && intval($_GET["stopcostf"]) > 0) ) {
  		$temp_c  = $_GET["stopcostf"];
  		$_GET["stopcostf"] = $_GET["startcostf"];
  		$_GET["startcostf"] = $temp_c;
  	}
	
  	if(isset($_GET["monthaveturn"]) && intval($_GET["monthaveturn"]) > 0) {
  		$_GET["monthaveturn"] = intval($_GET["monthaveturn"]);
  	} else {
  		$_GET["monthaveturn"] = 0;
  	}
  	
  	if(isset($_GET["profit"]) && intval($_GET["profit"]) > 0) {
  		$_GET["profit"] = intval($_GET["profit"]);
  	} else {
  		$_GET["profit"] = 0;
  	}  	
  	
  	
	if(isset($_GET["countryplace"])) {
		if($_GET["countryplace"] == 'eur') {
			$_GET["countyId"] = '3113,3112,3122,3108,3106,3107,3110,3120,3111,3115,3121,3117,3118,3676,4330,4507,4542,4605,4678';
		}
		if($_GET["countryplace"] == 'sng') {
			$_GET["countyId"] = '3075,2536,2537,3105';
		}
		
	}
  	
	$title_country = "";
	if(isset($_GET["countyId"])) {
	 	$list_countyId  = explode(",", $_GET["countyId"]);
	 	while (list($k_country, $v_country) = each($list_countyId)) {
	 		$list_countyId[$k_country] = intval($v_country);
	 	}
		if(in_array(3113, $list_countyId)) {	 	
			$title_country = "������� ������� � ������";
			$aCountry = $country->GetEuropeCountry();
			$smarty->assign('aCountry', $aCountry);
		} else if (in_array(3075, $list_countyId)) {
			$title_country = "������� ������� � ������� ���";
			$aCountry = $country->GetSNGCountry();
			$smarty->assign('aCountry', $aCountry);			
		}
	 	reset($list_countyId);
	 	$_GET["countyId"] = implode(",", $list_countyId);
	}

	$name_words = array("turagenstvo", "restoran", "parik", "krasota", "otel");
	if(isset($_GET["name"])) {
		if(in_array($_GET["name"], $name_words)) {
		} else {
			$_GET["name"] = "";
		}
	}
  
	$action_words = array("category", "city", "price", "topbiz");
	if(isset($_GET["action"])) {
		if(in_array($_GET["action"], $action_words)) {
		
		} else {
			$_GET["action"] = "";
		}
	}

	$sell->districtID = (isset($_GET["districtId"]) ? intval($_GET["districtId"]) : 0);
	$sell->metroID = (isset($_GET["metroId"]) ? intval($_GET["metroId"]) : 0);
	$sell->SubCategoryID = (isset($_GET["subCatId"]) ? intval($_GET["subCatId"]) : 0);
	$sell->CategoryID = (isset($_GET["catId"]) ? intval($_GET["catId"]) : 0);
	
	if(isset($_GET["subCityId"]) and intval($_GET["subCityId"]) > 0) {
		$sell->CityID = intval($_GET["subCityId"]);
        $objCity = $city->GetItem(array("ID" => $sell->CityID));
	} else {
		$sell->CityID = (isset($_GET["cityId"]) ? intval($_GET["cityId"]) : 0);	
	}
	$title = $sell->GetTitleForUserFriendlySelect();
	
	$title = (strlen($title) > 0 ? $title : "������� �������� �������");
	
	if(isset($broker_getitem)) {
		$title = $title." - �������� �".$broker_getitem->NameForSite."�";	
	}
	
	if(strlen($title_country) > 0) {
		$title = $title_country; 
	}
	
	$smarty->assign("title", $title);
	
	$ad->InitAd($smarty, $_GET);
 
	$smarty->assign('aCityRussia', array_chunk($country->GetCityRussia(), 11));
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html> 
  <HEAD>
   <title><?=$title;?></title>		
	<META NAME="Description" CONTENT="<?=$title;?>">
        <meta name="Keywords" content="<?=$title;?>"> 
        <LINK href="http://www.bizzona.ru/general.css" type="text/css" rel="stylesheet">
        <meta http-equiv="content-type" content="text/html; charset=windows-1251"/>
        <link rel="shortcut icon" href="http://www.bizzona.ru/images/favicon.ico">
    </HEAD>
<body>

<?
  	$smarty->display("./site/headerbanner.tpl");
?>
<?
	$topmenu = GetMenu();
	$smarty->assign("topmenu", $topmenu);
	
	$link_country = GetSubMenu();	
	$smarty->assign("ad", $link_country);
?>
<table class="w" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="2">
			<?
				$output_header = $smarty->fetch("./site/detailssearchheader.tpl");
				echo minimizer($output_header);			
			?>	
		</td>
	</tr>
    <tr>
        <td width="30%" valign="top" style="padding-top:2pt;">
        <?
        	$smarty->caching = false; 
			echo fminimizer($smarty->fetch("./site/inner_bannertop_left.tpl"));
		?>
        <?
        if(isset($_GET["brokerId"]) && intval($_GET["brokerId"]) > 0) {
        	ShowCategoryForBroker(intval($_GET["brokerId"]));
        } else {
			$sizeof_subcat_friendly = 0;
          	$sizeof_sel_friendly = 0;
          	$res_sel_friendly = array();
          	$res_sel_subcat_friendly = array();
          	if(isset($_GET["subCatId"]) && intval($_GET["subCatId"]) > 0 && isset($_GET["cityId"]) && intval($_GET["cityId"]) > 0) {
          		$smarty->caching = false;
          		$sell->SubCategoryID = intval($_GET["subCatId"]);
          		$sell->CityID = intval($_GET["cityId"]);
          		$res_sel_friendly = $sell->SelectUserFriendlySell();
          		$sizeof_sel_friendly = sizeof($res_sel_friendly);
          	} 
          	if (isset($_GET["catId"]) && intval($_GET["catId"]) > 0 /*&& $sizeof_sel_friendly <= 0*/) {
          		$set_cityId = ((isset($_GET["subCityId"]) && intval($_GET["subCityId"]) > 0) ? intval($_GET["subCityId"]) : intval($_GET["cityId"]));
          		$smarty->caching = false;
          		$sell->CategoryID = intval($_GET["catId"]);
          		$sell->CityID = $set_cityId;
          		$res_sel_subcat_friendly = $sell->SelectSubCategoryFriendly();
          		$sizeof_subcat_friendly = sizeof($res_sel_subcat_friendly);	
          	}
			if($sizeof_sel_friendly > 0) {
          		$smarty->assign("dataquickmetro", $res_sel_friendly);
  				$output_sel_friendly = $smarty->fetch("./site/quicksearchbymetro.tpl");
  				echo fminimizer($output_sel_friendly);
          	}
			if ($sizeof_subcat_friendly > 0) {
          		$smarty->assign("subcatfriendly", $res_sel_subcat_friendly);
  				$output_subcatfriendly = $smarty->fetch("./site/subcatfriendly.tpl");
  				echo fminimizer($output_subcatfriendly);          			
		  	} 
		  	if ($sizeof_sel_friendly <= 0 && $sizeof_subcat_friendly <= 0) {
		  		if(isset($_GET["countyId"])) {
		  			SeoSuggestedCatCountry($_GET["countyId"]);
		  		} else {
          			SeoSuggestedCat();
		  		}
          		
          	} else if ($sizeof_sel_friendly <= 0 && $sizeof_subcat_friendly > 0) {
          		if(isset($_GET["subCityId"]) && intval($_GET["subCityId"]) > 0) {
					$_GET["ID"] = intval($_GET["subCityId"]); 
					$_GET["action"] = "city";
          			ShowLeftCategory();
          		} else if (isset($_GET["cityId"]) && intval($_GET["cityId"]) > 0) {
					$_GET["ID"] = intval($_GET["cityId"]); 
					$_GET["action"] = "city";
					ShowLeftCategory();
          		}
          	}
        }  
        ?>
        <?
            $datahotbuyer["rowCount"] = 4;
            $datahotbuyer["sort"] = "datecreate";
            $datahotbuyer["StatusID"] = "-1";
            	
            if(isset($_GET["countyId"])) {
            	$datahotbuyer["countyId"] = $_GET["countyId"];
            }
            
            if (isset($_GET["action"])) {
            	switch(trim($_GET["action"])) {
	            	case "category": {
    	            	$datahotbuyer["typeBizID"] = intval($_GET["ID"]);
                    } break;
                    case "city": {
                    	$datahotbuyer["regionID"] = intval($_GET["ID"]);
                    } break;                       
        	        	default: {
                    }
                     break;
               }            	
           }		
            	
		    //$output_call = $smarty->fetch("./site/call.tpl");
			//echo fminimizer($output_call);
            echo '<div style="margin:3px;"></div>';  
            /* up ���������� ���� ����� ��� ����������� �������� ������ http://bizzona.ru/bgz/view.php?id=8 */
            //$dataadvertising['realty'] = true;
            //$dataadvertising['vip'] = true;
            //$dataadvertising['leftblock'] = 1;
            	
            //$dataad = $sell->SelectSellAdvertising($dataadvertising);
            //$smarty->assign("datarealty", $dataad);
            
					//$data = array();
					//$data["offset"] = 0;
					//$data["rowCount"] = 0;
					//$data["viptopposition"] = 1;
					//$data["sort"] = "DateStart";
					//$data["StatusID"] = "2";              
					//$data["leftblock"] = 1;
					//$result = $sell->Select($data);
					            
            //$smarty->assign("datarealty", $result);


                    LeftBlockTopSell();	 		
			  
			/* up  ����� ���� ����������� ���������� �� ������� �������� ������ http://bizzona.ru/bgz/view.php?id=8
  	        $res_datahotbuyer = $buyer->Select($datahotbuyer);
  	        if(sizeof($res_datahotbuyer) > 0) {
    			$smarty->assign("databuyer", $res_datahotbuyer);
            		
				$output_iblockbuyer = $smarty->fetch("./site/iblockbuyer.tpl");
				echo fminimizer($output_iblockbuyer);
  	        }
  	          	
			$output_proposal = $smarty->fetch("./site/proposal.tpl");
			echo fminimizer($output_proposal);
        	
			$output_request = $smarty->fetch("./site/request.tpl");
			echo fminimizer($output_request);
			
			$output_citydomains = $smarty->fetch("./site/citydomains.tpl");
           	echo minimizer($output_citydomains);
			down  ����� ���� ����������� ���������� �� ������� �������� ������ http://bizzona.ru/bgz/view.php?id=8
           	*/            	
        ?>              
        <?
        
			if($sell->SubCategoryID > 0) {

				include_once("./engine/class.subcategory_article.inc"); 
				$subcategory_article = new subcategory_article();	
				
                $smarty->caching = true; 
                $smarty->cache_dir = "./persistentcache";
                $smarty->cache_lifetime = 86400*30;

                if (!$smarty->is_cached("./site/subcategory_articles.tpl", $sell->SubCategoryID)) {
				
					$subcategory_article->subcategoryId = $sell->SubCategoryID; 
					$result_subcategory_article = $subcategory_article->Select();
					$smarty->assign("subcategory_article", $result_subcategory_article);
                }
				
				$output_subcategory_articles = $smarty->fetch("./site/subcategory_articles.tpl", $sell->SubCategoryID);
				echo minimizer($output_subcategory_articles);			
				
				$smarty->caching = false; 
			}	
        ?>
        </td>
        <td width="70%" valign="top">
        
			<?
				if(isset($objCity) && strlen($objCity->phone) > 0) {
					$smarty->assign("phone", $objCity->phone);
					$smarty->assign("phonetitle", $objCity->phonetitle);
				} 
				
				$smarty->caching = false;
				$output_phonecode = $smarty->fetch("./site/phonecode.tpl");
				echo minimizer($output_phonecode);						
				$smarty->caching = true;
			?>        
        
        	<?
        		//$trio_promotion = false;
        		//if(isset($_GET["brokerId"]) && (intval($_GET["brokerId"]) == 19 or intval($_GET["brokerId"]) == 30)) {
				//	$output_trioads = $smarty->fetch("./site/trioads.tpl");
				//	echo fminimizer($output_trioads);        			
				//	$trio_promotion = true;
        		//}
        	?>
        	<?
        		//if(!$trio_promotion) {
					ShowTopParameters();
					if(isset($_GET["cityId"]) && intval($_GET["cityId"]) > 0 && isset($_GET["subCatId"]) && intval($_GET["subCatId"]) > 0 ) {
				
						$sell->CityID = intval($_GET["cityId"]);
						$sell->SubCategoryID = intval($_GET["subCatId"]);
						$res_FriendlySellByDistrict = $sell->SelectUserFriendlySellByDistrict();
						if(sizeof($res_FriendlySellByDistrict) > 0) {
							$smarty->assign("dataFriendlySellByDistrict", $res_FriendlySellByDistrict);
							$output_quickseacrhbysubcategory = $smarty->fetch("./site/quickseacrhbysubcategory.tpl");
							echo fminimizer($output_quickseacrhbysubcategory);
						}
				
					}
        		//}
        	?>
            <?
            	if(!isset($_GET["countyId"]) && !isset($_GET["countryplace"]) && !isset($_GET["subCatId"]) && !isset($_GET["catId"]) && !isset($_GET["subCityId"]) && !isset($_GET["cityId"]) && !isset($_GET["brokerId"]) )	{
            		echo fminimizer($smarty->fetch("./site/citiesrussia.tpl"));
            	}
            
            	//if(!$trio_promotion) {
            
            		$show_ShowDetailsSearchForm = false;
            		$sell->SubCategoryID = (isset($_GET["subCatId"]) ? intval($_GET["subCatId"]) : 0);
            		if($sell->SubCategoryID > 0) {
            	
            			$_GET["catId"] = $sell->CategoryID;
            			$_GET["subCatId"] = $sell->SubCategoryID;
            		
           				$res_ListCityForSearch = $sell->ListCityForSearch();
           				$res_ListRegionForSearch = $sell->ListRegionForSearch();
           				while (list($k, $v) = each($res_ListCityForSearch)) {
           					if($v->CityID == 77) {
           						$smarty->assign("show_moscow_link", "1");
           					}
           					if($v->CityID == 78) {
           						$smarty->assign("show_spb_link", "1");
           					}
           				}
           				reset($res_ListCityForSearch);
           				while (list($k, $v) = each($res_ListRegionForSearch)) {
           					if($v->CityID == 50) {
           						$smarty->assign("show_moscow_region_link", "1");
           						break;
           					}
           				}
           				reset($res_ListRegionForSearch);
           				$smarty->assign("ListCityForSearch", $res_ListCityForSearch);
           				$smarty->assign("ListRegionForSearch", $res_ListRegionForSearch);

            			$output_SelectByRegionSubRegion = $smarty->fetch("./site/SelectByRegionSubRegion.tpl", $sell->SubCategoryID);
						echo fminimizer($output_SelectByRegionSubRegion);
					
            		} else if (isset($_GET["countyId"])) {
            			
            			$list_country = explode(",", $_GET["countyId"]);
            			if(in_array(3113, $list_country) or  in_array(3075, $list_country) or in_array(3112, $list_country) or in_array(3122, $list_country)) {
							$smarty->caching = false; 
							echo fminimizer($smarty->fetch("./site/countries.tpl"))."<br>";
            			} else  {
            	  			ShowDetailsSearchForm();	
            	  			$show_ShowDetailsSearchForm = true;            				
            			}
            			
            		} else {
            	  		ShowDetailsSearchForm();	
            	  		$show_ShowDetailsSearchForm = true;
            		}
            	//}
            	
            	//$smarty->display("./site/navsearch.tpl");
				$output_navsearch = $smarty->fetch("./site/navsearchdetails.tpl");
				echo fminimizer($output_navsearch);
				
                $data = array();
                $dataPage = array(); 
                $pagesplit = GetPageSplit();

                if (isset($_GET["offset"])) {
                	$data["offset"] = intval($_GET["offset"]);
                } else {
                	$data["offset"] = 0;
                }
                if (isset($_GET["rowCount"])) { 
                	$data["rowCount"] = intval($_GET["rowCount"]);
                } else {
                	$data["rowCount"] = $pagesplit;
                }

                $data["sort"] = "ID";

                    
                $data["cityId"] = (isset($_GET["cityId"]) ?  intval($_GET["cityId"]) : 0);
                $data["subCityId"] =  (isset($_GET["subCityId"]) ?  intval($_GET["subCityId"]) : 0);
                $data["catId"] =  (isset($_GET["catId"]) ?  intval($_GET["catId"]) : 0); 
                $data["subCatId"] = (isset($_GET["subCatId"]) ?  intval($_GET["subCatId"]) : 0);
                $data["metroId"] = (isset($_GET["metroId"]) ?  intval($_GET["metroId"]) : 0); 
                $data["districtId"] = (isset($_GET["districtId"]) ?  intval($_GET["districtId"]) : 0);
                $data["brokerId"] = (isset($_GET["brokerId"]) ?  intval($_GET["brokerId"]) : 0);
                $data["countyId"] = (isset($_GET["countyId"]) ?  $_GET["countyId"] : '');
                    
           
                $_GET["cityId"] = $data["cityId"];
                $_GET["subCityId"] = $data["subCityId"];
                $_GET["catId"] = $data["catId"];
                $_GET["subCatId"] = $data["subCatId"];
                $_GET["metroId"] = $data["metroId"];
                $_GET["districtId"] = $data["districtId"];
                $_GET["brokerId"] = $data["brokerId"];
                    
                if(isset($_GET["par1"])) {
                	$data["par1"] = intval($_GET["par1"]);
                    $_GET["par1"] = $data["par1"];
                    $dataPage["par1"] = $data["par1"];
                }
                if(isset($_GET["par2"])) {
                	$data["par2"] = intval($_GET["par2"]);
                    $_GET["par2"] = $data["par2"];
                    $dataPage["par2"] = $data["par2"];
                }
                if(isset($_GET["par3"])) {
                	$data["par3"] = intval($_GET["par3"]);
                    $_GET["par3"] = $data["par3"];
                    $dataPage["par3"] = $data["par3"];
                }
                if(isset($_GET["par4"])) {
                	$data["par4"] = intval($_GET["par4"]);
                    $_GET["par4"] = $data["par4"];
                    $dataPage["par4"] = $data["par4"];
                }
                if(isset($_GET["par5"])) {
                	$data["par5"] = intval($_GET["par5"]);
                    $_GET["par5"] = $data["par5"];
                    $dataPage["par5"] = $data["par5"];
                }
                if(isset($_GET["par6"])) {
                	$data["par6"] = intval($_GET["par6"]);
                    $_GET["par6"] = $data["par6"];
                    $dataPage["par6"] = $data["par6"];
                }
                if(isset($_GET["par7"])) {
                	$data["par7"] = intval($_GET["par7"]);
                    $_GET["par7"] = $data["par7"];
                    $dataPage["par7"] = $data["par7"];
                }                    
                    
                reset($data);

                $dataPage["offset"] = 0;
                $dataPage["rowCount"] = 0;
                $dataPage["sort"] = "ID";
                $dataPage["cityId"] = $data["cityId"];
                $dataPage["subCityId"] = $data["subCityId"];
                $dataPage["catId"] = $data["catId"];
                $dataPage["subCatId"] = $data["subCatId"];
                $dataPage["metroId"] = $data["metroId"];
                $dataPage["districtId"] = $data["districtId"];
                $dataPage["brokerId"] = $data["brokerId"];
                $dataPage["countyId"] = $data["countyId"];
                    
  				$data["stopcostf"] = intval($_GET["stopcostf"]);
  				$data["startcostf"] = intval($_GET["startcostf"]);			
  				$data["monthaveturn"] = intval($_GET["monthaveturn"]);			
  				$data["profit"] = intval($_GET["profit"]);
  				$dataPage["stopcostf"] = $data["stopcostf"];
				$dataPage["startcostf"] = $data["startcostf"];
				$dataPage["monthaveturn"] = $data["monthaveturn"];
				$dataPage["profit"] = $data["profit"];
                
                $resultPage = $sell->DetailsCountSelect($dataPage);
                    
                $smarty->assign("CountRecord",$resultPage);
                $smarty->assign("CountSplit", $pagesplit);
                $smarty->assign("CountPage", ceil($resultPage/5));

                $data["StatusID"] = "-1";
                $data["sort"] = "pay_datetime";

                $result = $sell->DetailsSelect($data);

                $smarty->assign("data", $result);

				$output_pagesplit = $smarty->fetch("./site/pagesplitdetails.tpl", $_SERVER["REQUEST_URI"]);
				echo fminimizer($output_pagesplit);
                  
                   
				$saveme = $smarty->fetch("./site/saveme.tpl");
				echo $saveme;

                $smarty->assign('colorsell', FrontEnd::ListColorSet());

				$output_ilistsell = $smarty->fetch("./site/ilistsell.tpl", $_SERVER["REQUEST_URI"]);
                                echo fminimizer($output_ilistsell);
				   
                if($resultPage <= 0) {
                   	?>
                   		<div class="lftpadding" style="width: auto;">
                    		<div style="font-family:Arial;font-size:13pt;color:#7C3535;font-weight:bold;padding-top:4pt;margin:0pt;">�� ������� ������� �������� �� ������� �� �������</div>
                    	</div>		
                    <?	
                }

                  
               ?>
            	<div class="lftpadding" style="width: auto;">
       				<div  style="background-image:url(<?=$_SERVER["HTTP_HOST"]?>/images/bl.gif); background-repeat:repeat-x;">&nbsp;</div>
            	</div>
            	<?
				  	echo fminimizer($output_pagesplit);
				  	echo fminimizer($output_navsearch);

				?>

				<?
					include_once("./yadirect.php");
				?>
								
				<?  	
				  	
				  	if(!$show_ShowDetailsSearchForm) {
				  		ShowDetailsSearchForm();
				  	}
				?>
				
				
				<div class="lftpadding_cnt" style="padding-top:10pt; padding-bottom: 5pt;padding-top:0pt; padding-bottom:0pt; width:auto;" >	    
					<table border="0" align="right" width="100%">
                		<tr>
                			<td width="30%"></td>
                			<td width="70%" align="right">
								<script type="text/javascript" src="//yandex.st/share/share.js" charset="utf-8"></script>
								<div class="yashare-auto-init" data-yashareType="button" data-yashareQuickServices="yaru,vkontakte,facebook,twitter,odnoklassniki,moimir,lj,friendfeed,moikrug"></div>
               				</td>			
							</tr>
						</table>
				</div>

                               <div class="lftpadding_cnt">
                                <?
                                    echo $review->LastBlockReview();
                                ?>
                               </div>            
				
        </td>
    </tr>
    <tr>
    	<td colspan="2">
				<?
				
					$smarty->caching = true; 
					$smarty->cache_lifetime = 10500;
					
					if (!$smarty->is_cached('./site/sugequipment.tpl', 'detailssearcj&catId='.$_GET["catId"].'&subcatId='.$_GET["subCatId"])) {
            
						$datasugequipment = array();
						$datasugequipment["offset"] = 0;
						$datasugequipment["rowCount"] = 4;
						$datasugequipment["sort"] = "datecreate";
						$datasugequipment["status_id"] = "-1";
						if(isset($_GET["catId"])) {
							$datasugequipment["category_id"] = $_GET["catId"];
						} else if (isset($_GET["subCatId"])) {
							$datasugequipment["subCategory_id"] = $_GET["subCatId"];
						}
            	 
						$res_datasugequipment = $equipment->SugSelect($datasugequipment);
						
						if(sizeof($res_datasugequipment) <= 2) {
							unset($datasugequipment["category_id"]);
							unset($datasugequipment["subCategory_id"]);
							$res_datasugequipment = $equipment->SugSelect($datasugequipment);
						}
  	          	
						$smarty->assign("datasugequipment", $res_datasugequipment);        	
					}				
				
					$output_sugequipment = $smarty->fetch("./site/sugequipment.tpl", 'detailssearcj&catId='.$_GET["catId"].'&subcatId='.$_GET["subCatId"]);
					echo fminimizer($output_sugequipment);
					
					$smarty->caching = false; 					
					
				?>
    	
    	</td>
    </tr>
	<tr>
		<td colspan="2">
			<div style="padding-top:4pt;padding-bottom:5pt;padding-right:10pt;padding-left:10pt;font-size:10pt;font-family:arial;">
				<table cellpadding="0" cellspacing="0" border="0"  style="width:100%;padding-bottom:2pt;" bgcolor="#eeeee0">
    				<tr>
        				<td style="background-image:url(http://<?=$_SERVER["HTTP_HOST"]?>/images/d1.gif); background-repeat:no-repeat;width:5px;height:5px; background-position:left top;">&nbsp;</td>
            			<td rowspan="2" valign="middle" align="center"  style="font-size:8pt; font-family:Arial; color:black;">
             				<a href="http://www.bizzona.ru/prodazha-biznesa-avtomoika.php" title="������� ���������">������� ���������</a>&nbsp;
             				<a href="http://www.bizzona.ru/prodazha-biznesa-avtoskola.php" title="������� ���������">������� ���������</a>&nbsp;
             				<a href="http://www.bizzona.ru/prodazha-biznesa-employment-agency.php" title="������� ��������� ���������">������� ��������� ���������</a>&nbsp;
             				<a href="http://www.bizzona.ru/prodazha-biznesa-building-company.php" title="������� ������������ �����">������� ������������ �����</a>&nbsp;
             				<a href="http://www.bizzona.ru/prodazha-biznesa-car-care-center.php" title="������� �����������">������� �����������</a>&nbsp;
             				<a href="http://www.bizzona.ru/prodazha-biznesa-car-magazin.php" title="������� ��������">������� ��������</a>&nbsp;
            			</td>
            			<td style="background-image:url(http://<?=$_SERVER["HTTP_HOST"]?>/images/d2.gif); background-repeat:no-repeat;width:5px;height:5px;background-position:right top;">&nbsp;</td>
        			</tr>
        			<tr>
            			<td style="background-image:url(http://<?=$_SERVER["HTTP_HOST"]?>/images/d4.gif); background-repeat:no-repeat;width:5px;height:5px; background-position:left bottom;">&nbsp;</td>
            			<td style="background-image:url(http://<?=$_SERVER["HTTP_HOST"]?>/images/d3.gif); background-repeat:no-repeat;width:5px;height:5px; background-position:right bottom;">&nbsp;</td>
        			</tr>
    			</table>
			</div>
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<?
				$subdomain_city = $smarty->fetch("./site/site_city.tpl");
				echo fminimizer($subdomain_city);
				$smarty->display("./site/footer.tpl");
			?>
			<?
  				$end = get_formatted_microtime(); 
  				$total = $end - $start;
  				echo "<center><span style='font-size:7pt;'>".round($total, 6)." : ".$count_sql_call." : ".$base_memory_usage." : ".memoryUsage(memory_get_usage(), $base_memory_usage)." </span><center>";
			?>			
		</td>
	</tr>
</table>
<style>
 td a 
 {
   color:black;
 }
</style>

<script language='javascript'>
  function SellPreviewAdd() {
  	
  }
  function AddToSearch(obj) {
	var input_search = document.getElementById("searchtext");
	if(input_search != null) {
		if(obj.innerText != null) {
			input_search.value = obj.innerText;
		} else if (obj.innerHTML != null) {
			input_search.value = obj.innerHTML;
		}
	}
  }  
</script>

</body>
</html>
<?
	$sell->Close();
	$category->Close();
	$city->Close();
	$country->Close();
	$buyer->Close();
	$metro->Close();
	$district->Close();
	$subcategory->Close();
	$streets->Close();
?>