<?
    global $DATABASE;

    include_once("./phpset.inc");
    include_once("./engine/functions.inc"); 
    
    AssignDataBaseSetting();

    //ini_set("display_startup_errors", "on");
    //ini_set("display_errors", "on"); 
    //ini_set("register_globals", "on");
    
    include_once("./engine/class.category.inc"); 
    $category = new Category();

    include_once("./engine/class.subcategory.inc"); 
    $subCategory = new SubCategory();

    include_once("./engine/class.investments.inc"); 
    $investments = new Investments();
  
    include_once("./engine/class.BlockIP.inc"); 
    $blockIp = new BlockIP();
    
    $blocking  = $blockIp->YouAreBlocking(array(
        'ip' => $_SERVER['REMOTE_ADDR']
    ));  
  
    include_once("./engine/class.insert_settings.inc"); 
    $insertSettings = new InsertSettings();  
    
    $insertSettings->type_insert = ConstInsertSettings::INVESTMENTS;
    $insertSettingsData = $insertSettings->GetItem();    
    
    include("./engine/FCKeditor/fckeditor.php"); 

    require_once("./libs/Smarty.class.php");
    $smarty = new Smarty;
    $smarty->template_dir = "./templates";
    $smarty->compile_dir  = "./templates_c";
    $smarty->compile_check = true;
    $smarty->debugging     = false;

    require_once("./regfuncsmarty.php");
    
    $data = array();
    $data["offset"] = 0;
    $data["rowCount"] = 0;
    $data["sort"] = "ID";

    $listCategory = $category->Select($data);
    $smarty->assign("listCategory", $listCategory);

    $listSubCategory = $subCategory->Select($data);
    $smarty->assign("listSubCategory", $listSubCategory);

    $data = array();
    $data["offset"] = 0;
    $data["rowCount"] = 0;
    $data["sort"] = "Count";
    $result = $category->Select($data);
    $smarty->assign("categorylist", $result);
  
  if (isset($_POST) && sizeof($_POST) && !$blocking) {
  	
  	$error = false;
  	$error_text = "";
 	
	$t_name = htmlspecialchars(trim($_POST["name"]), ENT_QUOTES);
	$_POST["name"] = $t_name;
	
	$total_typeBizID = htmlspecialchars(trim($_POST["category_id"]), ENT_QUOTES);
	$arr_typeBizID = split("[:]",$total_typeBizID);
	if(sizeof($arr_typeBizID) == 2) {
		$data["category_id"] = intval($arr_typeBizID[0]);
		$data["subCategory_id"] = intval($arr_typeBizID[1]);
		$_POST["subCategory_id"] = $data["subCategory_id"];
		$t_category_id = $data["category_id"];
		$t_subCategory_id = $data["subCategory_id"];
	} else {
		$data["category_id"] = "-1";
		$data["subCategory_id"] = "-1";
		$t_category_id = $data["category_id"];
		$t_subCategory_id = $data["subCategory_id"];
	}
	
	$t_region_text = htmlspecialchars(trim($_POST["region_text"]), ENT_QUOTES);
	$_POST["region_text"] = $t_region_text;
	
	$t_region_id = "";
	$t_cost = htmlspecialchars(trim($_POST["cost"]), ENT_QUOTES);
	$_POST["cost"] = $t_cost;
	
	$t_ccost = htmlspecialchars(trim($_POST["ccost"]), ENT_QUOTES);
	$_POST["ccost"] = $t_ccost;
	
	$t_period = htmlspecialchars(trim($_POST["period"]), ENT_QUOTES);
	$_POST["period"] = $t_period;
	
	$t_payback = htmlspecialchars(trim($_POST["payback"]), ENT_QUOTES);
	$_POST["payback"] = $t_payback;
	
	$t_yearprofit = htmlspecialchars(trim($_POST["yearprofit"]), ENT_QUOTES);
	$_POST["yearprofit"] = $t_yearprofit;
	
	$t_cyearprofit = htmlspecialchars(trim($_POST["cyearprofit"]), ENT_QUOTES);
	$_POST["cyearprofit"] = $t_cyearprofit;
	
	$t_totalinvestcost = htmlspecialchars(trim($_POST["totalinvestcost"]), ENT_QUOTES);
	$_POST["totalinvestcost"] = $t_totalinvestcost;
	
	$t_totalinvestccost = htmlspecialchars(trim($_POST["totalinvestccost"]), ENT_QUOTES);
	$_POST["totalinvestccost"] = $t_totalinvestccost;
	
	$t_stage = htmlspecialchars(trim($_POST["stage"]), ENT_QUOTES);
	$_POST["stage"] = $t_stage;
	
	$t_documents = htmlspecialchars(trim($_POST["documents"]), ENT_QUOTES);
	$_POST["documents"] = $t_documents;
	
	$t_contactface = htmlspecialchars(trim($_POST["contactface"]), ENT_QUOTES);
	$_POST["contactface"] = $t_contactface;
	
	$t_contactphone = htmlspecialchars(trim($_POST["contactphone"]), ENT_QUOTES);
	$_POST["contactphone"] = $t_contactphone;
	
	$t_contactfax = htmlspecialchars(trim($_POST["contactfax"]), ENT_QUOTES);
	$_POST["contactfax"] = $t_contactfax;
	
	$t_contactemail = htmlspecialchars(trim(strtolower($_POST["contactemail"])), ENT_QUOTES);
	$_POST["contactemail"] = $t_contactemail;
	
	$t_shortDescription = htmlspecialchars(trim($_POST["shortDescription"]), ENT_QUOTES);
	$_POST["shortDescription"] = $t_shortDescription;
	
	$t_attractedPerson = htmlspecialchars(trim($_POST["attractedPerson"]), ENT_QUOTES);
	$_POST["attractedPerson"] = $t_attractedPerson;
  	
  	if (strlen(trim($t_name)) <= 0)
  	{
  		$error = true;	
  		$text_error = "������ ����� ������ \"�������� �������\"";
  		$smarty->assign("ErrorInput_name",true);
  	}
  	else if(strlen(trim($t_cost)) <= 0)
  	{
  		$error = true;
  		$text_error = "������ ����� ������ \"��������� ����� ����������\"";
  		$smarty->assign("ErrorInput_cost",true);
  	}
  	else if(strlen(trim($t_region_text)) <= 0)
  	{
  		$error = true;
  		$text_error = "������ ����� ������ \"������\"";
  		$smarty->assign("ErrorInput_region_text",true);
  	}
  	else if(strlen(trim($t_shortDescription)) <= 0)
  	{
  		$error = true;
  		$text_error = "������ ����� ������ \"������� ��������\"";
  		$smarty->assign("ErrorInput_shortDescription",true);
  	}
  	
  	else if(strlen(trim($t_contactface)) <= 0)
  	{
  		$error = true;
  		$text_error = "������ ����� ������ \"���������� ����\"";
  		$smarty->assign("ErrorInput_contactface",true);
  	}
  	else if(strlen(trim($t_contactphone)) <= 0)
  	{
  		$error = true;
  		$text_error = "������ ����� ������ \"��������\"";
  		$smarty->assign("ErrorInput_contactphone",true);
  	}
  	else if(strlen(trim($t_contactemail)) <= 0)
  	{
  		$error = true;
  		$text_error = "������ ����� ������ \"Email\"";
  		$smarty->assign("ErrorInput_contactemail",true);
  	}
  	
  	if(!$error)
  	{
		$investments->name = $t_name;
		$investments->category_id = $t_category_id;
		$investments->subCategory_id = $t_subCategory_id;
		$investments->region_text = $t_region_text;
		$investments->region_id = $t_region_id;
		$investments->cost = $t_cost;
		$investments->ccost = $t_ccost;
		$investments->period = $t_period;
		$investments->payback = $t_payback;
		$investments->yearprofit = $t_yearprofit;
		$investments->cyearprofit = $t_cyearprofit;
		$investments->totalinvestcost = $t_totalinvestcost;
		$investments->totalinvestccost = $t_totalinvestccost;
		$investments->stage = $t_stage;
		$investments->documents = $t_documents;
		$investments->contactface = $t_contactface;
		$investments->contactphone = $t_contactphone;
		$investments->contactfax = $t_contactface;
		$investments->contactemail = $t_contactemail;
		$investments->shortDescription = $t_shortDescription;
		$investments->qview = 0;
		$investments->attractedPerson = $t_attractedPerson;
		
		if(isset($_SESSION["partnerid"])) {
			$investments->partnerid =  intval($_SESSION["partnerid"]);
		}
		
		$code = $investments->Insert();
                $_SESSION['timepost'] = time();
		
		$secret_code = "vbhjplfybt";
		$purse = "4146"; // sms:bank id        ������������� ���:�����
		$order_id = intval($code) + 9000000; //  ������������� ��������
		$amount = "3"; // transaction sum  ����� ����������
		$clear_amount  = "0"; // billing algorithm  �������� �������� ���������
		$description  = "������ ���������� ����������"; // operation desc  �������� ��������
  		$submit  = "���������"; // submit label    ������� �� ������ submit

		$sign = ref_sign($purse, $order_id, $amount, $clear_amount, $description, $secret_code);

		$m_description = cp1251_to_utf8($description." - �������������� (".$order_id.") ");
		$m_description = base64_encode($m_description);		
		
		
                $smarty->assign("purse", $purse);
                $smarty->assign("order_id", $order_id);
                $smarty->assign("rbk_order_id", $order_id - 9000000);
                $smarty->assign("amount", $amount);
                $smarty->assign("clear_amount", $clear_amount);
                $smarty->assign("description", $description);
                $smarty->assign("m_description", $m_description);
                $smarty->assign("submit", $submit);
                $smarty->assign("sign", $sign);
                $smarty->assign("email", $investments->contactemail);



                $to      = 'bizzona.ru@gmail.com';
                $subject = 'NEW INVESTMENTS: '.$investments->name.'';

                $message = "�������� ".$investments->name."\n";
                $message.= "��������� ����� ���������� ".$investments->cost."\n";
                $message.= "��������� ����� ����������(���) ".$investments->ccost."\n";
                $message.= "��� ������� ".$investments->category_id."\n";
                $message.= "������ ".$investments->region_text."\n";
                $message.= "���� ����������� ���������� ".$investments->period."\n";
                $message.= "����������� ".$investments->payback."\n";
                $message.= "������� ���������� ".$investments->yearprofit."\n";
                $message.= "����� ������������� ���������� ".$investments->totalinvestcost."\n";
                $message.= "����� ������������� ����������(���) ".$investments->totalinvestccost."\n";
                $message.= "������ ���������� ".$investments->stage."\n";
                $message.= "��������� � ������� ".$investments->documents."\n";
                $message.= "������� �������� ".$investments->shortDescription."\n";
                $message.= "������������ ��������� ".$investments->attractedPerson."\n";
                $message.= "���������� ���� ".$investments->contactface."\n";
                $message.= "�������� ".$investments->contactphone."\n";
                $message.= "Email ".$investments->contactemail."\n";
                $message.= "������������ ���������� ".$investments->period_id."\n";

                $from = "eugenekurilov@gmail.com";

                $headers =  "From: BizZONA.ru  <".$from.">\r\n".               
                                        "Reply-To: BizZONA.ru <".$from.">\r\n".
                                        "Content-type: text/plain; charset=windows-1251;\r\n".
                                        "Return-Path: ".$from."\r\n";


                mail($to, $subject, $message, $headers);
		
        
  		unset($_POST);
  	}
  	else 
  	{
  		$smarty->assign("text_error", $text_error);
  	}
  	
  } 
  
    include_once("./engine/class.ContactInfo_Lite.inc");
    $contact = new ContactInfo_Lite();  

    $contact->GetContactInfo();  
    $smarty->assign("contact", $contact);

    $smarty->assign("insertsettings", $insertSettingsData);    
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>

  <HEAD>
      <title>���������� ������ �������������� - ������ ����</title>		
	<META NAME="Description" CONTENT="������ ��������������, ������� �������, ������� �������, ������� ������, ������� �������, ������� �������� �������, ������� �������, ������� ��������,  ������� ��������, ������� ��������, ���� ������� ������?">
        <meta name="Keywords" content="������ ��������������, ������� �������, ������� �������, ������� ������, ������� �������, ������� �������� �������, ������� �������, ������� ��������,  ������� ��������, ������� ��������, ���� ������� ������?, Ready business, �����������, �������� � ��������, ���������, ����������, �����������"> 
	<meta http-equiv="content-type" content="text/html; charset=windows-1251"/>
        <LINK href="./general.css" type="text/css" rel="stylesheet">
  </HEAD>

<?
    $topmenu = GetMenu();
    $smarty->assign("topmenu", $topmenu);

    $link_country = "&nbsp;&nbsp;<a href='".NAMESERVER."help.php' class='city_a'  style='color:white;font-size:10pt;color:yellow;' title='������ - ������� �������' target='_blank'>������</a>&nbsp;&nbsp;";	
    $smarty->assign("ad", $link_country);

    $smarty->display("./site/headeri.tpl");

    $smarty->assign("insertsettings", $insertSettingsData);        
?>

<table class="w" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td width="100%">
            <? 
                if (isset($code) && $code > 0 && (int)$insertSettingsData['type_show'] == ConstInsertSettings::TYPE_SHOW_PAY) {

                    $pay = $smarty->fetch("./site/confirmpaymentinvestment.tpl");
                    echo $pay;
                    
                } else if (isset($code) && $code > 0 && in_array($insertSettingsData['type_show'], array(ConstInsertSettings::TYPE_SHOW_NO_PAY, ConstInsertSettings::TYPE_SHOW_PROMO)) ) {
                  ?>
            
                    <table width='100%' align='center' bgcolor='#EDEDCC' style='border-top: 1px; border-bottom: 1px; border-left:1px;  border-right:1px;  border-color:#C1C1A4;  border-style: solid;' cellpadding="0" cellspacing="0" >
                        <tr>
                          <td valign='top' style='padding-left:5pt;padding-right:5pt;'>
                            <h2>���� ���������� �� �������������� �������!</h2>
                          </td>
                        </tr>
                    </table>            
            
                  <?
                } else {
            ?>
                <table width='100%' align='center' bgcolor='#EDEDCC' style='border-top: 1px; border-bottom: 1px; border-left:1px;  border-right:1px;  border-color:#C1C1A4;  border-style: solid;' cellpadding="0" cellspacing="0" >
                    <tr>
                         <td  valign='top' style='padding-left:5pt;padding-right:5pt;'>
                            <div align="center" style="padding-top:5pt;padding-bottom:5pt;font-size:14pt;font-weight:bold;">���������� ���������� �� �������������</div>
                            <?
                                echo $insertSettingsData['content'];
                                
                                if($insertSettingsData['use_promo']) {
                                    $promo = $smarty->fetch("./site/promo.tpl");
                                    echo $promo;
                                }                                
                            ?>
                         </td>
                         <td width="25%" valign="top" style="padding-top:5pt;padding-right:5pt;">
                            <?
                               $out_req = $smarty->fetch("./site/requestmin.tpl");
                               echo $out_req;
                            ?>
                         </td>
                     </tr>
                </table>
            

                <table class="w" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td width="100%">
                         <table width='100%' height='100%' cellpadding="0" cellspacing="0">
                           <tr>
                             <td valign='top' width='60%' height='100%'>
                               <?
                                  if($blocking) {
                                      $smarty->display("./site/blockip.tpl");                      
                                  }  else {
                                      $smarty->display("./site/insertinvestments.tpl");
                                  }
                               ?>
                             </td>
                           </tr>
                         </table>
                        </td>
                    </tr>
                </table>
            
            
            
            
                <?
                }
            ?>
        </td>
    </tr>		
</table>

<?
	$smarty->display("./site/footer.tpl");
?>

</body>
</html>


