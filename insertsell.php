<?php
    global $DATABASE;

    include_once("./phpset.inc");
    include_once("./engine/functions.inc");
    include_once("./engine/exportfunctions.inc");
  
    include_once('./engine/class.phpmailer.php');
    
    include_once('./inserthelp.php');

    ini_set("display_startup_errors", "off");
    ini_set("display_errors", "off");
    ini_set("register_globals", "on");

    $start = get_formatted_microtime();
 
    AssignDataBaseSetting();
    include_once("./engine/class.category.inc"); 
    $category = new Category();

    include_once("./engine/class.subcategory.inc"); 
    $subCategory = new SubCategory();
  
    include_once("./engine/class.sell.inc"); 
    $sell = new Sell();

    include_once("./engine/class.BlockIP.inc"); 
    $blockIp = new BlockIP();
    
    include_once("./engine/class.Settings.inc");
    $settings = new Settings();
    
    include_once("./engine/class.insert_settings.inc"); 
    $insertSettings = new InsertSettings();  
    
    $insertSettings->type_insert = ConstInsertSettings::SELL;
    $insertSettingsData = $insertSettings->GetItem();    
    
    
    $blocking  = $blockIp->YouAreBlocking(array(
        'ip' => $_SERVER['REMOTE_ADDR']
    ));
    
    require_once("./libs/Smarty.class.php");
    $smarty = new Smarty;
    $smarty->template_dir = "./templates";
    $smarty->compile_dir  = "./templates_c";
    //$smarty->cache_dir = "./cache";
    $smarty->compile_check = true;
    $smarty->debugging     = false;
    $smarty->clear_compiled_tpl("./site/insertsell.tpl");

    $smarty->register_function('NameCity', 'getNameCity');

    $assignDescription = AssignDescription();

    $promo =  (int)$insertSettingsData['use_promo']; //$settings->GetValue('PROMO');

    $error = "";

    if(isset($_POST) && count($_POST)) {
        
        $newname_ImgID = LoadClientImg("ImgID", 1);
        if(strlen($newname_ImgID) > 0) {
            $_SESSION['newname_ImgID'] = $newname_ImgID;
        } else if (isset($_SESSION['newname_ImgID'])) {
            $newname_ImgID = $_SESSION['newname_ImgID'];
        }
        
        $newname_Img1ID = LoadClientImg("Img1ID", 2);
        if(strlen($newname_Img1ID) > 0) {
            $_SESSION['newname_Img1ID'] = $newname_Img1ID;
        } else if (isset($_SESSION['newname_Img1ID'])) {
            $newname_Img1ID = $_SESSION['newname_Img1ID'];
        }
        
        $newname_Img2ID = LoadClientImg("Img2ID", 3);
        if(strlen($newname_Img2ID) > 0) {
            $_SESSION['newname_Img2ID'] = $newname_Img2ID;
        } else if (isset($_SESSION['newname_Img2ID'])) {
            $newname_Img2ID = $_SESSION['newname_Img2ID'];
        }
        
        $newname_Img3ID = LoadClientImg("Img3ID", 4);
        if(strlen($newname_Img3ID) > 0) {
            $_SESSION['newname_Img3ID'] = $newname_Img3ID;
        } else if (isset($_SESSION['newname_Img3ID'])) {
            $newname_Img3ID = $_SESSION['newname_Img3ID'];
        }
        
        $newname_Img4ID = LoadClientImg("Img4ID", 5);
        if(strlen($newname_Img4ID) > 0) {
            $_SESSION['newname_Img4ID'] = $newname_Img4ID;
        } else if (isset($_SESSION['newname_Img4ID'])) {
            $newname_Img4ID = $_SESSION['newname_Img4ID'];
        }
        
        $data = ReadingDataPostSell();
    }
    
    if (isset($_POST["insert"]) && !$blocking) {

        $ErrorFile = false;
     
    
        if (!$ErrorFile) {
    	
            if (strlen($data["FML"]) <= 0) {
                $error = "������ �����: "."\"��� � ������� ����, �������������� �� ��������\"";
                $smarty->assign("ErrorInput_FML",true);
            } else if (strlen($data["EmailID"]) <= 0) {
                $error = "������ �����: "."\"���������� �-����\"";
                $smarty->assign("ErrorInput_EmailID",true);
            } else if (strlen($data["PhoneID"]) <= 0) {
                $error = "������ �����: "."\"����� ��������\"";
                $smarty->assign("ErrorInput_PhoneID",true);
            } else if (strlen($data["NameBizID"]) <= 0) { 
                $error = "������ �����: "."\"�������� �������\"";
                $smarty->assign("ErrorInput_NameBizID",true);
            } else if (strlen($data["SiteID"]) <= 0) { 
                $error = "������ �����: "."\"�����, �����, ���, ��������/����\"";
                $smarty->assign("ErrorInput_SiteID",true);
            } else if (strlen($data["CostID"]) <= 0) {
                $error = "������ �����: "."\"��������� ������������ �������\"";
                $smarty->assign("ErrorInput_CostID",true);
            } else if (strlen($data["ShortDetailsID"]) <= 0) {
                $error = "������ �����: "."\"������� ����������, ������ ������������� � ����������� �������\"";     
                $smarty->assign("ErrorInput_ShortDetailsID",true);
            } else if (strlen($data["DetailsID"]) <= 0) {
                $error = "������ �����: "."\"������ ������ ����������, ������ ����� ������� ������������� � ����������� �������\"";     
                $smarty->assign("ErrorInput_DetailsID",true);
            } else {
        }
 
        if (strlen($error) <= 0) {

            $to      = 'bizzona.ru@gmail.com';
            $from      = 'bizzona.ru@gmail.com';
            $subject = '������ �� ������� �������: '.$data["FML"].'';

            $message = "<table border=1 width='100%'>";

            if($data["helpbroker"] == 1) {
                    $message.= "<tr><td colspan=2><h3 style='color:red;'>����� ���������� �����</h3></td></tr>";	
            }

            $message.= "<tr><td width='30%'><b>".$assignDescription["FML"].":</b></td><td>&nbsp;".$data["FML"]."</td></tr>";
            $message.= "<tr><td><b>".$assignDescription["EmailID"].":</b></td><td>&nbsp;".$data["EmailID"]."</td></tr>";
            $message.= "<tr><td><b>".$assignDescription["PhoneID"].":</b></td><td>&nbsp;".$data["PhoneID"]."</td></tr>";
            $message.= "<tr><td><b>".$assignDescription["FaxID"].":</b></td><td>&nbsp;".$data["FaxID"]."</td></tr>";
            $message.= "<tr><td><b>".$assignDescription["NameBizID"].":</b></td><td>&nbsp;".$data["NameBizID"]."</td></tr>";

            $nameCategory = $subCategory->GetName(intval($data["CategoryID"]));

            if(strlen($nameCategory) > 0) {
                    $message.= "<tr><td><b>".$assignDescription["CategoryID"].":</b></td><td>&nbsp;".$nameCategory."</td></tr>";
            }

            $message.= "<tr><td><b>".$assignDescription["BizFormID"].":</b></td><td>&nbsp;".NameBizFormByID($data["BizFormID"])."</td></tr>";
            //$message.= "<tr><td>".$assignDescription["CityID"]." ".$data["CityID"]."</td></tr>";
            $message.= "<tr><td><b>".$assignDescription["SiteID"].":</b></td><td>&nbsp;".$data["SiteID"]."</td></tr>";
            $message.= "<tr><td><b>".$assignDescription["CostID"].":</b></td><td>&nbsp;".$data["CostID"]." (".$data["cCostID"].")</td></tr>";
            $message.= "<tr><td><b>".$assignDescription["ProfitPerMonthID"].":</b></td><td>&nbsp;".$data["ProfitPerMonthID"]." (".$data["cProfitPerMonthID"].")</td></tr>";
            $message.= "<tr><td><b>".$assignDescription["TimeInvestID"].":</b></td><td>&nbsp;".$data["TimeInvestID"]."</td></tr>";
            $message.= "<tr><td><b>".$assignDescription["ListObjectID"].":</b></td><td>&nbsp;".$data["ListObjectID"]."</td></tr>";
            $message.= "<tr><td><b>".$assignDescription["ContactID"].":</b></td><td>&nbsp;".$data["ContactID"]."</td></tr>";
            $message.= "<tr><td><b>".$assignDescription["MatPropertyID"].":</b></td><td>&nbsp;".$data["MatPropertyID"]."</td></tr>";
            $message.= "<tr><td><b>".$assignDescription["MonthAveTurnID"].":</b></td><td>&nbsp;".$data["MonthAveTurnID"]." (".$data["cMonthAveTurnID"].")</td></tr>";
            $message.= "<tr><td><b>".$assignDescription["SumDebtsID"].":</b></td><td>&nbsp;".$data["SumDebtsID"]." (".$data["cSumDebtsID"].")</td></tr>";
            $message.= "<tr><td><b>".$assignDescription["WageFundID"].":</b></td><td>&nbsp;".$data["WageFundID"]." (".$data["cWageFundID"].")</td></tr>";
            $message.= "<tr><td><b>�������� �����:</b></td><td>&nbsp;".$data["WageFundID"]." (".$data["cWageFundID"].")</td></tr>";
            $message.= "<tr><td><b>".$assignDescription["ObligationID"].":</b></td><td>&nbsp;".$data["ObligationID"]."</td></tr>";
            $message.= "<tr><td><b>".$assignDescription["CountEmplID"].":</b></td><td>&nbsp;".$data["CountEmplID"]."</td></tr>";
            $message.= "<tr><td><b>".$assignDescription["CountManEmplID"].":</b></td><td>&nbsp;".$data["CountManEmplID"]."</td></tr>";
            $message.= "<tr><td><b>".$assignDescription["TermBizID"].":</b></td><td>&nbsp;".$data["TermBizID"]."</td></tr>";
            $message.= "<tr><td><b>".$assignDescription["ShortDetailsID"].":</b></td><td>&nbsp;".$data["ShortDetailsID"]."</td></tr>";
            $message.= "<tr><td><b>".$assignDescription["DetailsID"].":</b></td><td>&nbsp;".$data["DetailsID"]."</td></tr>";

            if(strlen($data["ImgID"]) > 0) {
                    $message.= "<tr><td><b>".$assignDescription["ImgID"].":</b></td><td>&nbsp; http://www.bizzona.ru/simg/".$data["ImgID"]."</td></tr>";
            }
            if(strlen($data["Img1ID"]) > 0) {
                    $message.= "<tr><td><b>".$assignDescription["ImgID"].":</b></td><td>&nbsp; http://www.bizzona.ru/simg/".$data["Img1ID"]."</td></tr>";
            }
            if(strlen($data["Img2ID"]) > 0) {
                    $message.= "<tr><td><b>".$assignDescription["ImgID"].":</b></td><td>&nbsp; http://www.bizzona.ru/simg/".$data["Img2ID"]."</td></tr>";
            }

            $message.= "<tr><td><b>".$assignDescription["ShareSaleID"].":</b></td><td>&nbsp;".$data["ShareSaleID"]."</td></tr>";
            $message.= "<tr><td><b>".$assignDescription["TarifID"].":</b></td><td>&nbsp;".$data["TarifID"]."</td></tr>";
            $message.= "<tr><td><b>".$assignDescription["ReasonSellBizID"].":</b></td><td>&nbsp;".NameReasonSellBizByID($data["ReasonSellBizID"])."</td></tr>";
            $message.= "</table>";

        
            $mail = new PHPMailer(); 
            $mail->CharSet = "windows-1251";
            $mail->IsSendmail();
            $body  = $message;
            $mail->AddReplyTo($from, "��� ������ - ����");
            $mail->SetFrom($from, "��� ������ - ����");
            $mail->AddAddress($to);
            //if($data["helpbroker"] == 1) {
            //        $mail->AddAddress("broker3@business-trio.ru");
            //        TrioExport($data);
            //}
            $mail->Subject    = $subject;
            $mail->MsgHTML($message);        
            $mail->Send();
        

            $mysql_insert_id = $sell->Insert($data);
            $smarty->assign("code", $mysql_insert_id);
            $statusAdd = true;
            
             $_SESSION['timepost'] = time(); 
            

            unset($_SESSION['newname_ImgID']);
            unset($_SESSION['newname_Img1ID']);
            unset($_SESSION['newname_Img2ID']);
            unset($_SESSION['newname_Img3ID']);
            unset($_SESSION['newname_Img4ID']);
            
            $secret_code = "vbhjplfybt";
            $purse = "4146"; // sms:bank id        ������������� ���:�����
            $order_id = $mysql_insert_id; //  ������������� ��������
            $amount = "5.5"; // transaction sum  ����� ����������
            $amount_megafon = "4"; // transaction sum  ����� ����������
            $amount_ukraine = "2.4"; // transaction sum  ����� ����������
            $clear_amount  = "0"; // billing algorithm  �������� �������� ���������
            $description  = "������ ���������� ������� �������"; // operation desc  �������� ��������
            $submit  = "���������"; // submit label    ������� �� ������ submit
            $submit_megafon  = "��������� (��� ��������)"; // submit label    ������� �� ������ submit
            $submit_ukraine  = "���������"; // submit label    ������� �� ������ submit

            $sign = ref_sign($purse, $order_id, $amount, $clear_amount, $description, $secret_code);
            $sign_megafon = ref_sign($purse, $order_id, $amount_megafon, $clear_amount, $description, $secret_code);
            $sign_ukraine = ref_sign($purse, $order_id, $amount_ukraine, $clear_amount, $description, $secret_code);

            $smarty->assign("purse", $purse);
            $smarty->assign("order_id", $order_id);
            $smarty->assign("amount", $amount);
            $smarty->assign("amount_megafon", $amount_megafon);
            $smarty->assign("amount_ukraine", $amount_ukraine);
            $smarty->assign("clear_amount", $clear_amount);
            $smarty->assign("description", $description);
            $smarty->assign("submit", $submit);
            $smarty->assign("submit_megafon", $submit_megafon);
            $smarty->assign("submit_ukraine", $submit_ukraine);
            $smarty->assign("sign", $sign);
            $smarty->assign("sign_megafon", $sign_megafon);
            $smarty->assign("sign_ukraine", $sign_ukraine);

            $smarty->assign("email", $_POST["EmailID"]);
        
            $money_message = cp1251_to_utf8("������ ����� �� ����� bizzona.ru (".$order_id.") ");
            $money_message = base64_encode($money_message);
            $smarty->assign("money_message", $money_message);        
         
            //$templateEvent->EmailEvent(1,1, $mysql_insert_id);

            if(in_array((int)$_POST['periodId'], array(600, 1000, 1200))) {
                $smarty->assign("sumpayment", (int)$_POST['periodId']);
            } else {
                $smarty->assign("sumpayment", 600);
            }
              
            if(in_array(trim($_POST['periodId']), array('promo'))) {
                $smarty->assign("isPromo", 1);
            }
            
            unset($_POST);
      }
    }
  } else if (isset($_POST["preview"])) {
      $_SESSION['previewsell'] = $data;
  }

//end = get_formatted_microtime();

//$end = get_formatted_microtime();

  $data["offset"]   = 0;
  $data["rowCount"] = 0;
  $data["sort"]= "Count";
  $listCategory = $category->Select($data); 

  unset($data["sort"]);
  $data["offset"]   = 0;
  $data["rowCount"] = 0;
  //$listCity = $city->Select($data); 



  $smarty->assign("listCategory", $listCategory);
  //$smarty->assign("listCity", $listCity);
  
  $listSubCategory = $subCategory->Select($data);
  $smarty->assign("listSubCategory", $listSubCategory);
 
//$end = get_formatted_microtime(); 

    include_once("./engine/class.ContactInfo_Lite.inc");
    $contact = new ContactInfo_Lite();  

    $contact->GetContactInfo();  
    $smarty->assign("contact", $contact);
  
    $smarty->assign("promo", $promo);
  
    $smarty->assign("insertsettings", $insertSettingsData);      
?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
 
  <HEAD>
      <title>���������� ������� - ������� � ������� �������. ������� ������ - ������ ����</title>		
	<META NAME="Description" CONTENT="������� �������, ������� �������, ������� ������, ������� �������, ������� �������� �������, ������� �������, ������� ��������,  ������� ��������, ������� ��������, ���� ������� ������?">
        <meta name="Keywords" content="������� �������, ������� �������, ������� ������, ������� �������, ������� �������� �������, ������� �������, ������� ��������,  ������� ��������, ������� ��������, ���� ������� ������?, Ready business, �����������, �������� � ��������, ���������, ����������, �����������"> 
        <meta http-equiv="content-type" content="text/html; charset=windows-1251"/>
        <LINK href="./general.css" type="text/css" rel="stylesheet">
        <link rel="shortcut icon" href="./images/favicon.ico">
        <script src="http://www.bizzona.ru/js/jquery.min.js"></script>
        <script src="http://www.bizzona.ru/js/jquery.colorbox.js"></script> 
        <link media="screen" rel="stylesheet" href="http://www.bizzona.ru/css/colorbox.css" />            
        
        <script>
            <? if(isset($_POST['preview'])): ?>              
                (function($){ 
                    $(document).ready(function() {
                    $.colorbox({width:"90%", height:"90%", iframe:true, href:"http://<?=$_SERVER['SERVER_NAME'];?>/detailsSellPreview.php?ID=42007"});
                    });
                })(jQuery);        
        
            <? endif ?>        
        </script>        
        
  </HEAD>


<?
	$topmenu = GetMenu();
	$smarty->assign("topmenu", $topmenu);

	$link_country.= "&nbsp;&nbsp;<a href='".NAMESERVER."help.php' class='city_a'  style='color:white;font-size:10pt;color:yellow;' title='������ - ������� �������' target='_blank'>������</a>&nbsp;&nbsp;";	
	$smarty->assign("ad", $link_country);
         //$end = get_formatted_microtime();
	$smarty->display("./site/headerinsertsell.tpl");
?>
  
<table class="w" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td width="100%">
         <table width='100%' height='100%'>
           <?
              if (strlen($error) > 0) { 
           ?> 
           <tr  valign='top' width='100%' height='100%'>
             <td align='center'><span style="color:red;"><b><?=$error;?></b></span></td> 
           </tr>
           <?
              }
           ?> 
           <tr>
             <td valign='top' width='60%' height='100%'>
               <?
                  //$end = get_formatted_microtime();
               
                  if($blocking) {
                      $smarty->display("./site/blockip.tpl");
                  } else {
                    $smarty->display("./site/insertsell.tpl");
                  
                  }
                  //$end = get_formatted_microtime();
               ?>
             </td>
           </tr>
         </table>
        
        
        </td> 
    </tr>
</table>

<?php
//$end = get_formatted_microtime();
?>

<style>
 td a 
 {
   color:black;
 }
</style>


<div style="padding-top:4pt;padding-bottom:5pt;padding-right:10pt;padding-left:10pt;font-size:10pt;font-family:arial;">
	<table cellpadding="0" cellspacing="0" border="0"  style="width:100%;padding-bottom:2pt;" bgcolor="#eeeee0">
    	<tr>
        	<td style="background-image:url(<?=$_SERVER["host_name"]?>images/d1.gif); background-repeat:no-repeat;width:5px;height:5px; background-position:left top;">&nbsp;</td>
            <td rowspan="2" valign="middle" align="center"  style="font-size:8pt; font-family:Arial; color:black;">
			<?
			?>
            </td>
            <td style="background-image:url(<?=$_SERVER["host_name"]?>images/d2.gif); background-repeat:no-repeat;width:5px;height:5px;background-position:right top;">&nbsp;</td>
        </tr>
        <tr>
            <td style="background-image:url(<?=$_SERVER["host_name"]?>images/d4.gif); background-repeat:no-repeat;width:5px;height:5px; background-position:left bottom;">&nbsp;</td>
            <td style="background-image:url(<?=$_SERVER["host_name"]?>images/d3.gif); background-repeat:no-repeat;width:5px;height:5px; background-position:right bottom;">&nbsp;</td>
        </tr>
    </table>
</div>



<?
	$smarty->display("./site/footer.tpl");
?>

<?php
$category->Close();
 $subCategory->Close();
$sell->Close();
?>


<?
  $end = get_formatted_microtime();
  $total = $end - $start;
  echo "<center><span style='font-size:7pt;'>".round($total, 6)."</span><center>";
?>


</body>
</html>
