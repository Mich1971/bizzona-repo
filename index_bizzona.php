<?
	global $DATABASE;
  	include_once("./phpset.inc");
  	include_once("./engine/functions.inc");

  	$count_sql_call = 0;
	$start = get_formatted_microtime();   
  	$base_memory_usage = memory_get_usage();	
  	
  	include_once("./seterror.php");
  
  	AssignDataBaseSetting();
  	
	include_once("./engine/class.category_lite.inc"); 
	$category = new Category_Lite();

	include_once("./engine/class.country_lite.inc"); 
	$country = new Country_Lite();

	include_once("./engine/class.city_lite.inc"); 
	$city = new City_Lite();

	include_once("./engine/class.sell_lite.inc"); 
	$sell = new Sell_Lite();
  
	include_once("./engine/class.buy_lite.inc"); 
	$buyer = new Buyer_Lite();
  
  	include_once("./engine/class.equipment_lite.inc"); 
  	$equipment = new Equipment_Lite();

  	include_once("./engine/class.metro_lite.inc"); 
  	$metro = new Metro_Lite();

  	include_once("./engine/class.district_lite.inc"); 
  	$district = new District_Lite();

  	include_once("./engine/class.subcategory_lite.inc");
  	$subcategory = new SubCategory_Lite();
  
  	include_once("./engine/class.streets_lite.inc");
  	$streets = new Streets_Lite();

  
  	require_once("./libs/Smarty.class.php");
  	$smarty = new Smarty;
	$smarty->template_dir = "./templates";
  	$smarty->compile_dir  = "./templates_c";
  	$smarty->cache_dir = "./cache";
  	$smarty->compile_check = true;
  	$smarty->debugging     = false;
  	//$smarty->caching = true;
  	//$smarty->clear_all_cache();

  	require_once("./regfuncsmarty.php");

  	$assignDescription = AssignDescription();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<HEAD>
<title>������� � ������� �������. ������� �������� ������� - ��� "������ ����"</title>		
<META NAME="Description" CONTENT="������� �������, ������� �������, ������� ������, ������� �������, ������� �������� �������, ������� �������, ������� ��������,  ������� ��������, ������� ��������, ������� ��������">
<meta name="Keywords" content="������� �������, ������� �������, ������� ������, ������� �������, ������� �������� �������, ������� �������, ������� ��������,  ������� ��������, ������� ��������, ������� ��������, ������� �����, ������ �����, �������� � ��������, ���������, ����������, �����������, ������ ������"> 
<link rel="shortcut icon" href="./images/favicon.ico">
<meta http-equiv="content-type" content="text/html; charset=windows-1251"/>
<LINK href="./general.css" type="text/css" rel="stylesheet">
</HEAD>
<body>
<?
  $smarty->display("./site/headerbanner.tpl");
?>
<table class="w" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="2">
		<?
			$topmenu = GetMenu();
			$smarty->assign("topmenu", $topmenu);
	
			$link_country = GetSubMenu();
			$smarty->assign("ad", $link_country);
			
			echo minimizer($smarty->fetch("./site/header.tpl"));		
		?>
		</td>
	</tr>
	<tr>
        <td width="75%" valign='top'>
            <div class="bwell present">����� ���������� �� �������� ������ <strong>������� � ������� �������� �������, �����, ��������</strong> �������� <strong>��� "������ - ����"</strong>. ����� �� ������ ���������� <strong>���������� � ������� �������� �����������</strong> ��� <strong>�����</strong>. ���� ������� ������� - ���������� ���������� � ����� �������������� ���������� <strong>�������� �������</strong> � ���������. <b>��� �������� �������</b>. ������ �������� ��������������� ����� <b>30 000</b> ��������.  <strong>������ ���� � ��� ������ ���������� �������� 2 - 3 �������</strong>. ���������� �������� ������������ ������� ����� <b>7 500 �����������</b>.  ��������� ���������� ���������� �������� <b>����� 100 ����� � 200-300 ������ �������</b>  � ������������� �� ������� �������. �� ������������� ������� ������ ��, ��� ������� ��!</div>
        	<?
				echo fminimizer($smarty->fetch("./site/price.tpl"));
					
                $smarty->caching = true; 
                $smarty->cache_dir = "./persistentcache";
                $smarty->cache_lifetime = 86400;
                $city_cach = ((isset($_GET["action"]) && $_GET["action"] == "city") ? intval($_GET["ID"]) : 0 );
                if (!$smarty->is_cached('./site/maincity.tpl', $city_cach)) {
                	$data = array();
                	$data["offset"] = 0;
                	$data["rowCount"] = 0;
                	$data["sort"] = "Count";
                	$result = $city->SelectActive($data);
                	$smarty->assign("data", $result);
                } 
                  			
                echo fminimizer($smarty->fetch("./site/maincity.tpl", $city_cach));
                  			
                $smarty->caching = true; 
                
				if (!$smarty->is_cached('./site/epoplistbiz.tpl')) {
					$counttopcategory = $sell->ListCountTopCategory();
					$smarty->assign("counttopcategory", $counttopcategory);
				}
				
				echo fminimizer($smarty->fetch("./site/epoplistbiz.tpl"));

				$smarty->cache_dir = "./cache";
				$smarty->caching = false; 
            ?>
			<div class="blk_lft" style="width: 95%;height:21pt;background-color:#7C3535;"><table width="100%" border="0" cellpadding="0" cellspacing="0" height="100%"><tr><td width="80%" valign="middle" style="font-size:12pt;font-family:Arial;color:White;font-weight:bold; vertical-align:middle;">��������� ����������� �� ������� �������</td><td width="20%" valign="middle" align="right" style="padding-right:24pt;" nowrap><a href="<?=$_SERVER["host_name"]?>searchsell/" style="font-size:9pt; font-family:Arial; color:White;" title="��� ����������� ������� �������">��� �����������</a></td></tr></table></div>
			<?
				echo $smarty->fetch("./site/saveme.tpl");
				
				$smarty->caching = true; 
				$smarty->cache_lifetime = 3600;
				
				if(isset($_SESSION["uCityID"]) || isset($_SESSION["uCategoryID"])) {
					$smarty->caching = false; 	
				}

				if (!$smarty->is_cached('./site/listsell.tpl')) {
					$data = array();
					$data["StatusID"] = "-1";
					$data["sort"] = "pay_datetime";
					$data["offset"] = 0;
					$data["rowCount"] = 10;
					
					$result_position = array();
					$result = $sell->SelectByPosition();
										
					while (list($k, $v) = each($result)) {
						if(intval($v->position)> 0) {
							if($v->position == 1)
							{
								$result_position[0] = $v;
							} else {
								$result_position[($v->position-1)] = $v; 
							}
						}
					}
					
					reset($result);
					
					while (list($k, $v) = each($result)) {
						if(intval($v->position)<= 0) {
						
							for($i = 0; $i <=9; $i++) {
								if(!isset($result_position[$i])) {
									$result_position[$i] = $v;
									break;
								}
							}
						}
					}
					
					
					ksort($result_position);
					
					$result = $result_position;
					
					$smarty->assign("data", $result);
				} 
				
				echo fminimizer($smarty->fetch("./site/listsell.tpl"));
				?>
				<table border="0" cellpadding="1" cellspacing="1" width="98%">
					<tr>
						<td align="left">
							<a href="http://www.yandex.ru?add=41134&amp;from=promocode" target="_blank"><img src="http://img.yandex.net/i/service/wdgt/yand-add-b.png" border="0" alt="�������� �� ������ ��������� ����������� �������"/></a>
						</td>
						<td align="right">
							<script type="text/javascript" src="//yandex.st/share/share.js" charset="utf-8"></script>
							<div align="right" class="yashare-auto-init" data-yashareType="button" data-yashareQuickServices="yaru,vkontakte,facebook,twitter,odnoklassniki,moimir,lj,friendfeed,moikrug"></div>						
						</td>
					</tr>
				</table>
				<?				
				  
			    $smarty->caching = true; 
			    $smarty->cache_dir = "./persistentcache";
			    $smarty->cache_lifetime = 86400;
            	if (!$smarty->is_cached("./site/mainkeysell.tpl")) {
					$datakeysell = $category->GetListSubCategoryActive();
					$smarty->assign("datakeysell", $datakeysell);
            	}
				
                echo minimizer($smarty->fetch("./site/mainkeysell.tpl"));
                
                $smarty->cache_dir = "./cache";
                $smarty->caching = false; 

				echo fminimizer($smarty->fetch("./site/price.tpl"));
			?>

			<?
    			echo minimizer($smarty->fetch("./site/topicblog.tpl"));   	 
			?>
			
		</td>
		<td width="25%" valign="top" style="padding-top:2pt;" align="left">
			<div class='w' style='padding:2pt;width:auto;margin:0pt;background-color:#eeeee0' align="center">
				<a href='http://www.bizzona.ru/pochemu-u-nas-prodaetsya-biznes.php' style='font-size:10pt;font-family:Arial;color:black;font-weight:bold;' title="������ � ��� ������ ��������?">������ � ��� ������ ��������?</a>
			</div>
			<?
			
  				$smarty->assign("banner3_file", GetBanner3());
  				
				echo fminimizer($smarty->fetch("./site/banner3.tpl"));
				echo fminimizer($smarty->fetch("./site/request.tpl"));
				
				$smarty->caching = true; 
				$smarty->cache_lifetime = 86400;
				$smarty->cache_dir = "./persistentcache";
				if (!$smarty->is_cached('./site/hotsell.tpl')) {
					$data = array();
					$data["offset"] = 0;
					$data["rowCount"] = 0;
					$data["sort"] = "DateStart";
					$data["StatusID"] = "2";              
					$result = $sell->Select($data);
					$smarty->assign("data", $result);

					if (isset($_SESSION["listID"])) { 
						$dataPage["rowCount"] = 0;
						$dataPage["offset"] = 0;
						unset($_SESSION["listID"]);
					}
					while (list($ld, $lv) = each($result)) {
						$_SESSION["listID"][] = $lv->ID; 
					}
				}

				echo fminimizer($smarty->fetch("./site/hotsell.tpl"));

				$smarty->caching = true; 
				
				$smarty->cache_lifetime = 3600;
				
				if (!$smarty->is_cached('./site/blockbuyer.tpl')) {
            
					$datahotbuyer = array();
					$datahotbuyer["offset"] = 0;
					$datahotbuyer["rowCount"] = 3;
					$datahotbuyer["sort"] = "datecreate";
					$datahotbuyer["StatusID"] = "-1";
            	
					$res_datahotbuyer = $buyer->Select($datahotbuyer);
					$smarty->assign("databuyer", $res_datahotbuyer);        	
            	
				}
              
				echo fminimizer($smarty->fetch("./site/blockbuyer.tpl"));
              
				$smarty->caching = false; 
				
				$smarty->caching = true; 
				$smarty->cache_lifetime = 3600;
				
				if (!$smarty->is_cached('./site/hotequipment.tpl')) {
            
					$datahotequipment = array();
					$datahotequipment["offset"] = 0;
					$datahotequipment["rowCount"] = 3;
					$datahotequipment["sort"] = "datecreate";
					$datahotequipment["status_id"] = "-1";
            	
					$res_datahotequipment = $equipment->Select($datahotequipment);
  	          	
					$smarty->assign("dataequipment", $res_datahotequipment);        	
				}
              
				
				echo fminimizer($smarty->fetch("./site/hotequipment.tpl"));
				$smarty->caching = false; 
				
				echo fminimizer($smarty->fetch("./site/proposal.tpl"));
				
				$smarty->cache_dir = "./cache";
			?>
        </td>
    </tr>
    
    <tr>
    	<td colspan="2">
		<?
			echo fminimizer($smarty->fetch("./site/adv.tpl"));
			echo fminimizer($smarty->fetch("./site/sell-howmework.tpl"));
    		echo fminimizer($smarty->fetch("./site/articles.tpl"));
		?>
<div class="adv">
	������� �����, ������� ��������, ������� ������������  
������� ����� ���, ����� � �������. �������� �������� �� �������
������� ��� "������-����" ���������� ����� ����������� ������� ��� 
������� ������� �������. ������ ��������� �������, ������ ���������� �����������, 
������ ��������, ���������� � ������ ����������� ���������, �������� 
���������������� ����������� ���������� � ����� ������� � ���������� � �������� �������� ������ ������� 
 ������ ������� ������� � �����������. 
</div>
<style> td a { color:black; }</style>
<div style="padding-top:4pt;padding-bottom:5pt;padding-right:10pt;padding-left:10pt;font-size:10pt;font-family:arial;">
<table cellpadding="0" cellspacing="0" border="0"  style="width:100%;padding-bottom:2pt;" bgcolor="#eeeee0">
<tr>
<td class="d1_gif" style="background-image:url(<?=$_SERVER["host_name"]?>images/d1.gif);">&nbsp;</td>
<td rowspan="2" valign="middle" align="center"  style="font-size:8pt; font-family:Arial; color:black;">
	������� �������, �����, ��������, ������������  
������� ����� ���, ����� � �������. ��� "������-����" ���������� ����� ����������� ������� ��� 
������� ������� �������.
</td>
<td class="d2_gif" style="background-image:url(<?=$_SERVER["host_name"]?>images/d2.gif);">&nbsp;</td>
</tr>
<tr>
<td class="d4_gif" style="background-image:url(<?=$_SERVER["host_name"]?>images/d4.gif);">&nbsp;</td>
<td class="d3_gif" style="background-image:url(<?=$_SERVER["host_name"]?>images/d3.gif);">&nbsp;</td>
</tr>
</table>
</div>
<?
	echo fminimizer($smarty->fetch("./site/site_city.tpl"));
	$smarty->display("./site/footer.tpl");
?>
    	</td>
    </tr>
</table>
<?
  $end = get_formatted_microtime(); 
  $total = $end - $start;
  echo "<center><span style='font-size:7pt;'>".round($total, 6)." : ".$count_sql_call." : ".$base_memory_usage." : ".memoryUsage(memory_get_usage(), $base_memory_usage)." </span><center>";
?>
<table cellpadding="0" cellspacing="0" border="0" width="100%">
<tr>
	<td align="center" bgcolor="#c5c5a8" style="padding-top:2pt;padding-bottom:2pt;">
	</td> 
</tr> 
</table>
<script type="text/javascript" src="ja.js"></script>
</body>
</html>
<?
	$sell->Close();
	$category->Close();
	$city->Close();
	$country->Close();
	$buyer->Close();
	$equipment->Close();
	$metro->Close();
	$district->Close(); 
	$subcategory->Close();
	$streets->Close();
?>