 <?
  global $DATABASE;
  
  include_once("./phpset.inc");
  include_once("./engine/functions.inc"); 
  
  $count_sql_call = 0;
  $start = get_formatted_microtime(); 
  $base_memory_usage = memory_get_usage();	
  
  
  AssignDataBaseSetting();
  
  include_once("./engine/class.category_lite.inc"); 
  $category = new Category_Lite();

  include_once("./engine/class.country_lite.inc"); 
  $country = new Country_Lite();	

  include_once("./engine/class.city_lite.inc"); 
  $city = new City_Lite();

  include_once("./engine/class.sell_lite.inc"); 
  $sell = new Sell_Lite();	

  include_once("./engine/class.district_lite.inc"); 
  $district = new District_Lite();	

  include_once("./engine/class.subcategory_lite.inc");
  $subcategory = new SubCategory_Lite();	

  include_once("./engine/class.review.inc");
  $review = new Review();
  
  include_once("./engine/class.streets_lite.inc");
  $streets = new Streets_Lite();

 include_once("./engine/class.frontend.inc");

  require_once("./libs/Smarty.class.php");
  $smarty = new Smarty;
  $smarty->template_dir = "./templates";
  $smarty->compile_dir  = "./templates_c";
  //$smarty->cache_dir = "./cache";
  $smarty->compile_check = true;
  $smarty->debugging     = false;

  require_once("./regfuncsmarty.php");

  $assignDescription = AssignDescription();

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>

  <HEAD>
      <title>������. ������� � ������� ������� - ��� "������-����"</title>		
	    <META NAME="Description" CONTENT="������� �������, ������� �������, ������� ������, ������� �������, ������� �������� �������, ������� �������, ������� ��������,  ������� ��������, ������� ��������,���� ������� ������?">
        <meta name="Keywords" content="������� �������, ������� �������, ������� ������, ������� �������, ������� �������� �������, ������� �������, ������� ��������,  ������� ��������, ������� ��������, ���� ������� ������?, Ready business, �����������, �������� � ��������, ���������, ����������, �����������"> 
        
		<script src="http://www.bizzona.ru/js/jquery.min.js"></script>
		<script src="http://www.bizzona.ru/js/jquery.colorbox.js"></script> 
		<link media="screen" rel="stylesheet" href="http://www.bizzona.ru/css/colorbox.css" />        
        
        <LINK href="./general.css" type="text/css" rel="stylesheet">
        <meta http-equiv="content-type" content="text/html; charset=windows-1251"/>
		<script type="text/javascript" src="ja.js"></script>        
		
		<script>
		$(document).ready(function(){
			$("a[rel='compliment']").colorbox({width:"60%", height:"80%", iframe:true});
			$("a[rel='scold']").colorbox({width:"60%", height:"80%", iframe:true});
			$("a[rel='advise']").colorbox({width:"60%", height:"80%", iframe:true});
			$("#click").click(function(){ 
				$('#click').css({"background-color":"#f00", "color":"#fff", "cursor":"inherit"}).text("");
				return false;
			});
		});
		</script>		
		
		
  </HEAD>

<?
  $smarty->display("./site/headerbanner.tpl");
?>  
  
<table class="w" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="2">
<?
	$smarty->caching = false; 
	$topmenu = GetMenu();
	$smarty->assign("topmenu", $topmenu);
	
	$link_country = GetSubMenu();
	$smarty->assign("ad", $link_country);

	$smarty->display("./site/header.tpl");
	$smarty->caching = true;
?>		
		</td>
	</tr>
    <tr>
        <td width="75%" valign="top" style='padding:10pt;'>
        
<p style="color:#7c3535;font-weight:bold;padding:0pt;margin:0pt;font-family:Arial;">���� ������</p>

<table width="100%">
	<tr>
		<td align="right" style="padding-bottom:5pt;"><a href='http://www.bizzona.ru/poleznie-sovety-po-prodazhe-biznesa.php' class="city_a" style='font-size:10pt;'>�������� ������ ������������� ��������� ������ ����� ��� ������</a></td>
	</tr>
</table>

<?
        $list = $review->Select(array(
            'statusId' => array(1)
        ));
        
        //$smarty->assign('list', $list);
	$smarty->display("./site/review.tpl");
        
        ?><br><?
        
        foreach ($list as $k => $v) {
            
            if($k % 2) {
                
                ?>
                    <table class="w"  cellpadding="0" cellspacing="0" border="0">
                       <tr>
                          <td class="res_h1_gif" style="background-image:url(http://www.bizzona.ru/images/res_tl.gif);"></td>
                          <td bgcolor="#eeeee0"></td>
                          <td class="res_h2_gif" style="background-image:url(http://www.bizzona.ru/images/res_tr.gif);"></td>
                       </tr>
                       <tr>
                            <td bgcolor="#eeeee0"></td>
                            <td bgcolor="#eeeee0">
                                    <div style="padding:5pt;color:#000;font-size:11pt;font-family:Arial;"><?=$v['comment'];?></div>
                                    <div style="padding:5pt;color:#006699;font-size:9pt;font-family:Arial;font-weight:bold;" align="left"><i>�����, <?=$v['fio'];?>,  (<?=$v['name'];?>)</i></div>
                            </td>
                            <td bgcolor="#eeeee0"></td>
                       </tr>
                       <tr>
                          <td class="res_h4_gif" style="background-image:url(http://www.bizzona.ru/images/res_bl.gif);"></td>
                          <td bgcolor="#eeeee0"></td>
                          <td class="res_h3_gif" style="background-image:url(http://www.bizzona.ru/images/res_br.gif);"></td>
                       </tr>
                    </table>

                    <div style="padding-top:4pt;"></div>        

                <?
            } else {
                
                ?>
            
                    <table class="w"  cellpadding="0" cellspacing="0" border="0">
                       <tr>
                          <td class="res_h1_gif" style="background-image:url(http://www.bizzona.ru/images/res_w_tl.gif);"></td>
                          <td bgcolor="#e4e4e2"></td>
                          <td class="res_h2_gif" style="background-image:url(http://www.bizzona.ru/images/res_w_tr.gif);"></td>
                       </tr>
                       <tr>
                            <td bgcolor="#e4e4e2"></td>
                            <td bgcolor="#e4e4e2">
                                    <div style="padding:5pt;color:#000;font-size:11pt;font-family:Arial;"><?=$v['comment'];?></div>
                                    <div style="padding:5pt;color:#006699;font-size:9pt;font-family:Arial;font-weight:bold;" align="left"><i>�����, <?=$v['fio'];?>,  (<?=$v['name'];?>)</i></div>
                            </td>
                            <td bgcolor="#e4e4e2"></td>
                       </tr>
                       <tr>
                          <td class="res_h4_gif" style="background-image:url(http://www.bizzona.ru/images/res_w_bl.gif);"></td>
                          <td bgcolor="#e4e4e2"></td>
                          <td class="res_h3_gif" style="background-image:url(http://www.bizzona.ru/images/res_w_br.gif);"></td>
                       </tr>
                    </table>

                    <div style="padding-top:4pt;"></div>                    
                    
                <?    
            }
            
        }
        
	$smarty->display("./site/footer.tpl");
        
	$end = get_formatted_microtime(); 
	$total = $end - $start;
	echo "<center><span style='font-size:7pt;'>".round($total, 6)." : ".$count_sql_call." : ".$base_memory_usage." : ".memoryUsage(memory_get_usage(), $base_memory_usage)." </span><center>";
?>
</body>
</html>