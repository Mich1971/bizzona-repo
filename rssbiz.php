<?php
	header('Content-type: application/xml');
	
	global $DATABASE;

	include_once("./phpset.inc");
	include_once("./engine/functions.inc");
	
	AssignDataBaseSetting();
	
	include_once("./engine/class.rss.inc");
	$rss = new RSS();
	
	if (isset($_GET["parentRegionId"]) && intval($_GET["parentRegionId"]) > 0) {
		$rss->parentRegionId = intval($_GET["parentRegionId"]);
	}
	
	if (isset($_GET["categoryId"]) && intval($_GET["categoryId"]) > 0) {
		$rss->categoryId = intval($_GET["categoryId"]);
	}
	
	if (isset($_GET["subCategoryId"]) && intval($_GET["subCategoryId"]) > 0) {
		$rss->subCategoryId = intval($_GET["subCategoryId"]);
	}

	$rss->ListRSSLenta();
?>  