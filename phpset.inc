<?
  //error_reporting(E_ALL);
  ini_set("display_startup_errors", "off");
  ini_set("display_errors", "off");
  ini_set("register_globals", "off");

  //setlocale(LC_ALL, 'ru_RU.koi8r');

  setlocale(LC_ALL, 'ru_RU.CP1251');
  setlocale(LC_TIME, "ru_RU.CP1251"); 

  /* 
  if(file_exists('./engine/SessionHandler.php')) {
    require_once('./engine/SessionHandler.php');
  } else if (file_exists('./SessionHandler.php')) {
    require_once('./SessionHandler.php');  
  }
  */  

  if(file_exists($_SERVER['DOCUMENT_ROOT'].'/engine/SessionHandler.php')) {
     require_once($_SERVER['DOCUMENT_ROOT'].'/engine/SessionHandler.php');
  } else {
     require_once('/var/www/mastermind/data/www/bizzona.ru/engine/SessionHandler.php');
  }  
 
  session_cache_expire(43200);

  if (isset($_COOKIE['BizZONA']) ) {
    session_id($_COOKIE['BizZONA']);
    session_start();    
  } else {
    session_start();
    $cExp = time()+(3600*24*30*12);
    $cPath = '/';
    $cDom = (isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : '');
    setcookie("BizZONA", session_id(), $cExp, $cPath, $cDom);
  }

  //$_SERVER["_HTTP_HOST"] = "http://".$_SERVER["HTTP_HOST"]."/"; 
  define("NAMESERVER", "http://".(isset($_SERVER["HTTP_HOST"]) ? $_SERVER["HTTP_HOST"] : '')."/"); 

  $_SERVER["host_name"] = NAMESERVER;	
  
  if(isset($_GET["partnerid"])) {
  	$_SESSION["partnerid"] = intval($_GET["partnerid"]);
  }
  
  if(!isset($_SESSION["referer"])) {
  	$_SESSION["referer"] = (isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '');
  }
  
?>
