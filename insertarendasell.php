<?
  global $DATABASE;

  include_once("./phpset.inc");
  include_once("./engine/functions.inc");

  ini_set("display_startup_errors", "off");
  ini_set("display_errors", "off");
  ini_set("register_globals", "on");

  $start = get_formatted_microtime();
 
  AssignDataBaseSetting();
  include_once("./engine/class.category.inc"); 
  $category = new Category();

  include_once("./engine/class.subcategory.inc"); 
  $subCategory = new SubCategory();
  
  //include_once("./engine/class.country.inc"); 
  //$country = new Country();

  //include_once("./engine/class.city.inc"); 
  //$city = new City();

  include_once("./engine/class.sell.inc"); 
  $sell = new Sell();

  include_once("./engine/class.operator.inc"); 
  $operator = new Operator();
  $operator->typeQueryId = 1;
  $operator->answer = "��� ���� ��������� ������ ������������ �� ����� � �������  ������ ����.";
  $operator->query = "����� ����� ���������� ������� ��������� ����� ������������ �� �����?";

    include_once("./engine/class.BlockIP.inc"); 
    $blockIp = new BlockIP();
    
    
    $blocking  = $blockIp->YouAreBlocking(array(
        'ip' => $_SERVER['REMOTE_ADDR']
    ));  
  
    include_once("./engine/class.insert_settings.inc"); 
    $insertSettings = new InsertSettings();  
    
    $insertSettings->type_insert = ConstInsertSettings::ARENDA_SELL;
    $insertSettingsData = $insertSettings->GetItem();    
    
  require_once("./libs/Smarty.class.php");
  $smarty = new Smarty;
  $smarty->template_dir = "./templates";
  $smarty->compile_dir  = "./templates_c";
  //$smarty->cache_dir = "./cache";
  $smarty->compile_check = true;
  $smarty->debugging     = false;
  $smarty->clear_compiled_tpl("./site/insertsell.tpl");

  $smarty->register_function('NameCity', 'getNameCity');

  $assignDescription = AssignDescription();


  $error = "";

function LoadClientImg($fileload, $count) {
  
    $max_image_width	= 380;
    $max_image_height	= 600;
    $max_image_size	= 1024 * 1024;
    $valid_types 	=  array("gif","GIF","jpg", "png", "jpeg","JPG");
  		
	if (isset($_FILES[$fileload])) {
		//$newname = time() + $count; 
		$newname = uniqid("sell_");
   
		if (is_uploaded_file($_FILES[$fileload]['tmp_name'])) {
        	$filename = $_FILES[$fileload]['tmp_name'];
        	$ext = substr($_FILES[$fileload]['name'], 1 + strrpos($_FILES[$fileload]['name'], "."));
        	
        	if (filesize($filename) > $max_image_size) {
          		$error = '������: ������ ����� ������ 1 ��.';
          		$newname = ""; 
        	} elseif (!in_array($ext, $valid_types)) {
          		$error = '������: ���������� ����� ����������� ���������� ����� �� ����������� ���';
          		$newname = ""; 
        	} else {
          		if (1) {
            		if (move_uploaded_file($filename, "./simg/".$newname.".".$ext)) {
	    			} else {
	      				$error =  '������ : ���������� ��������� ���������� ���������� �� ������� � webmaster@bizzona.ru.';
          				$newname = ""; 
	    			}
          		} else {
	    			$error = '������: ���������� ��������� ������ �� �����. ���������� ������� ������ ����.';
            		$newname = "";
          		}
        	}
       } else {
         $newname = "";
       }
    }  	
  	
    return (strlen($newname) > 0 ? $newname.".".$ext : "");
  }  
 

  if (isset($_POST["insert"]) && !$blocking) {

    $ErrorFile = false;

    $newname_ImgID = LoadClientImg("ImgID", 1);
    $newname_Img1ID = LoadClientImg("Img1ID", 2);
    $newname_Img2ID = LoadClientImg("Img2ID", 3);


    if (!$ErrorFile) {
    	
    	
      $costsymbols = array(".", " ", ",");
 	

      $data["FML"]              = htmlspecialchars(trim($_POST["FML"]), ENT_QUOTES);
      $_POST["FML"] = $data["FML"];
      
      $data["EmailID"]          = htmlspecialchars(trim(strtolower($_POST["EmailID"])), ENT_QUOTES);
      $_POST["EmailID"] = $data["EmailID"];
      
      $data["PhoneID"]          = htmlspecialchars(trim($_POST["PhoneID"]), ENT_QUOTES);
      $_POST["PhoneID"] = $data["PhoneID"];
      
      $data["NameBizID"]        = htmlspecialchars(trim($_POST["NameBizID"]), ENT_QUOTES);
      $_POST["NameBizID"] = $data["NameBizID"];
      $data["NameBizID"]        = FixName(ucwords(strtolower($data["NameBizID"])));
      
  	  $total_typeBizID = htmlspecialchars(trim($_POST["CategoryID"]), ENT_QUOTES);
  	  $arr_typeBizID = split("[:]",$total_typeBizID);
  	  if(sizeof($arr_typeBizID) == 2) {
  		$data["CategoryID"] = intval($arr_typeBizID[0]);
  		$data["SubCategoryID"] = intval($arr_typeBizID[1]);
  		$_POST["SubCategoryID"] = $data["SubCategoryID"];
  	  } else {
  	  	$data["CategoryID"] = "-1";
  		$data["SubCategoryID"] = "-1";
  	  }
      
      $data["BizFormID"]        = htmlspecialchars(trim($_POST["BizFormID"]), ENT_QUOTES);
      $_POST["BizFormID"] = $data["BizFormID"];
      $data["CityID"]           = "-1";//htmlspecialchars(trim($_POST["CityID"]), ENT_QUOTES);
      
      $data["SiteID"]           = htmlspecialchars(trim($_POST["SiteID"]), ENT_QUOTES);
      $_POST["SiteID"] = $data["SiteID"];
      
      
      //$data["CostID"]           = htmlspecialchars(trim($_POST["CostID"]), ENT_QUOTES);
	  $data["txtCostID"]           = htmlspecialchars(trim($_POST["CostID"]), ENT_QUOTES);
	  $_POST["CostID"] = $data["txtCostID"];
	  
	  $data["CostID"]           = str_replace($costsymbols, "", htmlspecialchars(trim($_POST["CostID"]), ENT_QUOTES));
	  $_POST["CostID"] = $data["CostID"];
	  
      $data["cCostID"]          = htmlspecialchars(trim($_POST["cCostID"]), ENT_QUOTES);
      $_POST["cCostID"] = $data["cCostID"];
      
      //$data["ProfitPerMonthID"] = htmlspecialchars(trim($_POST["ProfitPerMonthID"]), ENT_QUOTES);
      $data["txtProfitPerMonthID"] = htmlspecialchars(trim($_POST["ProfitPerMonthID"]), ENT_QUOTES);
      $_POST["ProfitPerMonthID"] = $data["txtProfitPerMonthID"];
	  $data["ProfitPerMonthID"] = str_replace($costsymbols, "", htmlspecialchars(trim($_POST["ProfitPerMonthID"]), ENT_QUOTES));      
	  $_POST["ProfitPerMonthID"] = $data["ProfitPerMonthID"];
      $data["cProfitPerMonthID"] = htmlspecialchars(trim($_POST["cProfitPerMonthID"]), ENT_QUOTES);
      
      $data["TimeInvestID"]     = 0;
      $data["ListObjectID"]     = htmlspecialchars(trim($_POST["ListObjectID"]), ENT_QUOTES);
      $_POST["ListObjectID"] = $data["ListObjectID"];
      
      $data["ContactID"]        = htmlspecialchars(trim($_POST["ContactID"]), ENT_QUOTES);
      $_POST["ContactID"] = $data["ContactID"];
      
      $data["MatPropertyID"]    = htmlspecialchars(trim($_POST["MatPropertyID"]), ENT_QUOTES);
      $_POST["MatPropertyID"] = $data["MatPropertyID"];
      
      //$data["MonthAveTurnID"]   = htmlspecialchars(trim($_POST["MonthAveTurnID"]), ENT_QUOTES);
      $data["txtMonthAveTurnID"]   = htmlspecialchars(trim($_POST["MonthAveTurnID"]), ENT_QUOTES);
      $_POST["MonthAveTurnID"] = $data["txtMonthAveTurnID"];
      
      $data["MonthAveTurnID"]   = str_replace($costsymbols, "", htmlspecialchars(trim($_POST["MonthAveTurnID"]), ENT_QUOTES));
      $data["cMonthAveTurnID"]  = htmlspecialchars(trim($_POST["cMonthAveTurnID"]), ENT_QUOTES);
      
      //$data["MonthExpemseID"]   = htmlspecialchars(trim($_POST["MonthExpemseID"]), ENT_QUOTES);
      $data["txtMonthExpemseID"]   = htmlspecialchars(trim($_POST["MonthExpemseID"]), ENT_QUOTES);
      $_POST["MonthExpemseID"] = $data["txtMonthExpemseID"];
      
      $data["MonthExpemseID"]   = str_replace($costsymbols, "", htmlspecialchars(trim($_POST["MonthExpemseID"]), ENT_QUOTES));
      $_POST["MonthExpemseID"] = $data["MonthExpemseID"];
      $data["cMonthExpemseID"]  = htmlspecialchars(trim($_POST["cMonthExpemseID"]), ENT_QUOTES);
      $_POST["cMonthExpemseID"] = $data["cMonthExpemseID"];
      
      
      $data["txtSumDebtsID"] = 0;
      $data["SumDebtsID"] = 0;
      $data["cSumDebtsID"]      = "usd";
      
      //$data["WageFundID"]       = htmlspecialchars(trim($_POST["WageFundID"]), ENT_QUOTES);
      $data["txtWageFundID"]       = htmlspecialchars(trim($_POST["WageFundID"]), ENT_QUOTES);
      $_POST["WageFundID"] = $data["txtWageFundID"];
      
      $data["WageFundID"]       = str_replace($costsymbols, "", htmlspecialchars(trim($_POST["WageFundID"]), ENT_QUOTES));
      $data["cWageFundID"]      = htmlspecialchars(trim($_POST["cWageFundID"]), ENT_QUOTES);
      
      $data["ObligationID"]     = "";
      
      $data["CountEmplID"]      = htmlspecialchars(trim($_POST["CountEmplID"]), ENT_QUOTES);
      $_POST["CountEmplID"] = $data["CountEmplID"];
      
      $data["CountManEmplID"]   = htmlspecialchars(trim($_POST["CountManEmplID"]), ENT_QUOTES);
      $_POST["CountManEmplID"] = $data["CountManEmplID"];
      
      $data["TermBizID"]        = htmlspecialchars(trim($_POST["TermBizID"]), ENT_QUOTES);
      $_POST["TermBizID"] = $data["TermBizID"];
      
      $data["ShortDetailsID"]   = htmlspecialchars(trim($_POST["ShortDetailsID"]), ENT_QUOTES);
      $_POST["ShortDetailsID"] = $data["ShortDetailsID"];
      
	  $str_ShortDetailsID = $data["ShortDetailsID"];
   	  hyperlink(&$str_ShortDetailsID, 8);
   	  $data["ShortDetailsID"] = $str_ShortDetailsID;
      
      
      $data["DetailsID"] = htmlspecialchars(trim($_POST["DetailsID"]), ENT_QUOTES);
      
      $_POST["DetailsID"] = $data["DetailsID"];
     
	  $str_details = $data["DetailsID"];
   	  hyperlink(&$str_details);
   	  $data["DetailsID"] = $str_details;
      
      $data["ImgID"]  = $newname_ImgID; 
      $data["Img1ID"] = $newname_Img1ID; 
      $data["Img2ID"] = $newname_Img2ID;      
      
      $data["FaxID"]            = htmlspecialchars(trim($_POST["FaxID"]), ENT_QUOTES);
      $_POST["FaxID"] = $data["FaxID"];
      
      $data["ShareSaleID"]      = 0;
      
      $data["ReasonSellBizID"]  = 0;
      
      $data["TarifID"]          = htmlspecialchars(trim($_POST["TarifID"]), ENT_QUOTES);
      $_POST["TarifID"] = $data["TarifID"];

      $data["youtubeURL"] = htmlspecialchars(trim($_POST["youtubeURL"]), ENT_QUOTES);
      $_POST["youtubeURL"] = $data["youtubeURL"];

      $data["newspaper"]          = 0;

      $data["helpbroker"]          = 0;
      
      $data["StatusID"]         = 0;
      $data["ConstGUID"]        = md5(uniqid(rand(), true)); 

      if (strlen($data["FML"]) <= 0) {
        $error = "������ �����: "."\"��� � ������� ����, �������������� �� ��������\"";
        $smarty->assign("ErrorInput_FML",true);
      } else if (strlen($data["EmailID"]) <= 0) {
        $error = "������ �����: "."\"���������� �-����\"";
        $smarty->assign("ErrorInput_EmailID",true);
      } else if (strlen($data["PhoneID"]) <= 0) {
        $error = "������ �����: "."\"����� ��������\"";
        $smarty->assign("ErrorInput_PhoneID",true);
      } else if (strlen($data["NameBizID"]) <= 0) { 
        $error = "������ �����: "."\"�������� �������\"";
        $smarty->assign("ErrorInput_NameBizID",true);
      } else if (strlen($data["SiteID"]) <= 0) { 
        $error = "������ �����: "."\"�����, �����, ���, ��������/����\"";
        $smarty->assign("ErrorInput_SiteID",true);
      } else if (strlen($data["CostID"]) <= 0) {
        $error = "������ �����: "."\"��������� ����������� �������\"";
        $smarty->assign("ErrorInput_CostID",true);
      } else if (strlen($data["ShortDetailsID"]) <= 0) {
        $error = "������ �����: "."\"������� ����������, ������ ������������� �� ���������� �������\"";     
        $smarty->assign("ErrorInput_ShortDetailsID",true);
      } else if (strlen($data["DetailsID"]) <= 0) {
        $error = "������ �����: "."\"������ ������ ����������, ������ ����� ������� ������������� �� ���������� �������\"";     
        $smarty->assign("ErrorInput_DetailsID",true);
      } else {
      }

      if(strlen($error) > 0)
      {
  		///$headers1 = 'BizZONA.RU: new looked' . "\r\n" . 'Reply-To:  ' . "\r\n" . 'X-Mailer: PHP/' . phpversion();
		//mail("eugenekurilov@gmail.ru", "������ ".$error."", "PHP_SELF: ".$_SERVER["PHP_SELF"]."\r\n"." HTTP_HOST: ".$_SERVER["HTTP_HOST"]."\r\n"."HTTP_REFERER: ".$_SERVER["HTTP_REFERER"]."\r\n"."REMOTE_ADDR: ".$_SERVER["REMOTE_ADDR"]."\r\n" , $headers1);
      }

      if (strlen($error) <= 0) {

        $to      = 'bizzona.ru@gmail.com';
        $subject = 'NEW SUBSCRIBE: '.$data["FML"].'';

        $message = $assignDescription["FML"]." ".$data["FML"]."\n";
        $message.= $assignDescription["EmailID"]." ".$data["EmailID"]."\n";
        $message.= $assignDescription["PhoneID"]." ".$data["PhoneID"]."\n";
        $message.= $assignDescription["NameBizID"]." ".$data["NameBizID"]."\n";
        $message.= $assignDescription["CategoryID"]." ".$data["CategoryID"]."\n";
        $message.= $assignDescription["BizFormID"]." ".$data["BizFormID"]."\n";
        $message.= $assignDescription["CityID"]." ".$data["CityID"]."\n";
        $message.= $assignDescription["SiteID"]." ".$data["SiteID"]."\n";
        $message.= $assignDescription["CostID"]." ".$data["CostID"]."\n";
        $message.= $assignDescription["ProfitPerMonthID"]." ".$data["ProfitPerMonthID"]."\n";
        $message.= $assignDescription["ListObjectID"]." ".$data["ListObjectID"]."\n";
        $message.= $assignDescription["ContactID"]." ".$data["ContactID"]."\n";
        $message.= $assignDescription["MatPropertyID"]." ".$data["MatPropertyID"]."\n";
        $message.= $assignDescription["MonthAveTurnID"]." ".$data["MonthAveTurnID"]."\n";
        $message.= $assignDescription["WageFundID"]." ".$data["WageFundID"]."\n";
        $message.= $assignDescription["CountEmplID"]." ".$data["CountEmplID"]."\n";
        $message.= $assignDescription["CountManEmplID"]." ".$data["CountManEmplID"]."\n";
        $message.= $assignDescription["TermBizID"]." ".$data["TermBizID"]."\n";
        $message.= $assignDescription["ShortDetailsID"]." ".$data["ShortDetailsID"]."\n";
        $message.= $assignDescription["DetailsID"]." ".$data["DetailsID"]."\n";
        $message.= $assignDescription["ImgID"]." ".$data["ImgID"]."\n";
        $message.= $assignDescription["FaxID"]." ".$data["FaxID"]."\n";
        $message.= $assignDescription["TarifID"]." ".$data["TarifID"]."\n";
        //$message.= $assignDescription["StatusID"]." ".$data["StatusID"]."\n"; 

        $headers = 'BizZONA.RU: new subscribe' . "\r\n" .
        'Reply-To: '.$data["EmailID"].' ' . "\r\n" .
        'X-Mailer: PHP/' . phpversion();
        mail($to, $subject, $message, $headers);
        //mail("biz.ru@mail.ru", "BizZONA.RU: NEW SUBSCRIBE", "BizZONA.RU: NEW SUBSCRIBE", $headers);

        $data["CostID"] = floatval($data["CostID"]);
        $data["typeSellID"] = 2;

        $mysql_insert_id = $sell->Insert($data);
        $operator->sellId = $mysql_insert_id;
        $operator->Insert();
        $smarty->assign("code", $mysql_insert_id);
        $statusAdd = true;

        $_SESSION['timepost'] = time();
        
		$secret_code = "vbhjplfybt";
  		$purse = "4146"; // sms:bank id        ������������� ���:�����
  		$order_id = $mysql_insert_id; //  ������������� ��������
  		$amount = "5.5"; // transaction sum  ����� ����������
  		$amount_megafon = "4"; // transaction sum  ����� ����������
  		$amount_ukraine = "2.4"; // transaction sum  ����� ����������
  		$clear_amount  = "0"; // billing algorithm  �������� �������� ���������
  		$description  = "������ ���������� ������� �������"; // operation desc  �������� ��������
  		$submit  = "���������"; // submit label    ������� �� ������ submit
  		$submit_megafon  = "��������� (��� ��������)"; // submit label    ������� �� ������ submit
  		$submit_ukraine  = "���������"; // submit label    ������� �� ������ submit

  		$sign = ref_sign($purse, $order_id, $amount, $clear_amount, $description, $secret_code);
  		$sign_megafon = ref_sign($purse, $order_id, $amount_megafon, $clear_amount, $description, $secret_code);
  		$sign_ukraine = ref_sign($purse, $order_id, $amount_ukraine, $clear_amount, $description, $secret_code);

        $smarty->assign("purse", $purse);
        $smarty->assign("order_id", $order_id);
        $smarty->assign("amount", $amount);
        $smarty->assign("amount_megafon", $amount_megafon);
        $smarty->assign("amount_ukraine", $amount_ukraine);
        $smarty->assign("clear_amount", $clear_amount);
        $smarty->assign("description", $description);
        $smarty->assign("submit", $submit);
        $smarty->assign("submit_megafon", $submit_megafon);
        $smarty->assign("submit_ukraine", $submit_ukraine);
        $smarty->assign("sign", $sign);
        $smarty->assign("sign_megafon", $sign_megafon);
        $smarty->assign("sign_ukraine", $sign_ukraine);

		$money_message = cp1251_to_utf8("������ ����� �� ����� bizzona.ru (".$order_id.") ");
		$money_message = base64_encode($money_message);
		$smarty->assign("money_message", $money_message);        
        

        unset($_POST);
      }
    }
  } 

//end = get_formatted_microtime();

//$end = get_formatted_microtime();

  $data["offset"]   = 0;
  $data["rowCount"] = 0;
  $data["sort"]= "Count";
  $listCategory = $category->Select($data); 

  unset($data["sort"]);
  $data["offset"]   = 0;
  $data["rowCount"] = 0;
  //$listCity = $city->Select($data); 



  $smarty->assign("listCategory", $listCategory);
  //$smarty->assign("listCity", $listCity);
  
  $listSubCategory = $subCategory->Select($data);
  $smarty->assign("listSubCategory", $listSubCategory);
 
//$end = get_formatted_microtime(); 

  include_once("./engine/class.ContactInfo_Lite.inc");
  $contact = new ContactInfo_Lite();  

  $contact->GetContactInfo();  
  $smarty->assign("contact", $contact);

    $smarty->assign("insertsettings", $insertSettingsData);  
?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>

  <HEAD>
      <title>���������� ������� - ������� � ������� �������. ������� ������ - ������ ����</title>		
	<META NAME="Description" CONTENT="������� �������, ������� �������, ������� ������, ������� �������, ������� �������� �������, ������� �������, ������� ��������,  ������� ��������, ������� ��������, ���� ������� ������?">
        <meta name="Keywords" content="������� �������, ������� �������, ������� ������, ������� �������, ������� �������� �������, ������� �������, ������� ��������,  ������� ��������, ������� ��������, ���� ������� ������?, Ready business, �����������, �������� � ��������, ���������, ����������, �����������"> 
		<meta http-equiv="content-type" content="text/html; charset=windows-1251"/>
        <LINK href="./general.css" type="text/css" rel="stylesheet">
<link rel="shortcut icon" href="./images/favicon.ico">
  </HEAD>


<?
	$topmenu = GetMenu();
	$smarty->assign("topmenu", $topmenu);

	$link_country.= "&nbsp;&nbsp;<a href='".NAMESERVER."help.php' class='city_a'  style='color:white;font-size:10pt;color:yellow;' title='������ - ������� �������' target='_blank'>������</a>&nbsp;&nbsp;";	
	$smarty->assign("ad", $link_country);
         //$end = get_formatted_microtime();
	$smarty->display("./site/headerinsertarendasell.tpl");
?>
  
<table class="w" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td width="100%">
         <table width='100%' height='100%'>
           <?
              if (strlen($error) > 0) { 
           ?> 
           <tr  valign='top' width='100%' height='100%'>
             <td align='center'><span style="color:red;"><b><?=$error;?></b></span></td> 
           </tr>
           <?
              }
           ?> 
           <tr>
             <td valign='top' width='60%' height='100%'>
               <?
                  //$end = get_formatted_microtime();
               
                  if($blocking) {
                      $smarty->display("./site/blockip.tpl");
                  } else {
                      $smarty->display("./site/insertarendasell.tpl");
                  }
               
                  
                  //$end = get_formatted_microtime();
               ?>
             </td>
           </tr>
         </table>
        
        
        </td>
    </tr>
</table>

<?php
//$end = get_formatted_microtime();
?>

<style>
 td a 
 {
   color:black;
 }
</style>


<div style="padding-top:4pt;padding-bottom:5pt;padding-right:10pt;padding-left:10pt;font-size:10pt;font-family:arial;">
	<table cellpadding="0" cellspacing="0" border="0"  style="width:100%;padding-bottom:2pt;" bgcolor="#eeeee0">
    	<tr>
        	<td style="background-image:url(<?=$_SERVER["host_name"]?>images/d1.gif); background-repeat:no-repeat;width:5px;height:5px; background-position:left top;">&nbsp;</td>
            <td rowspan="2" valign="middle" align="center"  style="font-size:8pt; font-family:Arial; color:black;">
			<?
			?>
            </td>
            <td style="background-image:url(<?=$_SERVER["host_name"]?>images/d2.gif); background-repeat:no-repeat;width:5px;height:5px;background-position:right top;">&nbsp;</td>
        </tr>
        <tr>
            <td style="background-image:url(<?=$_SERVER["host_name"]?>images/d4.gif); background-repeat:no-repeat;width:5px;height:5px; background-position:left bottom;">&nbsp;</td>
            <td style="background-image:url(<?=$_SERVER["host_name"]?>images/d3.gif); background-repeat:no-repeat;width:5px;height:5px; background-position:right bottom;">&nbsp;</td>
        </tr>
    </table>
</div>



<?
	$smarty->display("./site/footer.tpl");
?>

<?php
$category->Close();
 $subCategory->Close();
$sell->Close();
?>


<?
  $end = get_formatted_microtime();
  $total = $end - $start;
  echo "<center><span style='font-size:7pt;'>".round($total, 6)."</span><center>";
?>


</body>
</html>
