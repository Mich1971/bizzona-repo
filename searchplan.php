<?
  global $DATABASE;

  include_once("./phpset.inc");
  include_once("./engine/functions.inc"); 

  $count_sql_call = 0;
  $start = get_formatted_microtime(); 
  $base_memory_usage = memory_get_usage();	

   
  AssignDataBaseSetting();
  
	include_once("./engine/class.category_lite.inc"); 
	$category = new Category_Lite();

	include_once("./engine/class.country_lite.inc"); 
	$country = new Country_Lite();	


  include_once("./engine/class.bizplan.inc"); 
  $bizplan = new BizPlan();

  include_once("./engine/class.bizplan_company.inc"); 
  $bizplan_company = new BizPlanCompany();

	include_once("./engine/class.buy_lite.inc"); 
	$buyer = new Buyer_Lite();

  	include_once("./engine/class.district_lite.inc"); 
  	$district = new District_Lite();

  	include_once("./engine/class.subcategory_lite.inc");
  	$subcategory = new SubCategory_Lite();

  include_once("./engine/class.streets_lite.inc");
  $streets = new Streets_Lite();

  require_once("./libs/Smarty.class.php");
  $smarty = new Smarty;
  $smarty->template_dir = "./templates";
  $smarty->compile_dir  = "./templates_c";
  //$smarty->cache_dir = "./cache";
  $smarty->compile_check = true;
  $smarty->debugging     = false;

  require_once("./regfuncsmarty.php");
  
  
  $name_words = array("turagenstvo", "restoran", "parik", "krasota", "otel");
  if(isset($_GET["name"])) {
	if(in_array($_GET["name"], $name_words)) {
	} else {
		$_GET["name"] = "";
	}
  }
  
  if(isset($_GET["MaxCostID"])) {
  	$_GET["MaxCostID"] = intval($_GET["MaxCostID"]);
  }
  
  if(isset($_GET["MinCostID"])) {
  	$_GET["MinCostID"] = intval($_GET["MinCostID"]);
  }

  if(isset($_GET["ID"])) {
  	$_GET["ID"] = intval($_GET["ID"]);
  }

	$action_words = array("category", "city", "price", "typeBizID");
	if(isset($_GET["action"])) {
		if(in_array($_GET["action"], $action_words)) {
		} else {
			$_GET["action"] = "";
		}
	} 
  
	$headertitle = "������� ������-������";
	$smarty->assign("headertitle", $headertitle);
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>

  <HEAD>

<?

  if (isset($_GET["action"])) {
    switch(trim($_GET["action"])) {
      case "category": {
        ?>
          <title>������� ������-������ - ��� "������ ����"</title>		
        <?
      } break;
      case "city": {
        $dataCity["ID"] = intval($_GET["ID"]);
        $objCity = $city->GetItem($dataCity);
        ?>
          <title>������� ������� <?=$objCity->title;?> | ������� ������� <?=$objCity->title;?> | ������� ������ - ��� "������ ����"</title>		
        <?

      } break;
      case "price": {
        ?>
          <title>������� ������-������ - ��� "������ ����"</title>
        <?
      } break;
      default: {
        ?>
          <title>������� ������-������ - ��� "������ ����"</title>		
        <?
      }   
    }
  } else {
    ?>
      <title>������� ������-������ - ��� "������ ����"</title>		
    <?
  } 


?>
	<META NAME="Description" CONTENT='������� ������-������- ��� "������ ����"'>
    <meta name="Keywords" content="������� ������-������"> 
    <LINK href="http://www.bizzona.ru/general.css" type="text/css" rel="stylesheet">
    <meta http-equiv="content-type" content="text/html; charset=windows-1251"/>
</HEAD>

<body>

<?
  $smarty->display("./site/headerbanner.tpl");
?>

<?
	$smarty->display("./site/headerplan.tpl");
?>

<table class="w" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td width="30%" valign="top">

        <?
          $smarty->cache_dir = "./persistentcache"; 
          $smarty->cache_lifetime = 10800;         
        
          if (isset($_GET["action"]) && $_GET["action"]  == "category") {
            $_GET["ID"] = (isset($_GET["ID"])  ? intval($_GET["ID"]) : 0);
            $smarty->caching = true; 
            if (!$smarty->is_cached('./site/categorysp.tpl', $_GET["ID"])) {
              $data = array();
              $data["offset"] = 0;
              $data["rowCount"] = 0;
              $data["sort"] = "CountP";
              $result = $category->Select($data);
              $smarty->assign("data", $result);
            }
            $smarty->display("./site/categorysp.tpl", $_GET["ID"]);
            $smarty->caching = false; 
          } else { 
            $smarty->caching = true; 
            if (!$smarty->is_cached('./site/categorysp.tpl')) {
              $data = array();
              $data["offset"] = 0;
              $data["rowCount"] = 0;
              $data["sort"] = "CountP";
              $result = $category->Select($data);
              $smarty->assign("data", $result);
            }
            $smarty->display("./site/categorysp.tpl");
            $smarty->caching = false; 
          }
          
          $smarty->cache_dir = "./cache";
          $smarty->cache_lifetime = 3600;          
        ?>
        
		<div style="padding-top:2pt;"></div>            
        <?
         	$smarty->display("./site/request.tpl");
        ?>  
        
        <?
          	$smarty->display("./site/proposal.tpl");
       ?>  
     
        
        </td>
        <td width="70%" valign="top">
      			<?
       				$res_company = $bizplan_company->SelectByStatus(OPEN_BIZPLAN_COMPANY);
       				$smarty->assign("data", $res_company);
           			$smarty->display("./site/bizplancompany.tpl");
       			?>
               <?
                    if (isset($_GET["action"])) {
                       switch(trim($_GET["action"])) {
                         case "typeBizID": {
                           $_GET["ID"] = intval($_GET["ID"]); 
                           $data["category_id"] = $_GET["ID"];
                           $dataPage["category_id"] = $_GET["ID"];
                         } break;
                         case "company": {
                           $_GET["ID"] = intval($_GET["ID"]); 
                           $data["company_id"] = $_GET["ID"];
                           $dataPage["company_id"] = $_GET["ID"];
                         } break;
                         
                         default: {
                         }   
                       }
                    }
               
                    $pagesplit = GetPageSplit();
               
                    $dataPage["offset"] = 0;
                    $dataPage["rowCount"] = 0;
                    $dataPage["sort"] = "id";
                    $dataPage["status_id"] = "-1";
                    $resultPage = $bizplan->Select($dataPage);
                    $smarty->assign("CountRecord", sizeof($resultPage));
                    $smarty->assign("CountSplit", $pagesplit);
                    $smarty->assign("CountPage", ceil(sizeof($resultPage)/5));

                 	$data["offset"] = 0;
                 	$data["rowCount"] = 0;
                 	$data["sort"] = "`bizplan`.`datecreate`";
                 	$data["status_id"] = "-1";

    				if (isset($_GET["offset"])) {
      					$data["offset"] = intval($_GET["offset"]);
    				} else {
      					$data["offset"] = 0;
    				}
    				
    				if (isset($_GET["rowCount"])) { 
      					$data["rowCount"] = intval($_GET["rowCount"]);
    				} else {
      					$data["rowCount"] = $pagesplit;
    				}
                 	
                 	
                 	$result = $bizplan->Select($data);
                 	$smarty->assign("data", $result);
                 
					$smarty->display("./site/navsearchp.tpl");
                	$smarty->display("./site/pagesplitp.tpl", $_SERVER["REQUEST_URI"]);

                	$smarty->display("./site/ilistsellplan.tpl", $_SERVER["REQUEST_URI"]);
                  
                	$smarty->display("./site/pagesplitp.tpl", $_SERVER["REQUEST_URI"]);
					$smarty->display("./site/navsearchp.tpl");
               ?>
              
            	<div class="lftpadding" style="width: auto;">
       				<div  style="background-image:url(<?=$_SERVER["HTTP_HOST"]?>images/bl.gif); background-repeat:repeat-x;">&nbsp;</div>
            	</div>
            	
				<div class="lftpadding_cnt" style="padding-top:10pt; padding-bottom: 5pt;padding-top:0pt; padding-bottom:0pt; width:auto;" >	    
					<table border="0" align="right">
                		<tr>
                			<td align="right">
								<script type="text/javascript" src="//yandex.st/share/share.js" charset="utf-8"></script>
								<div class="yashare-auto-init" data-yashareType="button" data-yashareQuickServices="yaru,vkontakte,facebook,twitter,odnoklassniki,moimir,lj,friendfeed,moikrug"></div>
                			</td>
						</tr>
					</table>
				</div>            	
        </td>
    </tr>
</table>

<style>
 td a 
 {
   color:black;
 }
</style>

<div style="padding-top:4pt;padding-bottom:5pt;padding-right:10pt;padding-left:10pt;font-size:10pt;font-family:arial;">
	<table cellpadding="0" cellspacing="0" border="0"  style="width:100%;padding-bottom:2pt;" bgcolor="#eeeee0">
    	<tr>
        	<td style="background-image:url(http://<?=$_SERVER["HTTP_HOST"]?>/images/d1.gif); background-repeat:no-repeat;width:5px;height:5px; background-position:left top;">&nbsp;</td>
            <td rowspan="2" valign="middle" align="center"  style="font-size:8pt; font-family:Arial; color:black;">
			<?
			?>
            </td>
            <td style="background-image:url(http://<?=$_SERVER["HTTP_HOST"]?>/images/d2.gif); background-repeat:no-repeat;width:5px;height:5px;background-position:right top;">&nbsp;</td>
        </tr>
        <tr>
            <td style="background-image:url(http://<?=$_SERVER["HTTP_HOST"]?>/images/d4.gif); background-repeat:no-repeat;width:5px;height:5px; background-position:left bottom;">&nbsp;</td>
            <td style="background-image:url(http://<?=$_SERVER["HTTP_HOST"]?>/images/d3.gif); background-repeat:no-repeat;width:5px;height:5px; background-position:right bottom;">&nbsp;</td>
        </tr>
    </table>
</div>

<?
	$smarty->display("./site/footer.tpl");
?>

<?
  $end = get_formatted_microtime(); 
  $total = $end - $start;
  echo "<center><span style='font-size:7pt;'>".round($total, 6)." : ".$count_sql_call." : ".$base_memory_usage." : ".memoryUsage(memory_get_usage(), $base_memory_usage)." </span><center>";
?>

</body>
</html>
