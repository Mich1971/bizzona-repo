<?
	global $DATABASE;

	include_once("./phpset.inc");
	include_once("./engine/functions.inc"); 

	$start = get_formatted_microtime(); 

	AssignDataBaseSetting();
  
	include_once("./engine/class.category.inc");
	$category = new Category();

	include_once("./engine/class.country.inc"); 
	$country = new Country();

	include_once("./engine/class.city.inc");  
	$city = new City();

	include_once("./engine/class.sell.inc"); 
	$sell = new Sell();

	include_once("./engine/class.buy.inc"); 
	$buyer = new Buyer();
  
	include_once("./engine/class.metro.inc");
	$metro = new Metro();
  
	include_once("./engine/class.district.inc"); 
	$district = new District();

	include_once("./engine/class.subcategory.inc");
	$subcategory = new SubCategory();

	include_once("./engine/class.streets.inc"); 
	$streets = new streets();

	require_once("./libs/Smarty.class.php");
	$smarty = new Smarty;
	$smarty->template_dir = "./templates";
	$smarty->compile_dir  = "./templates_c";
	//$smarty->cache_dir = "./cache";
	$smarty->compile_check = true;
	$smarty->debugging     = false;

	
	
	require_once("./regfuncsmarty.php");
  
	if(isset($_GET["ID"])) {
		$_GET["ID"] = intval($_GET["ID"]);
	}
  
	if(isset($_GET["rowCount"])) {
		$_GET["rowCount"] = intval($_GET["rowCount"]);
	}

	if(isset($_GET["offset"])) {
		$_GET["offset"] = intval($_GET["offset"]);
	}

	if(isset($_GET["streetID"])) {
		$_GET["streetID"] = intval($_GET["streetID"]);
	}
  
	if(isset($_GET["cityId"])) {
		$_GET["cityId"] = intval($_GET["cityId"]);
	}	

	if(isset($_GET["subCityId"])) {
		$_GET["subCityId"] = intval($_GET["subCityId"]);
	}	
	
	if(isset($_GET["catId"])) {
		$_GET["catId"] = intval($_GET["catId"]);
	}	
	
	if(isset($_GET["subCatId"])) {
		$_GET["subCatId"] = intval($_GET["subCatId"]);
	}	
	
	$name_words = array("turagenstvo", "restoran", "parik", "krasota", "otel");
	if(isset($_GET["name"])) {
		if(in_array($_GET["name"], $name_words)) {
		} else {
			$_GET["name"] = "";
		}
	}
  
	$action_words = array("category", "city", "price", "topbiz");
	if(isset($_GET["action"])) {
		if(in_array($_GET["action"], $action_words)) {
		
		} else {
			$_GET["action"] = "";
		}
	}

	$sell->districtID = (isset($_GET["districtId"]) ? intval($_GET["districtId"]) : 0);
	$sell->metroID = (isset($_GET["metroId"]) ? intval($_GET["metroId"]) : 0);
	$sell->SubCategoryID = (isset($_GET["subCatId"]) ? intval($_GET["subCatId"]) : 0);
	$sell->CategoryID = (isset($_GET["catId"]) ? intval($_GET["catId"]) : 0);
	
	if(isset($_GET["subCityId"]) and intval($_GET["subCityId"]) > 0) {
		$sell->CityID = intval($_GET["subCityId"]);
	} else {
		$sell->CityID = (isset($_GET["cityId"]) ? intval($_GET["cityId"]) : 0);	
	}
	$title = $sell->GetTitleForUserFriendlySelect();
	
	$title = (strlen($title) > 0 ? $title : "������� �������� �������");
	
	$smarty->assign("title", $title);
	 
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
  <HEAD>
   <title><?=$title;?></title>		
  
	<META NAME="Description" CONTENT="<?=$title;?>">
        <meta name="Keywords" content="<?=$title;?>"> 
        <LINK href="http://www.bizzona.ru/general.css" type="text/css" rel="stylesheet">
        <meta http-equiv="content-type" content="text/html; charset=windows-1251"/>
        <link rel="shortcut icon" href="http://www.bizzona.ru/images/favicon.ico">
    </HEAD>
<body>

<?
  $smarty->display("./site/headerbanner.tpl");
?>

<?
	$topmenu = GetMenu();
	$smarty->assign("topmenu", $topmenu);
	
	$link_country = GetSubMenu();	
	$smarty->assign("ad", $link_country);
	
?>
<table class="w" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="2">
			<?
				$output_header = $smarty->fetch("./site/detailssearchheader.tpl");
				echo minimizer($output_header);			
			?>	
		</td>
	</tr>
    <tr>
        <td width="30%" valign="top" style="padding-top:2pt;">
        <?
          $sizeof_subcat_friendly = 0;
          $sizeof_sel_friendly = 0;
          $res_sel_friendly = array();
          $res_sel_subcat_friendly = array();
          
          if(isset($_GET["subCatId"]) && intval($_GET["subCatId"]) > 0 && isset($_GET["cityId"]) && intval($_GET["cityId"]) > 0) {

          	$smarty->caching = false;
          	$sell->SubCategoryID = intval($_GET["subCatId"]);
          	$sell->CityID = intval($_GET["cityId"]);
          	$res_sel_friendly = $sell->SelectUserFriendlySell();
          	$sizeof_sel_friendly = sizeof($res_sel_friendly);
          	
          } 
          
          if (isset($_GET["catId"]) && intval($_GET["catId"]) > 0 /*&& $sizeof_sel_friendly <= 0*/) {
          	$set_cityId = ((isset($_GET["subCityId"]) && intval($_GET["subCityId"]) > 0) ? intval($_GET["subCityId"]) : intval($_GET["cityId"]));
          	$smarty->caching = false;
          	$sell->CategoryID = intval($_GET["catId"]);
          	$sell->CityID = $set_cityId;
          	$res_sel_subcat_friendly = $sell->SelectSubCategoryFriendly();
          	$sizeof_subcat_friendly = sizeof($res_sel_subcat_friendly);	
          	
          }
          		
          if($sizeof_sel_friendly > 0) {
          	
          		$smarty->assign("dataquickmetro", $res_sel_friendly);
  				$output_sel_friendly = $smarty->fetch("./site/quicksearchbymetro.tpl");
  				echo fminimizer($output_sel_friendly);
          	
          } 
          
          if ($sizeof_subcat_friendly > 0) {
          		$smarty->assign("subcatfriendly", $res_sel_subcat_friendly);
  				$output_subcatfriendly = $smarty->fetch("./site/subcatfriendly.tpl");
  				echo fminimizer($output_subcatfriendly);          			
          	
          	
		  } 
		  
		  if ($sizeof_sel_friendly <= 0 && $sizeof_subcat_friendly <= 0) {
		  	
          		SeoSuggestedCat();
          		
          } else if ($sizeof_sel_friendly <= 0 && $sizeof_subcat_friendly > 0) {
          	
          		if(isset($_GET["subCityId"]) && intval($_GET["subCityId"]) > 0) {
          			
					$_GET["ID"] = intval($_GET["subCityId"]); 
					$_GET["action"] = "city";
          			ShowLeftCategory();
          			
          		} else if (isset($_GET["cityId"]) && intval($_GET["cityId"]) > 0) {
          			
					$_GET["ID"] = intval($_GET["cityId"]); 
					$_GET["action"] = "city";
					ShowLeftCategory();
					
          		}
          }
          
 
        ?>
            <?
            	$datahotbuyer = array();
            	$datahotbuyer["offset"] = 0;
            	$datahotbuyer["rowCount"] = 4;
            	$datahotbuyer["sort"] = "datecreate";
            	$datahotbuyer["StatusID"] = "-1";
            	
                if (isset($_GET["action"])) {
                   switch(trim($_GET["action"])) {
	                   case "category": {
    	                 $datahotbuyer["typeBizID"] = intval($_GET["ID"]);
                       } break;
                       case "city": {
                         $datahotbuyer["regionID"] = intval($_GET["ID"]);
                       } break;                       
        	           default: {
                         		
                       }
                       break;
                   }            	
                }		
            	
                //$smarty->display("./site/call.tpl");
                
				$output_call = $smarty->fetch("./site/call.tpl");
				echo fminimizer($output_call);
				
                
  	          	$res_datahotbuyer = $buyer->Select($datahotbuyer);
  	          	if(sizeof($res_datahotbuyer) > 0)
  	          	{
    				$smarty->assign("databuyer", $res_datahotbuyer);
            		//$smarty->display("./site/iblockbuyer.tpl");
            		
					$output_iblockbuyer = $smarty->fetch("./site/iblockbuyer.tpl");
					echo fminimizer($output_iblockbuyer);
            		
  	          	}
  	          	else 
  	          	{
  	          		//$smarty->display("./site/begun.tpl");
  	          	}
  	          	
        	//$smarty->display("./site/proposal.tpl");
			$output_proposal = $smarty->fetch("./site/proposal.tpl");
			echo fminimizer($output_proposal);
        	
         	//$smarty->display("./site/request.tpl");
			$output_request = $smarty->fetch("./site/request.tpl");
			echo fminimizer($output_request);
			
			$output_citydomains = $smarty->fetch("./site/citydomains.tpl");
           	echo minimizer($output_citydomains);
        ?>              
        
        </td>
        <td width="70%" valign="top">
        
        	<?
				if(isset($_GET["cityId"]) && intval($_GET["cityId"]) > 0 && isset($_GET["subCatId"]) && intval($_GET["subCatId"]) > 0 ) {
				
					$sell->CityID = intval($_GET["cityId"]);
					$sell->SubCategoryID = intval($_GET["subCatId"]);
					$res_FriendlySellByDistrict = $sell->SelectUserFriendlySellByDistrict();
					if(sizeof($res_FriendlySellByDistrict) > 0) {
						$smarty->assign("dataFriendlySellByDistrict", $res_FriendlySellByDistrict);
						$output_quickseacrhbysubcategory = $smarty->fetch("./site/quickseacrhbysubcategory.tpl");
						echo fminimizer($output_quickseacrhbysubcategory);
					}
				
				}
        	
        	?>
        
        
            <?
            	$show_ShowDetailsSearchForm = false;
            	$sell->SubCategoryID = (isset($_GET["subCatId"]) ? intval($_GET["subCatId"]) : 0);
            	if($sell->SubCategoryID > 0) {
            	
            		
            		$_GET["catId"] = $sell->CategoryID;
            		$_GET["subCatId"] = $sell->SubCategoryID;
            		
           			$res_ListCityForSearch = $sell->ListCityForSearch();
           			$res_ListRegionForSearch = $sell->ListRegionForSearch();
           			while (list($k, $v) = each($res_ListCityForSearch)) {
           				if($v->CityID == 77) {
           					$smarty->assign("show_moscow_link", "1");
           				}
           				if($v->CityID == 78) {
           					$smarty->assign("show_spb_link", "1");
           				}
           			}
           			reset($res_ListCityForSearch);
           			while (list($k, $v) = each($res_ListRegionForSearch)) {
           				if($v->CityID == 50) {
           					$smarty->assign("show_moscow_region_link", "1");
           					break;
           				}
           			}
           			reset($res_ListRegionForSearch);
           			$smarty->assign("ListCityForSearch", $res_ListCityForSearch);
           			$smarty->assign("ListRegionForSearch", $res_ListRegionForSearch);

           			if(isset($_GET["subCatId"]) && intval($_GET["subCatId"]) == 81) {
            			$output_81 = $smarty->fetch("./site/topparameters/81.tpl");
						echo fminimizer($output_81);
           			}
           			
           			
            		$output_SelectByRegionSubRegion = $smarty->fetch("./site/SelectByRegionSubRegion.tpl", $sell->SubCategoryID);
					echo fminimizer($output_SelectByRegionSubRegion);
					
					
            	} else {
            	  ShowDetailsSearchForm();	
            	  $show_ShowDetailsSearchForm = true;
            	}

            	//$smarty->display("./site/navsearch.tpl");
				$output_navsearch = $smarty->fetch("./site/navsearchdetails.tpl");
				echo fminimizer($output_navsearch);
				
                    $data = array();
                    $dataPage = array(); 
                    $pagesplit = GetPageSplit();

                    if (isset($_GET["offset"])) {
                      $data["offset"] = intval($_GET["offset"]);
                    } else {
                      $data["offset"] = 0;
                    }
                    if (isset($_GET["rowCount"])) { 
                      $data["rowCount"] = intval($_GET["rowCount"]);
                    } else {
                      $data["rowCount"] = $pagesplit;
                    }

                    $data["sort"] = "ID";

                    
                    $data["cityId"] = intval($_GET["cityId"]);
                    $data["subCityId"] = intval($_GET["subCityId"]);
                    $data["catId"] = intval($_GET["catId"]);
                    $data["subCatId"] = intval($_GET["subCatId"]);
                    $data["metroId"] = intval($_GET["metroId"]);
                    $data["districtId"] = intval($_GET["districtId"]);
                    $data["brokerId"] = intval($_GET["brokerId"]);
                    
           
                    $_GET["cityId"] = $data["cityId"];
                    $_GET["subCityId"] = $data["subCityId"];
                    $_GET["catId"] = $data["catId"];
                    $_GET["subCatId"] = $data["subCatId"];
                    $_GET["metroId"] = $data["metroId"];
                    $_GET["districtId"] = $data["districtId"];
                    $_GET["brokerId"] = $data["brokerId"];
                    
                    if(isset($_GET["par1"])) {
                    	$data["par1"] = intval($_GET["par1"]);
                    	$_GET["par1"] = $data["par1"];
                    	$dataPage["par1"] = $data["par1"];
                    }                    
                    
                    reset($data);

                    $dataPage["offset"] = 0;
                    $dataPage["rowCount"] = 0;
                    $dataPage["sort"] = "ID";
                    $dataPage["cityId"] = $data["cityId"];
                    $dataPage["subCityId"] = $data["subCityId"];
                    $dataPage["catId"] = $data["catId"];
                    $dataPage["subCatId"] = $data["subCatId"];
                    $dataPage["metroId"] = $data["metroId"];
                    $dataPage["districtId"] = $data["districtId"];
                    $dataPage["brokerId"] = $data["brokerId"];
                    
                    $resultPage = $sell->DetailsCountSelect($dataPage);
                    
                    
                    $smarty->assign("CountRecord",$resultPage);
                    $smarty->assign("CountSplit", $pagesplit);
                    $smarty->assign("CountPage", ceil($resultPage/5));

                    $data["StatusID"] = "-1";
                    $data["sort"] = "pay_datetime";

                    $result = $sell->DetailsSelect($data);

                    $smarty->assign("data", $result);

				  $output_pagesplit = $smarty->fetch("./site/pagesplitdetails.tpl", $_SERVER["REQUEST_URI"]);
				  echo fminimizer($output_pagesplit);
                  
                  
				  $saveme = $smarty->fetch("./site/saveme.tpl");
				  echo $saveme;
                  
				  $output_ilistsell = $smarty->fetch("./site/ilistsell.tpl", $_SERVER["REQUEST_URI"]);
				  echo fminimizer($output_ilistsell);
				   
                   if($resultPage <= 0) {
                   	?>
                   		<div class="lftpadding" style="width: auto;">
                    		<div style="font-family:Arial;font-size:13pt;color:#7C3535;font-weight:bold;padding-top:4pt;margin:0pt;">�� ������� ������� �������� �� ������� �� �������</div>
                    	</div>		
                    <?	
                    }

                  
               ?>
            	<div class="lftpadding" style="width: auto;">
       				<div  style="background-image:url(<?=$_SERVER["HTTP_HOST"]?>/images/bl.gif); background-repeat:repeat-x;">&nbsp;</div>
            	</div>
            	<?
				  	echo fminimizer($output_pagesplit);
				  	echo fminimizer($output_navsearch);
				  	
				  	if(!$show_ShowDetailsSearchForm) {
				  		ShowDetailsSearchForm();
				  	}
				?>
				
				<div class="lftpadding_cnt" style="padding-top:10pt; padding-bottom: 5pt;padding-top:0pt; padding-bottom:0pt; width:auto;" >	    
					<table border="0" align="right" width="100%">
                		<tr>
                			<td width="60%"></td>
                				<td>
									<script type="text/javascript" src="http://vkontakte.ru/js/api/share.js?10" charset="windows-1251"></script>
									<script type="text/javascript">
									<!--
										document.write(VK.Share.button(false,{type: "round", text: "���������"}));
									-->
									</script>            
								</td>
								<td>
									<a class="mrc__share" href="http://connect.mail.ru/share">� ��� ���</a>
									<script src="http://cdn.connect.mail.ru/js/share/2/share.js" type="text/javascript" charset="UTF-8"></script>
								</td>
                				<td>
									<a href="http://twitter.com/share" class="twitter-share-button" data-count="horizontal">Tweet</a><script type="text/javascript" src="http://platform.twitter.com/widgets.js"></script>                
                				</td>	
                				<td>
                				</td>			
							</tr>
						</table>
				</div>
				
        </td>
    </tr>
    
	<tr>
		<td colspan="2">
		
<div style="padding-top:4pt;padding-bottom:5pt;padding-right:10pt;padding-left:10pt;font-size:10pt;font-family:arial;">
	<table cellpadding="0" cellspacing="0" border="0"  style="width:100%;padding-bottom:2pt;" bgcolor="#eeeee0">
    	<tr>
        	<td style="background-image:url(http://<?=$_SERVER["HTTP_HOST"]?>/images/d1.gif); background-repeat:no-repeat;width:5px;height:5px; background-position:left top;">&nbsp;</td>
            <td rowspan="2" valign="middle" align="center"  style="font-size:8pt; font-family:Arial; color:black;">
            
             <a href="http://www.bizzona.ru/prodazha-biznesa-avtomoika.php" title="������� ���������">������� ���������</a>&nbsp;
             <a href="http://www.bizzona.ru/prodazha-biznesa-avtoskola.php" title="������� ���������">������� ���������</a>&nbsp;
             <a href="http://www.bizzona.ru/prodazha-biznesa-employment-agency.php" title="������� ��������� ���������">������� ��������� ���������</a>&nbsp;
             <a href="http://www.bizzona.ru/prodazha-biznesa-building-company.php" title="������� ������������ �����">������� ������������ �����</a>&nbsp;
             <a href="http://www.bizzona.ru/prodazha-biznesa-car-care-center.php" title="������� �����������">������� �����������</a>&nbsp;
             <a href="http://www.bizzona.ru/prodazha-biznesa-car-magazin.php" title="������� ��������">������� ��������</a>&nbsp;
             
             
            </td>
            <td style="background-image:url(http://<?=$_SERVER["HTTP_HOST"]?>/images/d2.gif); background-repeat:no-repeat;width:5px;height:5px;background-position:right top;">&nbsp;</td>
        </tr>
        <tr>
            <td style="background-image:url(http://<?=$_SERVER["HTTP_HOST"]?>/images/d4.gif); background-repeat:no-repeat;width:5px;height:5px; background-position:left bottom;">&nbsp;</td>
            <td style="background-image:url(http://<?=$_SERVER["HTTP_HOST"]?>/images/d3.gif); background-repeat:no-repeat;width:5px;height:5px; background-position:right bottom;">&nbsp;</td>
        </tr>
    </table>
</div>
		
		
		</td>
	</tr>

	<tr>
		<td colspan="2">

<?
	$subdomain_city = $smarty->fetch("./site/site_city.tpl");
	echo fminimizer($subdomain_city);
	//$subdomain_city = $smarty->fetch("./site/subdomain_city.tpl");
	//echo fminimizer($subdomain_city);

	$smarty->display("./site/footer.tpl");
  	//$output_footer = $smarty->fetch("./site/footer.tpl");
  	//echo minimizer($output_footer);
?>

<?
  $end = get_formatted_microtime(); 
  $total = $end - $start;
  echo "<center><span style='font-size:7pt;'>".round($total, 6)."</span><center>";
?>		
		
		</td>
	</tr>
	
	<tr>
		<td colspan="2">
		
		</td>
	</tr>
	
</table>

<style>
 td a 
 {
   color:black;
 }
</style>


<!-- script type="text/javascript" src="http://www.bizzona.ru/ja.js"></script -->

<script language='javascript'>
  function SellPreviewAdd() {
  	
  }
  function AddToSearch(obj) {
	var input_search = document.getElementById("searchtext");
	if(input_search != null) {
		if(obj.innerText != null) {
			input_search.value = obj.innerText;
		} else if (obj.innerHTML != null) {
			input_search.value = obj.innerHTML;
		}
	}
  }  
</script>


</body>
</html>
<?
	$sell->Close();
	$category->Close();
	$city->Close();
	$country->Close();
	$buyer->Close();
	$metro->Close();
	$district->Close();
	$subcategory->Close();
	$streets->Close();
?>
