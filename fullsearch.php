<?
	global $DATABASE;
	include_once("./phpset.inc");
	include_once("./engine/functions.inc"); 

	$count_sql_call = 0;
	$start = get_formatted_microtime(); 
	$base_memory_usage = memory_get_usage();	
	
	AssignDataBaseSetting();
	
	//ini_set("display_startup_errors", "on");
	//ini_set("display_errors", "on");
	//ini_set("register_globals", "on");
	
	require_once("./engine/Cache/Lite.php");

	include_once("./engine/class.category_lite.inc"); 
	$category = new Category_Lite();
	
	include_once("./engine/class.country_lite.inc"); 
	$country = new Country_Lite();
	
	include_once("./engine/class.city_lite.inc"); 
	$city = new City_Lite();

	include_once("./engine/class.sell_lite.inc"); 
	$sell = new Sell_Lite();

	include_once("./engine/class.buy_lite.inc"); 
	$buyer = new Buyer_Lite();

  	include_once("./engine/class.equipment_lite.inc"); 
  	$equipment = new Equipment_Lite();
  
  	include_once("./engine/class.metro_lite.inc"); 
  	$metro = new Metro_Lite();
  
  	include_once("./engine/class.district_lite.inc"); 
  	$district = new District_Lite();

  	include_once("./engine/class.subcategory_lite.inc");
  	$subcategory = new SubCategory_Lite();

  	include_once("./engine/class.streets_lite.inc");
  	$streets = new Streets_Lite();

  	include_once("./engine/class.company.inc"); 
  	$company = new Company();    	
  	
  	include_once("./engine/class.ad.inc");
  	$ad = new Ad();

        include_once("./engine/class.review.inc"); 
        $review = new Review();        
        
    include_once("./engine/class.frontend.inc");

    require_once("./libs/Smarty.class.php");
  	$smarty = new Smarty;
  	$smarty->template_dir = "./templates";
  	$smarty->compile_dir  = "./templates_c";
  	//$smarty->cache_dir = "./cache";
  	$smarty->compile_check = true;
  	$smarty->debugging     = false;
  
	$smarty->assign("listcompany", $company->ListCompanyInList());  	  	  	  	
  	
  	require_once("./regfuncsmarty.php");
  
  	$assignDescription = AssignDescription();
  
  	$colorset = GetColorSet();
  	$smarty->assign("colorset", $colorset);
  	
  	if(isset($_GET["searchtext"])) {
  		$_GET["searchtext"] = ParsingIndexingSell($_GET["searchtext"]);
  	}
  
  	if(isset($_GET["offset"])) {
  		$_GET["offset"] =  intval($_GET["offset"]);
  	}
  
  	if(isset($_GET["startcostf"])) {
  		$_GET["startcostf"] = intval($_GET["startcostf"]);
  	} else {
  		$_GET["startcostf"] = 0;
  	}
  
  	if(isset($_GET["stopcostf"])) {
  		$_GET["stopcostf"] = intval($_GET["stopcostf"]);
  	} else {
  		$_GET["stopcostf"] = 0;
  	}

  	if((intval($_GET["startcostf"]) > intval($_GET["stopcostf"])) && (intval($_GET["startcostf"]) > 0 && intval($_GET["stopcostf"]) > 0) ) {
  		$temp_c  = $_GET["stopcostf"];
  		$_GET["stopcostf"] = $_GET["startcostf"];
  		$_GET["startcostf"] = $temp_c;
  	}
  
  	if(isset($_GET["fr"])) {
  		$_GET["fr"] = 1;
  	}  
  
  	if(isset($_GET["own"])) {
  		$_GET["own"] = 1;
  	}
  
  	if(isset($_GET["monthaveturn"])  && intval($_GET["monthaveturn"]) > 0) {
  		$_GET["monthaveturn"] = intval($_GET["monthaveturn"]);
  	} else {
  		$_GET["monthaveturn"] = 0;
  	}
  	
  	if(isset($_GET["profit"]) &&  intval($_GET["profit"]) > 0) {
  		$_GET["profit"] = intval($_GET["profit"]);
  	} else {
  		$_GET["profit"] = 0;
  	}  
?>
<?
  	$options = array(
  		'cacheDir' => "sellcache/",
   		'lifeTime' => 86400
  	);
		
  	$cache = new Cache_Lite($options);
	
  	if ($data = $cache->get('subcategoryfullseacrh')) {
		$dataSubCategory = unserialize($data);
  	} else { 
		$dataSubCategory = $subcategory->GetSubcategoryForFullSearch();
		$cache->save(serialize($dataSubCategory));
  	}
  	$smarty->assign("dataSubCategory", $dataSubCategory);
?>
<?
  	$dataCity = array();
  	$dataCity["offset"] = 0;
  	$dataCity["rowCount"] = 0;
  	$dataCity["sort"] = "Count";
  	$resultCity = $city->SelectActive($dataCity);
  	$smarty->assign("dataCity", $resultCity);

  	unset($dataCity);
  	
  	if ($data = $cache->get('countriesfullseacrh')) {
  		$resultCountries = unserialize($data);
  	} else {
  		$resultCountries = $city->SelectCountryActive();
  		$cache->save(serialize($resultCountries));
  	}
  	$smarty->assign("dataCountries", $resultCountries);
        
  	unset($resultCountries);
?>
<?
	if (isset($_GET["CategoryID"]) && is_array($_GET["CategoryID"])) {
    	while(list($kCat, $vCat) = each($_GET["CategoryID"])) {
      		$_GET["CategoryID"][$kCat] = intval($vCat);
    	} 
    	$smarty->assign("afterDataCategory", $_GET["CategoryID"]);
    	$strCategoryID = implode(",", $_GET["CategoryID"]);
    	$_GET["CategoryID"] = $strCategoryID;
  	} else if (isset($_GET["CategoryID"]) && strlen($_GET["CategoryID"]) > 0) {
    	$aCat = split("[,]", trim($_GET["CategoryID"]));
    	if (is_array($aCat) && sizeof($aCat) > 0) {
      		while (list($k, $v) = each($aCat)) {
        		$aCat[$k] = intval($v);
      		}
      		reset($aCat);        
      		$strCategoryID = implode(",", $aCat);
      		$_GET["CategoryID"] = $strCategoryID;
      		while (list($k, $v) = each($aCat)) {
        		$tempCat[$v] = $v;
      		}
      		if (isset($tempCat)) {
        		$smarty->assign("afterDataCategory", $tempCat);
      		}
    	}	
  	}

  	if (isset($_GET["CityID"]) && is_array($_GET["CityID"])) {
    	while(list($kCity, $vCity) = each($_GET["CityID"])) {
       		$_GET["CityID"][$kCity] = intval($vCity);
    	}
    	$smarty->assign("afterDataCity", $_GET["CityID"]);
    	$strCityID = implode(",", $_GET["CityID"]);
    	$_GET["CityID"] = $strCityID;
  	} else if (isset($_GET["CityID"]) && strlen($_GET["CityID"]) > 0) {
    	$aCit = split("[,]", trim($_GET["CityID"]));
    	if (is_array($aCit) && sizeof($aCit) > 0) {
      		while (list($k, $v) = each($aCit)) {
        		$aCit[$k] = intval($v);
      		} 
      		reset($aCit);      
      		$strCityID = implode(",", $aCit);
      		$_GET["CityID"] = $strCityID;
      		while (list($k, $v) = each($aCit)) {
        		$tempCit[$v] = $v;
      		}
      		if (isset($tempCit)) {
        		$smarty->assign("afterDataCity", $tempCit);
      		}
    	} 
  	}

	if (isset($_GET["add"])) {
		switch (trim($_GET["add"])) {
        	case "c": {
          		$sort = "Sell.CostID ";
        	} break;
        	case "d":  {
          		$sort = "Sell.pay_datetime";
        	} break;
        	default:  {
         		$sort = "Sell.pay_datetime";
         		$_GET["add"] = $sort;
        	}
     	}
  	} else {
		$sort = "Sell.pay_datetime"; 
	}
	
	$ad->InitAd($smarty, $_GET);  		
	
	
	if(isset($_GET['type_ad']) && intval($_GET['type_ad']) > 1) {
		
		$aLink[1] = '';
		
		$aLink[2] = 'searchbuy.php';
		$aLink[3] = 'searchequipment.php';
		$aLink[4] = 'searchequipment.php';
		$aLink[5] = 'detailssearchroom.php';
		$aLink[6] = 'detailssearchroom.php';
		$aLink[7] = 'detailssearchroom.php';
		$aLink[8] = 'searchinvestments.php';
		$aLink[9] = 'searchcoop.php';
		
		header('Location: http://www.bizzona.ru/'.(isset($aLink[intval($_GET['type_ad'])])  ? $aLink[intval($_GET['type_ad'])].'?'.$_SERVER['QUERY_STRING'] : $aLink[1]));
	}
	
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
  	<HEAD>
      	<title>������� �������, ������� �������� ������� - ��� "������ ����"</title>		
		<META NAME="Description" CONTENT="������� �������, ������� �������, ������� ������, ������� �������, ������� �������� �������, ������� �������, ������� ��������,  ������� ��������, ������� ��������">
        <meta name="Keywords" content="������� �������, ������� �������, ������� ������, ������� �������, ������� �������� �������, ������� �������, ������� ��������,  ������� ��������, ������� ��������, �����������, �������� � ��������, ���������, ����������, �����������"> 
        <link rel="shortcut icon" href="./images/favicon.ico">
        <LINK href="http://www.bizzona.ru/general.css" type="text/css" rel="stylesheet">
        <meta http-equiv="content-type" content="text/html; charset=windows-1251"/>
		<script type="text/javascript" src="http://www.bizzona.ru/ja.js"></script>
  	</HEAD>

<?
  $smarty->display("./site/headerbanner.tpl");
?>

<table class="w" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="2">
		<?
			$topmenu = GetMenu();
			$smarty->assign("topmenu", $topmenu);	
		
			$link_country = GetSubMenu();
			$smarty->assign("ad", $link_country);
	
			$smarty->caching = false;
			$output_header = $smarty->fetch("./site/header.tpl");
			echo minimizer($output_header);
			unset($output_header);
		?>		
		</td>
	</tr>
	<tr>
		<td width="75%" style="padding-top:1pt;" valign="top">
		<?
			$output_fullsearch = $smarty->fetch("./site/fullsearchext.tpl");
			echo minimizer($smarty->fetch("./site/fullsearchext.tpl"));
			unset($output_fullsearch);
			
			$data = array();
			$dataPage = array();

			if(isset($_GET["fr"])) {
				$data["fr"] = $_GET["fr"];
				$dataPage["fr"] = $data["fr"];
			}
			
			if(isset($_GET["own"])) {
				$data["own"] = $_GET["own"];
				$dataPage["own"] = $data["own"];
			}			
			
  			$data["stopcostf"] = intval($_GET["stopcostf"]);
  			$data["startcostf"] = intval($_GET["startcostf"]);			
  			$dataPage["stopcostf"] = $data["stopcostf"];
			$dataPage["startcostf"] = $data["startcostf"];

  			$data["monthaveturn"] = intval($_GET["monthaveturn"]);			
  			$data["profit"] = intval($_GET["profit"]);
			$dataPage["monthaveturn"] = $data["monthaveturn"];
			$dataPage["profit"] = $data["profit"];
			
			$pagesplit = 10;//GetPageSplit();

			if (isset($_GET["offset"])) {
				$data["offset"] = intval($_GET["offset"]);
			} else {
				$data["offset"] = 0;
			}
			
			if (isset($_GET["rowCount"])) {
				$data["rowCount"] = intval($_GET["rowCount"]);
			} else {
				$data["rowCount"] = $pagesplit;
			}

			$data["sort"] = $sort;

			if (isset($_GET["CategoryID"]) && strlen($_GET["CategoryID"]) > 0) {
				$data["CategoryID"] = $_GET["CategoryID"];
				$dataPage["CategoryID"] = $_GET["CategoryID"];
			}

			if (isset($_GET["CityID"]) && strlen($_GET["CityID"]) > 0) {
				$data["CityID"] = $_GET["CityID"];
				$dataPage["CityID"] = $_GET["CityID"];
			}

			$dataPage["offset"] = 0;
			$dataPage["rowCount"] = 0;
			$dataPage["sort"] = $sort;
			$dataPage["StatusID"] = "-1";
			$resultPage = $sell->CountSelectFull($dataPage, false);

			if (sizeof($resultPage) > 0) {

				$smarty->assign("CountRecord", $resultPage);
				$smarty->assign("CountSplit", $pagesplit);
				$smarty->assign("CountPage", ceil($resultPage/10));

				$data["StatusID"] = "-1";
				$data["sort"] = $sort;

				$result = $sell->SelectFull($data, false);
				
				$smarty->assign("data", $result);

				?>
					<div style="width:auto;">
						<div  style="background-image:url(images/bl.gif); background-repeat:repeat-x;">&nbsp;</div>
					</div>
				<?
				
				$saveme = $smarty->fetch("./site/saveme.tpl");
				echo $saveme;  
				
				$output_fullpagesplit = $smarty->fetch("./site/fullpagesplit.tpl");
				echo minimizer($output_fullpagesplit);

                $smarty->assign('colorsell', FrontEnd::ListColorSet());

                $output_listsell = $smarty->fetch("./site/listsell.tpl");
				echo minimizer($output_listsell);
				?>
					<div style="width:auto;">
						<div  style="background-image:url(images/bl.gif); background-repeat:repeat-x;">&nbsp;</div>
					</div>
				<?
				echo minimizer($output_fullpagesplit);
				
				
			} else {
				?><h1 align='center' class='listMessage' style='font-size:11pt;font-family:verdana;color:#7C3535;'><b>�� ������ ������� ������ �������� ������� �� �������</b></h1><?
			}

			unset($data);
			unset($dataPage);
		?>
		
		<?
			include_once("./yadirect.php");
		?>

<div class="adv">
<h3>���� ������������  � ������� ��� ������� �������:</h3>
<ol>
<li>	���������� ������� ��� ������� ������� ���������� ����� � ���������� ����������� �������� ��������� �� ������� �������� �������. 
<li>	���������� ���������� � ������� �������� ������� � �������� ������� � �<b>������� ������</b>�
<li>	�������������� �������� ����������� �� ������� ������� ������������� ����������� �������� �������.
<li>	���������� ��������� ���������� ������� ��� ������� �������.
<li>	������ ���������� �����������. ���������� ������ ����� ��� � ������ �������. ��� �������� �������. ������� ���������� �� ������. �� ��������� ���������.
<li>	������ ������� � ����� �������� ���������� ������� � ��������� ��������.
<li>	��� ����������� ���������� � ��� ������ ����������� ����� ��  ������� ��� ������� ������ ��������.
<li>	��������� ������� ��������� �����������.
<li>	������� �������������. <b>������ ������� � ��� �� ������ ����� ��� �������!</b>
<li>	�� ������������� ������ ����� �� �������� �����, ����������� ����� �������� ������ ��������� ���������� ������� �� �������.
<li>	˸���� ������ �� ���������� ���������� � ������� ��� ������� �������. ��� ���������� ����� ���� ��������� ��� ���������. ������ �������� � ������� ������ ��������. ����� �� ������.
<li>	������ ������� - �����  �������� ��� ����� 5 ��� � �������������� ���� ��� ����� �������� ������.
</ol>
</div>

<?
	$output_mework = $smarty->fetch("./site/sell-howmework.tpl");
	echo fminimizer($output_mework);
	unset($output_mework);
?>

<?
    $output_articles = $smarty->fetch("./site/articles.tpl");
    echo fminimizer($output_articles);
    unset($output_articles);
?>

<?
	$output_topicblog = $smarty->fetch("./site/topicblog2.tpl");
    echo minimizer($output_topicblog);   	 
    unset($output_topicblog);
?>

<?
	$smarty->caching = true; 
	$smarty->cache_lifetime = 10500;
					
	if (!$smarty->is_cached('./site/sugequipment.tpl', 'fullsearch')) {
           
		$datasugequipment = array();
		$datasugequipment["offset"] = 0;
		$datasugequipment["rowCount"] = 3;
		$datasugequipment["sort"] = "datecreate";
		$datasugequipment["status_id"] = "-1";
            	 
		$res_datasugequipment = $equipment->SugSelect($datasugequipment);
  	          	
		$smarty->assign("datasugequipment", $res_datasugequipment);        	
		
	}				

	$output_sugequipment = $smarty->fetch("./site/sugequipment.tpl", 'fullsearch');
	echo fminimizer($output_sugequipment);
	unset($output_sugequipment);
					
	$smarty->caching = false; 					
?>

		</td>
		<td width="25%" valign="top" style="padding-top:1pt;"> 

        <?
        	$smarty->caching = false; 
			echo fminimizer($smarty->fetch("./site/inner_bannertop_right.tpl"));
		?> 		
		
			<?
				$output_request = $smarty->fetch("./site/request.tpl");
				echo minimizer($output_request);
				unset($output_request);            	
				
				$smarty->caching = true; 
				$smarty->cache_lifetime = 300;
				$smarty->cache_dir = "./persistentcache";

                                RightBlockTopSell();

            $smarty->caching = false;
				//$smarty->display("./site/begun.tpl");
            	$datahotbuyer = array();
            	$datahotbuyer["offset"] = 0;
            	$datahotbuyer["rowCount"] = 4;
            	$datahotbuyer["sort"] = "datecreate";
            	$datahotbuyer["StatusID"] = "-1";
            	
  	          	$res_datahotbuyer = $buyer->Select($datahotbuyer);
    			$smarty->assign("databuyer", $res_datahotbuyer);
    			
    			unset($datahotbuyer);
    			unset($res_datahotbuyer);
            	
   	  		    $output_blockbuyer = $smarty->fetch("./site/blockbuyer.tpl");
		  	    echo minimizer($output_blockbuyer);
		  	    unset($output_blockbuyer);

        		$list_select_by_price = $smarty->fetch("./site/select_by_price.tpl");
        		echo fminimizer($list_select_by_price);
				unset($list_select_by_price);

				$smarty->caching = true; 
				$smarty->cache_lifetime = 3600;
				
				if (!$smarty->is_cached('./site/hotequipment.tpl')) {
            
            		$datahotequipment = array();
            		$datahotequipment["offset"] = 0;
            		$datahotequipment["rowCount"] = 4;
            		$datahotequipment["sort"] = "datecreate";
            		$datahotequipment["status_id"] = "-1";
            	
  	          		$res_datahotequipment = $equipment->Select($datahotequipment);
  	          	
    				$smarty->assign("dataequipment", $res_datahotequipment);

              	}
             
				$output_hotequipment = $smarty->fetch("./site/hotequipment.tpl");
				echo minimizer($output_hotequipment);
				unset($output_hotequipment);
 
				$output_proposal = $smarty->fetch("./site/proposal.tpl");
				echo minimizer($output_proposal);
				unset($output_proposal);
				
                                echo $review->LastBlockReview();
                                
				$smarty->caching = false; 
				
				$list_countries_box = $smarty->fetch("./site/list_countries_box.tpl");
				echo fminimizer($list_countries_box);
				unset($list_countries_box);
				
				$smarty->cache_dir = "./cache";
			?>
		</td>
    </tr>
	<tr>
		<td colspan=2>

<style> td a  {   color:black; }</style>
<div style="padding-top:4pt;padding-bottom:5pt;padding-right:10pt;padding-left:10pt;font-size:10pt;font-family:arial;">
	<table cellpadding="0" cellspacing="0" border="0"  style="width:100%;padding-bottom:2pt;" bgcolor="#eeeee0">
    	<tr>
        	<td style="background-image:url(<?=$_SERVER["host_name"]?>images/d1.gif); background-repeat:no-repeat;width:5px;height:5px; background-position:left top;">&nbsp;</td>
            <td rowspan="2" valign="middle" align="center"  style="font-size:8pt; font-family:Arial; color:black;">
             <a href="http://www.bizzona.ru/prodazha-biznesa-recreation-center.php" title="������� ���� ������">������� ���� ������</a>&nbsp;
             <a href="http://www.bizzona.ru/prodazha-biznesa-restoran.php" title="������� ���������">������� ���������</a>&nbsp;
            </td>
            <td style="background-image:url(<?=$_SERVER["host_name"]?>images/d2.gif); background-repeat:no-repeat;width:5px;height:5px;background-position:right top;">&nbsp;</td>
        </tr>
        <tr>
            <td style="background-image:url(<?=$_SERVER["host_name"]?>images/d4.gif); background-repeat:no-repeat;width:5px;height:5px; background-position:left bottom;">&nbsp;</td>
            <td style="background-image:url(<?=$_SERVER["host_name"]?>images/d3.gif); background-repeat:no-repeat;width:5px;height:5px; background-position:right bottom;">&nbsp;</td>
        </tr>
    </table>
</div>
<?
	$subdomain_city = $smarty->fetch("./site/subdomain_city.tpl");
	echo fminimizer($subdomain_city);
	unset($subdomain_city);

	$smarty->display("./site/footer.tpl");
?>
		</td>
	</tr>		
</table>
<?
  $end = get_formatted_microtime(); 
  $total = $end - $start;
  echo "<center><span style='font-size:7pt;'>".round($total, 6)." : ".$count_sql_call." : ".$base_memory_usage." : ".memoryUsage(memory_get_usage(), $base_memory_usage)." </span><center>";
  unset($end);
  unset($total);
  unset($count_sql_call);
?>
</body>
</html>
<?
	unset($DATABASE);

	$sell->Close();
	$category->Close();
	$city->Close();
	$country->Close();
	$buyer->Close();
	$equipment->Close();
	$metro->Close();
	$district->Close();
	$subcategory->Close();
	$streets->Close();
?>