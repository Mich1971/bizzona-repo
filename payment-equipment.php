<?php
	global 	$DATABASE,
			$DEFAULT_COUNTRY_ID,
			$DEFAULT_LANGUAGE_ID;
	
  	include_once("./phpset.inc");
  	include_once("./engine/functions.inc");

  	AssignDataBaseSetting();

	ini_set("display_startup_errors", "off");
	ini_set("display_errors", "off");
	ini_set("register_globals", "on");
	
	include_once("./engine/class.equipment.inc");
	$equipment = new Equipment();

  	require_once("./libs/Smarty.class.php");
	$smarty = new Smarty;
	$smarty->template_dir = "./templates";
	$smarty->compile_dir  = "./templates_c";
	$smarty->cache_dir = "./cache";
	$smarty->compile_check = true;
	$smarty->debugging     = false;

	$equipment->id = intval($_GET["Id"]);
	$res = $equipment->GetItem($data);
    $smarty->assign("Id", $data["ID"]);	
	
	$smarty->assign("fml", $res->contact_face); 
	$smarty->assign("email", $res->email);
			
	$secret_code = "vbhjplfybt";
	$purse = "4146"; // sms:bank id        ������������� ���:�����
	$order_id = 6000000 + $equipment->id; //  ������������� ��������
	$amount = "3"; // transaction sum  ����� ����������
	$clear_amount  = "0"; // billing algorithm  �������� �������� ���������
	$description  = "������ ���������� ����������"; // operation desc �������� ��������
	$submit  = "���������"; // submit label ������� �� ������ submit
	
	$sign = ref_sign($purse, $order_id, $amount, $clear_amount, $description, $secret_code);
	
	
	$smarty->assign("purse", $purse); 
	$smarty->assign("order_id", $order_id); 
	$smarty->assign("rbk_order_id", $equipment->id); 
	$smarty->assign("amount", $amount); 
	$smarty->assign("clear_amount", $clear_amount);
	$smarty->assign("description", $description); 
	$smarty->assign("submit", $submit);
	$smarty->assign("sign", $sign);
	
	//email

	$money_message = cp1251_to_utf8("������ ����� ���������� ����������� �� ����� bizzona.ru (".$order_id.") ");
	$money_message = base64_encode($money_message);
	$smarty->assign("money_message", $money_message);	
	
	$payment = $smarty->fetch("./site/payment-equipment.tpl");
	echo $payment;
?>
