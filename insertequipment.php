<?php
    global $DATABASE;
  
    include_once("./phpset.inc");
  
    include_once("./engine/functions.inc"); 
    
    AssignDataBaseSetting();
    
    include_once("./engine/class.category.inc"); 
    $category = new Category();

    include_once("./engine/class.subcategory.inc"); 
    $subCategory = new SubCategory();  
  
    include_once("./engine/class.equipment.inc"); 
    $equipment = new Equipment();
  
    include_once("./engine/class.insert_settings.inc"); 
    $insertSettings = new InsertSettings();  
    
    $insertSettings->type_insert = ConstInsertSettings::EQUIPMENT;
    $insertSettingsData = $insertSettings->GetItem();
  
    include_once('./inserthelp.php');  
  
    //ini_set("display_startup_errors", "on");
    //ini_set("display_errors", "on"); 
    //ini_set("register_globals", "on");
    
    
    include_once("./engine/class.BlockIP.inc"); 
    $blockIp = new BlockIP();

    $blocking  = $blockIp->YouAreBlocking(array(
        'ip' => $_SERVER['REMOTE_ADDR']
    ));  
  
    require_once("./libs/Smarty.class.php");
    $smarty = new Smarty;
    $smarty->template_dir = "./templates";
    $smarty->compile_dir  = "./templates_c";
    //$smarty->cache_dir = "./cache";
    $smarty->compile_check = true;
    $smarty->debugging     = false;

    $smarty->register_function('NameCity', 'getNameCity');

    require_once("./regfuncsmarty.php");   
    
    $assignDescription = AssignDescription();

    $text_error = "";

    if (isset($_POST) && sizeof($_POST) > 0 && !$blocking) {
	
        $ErrorFile = false;

        $file_icon = "";
    
        if (isset($_FILES["icon"])) {
            $file_icon = LoadClientImg("icon", 1, 'equipment_');
        } 
  	
        if (!$ErrorFile) {

            $t_name = htmlspecialchars(trim($_POST["name"]), ENT_QUOTES);
            $_POST["name"] = $t_name;

            $t_age = htmlspecialchars(trim($_POST["age"]), ENT_QUOTES);
            $_POST["age"] = $t_age;

            $t_cost = htmlspecialchars(trim($_POST["cost"]), ENT_QUOTES);
            $_POST["cost"] = $t_cost;

            $t_ccost = htmlspecialchars(trim($_POST["ccost"]), ENT_QUOTES);
            $_POST["ccost"] = $t_ccost;

            $t_category_id = htmlspecialchars(trim($_POST["category_id"]), ENT_QUOTES);
            $_POST["category_id"] = $t_category_id;

            $total_typeBizID = htmlspecialchars(trim($_POST["category_id"]), ENT_QUOTES);
            $arr_typeBizID = split("[:]",$total_typeBizID);
            
            if(sizeof($arr_typeBizID) == 2) {
                
                $data["category_id"] = intval($arr_typeBizID[0]);
                $data["subCategory_id"] = intval($arr_typeBizID[1]);
                $_POST["subCategory_id"] = $data["subCategory_id"];
                $t_category_id = $data["category_id"];
                $t_subCategory_id = $data["subCategory_id"];
                
            } else {

                $data["category_id"] = "-1";
                $data["subCategory_id"] = "-1";
                $t_category_id = $data["category_id"];
                $t_subCategory_id = $data["subCategory_id"];
            }

            $t_description = htmlspecialchars(trim($_POST["description"]), ENT_QUOTES);
            $_POST["description"] = $t_description; 

            $t_city_id = "";
            $t_city_text = htmlspecialchars(trim($_POST["city_text"]), ENT_QUOTES);
            $_POST["city_text"] = $t_city_text;

            $t_icon = $file_icon;
            $t_email = htmlspecialchars(trim(strtolower($_POST["email"])), ENT_QUOTES);
            $_POST["email"] = $t_email;

            $t_contact_face = htmlspecialchars(trim($_POST["contact_face"]), ENT_QUOTES);
            $_POST["contact_face"] = $t_contact_face;

            $t_phone = htmlspecialchars(trim($_POST["phone"]), ENT_QUOTES);
            $_POST["phone"] = $t_phone;

            $t_youtubeURL = htmlspecialchars(trim($_POST["youtubeURL"]), ENT_QUOTES);
            $_POST["youtubeURL"] = $t_youtubeURL;

            $t_status_id = "";
            $t_datecreate = "";

            $t_statussend_id = "";
            $t_qview = "";     

            if (strlen($t_name) <= 0) {

            $text_error = "������ �����: "."\"�������� ������������\"";
            $smarty->assign("ErrorInput_name", true);

        } else if(strlen($t_cost) <= 0) {

            $text_error = "������ �����: "."\"���������\"";
            $smarty->assign("ErrorInput_cost", true);

            } else if(strlen($t_city_text) <= 0) {

                    $text_error = "������ �����: "."\"������������\"";
                    $smarty->assign("ErrorInput_city_text", true);

            } else if (strlen($t_description) <= 0) {

                    $text_error = "������ �����: "."\"������� ��������\"";
                    $smarty->assign("ErrorInput_description", true);

            } else if (strlen($t_contact_face) <= 0) {

                    $text_error = "������ �����: "."\"���������� ����\"";
                    $smarty->assign("ErrorInput_contact_face", true);

            } else if(strlen($t_phone) <= 0) {

                    $text_error = "������ �����: "."\"��������\"";
                    $smarty->assign("ErrorInput_phone", true);

            } else if(strlen($t_email) <= 0) {

                    $text_error = "������ �����: "."\"Email\"";
                    $smarty->assign("ErrorInput_email", true);
            }


            if(strlen($text_error) > 0)	{
                    $smarty->assign("text_error", $text_error);
            }

            if (strlen($text_error) <= 0) {

                $equipment->name = $t_name;
                $equipment->age = $t_age;
                $equipment->cost = $t_cost;
                $equipment->icon = $t_icon;
                $equipment->ccost = $t_ccost;
                $equipment->category_id = $t_category_id;
                $equipment->subCategory_id = $t_subCategory_id;


                $equipment->description = $t_description;
                $equipment->city_text = $t_city_text;
                $equipment->email = $t_email;
                $equipment->contact_face = $t_contact_face;
                $equipment->phone = $t_phone;
                $equipment->duration_id = 1;
                $equipment->youtubeURL = $t_youtubeURL;

                if(isset($_SESSION["partnerid"])) {
                    $equipment->partnerid = intval($_SESSION["partnerid"]);
                }

                if(isset($_SESSION["referer"])) {
                    $equipment->source  = htmlspecialchars(trim($_SESSION["referer"]), ENT_QUOTES);
                } else {
                    $equipment->source = "";
                }

                $mysql_insert_id = $equipment->Insert();
                $smarty->assign("code", $mysql_insert_id);
                $_SESSION['timepost'] = time();


                $secret_code = "vbhjplfybt"; 
                $purse = "4146"; // sms:bank id        ������������� ���:�����
                $order_id = 6000000 + intval($mysql_insert_id); //  ������������� ��������
                $amount = "3"; // transaction sum  ����� ����������
                $clear_amount  = "0"; // billing algorithm  �������� �������� ���������
                $description  = "������ ���������� ����������"; // operation desc  �������� ��������
                $submit  = "���������"; // submit label    ������� �� ������ submit

                $sign = ref_sign($purse, $order_id, $amount, $clear_amount, $description, $secret_code);

                $m_description = cp1251_to_utf8($description."  (".$order_id.") ");
                $m_description = base64_encode($m_description);  			

                $smarty->assign("purse", $purse);
                $smarty->assign("order_id", $order_id);
                $smarty->assign("rbk_order_id", $order_id-6000000);
                $smarty->assign("amount", $amount);
                $smarty->assign("clear_amount", $clear_amount);
                $smarty->assign("description", $description);
                $smarty->assign("m_description", $m_description);
                $smarty->assign("submit", $submit);
                $smarty->assign("sign", $sign);
                $smarty->assign("email", $equipment->email);


                $smarty->assign("text_success", "�������� ������������ ���� ������� ���������. � �������� ���� ���������� ����� ����������.");


                $to      = 'bizzona.ru@gmail.com';
                $subject = 'new equipment: ';
                $message = "�������� ������������: ".$equipment->name."\n";
                $message.= "������������ ��������������: ".$equipment->age."\n";
                $message.= "���������: ".$equipment->cost."\n";
                $message.= "����������: ".$equipment->icon."\n";
                $message.= "��� ������: ".$equipment->ccost."\n";
                $message.= "��� ������������: ".$equipment->category_id."\n";
                $message.= "������� ��������: ".$equipment->description."\n";
                $message.= "������������: ".$equipment->city_text."\n";
                $message.= "Email: ".$equipment->email."\n";
                $message.= "���������� ����: ".$equipment->contact_face."\n";
                $message.= "��������: ".$equipment->phone."\n";
                $message.= "������������: ".$equipment->duration_id."\n";
                $headers = 'BizZONA.RU: new equipment' . "\r\n" .
                'Reply-To: '.$data["EmailID"].' ' . "\r\n" .
                'X-Mailer: PHP/' . phpversion();
                mail($to, $subject, $message, $headers);

                unset($_POST);
            }
        }
    } 
 
    $data["offset"]   = 0;
    $data["rowCount"] = 0;
    $data["sort"]= "Count";
    $listCategory = $category->Select($data); 

    unset($data["sort"]);
    $data["offset"]   = 0;
    $data["rowCount"] = 0;
    //$listCity = $city->Select($data); 

    $smarty->assign("listCategory", $listCategory);
    //$smarty->assign("listCity", $listCity);

    $listSubCategory = $subCategory->Select($data);
    $smarty->assign("listSubCategory", $listSubCategory);
  
    include_once("./engine/class.ContactInfo_Lite.inc");
    $contact = new ContactInfo_Lite();  

    $contact->GetContactInfo();  
    $smarty->assign("contact", $contact);  
    
    $smarty->assign("insertsettings", $insertSettingsData);  
?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>

  <HEAD>
      <title>���������� ������� ������������ - ��� "������-����"</title>
	<META NAME="Description" CONTENT="������� ������������, ������� �������, ������� �������, ������� ������, ������� �������, ������� �������� �������, ������� �������, ������� ��������,  ������� ��������, ������� ��������, ���� ������� ������?">
        <meta name="Keywords" content="������� ������������, ������� �������, ������� �������, ������� ������, ������� �������, ������� �������� �������, ������� �������, ������� ��������,  ������� ��������, ������� ��������, ���� ������� ������?, Ready business, �����������, �������� � ��������, ���������, ����������, �����������"> 
	<meta http-equiv="content-type" content="text/html; charset=windows-1251"/>
        <LINK href="./general.css" type="text/css" rel="stylesheet">
  </HEAD>


<?
    $topmenu = GetMenu();
    $smarty->assign("topmenu", $topmenu);

    $link_country.= "&nbsp;&nbsp;<a href='".NAMESERVER."help.php' class='city_a'  style='color:white;font-size:10pt;color:yellow;' title='������ - ������� �������' target='_blank'>������</a>&nbsp;&nbsp;";	
    $smarty->assign("ad", $link_country);

    $smarty->display("./site/headere.tpl");
?>
  
<table class="w" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td width="100%">
         <table width='100%' height='100%'>
           <tr>
             <td valign='top' width='60%' height='100%'>
               <?
                  if($blocking) {
                    $smarty->display("./site/blockip.tpl");  
                  } else {
                    $smarty->display("./site/insertequipment.tpl");    
                  }  
               ?>
             </td>
           </tr>
         </table>
        </td>
    </tr>
</table>

<?
    $smarty->display("./site/footer.tpl");
?>

</body>
</html>