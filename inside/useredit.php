<?php
/**
 *
 * @author Alexander
 */

define('ADMINLIST', 'TRUE');
// include the configs / constants for the database connection
require_once("classes/db.config.php");

// load the registration class
require_once("classes/Registration.class.php");
require_once("classes/UserList.class.php");

require_once($_SERVER['DOCUMENT_ROOT'].'/inside/auth.php');

// ����� ������� � �������
if(!$login->isAdmin('ADMIN, MODER')){
	exit('No Privileges to User Edit. No access allowed');
}

// create the user list object
$userlist = new UserList();
if (isset($_POST['register'])) {
	if (!isset($_POST['user_id'])) {return false;}
		$userlist->getUserData($_POST['user_id']);

	if (isset($_POST['user_level'])) {
		$userlist->set_user_level($_POST['user_id'], $_POST['user_level']);
	}

	if (isset($_POST['user_active'])) {
		if($_POST['user_active']==1) {
			$userlist->activate_user($_POST['user_id']);
		}
	} else {
			$userlist->deactivate_user($_POST['user_id']);
	}
// change email
	if (isset($_POST['user_email'])) {
		$userlist->getUserData($_POST['user_id']);
		if($userlist->usrdata['user_email'] != $_POST['user_email']) {
			$userlist->user_change_email($_POST['user_id']);
		}
	}

// change password
// checkbox user_psw_change
	if (isset($_POST['user_psw_change'])) {
		$userlist->user_change_password($_POST['user_id']);
	}

// redirect
		$host  = $_SERVER['HTTP_HOST'];
		$uri   = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
		$extra = 'userlist.php';
		header("Location: http://$host$uri/$extra");
	    exit();
}
if (isset($_POST['cancel'])) {
		$host  = $_SERVER['HTTP_HOST'];
		$uri   = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
		$extra = 'userlist.php';
		header("Location: http://$host$uri/$extra");
	    exit();
}

if(isset($_GET['user_id'])) {
	$userlist->getUserData($_GET['user_id']);
// show the register view (with the registration form, and messages/errors)
include("views/vuseredit.php");
} else {
	exit('No $_GET parameters');
}

?>