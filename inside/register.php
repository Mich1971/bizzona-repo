<?php
/**
 *
 * @author Alexander
**/

define('ADMINLIST', 'TRUE');

// include the configs / constants for the database connection
require_once("classes/db.config.php");

// load the registration class
require_once("classes/Registration.class.php");

require_once($_SERVER['DOCUMENT_ROOT'].'/inside/auth.php');

// ����� ������� � �������
if(!$login->isAdmin('ADMIN, MODER')){
	exit('No Privileges to Register new User. No access allowed');
}

// create the registration object.
$registration = new Registration();

if (isset($_POST["register"])) {
		$host  = $_SERVER['HTTP_HOST'];
		$uri   = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
		$extra = 'userlist.php';
		header("Location: http://$host$uri/$extra");
	    exit();
}
// show the register view (with the registration form, and messages/errors)
include("views/vregister.php");
?>
