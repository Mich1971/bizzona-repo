<?php
/**
 * Edit users for PHP Login Script
 *
 * @author Alexander
 */
if ( !defined('ADMINLIST')) exit('No direct script access allowed');
header("Content-Type: text/html;charset=windows-1251");
?>
<!DOCTYPE html>
<html>
  <head>
    <title>�������������� ������������</title>
    <meta charset="windows-1251" />
	<link href="login_users.css" media="all" rel="stylesheet" type="text/css" />
	<script src="../js/jquery.min.js"></script>

<script>
$(document).ready(function() {
$('#login_psw_change').click( function () {
	if ($(this).is(':checked')) {
			$('#login_input_password_new').removeAttr('disabled');
			$('#login_input_password_repeat').removeAttr('disabled');
		} else {
			$('#login_input_password_new').val('').attr('disabled', true);
			$('#login_input_password_repeat').val('').attr('disabled', true);
	}
});

});
</script>
  </head>
<body>


<?php

// show potential errors / feedback (from registration object)
if (isset($registration)) {
    if ($registration->errors) {
        foreach ($registration->errors as $error) {
            echo $error;
        }
    }
    if ($registration->messages) {
        foreach ($registration->messages as $message) {
            echo $message;
        }
    }
}
/*
include_once($_SERVER['DOCUMENT_ROOT'].'/biz/dlog.php');
dlog($registration->errors, '$registration->errors');
dlog($registration->messages, '$registration->messages');
*/
// include('displaypost.php');
?>

<div id="register" class="container">
<div class="shadowhead">
	<h2>�������������� ������������.</h2>
</div>
</div>
<div id="register" class="container">
<!-- register form -->
<form method="post" action="useredit.php" name="registerform" id="login">
<fieldset>
    <input id="login_input_userid" type="hidden" name="user_id" value="<? echo $userlist->usrdata['user_id'];?>"/>

	<legend> &nbsp;����� ��������������&nbsp;</legend>
    <!-- the user name input field uses a HTML5 pattern check -->
<p>
    <label for="login_input_username">��� ������������: <!--(������ ����� � �����, �� 2 �� 64 ��������) --></label> <br />
    <input id="login_input_username" class="textbox" type="text" pattern="[a-zA-Z0-9]{2,64}" name="user_name" placeholder="adminuser" autofocus required value="<? echo $userlist->usrdata['user_name'];?>"/><br>
</p>
<p>
    <!-- the email input field uses a HTML5 email type check -->
    <label for="login_input_email">Email ������������</label>
    <input id="login_input_email" class="textbox" type="email" name="user_email" placeholder="admin@bizzona.ru" required value="<? echo $userlist->usrdata['user_email'];?>"/>
</p>

<p>
    <!-- the user level input field -->
<?php
    //var_dump($userlist->list_level);
/*
echo $userlist->list_level[0];
echo $userlist->list_level[1];
echo $userlist->list_level[2];
*/
?>
    <label for="login_input_level">����� �������</label>
<select id="login_input_level" class="textbox" name="user_level">
<?php
if($userlist->list_level) {
	foreach ($userlist->list_level as $val => $option){
		if($val==$userlist->usrdata['user_level']) {$select = "selected";} else {$select = "";}
		echo "<option value=\"{$val}\" {$select} >";
		echo $option;
		echo "</option>\n";
	}
}
?>
</select>
</p>

<?php
$usr_active = ($userlist->usrdata['user_active'] > 0 ? 'checked': '');
?>
<p>
    <!-- the user activate input field -->
    <input id="login_input_active" type="checkbox" name="user_active" <? echo $usr_active;?> value="1"/>
    <label for="login_input_active">��������� ������������</label>
	<br><br>
</p>

<p>
    <input id="login_psw_change" type="checkbox" name="user_psw_change" value="1"/ title="�������� ������ ������������">
    <label for="login_psw_change" title="�������� ������ ������������">������ (�� ������ 6 ��������)</label>
    <input disabled id="login_input_password_new" class="textbox" type="password" name="user_password_new" pattern=".{6,}" placeholder="������� ������" autocomplete="off" />
</p>
<p>
    <label for="login_input_password_repeat">��������� ������</label>
    <input disabled id="login_input_password_repeat" class="textbox" type="password" name="user_password_repeat" pattern=".{6,}" placeholder="��� ��� ������" autocomplete="off" />
</p>
<p>
    <input type="submit"  id="submit" class="button" name="register" value="��������� ���������" />
</p>
<p>
    <input type="submit"  id="cancel" class="button" name="cancel" value="��������" />
</p>

    </fieldset>
</form>

</div>
<!-- backlink -->
</body>
</html>
