<?php
/**
 * Register users for PHP Login Script
 *
 * @author Alexander
 */
if ( !defined('ADMINLIST')) exit('No direct script access allowed');
header("Content-Type: text/html;charset=windows-1251");
?>
<!DOCTYPE html>
<html>
  <head>
    <title>����������� ������ ������������</title>
    <meta charset="windows-1251" />
	<link href="login_users.css" media="all" rel="stylesheet" type="text/css" />
  </head>
<body>


<?php

// show potential errors / feedback (from registration object)
if (isset($registration)) {
    if ($registration->errors) {
        foreach ($registration->errors as $error) {
            echo $error;
        }
    }
    if ($registration->messages) {
        foreach ($registration->messages as $message) {
            echo $message;
        }
    }
}
/*
include_once($_SERVER['DOCUMENT_ROOT'].'/biz/dlog.php');
dlog($registration->errors, '$registration->errors');
dlog($registration->messages, '$registration->messages');
*/
// include('displaypost.php');
?>

<div id="register" class="container">
<div class="shadowhead">
	<h2>����������� ������ ������������.</h2>
</div>
</div>
<div id="register" class="container">
<!-- register form -->
<form method="post" action="register.php" name="registerform" id="login">
<fieldset>
	<legend> &nbsp;����� �����������&nbsp;</legend>
    <!-- the user name input field uses a HTML5 pattern check -->
<p>
    <label for="login_input_username">��� ������������: <!--(������ ����� � �����, �� 2 �� 64 ��������) --></label> <br />
    <input id="login_input_username" class="textbox" type="text" pattern="[a-zA-Z0-9]{2,64}" name="user_name" placeholder="adminuser" autofocus required /><br>
</p>
<p>
    <!-- the email input field uses a HTML5 email type check -->
    <label for="login_input_email">Email ������������</label>
    <input id="login_input_email" class="textbox" type="email" name="user_email" placeholder="admin@bizzona.ru" required />
</p>
<p>
    <label for="login_input_password_new">������ (�� ������ 6 ��������)</label>
    <input id="login_input_password_new" class="textbox" type="password" name="user_password_new" pattern=".{6,}" placeholder="������� ������" required autocomplete="off" />
</p>
<p>
    <label for="login_input_password_repeat">��������� ������</label>
    <input id="login_input_password_repeat" class="textbox" type="password" name="user_password_repeat" pattern=".{6,}" placeholder="��� ��� ������" required autocomplete="off" />
</p>
    <input type="submit"  id="submit" class="button" name="register" value="�����������" />

    </fieldset>
</form>

</div>
<!-- backlink -->
</body>
</html>
