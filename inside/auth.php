<?php
// ��������� � ������ ������ ��� ������� � �������� ��������� �����������.
// ������: require_once($_SERVER['DOCUMENT_ROOT'].'/inside/auth.php');
// �������� ������� ���� � ����� ����� ������ ����������� ����������� �������
// � ������ �����������

// include the configs / constants for the database connection
require_once($_SERVER['DOCUMENT_ROOT'].'/inside/classes/db.config.php');

// load the login class
require_once($_SERVER['DOCUMENT_ROOT'].'/inside/classes/Login.class.php');

$login = new Login();

if ($login->isUserLoggedIn() == false) {
		$host  = $_SERVER['HTTP_HOST'];
		// $uri   = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
		$uri   = '/inside';
		$extra = 'index.php';
		header("Location: http://$host$uri/$extra");
	    exit();
}
?>