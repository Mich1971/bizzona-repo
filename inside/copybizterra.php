<?php
	global $DATABASE;

	ini_set("display_startup_errors", "on");
	ini_set("display_errors", "on");
	ini_set("register_globals", "on");
	
	
	include_once("../../bizterra.ru/phpset.inc");
	include_once("../../bizterra.ru/engine/functions.inc");
	AssignDataBaseSetting("../../bizterra.ru/config.ini");

	//ini_set("display_startup_errors", "on");
	//ini_set("display_errors", "on");
	//ini_set("register_globals", "on");

	
	include_once("../engine/class.clonesite.inc");
	$clonesite = new CloneSite();

	
	$clonesite->Id = intval($_GET["Id"]);
	$clonesite->namesite = "bizterra";
	$clonesite->statusId = 1;
	$clonesite->type = trim($_GET["type"]);
    
	if(isset($_GET["statusId"])) {

		$clonesite->statusId = intval($_GET["statusId"]);
		$clonesite->ChangeStatusToBizterra();
		
		echo "Update status to Bizterra.ru";
		
	} else {
		
		$clonesite->ExportToBizterra();	
		echo "Exporting to Bizterra.ru";
	}
	
?>
