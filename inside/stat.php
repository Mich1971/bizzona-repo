<?php
    global $DATABASE;
    include_once("../phpset.inc");
    include_once("../engine/functions.inc");
    AssignDataBaseSetting("../config.ini");
// ��������� �����������
	require_once($_SERVER['DOCUMENT_ROOT'].'/inside/auth.php');
//

    include_once("../engine/class.trouble.inc");
    $trouble = new Trouble();

    include_once("../engine/class.company.inc");
    $company = new Company();

    $aBloeckedCompany = $company->IdsBlockedCompany();

    require_once("../libs/Smarty.class.php");
    $smarty = new Smarty;
    $smarty->template_dir = "../templates";
    $smarty->compile_dir  = "../templates_c";
    $smarty->cache_dir = "../cache";
    $smarty->compile_check = true;
	$smarty->debugging     = false;
//	$smarty->debugging = true;

    $smarty->clear_all_cache();
    $smarty->clear_compiled_tpl("stat.tpl");

    $smarty->assign("aBloeckedCompany", $aBloeckedCompany);

    //include_once("../engine/class.bzn_items.inc");
    //$bzn = new BZN_Items();
    //$bzn->Import();

    $headers = "Content-type: text/plain; charset=windows-1251;\r\n";

    $sendmessageme = false;
    if (isset($_POST["messageMeID"])) {
            $trouble->answer = "";
            $trouble->message = $_POST["messageMeID"];
            $trouble->Insert();
            $sendmessageme = mail("eugenekurilov@gmail.com","BizZONA.ru - ��������", $_POST["messageMeID"], $headers);
    }

    $arr_troubles = $trouble->Select();

    $smarty->assign("arr_troubles", $arr_troubles);

    include_once("../engine/class.sell.inc");
    $sell = new Sell();

    include_once("../engine/class.stat.inc");
    $stat = new Stat();

    include_once("../engine/class.buy.inc");
    $buyer = new Buyer();

    include_once("../engine/class.investments.inc");
    $investments = new Investments();

    include_once("../engine/class.equipment.inc");
    $equipment = new Equipment();

    include_once("../engine/class.coopbiz.inc");
    $coopbiz = new CoopBiz();

    include_once("../engine/class.Room.inc");
    $room = new Room();

    include_once("../engine/class.review.inc");
    $review = new Review();

    include_once("../engine/class.communication.inc");
    $communication = new Communication();

    $smarty->assign("aComm", $communication->Select(array(
        'limit' => 5,
    )));

    include_once("../engine/class.ApprovingSell.inc");
    $aSell = new ApprovingSell();

    include_once("../engine/class.ControllerWallet.inc");
    $cwallet = new ControllerWallet();

    include_once("../engine/class.Settings.inc");
    $settings = new Settings();


    $smarty->assign("aListOutcome", $cwallet->ListOutcomeAdmin(array(
        'limit' => 10,
    )));

    $smarty->assign("aListIncome", $cwallet->ListIncomeAdmin(array(
        'limit' => 10,
    )));
    $smarty->assign("incometotal", $stat->IncomeMoneyWallet());

    $aCountPeriod = $sell->CountSellPeriod();
    $aCountOwnerPeriod = $sell->CountSellPeriod(0, true);
    $aCountOwnerPaymentPeriod = $sell->CountSellPeriod(0, true, true);
    $aCountSellStatusClosed = $sell->CountSellByStatus();
    $profit = $stat->Profit();
    $smarty->assign('profit', $profit);

    //$percent = $stat->PercentPayment();
    //$smarty->assign('percent', $percent);

    //$objProfitToday = $stat->ProfitToday();
    //$smarty->assign('profittoday', $objProfitToday);

    //$aTarif1 = $sell->CountSellByTarif();
    //$aTarif2 = $sell->CountSellByTarif(0,2);
    //$aTarif3 = $sell->CountSellByTarif(0,3);


    $smarty->assign( 'month', $aCountPeriod );

    foreach ($aCountOwnerPeriod as $k => $v) {

        foreach($aCountOwnerPaymentPeriod as $k1 => $v1) {

            if($v->DateStart == $v1->DateStart) {

                $v->CountPayment = $v1->Count;

                $aCountOwnerPeriod[$k] = $v;
            }

        }

    }

    $smarty->assign( 'monthowner', $aCountOwnerPeriod );
    $smarty->assign( 'monthclosed', $aCountSellStatusClosed );


    $approveSells = $aSell->ListIds();

    if(count($approveSells)) {
        if(count($approveSells) > 5) {
            $a_approveSells = array_chunk($approveSells, (int)(count($approveSells)/4));
        } else {
            $a_approveSells = $approveSells;
        }
        $smarty->assign('aprvSells', $a_approveSells);
    } else {
        $smarty->assign('aprvSells', array());
    }

    //$smarty->assign( 'tarif1', $aTarif1 );
    //$smarty->assign( 'tarif2', $aTarif2 );
    //$smarty->assign( 'tarif3', $aTarif3 );

    //$smarty->assign("statgetseo", $sell->StatGetSeo());

	$countNewSell = $sell->CountStatus(0);
	$smarty->assign("countNewSell", $countNewSell);

	$countPaySell = $sell->CountPayStatus();
	$smarty->assign("countPaySell", $countPaySell);

	if($countPaySell > 0) {
            $listCountPayStatus = $sell->ListCountPayStatus();
            while (list($k, $v) = each($listCountPayStatus)) {
                $listCountPayStatus[$k] = "<a href='/engine/UpdateSell.php?ID=".$v."' style='color:white;'>".$v."</a>";
            }
            reset($listCountPayStatus);
            $smarty->assign("listCountPayStatus", implode(" ", $listCountPayStatus));
	}

	$countNewRoom = $room->CountStatus(0);
	$smarty->assign("countNewRoom", $countNewRoom);

	$countNewReview = $review->CountStatus(0);
	$smarty->assign("countNewReview", $countNewReview);

	$countPayRoom = $room->CountPayStatus();
	$smarty->assign("countPayRoom", $countPayRoom);

	if($countPayRoom > 0) {
            $listCountPayStatusRoom = $room->ListCountPayStatus();
            while (list($k, $v) = each($listCountPayStatusRoom)) {
                $listCountPayStatusRoom[$k] = "<a href='/engine/UpdateRoom.php?ID=".$v."' style='color:white;'>".$v."</a>";

            }
            reset($listCountPayStatusRoom);
            $smarty->assign("listCountPayStatusRoom", implode(" ", $listCountPayStatusRoom));
	}



	$countNewBuyer = $buyer->CountStatus(0);
	$smarty->assign("countNewBuyer", $countNewBuyer);

	$countPatBuyer = $buyer->CountPayStatus();
	$smarty->assign("countPatBuyer", $countPatBuyer);

	if($countPatBuyer > 0) {
            $listCountPayStatusBuyer = $buyer->ListCountPayStatus();
            while (list($k, $v) = each($listCountPayStatusBuyer)) {
                $listCountPayStatusBuyer[$k] = "<a href='/engine/UpdateBuyer.php?ID=".$v."' style='color:white;'>".$v."</a>";
            }
            reset($listCountPayStatusBuyer);
            $smarty->assign("listCountPayStatusBuyer", implode(" ", $listCountPayStatusBuyer));
	}



	$countNewInvestments = $investments->CountStatus(0);
	$smarty->assign("countNewInvestments", $countNewInvestments);

	$countPayInvestment = $investments->CountPayStatus();
	$smarty->assign("countPayInvestment", $countPayInvestment);

	if($countPayInvestment > 0) {
            $listCountPayStatusInvestment = $investments->ListCountPayStatus();
            while (list($k, $v) = each($listCountPayStatusInvestment)) {
                $listCountPayStatusInvestment[$k] = "<a href='/engine/investments_list.php?action=investments&ID=".$v."' style='color:white;'>".$v."</a>";
            }
            reset($listCountPayStatusInvestment);
            $smarty->assign("listCountPayStatusInvestment", implode(" ", $listCountPayStatusInvestment));
	}


	$countNewEquipment = $equipment->CountStatus(0);
	$smarty->assign("countNewEquipment", $countNewEquipment);

	$countPayEquipment = $equipment->CountPayStatus();
	$smarty->assign("countPayEquipment", $countPayEquipment);

	if($countPayEquipment > 0) {
            $listCountPayStatusEquipment = $equipment->ListCountPayStatus();
            while (list($k, $v) = each($listCountPayStatusEquipment)) {
                $listCountPayStatusEquipment[$k] = "<a href='/engine/equipment_list.php?ID=".$v."' style='color:white;'>".$v."</a>";
            }
            reset($listCountPayStatusEquipment);
            $smarty->assign("listCountPayStatusEquipment", implode(" ", $listCountPayStatusEquipment));
	}


	$countCoopbiz = $coopbiz->CountStatus(0);
	$smarty->assign("countCoopbiz", $countCoopbiz);



	$countPayCoop = $coopbiz->CountPayStatus();
	$smarty->assign("countPayCoop", $countPayCoop);

	if($countPayCoop > 0) {
            $listCountPayStatusCoop = $coopbiz->ListCountPayStatus();
            while (list($k, $v) = each($listCountPayStatusCoop)) {
                $listCountPayStatusCoop[$k] = "<a href='/engine/coopbiz_list.php?ID=".$v."' style='color:white;'>".$v."</a>";
            }
            reset($listCountPayStatusCoop);
            $smarty->assign("listCountPayStatusCoop", implode(" ", $listCountPayStatusCoop));
	}


	$today = date("H:i:s");
	$smarty->assign("today", $today);

        $countnew = $countNewRoom + $countNewSell + $countNewInvestments + $countNewBuyer + $countNewEquipment + $countCoopbiz;
        $countpay  = $countPayRoom + $countPaySell + $countPayInvestment + $countPatBuyer + $countPayEquipment + $countPayCoop;

        $_useSound = (int)$settings->GetValue('SOUND');

        if($countpay  && $_useSound) {
//			if(strtolower($_SERVER['SERVER_NAME']) == "bizzona.ru")
		echo "<EMBED SRC='/sound/payment.wav'  HEIGHT=0 WIDTH=0  hidden='true' autostart='true' loop='false'></EMBED>";
        } else if($countnew && $_useSound) {
//			if(strtolower($_SERVER['SERVER_NAME']) == "bizzona.ru")
		echo "<EMBED SRC='/sound/notify.wav'  HEIGHT=0 WIDTH=0  hidden='true' autostart='true' loop='false'></EMBED>";
	}
		// ������� ��� ������ � ��������. ������ ��� �� ������ �� bizzona.ru ��������
		// ����� �� ������ ��� ���� ��������� - GetValue('SOUND')
		// ���� ��� � �� �����, ��� ������ ��������� ������� ��������� ����
		// ������� ������ ����� �������� ����� �������
		// P.S. ����� ��������� � ����� �������� ��� ������ �� �������� EMBED SRC

	//$log = file_get_contents('../engine/log/trace.txt');
        //$smarty->assign("log", $log);

	$smarty->display('stat.tpl');
?>



