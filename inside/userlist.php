<?php

/*
* for control direct access to other modules
*/
define('ADMINLIST', 'TRUE');
//
// include the configs / constants for the database connection
require_once("classes/db.config.php");

// load the login class
require_once("classes/Login.class.php");
// load the registration class
require_once("classes/Registration.class.php");
require_once("classes/UserList.class.php");

require_once($_SERVER['DOCUMENT_ROOT'].'/inside/auth.php');
// create the registration object. when this object is created, it will do all registration stuff automatically
// so this single line handles the entire registration process.
$userlist = new UserList();
// require('displaypost.php');
// for control direct access to this modules
// ����� ������� � �������
if(!$login->isAdmin('ADMIN, MODER')){
	exit('No Privileges to show User List. No access allowed');
}

// show the register view (with the registration form, and messages/errors)
include("views/vuser_list.php");

?>