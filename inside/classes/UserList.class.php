<?php
// This library is intended to provide forward compatibility with the password_* functions being worked on for PHP 5.5.
// https://github.com/ircmaxell/password_compat
if (version_compare(PHP_VERSION, '5.5.0', '<')) {
    // if you are using PHP 5.3 or PHP 5.4 you have to include the password_api_compatibility_library.php
    // (this library adds the PHP 5.5 password hashing functions to older versions of PHP)
//    require_once("password_compatibility_library.php");
    require_once("passwordLib.php"); // ��� PHP 5.2.x
}

/**
 * Class list user
 * TODO: �������� ��������� ������
 */
class UserList
{
    /**
     * @var object The database connection
     */
    private $db_connection = null;
    /**
     * @var array Collection of error messages
     */
    public $errors = array();
    /**
     * @var array Collection of success / neutral messages
     */
    public $messages = array();

    public $usrlist = array();
    public $usrdata = array();
    public $tableusr = array();

    public $list_level = array( 0 => '�������������', 1 => '��������', 2 => '��������');

    /**
     ** __construct()
     **/
	public function __construct()
	{
		$this->user_list();
	}

	/*
	*
	*/
	public function user_list()
	{
            // create a database connection, ���������� ��������� ����������� �� db.config.php
			$this->db_connection = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

            // change character set to cp1251 and check it
            if (!$this->db_connection->set_charset("cp1251")) {
                $this->errors[] = $this->db_connection->error;
            }
            if (!$this->db_connection->connect_errno) {

                // escape the POST stuff
                // $user_name = $this->db_connection->real_escape_string($_POST['user_name']);

                // database query, getting all the info of the users
                $sql = "SELECT user_id, user_name, user_email, user_level, user_active
                        FROM users
                        WHERE 1=1 ORDER BY user_name;";
                $result_of_user_list = $this->db_connection->query($sql);

                // if this users exists
                if ($result_of_user_list->num_rows >= 1) {
                    // get result row (as an object)
                    $this->usrlist = array();
                    while ($result_row = $result_of_user_list->fetch_assoc()) {
                    	$this->usrlist[] = $result_row;
                    }

            	}

            } else {
                $this->errors[] = "�������� ����������� � ���� ������.";
            }


	} // end user_list
	/*
	*
	* TODO: �������� �����
	*/
	public function register_new()
	{
/*
		if (!empty($_POST['register_new'])) {
			//include('register.php');
			// require('register.php');
		$host  = $_SERVER['HTTP_HOST'];
		$uri   = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
		$extra = 'register.php';
		header("Location: http://$host$uri/$extra");
	    exit();
		}
*/
		return false;
	}

	/*
	*
	* TODO: �������� �����
	*/
	public function delete_user($user_id = 0)
	{
		if(!$user_id > 0) {
			return true;
		}
		return false;
	}

	public function activate_user($user_id = 0)
	{
		if($user_id > 0) {
        if ($this->DBConnection()) {
			$user_id = $this->db_connection->real_escape_string($user_id);
			$sql = "UPDATE users
			SET user_active = 1
			WHERE user_id = ".$user_id.";";
			$result_set_active = $this->db_connection->query($sql);
		}
		} else {return false;}
	} // end activate_user

	public function deactivate_user($user_id = 0)
	{
		if($user_id > 0) {
        if ($this->DBConnection()) {
			$user_id = $this->db_connection->real_escape_string($user_id);
			$sql = "UPDATE users
			SET user_active = 0
			WHERE user_id = ".$user_id.";";
			$result_set_active = $this->db_connection->query($sql);
		}
		} else {return false;}

	} // end deactivate_user

	public function set_user_level($user_id = 0, $user_level = 2)
	{
		if($user_id > 0) {
        	if ($this->DBConnection()) {
				$user_id = $this->db_connection->real_escape_string($user_id);
				$user_level = $this->db_connection->real_escape_string($user_level);
				$sql = "UPDATE users
				SET user_level = ".$user_level."
				WHERE user_id = ".$user_id.";";
				$result_set_level = $this->db_connection->query($sql);
			}
		} else {return false;}

	} // end set_user_level

	private function DBConnection()
	{
        // if connection already exists
        if ($this->db_connection != null) {
            return true;
        } else {
			$this->db_connection = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

            // change character set to cp1251 and check it
            if (!$this->db_connection->set_charset("cp1251")) {
                $this->errors[] = $this->db_connection->error;
            }
            if (!$this->db_connection->connect_errno) {
            	return true;
            } else {
            	$this->errors[] = $this->db_connection->error;
				$this->errors[] = "�������� ����������� � ���� ������.";
            	$this->db_connection = null;
            	return false;
            }

        }
        // default return
		return false;
	} // DBConnection

    public function getUserData($uid)
    {
                    $this->usrdata = array();
		// if database connection opened
		if ($this->DBConnection()) {
				$user_id = $this->db_connection->real_escape_string($uid);
                $sql = "SELECT user_id, user_name, user_email, user_level, user_active
                        FROM users
                        WHERE user_id =".$user_id." LIMIT 1;";
                $result_of_user_list = $this->db_connection->query($sql);

                // if this user exists
                if ($result_of_user_list->num_rows == 1) {
                    // get result row (as an object)
                    $result_row = $result_of_user_list->fetch_assoc();
                    $this->usrdata = $result_row;
            	} else {
            		return false;
            	}
        } // db connection
	}

	/*	change email for user
	*	user_change_email($uid);
	*/
	public function user_change_email($user_id = 0) {
		if(!$this->form_validate_email()) {return false;}
		// if database connection opened
		if ($this->DBConnection()) {
			$user_id = $this->db_connection->real_escape_string($user_id);
			$user_email = $this->db_connection->real_escape_string(strip_tags($_POST['user_email'], ENT_QUOTES));
			$this->getUserData($user_id);
			if( $this->usrdata['user_email'] == $user_email) {return false;}

			// check if email address already exists
			$sql = "SELECT * FROM users WHERE user_email = '" . $user_email . "';";
			$query_check_user_email = $this->db_connection->query($sql);

                if ($query_check_user_email->num_rows >= 1) {
                    $this->errors[] = "��������, ���� ����� ����������� ����� ��� �����.";
                } else {

				// update user's data into database
				$sql = "UPDATE users
				SET user_email = '".$user_email."'
				WHERE user_id = ".$user_id.";";
					if( !$result_email_change = $this->db_connection->query($sql)) {
					$this->errors[] = $result_email_change->error;
					$this->errors[] = "������ ��������� email.";
					return false;
					}
				return true;
				}
		}
		return false;
	} // end user_change_email

	/*	change password for user
	*	user_change_password($user_id);
	*/
	public function user_change_password($user_id) {

		if(!$this->form_validate_password()) {return false;}
		$user_password = $_POST['user_password_new'];
		$user_password_hash = password_hash($user_password, PASSWORD_DEFAULT);
		// if database connection opened
		if ($this->DBConnection()) {
			$user_id = $this->db_connection->real_escape_string($user_id);
			$sql = "UPDATE users
			SET user_password_hash = '".$user_password_hash."'
			WHERE user_id = ".$user_id.";";
			if( !$result_password_change = $this->db_connection->query($sql)) {
				$this->errors[] = $result_password_change->error;
				$this->errors[] = "������ ��������� ������.";
				return false;
			}
				return true;
		} // db connection
		return false;
	} // end user_change_password


	public function user_email_exists($user_email = '') {
		if(empty($user_email)) {return false;}
	}

	public function form_validate_email() {

		if (empty($_POST['user_email'])) {
            $this->errors[] = "Email �� ����� ���� ������";
        } elseif (strlen($_POST['user_email']) > 64) {
            $this->errors[] = "Email �� ����� ���� ������� 64 ��������";
        } elseif (!filter_var($_POST['user_email'], FILTER_VALIDATE_EMAIL)) {
            $this->errors[] = "��� ����� ����������� ����� ����� ������������ ������";
        } elseif ( !empty($_POST['user_email'])
            && strlen($_POST['user_email']) <= 64
            && filter_var($_POST['user_email'], FILTER_VALIDATE_EMAIL)
        ) {
		return true;
		}
		return false;
	} // form_validate_email

	public function form_validate_password() {
		if (empty($_POST['user_password_new']) || empty($_POST['user_password_repeat'])) {
            $this->errors[] = "������ ������";
        } elseif ($_POST['user_password_new'] !== $_POST['user_password_repeat']) {
            $this->errors[] = "������ � ��������� ������ �� ���������";
        } elseif (strlen($_POST['user_password_new']) < 6) {
            $this->errors[] = "������ ������ ���� �� ����� 6 ��������";
        } elseif (strlen($_POST['user_name']) > 64 || strlen($_POST['user_name']) < 2) {
            $this->errors[] = "��� ������������ �� ����� 2 � �� ����� 64 �������";
        } elseif (!preg_match('/^[a-z\d]{2,64}$/i', $_POST['user_name'])) {
            $this->errors[] = "� ����� ������������ ��������� ������ ���������� ����� � �����";
        } elseif (filter_var($_POST['user_email'], FILTER_VALIDATE_EMAIL)
            && !empty($_POST['user_password_new'])
            && !empty($_POST['user_password_repeat'])
            && ($_POST['user_password_new'] === $_POST['user_password_repeat'])
        ) {
		return true;
        }
		return false;
	} // form_validate_password


	public function user_validate_input() {
        if (empty($_POST['user_name'])) {
            $this->errors[] = "������ ��� ������������";
        } elseif (empty($_POST['user_password_new']) || empty($_POST['user_password_repeat'])) {
            $this->errors[] = "������ ������";
        } elseif ($_POST['user_password_new'] !== $_POST['user_password_repeat']) {
            $this->errors[] = "������ � ��������� ������ �� ���������";
        } elseif (strlen($_POST['user_password_new']) < 6) {
            $this->errors[] = "������ ������ ���� �� ����� 6 ��������";
        } elseif (strlen($_POST['user_name']) > 64 || strlen($_POST['user_name']) < 2) {
            $this->errors[] = "��� ������������ �� ����� 2 � �� ����� 64 �������";
        } elseif (!preg_match('/^[a-z\d]{2,64}$/i', $_POST['user_name'])) {
            $this->errors[] = "� ����� ������������ ��������� ������ ���������� ����� � �����";
        } elseif (empty($_POST['user_email'])) {
            $this->errors[] = "Email �� ����� ���� ������";
        } elseif (strlen($_POST['user_email']) > 64) {
            $this->errors[] = "Email �� ����� ���� ������� 64 ��������";
        } elseif (!filter_var($_POST['user_email'], FILTER_VALIDATE_EMAIL)) {
            $this->errors[] = "��� ����� ����������� ����� ����� ������������ ������";
        } elseif (!empty($_POST['user_name'])
            && strlen($_POST['user_name']) <= 64
            && strlen($_POST['user_name']) >= 2
            && preg_match('/^[a-z\d]{2,64}$/i', $_POST['user_name'])
            && !empty($_POST['user_email'])
            && strlen($_POST['user_email']) <= 64
            && filter_var($_POST['user_email'], FILTER_VALIDATE_EMAIL)
            && !empty($_POST['user_password_new'])
            && !empty($_POST['user_password_repeat'])
            && ($_POST['user_password_new'] === $_POST['user_password_repeat'])
        ) {
		// it is ok

            return true;
		} else {
            $this->errors[] = "��������� ����������� ������.";
            return false;
        }
            return false;
	} // end user_validate_input

} // end class UserList

class DisplayTable {

	public $arrayheader = array();
	public $arraytable = array();
	public $stable = '';
/*
*
*/
	public function __construct($theader = array(), $tdata = array()) {
		$this->arrayheader = $theader;
		$this->arraytable = $tdata;
	}

public function show() {
	$this->TableBegin();

	$this->TableEnd();
}

public function TableBegin($tclass='') {
	return "<table ".(!empty($tclass)? "class=\"".$tclass."\"" : '').">";
}
public function TableEnd() {
	return "</table>";
}

public function show_header($col_names = array()) {
    if(!empty($col_names)) {$this->arrayheader = $col_names;}
    if(empty($this->arrayheader)) {return false;}

	$this->stable = $this->theadb();
	foreach ($this->arrayheader as $tdvar) {
		$this->stable .= $this->theadcell($tdvar);
	}
	$this->stable .= $this->theade();
	return $this->stable;
}
/**
* html table head
**/
public function theadb($trclass='', $trid='') {
	$trB = "<thead>\n";
	$trB .= "<tr ".(!empty($trclass)? "class=\"".$trclass."\"" : '').">";
	return $trB;
}

/**
* html table head
**/
public function theade() {
	return "</tr>\n</thead>\n";
}

public function theadcell($cell = '') {
	$tcell_head = "<th>";
	$tcell_head .= $cell;
	$tcell_head .= "</th>";
	return $tcell_head;
}

/**
* html table row
*/
public function trowb($trclass='', $trid='') {
	$trB = "<tr ".(!empty($trclass)? "class=\"".$trclass."\"" : '').">";
	return $trB;
}

/**
* html table row
*/
public function trowe() {
	return "</tr>\n";
}

public function tcell($cell = '') {
	$scell = '';
	$scell .= $this->tcellb();
	$scell .= $cell;
	$scell .= $this->tcelle();
	return $scell;
}


public function tcellb() {
	return "<td>\n";
}

public function tcelle() {
	return "</td>\n";
}

public function trow() {

}

} // end class


?>