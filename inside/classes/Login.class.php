<?php
// � PHP ����� ������ ���� ������� password_*, ��� ���������� ������ �������� ������
// This library is intended to provide forward compatibility with the password_* functions being worked on for PHP 5.5.
// https://github.com/ircmaxell/password_compat
// ������ ��� PHP 5.3.7
if (version_compare(PHP_VERSION, '5.5.0', '<')) {
    // if you are using PHP 5.3 or PHP 5.4 you have to include the password_api_compatibility_library.php
    // (this library adds the PHP 5.5 password hashing functions to older versions of PHP)
//    require_once("password_compatibility_library.php");
	require_once("passwordLib.php"); // ��� PHP 5.2.x
}

define('ADMIN', 0);
define('MODER', 1);
define('USER', 2);

/**
 * Class login
 * handles the user's login and logout process
 * TODO: �������� ��������� ������
 */
class Login {
    /**
     * @var object The database connection
     */
    private $db_connection = null;
    /**
     * @var array Collection of error messages
     */
    public $errors = array();
    /**
     * @var array Collection of success / neutral messages
     */
    public $messages = array();

    /**
     * @var array User Level
     */
    public $list_level = array( 0 => '�������������', 1 => '��������', 2 => '��������');

    /**
     ** __construct()
     **/
    public function __construct()
    {
        // create/read session, absolutely necessary
        session_start();

        // check the possible login actions:
        // if user tried to log out (happen when user clicks logout button)
        if (isset($_GET["logout"])) {
            $this->doLogout();
        }
        // login via post data (if user just submitted a login form)
        elseif (isset($_POST["login"])) {
            $this->dologinWithPostData();
        }
    }

    /**
     * log in with post data
     */
    private function dologinWithPostData()
    {
        // check login form contents
        if (empty($_POST['user_name'])) {
            $this->errors[] = "�������� ��� ������������ ������.";
        } elseif (empty($_POST['user_password'])) {
            $this->errors[] = "�������� ������ ������.";
        } elseif (!empty($_POST['user_name']) && !empty($_POST['user_password'])) {

            // create a database connection, ���������� ��������� ����������� �� db.config.php
			$this->db_connection = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

            // change character set to cp1251 and check it
            if (!$this->db_connection->set_charset("cp1251")) {
                $this->errors[] = $this->db_connection->error;
            }

            // if no connection errors (= working database connection)
            if (!$this->db_connection->connect_errno) {

                // escape the POST stuff
                $user_name = $this->db_connection->real_escape_string($_POST['user_name']);

                // database query, getting all the info of the selected user (allows login via email address in the
                // username field)
                $sql = "SELECT user_id, user_name, user_email, user_password_hash, user_level, user_active
                        FROM users
                        WHERE user_name = '" . $user_name . "' OR user_email = '" . $user_name . "';";
                $result_of_login_check = $this->db_connection->query($sql);

                // if this user exists
                if ($result_of_login_check->num_rows == 1) {

                    // get result row (as an object)
                    $result_row = $result_of_login_check->fetch_object();

                    // using PHP 5.5's password_verify() function to check if the provided password fits
                    // the hash of that user's password

                    if (password_verify($_POST['user_password'], $result_row->user_password_hash)) {

						if($result_row->user_active == 1) {
                        // write user data into PHP SESSION (a file on your server)
                        $_SESSION['user_id'] = $result_row->user_id;
                        $_SESSION['user_name'] = $result_row->user_name;
                        $_SESSION['user_email'] = $result_row->user_email;
                        $_SESSION['user_level'] = $result_row->user_level;
                        $_SESSION['user_active'] = $result_row->user_active;
                        $_SESSION['user_login_status'] = 1;
                        } else {
                        	$this->errors[] = "������������ �� �����������. ������ ��������.";
                        }

                    } else {
                        $this->errors[] = "�������� ������. ���������� ��� ���.";
                    }
                } else {
                    $this->errors[] = "������ ������������ �� ����������.";
                }
            } else {
                $this->errors[] = "�������� ����������� � ���� ������.";
            }
        }
    }

    /**
     * perform the logout
     */
    public function doLogout()
    {
        // delete the session of the user
        $_SESSION = array();
        session_destroy();
        // return a message
        $this->messages[] = "�� ����� �� �������.";

    }

    /**
     * simply return the current state of the user's login
     * @return boolean user's login status
     */
    public function isUserLoggedIn()
    {
        if (isset($_SESSION['user_login_status']) AND $_SESSION['user_login_status'] == 1) {
        	if(!$this->isActive()) {
        		$this->doLogout();
        		return false;
        	}
		return true;
        }
        // default return
        return false;
    }

	public function isActive() {
		if(isset($_SESSION['user_login_status']) && $_SESSION['user_login_status'] == 1) {
		if ($this->DBConnection()) {
			$user_id = $this->db_connection->real_escape_string($_SESSION['user_id']);
			$sql = "SELECT user_name, user_active
			FROM users
			WHERE user_id = '".$user_id."';";
			$result_get_active = $this->db_connection->query($sql);
			// if this users exists
			if ($result_get_active->num_rows > 0) {
				$result_row = $result_get_active->fetch_assoc();
				if($result_row['user_active'] == 1) {
					return true;
				} else {
					$this->errors[] = "������������ {$result_row['user_name']} �� �����������. ������ ��������.";
					$_SESSION['user_active'] = 0;
					$_SESSION['user_login_status'] = 0;
				}
			}
		}
		}
		return false;
	}

	private function DBConnection() {
        // if connection already exists
        if ($this->db_connection != null) {
            return true;
        } else {
			$this->db_connection = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

            // change character set to cp1251 and check it
            if (!$this->db_connection->set_charset("cp1251")) {
                $this->errors[] = $this->db_connection->error;
            }
            if (!$this->db_connection->connect_errno) {
            	return true;
            } else {
            	$this->errors[] = $this->db_connection->error;
				$this->errors[] = "�������� ����������� � ���� ������.";
            	$this->db_connection = null;
            	return false;
            }

        }
        // default return
		return false;
	} // DBConnection

	public function isAdmin($usrlevel = "ADMIN, MODER, USER") {
		$usrlevel = explode(',', $usrlevel);
		$const_levels = array();
		foreach($usrlevel as $vconst) {
			array_push($const_levels, constant(trim($vconst)));
		}
		if(in_array($this->getLevel(), $const_levels)) return true;
		return false;
	}

	public function getLevel(){
		return (isset($_SESSION['user_level']) ? $_SESSION['user_level']: false);
	}

} // end class Login

