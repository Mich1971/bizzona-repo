<?
	global $DATABASE;
	include_once("../phpset.inc");
	include_once("../engine/functions.inc");
	include_once("../engine/valuation_functions.inc");	
	include_once("../engine/analitics_functions.inc");	

	AssignDataBaseSetting("../config.ini");

	//ini_set("display_startup_errors", "on");
	//ini_set("display_errors", "on");  
	//ini_set("register_globals", "on");	

  	require_once("../libs/Smarty.class.php");
  	$smarty = new Smarty;
  	$smarty->template_dir = "../templates";
  	$smarty->compile_dir  = "../templates_c";
  	$smarty->cache_dir = "../cache";
  	$smarty->compile_check = true;
  	$smarty->debugging     = false;
  	
  	$action_word = GetActionWord();
?>
	
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
  <HEAD>
<title>�������� ��������� <?=$action_word;?> ������ - ��� "������-����"</title>
  
	<META NAME="Description" CONTENT='�������� ��������� <?=$action_word;?> ������  - ��� "������-����"'>
        <meta name="Keywords" content="������� ����� ������"> 
        <LINK href="http://www.bizzona.ru/general.css" type="text/css" rel="stylesheet">
        <meta http-equiv="content-type" content="text/html; charset=windows-1251"/>
        <link rel="shortcut icon" href="http://www.bizzona.ru/images/favicon.ico">
    </HEAD>
<body bgcolor="#7c3535">

<?
	$smarty->assign("title", "�������� ��������� ".$action_word." ������");	
	$topmenu = GetMenu();
	$smarty->assign("topmenu", $topmenu);
	
	$link_country = GetSubMenuRightAnalitics();
	$smarty->assign("ad", $link_country);

	$title = cp1251_to_utf8("�������� ��������� ��������� ������ (bizzona.ru)");
	$max_cost = 6000000;
	if(isset($_GET["action"])) {
		if(!in_array($_GET["action"], array("cost","profit", "request"))) {
			$_GET["action"] = "cost";
		} else {
			$title = cp1251_to_utf8("�������� ��������� ".$action_word." ������ (bizzona.ru)");
			
			if($_GET["action"] == "profit") {
				$max_cost = 600000;
			}
		}
	} else {
		$_GET["action"] = "cost";
	}	
?>
<table class="w" border="0" cellpadding="0" cellspacing="0" bgcolor="#eeeee0">
	<tr>
		<td colspan="2">
			<?
				$output_header = $smarty->fetch("./site/headervaluation.tpl");
				echo minimizer($output_header);			
			?>	
		</td>
	</tr>
</table>	
<table class="w" border="0" cellpadding="0" cellspacing="0" bgcolor="#eeeee0">	
    <tr>
        <td valign="top" style="padding:10px;" >
			<?
				$list_top = $smarty->fetch("./site/biznes-analitika/list_top.tpl");
				echo fminimizer($list_top);
			?>        
        </td>    
    </tr>
    <tr>
        <td valign="top" style="padding-left:10pt;padding-right:10pt;" align="center">
			<?
				$submenu = $smarty->fetch("./site/biznes-analitika/submenu.tpl");
				echo fminimizer($submenu);
			?>        
			<?
				include_once 'open_flash_chart_object.php';
				$baseURL = "http://bizzona.ru/biznes-analitika/";
				open_flash_chart_object( '100%', '380', $baseURL.'datagraph.php?category_list=22&title='.$title.'&max_cost='.$max_cost.'&action='.$_GET["action"].'&v='.rand().'', false, $baseURL );
			?>
        </td>
    </tr>
    <tr>
    	<td>
			<?
				$listcategory = $smarty->fetch("./site/biznes-analitika/listcategory.tpl");
				echo fminimizer($listcategory);
			?>    	
    	</td>
    </tr>    
    <tr>
        <td valign="top" style="padding:10px;" >
			<?
				$suggest = $smarty->fetch("./site/biznes-analitika/suggest.tpl");
				echo fminimizer($suggest);
			?>        
        </td>    
    </tr>    
    <tr>
        <td valign="top" style="padding:10px;" >
			<?
				$list_bottom = $smarty->fetch("./site/biznes-analitika/list_bottom.tpl");
				echo fminimizer($list_bottom);
			?>        
        </td>    
    </tr>    
    <tr>
    	<td align="right">
			<div style="padding-top:10pt; padding-bottom: 5pt;padding-top:0pt; padding-bottom:0pt; width:auto;" >	    
				<table border="0" align="right" width="100%">
               		<tr>
               			<td align="right">
							<script type="text/javascript" src="//yandex.st/share/share.js" charset="utf-8"></script>
							<div class="yashare-auto-init" data-yashareType="button" data-yashareQuickServices="yaru,vkontakte,facebook,twitter,odnoklassniki,moimir,lj,friendfeed,moikrug"></div>
           				</td>			
					</tr>
				</table>
			</div>    	
    	</td>
    </tr>
	<tr>
		<td>
			<?
				$smarty->display("./site/footer.tpl");
			?>
		</td>
	</tr>    
</table>
</body>
</html>