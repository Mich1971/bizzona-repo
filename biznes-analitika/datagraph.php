<?php
	global $DATABASE;
	include_once("../phpset.inc");
	include_once("../engine/functions.inc");
	include_once("../engine/analitics_functions.inc");
	include_once("../engine/businessanalitics/class.businessanalitics.inc");	

	ini_set("display_startup_errors", "off");
	ini_set("display_errors", "off");  
	ini_set("register_globals", "on");	
	
	AssignDataBaseSetting("../config.ini");

	
	$businessanalitics = new Business_Analitics();
	
	$businessanalitics->text_cost = cp1251_to_utf8("���������");
	$businessanalitics->text_profit = cp1251_to_utf8("�������");
	
	if(!isset($_GET["category_list"])) {
		$_GET["category_list"] = "13";
	}
	
	if(!isset($_GET["title"])) {
		
		if(isset($_GET["action"]) &&  $_GET["action"] == "profit") {
			$_GET["title"] = "�������� ��������� ������������ ������ ������� (bizzona.ru)";
		} else if (isset($_GET["action"]) &&  $_GET["action"] == "reasonsell") { 
			$_GET["title"] = "(bizzona.ru)";		
		} else {
			$_GET["title"] = "�������� ��������� ��������� ������ ������� (bizzona.ru)";
		}
	}
	
	if(isset($_GET["max_cost"])) {
		$_GET["max_cost"] = intval($_GET["max_cost"]);
	} else {
		$_GET["max_cost"] = 8000000;
	}
	
	if(isset($_GET["max_profit"])) {
		$_GET["max_profit"] = intval($_GET["max_profit"]);
	} else {
		$_GET["max_profit"] = 5000000;
	}	
	
	if(isset($_GET["max_wagefund"])) {
		$_GET["max_wagefund"] = intval($_GET["max_wagefund"]);
	} else {
		$_GET["max_wagefund"] = 500000;
	}	

	if(isset($_GET["max_expemse"])) {
		$_GET["max_expemse"] = intval($_GET["max_expemse"]);
	} else {
		$_GET["max_expemse"] = 500000;
	}	
	
	if(isset($_GET["max_rent"])) {
		$_GET["max_rent"] = intval($_GET["max_rent"]);
	} else {
		$_GET["max_rent"] = 1000000;
	}	
	
	
	$businessanalitics->max_expemse = $_GET["max_expemse"];
	$businessanalitics->max_wagefund = $_GET["max_wagefund"];
	$businessanalitics->max_cost = $_GET["max_cost"]; 
	$businessanalitics->max_rent = $_GET["max_rent"]; 
	
	$businessanalitics->title = cp1251_to_utf8($_GET["title"].",{font-size:16px; color: #FFFFFF; margin: 5px; background-color: #006699; padding:2px;}");
	$businessanalitics->x_labels = cp1251_to_utf8("������, �������, ����, ������, ���, ����, ����, ������, ��������, �������, ������, �������");
	$businessanalitics->category_list = explode(",", $_GET["category_list"]);
	$businessanalitics->year_list = array(2009,2010,2011,2012);
	
	if(isset($_GET["action"]) &&  $_GET["action"] == "profit") {
		
		$title_action = GetTitleActionAnalitics("profit", $businessanalitics->category_list[0]);
		if(strlen($title_action) > 0) {
			$businessanalitics->title = cp1251_to_utf8($title_action).",{font-size:16px; color: #FFFFFF; margin: 5px; background-color: #006699; padding:2px;}";
		}		
		
		$businessanalitics->y_legend =  cp1251_to_utf8("Profit (RUB),12");
		echo $businessanalitics->RenderProfitDefault();
	} else if (isset($_GET["action"]) &&  $_GET["action"] == "cost") { 
		
		$title_action = GetTitleActionAnalitics("cost", $businessanalitics->category_list[0]);
		if(strlen($title_action) > 0) {
			$businessanalitics->title = cp1251_to_utf8($title_action).",{font-size:16px; color: #FFFFFF; margin: 5px; background-color: #006699; padding:2px;}";
		}		
		
		$businessanalitics->y_legend = cp1251_to_utf8("Asking Price (RUB),12");
		echo $businessanalitics->RenderDefault();
	} else if (isset($_GET["action"]) &&  $_GET["action"] == "request") { 
		
		$title_action = GetTitleActionAnalitics("request", $businessanalitics->category_list[0]);
		if(strlen($title_action) > 0) {
			$businessanalitics->title = cp1251_to_utf8($title_action).",{font-size:16px; color: #FFFFFF; margin: 5px; background-color: #006699; padding:2px;}";
		}		
		
		$businessanalitics->y_legend = cp1251_to_utf8("count request,12");
		echo $businessanalitics->RenderRequestDefault();
	} else if (isset($_GET["action"]) &&  $_GET["action"] == "reasonsell") { 
		$businessanalitics->title = cp1251_to_utf8("������ ������� ������� ������? (bizzona.ru)".",{font-size:16px; color: #FFFFFF; margin: 5px; background-color: #7c3535; padding:2px;}");
		$businessanalitics->pie_labels = cp1251_to_utf8("������ ��� �������, ������� ��� ���������, ���������� �������, ���������� �������, �������� �������, �������, ������������ �����, �����������, �������� �����������, ������ ����� ������, C���� ������������, �������������� � ������");
		echo $businessanalitics->RenderWhySellBusinessesDefault();
	} else if (isset($_GET["action"]) &&  $_GET["action"] == "costprofit") { 
		
		$title_action = GetTitleActionAnalitics("costprofit", $businessanalitics->category_list[0]);
		if(strlen($title_action) > 0) {
			$businessanalitics->title = cp1251_to_utf8($title_action).",{font-size:16px; color: #FFFFFF; margin: 5px; background-color: #006699; padding:2px;}";
		}
		
		$businessanalitics->max_profit = $_GET["max_profit"];
		echo $businessanalitics->RenderCostProfitDefault();
	}  else if (isset($_GET["action"]) &&  $_GET["action"] == "totalshow") { 

		$title_action = GetTitleActionAnalitics("totalshow", $businessanalitics->category_list[0]);
		if(strlen($title_action) > 0) {
			$businessanalitics->title = cp1251_to_utf8($title_action).",{font-size:16px; color: #FFFFFF; margin: 5px; background-color: #006699; padding:2px;}";
		}
		
		$businessanalitics->pie_labels = cp1251_to_utf8("�������, ��������, ��������� �������, �������, ������");
		echo $businessanalitics->RenderShowInfoForBuyer();
	} else if (isset($_GET["action"]) &&  $_GET["action"] == "statview") { 
		
		$businessanalitics->Id = intval($_GET["Id"]);
		$businessanalitics->title = cp1251_to_utf8("���������� ���������� ���������� ������� ������� (���: ".$businessanalitics->Id.")").",{font-size:16px; color: #FFFFFF; margin: 5px; background-color: #006699; padding:2px;}";
		$businessanalitics->y_legend = cp1251_to_utf8("Stat view (count),12");
		
		echo $businessanalitics->RenderStatView();
		
	} else {
		$businessanalitics->y_legend = cp1251_to_utf8("Asking Price (RUB),12");
		echo $businessanalitics->RenderDefault();
	}
?>
