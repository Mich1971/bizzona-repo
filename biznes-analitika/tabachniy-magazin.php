<?
	global $DATABASE;
	include_once("../phpset.inc");
	include_once("../engine/functions.inc");
	include_once("../engine/valuation_functions.inc");	
	include_once("../engine/analitics_functions.inc");
	include_once("../engine/businessanalitics/class.businessanalitics.inc");

	AssignDataBaseSetting("../config.ini");

	//ini_set("display_startup_errors", "on");
	//ini_set("display_errors", "on");  
	//ini_set("register_globals", "on");	

  	require_once("../libs/Smarty.class.php");
  	$smarty = new Smarty;
  	$smarty->template_dir = "../templates";
  	$smarty->compile_dir  = "../templates_c";
  	$smarty->cache_dir = "../cache";
  	$smarty->compile_check = true;
  	$smarty->debugging     = false;
  
  	$title = "�������� ��������� ��������� - ������� ��������� ��������";  	
  	
	if(!isset($_GET["action"])) {
		$_GET["action"] = "costprofit";
	} else {
		$title = "���������� ���������� ��������� ��������";
		$_GET["action"] = "totalshow";
	}  	
  		
?>
	
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
  <HEAD>
		<title><?=$title;?> - ��� "������-����"</title>
		<META NAME="Description" CONTENT='<?=$title;?>  - ��� "������-����"'>
        <meta name="Keywords" content="������� ����� �������� �������"> 
        <LINK href="http://www.bizzona.ru/general.css" type="text/css" rel="stylesheet">
        <meta http-equiv="content-type" content="text/html; charset=windows-1251"/>
        <link rel="shortcut icon" href="http://www.bizzona.ru/images/favicon.ico">
    </HEAD>
<body bgcolor="#7c3535">

<?
	$title = $title." (bizzona.ru)";
	$smarty->assign("title", $title);	
	
	$topmenu = GetMenu();
	$smarty->assign("topmenu", $topmenu);
	
	$link_country = GetSubMenuRightAnalitics();
	$smarty->assign("ad", $link_country);
	
	
	$title = cp1251_to_utf8($title);
	
	$max_cost = 3000000;
	$max_profit = 1000000;
	$max_wagefund = 500000;
	$max_expemse = 1000000;	
	$max_rent =  300000;
	$category_list = 365;
	
	$submenulink = GetLinkToSubMenuForSale('http://www.bizzona.ru/detailssearch.php?catId=115&subCatId='.$category_list.'', '������� ��������� ��������', '����������� ������� ��������� ��������');
	$smarty->assign("submenulink", $submenulink);	
	
	if(isset($_GET["action"]) and $_GET["action"] == "totalshow") {
		
		$analitics = new Business_Analitics();
		$analitics->max_cost = $max_cost; 
		$analitics->max_profit = $max_profit; 
		$analitics->max_wagefund = $max_wagefund; 
		$analitics->max_expemse = $max_expemse; 
		$analitics->max_rent = $max_rent;
		$analitics->category_list = array($category_list);
	
    	$array_analitics = $analitics->GetDataShowInfoForBuyer();
    	$smarty->assign("array_analitics", $array_analitics);
	}	
?>
<table class="w" border="0" cellpadding="0" cellspacing="0" bgcolor="#eeeee0">
	<tr>
		<td colspan="2">
			<?
				$output_header = $smarty->fetch("./site/headervaluation.tpl");
				echo minimizer($output_header);			
			?>	
		</td>
	</tr>
</table>	
<table class="w" border="0" cellpadding="0" cellspacing="0" bgcolor="#eeeee0">	
    <tr>
        <td valign="top" style="padding:10px;" >
			<?
				$list_top = $smarty->fetch("./site/biznes-analitika/list_top.tpl");
				echo fminimizer($list_top);
			?>        
        </td>    
    </tr>
    <tr>
        <td valign="top" style="padding-left:10pt;padding-right:10pt;" align="center">
			<?
				$submenu = $smarty->fetch("./site/biznes-analitika/submenuforall.tpl");
				echo fminimizer($submenu);
			?>        
			<?
				if(isset($_GET["action"]) and $_GET["action"] == "totalshow") {
					?>
						<table class="w" cellspacing="0" cellspacing="0" border="0">
							<tr>
								<td width="50%" valign="top">
					<?
				}
			?>			
			<?
				include_once 'open_flash_chart_object.php';
				$baseURL = "http://bizzona.ru/biznes-analitika/";
				open_flash_chart_object( '100%', '350', $baseURL.'datagraph.php?category_list='.$category_list.'&title='.$title.'&max_profit='.$max_profit.'&max_cost='.$max_cost.'&action='.$_GET["action"].'&max_wagefund='.$max_wagefund.'&max_expemse='.$max_expemse.'&max_rent='.$max_rent.'', false, $baseURL );
			?>
			<?
				if(isset($_GET["action"]) and $_GET["action"] == "totalshow") {
					?>
								</td>
								<td width="50%" valign="top" align="left">
									<?
										$totalshow = $smarty->fetch("./site/biznes-analitika/totalshow.tpl");
										echo fminimizer($totalshow);									
									?>
								</td>
							</tr>
						</table>
					<?
				}
			?>			
        </td>
    </tr>
    <tr>
    	<td>
			<?
				$listcategory = $smarty->fetch("./site/biznes-analitika/listcategory.tpl");
				echo fminimizer($listcategory);
			?>    	
    	</td>
    </tr>    
    <tr>
        <td valign="top" style="padding:10px;" >
			<?
				$suggest = $smarty->fetch("./site/biznes-analitika/suggest.tpl");
				echo fminimizer($suggest);
			?>        
        </td>    
    </tr>    
    <tr>
        <td valign="top" style="padding:10px;" >
			<?
				$list_bottom = $smarty->fetch("./site/biznes-analitika/list_bottom.tpl");
				echo fminimizer($list_bottom);
			?>        
        </td>    
    </tr>    
    <tr>
    	<td align="right">
			<div style="padding-top:10pt; padding-bottom: 5pt;padding-top:0pt; padding-bottom:0pt; width:auto;" >	    
				<table border="0" align="right" width="100%">
               		<tr>
               			<td align="right">
							<script type="text/javascript" src="//yandex.st/share/share.js" charset="utf-8"></script>
							<div class="yashare-auto-init" data-yashareType="button" data-yashareQuickServices="yaru,vkontakte,facebook,twitter,odnoklassniki,moimir,lj,friendfeed,moikrug"></div>
           				</td>			
					</tr>
				</table>
			</div>    	
    	</td>
    </tr>    
	<tr>
		<td>
			<?
				$smarty->display("./site/footer.tpl");
			?>
		</td>
	</tr>    
</table>
</body>
</html>