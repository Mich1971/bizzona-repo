<?
	global $DATABASE;
	include_once("../phpset.inc");
	include_once("../engine/functions.inc");
	include_once("../engine/valuation_functions.inc");	
	include_once("../engine/analitics_functions.inc");

	AssignDataBaseSetting("../config.ini");

	//ini_set("display_startup_errors", "on");
	//ini_set("display_errors", "on");  
	//ini_set("register_globals", "on");	

  	require_once("../libs/Smarty.class.php");
  	$smarty = new Smarty;
  	$smarty->template_dir = "../templates";
  	$smarty->compile_dir  = "../templates_c";
  	$smarty->cache_dir = "../cache";
  	$smarty->compile_check = true;
  	$smarty->debugging     = false;
?>
	
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
  <HEAD>
<title>������ ������� ���������� ������? - ��� "������-����"</title>
  
	<META NAME="Description" CONTENT='������ ������� ���������� ������?  - ��� "������-����"'>
        <meta name="Keywords" content="������ ������� ���������� �����?"> 
        <LINK href="http://www.bizzona.ru/general.css" type="text/css" rel="stylesheet">
        <meta http-equiv="content-type" content="text/html; charset=windows-1251"/>
        <link rel="shortcut icon" href="http://www.bizzona.ru/images/favicon.ico">
    </HEAD>
<body bgcolor="#7c3535">

<?
	$smarty->assign("title", "������ ������� ���������� ������?");	
	
	$topmenu = GetMenu();
	$smarty->assign("topmenu", $topmenu);
	
	$link_country = GetSubMenuRightAnalitics();
	$smarty->assign("ad", $link_country);
	
	$_GET["action"] = "reasonsell";
	$title = cp1251_to_utf8("������ ������� ������� ������? (bizzona.ru)");
?>
<table class="w" border="0" cellpadding="0" cellspacing="0" bgcolor="#eeeee0">
	<tr>
		<td colspan="2">
			<?
				$output_header = $smarty->fetch("./site/headervaluation.tpl");
				echo minimizer($output_header);			
			?>	
		</td>
	</tr>
</table>	
<table class="w" border="0" cellpadding="0" cellspacing="0" bgcolor="#eeeee0">	
    <tr>
        <td valign="top" style="padding:5pt;" align="center" width="50%">
			<?
				include_once 'open_flash_chart_object.php';
				$baseURL = "http://bizzona.ru/biznes-analitika/";
				open_flash_chart_object( '100%', '380', $baseURL.'datagraph.php?title='.$title.'&action='.$_GET["action"].'', false, $baseURL );
			?>
        </td>
        <td width="50%" valign="top" style="padding:5pt;">
     		<h3 align="center" style="color:#006699;margin:0pt;padding:4pt;font-family:arial;">����� ��������� ��, ��� �������� �����?</h3>
     		<div style='font-family:arial;font-size:11pt;border-top: 1px; border-bottom: 1px; border-left:1px;  border-right:1px;  border-color:#C1C1A4;  border-style: solid;padding:5px;background-color:#ffffff;' >
     		
     			<p style="text-indent:20pt;">
					����� ���������� ���������������� ������ ������ � ���, ��� ������ ������� ������ ������ ��� �� �������� ���� ���������� ����  � ��� ��������� ����� ���� ��������. �� ����� ���� ��� �� ���. � ����������� ���� ���������� ������� ������ ���� �� ����������� �� ���� ��������� (�� ���������� ����� 5-6 ���) ����  ����������� ����� ���������� � ������ ����� ������������. ��, ������, ������ ������ ���� �������������� ������ ����� ������ ���������� �� ������� �� ������� ����, ��� ���������� �� �������� ��������� ������������� � ���������� ����� �� ����������� �������. � ���� ������ �� ����������� ��� ����������� �������� ����� �������� ��� ��������� �������� �������. ����� �������� ������� ������������� � ����� ��������� � ��������� �����  �������� ���� � ������� ��������� �������  �� ��������� �������� ����������.  �� ������ ������� � ����� ������� ������������ ������ ��������������  ������� ������ �������� ������� ������� �������� � ���������� �����������  �������� �����������  � ���� ���� �������� � (�<b>�����</b>�) ������ ������.       			
     			</p>
     		
			</div>
        </td>
    </tr>
    <tr>
    	<td align="right" colspan="2">
			<div style="padding-top:10pt; padding-bottom: 5pt;padding-top:0pt; padding-bottom:0pt; width:auto;" >	    
				<table border="0" align="right" width="100%">
               		<tr>
               			<td align="right">
							<script type="text/javascript" src="//yandex.st/share/share.js" charset="utf-8"></script>
							<div class="yashare-auto-init" data-yashareType="button" data-yashareQuickServices="yaru,vkontakte,facebook,twitter,odnoklassniki,moimir,lj,friendfeed,moikrug"></div>
           				</td>			
					</tr>
				</table>
			</div>    	
    	</td>
    </tr>    
	<tr>
		<td colspan="2">
			<?
				$smarty->display("./site/footer.tpl");
			?>
		</td>
	</tr>    
</table>
</body>
</html>