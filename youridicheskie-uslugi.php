 <?
  global $DATABASE;
  include_once("./phpset.inc");
  include_once("./engine/functions.inc"); 
  AssignDataBaseSetting();

  require_once("./libs/Smarty.class.php");
  $smarty = new Smarty;
  $smarty->template_dir = "./templates";
  $smarty->compile_dir  = "./templates_c";
  //$smarty->cache_dir = "./cache";
  $smarty->compile_check = true;
  $smarty->debugging     = false;

  require_once("./regfuncsmarty.php");

  $assignDescription = AssignDescription();
  
	$title = "����������� ������, ��� \"������ ����\"";
	$smarty->assign("title", $title);  

  include_once("./engine/class.ContactInfo_Lite.inc");
  $contact = new ContactInfo_Lite();  

  $contact->GetContactInfo();  
  
  
  $smarty->assign("contact", $contact);	
	
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>

  <HEAD>
      <title>����������� ������ (����������� ���, ���������� ���, ������ ������), ��� "������ ����"</title>		
	<META NAME="Description" CONTENT="����������� ������ (����������� ���, ���������� ���, ������ ������)">
        <meta name="Keywords" content="����������� ������, ����������� ���, ���������� ���"> 
        <LINK href="./general.css" type="text/css" rel="stylesheet">
        <meta http-equiv="content-type" content="text/html; charset=windows-1251"/>
		<script type="text/javascript" src="ja.js"></script>        
  </HEAD>

<?php
  //$smarty->display("./site/headerbanner.tpl");
?>

<table class="w" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="2">
<?
	$topmenu = GetMenu();
	$smarty->assign("topmenu", $topmenu);

	$link_country = GetSubMenu();
	$smarty->assign("ad", $link_country);
	
	$output_header = $smarty->fetch("./site/detailssearchheader.tpl");
	echo minimizer($output_header);
?>		
		</td>
	</tr>
    <tr>
        <td valign="top"  colspan="2">

        	<table width="100%" border="0">
        	<tr>
        	<td>
        	<div style="font-size:12pt;font-family:arial;padding-left:2pt;padding-right:2pt;padding-top:10pt;padding-bottom:10pt;background-color:#efefe2;" align="center">
        	������������ �� �������� ����������� / ����������  - ���������! <br>������� <b><?=$contact->supportPhone;?></b>.
        	</div>
        	</td>
        	</tr>
        	</table>

        	<div style="font-size:12pt;font-family:arial;padding-left:4pt;padding-right:4pt;padding-top:10pt;padding-bottom:10pt;background-color:#f6f6ed;">
        	
        	<h1>����������� ����������� ������ � ��������������</h1>
        	<p>(��������� ������ �� 6/11 ������� -�10 000/13 000 �� ���� ���� �. ������.)</p>

        	<h1 align="center" style="color:#c32121">���������������</h1>
        	<h2 align="center" style="background-color:#fcfcfb;">����������� ��� "��� ����" - 7 000 ������!</h2>
        	<p>� ��������� ��������:</p>
        	<ul>
        		<li>������������ �� ������ ������������
        		<li>������������ �� ������ ������� ���������������
        		<li>������ ����� �����
        		<li>���������� ������� ������ ���������� ��� ��������� � ���������
        		<li>��������� ������� ���������� ��� ������ �����������
        		<li>��������� ����� ����������
        		<li>��������� ��������� ��� � ���
        		<li>���������� ��������� � �������� �� ���
        		<li>������������ ������ �� ����������� ��������
        	</ul>
        	
        	<p><i>
				�����10 ������� ���� (�� ����8 ������� ���� ��������� ��������� � ���� �� ��������������� �����������) �� �������� ��������� ������� � ������ �����������.  � ��������� �� ������: ���������� (4000 ������) � ������ ��������� (1300 ������)
        	</i></p>
        	
        	
        	<h2 align="center" style="background-color:#fcfcfb;">���������� ��� / �� �������������</h2>
        	
        	<table  align="center" width="70%">
        		<tr  bgcolor="#fcfcfb">
        			<td width="70%">���������� �� - 10 �. ��. (���������� 160 ������)</td>
        			<td align="right">3 000 ������</td>
        		</tr>
        		<tr bgcolor="#fcfcfb">
        			<td width="70%">���������� ��� �������������� 3 ������ <br><i>(�������� ��� ���������� � ������ ���������)</i></td>
        			<td align="right">24 000 ������</td>
        		</tr>
        		<tr bgcolor="#fcfcfb">
        			<td width="70%">���������� ��� ������������ - �� 8 �������</td>
        			<td align="right">40 000 ������</td>
        		</tr> 
        		<tr bgcolor="#fcfcfb">
        			<td width="70%">���������� ��� ����� ������������� (������)</td>
        			<td align="right">�� 50 000 ������</td>
        		</tr>
        		<tr bgcolor="#fcfcfb">
        			<td width="70%">������������� ��� � ���</td>
        			<td align="right">25 000 ������</td>
        		</tr> 
        		<tr bgcolor="#fcfcfb">
        			<td width="70%">������������ ������ �����-������� ����� ���</td>
        			<td align="right">7 000 ������</td>
        		</tr>        		       		        		       		        		
        	</table>
        	
        	<h2 align="center" style="background-color:#fcfcfb;">�������� ��������� � ����� � ������������� ��������� ������������ ���� / ��</h2>
        	
        	<table  align="center" width="70%">
        		<tr  bgcolor="#fcfcfb">
        			<td width="70%">����� ���������</td>
        			<td align="right">3 000 ������</td>
        		</tr>
        		<tr  bgcolor="#fcfcfb">
        			<td width="70%">����/����� ���������</td>
        			<td align="right">3 000 ������</td>
        		</tr>
        		<tr  bgcolor="#fcfcfb">
        			<td width="70%">����� ������������</td>
        			<td align="right">3 000 ������</td>
        		</tr>        		  	
        		<tr  bgcolor="#fcfcfb">
        			<td width="70%">����� ����� ������������</td>
        			<td align="right">3 000 ������</td>
        		</tr>        			 
        		<tr  bgcolor="#fcfcfb">
        			<td width="70%">����� ������ ���������������</td>
        			<td align="right">3 000 ������</td>
        		</tr>        		
        		       		
        	</table>

			<h2 align="center" style="background-color:#fcfcfb;">����������� ���, ��, ��, ��� ����������� ������� ����� ��� � ����</h2>        	
        	
        	<table  align="center" width="70%">
        		<tr  bgcolor="#fcfcfb">
        			<td width="70%">�������� ������������ �������������</td>
        			<td align="right">3 000 ������</td>
        		</tr>
        		<tr  bgcolor="#fcfcfb">
        			<td width="70%">�������� ��������, ����������������</td>
        			<td align="right">����������</td>
        		</tr>
        		<tr  bgcolor="#fcfcfb">
        			<td width="70%">����������� /� ��������������� ��������� ����� (����� ������������); �������� ��������� � ������������� </td>
        			<td align="right">�� 4 000 ������</td>
        		</tr>        		  	
        	</table>			
        	
        	
        	<h2 align="center" style="background-color:#fcfcfb;">������ ������</h2>        	
        	
        	<table  align="center" width="70%">
        		<tr  bgcolor="#fcfcfb">
        			<td width="70%">������� �� ����� � �.�. �������</td>
        			<td align="right">1 500  ������</td>
        		</tr>
        		<tr  bgcolor="#fcfcfb">
        			<td width="70%">�������� � ������ ��� </td>
        			<td align="right">3 000 ������</td>
        		</tr>
        		<tr  bgcolor="#fcfcfb">
        			<td width="70%">�����������/������ � ��������</td>
        			<td align="right">4 000 ������</td>
        		</tr> 
        		<tr  bgcolor="#fcfcfb">
        			<td width="70%">�������� ���������� ����� (�� 1 ����)</td>
        			<td align="right">2 000 / 4 000 ������</td>
        		</tr> 
        		<tr  bgcolor="#fcfcfb">
        			<td width="70%">��������� ������� �� ���������� ������������� ��� ��</td>
        			<td align="right">5 000 ������</td>
        		</tr> 
        		<tr  bgcolor="#fcfcfb">
        			<td width="70%">������������ �������, �������</td>
        			<td align="right">300 ������</td>
        		</tr>        		       		       		       		  	
        	</table>        	
        	
        	
			</div>
        	
        	
        	<table width="100%" border="0">
        	<tr>
        	<td>
        	<div style="font-size:12pt;font-family:arial;padding-left:2pt;padding-right:2pt;padding-top:10pt;padding-bottom:10pt;background-color:#efefe2;" align="center">
        	������������ �� �������� ����������� / ����������  - ���������! <br>������� <b><?=$contact->supportPhone;?></b>.
        	</div>
        	</td>
        	</tr>
        	</table>
        	
        	
        
        </td>
	<tr>
		<td colspan="2">

<div style="padding-top:4pt;padding-bottom:5pt;padding-right:10pt;padding-left:10pt;font-size:10pt;font-family:arial;">
	<table cellpadding="0" cellspacing="0" border="0"  style="width:100%;padding-bottom:2pt;" bgcolor="#eeeee0">
    	<tr>
        	<td style="background-image:url(<?=$_SERVER["host_name"]?>images/d1.gif); background-repeat:no-repeat;width:5px;height:5px; background-position:left top;">&nbsp;</td>
            <td rowspan="2" valign="middle" align="center"  style="font-size:8pt; font-family:Arial; color:black;">
            </td>
            <td style="background-image:url(<?=$_SERVER["host_name"]?>images/d2.gif); background-repeat:no-repeat;width:5px;height:5px;background-position:right top;">&nbsp;</td>
        </tr>
        <tr>
            <td style="background-image:url(<?=$_SERVER["host_name"]?>images/d4.gif); background-repeat:no-repeat;width:5px;height:5px; background-position:left bottom;">&nbsp;</td>
            <td style="background-image:url(<?=$_SERVER["host_name"]?>images/d3.gif); background-repeat:no-repeat;width:5px;height:5px; background-position:right bottom;">&nbsp;</td>
        </tr>
    </table>
</div>

<?
	$smarty->display("./site/footer.tpl");
?>
		</td>
	</tr>
</table>	
</body>
</html>