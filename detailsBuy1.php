<?
  global $DATABASE;

  include_once("./phpset.inc");
  include_once("./engine/functions.inc");
  
  $count_sql_call = 0; 

  $start = get_formatted_microtime(); 

  AssignDataBaseSetting();
  
  include_once("./engine/class.category.inc"); 
  $category = new Category();

  include_once("./engine/class.country.inc"); 
  $country = new Country();

  include_once("./engine/class.city.inc"); 
  $city = new City();

  include_once("./engine/class.buy.inc"); 
  $buyer = new Buyer();

  include_once("./engine/class.sell.inc"); 
  $sell = new Sell();  
  
  include_once("./engine/class.metro.inc");
  $metro = new Metro();

  include_once("./engine/class.district.inc"); 
  $district = new District();

  include_once("./engine/class.subcategory.inc");
  $subcategory = new SubCategory();

  include_once("./engine/class.streets.inc");
  $streets = new streets();
  
  require_once("./libs/Smarty.class.php");
  $smarty = new Smarty;
  $smarty->template_dir = "./templates";
  $smarty->compile_dir  = "./templates_c";
  $smarty->cache_dir = "./cache";
  $smarty->compile_check = true;
  $smarty->debugging     = false;
  //$smarty->caching = true; 

  require_once("./regfuncsmarty.php");
  
  $assignDescription = AssignDescription();

  $dataBuyer = Array();
  $dataBuyer["ID"] = intval($_GET["ID"]);

  //$resultGetItem = $buyer->GetItem($dataBuyer);
  
  // up caching call details buyer
  $options = array(
  		'cacheDir' => "buycache/",
   		'lifeTime' => 2592000
  );
		
  $cache = new Cache_Lite($options);
		
  if ($data = $cache->get(''.$dataBuyer["ID"].'____buyitem')) {
	$resultGetItem = unserialize($data);
  } else { 
	$resultGetItem = $buyer->GetItem($dataBuyer);
	$cache->save(serialize($resultGetItem));
  }
  
  // donw caching call details buyer  
  
  $headertitle = "";
  
  if (isset($resultGetItem->id)) {
    $seoname = $resultGetItem->seoname;
    if(strlen($seoname) > 0) {
    	$seoname =  $seoname.", ";
    }
    if(strlen($resultGetItem->subtitle) > 0) {
  		$headertitle = $resultGetItem->subtitle;
  		$seoname = 	$headertitle;
  		$seoname = str_replace(".", ", ", $seoname);
    }
  }
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
  <HEAD>
      <title><?=(strlen($headertitle) > 0 ? $headertitle : "������� � ������� �������.");?>   ����� <?=strtolower($resultGetItem->name);?>, ��� "������ ����"</title>		
		<META NAME="Description" CONTENT="������� �������, ������� �������, ������� ������, ������� �������, ������� �������� �������, ������� �������, ������� ��������,  ������� ��������, ������� ��������, ���� ������� ������?">
        <meta name="Keywords" content="<?=$seoname;?>������� �������, ������� ��������, ����� <?=$resultGetItem->name;?>"> 
        <LINK href="http://www.bizzona.ru/general.css" type="text/css" rel="stylesheet">
        <meta http-equiv="content-type" content="text/html; charset=windows-1251"/>
		<script type="text/javascript" src="http://www.bizzona.ru/ja.js"></script>        
  </HEAD>

<?php
  	$smarty->display("./site/headerbanner.tpl");
  	
	$smarty->caching = false; 
	
	$smarty->assign("headertitle", $headertitle);
	
	$topmenu = GetMenu();
	$smarty->assign("topmenu", $topmenu);

	$link_country = GetSubMenu();
	$smarty->assign("ad", $link_country);

	$output_header = $smarty->fetch("./site/headerbuy.tpl");
	echo minimizer($output_header);	
	
	$smarty->caching = true; 
?>

<table class="w" border="0" cellpadding="0" cellspacing="0" height="100%">
    <tr>
        <td width="30%" valign="top"  height="100%">
        <?
        
          $smarty->cache_dir = "./persistentcache"; 
          $smarty->cache_lifetime = 10800;          
        
          if (isset($_GET["action"]) && $_GET["action"]  == "category") {
            $_GET["ID"] = (isset($_GET["ID"])  ? intval($_GET["ID"]) : 0);
            $smarty->caching = true; 
            if (!$smarty->is_cached('./site/categorysb.tpl', $_GET["ID"])) {
              $data = array();
              $data["offset"] = 0;
              $data["rowCount"] = 0;
              $data["sort"] = "CountB";
              $result = $category->Select($data);
              $smarty->assign("data", $result);
            }
            
			$output_categorysb = $smarty->fetch("./site/categorysb.tpl", $_GET["ID"]);
			echo fminimizer($output_categorysb);            
            
            
            $smarty->caching = false; 
          } else { 
            $smarty->caching = true; 
            if (!$smarty->is_cached('./site/categorysb.tpl')) {
              $data = array();
              $data["offset"] = 0;
              $data["rowCount"] = 0;
              $data["sort"] = "CountB";
              $result = $category->Select($data);
              $smarty->assign("data", $result);
            }
            
			$output_categorysb = $smarty->fetch("./site/categorysb.tpl");
			echo fminimizer($output_categorysb);            
            
            $smarty->caching = false; 
          }
          
          $smarty->cache_dir = "./cache";
          $smarty->cache_lifetime = 3600;          
          
          $smarty->display("./site/call.tpl");
        ?>
		<div style="padding-top:2pt;"></div>                    
        <?
			$output_proposal = $smarty->fetch("./site/proposal.tpl");
			echo minimizer($output_proposal);
        	
			$output_request = $smarty->fetch("./site/request.tpl");
			echo minimizer($output_request);
			
			$data = array();
			$data["offset"] = 0;
			$data["rowCount"] = 3;
			$data["CategoryID"] = $resultGetItem->typeBizID;
			$data["CityID"] = $resultGetItem->regionID;
			$data["StatusID"] = "-1";
			$data["sort"] = "DataCreate";
			$resultsell = $sell->Select($data);
        	
        	if (isset($resultGetItem->id) && sizeof($resultsell) > 0) {
				$smarty->caching = true; 
				if (!$smarty->is_cached("./site/keybuylist.tpl")) {
					$datakeybuy = $category->GetListSubCategoryActiveBuy();
					$smarty->assign("datakeybuy", $datakeybuy);
				}
				$smarty->assign("countdatakeybuy", 18);
				$output_keybuy = $smarty->fetch("./site/keybuylist.tpl");
				echo minimizer($output_keybuy);
				$smarty->caching = false; 					        	
        	}
        	
		?>
        </td>
        <td width="70%" valign="top">
			<div style="width:auto;height:100%;" >
       			<?
           			$smarty->caching = true; 
           			$smarty->cache_lifetime = 10800;
           			$smarty->cache_dir = "./persistentcache"; 
           			if (!$smarty->is_cached('./site/cityb.tpl')) {
               			$data = array();
               			$data["offset"] = 0;
               			$data["rowCount"] = 0;
               			$data["sort"] = "CountB";
               			$result = $city->SelectActiveB($data);
               			$smarty->assign("data", $result);
           			} 
           			$output_cityb = $smarty->fetch("./site/cityb.tpl");
					echo minimizer($output_cityb);
           			
         			$smarty->cache_dir = "./cache";
         			$smarty->cache_lifetime = 3600;           			
           			$smarty->caching = false; 
           			
					if (isset($resultGetItem->id)) {
						$buyer->typeBizID = $resultGetItem->typeBizID;
						$_GET["subcategory"] = $resultGetItem->subTypeBizID;
						$_GET["ID"] = $resultGetItem->typeBizID;
						$res_sub = $buyer->SelectSubCategory();
						if(sizeof($res_sub) > 1)
						{
							$smarty->assign("subbizcategory", $res_sub);
							$output_subbiz = $smarty->fetch("./site/subbizbuy.tpl");
							echo minimizer($output_subbiz);
						} 
						unset($_GET["subcategory"]);
						unset($_GET["ID"]);
					}
           			
       			?>
            <div class="lftpadding_cnt w" style="padding-top:10pt; padding-bottom: 5pt;height:20pt; padding-top:0pt; padding-bottom:0pt;width:auto;" >
                <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#7C3535" style="height:100%;">
                    <tr>
                    	<td valign="middle" align="left" style="padding-left:3pt; font-size:9pt;font-family:Arial;color:White;font-weight:bold; vertical-align:middle;">���: <?=$resultGetItem->id;?></td>
                        <td valign="middle" align="center" style="padding-left:3pt; font-size:12pt;font-family:Arial;color:White;font-weight:bold; vertical-align:middle;">��������� �������� ������� �������</td>
                        <td valign="middle" align="right" style="padding-right:3pt; font-size:9pt;font-family:Arial;color:White;font-weight:bold; vertical-align:middle;"> (<?=$resultGetItem->datecreate;?>)</td>
                    </tr>
                </table>
            </div>
               <?
                 if (isset($resultGetItem->id)) {
                     $smarty->caching = true; 
                     $smarty->cache_dir = "./buycache";
                     $smarty->cache_lifetime = 86400*30;                     
                     if (!$smarty->is_cached("./site/detailsBuy.tpl", $resultGetItem->id)) {
                     	$smarty->assign("data", $resultGetItem);
                     }

                     $smarty->display("./site/detailsBuy.tpl", $resultGetItem->id);
                     
                     $smarty->cache_dir = "./cache";
                     $smarty->caching = false;  

                     ShowDetailsSearchBuyerForm();

					 SeoAdaptionDetailsBuyer($dataBuyer["ID"]);
                     
                     
                     $dataBuyer["ID"] = $resultGetItem->id;
                     
					 if(!isset($_GET["nocount"])) {
                     	$buyer->IncrementQView($dataBuyer);
					 }
                     
                    $smarty->assign("data", $resultsell);
                    if(isset($resultsell) && sizeof($resultsell) > 0) {
                    	$smarty->display("./site/sugnavsearch.tpl");
                    }
                    
					$saveme = $smarty->fetch("./site/saveme.tpl");
					echo $saveme;

                    $smarty->display("./site/ilistsell.tpl", $_SERVER["REQUEST_URI"]);
                    if(isset($resultsell) && sizeof($resultsell) > 0) {
                    	$smarty->display("./site/sugnavsearch.tpl");
                    }

                    if(isset($resultsell) && sizeof($resultsell) <= 0) {
            		?>
					<div class="lftpadding_cnt" style="padding-top:10pt; padding-bottom: 5pt;padding-top:0pt; padding-bottom:0pt; width:auto;" >
					<?
			    		$smarty->caching = true; 
          				$smarty->cache_dir = "./persistentcache"; 
          				$smarty->cache_lifetime = 10800;			    	
						if (!$smarty->is_cached("./site/keybuy.tpl")) {
							$datakeysell = $category->GetListSubCategoryActiveBuy();
							$smarty->assign("datakeysell", $datakeysell);
						}
						$output_keysell = $smarty->fetch("./site/keybuy.tpl");
						echo minimizer($output_keysell);
         				$smarty->cache_dir = "./cache";
         				$smarty->cache_lifetime = 3600;					
						$smarty->caching = false; 
					?>
					</div>
                    <?
                    }
                    
					$seocity = $city->GetSeoParent($resultGetItem->regionID);
					$dataseocity = $city->GetRegionbyParentForBuyer($resultGetItem->regionID);
					if (strlen(trim($seocity->seobuy)) > 0 && sizeof($dataseocity) > 1) {
           				?>
           				<div class="lftpadding_cnt" style="padding-top:10pt; padding-bottom: 5pt;padding-top:0pt; padding-bottom:0pt; width:auto;" >
           					<br>
							<div style="width:auto;">
								<div  style="background-image:url(http://www.bizzona.ru/images/bl.gif); background-repeat:repeat-x;">&nbsp;</div>
							</div>
           				</div>
           				<?
						$smarty->assign("seocity", $seocity);
						$smarty->assign("dataseocity", $dataseocity);
						$smarty->display("./site/hsubregionbuyer.tpl");
					}
                 }
               ?>
            </div>
        </td>
    </tr>
</table>
<div style="padding-top:4pt;padding-bottom:5pt;padding-right:10pt;padding-left:10pt;font-size:10pt;font-family:arial;">
	<table cellpadding="0" cellspacing="0" border="0"  style="width:100%;" bgcolor="#eeeee0">
    	<tr>
        	<td style="background-image:url(http://<?=$_SERVER["HTTP_HOST"]?>/images/d1.gif); background-repeat:no-repeat;width:5px;height:5px; background-position:left top;">&nbsp;</td>
            <td rowspan="2" valign="middle" align="center"  style="font-size:8pt; font-family:Arial; color:black;">
				<h1 style='margin:0pt;padding:0pt;font-family:arial;font-size:8pt;text-align:center;margin-bottom:0pt;color:#000;' >��� "������-����" - <?=$seoname;?> �������� ����� ����������� ������� �� �������.</h1> 
            </td>
            <td style="background-image:url(http://<?=$_SERVER["HTTP_HOST"]?>/images/d2.gif); background-repeat:no-repeat;width:5px;height:5px;background-position:right top;">&nbsp;</td>
        </tr>
        <tr>
            <td style="background-image:url(http://<?=$_SERVER["HTTP_HOST"]?>/images/d4.gif); background-repeat:no-repeat;width:5px;height:5px; background-position:left bottom;">&nbsp;</td>
            <td style="background-image:url(http://<?=$_SERVER["HTTP_HOST"]?>/images/d3.gif); background-repeat:no-repeat;width:5px;height:5px; background-position:right bottom;">&nbsp;</td>
        </tr>
    </table>
</div>
<?
	$smarty->display("./site/footer.tpl");
?>
<?
  $end = get_formatted_microtime(); 
  $total = $end - $start;
  echo "<center><span style='font-size:7pt;'>".round($total, 6)." : ".$count_sql_call." </span><center>";
?>
</body>
</html>
<?
	$sell->Close();
	$category->Close();
	$city->Close();
	$country->Close();
	$buyer->Close();
	$metro->Close();	
	$district->Close();
	$subcategory->Close();
    $streets->Close();
?>