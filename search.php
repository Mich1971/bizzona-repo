<?php 

  global $DATABASE;
  include_once("./phpset.inc");
  include_once("./engine/functions.inc"); 

  $count_sql_call = 0;
  $start = get_formatted_microtime(); 
  $base_memory_usage = memory_get_usage();	


  AssignDataBaseSetting();
  
  include_once("./engine/class.category_lite.inc"); 
  $category = new Category_Lite();

  include_once("./engine/class.country_lite.inc"); 
  $country = new Country_Lite();

  include_once("./engine/class.city_lite.inc");  
  $city = new City_Lite();

  include_once("./engine/class.sell_lite.inc"); 
  $sell = new Sell_Lite();

  include_once("./engine/class.buy_lite.inc"); 
  $buyer = new Buyer_Lite();

  include_once("./engine/class.equipment_lite.inc"); 
  $equipment = new Equipment_Lite();
  
  include_once("./engine/class.metro_lite.inc"); 
  $metro = new Metro_Lite();

  include_once("./engine/class.district_lite.inc"); 
  $district = new District_Lite();

  include_once("./engine/class.subcategory_lite.inc");
  $subcategory = new SubCategory_Lite();

  include_once("./engine/class.streets_lite.inc");
  $streets = new Streets_Lite();

  	include_once("./engine/class.company.inc"); 
  	$company = new Company();    
  	
  	include_once("./engine/class.ad.inc");
  	$ad = new Ad();  	  	
  
  include_once("./engine/class.search.inc");
  include_once("./engine/class.stem.inc");
  
  
  include_once("./engine/class.bzn_items.inc");
  
  $bzn_items = new BZN_Items();

  if(IsSetGet($_GET["searchcode"])) {
      
      $result_info = $bzn_items->GetTypeInfoById(array(
          'codeId' => (int)$_GET["searchcode"]
      ));
      
      if(sizeof($result_info) == 1) {
          header ("Location: ".$result_info[0]['url']);
      }
      
  }
  
  
  $search = new search();
  $stemmer = new Lingua_Stem_Ru();

  include_once("./engine/class.frontend.inc");

  require_once("./libs/Smarty.class.php");
  $smarty = new Smarty;
  $smarty->template_dir = "./templates";
  $smarty->compile_dir  = "./templates_c";
  //$smarty->cache_dir = "./cache";
  $smarty->compile_check = true;
  $smarty->debugging     = false;
  
  require_once("./regfuncsmarty.php");
  
	$smarty->assign("listcompany", $company->ListCompanyInList());  	  	  	  	  
  
  $assignDescription = AssignDescription();
  
  //if(IsSetGet($_GET["searchcode"])) {
  
  	//$code = intval($_GET["searchcode"]);
  	//header ("Location: http://www.bizzona.ru/detailsSell/".$code."");
  		
  //}
  
  
  
?>
<?
  $dataCategory = array();
  $dataCategory["offset"] = 0;
  $dataCategory["rowCount"] = 0;
  $dataCategory["sort"] = "Count";
  $resultCategory = $category->Select($dataCategory);
  echo $sqlCategory;
  $smarty->assign("dataCategory", $resultCategory);
?>
<?
  $dataCity = array();
  $dataCity["offset"] = 0;
  $dataCity["rowCount"] = 0;
  $dataCity["sort"] = "Count";
  $resultCity = $city->SelectActive($dataCity);
  $smarty->assign("dataCity", $resultCity);
?>
<?
  if (isset($_GET["CategoryID"]) && is_array($_GET["CategoryID"])) {
    while(list($kCat, $vCat) = each($_GET["CategoryID"])) {
      $_GET["CategoryID"][$kCat] = intval($vCat);
    } 
    $smarty->assign("afterDataCategory", $_GET["CategoryID"]);
    $strCategoryID = implode(",", $_GET["CategoryID"]);
    $_GET["CategoryID"] = $strCategoryID;
  } else if (isset($_GET["CategoryID"]) && strlen($_GET["CategoryID"]) > 0) {
    $aCat = split("[,]", trim($_GET["CategoryID"]));
    if (is_array($aCat) && sizeof($aCat) > 0) {
      while (list($k, $v) = each($aCat)) {
        $aCat[$k] = intval($v);
      }
      reset($aCat);        
      $strCategoryID = implode(",", $aCat);
      $_GET["CategoryID"] = $strCategoryID;
      while (list($k, $v) = each($aCat)) {
        $tempCat[$v] = $v;
      }
      if (isset($tempCat)) {
        $smarty->assign("afterDataCategory", $tempCat);
      }
    } else {
    }
  } else {
  }

  if (isset($_GET["CityID"]) && is_array($_GET["CityID"])) {
    while(list($kCity, $vCity) = each($_GET["CityID"])) {
       $_GET["CityID"][$kCity] = intval($vCity);
    }
    $smarty->assign("afterDataCity", $_GET["CityID"]);
    $strCityID = implode(",", $_GET["CityID"]);
    $_GET["CityID"] = $strCityID;
  } else if (isset($_GET["CityID"]) && strlen($_GET["CityID"]) > 0) {
    $aCit = split("[,]", trim($_GET["CityID"]));
    if (is_array($aCit) && sizeof($aCit) > 0) {
      while (list($k, $v) = each($aCit)) {
        $aCit[$k] = intval($v);
      } 
      reset($aCit);      
      $strCityID = implode(",", $aCit);
      $_GET["CityID"] = $strCityID;
      while (list($k, $v) = each($aCit)) {
        $tempCit[$v] = $v;
      }
      if (isset($tempCit)) {
        $smarty->assign("afterDataCity", $tempCit);
      }
    } else {
    }
  } else {
  }

  if (isset($_GET["add"])) {
     switch (trim($_GET["add"])) {
        case "c": {
          $sort = "CostID";
        } break;
        case "d":  {
          $sort = "ID";
        } break;
        default:
         $sort = "ID";
     }
  } else {
    $sort = "ID"; 
  }
  
	$ad->InitAd($smarty, $_GET);    
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
  	<HEAD>
      	<title>������� � ������� �������. ������� ������ - ������ ����</title>		
		<META NAME="Description" CONTENT="������� �������, ������� �������, ������� ������, ������� �������, ������� �������� �������, ������� �������, ������� ��������,  ������� ��������, ������� ��������,���� ������� ������?">
        <meta name="Keywords" content="������� �������, ������� �������, ������� ������, ������� �������, ������� �������� �������, ������� �������, ������� ��������,  ������� ��������, ������� ��������, ���� ������� ������?, Ready business, �����������, �������� � ��������, ���������, ����������, �����������"> 
        <link rel="shortcut icon" href="./images/favicon.ico">
        <LINK href="http://www.bizzona.ru/general.css" type="text/css" rel="stylesheet">
        <meta http-equiv="content-type" content="text/html; charset=windows-1251"/>
	<script type="text/javascript" src="ja.js"></script>
  	</HEAD>

<?php
  $smarty->display("./site/headerbanner.tpl");
?>

	<?
	    $topmenu = GetMenu();
		$smarty->assign("topmenu", $topmenu);
	
		$link_country = GetSubMenu();
		$smarty->assign("ad", $link_country);
	
		//$smarty->display("./site/header.tpl");
		$output_header = $smarty->fetch("./site/header.tpl");
		echo minimizer($output_header);
		
	?>
<table class="w" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td width="75%" style="padding-top:2pt;" valign="top">
		<?php
                        if(isset($result_info) && sizeof($result_info) > 1) {
                            ?>
                                <div style="background-color: #f3e9e9;padding:10px;border: 1px solid #ddd;border-radius: 7px;box-shadow: 0 0 5px #eee;margin:5px;">
                                <h1>�� ���������� ���� ������ ������� ��������� ����������:</h1>
                    
                            <?php
                                ?><ul><?php
                                foreach($result_info as $k => $v) {
                                    ?><li><?=$v['prefix'];?> <a href="<?=$v['url'];?>" class="city_a" style="font-size:12pt;" target="_blank" ><?=$v['url'];?></a></li><?php
                                }
                                ?></ul>
                                </div>        
                                <?php
                        }
                        
                        
			$message_by_search = "�� ������ ������� ������ �������� ������� �� �������.";
		
			$str = $_GET["searchtext"];
			
			$str = ParsingIndexingSell($str);
			
			$_GET["searchtext"] = $str;
			
			if(!isset($_SESSION["searchlist"])) {
				$array_history = array();
				$array_history[] = $_GET["searchtext"]; 
				$_SESSION["searchlist"] = serialize($array_history);
			} else {
				$array_history = unserialize($_SESSION["searchlist"]);
				array_push($array_history, $_GET["searchtext"]);
				$_SESSION["searchlist"] = serialize($array_history);
			}

			if(strlen($str) > 80) {
				$str = substr($str,0,80);
			}
			
			if(strlen($str) <= 0)
			{
				$message_by_search = "����� ������ ��������� ������. ";
			} else {
				st_search();
			}
			
			$arr = split("[ ]", $str);
	
			while (list($k, $v) = each($arr)) {
				$arr[$k] = $stemmer->stem_word($v);
			}
	
			$search->awords = $arr;
		
			$data = array();
			$dataPage = array();
			$pagesplit = 10;//GetPageSplit();

			if (isset($_GET["offset"])) {
				$data["offset"] = intval($_GET["offset"]);
			} else {
				$data["offset"] = 0;
			}
			
			if (isset($_GET["rowCount"])) {
				$data["rowCount"] = intval($_GET["rowCount"]);
			} else {
				$data["rowCount"] = $pagesplit;
			}

			$data["sort"] = $sort;

			if (strlen($_GET["CategoryID"]) > 0) {
				$data["CategoryID"] = $_GET["CategoryID"];
				$dataPage["CategoryID"] = $_GET["CategoryID"];
			}

			if (strlen($_GET["CityID"]) > 0) {
				$data["CityID"] = $_GET["CityID"];
				$dataPage["CityID"] = $_GET["CityID"];
			}

			$dataPage["offset"] = 0;
			$dataPage["rowCount"] = 0;
			$dataPage["sort"] = $sort;
			$dataPage["StatusID"] = "-1";
			$resultPage = $search->CountSelectSell($data, false);

			if ($resultPage > 0) {

				$smarty->assign("CountRecord", $resultPage);
				$smarty->assign("CountSplit", $pagesplit);
				$smarty->assign("CountPage", ceil($resultPage/10));

				$data["StatusID"] = "-1";
				$data["sort"] = $sort;

				
				$result = $search->SelectSell($data, false);
				  
				$smarty->assign("data", $result);
				//$smarty->display("./site/fullpagesplit.tpl");

                $saveme = $smarty->fetch("./site/saveme.tpl");
                echo $saveme;  

				
				$output_fullpagesplit = $smarty->fetch("./site/fullpagesplit.tpl");
				echo minimizer($output_fullpagesplit);
                    
				//$smarty->display("./site/listsell.tpl");

                $smarty->assign('colorsell', FrontEnd::ListColorSet());

				$output_listsell = $smarty->fetch("./site/listsell.tpl");
				echo minimizer($output_listsell);
				?>
					<div style="width:auto;">
						<div  style="background-image:url(images/bl.gif); background-repeat:repeat-x;">&nbsp;</div>
					</div>
				<?
				//$smarty->display("./site/fullpagesplit.tpl");
				$output_fullpagesplit = $smarty->fetch("./site/fullpagesplit.tpl");
				echo minimizer($output_fullpagesplit);
			} else {
				?>
					<h1 align='center' class='listMessage' style='font-size:11pt;font-family:verdana;color:#7C3535;'><b><?=$message_by_search;?></b></h1> 
				<?
			}
		?>


		<?
			include_once("./yadirect.php");
		?>

<div class="adv">
<h3>���� ������������  � ������� ��� ������� �������:</h3>
<ol>
<li>	���������� ������� ��� ������� ������� ���������� ����� � ���������� ����������� �������� ��������� �� ������� �������� �������. 
<li>	���������� ���������� � ������� �������� ������� � �������� ������� � �<b>������� ������</b>�
<li>	�������������� �������� ����������� �� ������� ������� ������������� ����������� �������� �������.
<li>	���������� ��������� ���������� ������� ��� ������� �������.
<li>	������ ���������� �����������. ���������� ������ ����� ��� � ������ �������. ��� �������� �������. ������� ���������� �� ������. �� ��������� ���������.
<li>	������ ������� � ����� �������� ���������� ������� � ��������� ��������.
<li>	��� ����������� ���������� � ��� ������ ����������� ����� ��  ������� ��� ������� ������ ��������.
<li>	��������� ������� ��������� �����������.
<li>	������� �������������. <b>������ ������� � ��� �� ������ ����� ��� �������!</b>
<li>	�� ������������� ������ ����� �� �������� �����, ����������� ����� �������� ������ ��������� ���������� ������� �� �������.
<li>	˸���� ������ �� ���������� ���������� � ������� ��� ������� �������. ��� ���������� ����� ���� ��������� ��� ���������. ������ �������� � ������� ������ ��������. ����� �� ������.
<li>	������ ������� - �����  �������� ��� ����� 3 ��� � �������������� ���� ��� ����� �������� ������.
</ol>
</div>

<?
	$smarty->caching = true; 
	$smarty->cache_lifetime = 10500;
					
	if (!$smarty->is_cached('./site/sugequipment.tpl', 'fullsearch')) {
           
		$datasugequipment = array();
		$datasugequipment["offset"] = 0;
		$datasugequipment["rowCount"] = 3;
		$datasugequipment["sort"] = "datecreate";
		$datasugequipment["status_id"] = "-1";
            	 
		$res_datasugequipment = $equipment->SugSelect($datasugequipment);
  	          	
		$smarty->assign("datasugequipment", $res_datasugequipment);        	
	}				

	$output_sugequipment = $smarty->fetch("./site/sugequipment.tpl", 'fullsearch');
	echo fminimizer($output_sugequipment);
					
	$smarty->caching = false; 
?>


		</td>
		<td width="25%" valign="top" style="padding-top:2pt;">

        <?
        		$smarty->caching = false; 
				echo fminimizer($smarty->fetch("./site/inner_bannertop_right.tpl"));
		
				$output_request = $smarty->fetch("./site/request.tpl");
				echo minimizer($output_request);
            	
				//$smarty->display("./site/proposal.tpl");

                /*
				$smarty->caching = true; 
				$smarty->cache_lifetime = 300;
				
				if (!$smarty->is_cached('./site/hotsell.tpl')) {
					$data = array();
					$data["offset"] = 0;
					$data["rowCount"] = 0;
					$data["viptopposition"] = 1;
					$data["sort"] = "DataCreate";
					//$data["StatusID"] = "2";     
					$data["rightblock"] = 1;         
					$result = $sell->Select($data);
					$smarty->assign("data", $result);

					if (isset($_SESSION["listID"])) { 
						$dataPage["rowCount"] = 0;
						$dataPage["offset"] = 0;
						unset($_SESSION["listID"]);
					}
					while (list($ld, $lv) = each($result)) {
						$_SESSION["listID"][] = $lv->ID; 
					}
				}

				$hotselljs = $smarty->fetch("./site/hotselljs.tpl");
				echo minimizer($hotselljs);


				$output_hotsell = $smarty->fetch("./site/hotsell.tpl");
				echo fminimizer($output_hotsell);
				$smarty->caching = false;
                */

            $smarty->caching = false;

            $resTopRight = FrontEnd::RightBlockTopSell();
            $resRight = FrontEnd::RightBlockSell();

            $smarty->assign("data", array_merge($resTopRight,$resRight));

            echo fminimizer($smarty->fetch("./site/hotsellvip.tpl"));

            $smarty->caching = false;


            $datahotbuyer = array();
            	$datahotbuyer["offset"] = 0;
            	$datahotbuyer["rowCount"] = 4;
            	$datahotbuyer["sort"] = "datecreate";
            	$datahotbuyer["StatusID"] = "-1";
            	
  	          	$res_datahotbuyer = $buyer->Select($datahotbuyer);
    			$smarty->assign("databuyer", $res_datahotbuyer);
            	//$smarty->display("./site/blockbuyer.tpl");
            	
   	  		    $output_blockbuyer = $smarty->fetch("./site/blockbuyer.tpl");
		  	    echo minimizer($output_blockbuyer);
		  	    
				$smarty->caching = true; 
				$smarty->cache_lifetime = 3600;
				
				if (!$smarty->is_cached('./site/hotequipment.tpl')) {
            
            		$datahotequipment = array();
            		$datahotequipment["offset"] = 0;
            		$datahotequipment["rowCount"] = 4;
            		$datahotequipment["sort"] = "datecreate";
            		$datahotequipment["status_id"] = "-1";
            	
  	          		$res_datahotequipment = $equipment->Select($datahotequipment);
  	          	
    				$smarty->assign("dataequipment", $res_datahotequipment);

              	}
				//$smarty->display("./site/hotequipment.tpl");
             
				$output_hotequipment = $smarty->fetch("./site/hotequipment.tpl");
				echo minimizer($output_hotequipment);
 
				$output_proposal = $smarty->fetch("./site/proposal.tpl");
				echo minimizer($output_proposal);
				
				$smarty->caching = false; 
			?>
		</td>
    </tr>
</table>
<?
	//$smarty->display("./site/sape.tpl");
?>
<style> td a  {   color:black; }</style>
<div style="padding-top:4pt;padding-bottom:5pt;padding-right:10pt;padding-left:10pt;font-size:10pt;font-family:arial;">
	<table cellpadding="0" cellspacing="0" border="0"  style="width:100%;padding-bottom:2pt;" bgcolor="#eeeee0">
    	<tr>
        	<td style="background-image:url(<?=$_SERVER["host_name"]?>images/d1.gif); background-repeat:no-repeat;width:5px;height:5px; background-position:left top;">&nbsp;</td>
            <td rowspan="2" valign="middle" align="center"  style="font-size:8pt; font-family:Arial; color:black;">
			<?
			?>
            </td>
            <td style="background-image:url(<?=$_SERVER["host_name"]?>images/d2.gif); background-repeat:no-repeat;width:5px;height:5px;background-position:right top;">&nbsp;</td>
        </tr>
        <tr>
            <td style="background-image:url(<?=$_SERVER["host_name"]?>images/d4.gif); background-repeat:no-repeat;width:5px;height:5px; background-position:left bottom;">&nbsp;</td>
            <td style="background-image:url(<?=$_SERVER["host_name"]?>images/d3.gif); background-repeat:no-repeat;width:5px;height:5px; background-position:right bottom;">&nbsp;</td>
        </tr>
    </table>
</div>
<?
	$smarty->display("./site/footer.tpl");
?>
<?
  $end = get_formatted_microtime(); 
  $total = $end - $start;
  echo "<center><span style='font-size:7pt;'>".round($total, 6)." : ".$count_sql_call." : ".$base_memory_usage." : ".memoryUsage(memory_get_usage(), $base_memory_usage)." </span><center>";
?>
</body>

</html>
<?
	$sell->Close();
	$category->Close();
	$city->Close();
	$country->Close();
	$buyer->Close();
	$equipment->Close();
	$metro->Close();
	$district->Close();
	$subcategory->Close();
	$streets->Close();
?>
