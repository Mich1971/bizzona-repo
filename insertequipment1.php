<?
  global $DATABASE;
  
  include_once("./phpset.inc");
  
  include_once("./engine/functions.inc"); 
  AssignDataBaseSetting();
  include_once("./engine/class.category.inc"); 
  $category = new Category();

  include_once("./engine/class.subcategory.inc"); 
  $subCategory = new SubCategory();  
  
  include_once("./engine/class.country.inc"); 
  $country = new Country();

  include_once("./engine/class.city.inc"); 
  $city = new City();

  include_once("./engine/class.sell.inc"); 
  $sell = new Sell();

  include_once("./engine/class.equipment.inc"); 
  $equipment = new Equipment();

  
  require_once("./libs/Smarty.class.php");
  $smarty = new Smarty;
  $smarty->template_dir = "./templates";
  $smarty->compile_dir  = "./templates_c";
  //$smarty->cache_dir = "./cache";
  $smarty->compile_check = true;
  $smarty->debugging     = false;

  $smarty->register_function('NameCity', 'getNameCity');

  $assignDescription = AssignDescription();

  $text_error = "";

  if (isset($_POST) && sizeof($_POST) > 0) {
	
  	$ErrorFile = false;
  	
    $max_image_size	= 128 * 1024;
    $valid_types 	=  array("gif","jpg", "png", "jpeg");

    $file_icon = "";
    
    if (isset($_FILES["icon"])) {

		$newname = time(); 
		if (is_uploaded_file($_FILES['icon']['tmp_name'])) {
			
        	$filename = $_FILES['icon']['tmp_name'];
        	$ext = substr($_FILES['icon']['name'], 1 + strrpos($_FILES['icon']['name'], "."));
        	if (filesize($filename) > $max_image_size) {
          		$text_error = '������: ������ ����� ������ 128K.';
          		$newname = ""; 
        	} else if (!in_array($ext, $valid_types)) {
          		$text_error = '������: ���������� ����� ����������� ���������� ����� �� ����������� ���';
          		$newname = ""; 
        	} else {
        		$file_icon = $newname.".".$ext;
          		if (move_uploaded_file($filename, dirname($SCRIPT_FILENAME)."/simg/".$file_icon)) {
	   				//echo 'File successful uploaded.';
	   			} else {
	   				$text_error =  '������ : ���������� ��������� ���������� ���������� �� ������� � webmaster@bizzona.ru.';
           			$newname = ""; 
	  			}
    		}
       	} else {
        	$newname = "";
       	}
       	
    } 
  	
  	
	if (!$ErrorFile) {

		$t_name = htmlspecialchars(trim($_POST["name"]), ENT_QUOTES);
		$_POST["name"] = $t_name;
		
    	$t_age = htmlspecialchars(trim($_POST["age"]), ENT_QUOTES);
    	$_POST["age"] = $t_age;
    	
		$t_cost = htmlspecialchars(trim($_POST["cost"]), ENT_QUOTES);
		$_POST["cost"] = $t_cost;
		
		$t_ccost = htmlspecialchars(trim($_POST["ccost"]), ENT_QUOTES);
		$_POST["ccost"] = $t_ccost;
		
		$t_category_id = htmlspecialchars(trim($_POST["category_id"]), ENT_QUOTES);
		$_POST["category_id"] = $t_category_id;
		
  	  	$total_typeBizID = htmlspecialchars(trim($_POST["category_id"]), ENT_QUOTES);
  	  	$arr_typeBizID = split("[:]",$total_typeBizID);
  	  	if(sizeof($arr_typeBizID) == 2) {
  			$data["category_id"] = intval($arr_typeBizID[0]);
  			$data["subCategory_id"] = intval($arr_typeBizID[1]);
  			$_POST["subCategory_id"] = $data["subCategory_id"];
  			$t_category_id = $data["category_id"];
  			$t_subCategory_id = $data["subCategory_id"];
  	  	} else {
  	  		$data["category_id"] = "-1";
  			$data["subCategory_id"] = "-1";
  			$t_category_id = $data["category_id"];
  			$t_subCategory_id = $data["subCategory_id"];
  	  	}
		
		
		$t_description = htmlspecialchars(trim($_POST["description"]), ENT_QUOTES);
		$_POST["description"] = $t_description; 
		
		$t_city_id = "";
		$t_city_text = htmlspecialchars(trim($_POST["city_text"]), ENT_QUOTES);
		$_POST["city_text"] = $t_city_text;
		
		$t_icon = $file_icon;
		$t_email = htmlspecialchars(trim($_POST["email"]), ENT_QUOTES);
		$_POST["email"] = $t_email;
		
		$t_contact_face = htmlspecialchars(trim($_POST["contact_face"]), ENT_QUOTES);
		$_POST["contact_face"] = $t_contact_face;
		
		$t_phone = htmlspecialchars(trim($_POST["phone"]), ENT_QUOTES);
		$_POST["phone"] = $t_phone;
		
		$t_status_id = "";
		$t_datecreate = "";
		$t_duration_id = htmlspecialchars(trim($_POST["duration_id"]), ENT_QUOTES);
		$_POST["duration_id"] = $t_duration_id;
		
		$t_statussend_id = "";
		$t_qview = "";     
      
		if (strlen($t_name) <= 0) {
			
        	$text_error = "������ �����: "."\"�������� ������������\"";
        	$smarty->assign("ErrorInput_name", true);
        	
      	} else if(strlen($t_cost) <= 0) {
      		
      		$text_error = "������ �����: "."\"���������\"";
      		$smarty->assign("ErrorInput_cost", true);
      		
		} else if(strlen($t_city_text) <= 0) {
			
			$text_error = "������ �����: "."\"������������\"";
			$smarty->assign("ErrorInput_city_text", true);
			
		} else if (strlen($t_description) <= 0) {
			
			$text_error = "������ �����: "."\"������� ��������\"";
			$smarty->assign("ErrorInput_description", true);
			
		} else if (strlen($t_contact_face) <= 0) {
			
			$text_error = "������ �����: "."\"���������� ����\"";
			$smarty->assign("ErrorInput_contact_face", true);
			
		} else if(strlen($t_phone) <= 0) {
			
			$text_error = "������ �����: "."\"��������\"";
			$smarty->assign("ErrorInput_phone", true);
			
		} else if(strlen($t_email) <= 0) {
			
			$text_error = "������ �����: "."\"Email\"";
			$smarty->assign("ErrorInput_email", true);
			
		}
		
		
      	if(strlen($text_error) > 0)
      	{
      		$smarty->assign("text_error", $text_error);
  			//$headers1 = 'BizZONA.RU: new looked' . "\r\n" . 'Reply-To:  ' . "\r\n" . 'X-Mailer: PHP/' . phpversion();
			//mail("staff@bizzona.ru", "������ ".$error."", "PHP_SELF: ".$_SERVER["PHP_SELF"]."\r\n"." HTTP_HOST: ".$_SERVER["HTTP_HOST"]."\r\n"."HTTP_REFERER: ".$_SERVER["HTTP_REFERER"]."\r\n"."REMOTE_ADDR: ".$_SERVER["REMOTE_ADDR"]."\r\n" , $headers1);
      	}

      	if (strlen($text_error) <= 0) {

			$equipment->name = $t_name;
			$equipment->age = $t_age;
			$equipment->cost = $t_cost;
			$equipment->icon = $t_icon;
			$equipment->ccost = $t_ccost;
			$equipment->category_id = $t_category_id;
			$equipment->subCategory_id = $t_subCategory_id;
			
		
			
			$equipment->description = $t_description;
			$equipment->city_text = $t_city_text;
			$equipment->email = $t_email;
			$equipment->contact_face = $t_contact_face;
			$equipment->phone = $t_phone;
			$equipment->duration_id = $t_duration_id;
			
			$equipment->Insert();
			
			$smarty->assign("text_success", "�������� ������������ ���� ������� ���������. � �������� ���� ���������� ����� ����������.");
			
			
        	$to      = 'bizzona.ru@gmail.com';
        	$subject = 'new equipment: ';
        	$message = "�������� ������������: ".$equipment->name."\n";
        	$message.= "������������ ��������������: ".$equipment->age."\n";
			$message.= "���������: ".$equipment->cost."\n";
			$message.= "����������: ".$equipment->icon."\n";
			$message.= "��� ������: ".$equipment->ccost."\n";
			$message.= "��� ������������: ".$equipment->category_id."\n";
			$message.= "������� ��������: ".$equipment->description."\n";
			$message.= "������������: ".$equipment->city_text."\n";
			$message.= "Email: ".$equipment->email."\n";
			$message.= "���������� ����: ".$equipment->contact_face."\n";
			$message.= "��������: ".$equipment->phone."\n";
			$message.= "������������: ".$equipment->duration_id."\n";
        	$headers = 'BizZONA.RU: new equipment' . "\r\n" .
        	'Reply-To: '.$data["EmailID"].' ' . "\r\n" .
        	'X-Mailer: PHP/' . phpversion();
        	mail($to, $subject, $message, $headers);
			
			
        	unset($_POST);
      }
    }
  } 


  $data["offset"]   = 0;
  $data["rowCount"] = 0;
  $data["sort"]= "Count";
  $listCategory = $category->Select($data); 

  unset($data["sort"]);
  $data["offset"]   = 0;
  $data["rowCount"] = 0;
  $listCity = $city->Select($data); 



  $smarty->assign("listCategory", $listCategory);
  $smarty->assign("listCity", $listCity);
  
  $listSubCategory = $subCategory->Select($data);
  $smarty->assign("listSubCategory", $listSubCategory);


?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>

  <HEAD id="Head">
      <title>���������� ������� ������������ - ������ ����</title>		
	<META NAME="Description" CONTENT="������� ������������, ������� �������, ������� �������, ������� ������, ������� �������, ������� �������� �������, ������� �������, ������� ��������,  ������� ��������, ������� ��������, ���� ������� ������?">
        <meta name="Keywords" content="������� ������������, ������� �������, ������� �������, ������� ������, ������� �������, ������� �������� �������, ������� �������, ������� ��������,  ������� ��������, ������� ��������, ���� ������� ������?, Ready business, �����������, �������� � ��������, ���������, ����������, �����������"> 
        <LINK href="./general.css" type="text/css" rel="stylesheet">
        <script src="http://tools.spylog.ru/counter.js" type="text/javascript"> </script>
  </HEAD>


<?
	$smarty->display("./site/headere.tpl");
?>
  


<table class="w" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td width="100%">
         <table width='100%' height='100%'>
           <tr>
             <td valign='top' width='60%' height='100%'>
               <?
                  $smarty->display("./site/insertequipment1.tpl");
               ?>
             </td>
           </tr>
         </table>
        </td>
    </tr>
</table>

<?
	//$smarty->display("./site/sape.tpl");
?>

<style>
 td a 
 {
   color:black;
 }
</style>

<div style="padding-top:4pt;padding-bottom:5pt;padding-right:10pt;padding-left:10pt;font-size:10pt;font-family:arial;">
	<table cellpadding="0" cellspacing="0" border="0"  style="width:100%;padding-bottom:2pt;" bgcolor="#eeeee0">
    	<tr>
        	<td style="background-image:url(<?=$_SERVER["host_name"]?>images/d1.gif); background-repeat:no-repeat;width:5px;height:5px; background-position:left top;">&nbsp;</td>
            <td rowspan="2" valign="middle" align="center"  style="font-size:8pt; font-family:Arial; color:black;">
			<?
     			define('_SAPE_USER', 'b91a4da0b21c4d197163d613e54e8d28'); 
     			require_once($_SERVER['DOCUMENT_ROOT'].'/'._SAPE_USER.'/sape.php'); 
     			$sape = new SAPE_client();
     			echo $sape->return_links();
			?>
            </td>
            <td style="background-image:url(<?=$_SERVER["host_name"]?>images/d2.gif); background-repeat:no-repeat;width:5px;height:5px;background-position:right top;">&nbsp;</td>
        </tr>
        <tr>
            <td style="background-image:url(<?=$_SERVER["host_name"]?>images/d4.gif); background-repeat:no-repeat;width:5px;height:5px; background-position:left bottom;">&nbsp;</td>
            <td style="background-image:url(<?=$_SERVER["host_name"]?>images/d3.gif); background-repeat:no-repeat;width:5px;height:5px; background-position:right bottom;">&nbsp;</td>
        </tr>
    </table>
</div>

<?
	$smarty->display("./site/footer.tpl");
?>

</body>
</html>
