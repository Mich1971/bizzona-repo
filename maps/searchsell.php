<?
  global $DATABASE;

  include_once("../phpset.inc");
  
  include_once("../engine/functions.inc"); 

  	$count_sql_call = 0;
	$start = get_formatted_microtime();   
  	$base_memory_usage = memory_get_usage();	

	//ini_set("display_startup_errors", "on");
	//ini_set("display_errors", "on");
	//ini_set("register_globals", "on");

 	AssignDataBaseSetting("../config.ini");
  
	include_once("../engine/class.category_lite.inc"); 
	$category = new Category_Lite();

	include_once("../engine/class.country_lite.inc"); 
	$country = new Country_Lite();

	include_once("../engine/class.city_lite.inc"); 
	$city = new City_Lite();

	include_once("../engine/class.sell_lite.inc");  
	$sell = new Sell_Lite();

	include_once("../engine/class.buy_lite.inc"); 
	$buyer = new Buyer_Lite();
  
  	include_once("../engine/class.metro_lite.inc"); 
  	$metro = new Metro_Lite();
  
  	include_once("../engine/class.district_lite.inc"); 
  	$district = new District_Lite();

  	include_once("../engine/class.subcategory_lite.inc");
  	$subcategory = new SubCategory_Lite();
  
  include_once("../engine/class.geocode.inc"); 
  $geocode = new GeoCode();

  	include_once("../engine/class.streets_lite.inc");
  	$streets = new Streets_Lite();
  
  
  require_once("../libs/Smarty.class.php");
  $smarty = new Smarty;
  $smarty->template_dir = "../templates";
  $smarty->compile_dir  = "../templates_c";
  $smarty->cache_dir = "../cache";
  $smarty->compile_check = true;
  $smarty->debugging     = false;

  require_once("../regfuncsmarty.php");
  
  $longitude = "55.755786";
  $latitude = "37.617633";
  $zoom = 12;
  
  if(isset($_GET["categorySell"])) {
  	$_GET["categorySell"] = intval($_GET["categorySell"]);
  }
  
  if(isset($_GET["subCategorySell"])) {
  	$_GET["subCategorySell"] = intval($_GET["subCategorySell"]);
  }
  
  if(isset($_GET["subRegionSell"])) {
  	$_GET["subRegionSell"] = intval($_GET["subRegionSell"]);
  }
  
  if(isset($_GET["regionSell"])) {
	$_GET["regionSell"] = intval($_GET["regionSell"]);
  }
  
  if(isset($_GET["categorySell"]) && intval($_GET["categorySell"]) > 0) {
  	$geocode->categorySell = intval($_GET["categorySell"]);
  }

  if(isset($_GET["subCategorySell"]) && intval($_GET["subCategorySell"]) > 0) {
  	$geocode->subCategorySell = intval($_GET["subCategorySell"]);
  }
  

  if(isset($_GET["subRegionSell"]) && intval($_GET["subRegionSell"]) > 0) {
  	
  	$geocode->subRegionSell = intval($_GET["subRegionSell"]);
  	$city->id = intval($_GET["subRegionSell"]);
  	$geocity = $city->GetGeoData();
  	
  	if(isset($geocity->statusGeoCode) && $geocity->statusGeoCode = G_GEO_SUCCESS) {
		$longitude = $geocity->longitude;
		$latitude = $geocity->latitude;
		$zoom = $geocity->zoom;
  	}
  } else {

  	if(isset($_GET["regionSell"]) && intval($_GET["regionSell"]) > 0) {
  		$geocode->regionSell = intval($_GET["regionSell"]);
  		$city->id = intval($_GET["regionSell"]);
  		$geocity = $city->GetGeoData();
  		if(isset($geocity->statusGeoCode) && $geocity->statusGeoCode = G_GEO_SUCCESS) {
			$longitude = $geocity->longitude;
			$latitude = $geocity->latitude;
			$zoom = $geocity->zoom;
  		}
  	}

  }
  
  
  $arrSell = array();
  
  if(	intval($geocode->regionSell) > 0 || 
  		intval($geocode->categorySell) || 
  		intval($geocode->subRegionSell) > 0 ||
  		intval($geocode->subCategorySell) > 0) {
  		$arrSell = $geocode->SelectSell();
  		
		$smarty->assign("countshow", sizeof($arrSell));
  }

  function GetCoordCircle($longitude="55.755786", $latitude="37.617633", $rad=5) {
	  $arr = array();	
	  
	  for($j=0.001; $j <= $rad; $j += 0.002) {
	  
  	  	for ($i=0; $i<=2* M_PI; $i+=0.005) {
  			$x = $longitude + $j*cos($i);
  			$y = $latitude + $j*sin($i);
  			$arr[] = array("x" => $x, "y" => $y);
  	  	}
	  }
  	  return $arr;
  }

  $cood_circle = GetCoordCircle($longitude, $latitude, $rad=0.02);
  
  function MapShortDescription($data) {

  	  	$str = "<div style='text-align:left;border: 2px solid #006699; background: #eeeee0; width:300pt;height:100%;padding-top:1pt;' >";
  	  	
		$str .= "<table height='100%' valign='top'>";
		$str .= "<tr><td  width='100%'  valign='top'><div style='color:#7c3535;font-family:arial;font-size:10pt;font-weight:bold;'>".$data['ShortDetailsID']."</div></td></tr>";
		$str .= "</table>";
  	  	
  	  	$str .= "</div>";
  	
  	  	return $str;
  }
  
  function MapContent($data) {
  	
  	$str = "<div style='text-align:left;border: 2px solid #006699; background: #d9d9b9; width:300pt;padding-top:1pt;' >";
  	
	$str .= "<table width='100%' border='0' cellpadding='0' cellspacing='0' bgcolor='#ffffff' style='height:100%;'>";
	$str .= "<tr>";
	$str .= "<td width='23%' style='padding-left:1pt;'>";
	$str .= "<div>";
	$str .= "<table cellpadding='0' cellspacing='0' border='0'  style='width:100%;height:40px;' bgcolor='#006699'>";
	$str .= "<tr>";
	$str .= "<td style='background-image:url(http://www.bizzona.ru/images/b1.gif); background-repeat:no-repeat;width:5px;height:5px; background-position:top;'>&nbsp;</td>";
	$str .= "<td rowspan='2' valign='middle' align='center'  style='font-size:11pt; font-family:Arial; font-weight:bold; color:White;' nowrap>".$data['cost']."</td>";
	$str .= "<td style='background-image:url(http://www.bizzona.ru/images/b2.gif); background-repeat:no-repeat;width:5px;height:5px; background-position:top;'>&nbsp;</td>";
		 
	$str .= "</tr>";
	$str .= "<tr>";
	$str .= "<td style='background-image:url(http://www.bizzona.ru/images/b4.gif); background-repeat:no-repeat;width:5px;height:5px; background-position:bottom;'>&nbsp;</td>";
	$str .= "<td style='background-image:url(http://www.bizzona.ru/images/b3.gif); background-repeat:no-repeat;width:5px;height:5px; background-position:bottom;'>&nbsp;</td>";
	$str .= "</tr>";
	$str .= "</table>";
	$str .= "</div>";
	$str .= "</td>";
	$str .= "<td width='77%' bgcolor='#ffffff'><div style='font-size:10pt; font-family:Arial; font-weight:bold; padding-left:10pt;'>".$data["name"]."</div></td>";
	$str .= "</tr>";
	$str .= "</table>";
         
	$str .= "<table width='100%' bgcolor='#ffffff' style='height:100%;border-collapse: collapse;' cellpadding='0' cellspacing='0' border='0'>";
	$str .= "<tr>";
	$str .= "<td colspan=2 class='title_id' bgcolor='#eeeee0'><span style='color:#7c3535;font-family:arial;font-size:10pt;font-weight:bold;'>���������� ������:</td>";
	$str .= "</tr>"; 
	$str .= "<tr>";
	$str .= "<td align='left' width='50%' class='desc_td' style='border-bottom: 1pt solid white'><span class='desc_f'>��� � �������</span></td>";
	$str .= "<td class='val_td' bgcolor='#eeeee0' style='border-bottom: 1pt solid white'><span class='val_f' style='font-weight:bold;'>".$data['FML']."</span></td>";
	$str .= "</tr>";
	$str .= "<tr>";
	$str .= "<td align='left' style='border-bottom: 1pt solid white' class='desc_td'><span class='desc_f'>���������� �-����</span></td>";
	$str .= "<td class='val_td' style='border-bottom: 1pt solid white'><span class='val_f'><a href='mailto:".$data['EmailID']."' class='city_a' style='font-size:10pt;'>".$data['EmailID']."</a></span>";
	$str .= "</td>";
	$str .= "</tr>";
	$str .= "<tr>";
	$str .= "<td align='left' style='border-bottom: 1pt solid white' class='desc_td'><span class='desc_f'>����� ��������</span></td>";
	$str .= "<td class='val_td' style='border-bottom: 1pt solid white'><div class='val_f'>".$data['PhoneID']."</div></td>";
	$str .= "</tr>"; 
	$str .= "<tr>";
	$str .= "<td align='left' style='border-bottom: 1pt solid white' class='desc_td'><span class='desc_f'>����</span></td>";
	$str .= "<td class='val_td' style='border-bottom: 1pt solid white' ><span class='val_f'>".$data['FaxID']."</span></td>";
	$str .= "</tr>"; 

	$str .= "<tr>";
	$str .= "<td colspan=2 class='title_id' bgcolor='#eeeee0' align='right' style='padding-right:2pt;'><div ><a href='http://www.bizzona.ru/detailsSell/".$data['id']."' class='a_c_desc' target='_blank' >���������</a></div></td>";
	$str .= "<tr>";
	$str .= "</table>";
	
	$str .= "</div>";
  	
    return $str;
  }

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
  <HEAD id="Head">
	<title>������� � ������� �������. ������� ������ - ������ ����</title>
	<META NAME="Description" CONTENT="������� �������, ������� �������, ������� ������, ������� �������, ������� �������� �������, ������� �������, ������� ��������,  ������� ��������, ������� ��������, ���� ������� ������?">
        <meta name="Keywords" content="������� �������, ������� �������, ������� ������, ������� �������, ������� �������� �������, ������� �������, ������� ��������,  ������� ��������, ������� ��������, Ready business, �����������, �������� � ��������, ���������, ����������, �����������"> 
        <LINK href="http://www.bizzona.ru/general.css" type="text/css" rel="stylesheet">
        <meta http-equiv="content-type" content="text/html; charset=windows-1251"/>
        <script src="http://tools.spylog.ru/counter.js" type="text/javascript"> </script>
  </HEAD>

<?php
  $smarty->display("./site/headerbanner.tpl");
?>

  
	<script charset="utf-8" src="http://maps.google.com/maps?file=api&amp;v=2&amp;hl=ru&key=ABQIAAAAnavXIsM5jdni-p-JoKqdIxTPIkIa5BjXbMz0WvIzZffrVGiazxTRu-mHw0IpP9Ib5fP2129LbcNtTw"
			type="text/javascript"></script>
	<script type="text/javascript">
		
    			//<![CDATA[
    				function load() {
      					if (GBrowserIsCompatible()) {
        					var map = new GMap2(document.getElementById("map"));
        					//map.enableScrollWheelZoom(); 
        					map.setCenter(new GLatLng(<?=$longitude;?>, <?=$latitude;?>), <?=$zoom;?>);

							// Create our "tiny" marker icon
							var blueIcon = new GIcon(G_DEFAULT_ICON);
							
        					map.addControl(new GMapTypeControl());
        					var bounds = map.getBounds();
          					var point = new GLatLng(<?=$longitude?>, <?=$latitude;?>);
          					map.addOverlay(new GMarker(point));
							blueIcon.iconSize = new GSize(20, 20);
							blueIcon.shadowSize = new GSize(0, 0);
          			  
							function CreateMarker(point, fileicon, text, id, html, htmldesc) {
								var letteredIcon = new GIcon(blueIcon);
								letteredIcon.image = "http://www.bizzona.ru/gicons/"+fileicon+"";
          						markerOptions = { icon:letteredIcon };								
								var marker = new GMarker(point, markerOptions);

          						GEvent.addListener(marker, "click", function() {
            						marker.openInfoWindowTabsHtml([new GInfoWindowTab("��������",html), new GInfoWindowTab("��������",htmldesc)]);
          						});
          						return marker;
							}
									
          					<?
          						$iter = 1;
          						while (list($k, $v) = each($arrSell)) {
          							
          							if($v->longitude == $longitude && $v->latitude == $latitude) {
          								
          								$index = rand(0,sizeof($cood_circle)-1);
										$a = $cood_circle[$index];
          								
          								$v->longitude = str_replace(",",".", $a["x"]);
          								$v->latitude = str_replace(",",".", $a["y"]);
          								$iter+=1;
          							}
          			                $data["id"] = $v->sellId; 
          			                $data["name"] = $v->sellName;
          			                $data["PhoneID"] = $v->PhoneID;
          			                $data["FML"] = $v->FML;
          			                $data["EmailID"] = $v->EmailID;
          			                $data["FaxID"] = $v->FaxID;
          			                $data["SiteID"] = "";$v->SiteID;
          			                $data["ShortDetailsID"] = $v->ShortDetailsID;
          			                $data["ShortDetailsID"] = str_replace("\r\n","", $data["ShortDetailsID"]);
          			                $data["ShortDetailsID"] = str_replace("\"","'", $data["ShortDetailsID"]);
          			                
          			                $cost = "����������";
          			                if(intval($v->cost) > 0) {
          			                	if($v->ccost == "usd") {
          			                		$cost = "$".$v->cost;
          			                	} else if ($v->ccost == "rub") {
          			                		$cost =  $v->cost." �";
          			                	} else if ($v->ccost == "eur") {
          			                		$cost = "&#8364;".$v->cost;
          			                	} else {
          			                		$cost = "$".$v->cost;
          			                	}
          			                }
          			                $data["cost"] = $cost;
          							$html = MapContent($data);			
          							$htmldesc = MapShortDescription($data);
          							?>
										map.addOverlay(CreateMarker(new GLatLng(<?=$v->longitude;?>, <?=$v->latitude;?>), "<?=$v->googleicon;?>","<?=$v->sellName;?>","<?=$v->sellId;?>","<?=$html;?>", "<?=$htmldesc;?>"));
          							<?
          						}
          					?>
        					map.addControl(new GSmallMapControl());
        					GEvent.addListener(map, "click", function(overlay,latlng) {
          						var lat = latlng.lat();
          						var lon = latlng.lng();
          						var latOffset = 0.01;
          						var lonOffset = 0.01;
       	  						var polygon = new GPolygon([
            					new GLatLng(lat, lon - lonOffset),
            					new GLatLng(lat + latOffset, lon),
            					new GLatLng(lat, lon + lonOffset),
            					new GLatLng(lat - latOffset, lon),
            					new GLatLng(lat, lon - lonOffset)
		  						], "#f33f00", 5, 1, "#ff0000", 0.2);
		  						map.addOverlay(polygon);
        					});
          					
          					
      					}
    				}
    			//]]>
		
		
	</script>  
	<body onload="load()" onunload="GUnload()">
<?
	$topmenu = GetMenu();
	$smarty->assign("topmenu", $topmenu);	
		
	$link_country = GetSubMenu();
	$smarty->assign("ad", $link_country);
	
	$smarty->caching = false;
	$output_header = $smarty->fetch("./site/header.tpl");
	echo minimizer($output_header);
?>
<table class="w" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td width="30%" valign="top">
        <?
              $data = array();
              $data["offset"] = 0;
              $data["rowCount"] = 0;
              $data["sort"] = "Count";
              $result = $category->Select($data);
              $smarty->assign("data", $result);
			  $output_categorys = $smarty->fetch("./site/mapscategorys.tpl");
			  echo minimizer($output_categorys);
			  
                
			  $output_call = $smarty->fetch("./site/call.tpl");
			  echo fminimizer($output_call);
 			  
              $datahotbuyer = array();
              $datahotbuyer["offset"] = 0;
              $datahotbuyer["rowCount"] = 4;
              $datahotbuyer["sort"] = "datecreate";
              $datahotbuyer["StatusID"] = "-1";
            	
              if (isset($_GET["action"])) {
                   switch(trim($_GET["action"])) {
	                   case "category": {
    	                 $datahotbuyer["typeBizID"] = intval($_GET["ID"]);
                       } break;
                       case "city": {
                         $datahotbuyer["regionID"] = intval($_GET["ID"]);
                       } break;                       
        	           default: {
                         		
                       }
                       break;
                   }            	
              }		
			  
  	          $res_datahotbuyer = $buyer->Select($datahotbuyer);
  	          if(sizeof($res_datahotbuyer) > 0) {
    				$smarty->assign("databuyer", $res_datahotbuyer);
					$output_iblockbuyer = $smarty->fetch("./site/iblockbuyer.tpl");
					echo fminimizer($output_iblockbuyer);
            		
  	          }
  	          
			  $output_proposal = $smarty->fetch("./site/proposal.tpl");
			  echo fminimizer($output_proposal);
        	
			  $output_request = $smarty->fetch("./site/request.tpl");
			  echo fminimizer($output_request);
  	          
        ?>
        </td>
        <td width="70%" valign="top" style="padding-top:1pt;">
        
			<?
				$output_navsearch = $smarty->fetch("./site/navsearchmap.tpl");
				echo minimizer($output_navsearch);
			?>
				<div class="lftpadding_cnt" style="width: auto;">
					<table width="100%" bgcolor="#ffffff" style="height:100%;border-collapse: collapse;" cellpadding="0" cellspacing="0" border="0">
						<tr>
							<td class="title_id"></td>
						</tr>
						<tr>
							<td align="center" style="border: 1pt solid #707070">
    							<div id="map" style="width: 100%; height: 290pt"></div>	
							</td>
						</tr>
						<tr>
							<td class="title_id"></td>
						</tr>
    				</table>
				</div>
            <?
				$output_navsearch = $smarty->fetch("./site/navsearchmap.tpl");
				echo minimizer($output_navsearch);
            ?>
      
        
            <?
				$data = array();
				$data["offset"] = 0;
				$data["rowCount"] = 0;
				$data["sort"] = "Count";
				$result = $city->SelectActive($data);
				while (list($k, $v) = each($result)) {
					if($v->ID == 2533) {
						unset($result[$k]);
						break;
					}
				}
				reset($result);
				$smarty->assign("data", $result);
				$output_city = $smarty->fetch("./site/mapscity.tpl");
				echo minimizer($output_city);
            ?>
            
            <?
            	if(isset($_GET["regionSell"]) && intval($_GET["regionSell"]) > 0) {
            		$data = array();
            		$data["offset"] = 0;
            		$data["rowCount"] = 0;
					$city->CityID = intval($_GET["regionSell"]);
            		$result = $city->SelectChildActive($data);
            		$smarty->assign("items", sizeof($result)/3);
                	$smarty->assign("datasubcity", $result);
                	
    				$dataCity["ID"] = intval($_GET["regionSell"]);
        			$objCity = $city->GetItem($dataCity);                	
					$smarty->assign("citytitleparent", $objCity->titleParentSelect);
        			
					$output_city = $smarty->fetch("./site/subcitymap.tpl");
					echo minimizer($output_city);
        			
            	}
            ?>	

            <?
            	if(isset($_GET["categorySell"])) {
					$sell->CategoryID = (intval($_GET["categorySell"]));
					$res_sub = $sell->SelectSubCategory();
					if(sizeof($res_sub) > 1)
					{
						$smarty->assign("subbizcategory", $res_sub);
						$output_subbiz = $smarty->fetch("./site/subbizmap.tpl");
						echo minimizer($output_subbiz);
					}
            	}
            ?>
            
            <?
					ShowDetailsSearchForm();
				
                    $data = array();
                    $dataPage = array(); 
  
                    $data["offset"] = 0;
                    $data["rowCount"] = 5;
                    $data["sort"] = "ID";
                 
                    if(isset($_GET["regionSell"])) {    
                    	$data["cityId"] = intval($_GET["regionSell"]);
                    }
                    if(isset($_GET["subRegionSell"])) {
                    	$data["subCityId"] = intval($_GET["subRegionSell"]);
                    }
                    if(isset($_GET["categorySell"])) {
                    	$data["catId"] = intval($_GET["categorySell"]);
                    }
                    if(isset($_GET["subCategorySell"])) {
                    	$data["subCatId"] = intval($_GET["subCategorySell"]);
                    }
                    

                    $data["StatusID"] = "-1";
                    $data["sort"] = "pay_datetime";

                    $result = $sell->DetailsSelect($data);				

                    $smarty->assign("data", $result);

				  	$output_ilistsell = $smarty->fetch("./site/ilistsell.tpl", $_SERVER["REQUEST_URI"]);
				  	echo fminimizer($output_ilistsell);
			?>	
            
        </td>
    </tr>
</table>
<?
	//$smarty->display("./site/sape.tpl");
?>
<style>
 td a 
 {
   color:black;
 }
</style>
<div style="padding-top:4pt;padding-bottom:5pt;padding-right:10pt;padding-left:10pt;font-size:10pt;font-family:arial;">
	<table cellpadding="0" cellspacing="0" border="0"  style="width:100%;padding-bottom:2pt;" bgcolor="#eeeee0">
    	<tr>
        	<td style="background-image:url(http://<?=$_SERVER["HTTP_HOST"]?>/images/d1.gif); background-repeat:no-repeat;width:5px;height:5px; background-position:left top;">&nbsp;</td>
            <td rowspan="2" valign="middle" align="center"  style="font-size:8pt; font-family:Arial; color:black;">
			<?
			?>
            </td>
            <td style="background-image:url(http://<?=$_SERVER["HTTP_HOST"]?>/images/d2.gif); background-repeat:no-repeat;width:5px;height:5px;background-position:right top;">&nbsp;</td>
        </tr>
        <tr>
            <td style="background-image:url(http://<?=$_SERVER["HTTP_HOST"]?>/images/d4.gif); background-repeat:no-repeat;width:5px;height:5px; background-position:left bottom;">&nbsp;</td>
            <td style="background-image:url(http://<?=$_SERVER["HTTP_HOST"]?>/images/d3.gif); background-repeat:no-repeat;width:5px;height:5px; background-position:right bottom;">&nbsp;</td>
        </tr>
    </table>
</div>
<?
	$smarty->display("./site/footer.tpl");
?>

<script language='javascript'>
  function SellPreviewAdd() {
  	
  }
  function AddToSearch(obj) {
	var input_search = document.getElementById("searchtext");
	if(input_search != null) {
		if(obj.innerText != null) {
			input_search.value = obj.innerText;
		} else if (obj.innerHTML != null) {
			input_search.value = obj.innerHTML;
		}
	}
  }  
</script>
<?
  $end = get_formatted_microtime(); 
  $total = $end - $start;
  echo "<center><span style='font-size:7pt;'>".round($total, 6)." : ".$count_sql_call." : ".$base_memory_usage." : ".memoryUsage(memory_get_usage(), $base_memory_usage)." </span><center>";
?>

</body>
</html>

<?
	$sell->Close();
	$category->Close();
	$city->Close();
	$country->Close();
	$buyer->Close();
	$metro->Close();
	$district->Close();
	$subcategory->Close();
	$geocode->Close();
?>
