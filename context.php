<?
	global $DATABASE;
	include_once("./phpset.inc");
	include_once("./engine/functions.inc");

	AssignDataBaseSetting();

    function price($type, $cost) {
       
    	if(intval($cost) <= 0) return  "����������";
    	
    	$cost = trim(strrev(chunk_split (strrev($cost), 3,' '))); 
    	
		if($type == "usd") {
	  		return  "$". $cost; 	
        } else if($type == "rub") {
          return $cost."�";
        } else if($type == "eur") {
          return "&#8364;".$cost;
		} 
       
    } 
	
	include_once("./engine/class.sell.inc"); 
	$sell = new Sell();

	include_once("./engine/class.city.inc");
	$city = new City();
	
	$width = (isset($_GET["width"]) ? intval($_GET["width"]) : 300);
	$sector = (isset($_GET["sector"]) ? intval($_GET["sector"]) : 0);
	$region = (isset($_GET["region"]) ? intval($_GET["region"]) : 0);
	$limit = (isset($_GET["limit"]) ? intval($_GET["limit"]) : 5);
	$color_0 = (isset($_GET["color_0"]) ? trim($_GET["color_0"]) : "#000000");
	$color_1 = (isset($_GET["color_1"]) ? trim($_GET["color_1"]) : "#000000");
	$color_2 = (isset($_GET["color_2"]) ? trim($_GET["color_2"]) : "#000000");
	$color_3 = (isset($_GET["color_3"]) ? trim($_GET["color_3"]) : "#000000");
	$color_4 = (isset($_GET["color_4"]) ? trim($_GET["color_4"]) : "#EEEEE0");
	$color_5 = (isset($_GET["color_5"]) ? trim($_GET["color_5"]) : "#DB8C03");
	$color_6 = (isset($_GET["color_6"]) ? trim($_GET["color_6"]) : "#FFFFFF");
	$color_7 = (isset($_GET["color_7"]) ? trim($_GET["color_7"]) : "#000000");

	$fonts_size_0 = (isset($_GET["fonts_size_0"]) ? trim($_GET["fonts_size_0"]) : "11pt");
	$fonts_size_1 = (isset($_GET["fonts_size_1"]) ? trim($_GET["fonts_size_1"]) : "11pt");
	$fonts_size_2 = (isset($_GET["fonts_size_2"]) ? trim($_GET["fonts_size_2"]) : "11pt");
	$fonts_size_3 = (isset($_GET["fonts_size_3"]) ? trim($_GET["fonts_size_3"]) : "10pt");
	$fonts_size_4 = (isset($_GET["fonts_size_4"]) ? trim($_GET["fonts_size_4"]) : "10pt");

	$block_type = (isset($_GET["block_type"]) ? trim($_GET["block_type"]) : "Vertical");
	
	$block_type = htmlspecialchars($block_type, ENT_QUOTES);
	$width = htmlspecialchars($width, ENT_QUOTES);
	$sector = htmlspecialchars($sector, ENT_QUOTES);
	$region = htmlspecialchars($region, ENT_QUOTES);
	$limit = htmlspecialchars($limit, ENT_QUOTES);
	$color_0 = htmlspecialchars($color_0, ENT_QUOTES);
	$color_1 = htmlspecialchars($color_1, ENT_QUOTES);
	$color_2 = htmlspecialchars($color_2, ENT_QUOTES);	
	$color_3 = htmlspecialchars($color_3, ENT_QUOTES);
	$color_4 = htmlspecialchars($color_4, ENT_QUOTES);
	$color_5 = htmlspecialchars($color_5, ENT_QUOTES);	
	$color_6 = htmlspecialchars($color_6, ENT_QUOTES);	
	$color_7 = htmlspecialchars($color_7, ENT_QUOTES);
	$fonts_size_0 = htmlspecialchars($fonts_size_0, ENT_QUOTES);
	$fonts_size_1 = htmlspecialchars($fonts_size_1, ENT_QUOTES);
	$fonts_size_2 = htmlspecialchars($fonts_size_2, ENT_QUOTES);	
	$fonts_size_3 = htmlspecialchars($fonts_size_3, ENT_QUOTES);
	$fonts_size_4 = htmlspecialchars($fonts_size_4, ENT_QUOTES);
	
	$sell->CityID = $region;
	$sell->SubCategoryID = $sector;
	$sell->limit = $limit;
	$arr_sell = $sell->Context();

	$arr_city = array();
	
	if(strcasecmp(strtolower($block_type), strtolower("Vertical")) == 0) {
	
		echo "document.write('<table width=\"".$width."\" bgcolor=\"".$color_4."\">')\n";

		echo "document.write('<tr>')\n";
		echo "document.write('<td colspan=\"2\" style=\"background-color:".$color_5.";color:".$color_6.";font-weight:bold;padding-left:2pt;padding-right:2pt;\" align=\"center\">������� ������� - BizZONA.ru</td>')\n";
		echo "document.write('</tr>')\n";	
	
		while (list($k, $v) = each($arr_sell)) {
			
			if(strlen(trim($v->ShortDetailsID)) > 150) {
				$v->ShortDetailsID =  substr($v->ShortDetailsID, 0, 150). " ... ";
			}
		
			$cId = 0;
			if(intval($v->subCityID) > 0) {
				$cId = intval($v->subCityID);
			} else {
				$cId = intval($v->CityID);
			}
		
			if(!isset($arr_city[$cId])) {
				$d["ID"] = $cId;
				$rcity = $city->GetItem($d);
				$arr_city[$cId] = $rcity->title;
			}
		
			echo "document.write('<tr style=\"padding-left:2pt;padding-right:2pt;\">')\n";
			echo "document.write('<td width=\"70%\">&nbsp;</td><td style=\"font-size:".$fonts_size_2.";font-weight:bold;color:".$color_3."\" align=\"right\" valign=\"top\" nowrap width=\"20%\">".price($v->cCostID, $v->CostID)."</td>')\n";
			echo "document.write('</tr>')\n";	
			echo "document.write('<tr style=\"padding-left:2pt;padding-right:2pt;\">')\n";
			echo "document.write('<td colspan=\"2\" style=\"font-size:".$fonts_size_1.";font-weight:bold;color:".$color_2."\">".$v->NameBizID."</td>')\n";
			echo "document.write('</tr>')\n";	
			echo "document.write('<tr>')\n";
			echo "document.write('<td  style=\"padding-left:2pt;padding-right:2pt;font-size:".$fonts_size_3.";color:".$color_1."\" colspan=\"2\">".$v->ShortDetailsID."</td>')\n";
			echo "document.write('</tr>')\n";
			echo "document.write('<tr style=\"padding-left:2pt;padding-right:2pt;\">')\n";
			echo "document.write('<td align=\"left\" nowrap style=\"font-size:".$fonts_size_4.";font-weight:bold;color:".$color_7."\">".$arr_city[$cId]."</td><td align=\"right\"><a href=\"http://www.bizzona.ru/detailsSell/".$v->ID."\" style=\"color:".$color_0.";font-size:".$fonts_size_0.";font-weight:bold;\">���������</a></td>')\n";
			echo "document.write('</tr>')\n";
			echo "document.write('<tr style=\"padding-left:2pt;padding-right:2pt;background-color:#ffffff;\">')\n";
			echo "document.write('<td colspan=\"2\"></td>')\n";
			echo "document.write('</tr>')\n";
		}
		echo "document.write('</table>')\n";
	} else if (strcasecmp(strtolower($block_type), strtolower("Horizontal")) == 0) {
		echo "document.write('<table width=\"".$width."\" bgcolor=\"".$color_4."\">')\n";

		echo "document.write('<tr>')\n";
		echo "document.write('<td colspan=\"".(sizeof($arr_sell)*2)."\" style=\"background-color:".$color_5.";color:".$color_6.";font-weight:bold;padding-left:2pt;padding-right:2pt;font-size:10pt;\" align=\"left\">������� ������� - BizZONA.ru</td>')\n";
		echo "document.write('</tr>')\n";
		
		echo "document.write('<tr style=\"padding-left:2pt;padding-right:2pt;\">')\n";
		while (list($k, $v) = each($arr_sell)) {
			echo "document.write('<td style=\"font-size:".$fonts_size_2.";font-weight:bold;color:".$color_3.";padding-left:2pt;padding-right:4pt;\" align=\"right\" valign=\"top\" nowrap width=\"20%\">".price($v->cCostID, $v->CostID)."</td>')\n";
			echo "document.write('<td></td>')\n";
		}
		echo "document.write('</tr>')\n";
		
		reset($arr_sell);
		
		echo "document.write('<tr style=\"padding-left:2pt;padding-right:2pt;\">')\n";
		while (list($k, $v) = each($arr_sell)) {
			if(strlen(trim($v->NameBizID)) > 50) {
				$v->NameBizID =  substr($v->NameBizID, 0, 50). " ... ";
			}
			echo "document.write('<td style=\"font-size:".$fonts_size_1.";font-weight:bold;color:".$color_2.";padding-left:2pt;padding-right:4pt;\"  align=\"left\" valign=\"top\">".$v->NameBizID."</td>')\n";
			echo "document.write('<td ></td>')\n";
		}
		echo "document.write('</tr>')\n";
		
		reset($arr_sell);

		echo "document.write('<tr style=\"padding-left:2pt;padding-right:2pt;\">')\n";
		while (list($k, $v) = each($arr_sell)) {
			
			if(strlen(trim($v->ShortDetailsID)) > 50) {
				$v->ShortDetailsID =  substr($v->ShortDetailsID, 0, 50). " ... ";
			}
			
			echo "document.write('<td valign=\"top\" style=\"padding-left:2pt;padding-right:2pt;font-size:".$fonts_size_3.";color:".$color_1.";padding-left:2pt;padding-right:4pt;\" >".$v->ShortDetailsID."</td>')\n";
			echo "document.write('<td ></td>')\n";
		}
		echo "document.write('</tr>')\n";
		
		reset($arr_sell);

		echo "document.write('<tr style=\"padding-left:2pt;padding-right:2pt;\">')\n";
		while (list($k, $v) = each($arr_sell)) {
			

			$cId = 0;
			if(intval($v->subCityID) > 0) {
				$cId = intval($v->subCityID);
			} else {
				$cId = intval($v->CityID);
			}
		
			if(!isset($arr_city[$cId])) {
				$d["ID"] = $cId;
				$rcity = $city->GetItem($d);
				$arr_city[$cId] = $rcity->title;
			}
			
			echo "document.write('<td align=\"left\" nowrap style=\"font-size:".$fonts_size_4.";font-weight:bold;color:".$color_7.";padding-left:2pt;padding-right:4pt;\">".$arr_city[$cId]."</td>')\n";
			echo "document.write('<td ></td>')\n";
		}
		echo "document.write('</tr>')\n";

		reset($arr_sell);		
		
		echo "document.write('<tr style=\"padding-left:2pt;padding-right:2pt;\">')\n";
		while (list($k, $v) = each($arr_sell)) {
			echo "document.write('<td align=\"right\"><a href=\"http://www.bizzona.ru/detailsSell/".$v->ID."\" style=\"color:".$color_0.";font-size:".$fonts_size_0.";font-weight:bold;;padding-left:2pt;padding-right:4pt;\">���������</a></td>')\n";
			echo "document.write('<td ></td>')\n";
		}
		echo "document.write('</tr>')\n";

		
		echo "document.write('</table>')\n";
	}
//Horizontal	
?>
