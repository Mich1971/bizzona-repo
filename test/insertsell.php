<?
  global $DATABASE;

  include_once("../phpset.inc");
  include_once("../engine/functions.inc"); 
  AssignDataBaseSetting();
  include_once("../engine/class.category.inc"); 
  $category = new Category();

  include_once("../engine/class.country.inc"); 
  $country = new Country();


  include_once("../engine/class.city.inc"); 
  $city = new City();


  include_once("../engine/class.sell.inc"); 
  $sell = new Sell();



  require_once("../libs/Smarty.class.php");
  $smarty = new Smarty;
  $smarty->template_dir = "../templates";
  $smarty->compile_dir  = "../templates_c";
  //$smarty->cache_dir = "./cache";
  $smarty->compile_check = true;
  $smarty->debugging     = false;

  $smarty->register_function('NameCity', 'getNameCity');

  $assignDescription = AssignDescription();

  $error = "";

  if (isset($_POST["insert"])) {

    $data["FML"]              = htmlspecialchars(trim($_POST["FML"]), ENT_QUOTES);
    $data["EmailID"]          = htmlspecialchars(trim($_POST["EmailID"]), ENT_QUOTES);
    $data["PhoneID"]          = htmlspecialchars(trim($_POST["PhoneID"]), ENT_QUOTES);
    $data["NameBizID"]        = htmlspecialchars(trim($_POST["NameBizID"]), ENT_QUOTES);
    $data["CategoryID"]       = htmlspecialchars(trim($_POST["CategoryID"]), ENT_QUOTES);
    $data["BizFormID"]        = htmlspecialchars(trim($_POST["BizFormID"]), ENT_QUOTES);
    $data["CityID"]           = "-1";//htmlspecialchars(trim($_POST["CityID"]), ENT_QUOTES);
    $data["SiteID"]           = htmlspecialchars(trim($_POST["SiteID"]), ENT_QUOTES);
    $data["CostID"]           = htmlspecialchars(trim($_POST["CostID"]), ENT_QUOTES);
    $data["ProfitPerMonthID"] = htmlspecialchars(trim($_POST["ProfitPerMonthID"]), ENT_QUOTES);
    $data["TimeInvestID"]     = htmlspecialchars(trim($_POST["TimeInvestID"]), ENT_QUOTES);
    $data["ListObjectID"]     = htmlspecialchars(trim($_POST["ListObjectID"]), ENT_QUOTES);
    $data["ContactID"]        = htmlspecialchars(trim($_POST["ContactID"]), ENT_QUOTES);
    $data["MatPropertyID"]    = htmlspecialchars(trim($_POST["MatPropertyID"]), ENT_QUOTES);
    $data["MonthAveTurnID"]   = htmlspecialchars(trim($_POST["MonthAveTurnID"]), ENT_QUOTES);
    $data["MonthExpemseID"]   = htmlspecialchars(trim($_POST["MonthExpemseID"]), ENT_QUOTES);
    $data["SumDebtsID"]       = htmlspecialchars(trim($_POST["SumDebtsID"]), ENT_QUOTES);
    $data["WageFundID"]       = htmlspecialchars(trim($_POST["WageFundID"]), ENT_QUOTES);
    $data["ObligationID"]     = htmlspecialchars(trim($_POST["ObligationID"]), ENT_QUOTES);
    $data["CountEmplID"]      = htmlspecialchars(trim($_POST["CountEmplID"]), ENT_QUOTES);
    $data["CountManEmplID"]   = htmlspecialchars(trim($_POST["CountManEmplID"]), ENT_QUOTES);
    $data["TermBizID"]        = htmlspecialchars(trim($_POST["TermBizID"]), ENT_QUOTES);
    $data["ShortDetailsID"]   = htmlspecialchars(trim($_POST["ShortDetailsID"]), ENT_QUOTES);
    $data["DetailsID"]        = htmlspecialchars(trim($_POST["DetailsID"]), ENT_QUOTES);
    $data["ImgID"]            = "  ";//trim($_POST["ImgID"]);
    $data["FaxID"]            = htmlspecialchars(trim($_POST["FaxID"]), ENT_QUOTES);
    $data["ShareSaleID"]      = htmlspecialchars(trim($_POST["ShareSaleID"]), ENT_QUOTES);
    $data["ReasonSellBizID"]  = htmlspecialchars(trim($_POST["ReasonSellBizID"]), ENT_QUOTES);
    $data["TarifID"]          = htmlspecialchars(trim($_POST["TarifID"]), ENT_QUOTES);
    $data["StatusID"]         = 0;

    
  
    if (strlen($data["FML"]) <= 0) {
       $error = "������ �����: "."\"��� � ������� ����, �������������� �� ��������\"";
    } else if (strlen($data["EmailID"]) <= 0) {
       $error = "������ �����: "."\"���������� �-����\"";
    } else if (strlen($data["PhoneID"]) <= 0) {
       $error = "������ �����: "."\"����� ��������\"";
    } else if (strlen($data["NameBizID"]) <= 0) { 
       $error = "������ �����: "."\"�������� �������\"";
    } else if (strlen($data["SiteID"]) <= 0) { 
       $error = "������ �����: "."\"�����, �����, ���, ��������/����\"";
    } else if (strlen($data["CostID"]) <= 0) {
       $error = "������ �����: "."\"��������� ������������ �������\"";
    } else if (strlen($data["ShortDetailsID"]) <= 0) {
       $error = "������ �����: "."\"������� ����������, ������ ������������� � ����������� �������\"";     
    } else if (strlen($data["DetailsID"]) <= 0) {
       $error = "������ �����: "."\"������ ������ ����������, ������ ����� ������� ������������� � ����������� �������\"";     
    } else {
    }

    if (strlen($error) <= 0) {

      $to      = 'staff@bizzona.ru';
      $subject = 'new subscribe ';

      $message = $assignDescription["FML"]." ".$data["FML"]."\n";
      $message.= $assignDescription["EmailID"]." ".$data["EmailID"]."\n";
      $message.= $assignDescription["PhoneID"]." ".$data["PhoneID"]."\n";
      $message.= $assignDescription["NameBizID"]." ".$data["NameBizID"]."\n";
      $message.= $assignDescription["CategoryID"]." ".$data["CategoryID"]."\n";
      $message.= $assignDescription["BizFormID"]." ".$data["BizFormID"]."\n";
      $message.= $assignDescription["CityID"]." ".$data["CityID"]."\n";
      $message.= $assignDescription["SiteID"]." ".$data["SiteID"]."\n";
      $message.= $assignDescription["CostID"]." ".$data["CostID"]."\n";
      $message.= $assignDescription["ProfitPerMonthID"]." ".$data["ProfitPerMonthID"]."\n";
      $message.= $assignDescription["TimeInvestID"]." ".$data["TimeInvestID"]."\n";
      $message.= $assignDescription["ListObjectID"]." ".$data["ListObjectID"]."\n";
      $message.= $assignDescription["ContactID"]." ".$data["ContactID"]."\n";
      $message.= $assignDescription["MatPropertyID"]." ".$data["MatPropertyID"]."\n";
      $message.= $assignDescription["MonthAveTurnID"]." ".$data["MonthAveTurnID"]."\n";
      $message.= $assignDescription["SumDebtsID"]." ".$data["SumDebtsID"]."\n";
      $message.= $assignDescription["WageFundID"]." ".$data["WageFundID"]."\n";
      $message.= $assignDescription["ObligationID"]." ".$data["ObligationID"]."\n";
      $message.= $assignDescription["CountEmplID"]." ".$data["CountEmplID"]."\n";
      $message.= $assignDescription["CountManEmplID"]." ".$data["CountManEmplID"]."\n";
      $message.= $assignDescription["TermBizID"]." ".$data["TermBizID"]."\n";
      $message.= $assignDescription["ShortDetailsID"]." ".$data["ShortDetailsID"]."\n";
      $message.= $assignDescription["DetailsID"]." ".$data["DetailsID"]."\n";
//      $message.= $assignDescription["ImgID"]." ".$data["ImgID"]."\n";
      $message.= $assignDescription["FaxID"]." ".$data["FaxID"]."\n";
      $message.= $assignDescription["ShareSaleID"]." ".$data["ShareSaleID"]."\n";
      $message.= $assignDescription["TarifID"]." ".$data["TarifID"]."\n";
      $message.= $assignDescription["ReasonSellBizID"]." ".$data["ReasonSellBizID"]."\n";
//      $message.= $assignDescription["StatusID"]." ".$data["StatusID"]."\n"; 

      $headers = 'BizZONA.RU: new subscribe' . "\r\n" .
      'Reply-To: staff@bizzona.ru' . "\r\n" .
      'X-Mailer: PHP/' . phpversion();
      mail($to, $subject, $message, $headers);


      $data["CostID"] = floatval($data["CostID"]);

      $sell->Insert($data);
      $statusAdd = true;
      unset($_POST);
    }
  } 





  $data["offset"]   = 0;
  $data["rowCount"] = 0;
  $listCategory = $category->Select($data); 


  $data["offset"]   = 0;
  $data["rowCount"] = 0;
  $listCity = $city->Select($data); 



  $smarty->assign("listCategory", $listCategory);
  $smarty->assign("listCity", $listCity);


?>



<body style="background-color: #d9d9b9;" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" width='100%' height='100%'>
  <HEAD id="Head">
      <title>������ ����</title>		
	<META NAME="Description" CONTENT="������� �������, ������� �������, ������� ������, ������� �������, ������� �������� �������, ������� �������, ������� ��������,  ������� ��������, ������� ��������, ���� ������� ������?">
        <meta name="Keywords" content="������� �������, ������� �������, ������� ������, ������� �������, ������� �������� �������, ������� �������, ������� ��������,  ������� ��������, ������� ��������, ���� ������� ������?"> 
        <LINK href="../general.css" type="text/css" rel="stylesheet">
        <script src="http://tools.spylog.ru/counter.js" type="text/javascript"> </script>
  </HEAD>

  <table border=0 cellpadding="0" cellspacing="0" width='100%' style="background-image: url(../b.gif); background-repeat: repeat-x;">

    <tr style="background-image: url(../b.gif); background-repeat: repeat-x;" height='92'>  
      <td align='left' width='20%' valign='top' height='92' style="paddign:0px;"><a href='./index.php'><img src="../bison1.gif" style="margin:0px;" border=0></a></td> 
      <td width='80%' valign='top' align='right'><span style='font-family:arial;color:#7C3535;font-size:9pt;'><b>����� - ����� ������ ��� - ����� ������� �������� ������ ����������</span></b>&nbsp;</td>
    </tr>


    <tr>
      <td colspan=2 align='center' height="25">
         <table width='100%' cellpadding="0" cellspacing="0">
           <tr style="background-image: url(./menu.gif); background-repeat: repeat-x;">
             <td width='20%' align='left'>&nbsp;<span style="font-family:verdana;color:#ffffff;font-size:10pt;letter-spacing:2pt;"><b><a href='./index.php' style="font-family:verdana;color:#424F31;font-size:10pt;letter-spacing:1pt;text-decoration:none;">[�� �������]</a></b></span> </td>
             <td width='80%' align='center'>
             </td>
           </tr>
         </table>
      </td> 
    </tr>



    <tr>
      <td valign='top' colspan=2>
       <?
         if (isset($statusAdd) && $statusAdd) {
          ?><center><span  style='font-size:18px;'>�������� ������� ���� ������� ���������. � �������� ���� �������� ����� ���������� � ������������.</span></center><?
         }
       ?>
         <table width='100%' height='100%' bgcolor='#d9d9b9'>
           <?
              if (strlen($error) > 0) { 
           ?> 
           <tr  valign='top' width='100%' height='100%'>
             <td><span style="color:red;"><b><?=$error;?></b></span></td> 
           </tr>
           <?
              }
           ?> 
           <tr>
             <td valign='top' width='60%' height='100%'>
               <?
                  $smarty->display("./site/insertsell.tpl");
               ?>
             </td>
           </tr>
         </table>
      </td>
    </tr> 


    <tr>
      <td colspan=3 width='100%' align='center' style="background-image: url(../images/submenu.gif); background-repeat: repeat-x;">&nbsp;</td>
    </tr> 

    <tr>
      <td colspan=3><br></td>
    </tr>

   </table>

  <table width='100%' >

    <tr>
      <td align='left' width='30%'>&nbsp;<a href='mailto:staff@BizZONA.RU' style='font-family:verdana;color:#000000;font-size:9pt;letter-spacing:3pt;font-weight:bold;'>staff@BizZONA.RU</a></td>
      <td align='center' width='40%'><script type="text/javascript">spylog_tracker(772883); </script></td>
      <td align='right' width='30%'><span style='font-family:verdana;color:#000000;font-size:9pt;letter-spacing:3pt;font-weight:bold;'>���: 8.917.519.100.6 &nbsp;</span></td>
    </tr>
  </table> 

</body>




