-- phpMyAdmin SQL Dump
-- version 4.0.10.6
-- http://www.phpmyadmin.net
--
-- ����: 127.0.0.1:3306
-- ����� ��������: ��� 27 2015 �., 11:39
-- ������ �������: 5.5.41-log
-- ������ PHP: 5.3.29

-- example:  mysql --default-character-set=cp1251  < migration_18.sql
 
USE `bizzona`; 
  

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES cp1251 */;

--
-- ���� ������: `bizzona`
--

-- --------------------------------------------------------

--
-- ��������� ������� `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'auto incrementing user_id ������������, ���������� ������, ��������� ����',
  `user_name` varchar(64) NOT NULL COMMENT '��� ������������, ����������',
  `user_password_hash` varchar(255) NOT NULL COMMENT '������ ������������ � ����� � ������������ �����',
  `user_email` varchar(64) NOT NULL COMMENT 'email ������������, ����������',
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `user_name` (`user_name`),
  UNIQUE KEY `user_email` (`user_email`)
) ENGINE=InnoDB  DEFAULT CHARSET=cp1251 COMMENT='user data' AUTO_INCREMENT=4 ;

--
-- ���� ������ ������� `users`
--

INSERT INTO `users` (`user_id`, `user_name`, `user_password_hash`, `user_email`) VALUES(1, 'alexanderworker', '$2y$10$HUD9TEBvMLRW6uxV1AYq/O6fZzi42.fRFdCEahzbLiJfNaMrvNvJa', 'alexander.worker@yandex.ua');
INSERT INTO `users` (`user_id`, `user_name`, `user_password_hash`, `user_email`) VALUES(2, 'alexanderadmin', '$2y$10$BFNKkpRVPZtDw/GQabalJ./fccTY2xSFZR9Jz2ktRAgiITBuHYfZi', 'alexanderadmin@google.com');
INSERT INTO `users` (`user_id`, `user_name`, `user_password_hash`, `user_email`) VALUES(3, 'moderator', '$2y$10$fJlpcVOzlXrmaRv1clQShu.b6iU.XYXvZME/SV2oo6TJoKknbXnJO', 'alexandermoderator@google.com');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
