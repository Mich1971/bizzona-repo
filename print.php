<?
  global $DATABASE;

  include_once("./phpset.inc");
  include_once("./engine/functions.inc"); 
  
  //ini_set("display_startup_errors", "on");
  //ini_set("display_errors", "on");
  //ini_set("register_globals", "on");
  
  AssignDataBaseSetting();
  
	include_once("./engine/class.category_lite.inc"); 
	$category = new Category_Lite();

	include_once("./engine/class.country_lite.inc"); 
	$country = new Country_Lite();

	include_once("./engine/class.city_lite.inc"); 
	$city = new City_Lite();

	include_once("./engine/class.sell_lite.inc"); 
	$sell = new Sell_Lite();

  	include_once("./engine/class.district_lite.inc"); 
  	$district = new District_Lite();

  	include_once("./engine/class.subcategory_lite.inc");
  	$subcategory = new SubCategory_Lite();


  require_once("./libs/Smarty.class.php");
  $smarty = new Smarty;
  $smarty->template_dir = "./templates";
  $smarty->compile_dir  = "./templates_c";
  //$smarty->cache_dir = "./cache";
  $smarty->compile_check = true;
  $smarty->debugging     = false;

  require_once("./regfuncsmarty.php");
  
  
  $assignDescription = AssignDescription();

  if (!isset($_GET["ID"])) {
    $_GET["ID"] = 0; 
  }

  $dataSell = Array();
  $dataSell["ID"] = intval($_GET["ID"]);
  $resultGetItem = $sell->GetItem($dataSell);
  if (isset($resultGetItem->ID) && $resultGetItem->StatusID != 17) {
    //$resultGetItem->CostID = trim(strrev(chunk_split (strrev($resultGetItem->CostID), 3,' '))); 
    $resultGetItem->MonthAveTurnID = trim(strrev(chunk_split (strrev($resultGetItem->MonthAveTurnID), 3,' '))); 
    $resultGetItem->MaxMonthAveTurnID = trim(strrev(chunk_split (strrev($resultGetItem->MaxMonthAveTurnID), 3,' '))); 
    $resultGetItem->MonthExpemseID = trim(strrev(chunk_split (strrev($resultGetItem->MonthExpemseID), 3,' ')));  
    $resultGetItem->ProfitPerMonthID = trim(strrev(chunk_split (strrev($resultGetItem->ProfitPerMonthID), 3,' '))); 
    $resultGetItem->MaxProfitPerMonthID = trim(strrev(chunk_split (strrev($resultGetItem->MaxProfitPerMonthID), 3,' '))); 

    $resultGetItem->WageFundID = trim(strrev(chunk_split (strrev($resultGetItem->WageFundID), 3,' ')));      
  } else {
    header ("Location: http://www.bizzona.ru");
  }

?>

<html>
  <HEAD>
  	  <?
  	     if(strlen($resultGetItem->subtitle) > 0) {
			?>
				<title><?=$resultGetItem->subtitle;?> ���, "������-����"</title>
				<META NAME="Description" CONTENT="<?=$resultGetItem->subtitle;?> ���, "������-����">
			<?  	     	
  	     } else {
  	     	?>
  	     		<title>������� <?=$resultGetItem->NameBizID;?>. ������ <?=$resultGetItem->NameBizID;?>, ����� <?=$resultGetItem->NameBizID;?>.</title>		
  	     		<META NAME="Description" CONTENT="������ <?=$resultGetItem->NameBizID;?> , ����� <?=$resultGetItem->NameBizID;?>">
  	     	<?
  	     }
      ?>	
      
      
		
        <meta name="Keywords" content="������ <?=$resultGetItem->NameBizID;?> , ����� <?=$resultGetItem->NameBizID;?>"> 
        <LINK href="http://www.bizzona.ru/general.css" type="text/css" rel="stylesheet">
        <meta http-equiv="content-type" content="text/html; charset=windows-1251"/>
  </HEAD>

<body style="background-color: #d9d9b9;" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" width='100%' height='100%'>

  <table border=0 cellpadding="0" cellspacing="0" width='100%'>

    <tr>
      <td  align='left' bgcolor='#d9d9b9'  valign='top'>

      </td>
      <td valign='top'>
         <table width='100%' border=0 cellpadding="0" cellspacing="0">
           <tr>
             <td valign='top' width='100%' bgcolor='white'>
             </td>
           </tr>
         </table>
         <table width='100%' height='100%' bgcolor='#d9d9b9' cellpadding="0" cellspacing="0">
           <tr>
             <td valign='top' width='100%' height='100%'>
               <?
                 if (isset($resultGetItem->ID)) {
                   $smarty->assign("data", $resultGetItem);
                   $BizFormID =  NameBizFormByID($resultGetItem->BizFormID);
                   $ReasonSellBizID = NameReasonSellBizByID($resultGetItem->ReasonSellBizID);
                   $smarty->assign("BizFormID", $BizFormID); 
                   $smarty->assign("ReasonSellBizID", $ReasonSellBizID);

                   $dataSell["ID"] = $resultGetItem->CityID;
                   $cityname = $city->GetItem($dataSell);                                
                   $smarty->assign("city", $cityname->title);

                   $smarty->display("./site/print.tpl");

                   $dataSell["ID"] = $resultGetItem->ID;
                   //$sell->IncrementQView($dataSell);
                 }
               ?>
             </td>
           </tr>
         </table>
      </td>
    </tr> 

  </table> 



</body>

</html>
