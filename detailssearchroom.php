<?
	global $DATABASE;

	include_once("./phpset.inc");
	include_once("./engine/functions.inc"); 

  	$count_sql_call = 0;
  	$start = get_formatted_microtime();   
  	$base_memory_usage = memory_get_usage();	
	
	AssignDataBaseSetting();
  
  	//ini_set("display_startup_errors", "on");
  	//ini_set("display_errors", "on");
  	//ini_set("register_globals", "on");
	
	include_once("./engine/class.category_lite.inc"); 
	$category = new Category_Lite();

	include_once("./engine/class.country_lite.inc"); 
	$country = new Country_Lite();

	include_once("./engine/class.city_lite.inc"); 
	$city = new City_Lite();
	
	include_once("./engine/class.sell_lite.inc"); 
	$sell = new Sell_Lite();

	include_once("./engine/class.buy_lite.inc"); 
	$buyer = new Buyer_Lite();
  
  	include_once("./engine/class.metro_lite.inc"); 
  	$metro = new Metro_Lite();
  
  	include_once("./engine/class.district_lite.inc"); 
  	$district = new District_Lite();

  	include_once("./engine/class.streets_lite.inc");
  	$streets = new Streets_Lite();
	
    include_once("./engine/class.Room_Lite.inc");
    $room = new Room_Lite();
	
	include_once("./engine/class.RoomCategory.inc");
	$roomCategory = new RoomCategory();	
	
  	include_once("./engine/class.ad.inc");
  	$ad = new Ad();	
	
	require_once("./libs/Smarty.class.php");
	$smarty = new Smarty;
	$smarty->template_dir = "./templates";
	$smarty->compile_dir  = "./templates_c";
	//$smarty->cache_dir = "./cache";
	$smarty->compile_check = true;
	$smarty->debugging     = false;

	require_once("./regfuncsmarty.php");
	
	$room->districtId = (isset($_GET["districtId"]) ? intval($_GET["districtId"]) : 0);
	$room->metroId = (isset($_GET["metroId"]) ? intval($_GET["metroId"]) : 0);
	$room->RoomCategoryId = (isset($_GET["RoomCategoryId"]) ? intval($_GET["RoomCategoryId"]) : 0);
	$room->cityId = (isset($_GET["cityId"]) ? intval($_GET["cityId"]) : 0);
	$room->streetId = (isset($_GET["streetId"]) ? intval($_GET["streetId"]) : 0);
	$room->statusId = 1;

	
	$_GET["districtId"] = $room->districtId;
	$_GET["metroId"] = $room->metroId;
	$_GET["RoomCategoryId"] = $room->RoomCategoryId;
	$_GET["cityId"] = $room->cityId;
	
	$title =  "������� ��������� ��� ������";
	
	if(isset($_GET["RoomCategoryId"]) && intval($_GET["RoomCategoryId"]) > 0) {
 		$roomCategory->Id = $_GET["RoomCategoryId"];
		$res_roomCategoryGet  = $roomCategory->GetItem();
		$title = (strlen(trim($res_roomCategoryGet->SeoTitle)) > 0 ? $res_roomCategoryGet->SeoTitle : "");
	}
	
	$smarty->assign("title", $title);

	$ad->InitAd($smarty, $_GET);   	

	
	if(isset($_GET['aCategoryID'])) {
		$_GET['CategoryID'] = explode(',', $_GET['aCategoryID']);
	}
  
	if(isset($_GET['aCityID'])) {
		$_GET['CityID'] = explode(',', $_GET['aCityID']);
	}  
	
	
	if(isset($_GET['CategoryID']) && is_array($_GET['CategoryID'])) {
  		$aCategoryID = $_GET['CategoryID'];
		array_walk($aCategoryID, create_function('&$v', 'return $v=intval($v);'));  		
	}
  
  	if(isset($_GET['CityID']) && is_array($_GET['CityID'])) {
  		$aCityID = $_GET['CityID'];
		array_walk($aCityID, create_function('&$v', 'return $v=intval($v);'));   	
  	}   
  	
  	if(isset($_GET["startcostf"])) {
  		$_GET["startcostf"] = intval($_GET["startcostf"]);
  	} else {
  		$_GET["startcostf"] = 0;
  	}
  
  	if(isset($_GET["stopcostf"])) {
  		$_GET["stopcostf"] = intval($_GET["stopcostf"]);
  	} else {
  		$_GET["stopcostf"] = 0;
  	}

  	if((intval($_GET["startcostf"]) > intval($_GET["stopcostf"])) && (intval($_GET["startcostf"]) > 0 && intval($_GET["stopcostf"]) > 0) ) {
  		$temp_c  = $_GET["stopcostf"];
  		$_GET["stopcostf"] = $_GET["startcostf"];
  		$_GET["startcostf"] = $temp_c;
  	}  	
  	
  $ad->InitAd($smarty, $_GET); 	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
  <HEAD>
   <title><?=$title;?>, ��� "������ ����"</title>		
  
	<META NAME="Description" CONTENT="<?=$title;?>">
        <meta name="Keywords" content="<?=strtolower($title);?>"> 
        <LINK href="http://www.bizzona.ru/general.css" type="text/css" rel="stylesheet">
        <meta http-equiv="content-type" content="text/html; charset=windows-1251"/>
        <link rel="shortcut icon" href="http://www.bizzona.ru/images/favicon.ico">
    </HEAD>
<body>

<?
  $smarty->display("./site/headerbanner.tpl");
?>
<?
	$link_country = GetSubMenu();	
	$smarty->assign("ad", $link_country);
	
	echo minimizer($smarty->fetch("./site/detailssearchheaderroom.tpl"));
?>
<table class="w" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td width="30%" style="padding-top:1pt;" valign="top">
        
        <?
        	$smarty->caching = false; 
			echo fminimizer($smarty->fetch("./site/inner_bannertop_left.tpl"));
		?>       
        
        
        <?
        	$roomcategorylist = $roomCategory->Select();
        	$smarty->assign("roomcategorylist", $roomcategorylist);
        
			echo  minimizer($smarty->fetch("./site/roomcategorylist.tpl")); 
			
            	echo fminimizer($smarty->fetch("./site/call.tpl"));
            
				$smarty->caching = true; 
				$smarty->cache_lifetime = 3600;
            	
            	if (!$smarty->is_cached("./site/iblockbuyer.tpl", "action=".(isset($_GET["action"]) ? $_GET["action"] : ""))) {
            	
            		$datahotbuyer = array();
            		$datahotbuyer["offset"] = 0;
            		$datahotbuyer["rowCount"] = 4;
            		$datahotbuyer["sort"] = "datecreate";
            		$datahotbuyer["StatusID"] = "-1";

                	if (isset($_GET["action"])) {
                   		switch(trim($_GET["action"])) {
	                   		case "category": {
    	                 		$datahotbuyer["typeBizID"] = intval($_GET["ID"]);
                       		} break;
                       		case "city": {
                         		$datahotbuyer["regionID"] = intval($_GET["ID"]);
                       		} break;                       
        	           		default: {
                       		}
                       		break;
                   		}            	
                	}		
            	
  	          		$res_datahotbuyer = $buyer->Select($datahotbuyer);
  	          		
    				$smarty->assign("databuyer", $res_datahotbuyer);        	
            	} 	
            	echo fminimizer($smarty->fetch("./site/iblockbuyer.tpl", "action=".(isset($_GET["action"]) ? $_GET["action"] : "")));
            	$smarty->caching = false; 
  	          	

			echo fminimizer($smarty->fetch("./site/proposal.tpl"));
			echo fminimizer($smarty->fetch("./site/request.tpl"));
           	echo minimizer($smarty->fetch("./site/citydomains.tpl"));
        ?>              
        
        </td>
        <td width="70%" style="padding-top:1pt;" valign="top">
            <?
				$output_navsearch = $smarty->fetch("./site/navsearchdetailsroom.tpl");
				echo fminimizer($output_navsearch);
				
                    $data = array();
                    $dataPage = array(); 
                    $pagesplit = GetPageSplit();

                    if (isset($_GET["offset"])) {
                      $room->offset = intval($_GET["offset"]);
                    } else {
                      $room->offset = 0;
                    }
                    if (isset($_GET["rowCount"])) { 
                      $room->rowCount = intval($_GET["rowCount"]);
                    } else {
                      $room->rowCount = $pagesplit;
                    }
                    
                    if(isset($aCityID)) {
                    	$room->aCityId = $aCityID;
                    }                    
                    
                    $data["sort"] = "ID";

                    
                    $data["cityId"] = intval($_GET["cityId"]);
                    $data["subCityId"] = intval($_GET["subCityId"]);
                    $data["catId"] = intval($_GET["catId"]);
                    $data["subCatId"] = intval($_GET["subCatId"]);
                    $data["metroId"] = intval($_GET["metroId"]);
                    $data["districtId"] = intval($_GET["districtId"]);
                    $data["brokerId"] = intval($_GET["brokerId"]);
                    
                    $_GET["cityId"] = $data["cityId"];
                    $_GET["subCityId"] = $data["subCityId"];
                    $_GET["catId"] = $data["catId"];
                    $_GET["subCatId"] = $data["subCatId"];
                    $_GET["metroId"] = $data["metroId"];
                    $_GET["districtId"] = $data["districtId"];
                    $_GET["brokerId"] = $data["brokerId"];
                    

					$data["stopcostf"] = intval($_GET["stopcostf"]);
  					$data["startcostf"] = intval($_GET["startcostf"]);			

  					$room->startcostf = $data["startcostf"];
                    $room->stopcostf = $data["stopcostf"];
  					
                    $resultPage = $room->CountSelect();
                    
                    $smarty->assign("CountRecord",$resultPage);
                    $smarty->assign("CountSplit", $pagesplit);
                    $smarty->assign("CountPage", ceil($resultPage/5));

                    $data["StatusID"] = "-1";
                    $data["sort"] = "pay_datetime";

                    $result = $room->Select();
                    
                    $smarty->assign("data", $result);


				  $output_pagesplit = $smarty->fetch("./site/pagesplitdetailsroom.tpl", $_SERVER["REQUEST_URI"]);
				  echo fminimizer($output_pagesplit);
                  
                  
				  $saveme = $smarty->fetch("./site/saveme.tpl");
				  echo $saveme;
                  
				  echo fminimizer($smarty->fetch("./site/listrooms.tpl", $_SERVER["REQUEST_URI"]));
				   
                   if($resultPage <= 0) {
                   	?>
                   		<div class="lftpadding" style="width: auto;">
                    		<div style="font-family:Arial;font-size:13pt;color:#7C3535;font-weight:bold;padding-top:4pt;margin:0pt;">�� ������� ������� ��������� �� ������� �� �������</div>
                    	</div>		
                    <?	
                    }

                  
               ?>
            	<div class="lftpadding" style="width: auto;">
       				<div  style="background-image:url(<?=$_SERVER["HTTP_HOST"]?>/images/bl.gif); background-repeat:repeat-x;">&nbsp;</div>
            	</div>
            	<?
				  	echo fminimizer($output_pagesplit);
            	?> 
            	<?
				  	echo fminimizer($output_navsearch);

				?>

				<?
					include_once("./yadirect.php");
				?>				
				
        </td>
    </tr>
</table>

<style>
 td a 
 {
   color:black;
 }
</style>
<div style="padding-top:4pt;padding-bottom:5pt;padding-right:10pt;padding-left:10pt;font-size:10pt;font-family:arial;">
	<table cellpadding="0" cellspacing="0" border="0"  style="width:100%;padding-bottom:2pt;" bgcolor="#eeeee0">
    	<tr>
        	<td style="background-image:url(http://<?=$_SERVER["HTTP_HOST"]?>/images/d1.gif); background-repeat:no-repeat;width:5px;height:5px; background-position:left top;">&nbsp;</td>
            <td rowspan="2" valign="middle" align="center"  style="font-size:8pt; font-family:Arial; color:black;">
            
            </td>
            <td style="background-image:url(http://<?=$_SERVER["HTTP_HOST"]?>/images/d2.gif); background-repeat:no-repeat;width:5px;height:5px;background-position:right top;">&nbsp;</td>
        </tr>
        <tr>
            <td style="background-image:url(http://<?=$_SERVER["HTTP_HOST"]?>/images/d4.gif); background-repeat:no-repeat;width:5px;height:5px; background-position:left bottom;">&nbsp;</td>
            <td style="background-image:url(http://<?=$_SERVER["HTTP_HOST"]?>/images/d3.gif); background-repeat:no-repeat;width:5px;height:5px; background-position:right bottom;">&nbsp;</td>
        </tr>
    </table>
</div>

<?
	$smarty->display("./site/footer.tpl");
?>

<?
  $end = get_formatted_microtime(); 
  $total = $end - $start;
  echo "<center><span style='font-size:7pt;'>".round($total, 6)." : ".$count_sql_call." : ".$base_memory_usage." : ".memoryUsage(memory_get_usage(), $base_memory_usage)." </span><center>";
?>

<!-- script type="text/javascript" src="http://www.bizzona.ru/ja.js"></script -->

<script language='javascript'>
  function SellPreviewAdd() {
  	
  }
  function AddToSearch(obj) {
	var input_search = document.getElementById("searchtext");
	if(input_search != null) {
		if(obj.innerText != null) {
			input_search.value = obj.innerText;
		} else if (obj.innerHTML != null) {
			input_search.value = obj.innerHTML;
		}
	}
  }  
</script>


</body>
</html>
<?
	$sell->Close();
	$category->Close();
	$city->Close();
	$country->Close();
	$buyer->Close();
	$metro->Close();
	$district->Close();
	$streets->Close();
?>