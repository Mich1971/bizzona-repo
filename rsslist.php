<?
  global $DATABASE;

  include_once("./phpset.inc");
  include_once("./engine/functions.inc"); 

  ini_set('display_errors', 1);
  error_reporting(E_ALL);
  
  $count_sql_call = 0;
  $start = get_formatted_microtime(); 
  $base_memory_usage = memory_get_usage();	

 
  AssignDataBaseSetting();
  
  include_once("./engine/class.category_lite.inc"); 
  $category = new Category_Lite();

  include_once("./engine/class.country_lite.inc"); 
  $country = new Country_Lite();

  include_once("./engine/class.city_lite.inc");  
  $city = new City_Lite();

  include_once("./engine/class.sell_lite.inc"); 
  $sell = new Sell_Lite();

  include_once("./engine/class.buy_lite.inc"); 
  $buyer = new Buyer_Lite();
  
  include_once("./engine/class.metro_lite.inc"); 
  $metro = new Metro_Lite();
  
  include_once("./engine/class.district_lite.inc"); 
  $district = new District_Lite();
  
  include_once("./engine/class.subcategory_lite.inc");
  $subcategory = new SubCategory_Lite();

  include_once("./engine/class.streets_lite.inc");
  $streets = new Streets_Lite();

  include_once("./engine/class.rss.inc");   
  $rss = new RSS();
 

  require_once("./libs/Smarty.class.php");
  $smarty = new Smarty;
  $smarty->template_dir = "./templates";
  $smarty->compile_dir  = "./templates_c";
  //$smarty->cache_dir = "./cache";
  $smarty->compile_check = true;
  $smarty->debugging     = false;

  require_once("./regfuncsmarty.php");
  
  
  if(isset($_GET["ID"])) {
  	$_GET["ID"] = intval($_GET["ID"]);
  }
  
  if(isset($_GET["rowCount"])) {
  	$_GET["rowCount"] = intval($_GET["rowCount"]);
  }

  if(isset($_GET["offset"])) {
  	$_GET["offset"] = intval($_GET["offset"]);
  }

  if(isset($_GET["streetID"])) {
  	$_GET["streetID"] = intval($_GET["streetID"]);
  }
  
  
  $name_words = array("turagenstvo", "restoran", "parik", "krasota", "otel");
  if(isset($_GET["name"])) {
	if(in_array($_GET["name"], $name_words)) {
	} else {
		$_GET["name"] = "";
	}
  }
  
	$action_words = array("category", "city", "price", "topbiz");
	if(isset($_GET["action"])) {
		if(in_array($_GET["action"], $action_words)) {
		
		} else {
			$_GET["action"] = "";
		}
	} 
  
  
  
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
  <HEAD>
<?
  $seokey = "";
	$seocity = "";
  
  if (isset($_GET["action"])) {
    switch(trim($_GET["action"])) {
      case "category": {
        ?>
          <title>������� � ������� �������. ������� ������ - ������ ����</title>		
        <?
        $seokey = $category->GetKeySell(intval($_GET["ID"]));
        if(strlen($seokey) > 0) {
        	$seokey = $seokey.", ";
        }
      } break;
      case "city": {
      	if(isset($_GET["subCityID"]) && intval($_GET["subCityID"]) > 0) {
      		$dataCity["ID"] = intval($_GET["subCityID"]); 
      	} else {
      		$dataCity["ID"] = intval($_GET["ID"]);	
      	}
        
        $objCity = $city->GetItem($dataCity);
        $seocity = $objCity->seo;
        if(strlen($seocity) > 0) {
        	$seocity = $seocity.", ";
        }
        ?>
          <title>������� ������� <?=$objCity->title;?> | ������� ������� <?=$objCity->title;?> | ������� ������ - ������ ����</title>		
        <?
        $seokey = "������� ������� ".$objCity->title.", ������� ������� ".$objCity->title.", ";
      } break;
      case "price": {
        ?>
          <title>������� � ������� �������. ������� ������ - ������ ����</title>
        <?
      } break;
      default: {
        ?>
          <title>������� � ������� �������. ������� ������ - ������ ����</title>		
        <?
      }   
    }
  } else {
    ?>
      <title>������� � ������� �������. ������� ������ - ������ ����</title>		
    <?
  } 
?>
	<META NAME="Description" CONTENT="������� �������, ������� �������, ������� ������, ������� �������, ������� �������� �������, ������� �������, ������� ��������,  ������� ��������, ������� ��������, ���� ������� ������?">
        <meta name="Keywords" content="<?=$seocity;?><?=$seokey;?>������� �������, ������� �������, ������� ������, ������� �������, ������� �������� �������, ������� �������, ������� ��������,  ������� ��������, ������� ��������, Ready business, �����������, �������� � ��������, ���������, ����������, �����������"> 
        <LINK href="http://www.bizzona.ru/general.css" type="text/css" rel="stylesheet">
        <script type="text/javascript" src="http://www.bizzona.ru/ja.js"></script>
        <meta http-equiv="content-type" content="text/html; charset=windows-1251"/>
  </HEAD>
<body>
<?
	//$smarty->display("./site/header.tpl");
	
	$output_header = $smarty->fetch("./site/header.tpl");
	echo minimizer($output_header);
?>
<table class="w" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td width="30%" valign="top">
        <?
          if (isset($_GET["action"]) && $_GET["action"]  == "category") {
            $_GET["ID"] = (isset($_GET["ID"])  ? intval($_GET["ID"]) : 0);
            $smarty->caching = true; 
            if (!$smarty->is_cached('./site/rsscategorys.tpl', $_GET["ID"])) {
              $data = array();
              $data["offset"] = 0;
              $data["rowCount"] = 0;
              $data["sort"] = "Count";
              $result = $category->Select($data);
              $smarty->assign("data", $result);
            }
            //$smarty->display("./site/categorys.tpl", $_GET["ID"]);
            
  			$output_categorys = $smarty->fetch("./site/rsscategorys.tpl", $_GET["ID"]);
  			echo minimizer($output_categorys);
            $smarty->caching = false; 
          } else { 
            $smarty->caching = true; 
            if (!$smarty->is_cached('./site/rsscategorys.tpl')) {
              $data = array();
              $data["offset"] = 0;
              $data["rowCount"] = 0;
              $data["sort"] = "Count";
              $result = $category->Select($data);
              $smarty->assign("data", $result);
            }
            //$smarty->display("./site/categorys.tpl");
            
			$output_categorys = $smarty->fetch("./site/rsscategorys.tpl");
			echo minimizer($output_categorys);
            
            $smarty->caching = false; 
          }
        ?>
            <?
            
            	$datahotbuyer = array();
            	$datahotbuyer["offset"] = 0;
            	$datahotbuyer["rowCount"] = 4;
            	$datahotbuyer["sort"] = "datecreate";
            	$datahotbuyer["StatusID"] = "-1";

            	
                if (isset($_GET["action"])) {
                   switch(trim($_GET["action"])) {
	                   case "category": {
    	                 $datahotbuyer["typeBizID"] = intval($_GET["ID"]);
                       } break;
                       case "city": {
                         $datahotbuyer["regionID"] = intval($_GET["ID"]);
                       } break;                       
        	           default: {
                         		
                       }
                       break;
                   }            	
                }		
            	
			?>
		<div style="padding-top:2pt;"></div>                    
        </td>
        <td width="70%" valign="top">
            <?

				if(isset($_GET["parentRegionId"])) {
					$rss->parentRegionId = intval($_GET["parentRegionId"]);
				} else {
  					$rss->parentRegionId = 0;
				}
  				$result = $rss->SelectRegion();
  				
				$smarty->assign("data", $result);
				$output_city = $smarty->fetch("./site/rsscity.tpl");
				echo minimizer($output_city);

            ?>
            
            <?
				if(isset($_GET["categoryId"]) && intval($_GET["categoryId"]) > 0) {
					$rss->subCategoryId =  intval($_GET["categoryId"]);

					$res_sub = $rss->SelectSubCategory();

					if(sizeof($res_sub) > 1)
					{
						$smarty->assign("subbizcategory", $res_sub);
						$output_subbiz = $smarty->fetch("./site/rsssubbiz.tpl");
						echo minimizer($output_subbiz);
					} 
				}            
          	?>
            
				<div class="lftpadding_cnt" style="padding-top:10pt; padding-bottom: 5pt;padding-top:0pt; padding-bottom:0pt; width:auto;">
					<div style="width:auto;">
						<div  style="background-image:url(http://www.bizzona.ru/images/bl.gif); background-repeat:repeat-x;">&nbsp;</div>
					</div>
					<?
						$options = array();
						
						if(isset($_GET["parentRegionId"]) && intval($_GET["parentRegionId"]) > 0) {
							$options["parentRegionId"] = intval($_GET["parentRegionId"]);
						}
						if(isset($_GET["categoryId"]) && intval($_GET["categoryId"]) > 0) {
							$options["categoryId"] = intval($_GET["categoryId"]);
						}
						if(isset($_GET["subCategoryId"]) && intval($_GET["subCategoryId"]) > 0) {
							$options["subCategoryId"] = intval($_GET["subCategoryId"]);
						}
						
						$resrss = $rss->ListRSS($options);
						$smarty->assign("resrss", $resrss);
						$output_rsslist = $smarty->fetch("./site/rsslist.tpl");
						echo minimizer($output_rsslist);
					?>
				</div>

        </td>
    </tr>
</table>
<?
	//$smarty->display("./site/sape.tpl");
?>
<style>
 td a 
 {
   color:black;
 }
</style>
<div style="padding-top:4pt;padding-bottom:5pt;padding-right:10pt;padding-left:10pt;font-size:10pt;font-family:arial;">
	<table cellpadding="0" cellspacing="0" border="0"  style="width:100%;padding-bottom:2pt;" bgcolor="#eeeee0">
    	<tr>
        	<td style="background-image:url(http://<?=$_SERVER["HTTP_HOST"]?>/images/d1.gif); background-repeat:no-repeat;width:5px;height:5px; background-position:left top;">&nbsp;</td>
            <td rowspan="2" valign="middle" align="center"  style="font-size:8pt; font-family:Arial; color:black;">
			<?
     			//define('_SAPE_USER', 'b91a4da0b21c4d197163d613e54e8d28'); 
     			//require_once($_SERVER['DOCUMENT_ROOT'].'/'._SAPE_USER.'/sape.php'); 
     			//$sape = new SAPE_client();
     			//echo $sape->return_links();
			?>
            </td>
            <td style="background-image:url(http://<?=$_SERVER["HTTP_HOST"]?>/images/d2.gif); background-repeat:no-repeat;width:5px;height:5px;background-position:right top;">&nbsp;</td>
        </tr>
        <tr>
            <td style="background-image:url(http://<?=$_SERVER["HTTP_HOST"]?>/images/d4.gif); background-repeat:no-repeat;width:5px;height:5px; background-position:left bottom;">&nbsp;</td>
            <td style="background-image:url(http://<?=$_SERVER["HTTP_HOST"]?>/images/d3.gif); background-repeat:no-repeat;width:5px;height:5px; background-position:right bottom;">&nbsp;</td>
        </tr>
    </table>
</div>
<?
	$smarty->display("./site/footer.tpl");
	
  	//$output_footer = $smarty->fetch("./site/footer.tpl");
  	//echo minimizer($output_footer);
	
?>
<?
  $end = get_formatted_microtime(); 
  $total = $end - $start;
  echo "<center><span style='font-size:7pt;'>".round($total, 6)." : ".$count_sql_call." : ".$base_memory_usage." : ".memoryUsage(memory_get_usage(), $base_memory_usage)." </span><center>";
?>

</body>
</html>

<?
	$sell->Close();
	$category->Close();
	$city->Close();
	$country->Close();
	$buyer->Close();
	$metro->Close();
	$district->Close();
	$subcategory->Close();
	$streets->Close();
?>
