<?
    global $DATABASE;

    include_once("./phpset.inc");
    include_once("./engine/functions.inc");
    include_once('./inserthelp.php');    

    ini_set("display_startup_errors", "off");
    ini_set("display_errors", "off");
    ini_set("register_globals", "on");

    $start = get_formatted_microtime();

    AssignDataBaseSetting();

    include_once("./engine/class.RoomCategory.inc"); 
    $category = new RoomCategory();

    include_once("./engine/class.Room.inc"); 
    $room = new Room();  

    include_once("./engine/class.operator.inc"); 

    include_once("./engine/class.BlockIP.inc"); 
    $blockIp = new BlockIP();
    
    
    $blocking  = $blockIp->YouAreBlocking(array(
        'ip' => $_SERVER['REMOTE_ADDR']
    ));  
  
    include_once("./engine/class.insert_settings.inc"); 
    $insertSettings = new InsertSettings();  
    
    $insertSettings->type_insert = ConstInsertSettings::ROOM;
    $insertSettingsData = $insertSettings->GetItem();
    
    require_once("./libs/Smarty.class.php");
    $smarty = new Smarty;
    $smarty->template_dir = "./templates";
    $smarty->compile_dir  = "./templates_c";
    //$smarty->cache_dir = "./cache";
    $smarty->compile_check = true;
    $smarty->debugging     = false;
    $smarty->clear_compiled_tpl("./site/insertroom.tpl");

    require_once("./regfuncsmarty.php");    
     
    $assignDescription = AssignDescription();

    $error = "";


      if (isset($_POST["insert"]) && !$blocking) {

        $ErrorFile = false;

        if (isset($_FILES["ImgID"])) {
            $newname = LoadClientImg("ImgID", 1, 'room_');
        } 


        if (!$ErrorFile) {

          $costsymbols = array(".", " ", ",");

          $data["FML"]              = htmlspecialchars(trim($_POST["FML"]), ENT_QUOTES);
          $_POST["FML"] = $data["FML"];

          $data["EmailID"]          = htmlspecialchars(trim(strtolower($_POST["EmailID"])), ENT_QUOTES);
          $_POST["EmailID"] = $data["EmailID"];

          $data["PhoneID"]          = htmlspecialchars(trim($_POST["PhoneID"]), ENT_QUOTES);
          $_POST["PhoneID"] = $data["PhoneID"];

          $data["CategoryID"] = htmlspecialchars(trim($_POST["CategoryID"]), ENT_QUOTES);
          $_POST["CategoryID"] = $data["CategoryID"];

          $data["SiteID"]           = htmlspecialchars(trim($_POST["SiteID"]), ENT_QUOTES);
          $_POST["SiteID"] = $data["SiteID"];


          $data["txtCostID"]   = htmlspecialchars(trim($_POST["CostID"]), ENT_QUOTES);
          $_POST["CostID"] = $data["txtCostID"];

          $data["CostID"]           = str_replace($costsymbols, "", htmlspecialchars(trim($_POST["CostID"]), ENT_QUOTES));
          $_POST["CostID"] = $data["CostID"];

          $data["cCostID"]          = htmlspecialchars(trim($_POST["cCostID"]), ENT_QUOTES);
          $_POST["cCostID"] = $data["cCostID"];

          $data["ShortDetailsID"]   = htmlspecialchars(trim($_POST["ShortDetailsID"]), ENT_QUOTES);
          $_POST["ShortDetailsID"] = $data["ShortDetailsID"];

          $str_ShortDetailsID = $data["ShortDetailsID"];
          hyperlink(&$str_ShortDetailsID, 8);
          $data["ShortDetailsID"] = $str_ShortDetailsID;

          $data["ImgID"]            = (strlen($newname) > 0 ? $newname : ""); 
          $data["FaxID"]            = htmlspecialchars(trim($_POST["FaxID"]), ENT_QUOTES);
          $_POST["FaxID"] = $data["FaxID"];

          $data["youtubeURL"] = htmlspecialchars(trim($_POST["youtubeURL"]), ENT_QUOTES);
          $_POST["youtubeURL"] = $data["youtubeURL"];

          $data["areaID"] = htmlspecialchars(trim($_POST["areaID"]), ENT_QUOTES);
          $_POST["areaID"] = $data["areaID"];      


          if (strlen($data["FML"]) <= 0) {
            $error = "������ �����: "."\"��� � ������� ����, �������������� �� ��������\"";
            $smarty->assign("ErrorInput_FML",true);
          } else if (strlen($data["EmailID"]) <= 0) {
            $error = "������ �����: "."\"���������� �-����\"";
            $smarty->assign("ErrorInput_EmailID",true);
          } else if (strlen($data["PhoneID"]) <= 0) {
            $error = "������ �����: "."\"����� ��������\"";
            $smarty->assign("ErrorInput_PhoneID",true);
          } else if (strlen($data["SiteID"]) <= 0) { 
            $error = "������ �����: "."\"�����, �����, ���, ��������/����\"";
            $smarty->assign("ErrorInput_SiteID",true);
          } else if (strlen($data["CostID"]) <= 0) {
            $error = "������ �����: "."\"��������� ������������ ���������\"";
            $smarty->assign("ErrorInput_CostID",true);
          } else if (strlen($data["areaID"]) <= 0) {
            $error = "������ �����: "."\"�� ������� ������� ���������\"";
            $smarty->assign("ErrorInput_areaID",true);
          } else if (strlen($data["ShortDetailsID"]) <= 0) {
            $error = "������ �����: "."\"������� ����������, ������ ������������� � ����������� �������\"";     
            $smarty->assign("ErrorInput_ShortDetailsID",true);
          } else {
          }

          if (strlen($error) <= 0) {

            $to      = 'bizzona.ru@gmail.com';
            $subject = 'NEW SUBSCRIBE ROOM : '.$data["FML"].'';

            $message = $assignDescription["FML"]." ".$data["FML"]."\n";
            $message.= $assignDescription["EmailID"]." ".$data["EmailID"]."\n";
            $message.= $assignDescription["PhoneID"]." ".$data["PhoneID"]."\n";
            $message.= $assignDescription["CategoryID"]." ".$data["CategoryID"]."\n";
            $message.= $assignDescription["SiteID"]." ".$data["SiteID"]."\n";
            $message.= $assignDescription["CostID"]." ".$data["CostID"]."\n";
            $message.= $assignDescription["ShortDetailsID"]." ".$data["ShortDetailsID"]."\n";
            $message.= $assignDescription["ImgID"]." ".$data["ImgID"]."\n";
            $message.= $assignDescription["FaxID"]." ".$data["FaxID"]."\n";
            $message.= $assignDescription["AreaID"]." ".$data["areaID"]."\n"; 
            $message.= $assignDescription["YoutubeURL"]." ".$data["youtubeURL"]."\n"; 

            $headers = 'BizZONA.RU: new subscribe' . "\r\n" .
            'Reply-To: '.$data["EmailID"].' ' . "\r\n" .
            'X-Mailer: PHP/' . phpversion();
            mail($to, $subject, $message, $headers);

            $data["CostID"] = floatval($data["CostID"]);

            $room->RoomCategoryId = $data["CategoryID"];
            $room->ShortDescription = $data["ShortDetailsID"];
            $room->Description = $data["ShortDetailsID"];
            $room->area_text = $data["areaID"];
            $room->area_from = 0;
            $room->area_to = 0;
            $room->cost = intval($data["CostID"]);
            $room->cost_text = $data["txtCostID"]; 
            $room->cCostId = $data["cCostID"];
            $room->photo_1 = $data["ImgID"];
            $room->statusId = 0;
            $room->fio = $data["FML"];
            $room->phone = $data["PhoneID"];
            $room->email = $data["EmailID"];
            $room->youtubeURL = $data["youtubeURL"];
            $room->fax = $data["FaxID"];
            $room->ip = $_SERVER['REMOTE_ADDR'];
            $room->Address = $data["SiteID"];

            if(isset($_SESSION["partnerid"])) {
                    $room->partnerid = intval($_SESSION["partnerid"]);
            }

            $mysql_insert_id = $room->Insert();
            $_SESSION['timepost'] = time();

            $smarty->assign("code", $mysql_insert_id);
            $smarty->assign("email", $_POST["EmailID"]);

            $statusAdd = true;

            $secret_code = "vbhjplfybt";
            $purse = "9192"; // sms:bank id        ������������� ���:�����
            $order_id = $mysql_insert_id; //  ������������� ��������
            $amount = "5.5"; // transaction sum  ����� ����������
            $amount_megafon = "4"; // transaction sum  ����� ����������
            $amount_ukraine = "2.4"; // transaction sum  ����� ����������
            $clear_amount  = "0"; // billing algorithm  �������� �������� ���������
            $description  = "������ ���������� ������� ���������"; // operation desc  �������� ��������
            $submit  = "���������"; // submit label    ������� �� ������ submit
            $submit_megafon  = "��������� (��� ��������)"; // submit label    ������� �� ������ submit
            $submit_ukraine  = "���������"; // submit label    ������� �� ������ submit

            $m_description = cp1251_to_utf8($description."  (".$order_id.") ");
            $m_description = base64_encode($m_description);		


            $sign = ref_sign($purse, $order_id, $amount, $clear_amount, $description, $secret_code);
            $sign_megafon = ref_sign($purse, $order_id, $amount_megafon, $clear_amount, $description, $secret_code);
            $sign_ukraine = ref_sign($purse, $order_id, $amount_ukraine, $clear_amount, $description, $secret_code);

            $smarty->assign("purse", $purse);
            $smarty->assign("order_id", $order_id);
            $smarty->assign("rbk_order_id", $order_id);
            $smarty->assign("amount", $amount);
            $smarty->assign("amount_megafon", $amount_megafon);
            $smarty->assign("amount_ukraine", $amount_ukraine);
            $smarty->assign("clear_amount", $clear_amount);
            $smarty->assign("description", $description);
            $smarty->assign("m_description", $m_description);
            $smarty->assign("submit", $submit);
            $smarty->assign("submit_megafon", $submit_megafon);
            $smarty->assign("submit_ukraine", $submit_ukraine);
            $smarty->assign("sign", $sign);
            $smarty->assign("sign_megafon", $sign_megafon);
            $smarty->assign("sign_ukraine", $sign_ukraine);

            unset($_POST);
          }
        }
      } 

    //end = get_formatted_microtime();

    //$end = get_formatted_microtime();

    $data["offset"]   = 0;
    $data["rowCount"] = 0;
    $data["sort"]= "Count";
    $listCategory = $category->Select($data); 

    unset($data["sort"]);
    $data["offset"]   = 0;
    $data["rowCount"] = 0;
    //$listCity = $city->Select($data); 



    $smarty->assign("listCategory", $listCategory);
    //$smarty->assign("listCity", $listCity);

    $listSubCategory =  $category->Select();
    $smarty->assign("listSubCategory", $listSubCategory);

    //$end = get_formatted_microtime(); 

    include_once("./engine/class.ContactInfo_Lite.inc");
    $contact = new ContactInfo_Lite();  

    $contact->GetContactInfo();  
    $smarty->assign("contact", $contact);

    $smarty->assign("insertsettings", $insertSettingsData);
    
?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>

  <HEAD>
      <title>���������� ������� �� ������� ��������� - ���, "������ ����"</title>		
	<META NAME="Description" CONTENT="���������� ������� ��������� - ���, ������ ����">
        <meta name="Keywords" content="���������� ������� ��������� - ���, ������ ����"> 
		<meta http-equiv="content-type" content="text/html; charset=windows-1251"/>
        <LINK href="./general.css" type="text/css" rel="stylesheet">
<link rel="shortcut icon" href="./images/favicon.ico">
  </HEAD>


<?
	$topmenu = GetMenu();
	$smarty->assign("topmenu", $topmenu);

	$link_country = "";
	$link_country.= "&nbsp;&nbsp;<a href='".NAMESERVER."help.php' class='city_a'  style='color:white;font-size:10pt;color:yellow;' title='������ - ������� �������' target='_blank'>������</a>&nbsp;&nbsp;";	
	$smarty->assign("ad", $link_country);
	$smarty->display("./site/headerinsertroom.tpl");
?>
  
<table class="w" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td width="100%">
         <table width='100%' height='100%'>
           <?
              if (strlen($error) > 0) { 
           ?> 
           <tr  valign='top' width='100%' height='100%'>
             <td align='center'><span style="color:red;"><b><?=$error;?></b></span></td> 
           </tr>
           <?
              }
           ?> 
           <tr>
             <td valign='top' width='60%' height='100%'>
               <?
                  //$end = get_formatted_microtime();
               
                  if($blocking) {
                      $smarty->display("./site/blockip.tpl");
                  } else {
                      $smarty->display("./site/insertroom.tpl");
                  }
               
                  
                  //$end = get_formatted_microtime();
               ?>
             </td>
           </tr>
         </table>
        
        
        </td>
    </tr>
</table>

<?php
//$end = get_formatted_microtime();
?>

<style>
 td a 
 {
   color:black;
 }
</style>


<div style="padding-top:4pt;padding-bottom:5pt;padding-right:10pt;padding-left:10pt;font-size:10pt;font-family:arial;">
	<table cellpadding="0" cellspacing="0" border="0"  style="width:100%;padding-bottom:2pt;" bgcolor="#eeeee0">
    	<tr>
        	<td style="background-image:url(<?=$_SERVER["host_name"]?>images/d1.gif); background-repeat:no-repeat;width:5px;height:5px; background-position:left top;">&nbsp;</td>
            <td rowspan="2" valign="middle" align="center"  style="font-size:8pt; font-family:Arial; color:black;">
			<?
			?>
            </td>
            <td style="background-image:url(<?=$_SERVER["host_name"]?>images/d2.gif); background-repeat:no-repeat;width:5px;height:5px;background-position:right top;">&nbsp;</td>
        </tr>
        <tr>
            <td style="background-image:url(<?=$_SERVER["host_name"]?>images/d4.gif); background-repeat:no-repeat;width:5px;height:5px; background-position:left bottom;">&nbsp;</td>
            <td style="background-image:url(<?=$_SERVER["host_name"]?>images/d3.gif); background-repeat:no-repeat;width:5px;height:5px; background-position:right bottom;">&nbsp;</td>
        </tr>
    </table>
</div>

<?
	$smarty->display("./site/footer.tpl");
?>

<?php
$category->Close();
$room->Close();
?>


<?
  $end = get_formatted_microtime();
  $total = $end - $start;
  echo "<center><span style='font-size:7pt;'>".round($total, 6)."</span><center>";
?>


</body>
</html>
