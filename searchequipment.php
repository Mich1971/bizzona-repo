<?
  global $DATABASE;

  include_once("./phpset.inc");
  include_once("./engine/functions.inc"); 

  $count_sql_call = 0;
  $start = get_formatted_microtime(); 
  $base_memory_usage = memory_get_usage();	

  AssignDataBaseSetting();
  
	include_once("./engine/class.category_lite.inc"); 
	$category = new Category_Lite();

	include_once("./engine/class.country_lite.inc"); 
	$country = new Country_Lite();	

	include_once("./engine/class.city_lite.inc"); 
	$city = new City_Lite();

	include_once("./engine/class.sell_lite.inc"); 
	$sell = new Sell_Lite();

	include_once("./engine/class.buy_lite.inc"); 
	$buyer = new Buyer_Lite();

  	include_once("./engine/class.equipment_lite.inc"); 
  	$equipment = new Equipment_Lite();	

  	include_once("./engine/class.district_lite.inc"); 
  	$district = new District_Lite();
  	include_once("./engine/class.subcategory_lite.inc");
  	$subcategory = new SubCategory_Lite();

	include_once("./engine/class.streets.inc"); 
	$streets = new streets();

  	include_once("./engine/class.ad.inc");
  	$ad = new Ad();	
	
  require_once("./libs/Smarty.class.php");
  $smarty = new Smarty;
  $smarty->template_dir = "./templates";
  $smarty->compile_dir  = "./templates_c";
  //$smarty->cache_dir = "./cache";
  $smarty->compile_check = true;
  $smarty->debugging     = false;

  require_once("./regfuncsmarty.php");

  if(isset($_GET["ID"])) {
  	$_GET["ID"] = intval($_GET["ID"]);
  }

	$action_words = array("category", "city", "price", "typeBizID");
	if(isset($_GET["action"])) {
		if(in_array($_GET["action"], $action_words)) {
		} else {
			$_GET["action"] = "";
		}
	} 

  $ad->InitAd($smarty, $_GET);

  
	if(isset($_GET['aCategoryID'])) {
		$_GET['CategoryID'] = explode(',', $_GET['aCategoryID']);
	}
  
	if(isset($_GET['aCityID'])) {
		$_GET['CityID'] = explode(',', $_GET['aCityID']);
	}  
  
	if(isset($_GET['CategoryID']) && is_array($_GET['CategoryID'])) {
  		$aCategoryID = $_GET['CategoryID'];
		array_walk($aCategoryID, create_function('&$v', 'return $v=intval($v);'));  		
	}
  
  	if(isset($_GET['CityID']) && is_array($_GET['CityID'])) {
  		$aCityID = $_GET['CityID'];
		array_walk($aCityID, create_function('&$v', 'return $v=intval($v);'));   	
  	}   
  	
  	
  	if(isset($_GET["startcostf"])) {
  		$_GET["startcostf"] = intval($_GET["startcostf"]);
  	} else {
  		$_GET["startcostf"] = 0;
  	}
  
  	if(isset($_GET["stopcostf"])) {
  		$_GET["stopcostf"] = intval($_GET["stopcostf"]);
  	} else {
  		$_GET["stopcostf"] = 0;
  	}

  	if((intval($_GET["startcostf"]) > intval($_GET["stopcostf"])) && (intval($_GET["startcostf"]) > 0 && intval($_GET["stopcostf"]) > 0) ) {
  		$temp_c  = $_GET["stopcostf"];
  		$_GET["stopcostf"] = $_GET["startcostf"];
  		$_GET["startcostf"] = $temp_c;
  	}  	
  	
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
  <HEAD>
<?
  $seosubcategory = "";
  $title = "������� � ������� ������������ - ��� \"������-����\"";

  if (isset($_GET["action"])) {
    switch(trim($_GET["action"])) {
      case "typeBizID": {
      	if(isset($_GET["subCategory_id"]) && intval($_GET["subCategory_id"]) > 0) {
      		
      		$subcategory->ID = intval($_GET["subCategory_id"]);
      		$seosubcategory = $subcategory->GetKeySellEquipment();
        	if(strlen($seosubcategory) > 0) {
          		$title = $seosubcategory.' - ��� "������-����';
        		$seosubcategory = $seosubcategory.", ";
        	} 
      	}
      } break;
      case "city": {
      	if(isset($_GET["subCity_id"]) && intval($_GET["subCity_id"]) > 0) {
      		$dataCity["ID"] = intval($_GET["subCity_id"]); 
      	} else {
      		$dataCity["ID"] = intval($_GET["ID"]);	
      	}
        
        $objCity = $city->GetItem($dataCity);
        $title = '������� ������������ '.$objCity->title.' | ������� ������������ '.$objCity->title.' | ��� "������-����"';
      } break;
      default: {
      }   
    }
  }
  
	if(isset($_GET["goodcatparent_id"])) {
		$a_goodcatparent[10] = '������� ������������ ��� �����������, �����������, ���';
		$a_goodcatparent[12] = '������� ������������ ��� ���������';
		$a_goodcatparent[11] = '������� ������������ ��� ������ �������, ��������������';
		if(isset($a_goodcatparent[intval($_GET["goodcatparent_id"])])) {
			$title = $a_goodcatparent[intval($_GET["goodcatparent_id"])];
		}
	}
?>
		<title><?=$title;?></title>
        <META NAME="Description" CONTENT="������� ������������,������������ ��� �������, �������� ������� ������������, ������������ ��� ������ �������, ������������ ��� ��������������, ������������ ��� �����������, ������������ ��� ������������">
        <meta name="Keywords" content="<?=$seosubcategory;?> ������� ������������, ������������ ��� �������, �������� ������� ������������, ������������ ��� ������ �������, ������������ ��� ��������������, ������������ ��� �����������, ������������ ��� ������������"> 
        <LINK href="http://www.bizzona.ru/general.css" type="text/css" rel="stylesheet">
        <meta http-equiv="content-type" content="text/html; charset=windows-1251"/>
  </HEAD>
<body>
<?
  	$smarty->display("./site/headerbanner.tpl");
	$smarty->assign("headertitle", ((isset($_GET["goodcatparent_id"]) && intval($_GET["goodcatparent_id"]) > 0 ) ? $title : '������� ������������'));
?>

<table class="w" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="2">
		<?
			$topmenu = GetMenu();
			$smarty->assign("topmenu", $topmenu);

			$link_country = GetSubMenu();
			$smarty->assign("ad", $link_country);

			$smarty->caching = false;
			$output_header = $smarty->fetch("./site/headere.tpl");
			echo minimizer($output_header);
		?>
		</td>
	</tr>
    <tr>
        <td width="30%" valign="top">
        
        <?
        	$smarty->caching = false; 
			echo fminimizer($smarty->fetch("./site/inner_bannertop_left.tpl"));
		?>        
        
        <?
          $smarty->cache_dir = "./persistentcache"; 
          $smarty->cache_lifetime = 10800;        
        
          if (isset($_GET["action"]) && $_GET["action"]  == "category") {
            $_GET["ID"] = (isset($_GET["ID"])  ? intval($_GET["ID"]) : 0);
            $smarty->caching = true; 
            if (!$smarty->is_cached('./site/categoryse.tpl', $_GET["ID"])) {
              $data = array();
              $data["offset"] = 0;
              $data["rowCount"] = 0;
              $data["sort"] = "CountE";
              $result = $category->Select($data);
              $smarty->assign("data", $result);
            }
            
  			$output_categoryse = $smarty->fetch("./site/categoryse.tpl", $_GET["ID"]);
  			echo minimizer($output_categoryse);            
            $smarty->caching = false; 
            
          } else { 
            $smarty->caching = true; 
            if (!$smarty->is_cached('./site/categoryse.tpl')) {
              $data = array();
              $data["offset"] = 0;
              $data["rowCount"] = 0;
              $data["sort"] = "CountE";
              $result = $category->Select($data);
              $smarty->assign("data", $result);
            }
  			$output_categoryse = $smarty->fetch("./site/categoryse.tpl");
  			echo minimizer($output_categoryse);
            $smarty->caching = false; 
            
            
            $smarty->caching = false; 
          }
          $smarty->cache_dir = "./cache";
          $smarty->cache_lifetime = 3600;          
        ?>
        <?
            	$datahotbuyer = array();
            	$datahotbuyer["offset"] = 0;
            	$datahotbuyer["rowCount"] = 4;
            	$datahotbuyer["sort"] = "datecreate";
            	$datahotbuyer["StatusID"] = "-1";

            	
                if (isset($_GET["action"])) {
                   switch(trim($_GET["action"])) {
	                   case "category": {
    	                 $datahotbuyer["typeBizID"] = intval($_GET["ID"]);
                       } break;
                       case "city": {
                         $datahotbuyer["regionID"] = intval($_GET["ID"]);
                       } break;                       
        	           default: {
                         		
                       }
                       break;
                   }            	
                }		
            	
                //$smarty->display("./site/call.tpl");
			?>
		<div style="padding-top:2pt;"></div>   	
        <?
			$output_proposal = $smarty->fetch("./site/proposal.tpl");
			echo minimizer($output_proposal);
        	
			$output_request = $smarty->fetch("./site/request.tpl");
			echo minimizer($output_request);
			
			$output_shoprequest = $smarty->fetch("./site/shoprequest.tpl");
			echo minimizer($output_shoprequest);			
        ?>  
        </td>
        <td width="70%" valign="top">
        	<?
        		if(!isset($_GET["action"]) or $_GET["action"] != "typeBizID") {
					echo minimizer($smarty->fetch("./site/topequipment.tpl"));
        		}
			?>
        
			<?
            	$smarty->caching = true; 
        		$smarty->cache_lifetime = 10800;
           		$smarty->cache_dir = "./persistentcache";                
                if (!$smarty->is_cached('./site/citye.tpl')) {
                	$data = array();
                    $data["offset"] = 0;
                    $data["rowCount"] = 0;
                    $data["sort"] = "CountE";
                    $result = $city->SelectActiveE($data);
                    $smarty->assign("data", $result);
                } 
				$output_city = $smarty->fetch("./site/citye.tpl");
				echo minimizer($output_city);                

				$smarty->cache_dir = "./cache";
         		$smarty->cache_lifetime = 3600;                
                $smarty->caching = false; 
                
					// up subcity show
            		$smarty->caching = true; 
            		$smarty->cache_lifetime = 3600;
					$city_cach = ((isset($_GET["action"]) && $_GET["action"] == "city") ? intval($_GET["ID"]) : 0 );
            		if (!$smarty->is_cached('./site/subcitye.tpl', $city_cach)) {
            			$data = array();
            			$data["offset"] = 0;
            			$data["rowCount"] = 0;
						$city->CityID = intval($_GET["ID"]);            		
            			$result = $city->SelectChildActive2Equipment($data);
            			$smarty->assign("items", sizeof($result)/3);
                		$smarty->assign("datasubcity", $result);
                	
    					$dataCity["ID"] = intval($_GET["ID"]);	
        				$objCity = $city->GetItem($dataCity);                	
        				$smarty->assign("citytitleparent", $objCity->titleParentSelect);
                	} 
                
					$output_city = $smarty->fetch("./site/subcitye.tpl", $city_cach);
					echo minimizer($output_city);
       				$smarty->caching = false;       			
       				// down subcity show           			
                
            ?>
           
            <?
				if(isset($_GET["action"]) && $_GET["action"]  == "typeBizID") {
					$equipment->category_id = (isset($_GET["ID"])  ? intval($_GET["ID"]) : 0);
					$res_sub = $equipment->SelectSubCategory();
					if(sizeof($res_sub) > 1) {
						$smarty->assign("subbizcategory", $res_sub);
						$output_subbiz = $smarty->fetch("./site/subbize.tpl");
						echo minimizer($output_subbiz);
					}
				}

				$output_navsearch = $smarty->fetch("./site/navsearche.tpl");
				echo minimizer($output_navsearch);

                    $data = array();
                    $dataPage = array(); 
                    $pagesplit = 12;//GetPageSplit();

                    if (isset($_GET["offset"])) {
                      $data["offset"] = intval($_GET["offset"]);
                    } else {
                      $data["offset"] = 0;
                    }
                    if (isset($_GET["rowCount"])) { 
                      $data["rowCount"] = intval($_GET["rowCount"]);
                    } else {
                      $data["rowCount"] = $pagesplit;
                    }

                    $data["sort"] = "ID";

                    if(isset($_GET["subCity_id"]) && intval($_GET["subCity_id"]) > 0) {
                    	$data["subCity_id"] = intval($_GET["subCity_id"]);
                    	$dataPage["subCity_id"] = intval($_GET["subCity_id"]);
                    }
                    
                    if (isset($_GET["action"])) {
                       switch(trim($_GET["action"])) {
                         case "typeBizID": {
                           $_GET["ID"] = intval($_GET["ID"]); 
                           $data["category_id"] = $_GET["ID"];
                           $dataPage["category_id"] = $_GET["ID"];
                           
                           if(isset($_GET["subCategory_id"]) && intval($_GET["subCategory_id"]) > 0) {
                             $_GET["subCategory_id"] = intval($_GET["subCategory_id"]);	
                             $data["subCategory_id"] = $_GET["subCategory_id"];
                             $dataPage["subCategory_id"] = $_GET["subCategory_id"];
                           }
                           
                         } break;
                         case "city": {
                           $_GET["ID"] = intval($_GET["ID"]);
                           $data["city_id"] = $_GET["ID"];
                           $dataPage["city_id"] = $_GET["ID"];
                         } break;
                         default: {
                         }   
                       }
                    }
                    
                    if(isset($_GET["goodcatparent_id"]) && intval($_GET["goodcatparent_id"]) > 0) {
                       $data["goodcatparent_id"] = intval($_GET["goodcatparent_id"]);
                       $dataPage["goodcatparent_id"] = $data["goodcatparent_id"];
                    }

  					$data["stopcostf"] = intval($_GET["stopcostf"]);
  					$data["startcostf"] = intval($_GET["startcostf"]);			
  					$dataPage["stopcostf"] = $data["stopcostf"];
					$dataPage["startcostf"] = $data["startcostf"];
                        
                    $dataPage["offset"] = 0;
                    $dataPage["rowCount"] = 0;
                    $dataPage["sort"] = "ID";
                    $dataPage["status_id"] = "-1";
                    
                    if(isset($aCategoryID)) {
                    	$dataPage["aCategoryID"] = $aCategoryID;
                    	$data["aCategoryID"] = $aCategoryID;
                    }
                    if(isset($aCityID)) {
                    	$dataPage["aCityID"] = $aCityID;
                    	$data["aCityID"] = $aCityID;
                    }                    
                    
                    $resultPage = $equipment->Select($dataPage);
                    $smarty->assign("CountRecord", sizeof($resultPage));
                    $smarty->assign("CountSplit", $pagesplit);
                    $smarty->assign("CountPage", ceil(sizeof($resultPage)/5));

                    $data["status_id"] = "-1";
                    $data["sort"] = "datecreate";

                    $result = $equipment->Select($data);

                    $smarty->assign("data", $result);

				  	$output_pagesplit = $smarty->fetch("./site/pagesplite.tpl", $_SERVER["REQUEST_URI"]);
				  	echo minimizer($output_pagesplit);
                  	
                  	
				 	//$output_ilistsell = $smarty->fetch("./site/ilistselle.tpl", $_SERVER["REQUEST_URI"]);
				 	$output_ilistsell = $smarty->fetch("./site/listsellshop.tpl", $_SERVER["REQUEST_URI"]);
				  	echo minimizer($output_ilistsell);
               ?>

            	<div class="lftpadding" style="width: auto;">
       				<div  style="background-image:url(<?=$_SERVER["HTTP_HOST"]?>images/bl.gif); background-repeat:repeat-x;">&nbsp;</div>
            	</div>
               
            	<?
               		echo minimizer($output_pagesplit);
               		echo minimizer($output_navsearch);
            	?>
            	
				<div class="lftpadding_cnt" style="padding-top:10pt; padding-bottom: 5pt;padding-top:0pt; padding-bottom:0pt; width:auto;" >	    
					<table border="0" align="right" width="100%">
                		<tr>
                			<td align="right">
								<script type="text/javascript" src="//yandex.st/share/share.js" charset="utf-8"></script>
								<div class="yashare-auto-init" data-yashareType="button" data-yashareQuickServices="yaru,vkontakte,facebook,twitter,odnoklassniki,moimir,lj,friendfeed,moikrug"></div>
               				</td>			
						</tr>
					</table>
				</div>            	
        </td>
    </tr>
    <tr>
    	<td colspan="2">
			<div style="padding-top:4pt;padding-bottom:5pt;padding-right:10pt;padding-left:10pt;font-size:10pt;font-family:arial;">
				<table cellpadding="0" cellspacing="0" border="0"  style="width:100%;padding-bottom:2pt;" bgcolor="#eeeee0">
    				<tr>
        				<td style="background-image:url(http://<?=$_SERVER["HTTP_HOST"]?>/images/d1.gif); background-repeat:no-repeat;width:5px;height:5px; background-position:left top;">&nbsp;</td>
            			<td rowspan="2" valign="middle" align="center"  style="font-size:8pt; font-family:Arial; color:black;">
            			<?
            			    if(isset($_GET["offset"]) && intval($_GET["offset"]) > 60) { 
            			    	 
            			    } else if(isset($_GET["offset"]) && intval($_GET["offset"]) == 60) {
            			    	?>
           							<a href="http://www.bizzona.ru/proizvodstvennoe-oborudovanie.php" class="city_a" style="font-size:10pt;" title="���������������� ������������">���������������� ������������</a> 			    	
            			    	<?
							} else if(isset($_GET["offset"]) && intval($_GET["offset"]) == 48) {
            			    	?>
            			    		<a href="http://www.bizzona.ru/internet-magazin-oborudovaniya.php" class="city_a" style="font-size:10pt;" title="�������� ������� ������������">�������� ������� ������������</a>&nbsp;&nbsp;<a href="http://www.bizzona.ru/stomatologicheskoe-oborudovanie.php" class="city_a" style="font-size:10pt;" title="����������������� ������������">����������������� ������������</a>&nbsp;&nbsp;<a href="http://www.bizzona.ru/torgovoe-oborudovanie-dlya-magazinov-odezhdy.php" class="city_a" style="font-size:10pt;" title="�������� ������������ ��� ��������� ������">�������� ������������ ��� ��������� ������</a>&nbsp;&nbsp;<a href="http://wap.bizzona.ru/torgovoe-cholodilnoe-oborudovanie.php" class="city_a" style="font-size:10pt;" title="�������� ����������� ������������">�������� ����������� ������������</a>
            			    	<?
							} else if(isset($_GET["offset"]) && intval($_GET["offset"]) == 36) {
            			    	?>
            			    		<a href="http://www.bizzona.ru/promyshlennoe-oborudovanie.php" class="city_a" style="font-size:10pt;" title="������������ ������������">������������ ������������</a>&nbsp;&nbsp;<a href="http://www.bizzona.ru/seni-na-oborudovanie.php" class="city_a" style="font-size:10pt;" title="���� �� ������������">���� �� ������������</a>&nbsp;&nbsp;<a href="http://www.bizzona.ru/torgovoe-oborudovanie-dlya-magazinov.php" class="city_a" style="font-size:10pt;" title="�������� ������������ ��� ���������">�������� ������������ ��� ���������</a>&nbsp;&nbsp;<a href="http://www.bizzona.ru/oborudovanie-dlya-shinomontazha.php" class="city_a" style="font-size:10pt;" title="������������ ��� �����������">������������ ��� �����������</a>&nbsp;&nbsp;<a href="http://www.bizzona.ru/ooo-oborudovanie.php" class="city_a" style="font-size:10pt;" title="��� ������������">��� ������������</a>&nbsp;&nbsp;<a href="http://www.bizzona.ru/rinok-oborudovaniya.php" class="city_a" style="font-size:10pt;" title="����� ������������">����� ������������</a>
            			    	<?
							} else if(isset($_GET["offset"]) && intval($_GET["offset"]) == 24) {
            			    	?>
           							<a href="http://www.bizzona.ru/oborudovanie-dlya-malogo-biznesa.php" class="city_a" style="font-size:10pt;" title="������������ ��� ������ �������">������������ ��� ������ �������</a>&nbsp;&nbsp;<a href="http://www.bizzona.ru/oborudovanie-dlya-avtoservisa.php" class="city_a" style="font-size:10pt;" title="������������ ��� �����������">������������ ��� �����������</a>&nbsp;&nbsp;<a href="http://www.bizzona.ru/oborudovanie-dlya-magazinov.php" class="city_a" style="font-size:10pt;" title="������������ ��� ���������">������������ ��� ���������</a>&nbsp;&nbsp;<a href="http://www.bizzona.ru/stroitelnoe-oborudovanie.php" class="city_a" style="font-size:10pt;" title="������������ ������������">������������ ������������</a>&nbsp;&nbsp;<a href="http://www.bizzona.ru/medisinskoe-oborudovanie.php" class="city_a" style="font-size:10pt;" title="����������� ������������">����������� ������������</a> 
            			    	<?
							} else if(isset($_GET["offset"]) && intval($_GET["offset"]) == 12) {
            			    	?>
									<a href="http://www.bizzona.ru/oborudovanie-dlya-parikmacherskich.php" class="city_a" style="font-size:10pt;" title="������������ ��� ��������������">������������ ��� ��������������</a>&nbsp;&nbsp;<a href="http://bizzona.ru/oborudovanie-dlya-restoranov.php" class="city_a" style="font-size:10pt;" title="������������ ��� ����������">������������ ��� ����������</a>&nbsp;&nbsp;<a href="http://www.bizzona.ru/oborudovanie-dlya-parikmacherskich.php" class="city_a" style="font-size:10pt;" title="������������ ��� ��������������">������������ ��� ��������������</a>&nbsp;&nbsp;<a href="http://www.bizzona.ru/oborudovanie-dlya-torgovli.php" class="city_a" style="font-size:10pt;" title="������������ ��� ��������">������������ ��� ��������</a>&nbsp;&nbsp;<a href="http://www.bizzona.ru/shinomontazhnoe-oborudovanie-seni.php" class="city_a" style="font-size:10pt;" title="������������� ������������ - ����">������������� ������������ - ����</a>
								<?
							} else {
								?>
									<a href="http://www.bizzona.ru/oborudovanie-dlya-proizvodstva.php" class="city_a" style="font-size:10pt;" title="������������ ��� ������������">������������ ��� ������������</a>&nbsp;&nbsp;<a href="http://www.bizzona.ru/torgovoe-oborudovanie.php" class="city_a" style="font-size:10pt;" title="�������� ������������">�������� ������������</a>&nbsp;&nbsp;<a href="http://www.bizzona.ru/cholodilnoe-oborudovanie.php" class="city_a" style="font-size:10pt;" title="����������� ������������">����������� ������������</a>&nbsp;&nbsp;<a href="http://bizzona.ru/oborudovanie-dlya-biznesa.php" class="city_a" style="font-size:10pt;" title="������������ ��� �������">������������ ��� �������</a>&nbsp;&nbsp;<a href="http://www.bizzona.ru/shinomontazhnoe-oborudovanie.php" class="city_a" style="font-size:10pt;" title="������������� ������������">������������� ������������</a>
								<?
							}
            			?>
            			
            			</td>
            			<td style="background-image:url(http://<?=$_SERVER["HTTP_HOST"]?>/images/d2.gif); background-repeat:no-repeat;width:5px;height:5px;background-position:right top;">&nbsp;</td>
        			</tr>
        			<tr>
            			<td style="background-image:url(http://<?=$_SERVER["HTTP_HOST"]?>/images/d4.gif); background-repeat:no-repeat;width:5px;height:5px; background-position:left bottom;">&nbsp;</td>
            			<td style="background-image:url(http://<?=$_SERVER["HTTP_HOST"]?>/images/d3.gif); background-repeat:no-repeat;width:5px;height:5px; background-position:right bottom;">&nbsp;</td>
        			</tr>
    			</table>
			</div>
			<?
				$smarty->display("./site/footer.tpl");
			?>
    	</td>
    </tr>
</table>

<?
  $end = get_formatted_microtime(); 
  $total = $end - $start;
  echo "<center><span style='font-size:7pt;'>".round($total, 6)." : ".$count_sql_call." : ".$base_memory_usage." : ".memoryUsage(memory_get_usage(), $base_memory_usage)." </span><center>";
?>
</body>
</html>
<?
	$sell->Close();
	$category->Close();
	$city->Close();
	$country->Close();
	$buyer->Close();
	$equipment->Close();
	$district->Close();
	$subcategory->Close();
	$streets->Close();
?>