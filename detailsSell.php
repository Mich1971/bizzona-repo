<?
	global $DATABASE;

	include_once("./phpset.inc");
	include_once("./engine/functions.inc"); 
	include_once("./engine/sell_functions.inc"); 
	require_once("./engine/Cache/Lite.php");

	$count_sql_call = 0;
	$start = get_formatted_microtime(); 
	$base_memory_usage = memory_get_usage();	

	AssignDataBaseSetting();
  
	//ini_set("display_startup_errors", "on");
	//ini_set("display_errors", "on");
	//ini_set("register_globals", "on");
 
	include_once("./engine/class.category_lite.inc"); 
	$category = new Category_Lite();

	include_once("./engine/class.country_lite.inc"); 
	$country = new Country_Lite();	

	include_once("./engine/class.city_lite.inc"); 
	$city = new City_Lite();

	include_once("./engine/class.sell_lite.inc"); 
	$sell = new Sell_Lite();	

	include_once("./engine/class.buy_lite.inc"); 
	$buyer = new Buyer_Lite();	

  	include_once("./engine/class.district_lite.inc"); 
  	$district = new District_Lite();	

  	include_once("./engine/class.subcategory_lite.inc");
  	$subcategory = new SubCategory_Lite();	

	include_once("./engine/class.geocode.inc"); 
	$geocode = new GeoCode();

  	include_once("./engine/class.streets_lite.inc");
  	$streets = new Streets_Lite();	
	
  	include_once("./engine/class.metro_lite.inc"); 
  	$metro = new Metro_Lite();	

  	include_once("./engine/class.equipment_lite.inc"); 
  	$equipment = new Equipment_Lite();  	
  	 
	include_once("./engine/class.optimization_region_category.inc");
	$optimizationRegionCategory = new OptimizationRegionCategory();
  
  	include_once("./engine/class.ad.inc");
  	$ad = new Ad();	
  	
  	include_once("./engine/class.company.inc"); 
  	$company = new Company();

        include_once("./engine/class.review.inc"); 
        $review = new Review();        
        
    include_once("./engine/class.frontend.inc");

    require_once("./libs/Smarty.class.php");
	$smarty = new Smarty;
	$smarty->template_dir = "./templates";
	$smarty->compile_dir  = "./templates_c";
	$smarty->cache_dir = "./cache";
	$smarty->compile_check = true;
	$smarty->debugging     = false;
	//$smarty->caching = true; 

	require_once("./regfuncsmarty.php");
	
	$smarty->assign("listcompany", $company->ListCompanyInList());	
	
	$assignDescription = AssignDescription();

	$sell = new Sell_Lite();
	$dataSell = Array();
	$dataSell["ID"] = intval($_GET["ID"]);
  
	//$resultGetItem = $sell->GetItem($dataSell);
	
	// up saving view sell
	$array_viewsell = array();
	
	if (isset($_SESSION["viewsell"])) {	
		$array_viewsell = unserialize($_SESSION["viewsell"]);
	} 
	
	$count_viewsell = sizeof($array_viewsell);
	if($count_viewsell > 200) {
		$array_viewsell = array_reverse($array_viewsell);		
		for($i=0; $i < $count_viewsell; $i++) {
			if($i>20) {
				unset($array_viewsell[$i]);
			}
		}
		$array_viewsell[] = array("Id" => $dataSell["ID"], "datetime" => mktime() );
	} else {
		$array_viewsell[] = array("Id" => $dataSell["ID"], "datetime" => mktime() );
	}
	$_SESSION["viewsell"] = serialize($array_viewsell);
	// dov saving view sell
	
	
  	// up caching call details sell
  	$options = array(
  		'cacheDir' => "sellcache/",
   		'lifeTime' => 86400
  	);
		
  	$cache = new Cache_Lite($options);
		
  	if ($data = $cache->get(''.$dataSell["ID"].'____sellitem')) {
		$resultGetItem = unserialize($data);
  	} else { 
		$resultGetItem = $sell->GetItem($dataSell);
		$cache->save(serialize($resultGetItem));
  	}
  	// donw caching call details sell	
	
	$smarty->assign("datasell", $resultGetItem);

  	$seoname = "";
  	 
  	if (isset($resultGetItem->ID) && $resultGetItem->StatusID != 17) {
    	$resultGetItem->ProfitPerMonthID = trim(strrev(chunk_split (strrev($resultGetItem->ProfitPerMonthID), 3,' '))); 
    	$resultGetItem->MaxProfitPerMonthID = trim(strrev(chunk_split (strrev($resultGetItem->MaxProfitPerMonthID), 3,' '))); 
    	$resultGetItem->SumDebtsID = trim(strrev(chunk_split (strrev($resultGetItem->SumDebtsID), 3,' '))); 
    	$resultGetItem->WageFundID = trim(strrev(chunk_split (strrev($resultGetItem->WageFundID), 3,' ')));
    	$resultGetItem->ArendaCostID = trim(strrev(chunk_split (strrev($resultGetItem->ArendaCostID), 3,' ')));

    	$_SESSION["uCityID"] = $resultGetItem->CityID; 
    	$_SESSION["uCategoryID"] = $resultGetItem->CategoryID; 
    
    	$seoname = $resultGetItem->seoname;
    	if(strlen($seoname) > 0) {
    		$seoname =  $seoname.", ";
    	}
    
    	$optimizationRegionCategory->regionId = ($resultGetItem->subCityID > 0 ? $resultGetItem->subCityID : $resultGetItem->CityID);
    	$optimizationRegionCategory->categoryId = $resultGetItem->SubCategoryID;
    	$optimizationRegionCategory->adId = 1;
    	$res_optimization_category = $optimizationRegionCategory->GetItem();
    	
    	if(@$res_optimization_category->useownurl == "1") {
    		if($resultGetItem->subCityID > 0) {
    			$res_optimization_category->url = "http://www.bizzona.ru/detailssearch.php?subCityId=".$resultGetItem->subCityID."&subCatId=".$resultGetItem->SubCategoryID."";
    		} else {
    			$res_optimization_category->url = "http://www.bizzona.ru/detailssearch.php?cityId=".$resultGetItem->CityID."&subCatId=".$resultGetItem->SubCategoryID."";
    		}
    	}
    	
    	$smarty->assign("optimization_category", $res_optimization_category);
  	} else {
  		header ("Location: http://www.bizzona.ru");
  	}
	
	if (isset($resultGetItem->ID)) {

		$cache->setLifeTime(86400);
  		if ($data = $cache->get(''.$resultGetItem->CategoryID.'-'.$resultGetItem->CityID.'____datahotbuyer')) {
			$res_datahotbuyer = unserialize($data);
  		} else { 
  			
			$datahotbuyer = array();
        	$datahotbuyer["offset"] = 0;
        	$datahotbuyer["rowCount"] = 3;
        	$datahotbuyer["sort"] = "datecreate";
        	$datahotbuyer["StatusID"] = "-1";
        	$datahotbuyer["typeBizID"] = $resultGetItem->CategoryID;
        	$datahotbuyer["regionID"] = $resultGetItem->CityID;
        	$res_datahotbuyer = $buyer->Select($datahotbuyer);  			
			$cache->save(serialize($res_datahotbuyer));
  		}		
		
        
		$cache->setLifeTime(3600);
  		if ($data = $cache->get(''.$resultGetItem->SubCategoryID.'-'.$resultGetItem->CityID.'-'.@$resultGetItem->SubCityID.'-'.$resultGetItem->brokerId.'____suggestedsell')) {
			$resultsell = unserialize($data);
  		} else { 
  			
			$datasuggsell = array();
			$datasuggsell["offset"] = 0;
			$datasuggsell["rowCount"] = 5;
			$datasuggsell["subCatId"] = $resultGetItem->SubCategoryID;
			$datasuggsell["cityId"] = $resultGetItem->CityID;
			$datasuggsell["subCityId"] = $resultGetItem->SubCityID;
			$datasuggsell["StatusID"] = "-1";
			$datasuggsell["sort"] = "DataCreate";
			$datasuggsell["brokerId"] = $resultGetItem->brokerId;
			$resultsell = $sell->DetailsSelect($datasuggsell); 			
			$cache->save(serialize($resultsell));
  		}		
		
		
		$BizFormID =  NameBizFormByID($resultGetItem->BizFormID);
        $ReasonSellBizID = NameReasonSellBizByID($resultGetItem->ReasonSellBizID);
        $smarty->assign("BizFormID", $BizFormID); 
        $smarty->assign("ReasonSellBizID", $ReasonSellBizID);		
		
	}  	
  	
	$geocode->serviceId = intval($_GET["ID"]);
	$geocode->typeServiceId = 1;
	
	$objgeo = $geocode->GetData();
	
	if(intval($resultGetItem->latitude) > 0) {
		$objgeo->latitude = $resultGetItem->latitude;	
	}
	
	if(intval($resultGetItem->longitude) > 0) {
		$objgeo->longitude = $resultGetItem->longitude;	
	}
	
	
	
  	if(time() - $resultGetItem->DateBySecondCreate > 1209600) {
  		$cache->setLifeTime(86400);		
  	} else {
  		$cache->setLifeTime(300);		
  	}
  	
	if ($data = $cache->get('countsellview_'.$dataSell["ID"])) {
		$sellcountview = $data;
	} else {
  		$sell->ID = $dataSell["ID"];
    	$sellcountview = $sell->GetCountView();
		$cache->save($sellcountview);		
	}
			
	$ad->InitAd($smarty, $_GET);   		
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
	<HEAD>
  	  <?
  	     if(strlen($resultGetItem->subtitle) > 0) {
  	     	$smarty->assign("headertitle", $resultGetItem->subtitle);
			?>
				<title><?=$resultGetItem->subtitle;?> ��� "������-����"</title>
				<META NAME="Description" CONTENT="<?=$resultGetItem->subtitle;?> ��� "������-����">
			<?  	     	
  	     } else {
  	     	?>
  	     		<title>������� <?=$resultGetItem->NameBizID;?>. ������ <?=$resultGetItem->NameBizID;?>, ����� <?=$resultGetItem->NameBizID;?>.</title>
  	     		<META NAME="Description" CONTENT="������ <?=$resultGetItem->NameBizID;?> , ����� <?=$resultGetItem->NameBizID;?>">
  	     	<?
  	     }
      ?>	
        <meta name="Keywords" content="������ <?=$resultGetItem->NameBizID;?> , ����� <?=$resultGetItem->NameBizID;?>"> 
        
		<script src="http://www.bizzona.ru/js/jquery.min.js"></script>
		<script src="http://www.bizzona.ru/js/jquery.colorbox.js"></script> 
		<link media="screen" rel="stylesheet" href="http://www.bizzona.ru/css/colorbox.css" />        
        
        <LINK href="http://www.bizzona.ru/general.css" type="text/css" rel="stylesheet">
        <meta http-equiv="content-type" content="text/html; charset=windows-1251"/>
		<link rel="shortcut icon" href="http://www.bizzona.ru/images/favicon.ico">
		
		<script>
		$(document).ready(function(){
			$("a[rel='sellemail']").colorbox({width:"60%", height:"80%", iframe:true});
			$("a[rel='sellphone']").colorbox({width:"60%", height:"80%", iframe:true});
			$("a[rel='sellprice']").colorbox({width:"60%", height:"80%", iframe:true});
			$("#click").click(function(){ 
				$('#click').css({"background-color":"#f00", "color":"#fff", "cursor":"inherit"}).text("");
				return false;
			});
		});
		</script>		
		
	</HEAD>
<?
	$smarty->display("./site/headerbanner.tpl");
?>
<?  
    $useMapGoogle = ( ($_SERVER["SERVER_NAME"] == "www.bizzona.ru" or $_SERVER["SERVER_NAME"] == "bizzona.ru")  ? true : false); 
    $useMapGoogle = (($resultGetItem->StatusID == 5 or $resultGetItem->StatusID == 12 or $resultGetItem->StatusID == 10) ? false : $useMapGoogle);
    
	if(isset($objgeo->status) && $objgeo->status == G_GEO_SUCCESS && $useMapGoogle ) {
		
		if($objgeo->accuracy >= 1) {
			$zoom = $objgeo->zoom;
			
?>
    			<script charset="utf-8" src="http://maps.google.com/maps?file=api&amp;v=2&amp;hl=ru&key=ABQIAAAAnavXIsM5jdni-p-JoKqdIxTPIkIa5BjXbMz0WvIzZffrVGiazxTRu-mHw0IpP9Ib5fP2129LbcNtTw"
      				type="text/javascript"></script>
    			<script type="text/javascript">
    			//<![CDATA[
    				function load() {
      					if (GBrowserIsCompatible()) {
        					var map = new GMap2(document.getElementById("map"));
        					//map.enableScrollWheelZoom(); 
        					map.setCenter(new GLatLng(<?=$objgeo->longitude;?>,<?=$objgeo->latitude;?>), <?=$zoom;?>);
        					map.addControl(new GMapTypeControl());
        					var bounds = map.getBounds();
          					var point = new GLatLng(<?=$objgeo->longitude;?>, <?=$objgeo->latitude;?>);
          					map.addOverlay(new GMarker(point));
        					map.addControl(new GSmallMapControl());
        					GEvent.addListener(map, "click", function(overlay,latlng) {
          						var lat = latlng.lat();
          						var lon = latlng.lng();
          						var latOffset = 0.01;
          						var lonOffset = 0.01;
       	  						var polygon = new GPolygon([
            					new GLatLng(lat, lon - lonOffset),
            					new GLatLng(lat + latOffset, lon),
            					new GLatLng(lat, lon + lonOffset),
            					new GLatLng(lat - latOffset, lon),
            					new GLatLng(lat, lon - lonOffset)
		  						], "#f33f00", 5, 1, "#ff0000", 0.2);
		  						map.addOverlay(polygon);
        					});
          					
          					
      					}
    				}
    			//]]>
    			</script>  
    			<body onload="load()" onunload="GUnload()">
<?
			$smarty->assign("googlemap", 1);
		}
	}
?> 

<?
	$smarty->caching = false;
	//$smarty->assign("banner4_file", "bizzona_blog.swf"/*GetBanner3()*/);        
	$smarty->assign("banner4_file", "biztrio260x200-2.swf");        
    $output_banner4 = $smarty->fetch("./site/banner4.tpl");
	$smarty->assign("output_banner4", minimizer($output_banner4));
	$smarty->caching = true;
?>


<table class="w" border="0" cellpadding="0" cellspacing="0" height="100%"  bgcolor="#eeeee0">
	<tr>
		<td colspan="2">
		<?
			$topmenu = GetMenu();
			$smarty->assign("topmenu", $topmenu);

			$link_country = GetSubMenu();	
			$smarty->assign("ad", $link_country);
	
			$smarty->caching = false;	
			$output_header = $smarty->fetch("./site/header.tpl");
			echo minimizer($output_header);
		?>
		</td>
	</tr>
	<tr>
		<td colspan="2" style="padding:2pt;">
			<?
				echo UpPositionAndShare_1(intval($_GET["ID"]));
			?>
		</td>
	</tr>
	<tr>
		<td colspan="2" bgcolor="#006699" id="menu_top" align="right" style="padding:3pt;">
			<?
				$output_linkcatdetsell = $smarty->fetch("./site/linkcatdetsell.tpl");
				echo minimizer($output_linkcatdetsell);
			?>
		</td>
	</tr>
    <tr>
		<td width="65%" valign="top" style="padding:2pt;">
			<?		
				echo SellHeaderShow_1($dataSell["ID"], $resultGetItem->DataCreateTxt, $sellcountview);
       		?>		
			<div style='font-family:arial;font-size:11pt;border-top: 1px; border-bottom: 1px; border-left:1px;  border-right:1px;  border-color:#C1C1A4;  border-style: solid;padding:5px;background-color:#ffffff;' >
			
				<?
					$output_detalssellleft = $smarty->fetch("./site/detalssellleft.tpl");
					echo minimizer($output_detalssellleft);
				?>

       			<div class="w" style="padding-top:10pt; padding-bottom: 5pt;height:20pt; padding-top:0pt; padding-bottom:0pt;width:auto;" >
       				<table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#eeeee0" style="height:100%;">
            			<tr>
							<td valign="middle" align="left" style="padding-left:3pt;vertical-align:middle; background-image:url(http://www.bizzona.ru/images/disc.gif); background-repeat:no-repeat; background-position:center; background-position:left; padding-left:15pt;"><span onclick='window.open("http://www.bizzona.ru/messagesell/<?=$resultGetItem->ID;?>","","width=550,height=500,scrollbars=auto,resizable=yes")' class="type_a" style="text-decoration:underline;cursor:hand;cursor: pointer;"  target='_blank'>����������</span></td>
							<td valign="middle" align="left" style="padding-left:3pt;vertical-align:middle; background-image:url(http://www.bizzona.ru/images/print.gif); background-repeat:no-repeat; background-position:center; background-position:left; padding-left:15pt;"><a href="http://www.bizzona.ru/print/<?=$resultGetItem->ID;?>" class="type_a" style="font-weight:normal;" target="_blank">������ ��� ������</a></td>
                			<td width="50%" valign="middle" align="right" style="padding-right:3pt; padding-left:3pt; font-size:12pt;font-family:Arial;color:White;font-weight:bold; vertical-align:middle; padding-left:15pt;">
							<div style='display:block' id='SendBlockID'>
								<input type='hidden' id='idsell' value='<?=$dataSell["ID"];?>'>
								<div style="background-image:url(http://www.bizzona.ru/images/email.gif); display:inline; background-repeat:no-repeat; background-position:center; background-position:left; padding-right:15pt;">&nbsp;</div>&nbsp;<input type="text"  id='email' name='email' value="�������� �� email" style="font-size:9pt; font-family:Arial;" onClick='this.value="";'  onfocusout="Restore(this);" /><input type="button" value="&nbsp;Ok&nbsp;"  style="font-size:9pt; font-family:Arial;"  onclick='send();' />
							</div>
                			</td>
                		</tr>
            		</table>
       			</div>

				<?
					include_once("./yadirect.php");
				?>      			
				
				<?
				if (isset($resultGetItem->ID)) {
					$sell->SubCategoryID = $resultGetItem->SubCategoryID;
					
					$smarty->assign("catId", $resultGetItem->CategoryID);
					$smarty->assign("subCatId", $resultGetItem->SubCategoryID);
					
           			$res_ListCityForSearch = $sell->ListCityForSearch();
           			$res_ListRegionForSearch = $sell->ListRegionForSearch();
           			while (list($k, $v) = each($res_ListCityForSearch)) {
           				if($v->CityID == 77) {
           					$smarty->assign("show_moscow_link", "1");
           				}
           				if($v->CityID == 78) {
           					$smarty->assign("show_spb_link", "1");
           				}
           			}
           			reset($res_ListCityForSearch);
           			while (list($k, $v) = each($res_ListRegionForSearch)) {
           				if($v->CityID == 50) {
           					$smarty->assign("show_moscow_region_link", "1");
           					break;
           				}
           			}
           			reset($res_ListRegionForSearch);
           			$smarty->assign("ListCityForSearch", $res_ListCityForSearch);
           			$smarty->assign("ListRegionForSearch", $res_ListRegionForSearch);
            
            		$output_SelectByRegionSubRegion = $smarty->fetch("./site/SelectByRegionSubRegion_1.tpl", $sell->SubCategoryID);
					echo fminimizer($output_SelectByRegionSubRegion);					
				}
				
				?>

                            
                                <? if (IsShowBlockSeoWords()):  ?>
				<?
					$smarty->caching = true; 
					if (!$smarty->is_cached("./site/keysell.tpl")) {
                                            $datakeysell = $category->GetListSubCategoryActive();
                                            $smarty->assign("datakeysell", $datakeysell);
					}
					$output_keysell = $smarty->fetch("./site/keysell.tpl");
					echo minimizer($output_keysell);
					$smarty->caching = false; 
				?>
                                <? endif ?>    
                            
				<?
					ShowDetailsSearchForm("", false);
				?>
				<?
					if (isset($_SESSION["viewsell"])) {	
						
						$arr_viewsell = unserialize($_SESSION["viewsell"]);
						if(sizeof($arr_viewsell) > 0) {
							
							$arr_viewsell = array_reverse($arr_viewsell);
							//echo sizeof($arr_viewsell);
							$arr_ids = array();
							
							$iter = 0;			
									
							while (list($k, $v) = each($arr_viewsell)) {
								if($iter > 5) break;

								if(!in_array($v["Id"], $arr_ids)) {
									$arr_ids[] = $v["Id"];
									$iter += 1;
								}
								
							}
							
                    		$dataview["offset"] = 0;
                    		$dataview["rowCount"] = 5;
                    		$dataview["sort"] = "ID";
                    		$dataview["StatusID"] = "-1";
                    		//$dataview["noorder"] = 1;
							$dataview["listIds"] = implode(",", $arr_ids);
							
							$resultview = $sell->Select($dataview);	
							
							if(sizeof($resultview) > 0) {
								?>
									<div class="w" style="padding-top:10pt; padding-bottom: 5pt;height:20pt; padding-top:0pt; padding-bottom:0pt;width:auto;" >
										<table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#7C3535" style="height:100%;">
    										<tr>
        										<td valign="middle" align="left" style="padding-left:3pt; font-size:12pt;font-family:Arial;color:White;font-weight:bold; vertical-align:middle;">
        											��������� ������������� ����������� 
        										</td>
        									</tr>
    									</table>
									</div>
								<?
								$smarty->caching = false;
								$smarty->assign("style", "width:100%;padding-left:0pt;");
                  				$smarty->assign("data", $resultview);
                                $smarty->assign('colorsell', FrontEnd::ListColorSet());
                  				$smarty->display("./site/listsell.tpl");
                  				$smarty->caching = false;
                  				
                  				//echo session_id();
							}
						}
						
					}
				?>

			</div>		
			
			
			
        </td>
        <td width="35%" valign="top" style="padding:2pt;">
        	<?
        		$smarty->caching = false; 
				$output_servicecompany = $smarty->fetch("./site/servicecompany.tpl");
				echo minimizer($output_servicecompany);        	
				$smarty->caching = false;        	
			?>
			<?		
				echo SellMessageForBuyer($resultGetItem->brokerId);
       		?>		

       		<?
       			if(in_array($resultGetItem->SubCategoryID, array(13, 14, 153, 158, 160, 161, 159, 162, 190, 300, 193, 155, 197, 384, 187, 298, 303, 200, 194, 301, 302, 380, 297, 189, 299, 192, 204, 367, 199, 198, 201, 195, 203, 188, 202, 377, 369, 191, 405, 365, 366, 368, 8, 9, 10, 252, 210, 209, 12, 166, 163, 11, 253, 211, 255, 165, 256, 250, 378, 385, 251, 254, 248, 249, 404, 22, 41, 183, 69, 42, 71, 391, 81, 80, 18, 19, 219, 374, 375, 220, 339, 20, 223, 222, 246, 226, 15, 17, 16))) { 
       		?>
       		
			<div style='font-family:arial;font-size:11pt;border-top: 1px; border-bottom: 1px; border-left:1px;  border-right:1px;  border-color:#C1C1A4;  border-style: solid;padding:5px;background-color:#fbfafa;margin-bottom:2pt;' >
        	<?
        		$smarty->caching = false; 
				$output_searchtoppatameters = $smarty->fetch("./site/searchtoppatameters.tpl");
				echo minimizer($output_searchtoppatameters);        	
				$smarty->caching = false; 
			?>
			</div>
       		
			<?
       			}
			?>
       		
        	<div style='font-family:arial;font-size:11pt;border-top: 1px; border-bottom: 1px; border-left:1px;  border-right:1px;  border-color:#C1C1A4;  border-style: solid;padding:5px;background-color:#fbfafa;' >
        		<?

				$output_detalssellright = $smarty->fetch("./site/detailssellright.tpl");
				echo minimizer($output_detalssellright);
				
        		
        		
                $smarty->assign("data", $resultsell);
                if(isset($resultsell) && sizeof($resultsell) > 0) {
					$output_sugnavsearch = $smarty->fetch("./site/sugnavsearch_1.tpl");
					echo fminimizer($output_sugnavsearch);
                }
                    
				$saveme = $smarty->fetch("./site/saveme.tpl");
				echo $saveme;

				$output_ilistsell = $smarty->fetch("./site/ilistsell_1.tpl", $_SERVER["REQUEST_URI"]);
				echo fminimizer($output_ilistsell);
                    
				
                if(isset($resultsell) && sizeof($resultsell) > 0) {
					$output_sugnavsearch = $smarty->fetch("./site/sugnavsearch_1.tpl");
					echo fminimizer($output_sugnavsearch);                    	
                }				
                

                
                
				?>
        	</div>

                        <? if (IsShowBlockSeoWords()):  ?>
			<? 
                            DetailsSuggestSubcategory($resultGetItem->SubCategoryID);  
			?> 
                        <? endif ?>
        	
			<?
				if(isset($res_datahotbuyer) && sizeof($res_datahotbuyer) > 0) {
			?>
				<div style='font-family:arial;font-size:11pt;border-top: 1px; border-bottom: 1px; border-left:1px;  border-right:1px;  border-color:#C1C1A4;  border-style: solid;padding:5px;background-color:#fbfafa;' >        	
					<?
  						$smarty->assign("data", $res_datahotbuyer);        	
  						
                     	$output_sugnavsearchb = $smarty->fetch("./site/sugnavsearchb_1.tpl");
		             	echo fminimizer($output_sugnavsearchb);
  						
                     	$output_ilistsellb = $smarty->fetch("./site/ilistsellb_1.tpl", $_SERVER["REQUEST_URI"]);
		             	echo fminimizer($output_ilistsellb);
          				
                     	$output_sugnavsearchb = $smarty->fetch("./site/sugnavsearchb_1.tpl");
		             	echo fminimizer($output_sugnavsearchb);
					?>
				</div>
			<?
				}
			?>
				<br>
				<?
					if($resultGetItem->SubCategoryID > 0) {

						?>
							<div style='font-family:arial;font-size:11pt;border-top: 1px; border-bottom: 1px; border-left:1px;  border-right:1px;  border-color:#C1C1A4;  border-style: solid;padding:5px;background-color:#fbfafa;' >        							
						<?
						
						include_once("./engine/class.subcategory_article.inc"); 
						$subcategory_article = new subcategory_article();	
				
                		$smarty->caching = true; 
                		$smarty->cache_dir = "./persistentcache";
                		$smarty->cache_lifetime = 86400*30;

                		if (!$smarty->is_cached("./site/subcategory_articles.tpl", $resultGetItem->SubCategoryID)) {
							$subcategory_article->subcategoryId = $resultGetItem->SubCategoryID; 
							$result_subcategory_article = $subcategory_article->Select();
							$smarty->assign("subcategory_article", $result_subcategory_article);
                		}
				
						$output_subcategory_articles = $smarty->fetch("./site/subcategory_articles.tpl", $resultGetItem->SubCategoryID);
						echo minimizer($output_subcategory_articles);			
				
						$smarty->caching = false; 
						
						?>
							<div>
							<br>
						<?
					}				
				?>			
				
			<div style='font-family:arial;font-size:11pt;border-top: 1px; border-bottom: 1px; border-left:1px;  border-right:1px;  border-color:#C1C1A4;  border-style: solid;padding:5px;background-color:#ffffff;' >        	
			<?
				if (sizeof($res_datahotbuyer) > 0) {	
			?>
                             
                                <? if (IsShowBlockSeoWords()):  ?>
                                    <table class="w" bgcolor="#e4e4e2">
                                            <tr>
                                                    <td>
                                                            <?
                                                                    $smarty->caching = true; 
                                                                    if (!$smarty->is_cached("./site/keyselllist.tpl")) {
                                                                            $datakeysell = $category->GetListSubCategoryActive();
                                                                            $smarty->assign("datakeysell", $datakeysell);
                                                                    }
                                                                    $smarty->assign("countdatakeysell", 18);
                                                                    $output_keysell = $smarty->fetch("./site/keyselllist.tpl");
                                                                    echo fminimizer($output_keysell);
                                                                    $smarty->caching = false; 					
                                                            ?>
                                                    </td>
                                            </tr>
                                    </table>
                                <? endif ?>
			<?
				} 
			?>
			<?
		    	$output_request = $smarty->fetch("./site/request.tpl");
		    	echo fminimizer($output_request);
			
		    	$output_proposal = $smarty->fetch("./site/proposal.tpl");
		    	echo fminimizer($output_proposal);
                        
                        echo $review->LastBlockReview();
			?>
			</div>

				
        </td>
    </tr>
    <tr>
    	<td colspan="2">
    		<?
					$smarty->caching = true; 
					$smarty->cache_lifetime = 10500;
				
					if (!$smarty->is_cached('./site/sugequipment.tpl', 'sellcatId='.$resultGetItem->CategoryID)) {
            
						$datasugequipment = array();
						$datasugequipment["offset"] = 0;
						$datasugequipment["rowCount"] = 4;
						$datasugequipment["sort"] = "datecreate";
						$datasugequipment["status_id"] = "-1";
						$datasugequipment["category_id"] = $resultGetItem->CategoryID;
            	 
						$res_datasugequipment = $equipment->SugSelect($datasugequipment);
						
						if(sizeof($res_datasugequipment) <= 2) {
							unset($datasugequipment["category_id"]);
							$res_datasugequipment = $equipment->SugSelect($datasugequipment);
						}
  	          	
						$smarty->assign("datasugequipment", $res_datasugequipment);        	
					}				
				
					$output_sugequipment = $smarty->fetch("./site/sugequipment.tpl",  'sellcatId='.$resultGetItem->CategoryID);
					echo fminimizer($output_sugequipment);
					$smarty->caching = false; 					
			?>
    	</td>
    </tr>
	<tr>
		<td colspan="2" bgcolor="#006699" id="menu_top" align="right" style="padding:3pt;">
			<?
				$output_linkcatdetsell = $smarty->fetch("./site/linkcatdetsell.tpl");
				echo minimizer($output_linkcatdetsell);
			?>
		</td>
	</tr>    
	<tr>
		<td colspan="2" style="padding:2pt;">
			<?
				echo UpPositionAndShare_1(intval($_GET["ID"]));
			?>
		</td>
	</tr>    
	
	<tr>
		<td colspan="2" bgcolor="White">
			<style>
 				td a {    color:black; }
			</style> 
			<div style="padding-top:4pt;padding-bottom:5pt;padding-right:10pt;padding-left:10pt;font-size:10pt;font-family:arial;">
			<table cellpadding="0" cellspacing="0" border="0"  style="width:100%;" bgcolor="#eeeee0">
    			<tr>
        			<td style="background-image:url(http://<?=$_SERVER["HTTP_HOST"]?>/images/d1.gif); background-repeat:no-repeat;width:5px;height:5px; background-position:left top;">&nbsp;</td>
            		<td rowspan="2" valign="middle" align="center"  style="font-size:8pt; font-family:Arial; color:black;">
						<h1 style='margin:0pt;padding:0pt;color:black;font-family:arial;font-size:8pt;text-align:center;margin-bottom:0pt;' >��� "������-����" - <?=$seoname;?> �������� ����� ����������� ������� �� �������.</h1> 
            		</td>
            		<td style="background-image:url(http://<?=$_SERVER["HTTP_HOST"]?>/images/d2.gif); background-repeat:no-repeat;width:5px;height:5px;background-position:right top;">&nbsp;</td>
        		</tr>
        		<tr>
            		<td style="background-image:url(http://<?=$_SERVER["HTTP_HOST"]?>/images/d4.gif); background-repeat:no-repeat;width:5px;height:5px; background-position:left bottom;">&nbsp;</td>
            		<td style="background-image:url(http://<?=$_SERVER["HTTP_HOST"]?>/images/d3.gif); background-repeat:no-repeat;width:5px;height:5px; background-position:right bottom;">&nbsp;</td>
        		</tr>
    		</table>
			</div>
			<?
				$output_adv = $smarty->fetch("./site/selladvice.tpl");
				echo fminimizer($output_adv);

				$output_mework = $smarty->fetch("./site/sell-howmework.tpl");
				echo fminimizer($output_mework);
			?>
			<?
				$smarty->display("./site/footer.tpl");
			?>
		</td>
	</tr>	
</table>
<?
	$end = get_formatted_microtime(); 
	$total = $end - $start;
	echo "<center><span style='font-size:7pt;'>".round($total, 6)." : ".$count_sql_call." : ".$base_memory_usage." : ".memoryUsage(memory_get_usage(), $base_memory_usage)." </span><center>";
?>
<script language='javascript'>
  function AddToSearch(obj) {
	var input_search = document.getElementById("searchtext");
	if(input_search != null) {
		if(obj.innerText != null) {
			input_search.value = obj.innerText;
		} else if (obj.innerHTML != null) {
			input_search.value = obj.innerHTML;
		}
	}
  }  
</script>
</body>
</html>
<?
	$dataSell["ID"] = $resultGetItem->ID;
    if(!isset($_GET["nocount"])) {
   		$sell->IncrementQView($dataSell);
    }

	$sell->Close();
	$category->Close();
	$city->Close();
	$country->Close();
	$buyer->Close();
	$district->Close();
	$subcategory->Close();
	$geocode->Close();
    $streets->Close();
    $optimizationRegionCategory->Close();
?>