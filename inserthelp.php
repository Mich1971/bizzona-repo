<?php
  include_once($_SERVER['DOCUMENT_ROOT']."/engine/typographus.php"); 
  include_once($_SERVER['DOCUMENT_ROOT']."/engine/CreateSubDirImg.php"); 
   
  function LoadClientImg($fileload, $count, $pref = 'sell_') {
   
    global $error;  
       
    $auto_create_path = CreateSubDirImg($_SERVER['DOCUMENT_ROOT'].'/simg/');  
      
    $max_image_width	= 380;
    $max_image_height	= 600;
    $max_image_size	= 3024 * 3024; 
    $valid_types 	=  array("gif","GIF","jpg", "png", "PNG", "jpeg","JPG");
  		 
	if (isset($_FILES[$fileload]) && $_FILES[$fileload]['error'] == UPLOAD_ERR_OK) {
		//$newname = time() + $count; 
		$newname = uniqid($pref);
   
		if (is_uploaded_file($_FILES[$fileload]['tmp_name'])) {
        	$filename = $_FILES[$fileload]['tmp_name'];
        	$ext = substr($_FILES[$fileload]['name'], 1 + strrpos($_FILES[$fileload]['name'], "."));
        	
        	if (filesize($filename) > $max_image_size) {
          		$error = '������: ������ ����� ������ 1 ��.';
          		$newname = ""; 
        	} elseif (!in_array($ext, $valid_types)) {
          		$error = '������: ���������� ����� ����������� ���������� ����� �� ����������� ���';
          		$newname = ""; 
        	} else {
          		if (1) {
                            if (move_uploaded_file($filename, $auto_create_path.'/'.$newname.".".$ext)) {
                                ;
                            } else {
                                $error =  '������ : ���������� ��������� ���������� ���������� �� ������� � bizzona.ru@gmail.com.';
                                $newname = ""; 
                            }
                        } else {
                            $error = '������: ���������� ��������� ������ �� �����. ���������� ������� ������ ����.';
                            $newname = "";
                        }
                    }
       } else {
         $newname = "";
       }
    }  	
  	
    return (strlen($newname) > 0 ? CreateSubDirImg().'/'.$newname.".".$ext : "");
  }
 
  function ReadingDataPostSell() {
      
        global $newname_ImgID, $newname_Img1ID, $newname_Img2ID, $newname_Img3ID, $newname_Img4ID;
      
        $costsymbols = array(".", " ", ",");
        $data["FML"]              = htmlspecialchars(trim($_POST["FML"]), ENT_QUOTES);
        $_POST["FML"] = $data["FML"];

        $data["EmailID"] = htmlspecialchars(trim(strtolower($_POST["EmailID"])), ENT_QUOTES);
        $_POST["EmailID"] = $data["EmailID"];        
        
        $data["periodId"] = htmlspecialchars(trim(strtolower($_POST["periodId"])), ENT_QUOTES);
        $_POST["periodId"] = $data["periodId"];

        $data["PhoneID"]          = htmlspecialchars(trim($_POST["PhoneID"]), ENT_QUOTES);
        $_POST["PhoneID"] = $data["PhoneID"];

        $data["NameBizID"]        = htmlspecialchars(trim($_POST["NameBizID"]), ENT_QUOTES);
        $_POST["NameBizID"] = $data["NameBizID"];

        $data["NameBizID"]        = FixName(ucfirst($data["NameBizID"]));

        $total_typeBizID = htmlspecialchars(trim($_POST["CategoryID"]), ENT_QUOTES);
        $arr_typeBizID = split("[:]",$total_typeBizID);
        if(sizeof($arr_typeBizID) == 2) {
            $data["CategoryID"] = intval($arr_typeBizID[0]);
            $data["SubCategoryID"] = intval($arr_typeBizID[1]);
            $_POST["SubCategoryID"] = $data["SubCategoryID"];
        } else {
            $data["CategoryID"] = "-1";
            $data["SubCategoryID"] = "-1";
        }

        $data["BizFormID"]        = htmlspecialchars(trim($_POST["BizFormID"]), ENT_QUOTES);
        $_POST["BizFormID"] = $data["BizFormID"];
        $data["CityID"]           = "-1";//htmlspecialchars(trim($_POST["CityID"]), ENT_QUOTES);

        $data["SiteID"]           = htmlspecialchars(trim($_POST["SiteID"]), ENT_QUOTES);
        $_POST["SiteID"] = $data["SiteID"];

        //$data["CostID"]           = htmlspecialchars(trim($_POST["CostID"]), ENT_QUOTES);
        $data["txtCostID"]           = htmlspecialchars(trim($_POST["CostID"]), ENT_QUOTES);
        $_POST["CostID"] = $data["txtCostID"];

        $data["CostID"]           = str_replace($costsymbols, "", htmlspecialchars(trim($_POST["CostID"]), ENT_QUOTES));
        $_POST["CostID"] = $data["CostID"];

        $data["cCostID"]          = htmlspecialchars(trim($_POST["cCostID"]), ENT_QUOTES);
        $_POST["cCostID"] = $data["cCostID"];

        //$data["ProfitPerMonthID"] = htmlspecialchars(trim($_POST["ProfitPerMonthID"]), ENT_QUOTES);
        $data["txtProfitPerMonthID"] = htmlspecialchars(trim($_POST["ProfitPerMonthID"]), ENT_QUOTES);
        $_POST["ProfitPerMonthID"] = $data["txtProfitPerMonthID"];
        $data["ProfitPerMonthID"] = str_replace($costsymbols, "", htmlspecialchars(trim($_POST["ProfitPerMonthID"]), ENT_QUOTES));      
        $_POST["ProfitPerMonthID"] = $data["ProfitPerMonthID"];
        $data["cProfitPerMonthID"] = htmlspecialchars(trim($_POST["cProfitPerMonthID"]), ENT_QUOTES);

        $data["TimeInvestID"]     = htmlspecialchars(trim($_POST["TimeInvestID"]), ENT_QUOTES);
        $_POST["TimeInvestID"] = $data["TimeInvestID"];
        $data["ListObjectID"]     = htmlspecialchars(trim($_POST["ListObjectID"]), ENT_QUOTES);
        $_POST["ListObjectID"] = $data["ListObjectID"];

        $data["ContactID"]        = htmlspecialchars(trim($_POST["ContactID"]), ENT_QUOTES);
        $_POST["ContactID"] = $data["ContactID"];

        $data["MatPropertyID"]    = htmlspecialchars(trim($_POST["MatPropertyID"]), ENT_QUOTES);
        $_POST["MatPropertyID"] = $data["MatPropertyID"];

        //$data["MonthAveTurnID"]   = htmlspecialchars(trim($_POST["MonthAveTurnID"]), ENT_QUOTES);
        $data["txtMonthAveTurnID"]   = htmlspecialchars(trim($_POST["MonthAveTurnID"]), ENT_QUOTES);
        $_POST["MonthAveTurnID"] = $data["txtMonthAveTurnID"];

        $data["MonthAveTurnID"]   = str_replace($costsymbols, "", htmlspecialchars(trim($_POST["MonthAveTurnID"]), ENT_QUOTES));
        $data["cMonthAveTurnID"]  = htmlspecialchars(trim($_POST["cMonthAveTurnID"]), ENT_QUOTES);

        //$data["MonthExpemseID"]   = htmlspecialchars(trim($_POST["MonthExpemseID"]), ENT_QUOTES);
        $data["txtMonthExpemseID"]   = htmlspecialchars(trim($_POST["MonthExpemseID"]), ENT_QUOTES);
        $_POST["MonthExpemseID"] = $data["txtMonthExpemseID"];

        $data["MonthExpemseID"]   = str_replace($costsymbols, "", htmlspecialchars(trim($_POST["MonthExpemseID"]), ENT_QUOTES));
        $_POST["MonthExpemseID"] = $data["MonthExpemseID"];
        $data["cMonthExpemseID"]  = htmlspecialchars(trim($_POST["cMonthExpemseID"]), ENT_QUOTES);
        $_POST["cMonthExpemseID"] = $data["cMonthExpemseID"];

        //$data["SumDebtsID"]       = htmlspecialchars(trim($_POST["SumDebtsID"]), ENT_QUOTES);
        $data["txtSumDebtsID"]       = htmlspecialchars(trim($_POST["SumDebtsID"]), ENT_QUOTES);
        $_POST["SumDebtsID"] = $data["txtSumDebtsID"];

        $data["SumDebtsID"]       = str_replace($costsymbols, "", htmlspecialchars(trim($_POST["SumDebtsID"]), ENT_QUOTES));
        $_POST["SumDebtsID"] = $data["SumDebtsID"];

        $data["cSumDebtsID"]      = htmlspecialchars(trim($_POST["cSumDebtsID"]), ENT_QUOTES);

        //$data["WageFundID"]       = htmlspecialchars(trim($_POST["WageFundID"]), ENT_QUOTES);
        $data["txtWageFundID"]       = htmlspecialchars(trim($_POST["WageFundID"]), ENT_QUOTES);
        $_POST["WageFundID"] = $data["txtWageFundID"];

        $data["WageFundID"]       = str_replace($costsymbols, "", htmlspecialchars(trim($_POST["WageFundID"]), ENT_QUOTES));
        $data["cWageFundID"]      = htmlspecialchars(trim($_POST["cWageFundID"]), ENT_QUOTES);

        $data["ArendaCostID"]       = str_replace($costsymbols, "", htmlspecialchars(trim($_POST["ArendaCostID"]), ENT_QUOTES));
        $data["cArendaCostID"]      = htmlspecialchars(trim($_POST["cArendaCostID"]), ENT_QUOTES);      

        $data["ObligationID"]     = htmlspecialchars(trim($_POST["ObligationID"]), ENT_QUOTES);
        $_POST["ObligationID"] = $data["ObligationID"];

        $data["CountEmplID"]      = htmlspecialchars(trim($_POST["CountEmplID"]), ENT_QUOTES);
        $_POST["CountEmplID"] = $data["CountEmplID"];

        $data["CountManEmplID"]   = htmlspecialchars(trim($_POST["CountManEmplID"]), ENT_QUOTES);
        $_POST["CountManEmplID"] = $data["CountManEmplID"];

        $data["TermBizID"]        = htmlspecialchars(trim($_POST["TermBizID"]), ENT_QUOTES);
        $_POST["TermBizID"] = $data["TermBizID"];

        if(isset($_SESSION["referer"])) {
            $data["source"]   = htmlspecialchars(trim($_SESSION["referer"]), ENT_QUOTES);
        } else {
            $data["source"] = "";
        }

        $data["ShortDetailsID"]   = htmlspecialchars(trim($_POST["ShortDetailsID"]), ENT_QUOTES);
        $_POST["ShortDetailsID"] = $data["ShortDetailsID"];

        $str_ShortDetailsID = $data["ShortDetailsID"];
        hyperlink(&$str_ShortDetailsID, 8);
        $data["ShortDetailsID"] = $str_ShortDetailsID;

        $data["DetailsID"] = htmlspecialchars(trim($_POST["DetailsID"]), ENT_QUOTES);

        $_POST["DetailsID"] = $data["DetailsID"];

        $str_details = $data["DetailsID"];
        hyperlink(&$str_details);
        $data["DetailsID"] = $str_details;

        $data["ImgID"]  = $newname_ImgID; 
        $data["Img1ID"] = $newname_Img1ID; 
        $data["Img2ID"] = $newname_Img2ID; 
        $data["Img3ID"] = $newname_Img3ID; 
        $data["Img4ID"] = $newname_Img4ID; 

        $data["FaxID"]            = htmlspecialchars(trim($_POST["FaxID"]), ENT_QUOTES);
        $_POST["FaxID"] = $data["FaxID"];

        $data["ShareSaleID"]      = htmlspecialchars(trim($_POST["ShareSaleID"]), ENT_QUOTES);
        $_POST["ShareSaleID"] = $data["ShareSaleID"];

        $data["ReasonSellBizID"]  = htmlspecialchars(trim($_POST["ReasonSellBizID"]), ENT_QUOTES);
        $_POST["ReasonSellBizID"] = $data["ReasonSellBizID"];

        $data["TarifID"]          = 3;//htmlspecialchars(trim($_POST["TarifID"]), ENT_QUOTES);
        $_POST["TarifID"] = $data["TarifID"];

        $data["youtubeURL"] = htmlspecialchars(trim($_POST["youtubeURL"]), ENT_QUOTES);
        $_POST["youtubeURL"] = $data["youtubeURL"];


        if(isset($_POST["newspaper"])) {
            $data["newspaper"]          = 1;
            $_POST["newspaper"] = 1;
        }	else {
            $data["newspaper"]          = 0;
            $_POST["newspaper"] = 0;
        }

        if(isset($_POST["helpbroker"])) {
            $data["helpbroker"]          = 1;
            $_POST["helpbroker"] = 1;
        }	else {
            $data["helpbroker"]          = 0;
            $_POST["helpbroker"] = 0;
        }

        $data["StatusID"]         = 0;    
        $data["ConstGUID"]        = md5(uniqid(rand(), true)); 
  
    	//up Typographus
    		
    	$typo = new Typographus();
    		
    	$data["NameBizID"] = $typo->process($data["NameBizID"]);
    	$data["SiteID"] = $typo->process($data["SiteID"]);
    	$data["ListObjectID"] = $typo->process($data["ListObjectID"]);
    	$data["MatPropertyID"] = $typo->process($data["MatPropertyID"]);
    	$data["ShortDetailsID"] = $typo->process($data["ShortDetailsID"]);
    	$data["DetailsID"] = $typo->process($data["DetailsID"]);
    		
    	//down Typographus		
        
        $data["CostID"] = floatval($data["CostID"]);

        if(isset($_SESSION["partnerid"])) {
            $data["partnerid"] =  intval($_SESSION["partnerid"]);
        } else {
            $data["partnerid"] = 0;
        }

        if(isset($data['periodId']) && $data['periodId'] == 'promo') {
            $data["isPromo"] = 1;
        } else {
            $data["isPromo"] = 0;
        }
        
        
        return $data;
  }
  
?>
