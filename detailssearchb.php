<?
  global $DATABASE;

  include_once("./phpset.inc");
  include_once("./engine/functions.inc"); 

  $count_sql_call = 0;
  $start = get_formatted_microtime();   
  $base_memory_usage = memory_get_usage();	

  AssignDataBaseSetting();
  
  include_once("./engine/class.category_lite.inc"); 
  $category = new Category_Lite();

  include_once("./engine/class.country_lite.inc"); 
  $country = new Country_Lite();

  include_once("./engine/class.city_lite.inc"); 
  $city = new City_Lite();

  include_once("./engine/class.sell_lite.inc"); 
  $sell = new Sell_Lite();

  include_once("./engine/class.buy_lite.inc"); 
  $buyer = new Buyer_Lite();

  include_once("./engine/class.district_lite.inc"); 
  $district = new District_Lite();

  include_once("./engine/class.subcategory_lite.inc");
  $subcategory = new SubCategory_Lite();

  include_once("./engine/class.streets_lite.inc");
  $streets = new Streets_Lite();

  require_once("./libs/Smarty.class.php");
  $smarty = new Smarty;
  $smarty->template_dir = "./templates";
  $smarty->compile_dir  = "./templates_c";
  //$smarty->cache_dir = "./cache";
  $smarty->compile_check = true;
  $smarty->debugging     = false;

  require_once("./regfuncsmarty.php");
  
  if(isset($_GET["ID"])) {
  	$_GET["ID"] = intval($_GET["ID"]);
  }
  
  $name_words = array("turagenstvo", "restoran", "parik", "krasota", "otel");
  if(isset($_GET["name"])) {
	if(in_array($_GET["name"], $name_words)) {
	} else {
		$_GET["name"] = "";
	}
  }

  if(isset($_GET["MaxCostID"])) {
  	$_GET["MaxCostID"] = intval($_GET["MaxCostID"]);
  }
  
  if(isset($_GET["MinCostID"])) {
  	$_GET["MinCostID"] = intval($_GET["MinCostID"]);
  }

	$action_words = array("typeBizID", "city", "price");
	if(isset($_GET["action"])) {
		if(in_array($_GET["action"], $action_words)) {
		
		} else {
			$_GET["action"] = "";
		}
	} 
  
  
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>

  <HEAD>

<?
	$buyer->districtID = (isset($_GET["districtId"]) ? intval($_GET["districtId"]) : 0);
	$buyer->regionID =  (isset($_GET["cityId"]) ? intval($_GET["cityId"]) : 0);
	$buyer->subTypeBizID = (isset($_GET["subCatId"]) ? intval($_GET["subCatId"]) : 0);

    $title = $buyer->GetTitleForUserFriendlySelect();
	
	$title = (strlen($title) > 0 ? $title : "����� ������� ������");
	
	$smarty->assign("title", $title);

	?>
		<title><?=$title;?></title>
	<?

?>

	<META NAME="Description" CONTENT="<?=$title;?>">
        <meta name="Keywords" content="<?=$title;?>"> 
        <LINK href="http://www.bizzona.ru/general.css" type="text/css" rel="stylesheet">
        <meta http-equiv="content-type" content="text/html; charset=windows-1251"/>
  </HEAD>

<body>

<?
  $smarty->display("./site/headerbanner.tpl");
?>


<?
	$link_country = $link_country = GetSubMenu();	
	$smarty->assign("ad", $link_country);
	echo minimizer($smarty->fetch("./site/detailssearchheader.tpl"));
?>

<table class="w" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td width="30%" valign="top">
         
        <?
        
          if(isset($_GET["subCatId"]) && intval($_GET["subCatId"]) > 0 && isset($_GET["cityId"]) && intval($_GET["cityId"]) > 0) {

          	$smarty->caching = false;
          	$sell->SubCategoryID = intval($_GET["subCatId"]);
          	$sell->CityID = intval($_GET["cityId"]);
          	$res_sel_friendly = $sell->SelectUserFriendlySellForBuyer();

          	$smarty->assign("dataquickdistrict", $res_sel_friendly);
  			echo fminimizer($smarty->fetch("./site/quicksearchbydistrict.tpl"));          	
          }        
        
          if (isset($_GET["action"]) && $_GET["action"]  == "category") {
            $_GET["ID"] = (isset($_GET["ID"])  ? intval($_GET["ID"]) : 0);
            $smarty->caching = true; 
            if (!$smarty->is_cached('./site/categorysb.tpl', $_GET["ID"])) {
              $data = array();
              $data["offset"] = 0;
              $data["rowCount"] = 0;
              $data["sort"] = "CountB";
              $result = $category->Select($data);
              $smarty->assign("data", $result);
            }
            
   			echo minimizer($smarty->fetch("./site/categorysb.tpl", $_GET["ID"]));
            
            $smarty->caching = false; 
          } else { 
            $smarty->caching = true; 
            if (!$smarty->is_cached('./site/categorysb.tpl')) {
              $data = array();
              $data["offset"] = 0;
              $data["rowCount"] = 0;
              $data["sort"] = "CountB";
              $result = $category->Select($data);
              $smarty->assign("data", $result);
            }
            //$smarty->display("./site/categorysb.tpl");
            
   			echo minimizer($smarty->fetch("./site/categorysb.tpl"));
            
            $smarty->caching = false; 
          }
        ?>
            
            <?
            
            	$datahotsell = array();
            	$datahotsell["offset"] = 0;
            	$datahotsell["rowCount"] = 4;
            	$datahotsell["sort"] = "DataCreate";
            	$datahotsell["StatusID"] = "-1";

                if (isset($_GET["action"])) {
                   switch(trim($_GET["action"])) {
	                   case "typeBizID": {
    	                 $datahotsell["CategoryID"] = intval($_GET["ID"]);
                       } break;
                       case "city": {
                         $datahotsell["CityID"] = intval($_GET["ID"]);
                       } break;                       
        	           default: {
                         		
                       }
                       break;
                   }            	
                }		
            	
   				echo minimizer($smarty->fetch("./site/call.tpl"));
                
                
  	          	$res_datahotsell = $sell->Select($datahotsell);
  	          	
  	          	if(sizeof($res_datahotsell) > 0)
  	          	{
    				$smarty->assign("datasell", $res_datahotsell);        	
   					echo minimizer($smarty->fetch("./site/iblocksell.tpl"));
            		
  	          	}
			?>
        
            <?
				echo minimizer($smarty->fetch("./site/request.tpl"));
            ?>  
        
        </td>
        <td width="70%" valign="top">

             <?
				if(isset($_GET["cityId"]) && intval($_GET["cityId"]) > 0 && isset($_GET["subCatId"]) && intval($_GET["subCatId"]) > 0 ) {
				
					$buyer->regionID = intval($_GET["cityId"]);
					$buyer->subTypeBizID = intval($_GET["subCatId"]);
					$res_FriendlySellByDistrict = $buyer->SelectUserFriendlySellByDistrict();
					if(sizeof($res_FriendlySellByDistrict) > 0) {
						$smarty->assign("dataFriendlySellByDistrict", $res_FriendlySellByDistrict);
						echo fminimizer($smarty->fetch("./site/quickseacrhbysubcategory.tpl"));
					}
				}
        	
        	?>
        
        
       			<?
					ShowDetailsSearchBuyerForm();	
					$output_navsearchb = $smarty->fetch("./site/navsearchb.tpl");
					echo minimizer($output_navsearchb);
            	?>
            
               	<?
               	
                    $data = array();
                    $dataPage = array(); 
                    $pagesplit = GetPageSplit();

                    if (isset($_GET["offset"])) {
                      $data["offset"] = intval($_GET["offset"]);
                    } else {
                      $data["offset"] = 0;
                    }
                    if (isset($_GET["rowCount"])) { 
                      $data["rowCount"] = intval($_GET["rowCount"]);
                    } else {
                      $data["rowCount"] = $pagesplit;
                    }


                    $data["StatusID"] = "-1";
                    $data["sort"] = "datecreate";

                    $data["cityId"] = intval($_GET["cityId"]);
                    $data["subCityId"] = intval($_GET["subCityId"]);
                    $data["catId"] = intval($_GET["catId"]);
                    $data["subCatId"] = intval($_GET["subCatId"]);
                    $data["districtId"] = intval($_GET["districtId"]);
                    
                    $_GET["cityId"] = $data["cityId"];
                    $_GET["subCityId"] = $data["subCityId"];
                    $_GET["catId"] = $data["catId"];
                    $_GET["subCatId"] = $data["subCatId"];                    
                    $_GET["districtId"] = $data["districtId"];
                    
                    $dataPage["offset"] = 0;
                    $dataPage["rowCount"] = 0;
                    $dataPage["cityId"] = $data["cityId"];
                    $dataPage["subCityId"] = $data["subCityId"];
                    $dataPage["catId"] = $data["catId"];
                    $dataPage["subCatId"] = $data["subCatId"];
                    $dataPage["districtId"] = $data["districtId"];
                    
                    $resultPage = $buyer->DetailsCountSelect($dataPage);
                    $smarty->assign("CountRecord", $resultPage);
                    $smarty->assign("CountSplit", $pagesplit);
                    $smarty->assign("CountPage", ceil($resultPage/5));
                    
                    $result = $buyer->DetailsSelect($data);
                    $smarty->assign("data", $result);

                  
				    $output_pagesplitb = $smarty->fetch("./site/pagesplitdetailsb.tpl", $_SERVER["REQUEST_URI"]);
				    echo minimizer($output_pagesplitb);
                  
                  	$smarty->display("./site/ilistsellb.tpl", $_SERVER["REQUEST_URI"]);
                  
				  	echo $output_ilistsellb;

               ?>

               
            	<div class="lftpadding" style="width: auto;">
       				<div  style="background-image:url(<?=$_SERVER["HTTP_HOST"]?>images/bl.gif); background-repeat:repeat-x;">&nbsp;</div>
            	</div>
               
            	<?
				    echo minimizer($output_pagesplitb);
            	?> 

            	<?
				    echo minimizer($smarty->fetch("./site/navsearchb.tpl"));
            	?>
         
            	
				<div class="lftpadding_cnt" style="padding-top:10pt; padding-bottom: 5pt;padding-top:0pt; padding-bottom:0pt; width:auto;" >
				<?
				  	  	
			    $smarty->caching = true; 
            	if (!$smarty->is_cached("./site/keybuy.tpl")) {
					$datakeysell = $category->GetListSubCategoryActiveBuy();
					$smarty->assign("datakeysell", $datakeysell);
            	}
				
                echo minimizer($smarty->fetch("./site/keybuy.tpl"));
                
                $smarty->caching = false; 
            	?>
           		</div>
            	
				<?
				  if (isset($_GET["action"])) {
    				switch(trim($_GET["action"])) {
						case "city": {
							$parentID = (isset($_GET["ID"])  ? intval($_GET["ID"]) : 0);
							$seocity = $city->GetSeoParent($parentID);
							$dataseocity = $city->GetRegionbyParentForBuyer($parentID);
							if(strlen(trim($seocity->seosell)) > 0 && sizeof($dataseocity) > 1) {
								$smarty->assign("seocity", $seocity);
								$smarty->assign("dataseocity", $dataseocity);
								$smarty->display("./site/hsubregionbuyer.tpl");
							}
						}	break;
    				}
				  }
				?>            		
            
        </td>
    </tr>
</table>

<?
		echo fminimizer($smarty->fetch("./site/adv.tpl"));
?>

<style>
 td a 
 {
   color:black;
 }
</style>


<div style="padding-top:4pt;padding-bottom:5pt;padding-right:10pt;padding-left:10pt;font-size:10pt;font-family:arial;">
	<table cellpadding="0" cellspacing="0" border="0"  style="width:100%;padding-bottom:2pt;" bgcolor="#eeeee0">
    	<tr>
        	<td style="background-image:url(http://<?=$_SERVER["HTTP_HOST"]?>/images/d1.gif); background-repeat:no-repeat;width:5px;height:5px; background-position:left top;">&nbsp;</td>
            <td rowspan="2" valign="middle" align="center"  style="font-size:8pt; font-family:Arial; color:black;">
			<?
			?>
            </td>
            <td style="background-image:url(http://<?=$_SERVER["HTTP_HOST"]?>/images/d2.gif); background-repeat:no-repeat;width:5px;height:5px;background-position:right top;">&nbsp;</td>
        </tr>
        <tr>
            <td style="background-image:url(http://<?=$_SERVER["HTTP_HOST"]?>/images/d4.gif); background-repeat:no-repeat;width:5px;height:5px; background-position:left bottom;">&nbsp;</td>
            <td style="background-image:url(http://<?=$_SERVER["HTTP_HOST"]?>/images/d3.gif); background-repeat:no-repeat;width:5px;height:5px; background-position:right bottom;">&nbsp;</td>
        </tr>
    </table>
</div>
<?
	$smarty->display("./site/footer.tpl");
?>

<?
  $end = get_formatted_microtime(); 
  $total = $end - $start;
  echo "<center><span style='font-size:7pt;'>".round($total, 6)." : ".$count_sql_call." : ".$base_memory_usage." : ".memoryUsage(memory_get_usage(), $base_memory_usage)." </span><center>";
?>

</body>
</html>
<?
	$sell->Close();
	$category->Close();
	$city->Close();
	$country->Close();
	$buyer->Close();
	$district->Close();
	$subcategory->Close();
	$streets->Close();
?>