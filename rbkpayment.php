<?
	//ini_set("display_startup_errors", "on");
	//ini_set("display_errors", "on");
	//ini_set("register_globals", "on");



	define("PAYMENT_S", 1); // ������ ������� �������
	define("PAYMENT_S_UP", 2); // ������ �������� ������� ������� �������

	define("PAYMENT_B", 3); // ������ - ������� �������
	define("PAYMENT_E", 4); // ������ - ������������
	define("PAYMENT_I", 5); // ������ - ����������
	define("PAYMENT_C", 6); // ������ - ���������� ������
	define("PAYMENT_R", 7); // ������ - ���������
	
	define("PAYMENT_S_WALLET", 10); // ������ - ���������� ������� �������� � �������� ���������� �� ������� �������	
	
  	global $DATABASE;

  	include_once("./phpset.inc");
  	include_once("./engine/functions.inc");

  	AssignDataBaseSetting();
  	
  	ini_set("display_startup_errors", "off");
  	ini_set("display_errors", "off");
  	ini_set("register_globals", "on");

  	include_once("./engine/class.sell.inc"); 
  	$sell = new Sell();
  	
  	include_once("./engine/class.buy.inc");
  	$buy = new Buyer();
	
  	include_once("./engine/class.equipment.inc"); 
  	$equipment = new Equipment();  	
  	
  	include_once("./engine/class.Room.inc"); 
  	$room = new Room();  

  	include_once("./engine/class.investments.inc"); 
  	$investments = new Investments();
  	
  	include_once("./engine/class.coopbiz.inc"); 
  	$coopbiz = new CoopBiz();

  	include_once("./engine/class.ControllerWallet.inc"); 
  	$wallet = new ControllerWallet();
  	
  	
  	include_once("./engine/class.templateevent.inc"); 
  	$templateEvent = new TemplateEvent();
  	
  	$_POST = array_merge_recursive($_POST, $_GET);
  	
	$eshopId = $_POST['eshopId'];
	$paymentId = $_POST['paymentId'];
	$orderId = $_POST['orderId'];
	$serviceName = $_POST['serviceName'];
	$recipientAmount = $_POST['recipientAmount'];
	$recipientCurrency = $_POST['recipientCurrency'];
	$paymentStatus = $_POST['paymentStatus'];
	$userName = $_POST['userName'];
	$userEmail = $_POST['userEmail'];
	$paymentData = $_POST['paymentData'];
	$secretKey = $_POST['secretKey'];
	$hash = $_POST['hash'];
	$userField_1 = $_POST['userField_1'];
	$userField_2 = $_POST['userField_2'];

	$merchantPaymentAmount = $_POST['merchantPaymentAmount'];
	
	//$str = "";
	
	//while (list($k, $v) = each($_POST)) {
	//	$str .= "$k => $v    .....  ";
	//}
	
	
	//mail("eugenekurilov@gmail.com", "rbkmoney", $str);
	
	
	$aOrderId = explode('-', $orderId);
	if(sizeof($aOrderId) == 2) {

		if($aOrderId[0] == PAYMENT_S_WALLET && $paymentStatus == 5) {
			$wallet->_codeId = intval($aOrderId[1]);
			$wallet->_summ = intval($merchantPaymentAmount);
			$wallet->_typeId = 'sell';		
			$wallet->_typePaymentId = 'rbcmoney';
			$wallet->_externPaymetCodeId = $paymentId;
			
			$wallet->AddBalance();
			
			$templateEvent->_sumpayment = $wallet->_summ;
			$templateEvent->EmailEvent(6,1, intval($aOrderId[1]));
		}
		
		if(($aOrderId[0] == PAYMENT_S or $aOrderId[0] == PAYMENT_S_UP) && $paymentStatus == 5) {
			$result = $sell->SetPayment(intval($aOrderId[1]), 1, intval($merchantPaymentAmount), '', $userName);
			
			$templateEvent->EmailEvent(2,1, intval($aOrderId[1]));
			
		}
		
		if($aOrderId[0] == PAYMENT_B  && $paymentStatus == 5) {
			$result = $buy->SetPayment(intval($aOrderId[1]), 1, intval($merchantPaymentAmount), '', $userName);
			
			$templateEvent->EmailEvent(2,2, intval($aOrderId[1]));
		}		
		
		if($aOrderId[0] == PAYMENT_E  && $paymentStatus == 5) {
			$result = $equipment->SetPayment(intval($aOrderId[1]), 1, intval($merchantPaymentAmount), '', $userName);
			
			$templateEvent->EmailEvent(2,4, intval($aOrderId[1]));
		}

		if($aOrderId[0] == PAYMENT_R  && $paymentStatus == 5) {
			$result = $room->SetPayment(intval($aOrderId[1]), 1, intval($merchantPaymentAmount), '', $userName);
			
			$templateEvent->EmailEvent(2,6, intval($aOrderId[1]));
		}
		
		if($aOrderId[0] == PAYMENT_I  && $paymentStatus == 5) {
			$result = $investments->SetPayment(intval($aOrderId[1]), 1, intval($merchantPaymentAmount), '', $userName);
			
			$templateEvent->EmailEvent(2,3, intval($aOrderId[1]));
		}		
		
		if($aOrderId[0] == PAYMENT_C  && $paymentStatus == 5) {
			$result = $coopbiz->SetPayment(intval($aOrderId[1]), 1, intval($merchantPaymentAmount), '', $userName);
			
			$templateEvent->EmailEvent(2,5, intval($aOrderId[1]));
		}		
		
	}
	
	echo 'ok';
?>