<?
  global $DATABASE;

  include_once("./phpset.inc");
  include_once("./engine/functions.inc"); 

  $count_sql_call = 0;
  $start = get_formatted_microtime(); 
  $base_memory_usage = memory_get_usage();	
 
  AssignDataBaseSetting();
  
  include_once("./engine/class.category_lite.inc"); 
  $category = new Category_Lite();

  include_once("./engine/class.country_lite.inc"); 
  $country = new Country_Lite();

  include_once("./engine/class.city_lite.inc");  
  $city = new City_Lite();

  include_once("./engine/class.sell_lite.inc"); 
  $sell = new Sell_Lite();

  include_once("./engine/class.buy_lite.inc"); 
  $buyer = new Buyer_Lite();

  include_once("./engine/class.metro_lite.inc"); 
  $metro = new Metro_Lite();

  include_once("./engine/class.district_lite.inc"); 
  $district = new District_Lite();

  include_once("./engine/class.subcategory_lite.inc");
  $subcategory = new SubCategory_Lite();

  include_once("./engine/class.streets_lite.inc");
  $streets = new Streets_Lite();
  
  include_once("./engine/class.equipment_lite.inc"); 
  $equipment = new Equipment_Lite();
  
  	include_once("./engine/class.company.inc"); 
  	$company = new Company(); 	
  
  
  	include_once("./engine/class.ad.inc");
  	$ad = new Ad();

        include_once("./engine/class.review.inc"); 
        $review = new Review();        
        
    include_once("./engine/class.frontend.inc");


require_once("./libs/Smarty.class.php");
  $smarty = new Smarty;
  $smarty->template_dir = "./templates";
  $smarty->compile_dir  = "./templates_c";
  //$smarty->cache_dir = "./cache";
  $smarty->compile_check = true;
  $smarty->debugging     = false;

  	$colorset = GetColorSet();
  	$smarty->assign("colorset", $colorset);  
  
  	$smarty->assign("listcompany", $company->ListCompanyInList());  	  	
  	
  require_once("./regfuncsmarty.php");
  
	$ad->InitAd($smarty, $_GET);  
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
  <HEAD>
<?

	$action_words = array("category", "city", "price", "topbiz");
	if(isset($_GET["action"])) {
		if(in_array($_GET["action"], $action_words)) {
		
		} else {
			$_GET["action"] = "category";
		}
	} else {
		$_GET["action"] = "";
	}

	if(isset($_GET["subcategory"])) {
		$_GET["subcategory"] = intval($_GET["subcategory"]);
		$sell->SubCategoryID = $_GET["subcategory"];
	}

	if(isset($_GET["ID"])) {
		$_GET["ID"] = intval($_GET["ID"]);
	}

  $titleseokey = "";	
	
  if (isset($_GET["action"])) {
    switch(trim($_GET["action"])) {
      case "category": {
        $seokey = $category->GetKeySell(intval($_GET["ID"]));
        $sell->CategoryID = intval($_GET["ID"]);
        if(isset($_GET["subcategory"])) {
        	$dsub["ID"] = intval($_GET["subcategory"]);
        	$rsubcategory = $subcategory->GetItem($dsub);
        	$smarty->assign("seoarticle", $rsubcategory->seoarticle);
        	if(isset($rsubcategory) && strlen($rsubcategory->titlekeysell) > 0) {
        		$titleseokey = $rsubcategory->titlekeysell;
        	}
        }
        ?>
          <title>������� � ������� ������� <?=(strlen($titleseokey) > 0 ? " - ".strtolower($titleseokey) : "");?>. ������� ������ - ��� "������-����"</title>	
        <?
  
        if(strlen($seokey) > 0) {
        	$seokey = $seokey.", ";
        }

      } break;
      case "city": {
        $dataCity["ID"] = intval($_GET["ID"]);
        $objCity = $city->GetItem($dataCity);
        $sell->CityID = $dataCity["ID"];
        ?>
          <title>������� ������� <?=$objCity->title;?> | ������� ������� <?=$objCity->title;?> | ������� ������ - ��� "������-����"</title>		
        <?
        $seokey = "������� ������� ".$objCity->title.", ������� ������� ".$objCity->title.", ";        
      } break;
      case "price": {
        ?>
          <title>������� � ������� �������. ������� ������ - ��� "������-����"</title>
        <?
      } break;
      default: {
        ?>
          <title>������� � ������� �������. ������� ������ - ��� "������-����"</title>		
        <?
      }   
    }
  } else {
    ?>
      <title>������� � ������� ��������. ������� ������ - ��� "������-����"</title>		
    <?
  } 
?>
	<META NAME="Description" CONTENT="������� �������, ������� �������, ������� ������, ������� �������, ������� �������� �������, ������� �������, ������� ��������,  ������� ��������, ������� ��������, ���� ������� ������?">
        <meta name="Keywords" content="<?=(strlen($titleseokey) > 0 ? $titleseokey."," : "");?><?=$seokey;?><?=$seokey;?>������� �������, ������� �������, ������� ������, ������� �������, ������� �������� �������, ������� �������, ������� ��������,  ������� ��������, ������� ��������, ���� ������� ������?, Ready business, �����������, �������� � ��������, ���������, ����������, �����������"> 
        <LINK href="http://www.bizzona.ru/general.css" type="text/css" rel="stylesheet">
        <meta http-equiv="content-type" content="text/html; charset=windows-1251"/>
       <script type="text/javascript" src="ja.js"></script>
  </HEAD>
<body>

<?php
  $smarty->display("./site/headerbanner.tpl");
?>

<? 
	$headertitle = ucfirst($titleseokey);
	$smarty->assign("headertitle", $headertitle);
?>

<table class="w" border="0" cellpadding="0" cellspacing="0" height="100%">
	<tr>
		<td colspan="2">
<?
	$link_country = GetSubMenu();
	$smarty->assign("ad", $link_country);

	echo minimizer($smarty->fetch("./site/header.tpl"));
?>
		</td>
	</tr>
    <tr>
        <td width="30%" valign="top"  height="100%" style="padding-top:2pt;">

        <?
        	$smarty->caching = false; 
			echo fminimizer($smarty->fetch("./site/inner_bannertop_left.tpl"));
		?>         
       
        
        <?
		if(isset($_GET["action"]) && $_GET["action"]  == "category") {
			if (!$smarty->is_cached("./site/subbizleftmenu.tpl", intval($_GET["ID"]))) {
				$sell->CategoryID = (isset($_GET["ID"])  ? intval($_GET["ID"]) : 0);
				$res_sub = $sell->SelectSubCategory();
				$smarty->assign("subbizcategory", $res_sub);
			}
			
			echo fminimizer($smarty->fetch("./site/subbizleftmenu.tpl", intval($_GET["ID"])));			 
		}
		?>        
        
        <?
          $smarty->cache_dir = "./persistentcache"; 
          $smarty->cache_lifetime = 83600;        
        
          if (isset($_GET["action"]) && $_GET["action"]  == "category") {
            $_GET["ID"] = (isset($_GET["ID"])  ? intval($_GET["ID"]) : 0);
            $smarty->caching = true; 
            if (!$smarty->is_cached('./site/categorys.tpl', $_GET["ID"])) {
              $data = array();
              $data["offset"] = 0;
              $data["rowCount"] = 0;
              $data["sort"] = "Count";
              $result = $category->Select($data);
              $smarty->assign("data", $result);
            }
            
  			echo fminimizer($smarty->fetch("./site/categorys.tpl", $_GET["ID"]));
            $smarty->caching = false; 
          } else { 
            $smarty->caching = true; 
            if (!$smarty->is_cached('./site/categorys.tpl')) {
              $data = array();
              $data["offset"] = 0;
              $data["rowCount"] = 0;
              $data["sort"] = "Count";
              $result = $category->Select($data);
              $smarty->assign("data", $result);
            }
            //$smarty->display("./site/categorys.tpl");
            
			echo fminimizer($smarty->fetch("./site/categorys.tpl"));
            
            $smarty->caching = false; 
          }
          $smarty->cache_dir = "./cache";
          $smarty->cache_lifetime = 3600;          
        ?>
        <?
            
            	//echo fminimizer($smarty->fetch("./site/call.tpl"));
            	/* up ���������� ���� ����� ��� ����������� �������� ������ http://bizzona.ru/bgz/view.php?id=8 */
            	$dataadvertising['realty'] = true;
            	$dataadvertising['vip'] = true;
            	
            	//$dataad = $sell->SelectSellAdvertising($dataadvertising);
            	//$smarty->assign("datarealty", $dataad);
      
                LeftBlockTopSell();
            
            	/* down ���������� ���� ����� ��� ����������� �������� ������ http://bizzona.ru/bgz/view.php?id=8 */
            	            	
            	
            	/* up  ����� ���� ����������� ���������� �� ������� �������� ������ http://bizzona.ru/bgz/view.php?id=8
				$smarty->caching = true; 
				$smarty->cache_lifetime = 3600;
            	
            	if (!$smarty->is_cached("./site/iblockbuyer.tpl", "action=".(isset($_GET["action"]) ? $_GET["action"] : ""))) {
            	
            		$datahotbuyer = array();
            		$datahotbuyer["offset"] = 0;
            		$datahotbuyer["rowCount"] = 4;
            		$datahotbuyer["sort"] = "datecreate";
            		$datahotbuyer["StatusID"] = "-1";

                	if (isset($_GET["action"])) {
                   		switch(trim($_GET["action"])) {
	                   		case "category": {
    	                 		$datahotbuyer["typeBizID"] = intval($_GET["ID"]);
                       		} break;
                       		case "city": {
                         		$datahotbuyer["regionID"] = intval($_GET["ID"]);
                       		} break;                       
        	           		default: {
                       		}
                       		break;
                   		}            	
                	}		
            	
  	          		$res_datahotbuyer = $buyer->Select($datahotbuyer);
  	          		
    				$smarty->assign("databuyer", $res_datahotbuyer);        	
            	} 	
            	echo fminimizer($smarty->fetch("./site/iblockbuyer.tpl", "action=".(isset($_GET["action"]) ? $_GET["action"] : "")));
            	$smarty->caching = false; 
            	
            	down  ����� ���� ����������� ���������� �� ������� �������� ������ http://bizzona.ru/bgz/view.php?id=8
            	*/
		?>
		
		<div style="padding-top:2pt;"></div>                    
        <?
			echo fminimizer($smarty->fetch("./site/proposal.tpl"));
        	
			echo fminimizer($smarty->fetch("./site/request.tpl"));
			
			$smarty->caching = false; 
			echo fminimizer($smarty->fetch("./site/seosubcategorytext.tpl"));			
        ?>              
        </td>
        <td width="70%" valign="top"   height="100%">
        
			<?
				if(isset($objCity) && strlen($objCity->phone) > 0) {
					$smarty->assign("phone", $objCity->phone);
					$smarty->assign("phonetitle", $objCity->phonetitle);
				} 
				
				$smarty->caching = false;
				echo minimizer($smarty->fetch("./site/phonecode.tpl"));						
				$smarty->caching = true;
			?>
        
            <?
				$smarty->cache_dir = "./persistentcache";
				$smarty->cache_lifetime = 10800;            
            	$smarty->caching = true; 
            	if (!$smarty->is_cached('./site/city.tpl')) {
            		$data = array();
            		$data["offset"] = 0;
            		$data["rowCount"] = 0;
            		$data["sort"] = "Count";
            		$result = $city->SelectActive($data);
                	$smarty->assign("data", $result);
                } 
                
				echo fminimizer($smarty->fetch("./site/city.tpl"));
       			$smarty->caching = false; 

                $smarty->caching = true; 
				if (!$smarty->is_cached('./site/epoplistbiz.tpl')) {
					$counttopcategory = $sell->ListCountTopCategory();
					$smarty->assign("counttopcategory", $counttopcategory);
				}
				echo fminimizer($smarty->fetch("./site/epoplistbiz.tpl"));
				
				$smarty->cache_dir = "./cache";
				$smarty->cache_lifetime = 3600; 
				$smarty->caching = false;       			
       			
            ?>
            <?
				$output_navsearch = $smarty->fetch("./site/navsearch.tpl");
				echo fminimizer($output_navsearch);
            ?>
               <?

                  $smarty->caching = true; 
                  if (!$smarty->is_cached('./site/city.tpl', $_SERVER["REQUEST_URI"])) {
                    $data = array();
                    $dataPage = array(); 
                    $pagesplit = GetPageSplit();

                    if (isset($_GET["offset"])) {
                      $data["offset"] = intval($_GET["offset"]);
                    } else {
                      $data["offset"] = 0;
                    }
                    if (isset($_GET["rowCount"])) { 
                      $data["rowCount"] = intval($_GET["rowCount"]);
                    } else {
                      $data["rowCount"] = $pagesplit;
                    }

                    $data["sort"] = "ID";

                    if (isset($_GET["action"])) {
                    	
                       switch(trim($_GET["action"])) {
                         case "category": {
                           $_GET["ID"] = intval($_GET["ID"]); 
                           $data["CategoryID"] = $_GET["ID"];
                           if(isset($_GET["subcategory"])) {
                             $data["SubCategoryID"] = intval($_GET["subcategory"]);
                             $dataPage["SubCategoryID"] = intval($_GET["subcategory"]);
                           }
                           $dataPage["CategoryID"] = $_GET["ID"];
                           $_SESSION["uCategoryID"] = $_GET["ID"];
                           if (isset($_SESSION["uCityID"])) unset($_SESSION["uCityID"]);
                         } break;
                         case "city": {
                           $_GET["ID"] = intval($_GET["ID"]);
                           $data["CityID"] = $_GET["ID"];
                           $dataPage["CityID"] = $_GET["ID"];
                           $_SESSION["uCityID"] = $_GET["ID"];
                           if (isset($_SESSION["uCategoryID"]))  unset($_SESSION["uCategoryID"]);
                         } break;
                         case "price": {
                           $_GET["MinCostID"] = isset($_GET["MinCostID"]) ? $_GET["MinCostID"]: 0;
                           $_GET["MaxCostID"] = isset($_GET["MaxCostID"]) ? $_GET["MaxCostID"]: 0; 
                           $data["MinCostID"] = intval($_GET["MinCostID"]);
                           $dataPage["MinCostID"] = intval($_GET["MinCostID"]);
                           $data["MaxCostID"] = intval($_GET["MaxCostID"]);
                           $dataPage["MaxCostID"] = intval($_GET["MaxCostID"]);
                         } break;
                         case "topbiz": {
                           $_GET["name"]  = ( isset($_GET["name"]) ? htmlspecialchars(trim($_GET["name"]), ENT_QUOTES) : "" );
                           if($_GET["name"] == "turagenstvo")
                           {
                              $data["icon"] =   96;
                              $dataPage["icon"] = 96;
                           }   
                           else if($_GET["name"] == "restoran") {
                              $data["icon"] =   92;
                              $dataPage["icon"] = 92;
                           } else if( $_GET["name"] == "otel") {  
                              $data["icon"] =   93;
                              $dataPage["icon"] = 93;
                           } else if ($_GET["name"] == "parik" || $_GET["name"] == "krasota") {   
                              $data["icon"] =   109;
                              $dataPage["icon"] = 109;
                           } else {
                              //$data["icon"] =   $_GET["name"]."."."gif";
                              //$dataPage["icon"] = $_GET["name"]."."."gif";
                           }
                         }
                         default: {
                         }   
                       }
                    }

                    $dataPage["offset"] = 0;
                    $dataPage["rowCount"] = 0;
                    $dataPage["sort"] = "ID";
                    $dataPage["StatusID"] = "-1";
                    $resultPage = $sell->Select($dataPage);
                    $smarty->assign("CountRecord", sizeof($resultPage));
                    $smarty->assign("CountSplit", $pagesplit);
                    $smarty->assign("CountPage", ceil(sizeof($resultPage)/5));

                    $data["StatusID"] = "-1";
                    $data["sort"] = "DataCreate";

                    $result = $sell->Select($data);
                    
                    $result_position = array();
                    
                    while (list ($k, $v) = each($result)) {

                    	if(intval($v->position) > 0) {
                    		$result_position[] = $v;
                    	}
                    }
                    
                    reset($result);
                    
                    while (list ($k, $v) = each($result)) {

                    	if(intval($v->position) <= 0) {
                    		$result_position[] = $v;
                    	}
                    }                    
                    
                    
                    $result = $result_position;

                    $smarty->assign("data", $result);


                  }

				echo $smarty->fetch("./site/saveme.tpl");
                  
                  //$smarty->display("./site/pagesplit.tpl", $_SERVER["REQUEST_URI"]);
				  $output_pagesplit = $smarty->fetch("./site/pagesplit.tpl", $_SERVER["REQUEST_URI"]);
				  echo fminimizer($output_pagesplit);

                    $smarty->assign('colorsell', FrontEnd::ListColorSet());

                    echo fminimizer($smarty->fetch("./site/ilistsell.tpl", $_SERVER["REQUEST_URI"]));
                  
                  $smarty->caching = false; 
               ?>
            	<div class="lftpadding_cnt" style="width: auto;">
       				<div  style="background-image:url(<?=$_SERVER["HTTP_HOST"]?>images/bl.gif); background-repeat:repeat-x;">&nbsp;</div>
            	</div>
            	<?
				  	echo fminimizer($output_pagesplit);
            	?> 
            	<?
				  	echo fminimizer($output_navsearch);
				?>
				
				<?
					include_once("./yadirect.php");
				?>				
				
                                    <? if (IsShowBlockSeoWords()):  ?>
					<div class="lftpadding_cnt" style="padding-top:10pt; padding-bottom: 5pt;padding-top:0pt; padding-bottom:0pt; width:auto;" >
                                            <?
                                                $smarty->caching = true; 
                                                $smarty->cache_dir = "./persistentcache";
                                                $smarty->cache_lifetime = 83600;
                                                if (!$smarty->is_cached("./site/keysell.tpl")) {
                                                    $datakeysell = $category->GetListSubCategoryActive();
                                                    $smarty->assign("datakeysell", $datakeysell);
                                                }
				
                                                echo fminimizer($smarty->fetch("./site/keysell.tpl"));
                                                $smarty->cache_lifetime = 3600;
                                                $smarty->cache_dir = "./cache";                
                                                $smarty->caching = false; 
                                            ?>
                                        </div>
                                    <? endif ?>    
           			
				<div class="lftpadding_cnt" style="padding-top:10pt; padding-bottom: 5pt;padding-top:0pt; padding-bottom:0pt; width:auto;" >	    
					<table border="0" align="right" width="100%">
                		<tr>
                			<td width="30%"></td>
                			<td width="70%" align="right">
								<script type="text/javascript" src="//yandex.st/share/share.js" charset="utf-8"></script>
								<div class="yashare-auto-init" data-yashareType="button" data-yashareQuickServices="yaru,vkontakte,facebook,twitter,odnoklassniki,moimir,lj,friendfeed,moikrug"></div>
               				</td>			
							</tr>
						</table>
				</div>    

                               <div class="lftpadding_cnt">
                                <?
                                    echo $review->LastBlockReview();
                                ?>
                               </div>            
           		
        </td>
    </tr>
    <tr>
    	<td colspan="2">
				<?
				
					$smarty->caching = true; 
					$smarty->cache_lifetime = 10500;
					
					if (!$smarty->is_cached('./site/sugequipment.tpl')) {
            
						$datasugequipment = array();
						$datasugequipment["offset"] = 0;
						$datasugequipment["rowCount"] = 4;
						$datasugequipment["sort"] = "datecreate";
						$datasugequipment["status_id"] = "-1";
            	 
						$res_datasugequipment = $equipment->SugSelect($datasugequipment);
  	          	
						$smarty->assign("datasugequipment", $res_datasugequipment);        	
					}				
				
					?><div class="lftpadding_cnt"><?
						echo fminimizer($smarty->fetch("./site/sugequipment.tpl"));
					?></div><?
					
					$smarty->caching = false; 					
					
				?>	           		
    	
    	</td>
    </tr> 
    <tr>
    	<td colspan="2">
<style>
 td a 
 {
   color:black;
 }
</style>
<div style="padding-top:4pt;padding-bottom:5pt;padding-right:10pt;padding-left:10pt;font-size:10pt;font-family:arial;">
	<table cellpadding="0" cellspacing="0" border="0"  style="width:100%;padding-bottom:2pt;" bgcolor="#eeeee0">
    	<tr>
        	<td style="background-image:url(<?=$_SERVER["host_name"]?>images/d1.gif); background-repeat:no-repeat;width:5px;height:5px; background-position:left top;">&nbsp;</td>
            <td rowspan="2" valign="middle" align="center"  style="font-size:8pt; font-family:Arial; color:black;">
            </td>
            <td style="background-image:url(<?=$_SERVER["host_name"]?>images/d2.gif); background-repeat:no-repeat;width:5px;height:5px;background-position:right top;">&nbsp;</td>
        </tr>
        <tr>
            <td style="background-image:url(<?=$_SERVER["host_name"]?>images/d4.gif); background-repeat:no-repeat;width:5px;height:5px; background-position:left bottom;">&nbsp;</td>
            <td style="background-image:url(<?=$_SERVER["host_name"]?>images/d3.gif); background-repeat:no-repeat;width:5px;height:5px; background-position:right bottom;">&nbsp;</td>
        </tr>
    </table>
</div>
    	</td>
    </tr>
</table>

<?
	if (isset($_GET["action"])) {
		switch(trim($_GET["action"])) {
			case "city": {
				$cID = (isset($_GET["ID"])  ? intval($_GET["ID"]) : 0);
				
				if($cID == 928 || $cID == 23) {
					$output_adv = $smarty->fetch("./site/adv23.tpl");
					echo minimizer($output_adv);
				} else if ($cID == 77 || $cID == 50) {
					$output_adv = $smarty->fetch("./site/adv77.tpl");
					echo minimizer($output_adv);
                                } else if ($cID == 1310 || $cID == 54) {
					$output_adv = $smarty->fetch("./site/adv54.tpl");
					echo minimizer($output_adv);
                                } else if ($cID == 1915 || $cID == 66) {
					$output_adv = $smarty->fetch("./site/adv66.tpl");
					echo minimizer($output_adv);
                                } else if ($cID == 47 || $cID == 78) {
					$output_adv = $smarty->fetch("./site/adv78.tpl");
					echo minimizer($output_adv);
                                } else if ($cID == 985 || $cID == 24) {
					$output_adv = $smarty->fetch("./site/adv24.tpl");
					echo minimizer($output_adv);
                                } else if ($cID == 1441 || $cID == 59) {
					$output_adv = $smarty->fetch("./site/adv59.tpl");
					echo minimizer($output_adv);
                                } else if ($cID == 1816 || $cID == 63) {
					$output_adv = $smarty->fetch("./site/adv63.tpl");
					echo minimizer($output_adv);
                                } else if ($cID == 1264 || $cID == 52) {
					$output_adv = $smarty->fetch("./site/adv52.tpl");
					echo minimizer($output_adv);
                                } else if ($cID == 304 || $cID == 2) {
					$output_adv = $smarty->fetch("./site/adv2.tpl");
					echo minimizer($output_adv);
                                } else if ($cID == 634 || $cID == 39) {
					$output_adv = $smarty->fetch("./site/adv39.tpl");
					echo minimizer($output_adv);
                                } else if ($cID == 1721 || $cID == 16) {
					$output_adv = $smarty->fetch("./site/adv16.tpl");
					echo minimizer($output_adv);
                                } else if ($cID == 1791 || $cID == 61) {
					$output_adv = $smarty->fetch("./site/adv61.tpl");
					echo minimizer($output_adv);
                                } else if ($cID == 1353 || $cID == 55) {
					$output_adv = $smarty->fetch("./site/adv55.tpl");
					echo minimizer($output_adv);
                                } else if ($cID == 1100 || $cID == 48) {
					$output_adv = $smarty->fetch("./site/adv48.tpl");
					echo minimizer($output_adv);
                                } else if ($cID == 2326 || $cID == 74) {
					$output_adv = $smarty->fetch("./site/adv74.tpl");
					echo minimizer($output_adv);
                                } else if ($cID == 554 || $cID == 38) {
					$output_adv = $smarty->fetch("./site/adv38.tpl");
					echo minimizer($output_adv);
                                } else if ($cID == 460 || $cID == 36) {
					$output_adv = $smarty->fetch("./site/adv36.tpl");
					echo minimizer($output_adv);
                                }

			}
			case "category": {
				$subCatID = (isset($_GET["subcategory"])  ? intval($_GET["subcategory"]) : 0);
				$cID = (isset($_GET["ID"])  ? intval($_GET["ID"]) : 0);
				if($cID == 109) {
					$output_adv = $smarty->fetch("./site/adv-salon.tpl");
					echo minimizer($output_adv);
				} else if ($cID == 82) {
					$output_adv = $smarty->fetch("./site/advsub1.tpl");
					echo minimizer($output_adv);
				} else if($cID == 115) {
					$output_adv = $smarty->fetch("./site/advsub156.tpl");
					echo minimizer($output_adv);
				} else if($cID == 92) {
					$output_adv = $smarty->fetch("./site/advsub8.tpl");
					echo minimizer($output_adv);
				} else if($cID == 88 && $subCatID == 22) {
					$output_adv = $smarty->fetch("./site/advsub22.tpl");
					echo minimizer($output_adv);
				} else if($cID == 88 && $subCatID == 23) {
					$output_adv = $smarty->fetch("./site/advsub23.tpl");
					echo minimizer($output_adv);
				} else if($cID == 90 && $subCatID == 41) {
					$output_adv = $smarty->fetch("./site/advsub41.tpl");
					echo minimizer($output_adv);
				} else if($cID == 90 && $subCatID == 42) {
					$output_adv = $smarty->fetch("./site/advsub42.tpl");
					echo minimizer($output_adv);
				} else if($cID == 91 && ($subCatID == 81 || $subCatID == 80)) {
					$output_adv = $smarty->fetch("./site/advsub81.tpl");
					echo minimizer($output_adv);
				} else if($cID == 114 && ($subCatID == 157 || $subCatID == 51 || $subCatID == 52 || $subCatID == 118)) {
					$output_adv = $smarty->fetch("./site/advsub157.tpl");
					echo minimizer($output_adv);
				} else if($cID == 93 && $subCatID == 18) {
					$output_adv = $smarty->fetch("./site/advsub18.tpl");
					echo minimizer($output_adv);
				} else if($cID == 93 && $subCatID == 19) {
					$output_adv = $smarty->fetch("./site/advsub19.tpl");
					echo minimizer($output_adv);
				} else if($cID == 93 && $subCatID == 20) {
					$output_adv = $smarty->fetch("./site/advsub20.tpl");
					echo minimizer($output_adv);
				}
				


			}
			case "topbiz": {
				if (isset($_GET["name"]) && (  $_GET["name"] == "parik" || $_GET["name"] == "krasota"))	{
					$output_adv = $smarty->fetch("./site/adv-salon.tpl");
					echo minimizer($output_adv);
				}
			}
		}
	}
?>


<?
	$smarty->display("./site/footer.tpl");
  	//$output_footer = $smarty->fetch("./site/footer.tpl");
  	//echo minimizer($output_footer);
?>

<?
  $end = get_formatted_microtime(); 
  $total = $end - $start;
  echo "<center><span style='font-size:7pt;'>".round($total, 6)." : ".$count_sql_call." : ".$base_memory_usage." : ".memoryUsage(memory_get_usage(), $base_memory_usage)." </span><center>";
?>


</body>
</html>
<?
	$sell->Close();
	$category->Close();
	$city->Close();
	$country->Close();
	$buyer->Close();
	$district->Close();
	$subcategory->Close();
	$streets->Close();
?>
