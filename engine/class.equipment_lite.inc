<?php
	include_once("class.db.inc");

	define("NEW_EQUIPMENT", 0);
	define("OPEN_EQUIPMENT", 1);
	define("CLOSE_EQUIPMENT",2);
	define("LOOKED_EQUIPMENT", 3);
	define("DUBLICATE_EQUIPMENT", 15);
	define("DUST_EQUIPMENT", 17);
  
  	class Equipment_Lite {
  	
    	var $db = "";
    	var $id = "";
    	var $name = "";
    	var $age = "";
		var $cost = "";
		var $ccost = "";
		var $agreementcost = "";
		var $category_id = "";
		var $subCategory_id = "";
		var $description = "";
		var $shortdescription = "";
		var $city_id = "";
		var $subCity_id = "";
		var $city_text = "";
		var $icon = "";
		var $micon = "";
		var $sicon = "";
		var $email = "";
		var $contact_face = "";
		var $phone = "";
		var $status_id = "";
		var $datecreate = "";
		var $duration_id = "";
		var $statussend_id = "";
		var $leasing = "";
		var $qview = "";     
		var $ip = "";
		var $partnerid = 0;
		var $sql;
		var $companyId = 0;
		var $goodcatparent_id = 0;

		var $pay_status = 0;
		var $pay_summ = 0;
		var $pay_datetime = "";
		
		
		var $urlobjCompanyId = "";
		var $objCompanyId = 0;
		
		var $youtubeURL = "";
		var $youtubeShortURL = "";
		
		
    	function Equipment_Lite() {
       		global $DATABASE;
       		$this->InitDB();
    	} 

     	function InitDB() {
       		global $DATABASE;
       		$this->db = new DB($DATABASE["HOSTNAME"], $DATABASE["DATABASENAME"], $DATABASE["DATABASEUSER"], $DATABASE["DATABASEPASSWORD"]);
     	}
     	
     	function Close() {
     		$this->db->Close();
     	}
     	
     	function CountEquipment() {
     		$sql = 'select count(*) as \'count\' from equipment where status_id in (1);';
			$result = $this->db->Select($sql);
			$row = mysql_fetch_row($result);
			return $row[0];       		
     	}
     	
     	function SugSelect($data)  {
     		
       		$limit = "";
       		$where = " where 1 ";
       		$order = " order  by ".$data["sort"]." desc";

       		if ($data["offset"] === 0 && $data["rowCount"] === 0) {
       		} else {
       			
				if(intval($data["rowCount"]) > 20) { $data["rowCount"] = 5; }
				if(intval($data["offset"]) > 50000) { $data["offset"] = 0; }        			
       			
         		$limit = " limit ".intval($data["offset"]).",".$data["rowCount"]."";
       		} 
     	
       		if (isset($data["category_id"])) {
         		$where .= " and equipment.category_id=".intval($data["category_id"])." ";
       		} 

       		if (isset($data["subCategory_id"])) {
         		$where .= " and equipment.subCategory_id=".intval($data["subCategory_id"])." ";
       		} 
       
       		if (isset($data["city_id"])) {
         		$where .= " and equipment.city_id=".intval($data["city_id"])." ";
       		} 
       
	   		if (isset($data["subCity_id"])) {
	   			$where .= " and subCity_id =".intval($data["subCity_id"])." or city_id=".intval($data["subCity_id"])." ";
	   		}
       
       		if (isset($data["status_id"])) {
         		if ($data["status_id"] == "-1") {
           			$where .= " and equipment.status_id in (1) ";
         		} else {
           			$where .= " and equipment.status_id=".intval($data["status_id"])." ";
         		}
       		} 

     		$sql = "select `equipment`.*  from `equipment` ".$where." ".$order." ".$limit;
     	
     		//echo "<h1>".$sql."</h1>";
     	
     		$result = $this->db->Select($sql);

       		$array = Array();
       		while ($row = mysql_fetch_object($result)) {
        		$obj = new stdclass();
         		$obj->id = $row->id;
				$obj->cost = $row->cost;
				$obj->cost = trim(strrev(chunk_split (strrev($row->cost), 3,' '))); 
				$obj->name = $row->name;
				$obj->age = $row->age;
				$obj->ccost = $row->ccost;
				$obj->agreementcost = $row->agreementcost;
				//$obj->description = $row->description;
				$obj->shortdescription = $row->shortdescription;
				$obj->city_id = $row->city_id;
				$obj->subCity_id = $row->subCity_id;
				$obj->city_text = $row->city_text;
				$obj->datecreate = strftime("%d %B %Y",  strtotime($row->datecreate));
				$obj->qview = $row->qview;
				$obj->shopcartimg = $row->shopcartimg;
			
         		$array[] = $obj;
       		}
       
       		return $array;      		
     		
     	}
     	
		function Select($data) {
     	
       		$limit = "";
       		$where = " where 1 ";
       		$order = " order  by ".$data["sort"]." desc";

       		if ($data["offset"] === 0 && $data["rowCount"] === 0) {
       		} else {
       			
				if(intval($data["rowCount"]) > 20) { $data["rowCount"] = 5; }
				if(intval($data["offset"]) > 50000) { $data["offset"] = 0; }        			
       			
         		$limit = " limit ".intval($data["offset"]).",".$data["rowCount"]."";
       		} 
     	
       		if (isset($data["category_id"])) {
         		$where .= " and equipment.category_id=".intval($data["category_id"])." ";
       		} 

       		if (isset($data["subCategory_id"])) {
         		$where .= " and equipment.subCategory_id=".intval($data["subCategory_id"])." ";
       		} 
       
       		if(isset($data['aCategoryID'])) {
         		$where .= " and  equipment.subCategory_id in (".implode(',', $data['aCategoryID']).") ";
       		}       		
       		
       		if(isset($data['aCityID'])) {
         		$where .= " and  equipment.city_id in (".implode(',', $data['aCityID']).") ";
       		}       		
       	
       		
			if(intval($data["startcostf"]) > 0 && intval($data["stopcostf"]) > 0) {
				$where .= ' and  costseach between '.intval($data["startcostf"]).' and '.intval($data["stopcostf"]).'  ';
			} else if (intval($data["startcostf"]) > 0 && intval($data["stopcostf"]) <= 0) {
				$where .= ' and costseach >= '.intval($data["startcostf"]).'  ';
			} else if (intval($data["startcostf"]) <= 0 && intval($data["stopcostf"]) > 0) {
				$where .= ' and costseach <= '.intval($data["stopcostf"]).'  ';
			}			
       		
       			
       		if (isset($data["city_id"])) {
         		$where .= " and equipment.city_id=".intval($data["city_id"])." ";
       		} 
       		
       		if (isset($data["goodcatparent_id"])) {
         		$where .= " and equipment.goodcatparent_id=".intval($data["goodcatparent_id"])." ";
       		} 
       		
       
	   		if (isset($data["subCity_id"])) {
	   			$where .= " and subCity_id =".intval($data["subCity_id"])." or city_id=".intval($data["subCity_id"])." ";
	   		}
       
       		if (isset($data["status_id"])) {
         		if ($data["status_id"] == "-1") {
           			$where .= " and equipment.status_id in (1) ";
         		} else {
           			$where .= " and equipment.status_id=".intval($data["status_id"])." ";
         		}
       		} else {
       			$where .= " and equipment.status_id in (1) ";
       		}

     		$sql = "select `equipment`.*, Category.icon AS 'categoryicon'  from `equipment`   LEFT JOIN Category ON ( Category.ID = equipment.category_id ) ".$where." ".$order." ".$limit;
     	
     		//echo "<h1>".$sql."</h1>";
     	
     		$result = $this->db->Select($sql);

       		$array = Array();
       		while ($row = mysql_fetch_object($result)) {
        		$obj = new stdclass();
         		$obj->id = $row->id;
				$obj->cost = $row->cost;
				$obj->cost = trim(strrev(chunk_split (strrev($row->cost), 3,' '))); 
				$obj->name = $row->name;
				$obj->age = $row->age;
				$obj->ccost = $row->ccost;
				$obj->agreementcost = $row->agreementcost;
				$obj->category_id = $row->category_id;
				$obj->subCategory_id = $row->subCategory_id;
				$obj->description = stripslashes($row->description);
				$obj->shortdescription =  stripslashes($row->shortdescription);
				$obj->city_id = $row->city_id;
				$obj->subCity_id = $row->subCity_id;
				$obj->city_text = $row->city_text;
				$obj->icon = $row->icon;
				$obj->sicon = $row->sicon;
				$obj->email = $row->email;
				$obj->contact_face = $row->contact_face;
				$obj->phone = $row->phone;
				$obj->status_id = $row->status_id;
				//$obj->datecreate = $row->datecreate;
				$obj->datecreate = strftime("%d %B %Y",  strtotime($row->datecreate));
				$obj->duration_id = $row->duration_id;
				$obj->statussend_id = $row->statussend_id;
				$obj->leasing = $row->leasing;
				$obj->qview = $row->qview;
				$obj->partnerid = $row->partnerid;
				
		 		if(strlen($row->iconlist) > 3) {
					$obj->categoryicon = $row->iconlist;
		 		} else {
					$obj->categoryicon = $row->categoryicon;
		 		}
				$obj->status_id = $row->status_id;
				$obj->pay_summ = $row->pay_summ;
				$obj->ip = $row->ip;
				$obj->shopcartimg = $row->shopcartimg;
			
				if($obj->status_id == NEW_EQUIPMENT) {
					$obj->status_text = "�����";
				} else if($obj->status_id == OPEN_EQUIPMENT) {
					$obj->status_text = "������";
				} else if($obj->status_id == LOOKED_EQUIPMENT) {
					$obj->status_text = "����������";
				} else if($obj->status_id == CLOSE_EQUIPMENT) {
					$obj->status_text = "������";
				} else if($obj->status_id == DUBLICATE_INVESTMENTS) {
					$obj->status_text = "��������";
				} else if($obj->status_id == DUST_INVESTMENTS) {
					$obj->status_text = "�����";
				}				
			
         		$array[] = $obj;
       		}
       
       		return $array;     	
     	}
     	
     	function GetItem() {
		     	
     		$sql = "select `equipment`.* from `equipment` where id=".$this->id.";";
     		$result = $this->db->Select($sql);
        	$obj = new stdclass();

       		while ($row = mysql_fetch_object($result)) {
		 		$this->id = $obj->id = $row->id;
				//$this->cost = $obj->cost = $row->cost;
				$this->cost = $obj->cost = trim(strrev(chunk_split (strrev($row->cost), 3,' '))); 
				$this->costtxt = $obj->costtxt = $row->costtxt;
				$this->name = $obj->name = $row->name;
				$this->age = $obj->age = $row->age;
				$this->ccost = $obj->ccost = $row->ccost;
				$this->agreementcost = $obj->agreementcost = $row->agreementcost;
				$this->category_id = $obj->category_id = $row->category_id;
				$this->subCategory_id = $obj->subCategory_id = $row->subCategory_id;
				$this->description = $obj->description = stripslashes($row->description);
				$this->shortdescription = $obj->shortdescription = stripslashes($row->shortdescription);
				$this->city_id = $obj->city_id = $row->city_id;
				$this->subCity_id = $obj->subCity_id = $row->subCity_id;
				$this->city_text = $obj->city_text = $row->city_text;
				$this->icon = $obj->icon = $row->icon;
				$this->micon = $obj->micon = $row->micon;
				$this->sicon = $obj->sicon = $row->sicon;
				$this->email = $obj->email = $row->email;
				$this->contact_face = $obj->contact_face = $row->contact_face;
				$this->phone = $obj->phone = $row->phone;
				$this->status_id = $obj->status_id = $row->status_id;
				$this->datecreate = $obj->datecreate = $row->datecreate; 
				//$this->datecreate = $obj->datecreate = strftime("%d %B %Y",  strtotime($row->datecreate)); 
				$this->duration_id = $obj->duration_id = $row->duration_id;
				$this->statussend_id = $obj->statussend_id = $row->statussend_id;
				$this->leasing = $obj->leasing = $row->leasing;
				$this->qview = $obj->qview = $row->qview;     
				$this->companyId = $obj->companyId = $row->companyId;
				$this->goodcatparent_id = $obj->goodcatparent_id = $row->goodcatparent_id;
				
				$this->pay_status = $obj->pay_status = $row->pay_status;
				$this->pay_summ = $obj->pay_summ = $row->pay_summ;
				$this->pay_datetime = $obj->pay_datetime = $row->pay_datetime;
				
				$this->companyId = $obj->companyId = $row->companyId;
				$this->objCompanyId = $obj->objCompanyId = $row->objCompanyId;
				$this->urlobjCompanyId = $obj->urlobjCompanyId = $row->urlobjCompanyId;
				
				$this->youtubeURL = $obj->youtubeURL = $row->youtubeURL;
				$this->youtubeShortURL = $obj->youtubeShortURL = $row->youtubeShortURL;
				
       		}

       		return $obj;     	
     	}

     	function IncrementQView() {
        	$sql = "update `equipment` set `qview`=`qview`+1 where id=".$this->id.";";
        	$result = $this->db->Select($sql);
     	}
     
     	function CountByCategory()
     	{
     		$sql = "select count(`id`) as 'count' from `equipment` where 1 and `equipment`.`category_id`='".$this->category_id."' and `equipment`.`id` != ".$this->id." and `status_id` in (".OPEN_EQUIPMENT.");";
     		$result = $this->db->Select($sql);
     		while ($row = mysql_fetch_object($result)) {
     			return $row->count;
     		}
     	}
     
     	function CountByCity()
     	{
     		$sql = "select count(`id`) as 'count' from `equipment` where 1 and `equipment`.`city_id`='".$this->city_id."' and `equipment`.`id` != ".$this->id." and `status_id` in (".OPEN_EQUIPMENT.");";
     		$result = $this->db->Select($sql);
     		while ($row = mysql_fetch_object($result)) {
     			return $row->count;
     		}
     	}
     
		function SelectSubCategory() {
     	
			$sql = "SELECT count( `equipment`.`subCategory_id` ) AS 'count', `SubCategory`.`ID`, `SubCategory`.`name`,  `SubCategory`.`keysellequipment`
					FROM `equipment`, `SubCategory` 
						WHERE 1 
							AND `equipment`.`category_id` =".$this->category_id."
							AND `SubCategory`.`ID` = `equipment`.`subCategory_id` 
							AND `equipment`.`status_id` 
							IN ('1')
								GROUP BY `equipment`.`subCategory_id` order by `SubCategory`.`ordernum` desc ";     	
     	
			$result = $this->db->Select($sql);

			$array = Array();
			while ($row = mysql_fetch_object($result)) {
				$obj = new stdclass();
				$obj->subCategory_id = $row->ID;
				$obj->Name = $row->name;
				$obj->SeoName = $row->keysellequipment;
				$obj->Count = $row->count;
				$array[] = $obj;
			}
		
			return $array;
		}

		function CountStatus($StatusID) {
			$sql = "select count(equipment.id) as 'c' from equipment where equipment.status_id=".intval($StatusID)."; ";
			$result = $this->db->Select($sql);
			while ($row = mysql_fetch_object($result)) {
				return $row->c;
			}
			return 0;
		}  
		
		function ListCountPayStatus() {
			$sql = "select ID from equipment where equipment.status_id in (3,0) and equipment.pay_status=1";
			$array = Array();
			$result = $this->db->Select($sql);
			while ($row = mysql_fetch_object($result)) {
				$array[] = $row->ID;
			}
			return $array;
		}		
		
		function GetResidence() {
			
			$sql = "select city_id, subCity_id from equipment where 1 and id=".$this->id."";
			$result = $this->db->Sql($sql);
			$obj = new stdclass();
     	
			while ($row = mysql_fetch_object($result)) {
				$obj->city_id = $row->city_id;
				$obj->subCity_id = $row->subCity_id;
			}
			return $obj;     	
		}
	}
?>
