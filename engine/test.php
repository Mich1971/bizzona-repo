<?
	global $DATABASE;

	include_once("../phpset.inc");
	include_once("./functions.inc"); 
	AssignDataBaseSetting("../config.ini");
// ��������� �����������
	require_once($_SERVER['DOCUMENT_ROOT'].'/inside/auth.php');
//

	include_once("./class.city.inc"); 
	$city = new City();
	
	include_once("./class.district.inc"); 
	$district = new District();
	
	$data = array();
	$data["offset"] = 0;
	$data["rowCount"] = 0;
	$data["sort"] = "ID";
  	$listCity = $city->Select($data); 
  	
	require ('./xajax_core/xajax.inc.php');
	$xajax = new xajax();
	$xajax->configure('debug', true);
	
	function SelectDistrict($regionID)
	{
		global $district;
		
		$district->regionID = $regionID;
		$res = $district->SelectByRegion();
		if(sizeof($res) > 0) {
			$text = "<select>";
			$text.= "<option>������ ������</option>";
			while(list($k, $v) = each($res)) {
				$text.= "<option>".$v->name."</option>";
			}
			$text.= "</select>";
		} else {
			$text = "";
		}
		
		$objResponse = new xajaxResponse();
		$objResponse->setCharacterEncoding("windows-1251");		
		$objResponse->assign('districtDiv', 'innerHTML', $text);
		$objResponse->alert("ok");
	
		return $objResponse;
	}

	$sDistrict =& $xajax->registerFunction('SelectDistrict');
	$sDistrict->setParameter(0, XAJAX_INPUT_VALUE, "districtID");
	$xajax->processRequest();
	echo '<?xml version="1.0" encoding="UTF-8"?>';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
<?
	$xajax->printJavascript();
?>
</head>

<div id="districtDiv">&#160;</div>

<select id="districtID" onchange='<?=$sDistrict->printScript();?>;'>
<?
	while (list($k, $v)=  each($listCity)) {
		?>
			<option value="<?=$v->ID;?>"><?=$v->ID;?> <?=$v->title;?></option>
		<?
	}
?>
</select>

<?
	$city->Close();
	$district->Close();
?>