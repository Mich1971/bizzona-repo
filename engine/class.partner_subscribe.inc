<?
    include_once("class.db.inc");  
    include_once("class.country_lite.inc");  

    class PartnerSubscribe {
    
        public $db = '';
        public $sql = '';
        public $_Id = 0;
        public $_email = '';
        public $_cityIds = '';
        public $_statusId = '';
        
        private $_aStatusId = array(
            0 => '���������', 
            1 => '��������',
        );
        
        private $_aListCity = null;
        
        function __construct() {
                $this->InitDB();
        }

        function InitDB() {
                global $DATABASE;
                $this->db = new DB($DATABASE["HOSTNAME"], $DATABASE["DATABASENAME"], $DATABASE["DATABASEUSER"], $DATABASE["DATABASEPASSWORD"]);
        }
        
        public function ListStatus() {
            return $this->_aStatusId;
        }
        
        public function ListCity() {
            
            if($this->_aListCity != null) {
                return $this->_aListCity;
            } else {
                $country = new Country_Lite();
                $this->_aListCity = $country->GetCityRussia();
                return $this->_aListCity;
            }
        }
        
        public function CityName($id) {
            
            foreach($this->_aListCity as $k => $v) {
                if($v['id'] == $id) {
                    return $v['name'];
                }
            }
            
        }
        
        public function Insert() {
            
            $this->sql = 'insert into partner_subscribe set '
                    . ' email="'.addslashes($this->_email).'",'
                    . ' cityIds="'.addslashes(serialize($this->_cityIds)).'",'
                    . ' statusId="'.  addslashes($this->_statusId).'"; ';
            
            $this->db->Insert($this->sql);
            
        }
        
        public function Update() {
            
            $this->sql = 'update partner_subscribe set '
                    . ' email="'.addslashes($this->_email).'",'
                    . ' cityIds="'.addslashes(serialize($this->_cityIds)).'",'
                    . ' statusId="'.  addslashes($this->_statusId).'" where Id='.(int)$this->_Id.' limit 1; ';
            
            $this->db->Sql($this->sql);            
            
        }
        
        public function GetItem() {
            
            if($this->_Id > 0) {
                
                $this->sql = 'select * from partner_subscribe where 1 and Id='.(int)$this->_Id.' limit 1;';            
                $result = $this->db->Select($this->sql);

                $array = array();

                while ($row = mysql_fetch_object($result)) {

                    $_cityIds = unserialize($row->cityIds);
                    
                    $array = array(
                        'Id' => $row->Id,
                        'email' => $row->email,
                        'cityIds' => $_cityIds,
                        'statusId' => $row->statusId,
                    );

                }

                return $array;
                
            }
            
            return array();
        }
        
        public function Select() {

            $this->sql = 'select * from partner_subscribe where 1;';            
            $result = $this->db->Select($this->sql);
            
            $array = array();
            
            while ($row = mysql_fetch_object($result)) {
                
                $_cityIds = unserialize($row->cityIds);
                
                $array[] = array(
                    'Id' => $row->Id,
                    'email' => $row->email,
                    'cityIds' => $_cityIds,
                    'aCityIds' => array_chunk($_cityIds, ceil(count($_cityIds)/4)),
                    'statusId' => $row->statusId,
                );
                
            }
            
            return $array;
            
        }
        
        public function Delete() {
             
            $this->sql = 'delete from partner_subscribe  '
                    . ' where Id='.(int)$this->_Id.' limit 1; ';
            
            $this->db->Sql($this->sql);            

        }
        
    }
?>