<?php
	include_once('class.db.inc');  
	include_once('class.phpmailer.php');

	class Sell_Communication_Lite {
		
		var $db = "";

		function Sell_Communication_Lite() {
			global $DATABASE;
			$this->InitDB();
		}

		function InitDB() {
			global $DATABASE;
			$this->db = new DB($DATABASE["HOSTNAME"], $DATABASE["DATABASENAME"], $DATABASE["DATABASEUSER"], $DATABASE["DATABASEPASSWORD"]);
		}
		
		function GetItemForCommunication() {
			
			$sql = 'select Sell.CostID, Sell.cCostID, Sell.FML, Sell.EmailID, Sell.PhoneID, Sell.namebizid,  company.NameForSite, company.email as \'CompanyEmail\' from Sell left join company on (company.Id=Sell.brokerId)  where Sell.ID='.$this->ID.' limit 1;';
			$result = $this->db->Sql($sql);
			
			$obj = new stdClass();
			
			while ($row = mysql_fetch_object($result)) {
					
				$obj->FML = $row->FML;
				$obj->EmailID = $row->EmailID;
				$obj->PhoneID = $row->PhoneID;
				$obj->NameBiz = $row->namebizid;
				$obj->CompanyName = $row->NameForSite;
				$obj->CompanyEmail = $row->CompanyEmail;
				
				$obj->CostID = $row->CostID;
				$obj->cCostID = $row->cCostID;
					
			} 
			
			return $obj;
		}		
		
		function Close() {
			$this->db->Close();
		}		
	}

?>