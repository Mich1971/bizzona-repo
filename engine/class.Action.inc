<?
include_once("class.db.inc");

class Action {

    public $_db = null;
    public $_sql;

    public $_id;
    public $_name;
    public $_categoryActionId;
    public $_price;
    public $_statusId;
    public $_prepayment;
    public $_duration;

    public static $_aStatusId = array(
        0 => '���������',
        1 => '��������',
    );

    function __construct() {
        global $DATABASE;
        $this->InitDB();
    }

    function InitDB() {

        global $DATABASE;

        $this->db = new DB($DATABASE["HOSTNAME"], $DATABASE["DATABASENAME"], $DATABASE["DATABASEUSER"], $DATABASE["DATABASEPASSWORD"]);
    }

    function __destruct() {

    }

    public function  Update() {
        $this->_sql =  " update Action set  `categoryActionId`=".(int)$this->_categoryActionId.",
                                            `name`='".addslashes($this->_name)."',
                                            `price`='".$this->_price."',
                                            `prepayment`=".$this->_prepayment.",
                                            `duration`=".(int)$this->_duration.",
                                            `statusId`=".(int)$this->_statusId."  where id=".(int)$this->_id." limit 1;  ";

        //echo $this->_sql;

        $result = $this->db->Sql($this->_sql);
    }

    public function Insert() {

        $this->_sql = " insert into Action (`categoryActionId`,
                                            `name`,
                                            `price`,
                                            `statusId`,
                                            `duration`,
                                            `prepayment`)
                                            values (
                                            ".(int)$this->_categoryActionId.",
                                            '".addslashes($this->_name)."',
                                            '".$this->_price."',
                                            ".(int)$this->_statusId.",
                                            ".(int)$this->_duration.",
                                            '".$this->_prepayment."'
                                            ); ";

        //echo $this->_sql;

        $id = $this->db->Insert($this->_sql);
        return $id;
    }

    public function GetItem() {

        $arr = array();

        $this->_sql = " select * from Action where id=".(int)$this->_id." ";

        $result = $this->db->Select($this->_sql);
        while ($row = mysql_fetch_object($result)) {

            $arr['id'] = $row->id;
            $arr['name'] = $row->name;
            $arr['categoryActionId'] = $row->categoryActionId;
            $arr['price'] = $row->price;
            $arr['statusId'] = $row->statusId;
            $arr['prepayment'] = $row->prepayment;
            $arr['duration'] = $row->duration;
        }

        return $arr;

    }

    public function Select() {

        $arr = array();

        $this->_sql = " select Action.*, CategoryAction.name as 'categoryname' from Action, CategoryAction where 1=1 and CategoryAction.id = Action.categoryActionId order by Action.categoryActionId;  ";

        $result = $this->db->Select($this->_sql);
        while ($row = mysql_fetch_object($result)) {

            $_arr = array();

            $_arr['id'] = $row->id;
            $_arr['name'] = $row->name;
            $_arr['categoryname'] = $row->categoryname;
            $_arr['categoryActionId'] = $row->categoryActionId;
            $_arr['price'] = $row->price;
            $_arr['statusId'] = $row->statusId;
            $_arr['prepayment'] = $row->prepayment;
            $_arr['duration'] = $row->duration;

            $arr[] = $_arr;
        }

        return $arr;
    }

    public function Delete($aParams) {
    }

}

?>