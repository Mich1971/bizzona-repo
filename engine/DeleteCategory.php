<?
  global $smarty,
         $category;
// требуется авторизация
	require_once($_SERVER['DOCUMENT_ROOT'].'/inside/auth.php');
//

  if (isset($_GET["ID"])) {
    $data = array();
    $data["ID"] = intval($_GET["ID"]);
    $data["offset"] = 0;
    $data["rowCount"] = 0;
    $data["sort"] = "ID";
    
    $category->Delete($data);
  }


  $data = array();
  $data["offset"] = 0;
  $data["rowCount"] = 0;
  $data["sort"] = "ID";

  $result = $category->Select($data);

  $smarty->assign("data", $result);
  $smarty->display("./category/delete.tpl");
?>