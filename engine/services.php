<?
	ini_set("magic_quotes_gpc","off");

	global $DATABASE;

	include_once("../phpset.inc");
	include_once("./functions.inc"); 

	AssignDataBaseSetting("../config.ini");
// требуется авторизация
	require_once($_SERVER['DOCUMENT_ROOT'].'/inside/auth.php');
//

	ini_set("display_startup_errors", "on");
	ini_set("display_errors", "on");
	ini_set("register_globals", "on");

    include_once("./class.Service2Action.inc");
    $service2action = new Service2Action();

    include_once("./class.services.inc");
	$services = new Services();

    include_once("./class.Action.inc");
    $action = new Action();



//include_once("./class.ServiceAction.inc");
	//$serviceAction = new ServicesAction();
	
	 
	require_once("../libs/Smarty.class.php");
	$smarty = new Smarty;
	$smarty->template_dir = "../templates";
	$smarty->compile_check = false;
	$smarty->caching = false;
	$smarty->compile_dir  = "../templates_c";
        $smarty->cache_dir = "../cache";        
	$smarty->debugging = false;
	$smarty->clear_all_cache();
	

	if(isset($_POST['serviceactionadd']) /*or isset($_POST['serviceactionupdate'])*/) {

        $service2action->_serviceId = (int)$_GET['id'];
        $service2action->Delete();

        if(isset($_POST['actions'])) {
           $service2action->_aAcionId = array_keys($_POST['actions']);
           $service2action->Insert();
        }

        /*
		$serviceAction->_serviceId = (int)$_GET['id'];
		$serviceAction->_name = $_POST['_name'];
		$serviceAction->_price = $_POST['_price'];
		$serviceAction->_statusId = $_POST['_statusId'];
		$serviceAction->_prepayment = (isset($_POST['_prepayment']) ? 1 : 0);
		if(isset($_POST['serviceactionadd'])) {
			$serviceAction->Insert();
		} else {
			$serviceAction->_id = (int)$_GET['serviceactionid'];
			$serviceAction->Update();
		}
		*/
	}


	if(isset($_POST['add']) or isset($_POST['update'])) {
		
		$services->_description = $_POST['description'];
		$services->_name = $_POST['name'];
		$services->_shortName = $_POST['shortName'];
		$services->_isBroker = (isset($_POST['isBroker']) ? '1' : '0');
		$services->_isFastShow = (isset($_POST['isFastShow']) ? '1' : '0');
		$services->_statusId = (isset($_POST['statusId']) ? '1' : '0');
		$services->_isPrice = (isset($_POST['isPrice']) ? '1' : '0');
		$services->_isCabinet = (isset($_POST['isCabinet']) ? '1' : '0');
		
		if(isset($_POST['add'])) {
			$services->Insert();
		} else {
			$services->_id = (int)$_GET['id'];
			$services->Update();
		}
	}
	
	if(isset($_GET['id'])) {
		$services->_id = (int)$_GET['id'];
		$item = $services->GetItem();
		$smarty->assign("item", $item);

        $smarty->assign("listaction", $action->Select());

        $service2action->_serviceId = (int)$_GET['id'];

        $smarty->assign("selectedlistaction", $service2action->GeActions());

        /*
		$serviceAction->_serviceId = (int)$_GET['id'];
		$smarty->assign("listaction", $serviceAction->Select());	
		
		if(isset($_GET['serviceactionid'])) {
			
			$serviceAction->_id = (int)$_GET['serviceactionid'];
			$smarty->assign("itemaction", $serviceAction->GetItem());	
			
		}
		*/
	}
	
	if(isset($_GET['up'])) {
		$services->_id = (int) $_GET['up'];
		$services->_incr = 'up';
		$services->SetPosition();
	}
	
	if(isset($_GET['down'])) {
		$services->_id = (int) $_GET['down'];
		$services->_incr = 'down';
		$services->SetPosition();
	}

	
	
	$smarty->assign("list", $services->Select());	
	
	
	
	
?>
	<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
    	<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">	
	 	<LINK href="../general.css" type="text/css" rel="stylesheet">
	 	<meta http-equiv="content-type" content="text/html; charset=windows-1251"/>     
	 	<body>
<?
        $smarty->display("./mainmenu.tpl");

		$smarty->display("./services/list.tpl");
		
		$smarty->display("./services/add.tpl");
		
		if(isset($_GET['id'])) {
			//$smarty->display("./services/action.tpl");
            $smarty->display("./services/service_action.tpl");
		}
		
		$smarty->display("./services/help.tpl");
?>
		</body>
		</html>
<?
?>