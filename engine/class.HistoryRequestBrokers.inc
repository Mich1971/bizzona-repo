<?php
    include_once("class.db.inc");  
 
    
    class HistoryRequestBrokers {
 
        public $db = '';
        public $sql = '';
        public $id = 0;
        public $brokerId = 0;
        public $statusId = 0;
        public $typeId = 0;
        public $message = '';
        public $datetime = '';
        public $countProcessed = 0;
  
        public static $TYPE_IMPORT = 1;
        public static $TYPE_UPDATE = 2; 
        public static $TYPE_DELETE = 3;
        public static $TYPE_REOPEN = 4;
         
        
        function __construct() {
                global $DATABASE;
                $this->InitDB();
        }

        function InitDB() {
                global $DATABASE;
                $this->db = new DB($DATABASE["HOSTNAME"], $DATABASE["DATABASENAME"], $DATABASE["DATABASEUSER"], $DATABASE["DATABASEPASSWORD"]);
        }
 
        function Insert() { 
                    
            $this->sql = "insert into HistoryRequestBrokers (`brokerId`,`statusId`,`typeId`,`message`, `datetime`, `countProcessed`) values (".$this->brokerId.", ".$this->statusId.", ".$this->typeId.", '".$this->message."', NOW(), ".$this->countProcessed.");";
            $result = $this->db->Insert($this->sql);
        }
        
        function TypeStr($typeId) {
            
            if($typeId == self::$TYPE_DELETE) {
                return '��������';
            }
            
            if($typeId == self::$TYPE_IMPORT) {
                return '����������';
            }
            
            if($typeId == self::$TYPE_UPDATE) {
                return '����������';
            }            
            
            if($typeId == self::$TYPE_REOPEN) {
                return '��������� ����������';
            }            
            
        }
        
        function Select($aParams = array()) {
            
            $this->sql = "select * from HistoryRequestBrokers where 1 and brokerId=".$this->brokerId." and DATEDIFF(NOW(), datetime) <= 2 order by id desc;";
            $result = $this->db->Select($this->sql); 
            
            $array = Array();
            while ($row = mysql_fetch_object($result)) {
                
                $array[] = array(
                    'statusId' => $row->statusId,
                    'brokerId' => $row->brokerId,
                    'typeId' => $this->TypeStr($row->typeId),
                    'datetime' => $row->datetime,
                    'message' => $row->message,
                    'countProcessed' => $row->countProcessed,
                );
                
            } 

            return $array;
            
        }
        
        
    }
 
?>
