<?php
	include_once("class.db.inc");  
	
	class Tariff
	{
		public $db = "";
		public $sql = "";
		public $Id;
		public $Name;
		public $Description;

		
		function __construct() {
			global $DATABASE;
			$this->InitDB();
		}

		function InitDB() {
			global $DATABASE;
			$this->db = new DB($DATABASE["HOSTNAME"], $DATABASE["DATABASENAME"], $DATABASE["DATABASEUSER"], $DATABASE["DATABASEPASSWORD"]);
		}
		
		
		function GetItem() {
			
			$_tariff = new Tariff();
			$this->sql = "select Id, Name, Description from tariff where 1 and id=".intval($this->Id).";";
			$result = $this->db->Select($this->sql);
			while ($row = mysql_fetch_object($result)) {
				$_tariff->Id = $row->Id;
				$_tariff->Name = $row->Name;
				$_tariff->Description = $row->Description;
			}
			return $_tariff;
			
		}

		
		function Close() {
			$this->db->Close();
		}

	}
?>