<?php
    //ini_set('xdebug.show_mem_delta', 'On');
    //ini_set('xdebug.collect_return', 'On');
    //ini_set('xdebug.collect_params', '4');

    //xdebug_start_trace('debug');        

    ini_set("magic_quotes_gpc","off");
    ini_set("memory_limit","1024M");

    //ini_set("memory_limit", "128M"); 

    global $DATABASE;

    include_once("../phpset.inc");
    include_once("./functions.inc"); 
// �����������    
	require_once($_SERVER['DOCUMENT_ROOT'].'/inside/auth.php');    
//

    //ini_set("display_startup_errors", "on");
    //ini_set("display_errors", "on");
    //ini_set("register_globals", "on");


    $count_sql_call = 0;
    $start = get_formatted_microtime();   
    $base_memory_usage = memory_get_usage();  
      
    
    include_once("./CreateSubDirImg.php");     
    include_once("./exportfunctions.inc");
    require_once("./Cache/Lite.php");
    require_once("./phones.php");

    st_engine("../st_engine.txt");

    CreateSubDirImg('../simg/');

    AssignDataBaseSetting("../config.ini");

    include_once("./class.district.inc"); 
    $district = new District();

    include_once("./class.metro.inc"); 
    $metro = new Metro();

    include_once("./class.category.inc"); 
    $category = new Category();

    include_once("./class.subcategory.inc");
    $subCategory = new SubCategory();

    include_once("./class.country.inc");
    $country = new Country();

    include_once("./class.city.inc");
    $city = new City();

    include_once("./class.emailoffer.inc");
    $emailoffer = new EmailOffer();

    include_once("./class.sell.inc");
    $sell = new Sell();

    include_once("./class.BuyerSellSubscribe.inc");
    $buyerSellSubscribe = new BuyerSellSubscribe();

    include_once("./class.streets.inc");
    $streets = new streets();

    include_once("./class.topparameters.inc");
    $topparameters = new TopParameters();

    include_once("./class.company.inc");
    $company = new Company();

    include_once("./class.services.inc");
    $services = new Services();

    include_once("./class.ProcessingActions.inc");
    $processingActions = new ProcessingActions();

    include_once("./class.CategoryAction.inc");
    $categoryAction = new CategoryAction();

    include_once("./class.MCSingleton.inc");

    include_once("./class.whois.inc");
    $whois = new WhoIs();

    include_once("./class.BadWords.inc");
    $badWords = new BadWords();

    include_once("./class.partner_subscribe.inc");
    $partnerSubscribe = new PartnerSubscribe();

    include_once("./class.geocode.inc");
    $googlegeocode = new GeoCode();
    $googlegeocode->pathfile = "../mapsxml";
    $googlegeocode->typeServiceId = 1;

    include_once("./class.templateevent.inc"); 
    $templateEvent = new TemplateEvent();

    include_once("./observer_filter.php"); 
        
    //filterSet('sell');

    require_once("../libs/Smarty.class.php");
    $smarty = new Smarty;
    $smarty->template_dir = "../templates";
    $smarty->compile_check = false;
    $smarty->caching = false;
    $smarty->compile_dir  = "../templates_c";
    $smarty->cache_dir = "../cache";
    $smarty->debugging = false;
    $smarty->clear_all_cache();

    $smarty->assign("currentDateBySecond", strtotime("now"));

    $smarty->assign('badWords', $badWords);

    require ('./xajax_core/xajax.inc.php');
    $xajax = new xajax();
    //$xajax->configure('debug', true);

    $arrayError = array();

    if(isset($_GET['blockip'])) {

        include_once("./class.BlockIP.inc"); 
        $blockIp = new BlockIP();
        
        if($_GET['blockip'] == 'up') {
            
            $blockIp->BlockUp(array(
                'ip' => trim($_GET['ip'])
            ));
                    
        } else if ($_GET['blockip'] == 'down') {

            $blockIp->BlockDown(array(
                'ip' => trim($_GET['ip'])
            ));
            
        }
    }
    
    if(isset($_POST['delservice'])) {

        $mc = MCSingleton::getInstance();
        $mc->connection->delete('mainsell');
        $mc->connection->delete('lefttopsell');
        $mc->connection->delete('righttopsell');

        $mc->connection->delete('leftsell');
        $mc->connection->delete('rightsell');

        $mc->connection->delete('colorsell');

        $processingActions->DeleteService(array(
            'serviceId' => (int)$_POST['serviceId'],
            'codeId' => (int)$_POST['codeId'],
        ));

    }

    if(isset($_POST['editservice'])) {

        $mc = MCSingleton::getInstance();
        $mc->connection->delete('mainsell');
        $mc->connection->delete('lefttopsell');
        $mc->connection->delete('righttopsell');

        $mc->connection->delete('leftsell');
        $mc->connection->delete('rightsell');

        $mc->connection->delete('colorsell');

        $processingActions->UpdateActions($_POST);

    }


    if(isset($_POST['addservice'])) {

        $mc = MCSingleton::getInstance();
        $mc->connection->delete('mainsell');
        $mc->connection->delete('lefttopsell');
        $mc->connection->delete('righttopsell');

        $mc->connection->delete('leftsell');
        $mc->connection->delete('rightsell');

        $mc->connection->delete('colorsell');

        $_aAddservice = array(
            'serviceId' => (int)$_POST['listservicesId'],
            'codeId' => (int) $_GET['ID'],
        );

        $processingActions->InsertService($_aAddservice);
    }

	$smarty->assign("vipsetting", $categoryAction->Select()); /*Sell::$vipsetting();*/
	
	function resetcache() {
		
		global $smarty;
		
		// up update cache sell
  		$smarty->cache_dir = "../sellcache";
  		$useMapGoogle = true;
  		$smarty->clear_cache("./site/detailsSell.tpl", intval($_POST["ID"])."-".$useMapGoogle);
  		$useMapGoogle = false;
  		$smarty->clear_cache("./site/detailsSell.tpl", intval($_POST["ID"])."-".$useMapGoogle);
  		$smarty->cache_dir = "../cache";
  	
  	
  		$options = array(
  			'cacheDir' => "../sellcache/",
   			'lifeTime' => 1
  		);
 		
  		$cache = new Cache_Lite($options);
  		
  		  		
  		$_POST['ID'] = (isset($_POST['ID']) ? $_POST['ID'] : $_GET['ID']);
  		
 		$cache->remove(''.intval($_POST["ID"]).'____sellitem');
  		// up update cache sell
	}
	
	function SelectSubDistrict($regionID) {

		global $district, $metro, $sell, $streets, $city;

		$default_district = "";
		$default_metro = "";
		$default_street = "";
		$default_subregion = "";
		if(isset($_GET["ID"])) {
			$sell->ID = intval($_GET["ID"]);
			$residence = $sell->GetResidence();
			$default_district = $residence->districtID;
			$default_street = $residence->streetID;
			$default_metro = $residence->metroID;
			$default_subregion = $residence->subCityID;
		}


		$district->regionID = $regionID;
		$metro->regionID = $regionID;
		$res = $district->SelectByRegion();
		$resmetro =  $metro->SelectByRegion();
		
		if(sizeof($res) > 0) {
			$text = "<select name='districtID'>";
			$text.= "<option  value='0'>������ ������</option>";
			while(list($k, $v) = each($res)) {
				if($v->ID == $default_district) {
					$text.= "<option value='".$v->ID."' selected>".$v->name."</option>";
				} else {	
					$text.= "<option value='".$v->ID."'>".$v->name."</option>";
				}
			}
			$text.= "</select>";
		} else {
			$text = "";
		}
		
		if(sizeof($resmetro) > 0) {
			$textmetro = "<select name='metroID'>";
			$textmetro.= "<option value='0'>�����</option>";
			while(list($k, $v) = each($resmetro)) {
				if($v->ID == $default_metro) {
					$textmetro.= "<option value='".$v->ID."' selected>".$v->name."</option>";
				} else {
					$textmetro.= "<option value='".$v->ID."'>".$v->name."</option>";
				}
			}
			$textmetro.= "</select>";
			
		}
		

		$streets->cityId = $regionID;
		
		$res = $streets->Select();
		
		if(sizeof($res) > 0) {
			$streestext = "<select name='streetID'>";
			$streestext.= "<option  value='0'>����� ������</option>";
			while(list($k, $v) = each($res)) {
				if($v->id == $default_street) {
					$streestext.= "<option value='".$v->id."' selected>".$v->name."</option>";
				} else {
					$streestext.= "<option value='".$v->id."'>".$v->name."</option>";
				}
				
			}
			$streestext.= "</select>";
		} else {
			$streestext = "";
		}

		
		$objResponse = new xajaxResponse();
		$objResponse->setCharacterEncoding("windows-1251");		
		$objResponse->assign('districtDiv', 'innerHTML', $text);
		$objResponse->assign('metroDiv', 'innerHTML', $textmetro);
		$objResponse->assign('streetsDiv', 'innerHTML', $streestext);
		return $objResponse;
		
	}
	
	
	function SelectDistrict($regionID) {
		global $district, $metro, $sell, $streets, $city;

		$default_district = "";
		$default_metro = "";
		$default_street = "";
		$default_subregion = "";
		if(isset($_GET["ID"])) {
			$sell->ID = intval($_GET["ID"]);
			$residence = $sell->GetResidence();
			$default_district = $residence->districtID;
			$default_street = $residence->streetID;
			$default_metro = $residence->metroID;
			$default_subregion = $residence->subCityID;
		}

		$district->regionID = $regionID;
		$metro->regionID = $regionID;
		$res = $district->SelectByRegion();
		$resmetro =  $metro->SelectByRegion();
		
		if(sizeof($res) > 0) {
			$text = "<select name='districtID'>";
			$text.= "<option  value='0'>������ ������</option>";
			while(list($k, $v) = each($res)) {
				if($v->ID == $default_district) {
					$text.= "<option value='".$v->ID."' selected>".$v->name."</option>";
				} else {	
					$text.= "<option value='".$v->ID."'>".$v->name."</option>";
				}
			}
			$text.= "</select>";
		} else {
			$text = "";
		}
  	    
		if(sizeof($resmetro) > 0) {
			$textmetro = "<select name='metroID'>";
			$textmetro.= "<option value='0'>�����</option>";
			while(list($k, $v) = each($resmetro)) {
				if($v->ID == $default_metro) {
					$textmetro.= "<option value='".$v->ID."' selected>".$v->name."</option>";
				} else {
					$textmetro.= "<option value='".$v->ID."'>".$v->name."</option>";
				}
			}
			$textmetro.= "</select>";
			
		}
		

		$streets->cityId = $regionID;
		
		$res = $streets->Select();
		
		if(sizeof($res) > 0) {
			$streestext = "<select name='streetID'>";
			$streestext.= "<option  value='0'>����� ������</option>";
			while(list($k, $v) = each($res)) {
				if($v->id == $default_street) {
					$streestext.= "<option value='".$v->id."' selected>".$v->name."</option>";
				} else {
					$streestext.= "<option value='".$v->id."'>".$v->name."</option>";
				}
				
			}
			$streestext.= "</select>";
		} else {
			$streestext = "";
		}
		
		$subregiontext = "";
		
		$city->id = $regionID;
		$asubregion = $city->GetChildRegionAdmin();
		if(sizeof($asubregion) > 0)
		{
			$subregiontext = "<select name='subRegionID' onchange='xajax_SelectSubDistrict(xajax.$(\"subRegionID\").value)' >";
			$subregiontext.= "<option  value='0'>������ �������, ����</option>";
			while(list($k, $v) = each($asubregion)) {
				if($v->id == $default_subregion) {
					$subregiontext.= "<option value='".$v->id."' selected>".$v->title."</option>";
				} else {
					$subregiontext.= "<option value='".$v->id."'>".$v->title."</option>";
				}
				
			}
			$subregiontext.= "</select>";
		}
		
		$objResponse = new xajaxResponse();
		$objResponse->setCharacterEncoding("windows-1251");		
		$objResponse->assign('districtDiv', 'innerHTML', $text);
		$objResponse->assign('metroDiv', 'innerHTML', $textmetro);
		$objResponse->assign('streetsDiv', 'innerHTML', $streestext);
		$objResponse->assign('subRegionDiv', 'innerHTML', $subregiontext);
		return $objResponse;

	}
  
	
	function DisplayIcon($src) {
		$text = "";
		if(strlen($src) > 3) {
			$text = "<img src='http://www.bizzona.ru/iconbiz/".$src."'>";
		} 
		$objResponse = new xajaxResponse();
		$objResponse->setCharacterEncoding("windows-1251");		
		$objResponse->assign('iconDiv', 'innerHTML', $text);
		return $objResponse;
  	
	}
	
	function DisplayCategoryIcon($id) {
		global $subCategory;
		$aid = split("[:]", $id);
		
		$subCategory->ID = $aid[1];
		$src = $subCategory->GetIcon();
		if(strlen($src) < 3) {
			$src = "money.jpg"; 
		}
		$text = "<img src='http://www.bizzona.ru/iconbiz/".$src."'>";
		$objResponse = new xajaxResponse();
		$objResponse->setCharacterEncoding("windows-1251");		
		$objResponse->assign('dCategoryIconID', 'innerHTML', $text);
		return $objResponse;
	}
	
	function AutoLoadDisplayCategoryIcon() {
		global $sell, $subCategory;
		if(isset($_GET["ID"])) {
			$sell->ID =  intval($_GET["ID"]);
			$subCategory->ID = $sell->GetSubCategoryID();
			$src = $subCategory->GetIcon();
			if(strlen($src) < 3) {
				$src = "money.jpg";
			}
			$text = "<img src='http://www.bizzona.ru/iconbiz/".$src."'>";
			$objResponse = new xajaxResponse();
			$objResponse->setCharacterEncoding("windows-1251");		
			$objResponse->assign('dCategoryIconID', 'innerHTML', $text);
			return $objResponse;
		}
	}
	
	function AutoLoadRegionSetting() {
		global $sell;
		if(isset($_GET["ID"])) {
			$sell->ID = intval($_GET["ID"]);
			$residence = $sell->GetResidence();
			return SelectDistrict($residence->CityID);
		}
	}

	function AutoLoadSubRegionSetting() {
		global $sell;
		if(isset($_GET["ID"])) {
			$sell->ID = intval($_GET["ID"]);
			$residence = $sell->GetResidence();
			return SelectSubDistrict($residence->subCityID);
		}
	}
	
	
	function AutoDisplayIcon() {
		
		global $sell;
		if(isset($_GET["ID"])) {
			$sell->ID = intval($_GET["ID"]);
			$sellicon = $sell->GetIcon();
			return DisplayIcon($sellicon);
		}
	}
	
	$sDistrict =& $xajax->registerFunction('SelectDistrict');
	$sDistrict->setParameter(0, XAJAX_INPUT_VALUE, "CityID");

	$sSubDistrict =& $xajax->registerFunction('SelectSubDistrict');
	$sSubDistrict->setParameter(0, XAJAX_INPUT_VALUE, "subRegionID");
	
	$dIcon =& $xajax->registerFunction('DisplayIcon');
	$dIcon->setParameter(0, XAJAX_INPUT_VALUE, "IconID");
	
	$dCategoryIcon =& $xajax->registerFunction('DisplayCategoryIcon');
	$dCategoryIcon->setParameter(0, XAJAX_INPUT_VALUE, "CategoryIconID");
  
	$autoLoad =& $xajax->registerFunction('AutoLoadRegionSetting');
	$autoSubLoad =& $xajax->registerFunction('AutoLoadSubRegionSetting');
	$autoIcon =& $xajax->registerFunction('AutoDisplayIcon');
	$autoCategoryIcon =& $xajax->registerFunction('AutoLoadDisplayCategoryIcon');
  
	$xajax->processRequest();
	echo '<?xml version="1.0" encoding="UTF-8"?>';  
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<LINK href="../general.css" type="text/css" rel="stylesheet">
<meta http-equiv="content-type" content="text/html; charset=windows-1251"/>	
<head>
<?
	$xajax->printJavascript();
?>
</head>
<meta http-equiv="content-type" content="text/html; charset=windows-1251"/>

<body>

<?

	if(isset($_POST["galleryupdate"]) && $_POST["galleryupdate"]=="galleryupdate") {
		
		
		/////////////////////////////////////////////
  		function LoadClientImg($fileload, $count) {
  
                    $auto_create_path = CreateSubDirImg();
                     
                    $max_image_width	= 380;
                    $max_image_height	= 600;
                    $max_image_size	= 3024 * 3024;
                    $valid_types 	=  array("gif","GIF","jpg", "png", "PNG", "jpeg","JPG");

                    if (isset($_FILES[$fileload])) {
                        
                        //$newname = time() + $count; 
                        $newname = uniqid("sell_");

                            if (is_uploaded_file($_FILES[$fileload]['tmp_name'])) {
                                
                                $filename = $_FILES[$fileload]['tmp_name'];
                                $ext = substr($_FILES[$fileload]['name'], 1 + strrpos($_FILES[$fileload]['name'], "."));


                                if (filesize($filename) > $max_image_size) {
                                    $error = '������: ������ ����� ������ 1 ��.';
                                    $arrayError[] = '������: ������ ����� ������ 1 ��.';
                                    $newname = ""; 
                                } elseif (!in_array($ext, $valid_types)) {
                                    $error = '������: ���������� ����� ����������� ���������� ����� �� ����������� ���';
                                    $arrayError[] = '������: ���������� ����� ����������� ���������� ����� �� ����������� ���';
                                    $newname = ""; 
                                } else {
                                    if (1) {
                                      
                                        if (move_uploaded_file($filename, "../simg/".$auto_create_path.'/'.$newname.".".$ext)) {
                                        } else {
                                            $error =  '������ : ���������� ��������� ���������� ���������� �� ������� � webmaster@bizzona.ru.';
                                            $arrayError[] = '������ : ���������� ��������� ���������� ���������� �� ������� � webmaster@bizzona.ru.';
                                            $newname = ""; 
                                        }
                                    } else {
                                        $error = '������: ���������� ��������� ������ �� �����. ���������� ������� ������ ����.';
                                        $arrayError[]  = '������: ���������� ��������� ������ �� �����. ���������� ������� ������ ����.';
                                        $newname = "";
                                    }
                                }
                            } else {
                                    $newname = "";
                            }
                        }  	

    		
                    return (strlen($newname) > 0 ? $auto_create_path.'/'.$newname.".".$ext : "");
  		}		
		/////////////////////////////////////////////
		
		
		
		$sell->ID = intval($_GET["ID"]);
		
		if(isset($_POST["admindeletefile"])) {
			
			$sell->DeleteAllPhoto();
			resetcache();
			
		}
		
                $auto_create_path = CreateSubDirImg();
                
		if(isset($_FILES["adminfile1"]) && strlen($_FILES["adminfile1"]["name"]) > 0) {

			$sell->DeletePhoto1();
			$newname_ImgID = LoadClientImg("adminfile1", 1);
                        
			$sell->imgId = $newname_ImgID;
			$sell->UpdatePhoto1();
			
                        $sImgFile = RatioResizeImg300("../simg/".$sell->imgId, "../simg/".$auto_create_path."/".uniqid("sell_"));
                        
                        $splImgFile= split("\/", $sImgFile);
                        $dataImg["ID"] = $_GET["ID"];
                        $dataImg["sImgID"] = $auto_create_path."/".$splImgFile[sizeof($splImgFile)-1];
                        
                        echo $dataImg["sImgID"];
                        
                        $sell->Update_sImg($dataImg);
			
		}
		
		if(isset($_FILES["adminfile2"]) && strlen($_FILES["adminfile2"]["name"]) > 0) {

			$sell->DeletePhoto2();
			$newname_Img1ID = LoadClientImg("adminfile2", 2);
			$sell->img1Id = $newname_Img1ID;
			$sell->UpdatePhoto2();
			
                        $sImgFile = RatioResizeImg300("../simg/".$sell->img1Id, "../simg/".$auto_create_path."/".uniqid("sell_"));
                        $splImgFile= split("\/", $sImgFile);
                        $dataImg["ID"] = $_GET["ID"];
                        $dataImg["sImg1ID"] = $auto_create_path."/".$splImgFile[sizeof($splImgFile)-1];
                        $sell->Update_sImg1($dataImg);
			
		}		
		
		if(isset($_FILES["adminfile3"]) && strlen($_FILES["adminfile3"]["name"]) > 0) {

			$sell->DeletePhoto3();
			$newname_Img3ID = LoadClientImg("adminfile3", 3);
			$sell->img2Id = $newname_Img3ID;
			$sell->UpdatePhoto3();
			
                        $sImgFile = RatioResizeImg300("../simg/".$sell->img2Id, "../simg/".$auto_create_path."/".uniqid("sell_"));
                        $splImgFile= split("\/", $sImgFile);
                        $dataImg["ID"] = $_GET["ID"];
                        $dataImg["sImg2ID"] = $auto_create_path."/".$splImgFile[sizeof($splImgFile)-1];
                        $sell->Update_sImg2($dataImg);
		}		

		if(isset($_FILES["adminfile4"]) && strlen($_FILES["adminfile4"]["name"]) > 0) {
			
			$sell->DeletePhoto4();
			$newname_Img4ID = LoadClientImg("adminfile4", 4);
			$sell->img3Id = $newname_Img4ID;
			$sell->UpdatePhoto4();
			
                        $sImgFile = RatioResizeImg300("../simg/".$sell->img3Id, "../simg/".$auto_create_path."/".uniqid("sell_"));
                        $splImgFile= split("\/", $sImgFile);
                        $dataImg["ID"] = $_GET["ID"];
                        $dataImg["sImg3ID"] = $auto_create_path."/".$splImgFile[sizeof($splImgFile)-1];
                        $sell->Update_sImg3($dataImg);
			
		}		

		if(isset($_FILES["adminfile5"]) && strlen($_FILES["adminfile5"]["name"]) > 0) {
			
			$sell->DeletePhoto5();
			$newname_Img5ID = LoadClientImg("adminfile5", 5);
			$sell->img4Id = $newname_Img5ID;
			$sell->UpdatePhoto5();
			
                        $sImgFile = RatioResizeImg300("../simg/".$sell->img4Id, "../simg/".$auto_create_path."/".uniqid("sell_"));
                        $splImgFile= split("\/", $sImgFile);
                        $dataImg["ID"] = $_GET["ID"];
                        $dataImg["sImg4ID"] = $auto_create_path."/".$splImgFile[sizeof($splImgFile)-1];
                        $sell->Update_sImg4($dataImg);
		}		
		
		resetcache();
	}
?>


<?  

  if(isset($_POST["updatetopparameters"]) && $_POST["updatetopparameters"]=="updatetopparameters") {
  	
  	$topparameters->objId = intval($_POST["objId"]);
  	$topparameters->typeId = 1;
  	$topparameters->categoryId = intval($_POST["categoryId"]);
  	$topparameters->par1 = intval($_POST["par1"]);
  	$topparameters->par2 = intval($_POST["par2"]);
  	$topparameters->par3 = intval($_POST["par3"]);
  	$topparameters->par4 = intval($_POST["par4"]);
  	$topparameters->par5 = (isset($_POST["par5"]) ?  1 : 0);
  	$topparameters->par6 = (isset($_POST["par6"]) ?  1 : 0);
  	$topparameters->par7 = intval($_POST["par7"]);
  	$topparameters->cityId = intval($_POST["cityId"]);
  	$topparameters->districtId = intval($_POST["districtId"]);
  	
    $topparameters->Update();  	
  	
    resetcache();
  }


  if(isset($_POST["autoupdate"])) {
  	
  		$status =  (isset($_POST["autoupdatestatus"]) ? 1 : 0);
		$sell->SetAutoUpdate($_POST["ID"], $status, $_POST["autoupdatetariff"], $_POST["autoupdatedatestart"]);  	
		resetcache();
		
  }
  
  if(isset($_POST["updatepayment"])) {
  	
  	$sell->SetStatusPaymentByAdmin(intval($_POST["ID"]), intval($_POST["pay_summ"]), intval($_POST["pay_status"]), $_POST["pay_datetime"], (isset($_POST["pay_datetime_auto"]) ? 1 : 0 ));
  	resetcache();
  }

  if (isset($_POST["update"])) {

    resetcache();
  	
  	
  	if (sizeof($_POST["StatusID"]) > 0) {
  		
            if (isset($_FILES["defaultUserIconFile"])) {

                if (is_uploaded_file($_FILES['defaultUserIconFile']['tmp_name'])) {

                    $filename = $_FILES['defaultUserIconFile']['tmp_name'];

                    $getsize = getimagesize($filename);

                    if($getsize[0] > 172 or $getsize[1] > 116) {
                            $arrayError[] = '����������-������ �� ���� ����������: ������ ������ ���� - 172 x 116';
                    } else {

                        $ext = substr($_FILES['defaultUserIconFile']['name'], 1 + strrpos($_FILES['defaultUserIconFile']['name'], "."));
                        move_uploaded_file($filename, "../sicon/".$_POST["ID"].".".$ext);

                        $dataUpdateIcon["ID"] = trim($_POST["ID"]);
                        $dataUpdateIcon["defaultUserIcon"] = $_POST["ID"].".".$ext;
                        $sell->Update_defaultUserIcon($dataUpdateIcon);
                    }
                }	
            }
  		 
            if (isset($_FILES["hotimg"])) {
                if (is_uploaded_file($_FILES['hotimg']['tmp_name'])) {
                    $filename = $_FILES['hotimg']['tmp_name'];
                    $ext = substr($_FILES['hotimg']['name'], 1 + strrpos($_FILES['hotimg']['name'], "."));
                    move_uploaded_file($filename, "../simg/hot".$_POST["ID"].".".$ext);

                    $hotIcon["ID"] = trim($_POST["ID"]);
                    $hotIcon["hotimg"] = "hot".$_POST["ID"].".".$ext;
                    $sell->Update_HotIcon($hotIcon);
                }	
            }  		
  		
                if(isset($_POST['pdfdrop'])) {
                    $sell->DeletePDF($_POST["ID"]);
                }
                
                $sell->UpdatePDF($_POST["ID"], $_FILES);

  		$data = array();
  		$data["ID"] = trim($_POST["ID"]);
  		$data["FML"] = ucwords(trim($_POST["FML"]));
  		$data["EmailID"] = trim($_POST["EmailID"]);
  		$data["Email2ID"] = trim($_POST["Email2ID"]);
  		
  		// up formating phone
  		if(isset($_POST["subRegionID"]) && $_POST["subRegionID"] == 1820) {
  			
			$data["PhoneID"] = phone_custom(trim($_POST["PhoneID"]));
  			
  		} else {
  			$data["PhoneID"] =  PhoneFix(trim($_POST["PhoneID"]));
  		}
  		// down formating phone
  		
  		$data["NameBizID"] = TextReplace("NameBizID", ReplaceSymbolToName(trim($_POST["NameBizID"])));
  		$data["subtitle"] = FixSubTitle(ReplaceSymbolToName(trim($_POST["subtitle"])));
  		if(substr($data["NameBizID"],-1)==".") {
  			$data["NameBizID"] = substr($data["NameBizID"], 0, strlen($data["NameBizID"]) - 1);
  		}
  		$aCategoryID = split("[:]",$_POST["CategoryID"]);
  		$data["CategoryID"] = $aCategoryID[0];
  		$data["SubCategoryID"] = $aCategoryID[1];
  		$data["BizFormID"] = trim($_POST["BizFormID"]);
  		$data["CityID"] = trim($_POST["CityID"]);
  		if(isset($_POST["subRegionID"])) {
  			$data["subRegionID"] = trim($_POST["subRegionID"]);
  		} else {
  			$data["subRegionID"] = "0";
  		}
  		$data["SiteID"] =  ReplaceSymbolToName(ParsingSiteID(trim($_POST["SiteID"])));
  		$data["CostID"] = ParsingMoney($_POST["CostID"]);
  		$data["cCostID"] = trim($_POST["cCostID"]);
  		$data["AgreementCostID"] = (strlen($_POST["AgreementCostID"])>0?1:0);
  		$data["Agent"] = (strlen($_POST["Agent"])>0?1:0);
  		$data["arendaStatusID"] = (strlen($_POST["arendaStatusID"])>0?1:0);
                $data["isPromo"] = (strlen($_POST["isPromo"])>0?1:0);
  		$data["torgStatusID"] = (strlen($_POST["torgStatusID"])>0?1:0);
  		$data["ProfitPerMonthID"] = ParsingMoney($_POST["ProfitPerMonthID"]);
  		$data["cProfitPerMonthID"] = trim($_POST["cProfitPerMonthID"]);
  		
  		$data["MaxProfitPerMonthID"] = ParsingMoney($_POST["MaxProfitPerMonthID"]);
                $data["cMaxProfitPerMonthID"] = trim($_POST["cMaxProfitPerMonthID"]);
         
  		$data["TimeInvestID"] = ParsingTimeInvest(trim($_POST["TimeInvestID"]));
  		$data["ListObjectID"] = TextReplace('ListObjectID', ParsingListObject(trim($_POST["ListObjectID"])));
  		$data["ContactID"] = trim($_POST["ContactID"]);
  		$data["MatPropertyID"] = TextReplace('MatPropertyID', trim($_POST["MatPropertyID"]));
  		$data["MonthAveTurnID"] =  ParsingMoney($_POST["MonthAveTurnID"]);
  		$data["cMonthAveTurnID"] = trim($_POST["cMonthAveTurnID"]);
  		$data["MaxMonthAveTurnID"] = ParsingMoney($_POST["MaxMonthAveTurnID"]);
  		$data["cMaxMonthAveTurnID"] = trim($_POST["cMaxMonthAveTurnID"]);
  		$data["MonthExpemseID"] = ParsingMoney($_POST["MonthExpemseID"]); 
  		$data["cMonthExpemseID"] = trim($_POST["cMonthExpemseID"]);
  		$data["SumDebtsID"] = ParsingMoney($_POST["SumDebtsID"]);
  		$data["cSumDebtsID"] = trim($_POST["cSumDebtsID"]);
  		$data["WageFundID"] = ParsingMoney($_POST["WageFundID"]);
  		$data["cWageFundID"] = trim($_POST["cWageFundID"]);
  		$data["ArendaCostID"] = ParsingMoney($_POST["ArendaCostID"]);
  		$data["cArendaCostID"] = trim($_POST["cArendaCostID"]);  		
  		$data["ArendaCommAl"] = (isset($_POST["ArendaCommAl"]) ? 1 : 0); 
  		$data["ObligationID"] = trim($_POST["ObligationID"]);
  		$data["CountEmplID"] = trim($_POST["CountEmplID"]);
  		$data["CountManEmplID"] = trim($_POST["CountManEmplID"]);
  		$data["TermBizID"] = ParsingTermBiz(trim($_POST["TermBizID"])); 
  		$data["DetailsID"] =  TextReplace('DetailsID', ParsingDetails(trim($_POST["DetailsID"])));
  		$data["typeSellID"] = intval($_POST["typeSellID"]);
  		
  		$data["DateDuration"] = trim($_POST["DateDuration"]);
  		$data["colorset"] = (strlen($_POST["colorset"])>0?1:0);
  		$data["colorsetdatestop"] = trim($_POST["colorsetdatestop"]);
  		$data["colorsetdatestart"] = trim($_POST["colorsetdatestart"]);
  		
  		$data["ShortDetailsID"] =  TextReplace('ShortDetailsID', ParsingShortDetails(trim($_POST["ShortDetailsID"])));
  		$data["HotShortDetailsID"] = $_POST["HotShortDetailsID"];
  		
  		//$data["ImgID"] = trim($_POST["ImgID"]);
  		$data["FaxID"] = PhoneFix(trim($_POST["FaxID"]));
  		$data["ShareSaleID"] = trim($_POST["ShareSaleID"]);
  		$data["maxShareSaleID"] = trim($_POST["maxShareSaleID"]);
  		$data["ReasonSellBizID"] = trim($_POST["ReasonSellBizID"]);
  		$data["TarifID"] = trim($_POST["TarifID"]);
  		$data["StatusID"] = trim($_POST["StatusID"]);
  		$data["Icon"] = trim($_POST["IconID"]);
  		$data["metroID"] = trim($_POST["metroID"]);
  		$data["districtID"] = trim($_POST["districtID"]);
  		$data["brokerId"] = trim($_POST["brokerId"]);
                
                $data["frannchising"] = (isset($_POST['frannchising']) ? 1 : 0);

		$data["youtubeURL"] = trim($_POST["youtubeURL"]);
		$data["youtubeShortURL"] = trim($_POST["youtubeShortURL"]);
		
		if(strlen($data["youtubeShortURL"]) <= 0 && strlen($data["youtubeURL"]) > 0) {
			
			$QUERYVAR= parse_url($data["youtubeURL"], PHP_URL_QUERY);
			$GETVARS = explode('&',$QUERYVAR);
			foreach($GETVARS as $string){
     			list($is,$what) = explode('=',$string);
     			if($is == "v") {
     				$data["youtubeShortURL"] = "http://www.youtube.com/v/".$what."?version=3";				
     				break;
     			}
			}			
			
		}
		
  		
  		if(isset($_POST["streetID"])) {
  			$data["streetID"] = trim($_POST["streetID"]);
  		} else {
  			$data["streetID"] = "0";
  		}

  		$data["latitude"] = trim($_POST["latitude"]);
  		$data["longitude"] = trim($_POST["longitude"]);

  		$data["position"] = (isset($_POST["position"]) ? 1 : 0);
  		$data["positiondatestart"] = trim($_POST["positiondatestart"]);
  		$data["positiondatestop"] = trim($_POST["positiondatestop"]);
  		
  		/*$data["realtymain"] = (isset($_POST["realtymain"]) ? 1 : 0);*/
  		
  		$data["leftblock"] = (isset($_POST["leftblock"]) ? 1 : 0);
  		$data["leftblockdatestart"] = trim($_POST["leftblockdatestart"]);
  		$data["leftblockdatestop"] = trim($_POST["leftblockdatestop"]);
  		
  		
  		$data["rightblock"] = (isset($_POST["rightblock"]) ? 1 : 0);
  		$data["rightblockdatestart"] = trim($_POST["rightblockdatestart"]);
  		$data["rightblockdatestop"] = trim($_POST["rightblockdatestop"]);
  		
  		
  		$data["viptopposition"] = (isset($_POST["viptopposition"]) ? 1 : 0);
  		$data["viptoppositiondatestart"] = trim($_POST["viptoppositiondatestart"]);
  		$data["viptoppositiondatestop"] = trim($_POST["viptoppositiondatestop"]);
  		
  		
  		$sell->ID =  intval($data["ID"]);
  		$sell->Update($data);
  		$sell->SetDistrictByMetro();
  		$sell->SetDistrictByStreet();
  		
  		$sell->UpdateGoogleCoord($data);
 		
  		if((int)$_POST["StatusID"] == 1) {
                    $templateEvent->EmailEvent(3,1, (int)$_POST["ID"]);
  		} else if (in_array((int)$_POST["StatusID"], array(10,12))) {
                    $templateEvent->EmailEvent(4,1, (int)$_POST["ID"]);
  		} else if ((int)$_POST["StatusID"] == 5) {
                    if($data['isPromo'] != 1) {
                        $templateEvent->EmailEvent(1,1, (int)$_POST["ID"]);
                    }
  		}
  		
  		
  		//up send email piter broker
  		if($_POST["StatusID"] == 5  or $_POST["StatusID"] == 1) { 
                    
                        $aPartnerSubscribe = $partnerSubscribe->Select();
                        
                        
                        
                        foreach($aPartnerSubscribe as $kps => $vps) {
                            if((int)$vps['statusId']) {
                                
                                $_cityIds = $vps['cityIds'];
                                
                                if(in_array($data["CityID"], $_cityIds) or in_array($data["subRegionID"], $_cityIds)) {
                           
                                    $sell->SendEmailToBroker(array(
                                        $vps['email'],
                                    ));                                    
                                    
                                } else {

                                    $aListCity = $partnerSubscribe->ListCity();
                                    
                                    foreach($aListCity as $k_ListCity => $v_ListCity) {
                                        if($v_ListCity['parent'] == $data["CityID"]) {
                                            
                                                                                        
                                            $_tempCity = $v_ListCity['id'];
                                            
                                            if(in_array($_tempCity, $_cityIds)) {
                                                
                                                //echo $vps['email'];
                                                
                                                $sell->SendEmailToBroker(array(
                                                    $vps['email'],
                                                ));                                    
                                            }
                                        }
                                    }
                                    
                                }
                            }
                        }
                        
                        /* 
  			if($data["CityID"] == 77 or $data["CityID"] == 50) { 
  				if(intval($data["brokerId"]) <= 0 && !isset($_POST["helpbroker"])) { 
  					$data["exportdefaulttrio"] = 1;
  					TrioExport($data);
  				}
  			}
                        */
  		}
  		//down send email piter broker
  		
  		/*up  Updating indexing for search*/

  		if($_POST["StatusID"] == 1 ||  $_POST["StatusID"] == 2) 
  		{
			include_once("./class.search.inc");
  			include_once("./class.stem.inc");
			$search = new search();
			$stemmer = new Lingua_Stem_Ru();
			$obj_indexing = new stdclass();
			$obj_indexing->ID = trim($_POST["ID"]);
			$obj_indexing->NameBizID = trim($_POST["NameBizID"]);
			$obj_indexing->ShortDetailsID = ParsingShortDetails(trim($_POST["ShortDetailsID"]));
			$obj_indexing->SiteID = ParsingSiteID(trim($_POST["SiteID"]));
			$obj_indexing->subtitle = ParsingSiteID(trim($_POST["subtitle"]));
			$sell_select[] = $obj_indexing;
			
			while(list($ks, $vs) = each($sell_select)) {	

				$word_indexing = array();
				$word_indexing_id = array();
				$word_indexing_count = array();
		
				$text = $vs->NameBizID;
				$text = ParsingIndexingSell($text);
		
				$ar = split("[ ]", $text);
				while(list($k, $v) = each($ar)) {
					if(trim(strlen($v)) <= 2) continue;
					$search->word = $stemmer->stem_word($v);
					$search->sound = soundex(ruslat($search->word));
					$res_id = $search->InsertIndexingWordSell();
					/*    */
					if(!isset($word_indexing[$search->word])) {
						$word_indexing[$search->word] = $res_id;
					}
					if(!isset($word_indexing_count[$search->word])) {
						$word_indexing_count[$search->word] = 1;
					} else {
						$word_indexing_count[$search->word] =  $word_indexing_count[$search->word] +1;
					}
					/*    */
				}

				$text = $vs->ShortDetailsID;
				$text = ParsingIndexingSell($text);
		
				$ar = split("[ ]", $text);
				while(list($k, $v) = each($ar)) {
					if(trim(strlen($v)) <= 2) continue;
					$search->word = $stemmer->stem_word($v);
					$search->sound = soundex(ruslat($search->word));
					$res_id = $search->InsertIndexingWordSell();
					/*    */
					if(!isset($word_indexing[$search->word])) {
						$word_indexing[$search->word] = $res_id;
					}
					if(!isset($word_indexing_count[$search->word])) {
						$word_indexing_count[$search->word] = 1;
					} else {
						$word_indexing_count[$search->word] =  $word_indexing_count[$search->word] +1;
					}
					/*    */
				}


				$text = $vs->SiteID;
				$text = ParsingIndexingSell($text);
		
				$ar = split("[ ]", $text);	
				while(list($k, $v) = each($ar)) {
					if(trim(strlen($v)) <= 2) continue;
					$search->word = $stemmer->stem_word($v);
					$search->sound = soundex(ruslat($search->word));
					$res_id = $search->InsertIndexingWordSell();
			
					/*    */
					if(!isset($word_indexing[$search->word])) {
						$word_indexing[$search->word] = $res_id;
					}
					if(!isset($word_indexing_count[$search->word])) {
						$word_indexing_count[$search->word] = 1;
					} else {
						$word_indexing_count[$search->word] =  $word_indexing_count[$search->word] +1;
					}
					/*    */
				}
		
				$text = $vs->subtitle;
				$text = ParsingIndexingSell($text);
		
				$ar = split("[ ]", $text);	
				while(list($k, $v) = each($ar)) {
					if(trim(strlen($v)) <= 2) continue;
					$search->word = $stemmer->stem_word($v);
					$search->sound = soundex(ruslat($search->word));
					$res_id = $search->InsertIndexingWordSell();
			
					/*    */
					if(!isset($word_indexing[$search->word])) {
						$word_indexing[$search->word] = $res_id;
					}
					if(!isset($word_indexing_count[$search->word])) {
						$word_indexing_count[$search->word] = 1;
					} else {
						$word_indexing_count[$search->word] =  $word_indexing_count[$search->word] +1;
					}
					/*    */
				}				
				
				reset($word_indexing);
		
				while (list($ik, $iv) = each($word_indexing)) {
					$s = new search();
					$s->sellId = $vs->ID;
					$s->wordId = $iv;
					$s->count = $word_indexing_count[$ik];
					$s->InsertIndexingIndexSell();
				}
			}
  		}
  		/*down  Updating indexing for search*/	

  		//$smarty->caching = false;
  		$smarty->cache_dir = "../cache";
  		$resclear = $smarty->clear_all_cache();
 		
  		if ($_POST["StatusID"] != 15 && $_POST["StatusID"] != 17) {
			if($_POST["CategoryID"] == "0") {
				$arrayError[] = "�� ������� ���������";
			}
			if($_POST["CityID"] == "0" && $_POST["subRegionID"] == "0") {
				$arrayError[] = "�� ������ �����/������";
			}
			if(strlen(trim($_POST["HotShortDetailsID"])) <= 0) {
				$arrayError[] = "�� ������� ������� �������� ��� �������� �����������";
			}
		}

  		
  		?>
  				<table bgcolor='#7AA4E7' width='100%' style="border:2px #cccccc solid;">
  					<tr>
  						<td align="center"><span style="color:#fff;font-weight:bold;font-family:verdana;" >������ ���������</span></td>
  					</tr>
				</table>  					
		<?
		
		if(trim($_POST["StatusID"]) != 15 && trim($_POST["StatusID"]) != 17) {
		 
  			$countCity = 0;

  			$countCity = $sell->CountPublicSellByCity($data["CityID"]);
  			
  			$d = array();
  			$d["Count"] = $countCity;
  			$d["ID"] = $data["CityID"];
  			$city->UpdateCount($d);


  			$countCategory = 0;

  			$countCategory = $sell->CountPublicSellByCategory($data["CategoryID"]);
  			
  			$d = array();
  			$d["Count"] = $countCategory;
  			$d["ID"] = $data["CategoryID"];
  			$category->UpdateCount($d);
		}	

  		if(trim($_POST["StatusID"]) == 1) {
  			$buyerSellSubscribe->sellID = trim($_POST["ID"]);
  			$buyerSellSubscribe->SellRegionID = trim($_POST["CityID"]);
  			$buyerSellSubscribe->SellCategoryID = $aCategoryID[0];
  			$buyerSellSubscribe->SellSubCategoryID = $aCategoryID[1];
  			$buyerSellSubscribe->InitSendmail();
  		}
  		
  	} else {
  		$data = array();
  		$data["StatusID"] = 5;
  		$data["ID"] = $_POST["ID"];
  		$sell->UpdateStatusID($data);
  	}


	/* up init google coordinate */
	//if($_POST["StatusID"] == 1 ||  $_POST["StatusID"] == 2 || $_POST["StatusID"] == 5) {
	
		
  		$sell->ID = intval($_POST["ID"]);
		$googlegeocode->address = $sell->GetAddressForGoogleMaps();
		$googlegeocode->serviceId = trim($_POST["ID"]);
		$googlegeocode->zoom = $_POST["zoomID"];
		$googlegeocode->Insert();
		if($googlegeocode->status == G_GEO_UNKNOWN_ADDRESS) {
			$googlegeocode->address = $sell->GetAddressForGoogleMaps(false, false);
			$googlegeocode->Insert();
		}
		if($googlegeocode->status == G_GEO_UNKNOWN_ADDRESS) {
			$googlegeocode->address = $sell->GetAddressForGoogleMaps(true);
			$googlegeocode->Insert();
		}
        
		
	//}   
	/* down init google coordinate */

  } 


  $data = array();
  $data["offset"] = 0;
  $data["rowCount"] = 0;
  $data["sort"] = "ID";

  $listCategory = $category->Select($data); 
  $smarty->assign("listCategory", $listCategory);
  
  $listCity = $city->SelectForAdmin($data); 
  $smarty->assign("listCity", $listCity);
  
  
  $listSubCategory = $subCategory->Select($data);
  $smarty->assign("listSubCategory", $listSubCategory);
  
  $pagesplit = 30;

  $companies = $company->ShortSelect();
  $smarty->assign("companies", $companies);
  
  if (isset($_GET["ID"])) {
  	 
  	
    $data["ID"] = $_GET["ID"];
    $result = $sell->GetItemAdmin($data);
    
    /*
    $whois->ip = $result->ip;
    if(isset($_SESSION[$whois->ip])) {
    	$whois_data = $_SESSION[$whois->ip];	
    } else {
        //echo "ok";
    	$whois_data = $whois->GetData();
    	$_SESSION[$whois->ip] =  $whois_data;	
    }
    */
    
    
    if(isset($whois_data)) {
        $smarty->assign("whois_city", $whois_data["city"]);
        $smarty->assign("whois_region", $whois_data["region"]);
    }
     
    
    // up ������� ������ ��� ������
    $size_img = array();
    $size_img1 = array();
    $size_img2 = array();
    
    if(intval($result->ImgID) > 0 && intval($result->sImgID) <= 0) {
    	$size_img = getimagesize("../simg/".$result->ImgID);
    }

    if(intval($result->Img1ID) > 0 && intval($result->sImg1ID) <= 0) {
    	$size_img1 = getimagesize("../simg/".$result->Img1ID);
    }    
    
    if(intval($result->Img2ID) > 0 && intval($result->sImg2ID) <= 0) {
    	$size_img2 = getimagesize("../simg/".$result->Img2ID);
    }    
    
    if(intval($result->Img3ID) > 0 && intval($result->sImg3ID) <= 0) {
    	$size_img3 = getimagesize("../simg/".$result->Img3ID);
    }     

    if(intval($result->Img4ID) > 0 && intval($result->sImg4ID) <= 0) {
    	$size_img4 = getimagesize("../simg/".$result->Img4ID);
    }     
    
    
    if(sizeof($size_img) > 0 && sizeof($size_img1) > 0 && sizeof($size_img2) > 0) {
    	if($size_img[0] < $size_img[1]) {
    		if($size_img1[0] > $size_img1[1]) {
    			$temp_img =  $result->ImgID;
    			$result->ImgID = $result->Img1ID;
    			$result->Img1ID = $temp_img;
    		} else if ($size_img2[0] > $size_img2[1]) {
    			$temp_img =  $result->ImgID;
    			$result->ImgID = $result->Img2ID;
    			$result->Img2ID = $temp_img;    			
    		}
    	}
    } else if (sizeof($size_img) <= 0 && sizeof($size_img1) > 0 && sizeof($size_img2) > 0) {

    	if($size_img1[0] > $size_img1[1]) {
    		$result->ImgID = $result->Img1ID;
    		$result->Img1ID = "";
    	} else if ($size_img2[0] > $size_img2[1]) {
    		$result->ImgID = $result->Img2ID;
    		$result->Img2ID = "";    		
    	}
    }
    

    // down ������� ������ ��� ������
	
    $create_defaultUserIcon = false;
    $create_defaultUserHotIcon = false;
    
    
    if(strlen(trim($result->ImgID)) > 0 && (intval($result->sImgID) <= 0 || strlen($result->hotimg) <= 1)) {

    	$dataImg["ID"] = $_GET["ID"];
    	$nImgFile = split("\.", $result->ImgID);
    	// up big photo resizing
    	$sImgFile = RatioResizeImg("../simg/".$result->ImgID, "../simg/".CreateSubDirImg().'/'.(time()+4));
    	$splImgFile= split("\/", $sImgFile);
    	$dataImg["sImgID"] = CreateSubDirImg().'/'.$splImgFile[sizeof($splImgFile)-1];
    	$sell->Update_sImg($dataImg);
    	// down big photo resizing
    	 
    	// up resizing photo icon on list
    	if(!$create_defaultUserIcon) {
            
            list($width, $height, $type, $attr) = getimagesize("../simg/".$result->ImgID);
              
            if($width > 0 && $height > 0) {
            
                $sImgFile = CreateDefaultUserIcon("../simg/".$result->ImgID, "../sicon/".$_GET["ID"]);    	
                $splImgFile= split("\/", $sImgFile);
                $dataImg["defaultUserIcon"] = $splImgFile[sizeof($splImgFile)-1];
                $sell->Update_defaultUserIcon($dataImg);
                $create_defaultUserIcon = true;

                $sHotImgFile = CreateDefaultHotIcon("../simg/".$result->ImgID, "../simg/hot".$_GET["ID"]);
                $splImgFile= split("\/", $sHotImgFile);
                $hotIcon["ID"] = trim($_GET["ID"]);
                $hotIcon["hotimg"] = $splImgFile[sizeof($splImgFile)-1];
                $sell->Update_HotIcon($hotIcon);    		
                $create_defaultUserHotIcon = true;
            }
    	}
    	// down resizing photo icon on list
    	
    	$sell->Update_ReplaceImg($result->ImgID, $_GET["ID"]);
    }

    if(strlen(trim($result->Img1ID)) > 0 && (intval($result->sImgID) <= 0 || strlen($result->hotimg) <= 1)) {

    	$dataImg["ID"] = $_GET["ID"];
    	$nImgFile = split("\.", $result->Img1ID);
    	// up big photo resizing
    	$sImgFile = RatioResizeImg300("../simg/".$result->Img1ID, "../simg/".CreateSubDirImg().'/'.(time()+5));
    	$splImgFile= split("\/", $sImgFile);
    	$dataImg["sImg1ID"] = CreateSubDirImg().'/'.$splImgFile[sizeof($splImgFile)-1];
    	$sell->Update_sImg1($dataImg);
    	// down big photo resizing
    	
    	// up resizing photo icon on list
    	if(!$create_defaultUserIcon) {
    		$sImgFile = CreateDefaultUserIcon("../simg/".$result->Img1ID, "../sicon/".$_GET["ID"]);    	
    		$splImgFile= split("\/", $sImgFile);
    		$dataImg["defaultUserIcon"] = $splImgFile[sizeof($splImgFile)-1];
    		$sell->Update_defaultUserIcon($dataImg);
    		$create_defaultUserIcon = true;
    		
    		$sHotImgFile = CreateDefaultHotIcon("../simg/".$result->Img1ID, "../simg/hot".$_GET["ID"]);
    		$splImgFile= split("\/", $sHotImgFile);
        	$hotIcon["ID"] = trim($_GET["ID"]);
    		$hotIcon["hotimg"] = $splImgFile[sizeof($splImgFile)-1];
    		$sell->Update_HotIcon($hotIcon);    		
    		$create_defaultUserHotIcon = true;

    	}
    	// down resizing photo icon on list    	
    	
    	$sell->Update_ReplaceImg1($result->Img1ID, $_GET["ID"]);
    } 
    
    if(strlen(trim($result->Img2ID)) > 0 && (intval($result->sImgID) <= 0 || strlen($result->hotimg) <= 1)) {

    	$dataImg["ID"] = $_GET["ID"];
    	$nImgFile = split("\.", $result->Img2ID);
    	// up big photo resizing
    	$sImgFile = RatioResizeImg300("../simg/".$result->Img2ID, "../simg/".CreateSubDirImg().'/'.(time()+6));
    	$splImgFile= split("\/", $sImgFile);
    	$dataImg["sImg2ID"] = CreateSubDirImg().'/'.$splImgFile[sizeof($splImgFile)-1];
    	$sell->Update_sImg2($dataImg);
    	// down big photo resizing
    	
    	// up resizing photo icon on list
    	if(!$create_defaultUserIcon) {
    		$sImgFile = CreateDefaultUserIcon("../simg/".$result->Img2ID, "../sicon/".$_GET["ID"]);    	
    		$splImgFile= split("\/", $sImgFile);
    		$dataImg["defaultUserIcon"] = $splImgFile[sizeof($splImgFile)-1];
    		$sell->Update_defaultUserIcon($dataImg);
    		$create_defaultUserIcon = true;
    		
    		$sHotImgFile = CreateDefaultHotIcon("../simg/".$result->Img2ID, "../simg/hot".$_GET["ID"]);
    		$splImgFile= split("\/", $sHotImgFile);
        	$hotIcon["ID"] = trim($_GET["ID"]);
    		$hotIcon["hotimg"] = $splImgFile[sizeof($splImgFile)-1];
    		$sell->Update_HotIcon($hotIcon);    		
    		$create_defaultUserHotIcon = true;
    	}
    	// down resizing photo icon on list     	
    	
    	$sell->Update_ReplaceImg2($result->Img2ID, $_GET["ID"]);
    }       
    
    if(strlen(trim($result->Img3ID)) > 0 && strlen($result->sImg3ID) <= 0) {
    	
    	$dataImg["ID"] = $_GET["ID"];
    	$nImgFile = split("\.", $result->Img3ID);
    	// up big photo resizing
    	$sImgFile = RatioResizeImg300("../simg/".$result->Img3ID, "../simg/".CreateSubDirImg().'/'.(time()+7));
    	$splImgFile= split("\/", $sImgFile);
    	$dataImg["sImg3ID"] = CreateSubDirImg().'/'.$splImgFile[sizeof($splImgFile)-1];
    	$sell->Update_sImg3($dataImg);
    	// down big photo resizing
    }
    
    if(strlen(trim($result->Img4ID)) > 0 && strlen($result->sImg4ID) <= 0) {
    	
    	$dataImg["ID"] = $_GET["ID"];
    	$nImgFile = split("\.", $result->Img4ID);
    	
    	// up big photo resizing
    	$sImgFile = RatioResizeImg300("../simg/".$result->Img4ID, "../simg/".CreateSubDirImg().'/'.(time()+8));
    	$splImgFile= split("\/", $sImgFile);
    	$dataImg["sImg4ID"] = CreateSubDirImg().'/'.$splImgFile[sizeof($splImgFile)-1];
    	$sell->Update_sImg4($dataImg);
    	// down big photo resizing
    }    
    
    
    $result->CostID = str_replace(" ", "", $result->CostID);
    $smarty->assign("data", $result);
    
    $googlegeocode->typeServiceId = 1;
    $googlegeocode->serviceId = intval($_GET["ID"]);
    
    $objgeocode = $googlegeocode->GetData();

    if(isset($objgeocode->zoom)) {
    	$smarty->assign("zoom", $objgeocode->zoom);
    } else {
    	$smarty->assign("zoom", 15);
    }

     $smarty->assign('listservices', $services->Select());
     $smarty->assign('listactions', $processingActions->SelectAdaptFormat(
                                        array('codeId' => (int)$_GET['ID']))
     );
     $smarty->assign('listactionstatus', ProcessingActions::$aStatus);

    
  } else {
    if (isset($_GET["offset"])) {
      $data["offset"] = intval($_GET["offset"]);
    } else {
      $data["offset"] = 0;
    }
    if (isset($_GET["rowCount"])) { 
      $data["rowCount"] = intval($_GET["rowCount"]);
    } else {
      $data["rowCount"] = $pagesplit;
    }

    
    
    if (isset($_GET["StatusID"]) && $_GET["StatusID"] != "-1") {
    	if ($_GET["StatusID"] != 100) {
    		$data["StatusID"] = $_GET["StatusID"];
    	} else {
    		$data["Overdue"] = "Overdue";
    	}
   	
    }
    
    
    if (isset($_GET["CategoryID"]) && $_GET["CategoryID"] != "-1") {
    	$data["CategoryID"] = $_GET["CategoryID"];
    }
    
    if (isset($_GET["CityID"]) && $_GET["CityID"] != "-1") {
    	$data["CityID"] = $_GET["CityID"];
    }

    if (isset($_GET["vipsettingID"]) && $_GET["vipsettingID"] != "-1") {
    	$data["vipsettingID"] = $_GET["vipsettingID"];
    }
    
    if(isset($_GET['brokerId'])) {
    	$data["brokerId"]  = (int) $_GET['brokerId'];
    }
    
    
    $result = $sell->InsideSelect($data);
    $smarty->assign("data", $result);
    
    
 }

  $aIcon = array();
  $dir = dir("../iconbiz/"); 
  while($file = $dir->read()) {
    $aIcon[] = $file;
  }
  
  sort($aIcon);
 
  $smarty->assign("icon", $aIcon);
 
  AssignDescription("../config.ini");

  $dataPage["offset"] = 0;
  $dataPage["rowCount"] = 0;
  $dataPage["sort"] = "ID";
  
  if (isset($_GET["StatusID"]) && $_GET["StatusID"] != "-1") {
  	if ($_GET["StatusID"] != 100) {
  		$dataPage["StatusID"] = $_GET["StatusID"];
  	} else {
  		$dataPage["Overdue"] = "Overdue";
  	}
  }
  if (isset($_GET["CategoryID"]) && $_GET["CategoryID"] != "-1") {
  	$dataPage["CategoryID"] = $_GET["CategoryID"];
  }
  if (isset($_GET["CityID"]) && $_GET["CityID"] != "-1") {
  	$dataPage["CityID"] = $_GET["CityID"];
  }
  
  if (isset($_GET["vipsettingID"]) && $_GET["vipsettingID"] != "-1") {
  	$dataPage["vipsettingID"] = $_GET["vipsettingID"];
  }
  
    if(isset($_GET['brokerId'])) {
    	$dataPage["brokerId"]  = (int) $_GET['brokerId'];
    }
  
  
  $resultPage = $sell->CountSelect($dataPage);

  
  $smarty->assign("CountRecord", $resultPage);
  $smarty->assign("CountSplit", $pagesplit);
  $smarty->assign("CountPage", ceil($resultPage/30));

  //$arrayError[]
  
  $smarty->assign("arrayError", $arrayError);

    if(isset($_GET['typeEventId']))  {
        $templateEvent->EmailEvent((int)$_GET['typeEventId'], 1, (int)$_GET["codeId"]);
        $smarty->assign('sendevent', 1);
    }  
  
  
  $smarty->display("./pagesplit.tpl", $_SERVER["REQUEST_URI"]);
 
  if (!isset($_GET["ID"])) {
  	$smarty->display("./selectsell.tpl");
  }

  $smarty->display("./sell/update.tpl");
  
  if (isset($_GET["ID"])) {
  	
  	if(intval($result->SubCategoryID)) {
  		
		$topparameters->objId = intval($_GET["ID"]);
		$topparameters->typeId = 1;
		$topparameters->categoryId = intval($result->SubCategoryID);
		$topparameters->cityId = (intval($result->subCityID) > 0 ? intval($result->subCityID) : intval($result->CityID));
		$topparameters->districtId = intval($result->districtID);
		$topprt = $topparameters->GetItem();
		
		//var_dump($topprt);
		
		$smarty->assign("districtId", $topparameters->districtId);
		$smarty->assign("cityId", $topparameters->cityId);
		$smarty->assign("topprt", $topprt);
  		$smarty->assign("categoryId", $result->SubCategoryID);
  		
  		$smarty->display("./topparameters/".intval($result->SubCategoryID).".tpl");
  	}
  	
  }
  
  if (!isset($_GET["ID"])) {
  	$smarty->display("./selectsell.tpl");
  }
  
  $smarty->display("./pagesplit.tpl", $_SERVER["REQUEST_URI"]);
  
?>
<script language="JavaScript">
<?
	$autoLoad->printScript();
	echo ";";
	$autoSubLoad->printScript();
	echo ";";
	$autoIcon->printScript();
	echo ";";
	$autoCategoryIcon->printScript();
?>
</script>
</body>
</html>
<?
	$sell->Close();
	$category->Close();
	$city->Close();
	$country->Close();
	//$buyer->Close();
	//$equipment->Close();
	//$investments->Close();
	$district->Close();
	$metro->Close();
	$streets->Close();
?>
<?
    //xdebug_stop_trace();
?>

<?
  $end = get_formatted_microtime(); 
  $total = $end - $start;
  echo "<center><span style='font-size:7pt;'>".round($total, 6)." : ".$count_sql_call." : ".$base_memory_usage." : ".memoryUsage(memory_get_usage(), $base_memory_usage)." </span><center>";
?>

