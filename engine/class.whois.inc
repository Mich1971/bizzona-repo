<?php

	class WhoIs {
		
		public $ip = "";
		private $xml_encoding = "windows-1251";
		private $in_charset = "UTF-8";
		private $out_charset = "windows-1251";
		
		
		function GetData() {

			
			$data = "<ipquery><fields><all/></fields><ip-list><ip>".$this->ip."</ip></ip-list></ipquery>";

			$ch = curl_init();

			curl_setopt($ch, CURLOPT_URL, "http://194.85.91.253:8090/geo/geo.html");
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_HEADER, false);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

			$xml = curl_exec($ch);
			curl_close($ch); 

			$xml = new SimpleXMLElement($xml);

			$GeoIP = array(
				'inetnum' => $this->ln_str_encode($xml->ip->inetnum, $this->in_charset, $this->out_charset),
				'city' => $this->ln_str_encode($xml->ip->city, $this->in_charset, $this->out_charset),
				'region' => $this->ln_str_encode($xml->ip->region, $this->in_charset, $this->out_charset),
				'district' => $this->ln_str_encode($xml->ip->district, $this->in_charset, $this->out_charset),
				'lat' => $this->ln_str_encode($xml->ip->lat, $this->in_charset, $this->out_charset),
				'lng' => $this->ln_str_encode($xml->ip->lng, $this->in_charset, $this->out_charset),
			);

			return $GeoIP; 
			
		}
		
		function ln_str_encode($str, $in_charset, $out_charset) {

			if (function_exists("mb_convert_encoding")) {
				return mb_convert_encoding($str, $this->out_charset, $this->in_charset);
			}  else {
				return iconv($this->in_charset, $this->out_charset, $str);
			}
		}
		
	}

?>