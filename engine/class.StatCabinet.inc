<?php
    include_once("class.db.inc");

    class StatCabinet {
        
        var $db = "";
        
        
    	function StatCabinet() {
       		global $DATABASE;
       		$this->InitDB();
    	} 

     	function InitDB() {
       		global $DATABASE;
       		$this->db = new DB($DATABASE["HOSTNAME"], $DATABASE["DATABASENAME"], $DATABASE["DATABASEUSER"], $DATABASE["DATABASEPASSWORD"]);
                $this->db->Select("SET @@lc_time_names='ru_RU';");
     	}

     	function Close() {
     		$this->db->Close();
     	}
        
        public function SelectStatUpdate($aParams) {
            
            $sql = "select 1 as 'count', date, DATE_FORMAT(`date`, '%Y-%m-%d') as 'shortdate', DATE_FORMAT(`date`, '%d %M %Y') as 'udate1', DATE_FORMAT(`date`, '%H:%i:%S') as 'udate2'  from HistoryAutoUpdate where typeId='".$aParams['typeId']."' and codeId=".$aParams['codeId']."  order by date asc;";
            $result = $this->db->Select($sql);

            $array = array();
            while ($row = mysql_fetch_object($result)) {
                $array[] = array( 
                    'count' => $row->count,
                    'date' => $row->date,
                    'udate1' => $row->udate1,
                    'udate2' => $row->udate2,
                    'shortdate' => $row->shortdate,
                );
            }

            return $array;
        }
        
        public function Select($aParams) {
 
            if($aParams['type_sql'] == 1) {
                
                $sql = "select count(DATE_FORMAT(FROM_UNIXTIME(`datetime`), '%d %M %Y')) as 'count', DATE_FORMAT(FROM_UNIXTIME(`datetime`), '%d %M %Y') as 'date', DATE_FORMAT(FROM_UNIXTIME(`datetime`), '%Y-%m-%d') as 'gdate' from statview where objectId=".(int)$aParams['objectId']." and typeId=".(int)$aParams['typeId']." group by date order by datetime desc limit ".(isset($aParams['count']) ? (int)$aParams['count'] : '30').";";
                $result = $this->db->Select($sql);
                $array = array();
                while ($row = mysql_fetch_object($result)) {
                    $array[] = array(
                        'count' => $row->count,
                        'date' => $row->date,
                        'gdate' => $row->gdate,
                    );
                }
                
                return $array;
                
            } else if ($aParams['type_sql'] == 2) {

                $sql = "select count(DATE_FORMAT(FROM_UNIXTIME(`datetime`), '%M %Y')) as 'count', DATE_FORMAT(FROM_UNIXTIME(`datetime`), '%M %Y') as 'date', DATE_FORMAT(FROM_UNIXTIME(`datetime`), '%Y-%m') as 'gdate' from statview where objectId=".(int)$aParams['objectId']." and typeId=".(int)$aParams['typeId']." group by date order by datetime desc;";
                
                $result = $this->db->Select($sql);
                $array = array();
                while ($row = mysql_fetch_object($result)) {
                    $array[] = array(
                        'count' => $row->count,
                        'date' => $row->date,
                        'gdate' => $row->gdate,
                    );
                }
                
                return $array;
            }
            
            return array();
        }
        
        private function createDateRangeArray($strDateFrom,$strDateTo) {
            
            // takes two dates formatted as YYYY-MM-DD and creates an
            // inclusive array of the dates between the from and to dates.

            // could test validity of dates here but I'm already doing
            // that in the main script

            $aryRange=array();

            $iDateFrom=mktime(1,0,0,substr($strDateFrom,5,2),     substr($strDateFrom,8,2),substr($strDateFrom,0,4));
            $iDateTo=mktime(1,0,0,substr($strDateTo,5,2),     substr($strDateTo,8,2),substr($strDateTo,0,4));

            if ($iDateTo>=$iDateFrom)
            {
                array_push($aryRange,date('Y-m-d',$iDateFrom)); // first entry
                while ($iDateFrom<$iDateTo)
                {
                    $iDateFrom+=86400; // add 24 hours
                    array_push($aryRange,date('Y-m-d',$iDateFrom));
                }
            }
            return $aryRange;
        }
        
        public function SelectStatUpdateExt($aParams) {
            
            
            $data = $this->SelectStatUpdate($aParams);

            if(count($data) <= 0)  return array();
            
            $periodData = $this->createDateRangeArray($data[0]['shortdate'], date('Y-m-d'));

            $aResult = array();
            
            foreach ($periodData as $k => $v) {
                
                foreach ($data as $k1 => $v1) {
                    
                    if($v ==  $v1['shortdate']) {
            
                        $aResult[] = array(
                            'count' => 2, //$v['count'],
                            'countperiod' => 1, 
                            'shortdate' => $v1['shortdate'],
                        );
                        
                    } 
                    
                }
                
                $aResult[] = array(
                    'count' => 0, //$v['count'],
                    'countperiod' => 1, 
                    'shortdate' => $v,
                );
                
            }
            
            return $aResult;
        }
        
        
    }

?> 