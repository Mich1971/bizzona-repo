<?
  global $smarty, $bizplan;
  AssignDescription("../config.ini");
// требуется авторизация
	require_once($_SERVER['DOCUMENT_ROOT'].'/inside/auth.php');
//

  include("../engine/FCKeditor/fckeditor.php");

  include_once("../engine/class.bizplan_company.inc");
  $bizplan_company = new BizPlanCompany();

  $companylist = $bizplan_company->SelectByStatus(OPEN_BIZPLAN_COMPANY);
  $smarty->assign("companylist", $companylist);

  $data = array();
  $data["offset"] = 0;
  $data["rowCount"] = 0;
  $data["sort"] = "Count";
  $result = $category->Select($data);
  $smarty->assign("categorylist", $result);



  if(isset($_POST) && sizeof($_POST) > 0)
  {
  	$bizplan->id = $_GET["ID"];
  	$bizplan->name = $_POST["name"];
  	$bizplan->description = $_POST["description"];
  	$bizplan->shortDescription = $_POST["shortDescription"];
    $bizplan->company_id = $_POST["company_id"];
    $bizplan->category_id = $_POST["category_id"];
    $bizplan->cost = str_replace(" ","",$_POST["cost"]);
    $bizplan->ccost = $_POST["ccost"];
    $bizplan->status_id = $_POST["status_id"];
    $bizplan->language_id = $_POST["language_id"];
    $bizplan->display_id = $_POST["display_id"];
    $bizplan->count_site = $_POST["count_site"];
    $bizplan->company_text = $_POST["company_text"];
  	$bizplan->Update();

	$smarty->cache_dir = "../cache";
	$resclear = $smarty->clear_all_cache();

  	$count_category = $bizplan->CountByCategory();
  	$count_category = $count_category + 1;
  	$d = array();
  	$d["CountP"] = $count_category;
  	$d["ID"] = $bizplan->category_id;
  	$category->UpdateCountP($d);

	//echo  $d["CountP"];

  	$count_company = $bizplan->CountByCompany();
	$bizplan_company->count = $count_company+1;
  	$bizplan_company->id = intval($_POST["company_id"]);
  	$bizplan_company->UpdateCount();

  }


  if(!isset($_GET["ID"])) {
  	$data = array();
  	$data["sort"] = "`bizplan`.`id`";
    $data["offset"] = 0;
    $data["rowCount"] = 0;

    $dataPage["offset"] = 0;
    $dataPage["rowCount"] = 0;
    $dataPage["sort"] = "id";
    $resultPage = $bizplan->Select($dataPage);

    $pagesplit = 40;

    $smarty->assign("CountRecord", sizeof($resultPage));
    $smarty->assign("CountSplit", $pagesplit);
    $smarty->assign("CountPage", ceil(sizeof($resultPage)/40));


    if (isset($_GET["offset"])) {
      $data["offset"] = intval($_GET["offset"]);
    } else {
      $data["offset"] = 0;
    }
    if (isset($_GET["rowCount"])) {
      $data["rowCount"] = intval($_GET["rowCount"]);
    } else {
      $data["rowCount"] = $pagesplit;
    }

	$result = $bizplan->Select($data);

	$smarty->assign("data", $result);
   	$smarty->display("./bizplan/pagesplitp.tpl", $_SERVER["REQUEST_URI"]);

	$smarty->display("./bizplan/bizplan_list.tpl");

   	$smarty->display("./bizplan/pagesplitp.tpl", $_SERVER["REQUEST_URI"]);
  } else {
  	$bizplan->id = intval($_GET["ID"]);
  	$result = $bizplan->GetItem();
  	$smarty->assign("data", $result);
  	$smarty->display("./bizplan/bizplan_edit.tpl");
  }

?>
