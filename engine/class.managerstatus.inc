<?php
    include_once("class.db.inc");  
    
    class ManagerStatus {
        
        var $db = "";        
        
        public function ManagerStatus() {
                global $DATABASE;
                $this->InitDB();
        }

        private function InitDB() {
                global $DATABASE;
                //print_r ($DATABASE);
                $this->db = new DB($DATABASE["HOSTNAME"], $DATABASE["DATABASENAME"], $DATABASE["DATABASEUSER"], $DATABASE["DATABASEPASSWORD"]);
        }

        public function UpdateStatus($aParams) {

            //statusId => 12 - продано 
            //statusId => 1 -  опубликовано 
            
            $statusId = $aParams['statusId'];
            $data = $aParams['data'];
            
            $sql = "update Sell set statusId=".(int)$statusId."  where ID in (".  implode(',', $data).")";
            $this->db->Sql($sql);
            
        }
        
        public function Cancel() {
            
            $data = $this->ListHistory();

            $Ids = array();
            foreach ($data as $k => $v) {
                $Ids[] = $v['ID'];
            }

            $aParams = array(
                'statusId' => 1,
                'data' => $Ids
            );
            
            $this->UpdateStatus($aParams);
            
            $this->ClearHistory();
            
            return $aParams['data'];
        }
        
        public function ClearHistory() {
            
            $sql = "delete from HistoryClosedAdmin;";
            $this->db->Sql($sql);
        }
        
        public function SaveHistory($aParams) {
            
            foreach ($aParams as $k => $v) {
                
                $sql = 'insert into HistoryClosedAdmin (`codeId`, `datetime`) values ('.(int)$v.', NOW());';
                $this->db->Insert($sql);
                
            }
            
        }
        
        public function ListHistory() {
            
            $sql = "select HistoryClosedAdmin.codeId, HistoryClosedAdmin.datetime, Sell.NameBizID from HistoryClosedAdmin, Sell where Sell.ID=HistoryClosedAdmin.codeId";

            $result = $this->db->Select($sql);
            
            $arr = array();
            
            while ($row = mysql_fetch_object($result)) {
                $arr[] = array(
                    'ID' => $row->codeId,  
                    'NameBizID' => $row->NameBizID,
                    'date' =>  strftime("%d %B %Y",  strtotime($row->datetime)),
                );
            }
        
            return $arr;
        }
        
        
        public function Search($aParams) {
            
            $where = " where 1 and Sell.statusId in (1,2) and Sell.date >= '".$aParams['startdatetime']."'  and Sell.date <= '".$aParams['stopdatetime']."' ";
            
            if(!$aParams['usebroker']) {
                
                $where .=  " and (brokerId = 0 or brokerId is null)"; 
                
            }
            
            $sql  = "select Sell.ID, Sell.NameBizID, Sell.date, Sell.brokerId, company.companyName from Sell left join company on (Sell.brokerId = company.Id)  {$where};";
            
            //echo $sql;
            
            $result = $this->db->Select($sql);
            
            $arr = array();
            
            while ($row = mysql_fetch_object($result)) {
                $arr[] = array(
                    'ID' => $row->ID,  
                    'NameBizID' => $row->NameBizID,
                    'date' =>  strftime("%d %B %Y",  strtotime($row->date)),
                    'brokerId' => $row->brokerId,
                    'companyName' => $row->companyName,
                );
            }
        
            return $arr;
            
        }
        
    }