<?php
	include_once("interface.sellsdapter.inc");
	include_once("../engine/class.sell.inc"); 
	include_once("../engine/class.mappingobjectbroker.inc"); 	
	
	function save_image($inPath,$outPath) { 
		
		 $in = fopen($inPath, "rb");     
		 $out =  fopen($outPath, "wb");     
		 while ($chunk = fread($in,8192))     
		 {         
		 	fwrite($out, $chunk, 8192);     
		 }     
		 fclose($in);     
		 fclose($out); 
	} 	
	
	final class NWE_ObjectId {
		
		/**
    		* ����������� (��������) � ������������ ����
    		* $apartments_in_high_rise_building int
    	*/
		const apartments_in_high_rise_building = 1;
		 
		/**
    		* ����������� � ����������� ����� ���������, ��������
    		* $apartments_in_low_rise_housing_complex int
    	*/
		const apartments_in_low_rise_housing_complex = 2;

		/**
    		* ��������������� ��� (�������, �����)
    		* $detached_house int
    	*/
		const detached_house = 3;
				
		/**
    		* ������� ������������ (��������, �����, �������)
    		* $elite_real_estate int
    	*/
		const elite_real_estate = 4;		
			
		/**
    		* ����, ��������
    		* $cafe_restoran int
    	*/
		const cafe_restoran = 5;		
		
		/**
    		* �������
    		* $store int
    	*/
		const store = 6;	
		
		/**
    		* ����
    		* $office int
    	*/
		const office = 7;

		/**
    		* ������������
    		* $production int
    	*/
		const production = 8;		
		
		/**
    		* �����, �������������
    		* $farm_farmland int
    	*/
		const farm_farmland = 9;	
		
		/**
    		* ��������� �������
    		* $land int
    	*/
		const land = 10;		
			
		/**
    		* �����, ���������
    		* $otel int
    	*/
		const otel = 12;	
		
		/**
    		* ���� ������������ ������������
    		* $other_commercial_real int
    	*/
		const other_commercial_real = 13;	

		/**
    		* ��������
    		* $penthouse int
    	*/
		const penthouse = 14;			
		
		static public $maps = array( NWE_ObjectId::apartments_in_high_rise_building => 110,
									 NWE_ObjectId::apartments_in_low_rise_housing_complex => 110,	
									 NWE_ObjectId::detached_house => 110,
									 NWE_ObjectId::elite_real_estate => 110,
									 NWE_ObjectId::cafe_restoran => 209,
									 NWE_ObjectId::store => 100,
									 NWE_ObjectId::office => 33,
									 NWE_ObjectId::production => 135,
									 NWE_ObjectId::farm_farmland => 103,
									 NWE_ObjectId::land => 54,
									 NWE_ObjectId::otel => 219,
									 NWE_ObjectId::other_commercial_real => 281
									 );
		
		
	}
	
    class NWE implements SellAdapter {
    	
    	public $id;
    	public $language_id;
    	public $name;
    	public $square_uch;
    	public $year;
    	public $rasp;
    	public $lift;
    	public $keller;
    	public $balkter;
    	public $garten;
    	public $komgostter;
    	public $komoxrana;
    	public $arenda;
    	public $arendagodtek;
    	public $object_id;
    	public $country_id;
    	public $district;
    	public $square;
    	public $type_id;
    	public $currency_id;
    	public $price;
    	public $placetype;
    	public $description;
    	public $images = array();
    	public $lastupdate;
    	
    	public $url = "http://nw-estate.ru/xmler/gate.xml?code=1mu2yd44ndzemmcy3wtud8l08";
    	//public $url = "http://nw-estate.ru/xmler/gate.xml?code=1mu2yd44ndzemmcy3wtxy8l08";
    	
    	private $array_nwe = array();
    	private $array_images_nwe = array();
    	
    	function __construct() {
    		
    	}
    	
    	public function Result() {
    		
    		return $this->array_nwe;
    		
    	}
    	
    	public function ResultImages() {
    		
    		return $this->array_images_nwe;
    		
    	}    	
    	
    	private function GetMargeCategory() {
    		
    		if(isset(NWE_ObjectId::$maps[$this->object_id])) {
    			return NWE_ObjectId::$maps[$this->object_id];
    		}
    		
    		return 0;
    	}
    	
    	private function GetMargeCity() {
    		
    		if($this->country_id == 283) {
    			return 3108;
    		}
    		return 0;
    	}
    	
    	private function GetMargeCurrency() {
    		
    		if(strtoupper($this->currency_id) == "USD") {
    			return "usd";
    		} else if (strtoupper($this->currency_id) == "EUR") {
    			return "eur";
    		} else if (strtoupper($this->currency_id) == "RUR") {
    			return "rub";
    		} else {
    			return "eur";
    		}
    		
    	}
    	
    	public function AddPdfContent($id) {

    		$arr = array();
    		
    		$pdf_name =  uniqid("nwe_").".pdf";
    		$source_name = "http://nw-estate.ru/pdf/".$id.".pdf";
			save_image($source_name, "../doc/".$pdf_name);

        	$content ="<p><a class=\"city_a\" style=\"FONT-SIZE: 10pt\" target=\"_blank\" href=\"http://www.bizzona.ru/doc/".$pdf_name."\">������� ��������� �������� (pdf)</a> &nbsp;<img src=\"http://www.bizzona.ru/images/pdf.gif\" /></p><p></p>"; 
    		
        	$arr["name"] = $pdf_name;
        	$arr["content"] = $content;
        	
    		return $arr;
    	}
    	
		private function ModerateContent($content) {
			
			$content = str_replace("\n\r","",$content);
			$content = str_replace("<br><br>","<br>",$content);
			$content = str_replace(" ?","&nbsp;&euro;",$content);
			$content = str_replace("������������:","",$content);
			$content = str_replace("��������������:","<b>��������������:</b>",$content);
			$content = str_replace("��� ������:","<b>��� ������:</b>",$content);
			$content = str_replace("�����:","<b>�����:</b>",$content);
			$content = str_replace("������:","<b>������:</b>",$content);
			$content = str_replace("����:","<b>����:</b>",$content);
			$content = str_replace("������:","<b>������:</b>",$content);
			$content = str_replace("���:","<b>���:</b>",$content);
			$content = str_replace("���������:","<b>���������:</b>",$content);
			$content = str_replace("��������� ����������� ����:","<b>��������� ����������� ����:</b>",$content);
			$content = str_replace("�������:","<b>�������:</b>",$content);
			$content = str_replace("������ ������ ����������:","<b>������ ������ ����������:</b>",$content);
			$content = str_replace("�������� �������:","<b>�������� �������:</b>",$content);
			$content = str_replace("���-�� ��������:","<b>���-�� ��������:</b>",$content);
			$content = str_replace("���-�� ������:","<b>���-�� ������:</b>",$content);
			$content = str_replace("���������� �� ���������:","<b>���������� �� ���������:</b>",$content);
			$content = str_replace("��� �������:","<b>��� �������:</b>",$content);
			$content = str_replace("��������� �������:","<b>��������� �������:</b>",$content);
			$content = str_replace("������������:","<b>������������:</b>",$content);
			$content = str_replace("����� �������:","<b>����� �������:</b>",$content);
			$content = str_replace("������� �������:","<b>������� �������:</b>",$content);
			$content = str_replace("����:","<b>����:</b>",$content);
			$content = str_replace("������� �������:","<b>������� �������:</b>",$content);
			$content = str_replace("��� ���������:","<b>��� ���������:</b>",$content);
			$content = str_replace("��� ������������:","<b>��� ������������:</b>",$content);
			$content = str_replace("������/�������:","<b>������/�������:</b>",$content);
			$content = str_replace("���� � ������:","<b>���� � ������:</b>",$content);
			$content = str_replace("����� � ��� �������:","<b>����� � ��� �������:</b>",$content);
			$content = str_replace("����� � ��� ������:","<b>����� � ��� ������:</b>",$content);
			$content = str_replace("����� ��� ������� ����� � ������:","<b>����� ��� ������� ����� � ������:</b>",$content);
			$content = str_replace("����� ��� ������ ����� � ������:","<b>����� ��� ������ ����� � ������:</b>",$content);
			$content = str_replace("���������� �������� ���������� 6% + ���.","<i>���������� �������� ���������� 6% + ���.</i>",$content);
			$content = str_replace("��� ����:","<b>��� ����:</b>",$content);
			$content = str_replace("���������� ����������� ����:","<b>���������� ����������� ����:</b>",$content);
			$content = str_replace("�������� ������������:","<b>�������� ������������:</b>",$content);
			$content = str_replace("����� ������:","<b>����� ������:</b>",$content);
			$content = str_replace("������������ ������:","<b>������������ ������:</b>",$content);
			$content = str_replace("���������� �������:","<b>���������� �������:</b>",$content);
			
			return $content;
		}
    	
    	public function DeleteFiles($Id) {
    		
    		$sell = new Sell();
    		$obj_files = $sell->GetFiles($Id);
    		
    		if(strlen($obj_files->ImgID) > 0) {
    			unlink("../simg/".$obj_files->ImgID);
    			//mail("eugenekurilov@gmail.com", "delete file ".$obj_files->ImgID." ".$Id, "delete file ".$obj_files->ImgID." ".$Id);
    		}
    		
    		if(strlen($obj_files->Img1ID) > 0) {
    			unlink("../simg/".$obj_files->Img1ID);
    		} 
    		
    		if(strlen($obj_files->Img2ID) > 0) {
    			unlink("../simg/".$obj_files->Img2ID);
    		}    		   		

    		if(strlen($obj_files->sImgID) > 0) {
    			unlink("../simg/".$obj_files->sImgID);
    		}

    		if(strlen($obj_files->sImg1ID) > 0) {
    			unlink("../simg/".$obj_files->sImg1ID);
    		}    		   		
    		
    		if(strlen($obj_files->sImg2ID) > 0) {
    			unlink("../simg/".$obj_files->sImg2ID);
    		}    		
    		
    		if(strlen($obj_files->pdf) > 0) {
    			unlink("../doc/".$obj_files->pdf);
    			//mail("eugenekurilov@gmail.com", "delete pdf file ".$obj_files->pdf." ".$Id, "delete file ".$obj_files->pdf." ".$Id);
    		}    		
    		
    	}
		
    	public function ConvertData() {

    		$data = array();
    		
			$data["FML"] = "North West Estate"; 
			$data["EmailID"] = "info@nw-estate.com";
			$data["PhoneID"] = "(495) 504-3784, (812) 309-5106";
			$data["NameBizID"] = $this->name;
			$data["BizFormID"] = "";
			$data["CityID"] = $this->GetMargeCity();
			$data["SiteID"] = $this->district;
			$data["CostID"] = $this->price;
			$data["cCostID"] = $this->GetMargeCurrency();
			$data["txtCostID"] = $this->price;
			$data["txtProfitPerMonthID"] = "";
			$data["ProfitPerMonthID"] = "";
			$data["cProfitPerMonthID"] = "";
			$data["cMaxProfitPerMonthID"] = "";
			$data["TimeInvestID"] = "";
			$data["ListObjectID"] = ""; 
			$data["ContactID"] = "";
			$data["MatPropertyID"] = "";
			$data["txtMonthAveTurnID"] = "";
			$data["MonthAveTurnID"] = "";
			$data["cMonthAveTurnID"] = "";
			$data["cMaxMonthAveTurnID"] = "";
			$data["txtMonthExpemseID"] = "";
			$data["MonthExpemseID"] = "";
			$data["cMonthExpemseID"] = "";
			$data["txtSumDebtsID"] = "";
			$data["SumDebtsID"] = "";
			$data["cSumDebtsID"] = "";
			$data["txtWageFundID"] = "";
			$data["WageFundID"] = "";
			$data["cWageFundID"] = "";
			$data["ObligationID"] = "";
			$data["CountEmplID"] = "";
			$data["CountManEmplID"] = "";
			$data["TermBizID"] = "";
			
			$this->description = $this->ModerateContent($this->description);
			
			$data["ShortDetailsID"] = $this->description;
			
			$pdf = $this->AddPdfContent($this->id);
			
			$data["DetailsID"] = $this->description." ".$pdf["content"];
			$data["pdf"] = $pdf["name"];
			
			$arr_images = $this->images;
			if(isset($arr_images[0])) {
			
				$fname_0 =  uniqid("nwe_").".jpg";
				save_image($arr_images[0], "../simg/".$fname_0);
				$data["ImgID"] = $fname_0;	
				
			} else {
				$data["ImgID"] = "";
			}
			
			if(isset($arr_images[1])) {
			
				$fname_1 =  uniqid("nwe_").".jpg";
				save_image($arr_images[1], "../simg/".$fname_1);
				$data["Img1ID"] = $fname_1;	
			} else {
				$data["Img1ID"] = "";
			}
			
			
			if(isset($arr_images[2])) {
			
				$fname_2 =  uniqid("nwe_").".jpg";
				save_image($arr_images[2], "../simg/".$fname_2);
				$data["Img2ID"] = $fname_2;	
			} else {
				$data["Img2ID"] = "";
			}
			
			$data["FaxID"] = ""; 
			$data["ShareSaleID"] = "";
			$data["ReasonSellBizID"] = "";
			$data["TarifID"] = ""; 
			$data["StatusID"] = "";
			$data["DataCreate"] = "";
			$data["DateStart"] = "";
			$data["TimeActive"] = "";
			$data["CategoryID"] = "";
			$data["SubCategoryID"] = $this->GetMargeCategory();
			$data["ConstGUID"] = "";
			$data["ip"] = "";
			$data["defaultUserCountry"] = "";
			$data["login"] = "";
			$data["password"] = "";
			$data["newspaper"] = "";
			$data["helpbroker"] = "";
			$data["youtubeURL"] = "";    		
    		$data["brokerId"] = "5";
    		return $data;
    	}
    	
    	
    	public function CallDataImages() {

			$xml_reader = new XMLReader();
			$xml_reader->open($this->url);
    		$xml_reader->setParserProperty(XMLReader::VALIDATE, false);	
    		
			while ($xml_reader->read()) {
	
				if ($xml_reader->name == "image") {

   	   				$arr = Array();
		
					while ($xml_reader->read()){
		
						if ($xml_reader->nodeType == XMLReader::ELEMENT){
				
							$key = $xml_reader->name;
				
						} elseif($xml_reader->nodeType != XMLReader::END_ELEMENT)  {
				
							$arr[$key] = $xml_reader->value;
				
						} else if($xml_reader->name == "image" && $xml_reader->nodeType == XMLReader::END_ELEMENT) {
				
							$this->ReadDataImagesCallBack($arr);
						}
					}		
					
					
				}
			}

    		$xml_reader->close();
    	}
    	
    	public function CallData() {
    		
			$xml_reader = new XMLReader();
			$xml_reader->open($this->url);
    		$xml_reader->setParserProperty(XMLReader::VALIDATE, false);	
    		
			while ($xml_reader->read()) {
	
				if ($xml_reader->name == "object") {

   	   				$arr = Array();
		
					while ($xml_reader->read()){
		
						if ($xml_reader->nodeType == XMLReader::ELEMENT){
				
							$key = $xml_reader->name;
				
						} elseif($xml_reader->nodeType != XMLReader::END_ELEMENT)  {
				
							$arr[$key] = $xml_reader->value;
				
						} else if($xml_reader->name == "object" && $xml_reader->nodeType == XMLReader::END_ELEMENT) {
				
							$this->ReadDataCallBack($arr);
						}
					}		
					
					
				}
			}

			$xml_reader->close();
    		
    	}
    	
		private function ReadDataImagesCallBack($arr) {

			$this->array_images_nwe[] = $arr["filename"];
	
		}    	
    	
		private function ReadDataCallBack($arr) {
 		
			$_nwe = new NWE(); 
	
    		$_nwe->id = (isset($arr["id"]) ? $arr["id"] : "");
    		$_nwe->language_id = (isset($arr["language_id"]) ? $arr["language_id"] : "");
    		$_nwe->name = (isset($arr["name"]) ? iconv('UTF-8','windows-1251', $arr["name"]) : "");
    		$_nwe->square_uch = (isset($arr["square_uch"]) ? $arr["square_uch"] : "");
    		$_nwe->year = (isset($arr["year"]) ? $arr["year"] : "");
    		$_nwe->rasp = (isset($arr["rasp"]) ? iconv('UTF-8','windows-1251', $arr["rasp"]) : "");
    		$_nwe->lift = (isset($arr["lift"]) ? $arr["lift"] : "");
    		$_nwe->keller = (isset($arr["keller"]) ? $arr["keller"] : "");
    		$_nwe->balkter = (isset($arr["balkter"]) ? $arr["balkter"] : "");
    		$_nwe->garten = (isset($arr["garten"]) ? $arr["garten"] : "");
    		$_nwe->komgostter = (isset($arr["komgostter"]) ? $arr["komgostter"] : "");
    		$_nwe->komoxrana = (isset($arr["komoxrana"]) ? $arr["komoxrana"] : "");
    		$_nwe->arenda = (isset($arr["arenda"]) ? $arr["arenda"] : "");
    		$_nwe->arendagodtek = (isset($arr["arendagodtek"]) ? $arr["arendagodtek"] : "");
    		$_nwe->object_id = (isset($arr["object_id"]) ? $arr["object_id"] : "");
    		$_nwe->country_id = (isset($arr["country_id"]) ? $arr["country_id"] : "");
    		$_nwe->district = (isset($arr["district"]) ? $arr["district"] : "");
    		$_nwe->square = (isset($arr["square"]) ? $arr["square"] : "");
    		$_nwe->type_id = (isset($arr["type_id"]) ? $arr["type_id"] : "");
    		$_nwe->currency_id = (isset($arr["currency_id"]) ? $arr["currency_id"] : "");
    		$_nwe->price = (isset($arr["price"]) ? $arr["price"] : ""); 
    		$_nwe->placetype = (isset($arr["placetype"]) ? $arr["placetype"] : "");
    		$_nwe->description = (isset($arr["description"]) ? iconv('UTF-8','windows-1251', $arr["description"]) : "");
    		$_nwe->images = (isset($arr["images"]) ? $arr["images"] : array());
    		$_nwe->lastupdate = (isset($arr["lastupdate"]) ? $arr["lastupdate"] : "");	
	
			$this->array_nwe[] = $_nwe;
	
		}
    	
    	public function ImportData() {
    		
    		$this->CallData();
    		$result = $this->Result();
    		
    		$this->CallDataImages();
    		$resultImages = $this->ResultImages();
    		
    		while (list($k, $v) = each($result)) {
    			
    			$arr_images = array();
    			
    			while (list($k1, $v1) = each($resultImages)) {

    				$arr_url = parse_url($v1);
    				parse_str($arr_url["query"],$res_get);
    				if(isset($res_get["id"]) && $res_get["id"] == $v->id) {
    					$arr_images[] = $v1;		
    				}
    			}
    			
    			$v->images = $arr_images;
    			$result[$k] = $v;
    			
    			reset($resultImages);
    		}
    		reset($result);

			    		
    		$sell = new Sell();	
			$mappingobjectbroker = new MappingObjectBroker();	

			while (list($k, $v) = each($result)) {

				$mappingobjectbroker->brokerObjectID = $v->id;
				$mappingobjectbroker->brokerId = 5;
				
				if(!$mappingobjectbroker->IsExistsBrokerObject()) {
							
					$mappingobjectbroker->bizzonaObjectId = $sell->NWE_Insert($v->ConvertData());
					$mappingobjectbroker->brokerId = 5;
					$mappingobjectbroker->brokerDateObject = HelpAdapter::unixToMySQL($v->lastupdate);
					$mappingobjectbroker->ObjectTypeId = 1;
					$mappingobjectbroker->Insert();						
					
				} 
				
			}
			
    	}
    	
    	public function UpdateData() {
    		
    		$this->CallData();
    		$result = $this->Result();

    		$this->CallDataImages();
    		$resultImages = $this->ResultImages();
    		
    		while (list($k, $v) = each($result)) {
    			
    			$arr_images = array();
    			
    			while (list($k1, $v1) = each($resultImages)) {

    				$arr_url = parse_url($v1);
    				parse_str($arr_url["query"],$res_get);
    				if(isset($res_get["id"]) && $res_get["id"] == $v->id) {
    					$arr_images[] = $v1;		
    				}
    			}
    			
    			$v->images = $arr_images;
    			$result[$k] = $v;
    			
    			reset($resultImages);
    		}
    		reset($result);    		
    		
    		$sell = new Sell();
    		
			$mappingobjectbroker = new MappingObjectBroker();
			$mappingobjectbroker->brokerId = 5;
			$mappingobjectbroker->ObjectTypeId = 1;
			$array_object = $mappingobjectbroker->Select();
			
			$iterr = 0;
			
			if ($iter < 1) {
				while (list($k, $v) = each($result)) {
				
					while (list($k1, $v1) = each($array_object)) {
					
						if ($v1["brokerObjectID"] == $v->id) {
							$v1_date = strtotime($v1["brokerDateObject"]);
							$v_date = strtotime(HelpAdapter::unixToMySQL($v->lastupdate));
						
							if($v1_date < $v_date) {

								$data = $v->ConvertData();
								$data["ID"] = $v1["bizzonaObjectId"];
							
								$this->DeleteFiles($data["ID"]);
							
								$sell->NWE_Update($data);
							
								$mappingobjectbroker->brokerDateObject = HelpAdapter::unixToMySQL($v->lastupdate);
								$mappingobjectbroker->brokerObjectID = $v1["brokerObjectID"];
								$mappingobjectbroker->UpdateBrokerDateObject(); 
							
								$iter = 1;
							}
							break;
						}
					}
					reset($array_object);
				}
    		}
    	}
    	
    	public function DeleteData() {
    		
    		$sell = new Sell();
    		$this->CallData();
    		$result = $this->Result();
    		
			$mappingobjectbroker = new MappingObjectBroker();
			$mappingobjectbroker->brokerId = 5;
			$mappingobjectbroker->ObjectTypeId = 1;
			$array_object = $mappingobjectbroker->Select(); 
			
			while (list($k, $v) = each($array_object)) {
				$status = false;	
				while (list($k1, $v1) = each($result)) {
					if($v["brokerObjectID"] == $v1->id) {
						$status = true;
						
					}
				}
				reset($result);
				
				if(!$status) {
			        //echo $v->bizzonaObjectId."<hr>";		
					$data = array();
					$data["StatusID"] = 12;
					$data["ID"] = $v["bizzonaObjectId"];
					$sell->NWE_UpdateStatusID($data);
					//mail("eugenekurilov@gmail.com", "need closed cache for ".$v->bizzonaObjectId." ", "need closed cache for ".$v->bizzonaObjectId." ");
				}
				
			}
    	}
    	
    }
	

?>