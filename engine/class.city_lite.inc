<?php

  include_once("class.db.inc");
  require_once("Cache/Lite.php");

  class City_Lite {
     var $db = "";
     var $id = "";
     var $subCityID = "";
     var $CityID = "";
     var $sql = "";
     var $aGetItem = array();
     var $aGetTitle = array();
     var $aSeoSellCity = array();
     var $aSeoEquipmentCity = array();

     var $aCountryIds = array(2536, 2537, 3075, 3531, 3519, 3104, 3105, 3106, 3107, 3701, 3122, 3108, 3109, 3110, 3111, 3120, 3119, 3112, 3113, 3114, 3115, 3121, 3122, 3116, 3117, 3118, 3676, 4330, 4419, 4507, 4542, 4605);
     
     function City_Lite() {
       global $DATABASE;
       $this->InitDB();
     } 

     function InitDB() {
       global $DATABASE;
       $this->db = new DB($DATABASE["HOSTNAME"], $DATABASE["DATABASENAME"], $DATABASE["DATABASEUSER"], $DATABASE["DATABASEPASSWORD"]);
     }

     function Close() {
     	$this->db->Close();
     }
     
     function Select($data) {
       if ($data["offset"] === 0 && $data["rowCount"] === 0) {
           $sql = "Select * from region where 1 ;";
       } else {
        // $sql = "Select * from region where 1 and parent in ('50','90') order  by ".$data["sort"]." limit ".intval($data["offset"]).",".$data["rowCount"].";";
        $sql = "Select * from region where 1  order  by ".$data["sort"]." limit ".intval($data["offset"]).",".$data["rowCount"].";";
       } 
       $result = $this->db->Select($sql);
       $array = Array();
       while ($row = mysql_fetch_object($result)) {
         $obj = new stdclass();
         $obj->ID = $row->ID;
         $obj->Name = $row->Name;
         $obj->title = convert_cyr_string($row->title,"koi8-r", "windows-1251");
         $array[] = $obj;
       }
       return $array;

     }

     function SelectCountryActive() {
     	
		$sql = " select region.ID, region.Name, Count(Sell.cityID) as 'Count', region.title  from region, Sell where 1 and Sell.StatusID in (1,2) and Sell.cityID=region.ID and region.ID in (".implode(",", $this->aCountryIds).")  group by Sell.cityID order  by Count desc;";     	
		
       	$result = $this->db->Select($sql);
       	$array = Array();
       	while ($row = mysql_fetch_object($result)) {
        	$obj = new stdclass();
         	$obj->ID = $row->ID;
         	$obj->Count = $row->Count;
         	$obj->title = convert_cyr_string($row->title,"koi8-r", "windows-1251");
         	$array[] = $obj;
       	}
       	return $array;
     }
     
     function SelectActive($data) {

       if(!isset($data["countryID"])) {
          $data["countryID"] = 0;
       }
     	
      	
       if ($data["offset"] === 0 && $data["rowCount"] === 0) {
         //$sql = "Select ID, Name, Count, title, seo, seobuy, seosell from region where 1 and ID in (77,78, 50, 23, 61, 66, 54, 2, 33, 16, 52, 55, 69, 40, 63, 42, 39, 36, 34, 67, 64, 57, 26, 71, 59, 38, 22, 35, 60, 37, 31, 43, 51, 56, 29, 27, 70, 46, 45, 44, 30, 68, 58, 49, 75, 74, 24, 76, 62, 72, 32, 48, 73, 47) and  Count > 0 and countryID=".$data["countryID"]."  order  by ".$data["sort"]." desc;";
         $sql = " select region.ID, region.Name, Count(Sell.cityID) as 'Count', region.title, region.seo, region.seobuy, region.seosell from region, Sell where 1 and Sell.StatusID in (1,2) and Sell.cityID=region.ID and region.ID in (77,78, 50, 23, 61, 66, 54, 2, 33, 16, 52, 55, 69, 40, 63, 42, 39, 36, 34, 67, 64, 57, 26, 71, 59, 38, 22, 35, 60, 37, 31, 43, 51, 56, 29, 27, 70, 46, 45, 44, 30, 68, 58, 49, 75, 74, 24, 76, 62, 72, 32, 48, 73, 47, 28, 53) and countryID=".$data["countryID"]."  group by Sell.cityID order  by ".$data["sort"]." desc;";
       } else {
         //$sql = "Select ID, Name, Count, title, seo, seobuy, seosell from region where 1 and ID in (77,78, 50, 23, 61, 66, 54, 2, 33, 16, 52, 55, 69, 40, 63, 42, 39, 36, 34, 67, 64, 57, 26, 71, 59, 38, 22, 35, 60, 37, 31, 43, 51, 56, 29, 27, 70, 46, 45, 44, 30, 68, 58, 49, 75, 74, 24, 76, 62, 72, 32, 48, 73, 47) and  Count > 0 and countryID=".$data["countryID"]." order  by ".$data["sort"]."  limit ".intval($data["offset"]).",".$data["rowCount"].";";
         $sql = "select region.ID, region.Name, Count(Sell.cityID) as 'Count', region.title, region.seo, region.seobuy, region.seosell from region, Sell where 1 and Sell.StatusID in (1,2) and Sell.cityID=region.ID and region.ID in (77,78, 50, 23, 61, 66, 54, 2, 33, 16, 52, 55, 69, 40, 63, 42, 39, 36, 34, 67, 64, 57, 26, 71, 59, 38, 22, 35, 60, 37, 31, 43, 51, 56, 29, 27, 70, 46, 45, 44, 30, 68, 58, 49, 75, 74, 24, 76, 62, 72, 32, 48, 73, 47, 28, 53) and countryID=".$data["countryID"]."  group by Sell.cityID order  by ".$data["sort"]." desc limit ".intval($data["offset"]).",".$data["rowCount"].";";
       } 

       //echo $sql;
       
       $result = $this->db->Select($sql);
       $array = Array();
       while ($row = mysql_fetch_object($result)) {
         $obj = new stdclass();
         $obj->ID = $row->ID;
         $obj->Name = $row->Name;
         $obj->Count = $row->Count;
         $obj->title = convert_cyr_string($row->title,"koi8-r", "windows-1251");
         $obj->seo = $row->seo;
         $obj->seobuy = $row->seobuy;
         $obj->seosell = $row->seosell;
         $array[] = $obj;
       }
       return $array;

     }

     function SelectChildActive($data) {

       if(!isset($data["countryID"])) {
          $data["countryID"] = 0;
       }
     	
       if ($data["offset"] === 0 && $data["rowCount"] === 0) {
         $sql = "select    
					count(Sell.subCityID) as 'Count',
        			region.title,
					region.seo,
					region.seobuy,
					region.seosell,
					region.Name,
					region.ID,
					region.url,
					region.parent as 'parentID',
					region.titleParentSelect
	    				from Sell, region 
							where Sell.StatusID in (1,2) 
									and 
		      					Sell.CityID=".intval($this->CityID)." 
									and 
		      					Sell.subCityID > 0  
									and
                      			region.ID = Sell.subCityID	
		        					and
                      			region.countryID = ".intval($data["countryID"])."
		      						GROUP BY Sell.subCityID ORDER BY title asc;";
       } else {
         $sql = "select    
					count(Sell.subCityID) as 'Count',
        			region.title,
					region.seo,
					region.seobuy,
					region.seosell,
					region.Name,
					region.ID,
					region.url,
					region.titleParentSelect
	    				from Sell, region 
							where Sell.StatusID in (1,2) 
									and 
		      					Sell.CityID=".intval($this->CityID)." 
									and 
		      					Sell.subCityID > 0  
									and
                      			region.ID = Sell.subCityID	
		        					and
                      			region.countryID = ".intval($data["countryID"])."
		      						GROUP BY Sell.subCityID ORDER BY title asc  limit ".intval($data["offset"]).",".intval($data["rowCount"]).";";
       } 
       
       $this->sql = $sql;
       
       $result = $this->db->Select($sql);
       $array = Array();
       while ($row = mysql_fetch_object($result)) {
         $obj = new stdclass();
         $obj->ID = $row->ID;
         $obj->city_url = $row->url;
         $obj->parentID = $row->parentID;
         $obj->Name = $row->Name;
         $obj->Count = $row->Count;
         $obj->title = convert_cyr_string($row->title,"koi8-r", "windows-1251");
         $obj->seo = $row->seo;
         $obj->seobuy = $row->seobuy;
         $obj->seosell = $row->seosell;
         $obj->titleParentSelect = $row->titleParentSelect;
         $array[] = $obj;
       }
       return $array;
     }

     function SelectChildActive2Buyer($data) {

       if(!isset($data["countryID"])) {
          $data["countryID"] = 0;
       }
     	
     	
       if ($data["offset"] === 0 && $data["rowCount"] === 0) {
         $sql = "select    
					count(buyer.subRegionID) as 'Count',
        			region.title,
					region.seo,
					region.seobuy,
					region.seosell,
					region.Name,
					region.ID,
					region.parent as 'parentID',
					region.titleParentSelect
	    				from buyer, region 
							where buyer.statusID in (1,2) 
									and 
		      					buyer.regionID=".intval($this->CityID)." 
									and 
		      					buyer.subRegionID > 0  
									and
                      			region.ID = buyer.subRegionID	
		        					and
                      			region.countryID = ".intval($data["countryID"])."
		      						GROUP BY buyer.subRegionID ORDER BY Count desc;";
       } else {
         $sql = "select    
					count(buyer.subRegionID) as 'Count',
        			region.title,
					region.seo,
					region.seobuy,
					region.seosell,
					region.Name,
					region.ID
					region.titleParentSelect
	    				from buyer, region 
							where buyer.statusID in (1,2) 
									and 
		      					buyer.regionID=".intval($this->CityID)." 
									and 
		      					buyer.subRegionID > 0  
									and
                      			region.ID = buyer.subRegionID	
		        					and
                      			region.countryID = ".intval($data["countryID"])."
		      						GROUP BY buyer.subRegionID ORDER BY Count desc  limit ".intval($data["offset"]).",".intval($data["rowCount"]).";";
       } 
       
       $this->sql = $sql;
       
       $result = $this->db->Select($sql);
       $array = Array();
       while ($row = mysql_fetch_object($result)) {
         $obj = new stdclass();
         $obj->ID = $row->ID;
         $obj->parentID = $row->parentID;
         $obj->Name = $row->Name;
         $obj->Count = $row->Count;
         $obj->title = convert_cyr_string($row->title,"koi8-r", "windows-1251");
         $obj->seo = $row->seo;
         $obj->seobuy = $row->seobuy;
         $obj->seosell = $row->seosell;
         $obj->titleParentSelect = $row->titleParentSelect;
         $array[] = $obj;
       }
       return $array;
     	
     }

     function SelectChildActive2Equipment($data) {

       if(!isset($data["countryID"])) {
          $data["countryID"] = 0;
       }
     	
     	
       if ($data["offset"] === 0 && $data["rowCount"] === 0) {
         $sql = "select    
					count(equipment.subCity_id) as 'Count',
        			region.title,
					region.seo,
					region.seobuy,
					region.seosell,
					region.Name,
					region.ID,
					region.parent as 'parentID',
					region.titleParentSelect
	    				from equipment, region 
							where equipment.status_id in (1,2) 
									and 
		      					equipment.city_id=".intval($this->CityID)." 
									and 
		      					equipment.subCity_id > 0  
									and
                      			region.ID = equipment.subCity_id	
		        					and
                      			region.countryID = ".intval($data["countryID"])."
		      						GROUP BY equipment.subCity_id ORDER BY Count desc;";
       } else {
         $sql = "select    
					count(equipment.subCity_id) as 'Count',
        			region.title,
					region.seo,
					region.seobuy,
					region.seosell,
					region.Name,
					region.ID
					region.titleParentSelect
	    				from equipment, region 
							where equipment.status_id in (1,2) 
									and 
		      					equipment.city_id=".intval($this->CityID)." 
									and 
		      					equipment.subCity_id > 0  
									and
                      			region.ID = equipment.subCity_id	
		        					and
                      			region.countryID = ".intval($data["countryID"])."
		      						GROUP BY equipment.subCity_id ORDER BY Count desc  limit ".intval($data["offset"]).",".intval($data["rowCount"]).";";
       } 
       
       $this->sql = $sql;
       
       $result = $this->db->Select($sql);
       $array = Array();
       while ($row = mysql_fetch_object($result)) {
         $obj = new stdclass();
         $obj->ID = $row->ID;
         $obj->parentID = $row->parentID;
         $obj->Name = $row->Name;
         $obj->Count = $row->Count;
         $obj->title = convert_cyr_string($row->title,"koi8-r", "windows-1251");
         $obj->seo = $row->seo;
         $obj->seobuy = $row->seobuy;
         $obj->seosell = $row->seosell;
         $obj->titleParentSelect = $row->titleParentSelect;
         $array[] = $obj;
       }
       return $array;
     	
     }
     
     function SelectChildActive2Investments($data) {

       if(!isset($data["countryID"])) {
          $data["countryID"] = 0;
       }
     	
       if ($data["offset"] === 0 && $data["rowCount"] === 0) {
         $sql = "select    
					count(investments.subRegion_id) as 'Count',
        			region.title,
					region.seo,
					region.seobuy,
					region.seosell,
					region.Name,
					region.ID,
					region.parent as 'parentID',
					region.titleParentSelect
	    				from investments, region 
							where investments.status_id in (1,2) 
									and 
		      					investments.region_id=".intval($this->CityID)." 
									and 
		      					investments.subRegion_id > 0  
									and
                      			region.ID = investments.subRegion_id	
		        					and
                      			region.countryID = ".intval($data["countryID"])."
		      						GROUP BY investments.subRegion_id ORDER BY Count desc;";
       } else {
         $sql = "select    
					count(investments.subRegion_id) as 'Count',
        			region.title,
					region.seo,
					region.seobuy,
					region.seosell,
					region.Name,
					region.ID
					region.titleParentSelect
	    				from investments, region 
							where investments.status_id in (1,2) 
									and 
		      					investments.region_id=".intval($this->CityID)." 
									and 
		      					investments.subRegion_id > 0  
									and
                      			region.ID = investments.subRegion_id	
		        					and
                      			region.countryID = ".intval($data["countryID"])."
		      						GROUP BY investments.subRegion_id ORDER BY Count desc  limit ".intval($data["offset"]).",".intval($data["rowCount"]).";";
       } 
       
       $this->sql = $sql;
       
       $result = $this->db->Select($sql);
       $array = Array();
       while ($row = mysql_fetch_object($result)) {
         $obj = new stdclass();
         $obj->ID = $row->ID;
         $obj->parentID = $row->parentID;
         $obj->Name = $row->Name;
         $obj->Count = $row->Count;
         $obj->title = convert_cyr_string($row->title,"koi8-r", "windows-1251");
         $obj->seo = $row->seo;
         $obj->seobuy = $row->seobuy;
         $obj->seosell = $row->seosell;
         $obj->titleParentSelect = $row->titleParentSelect;
         $array[] = $obj;
       }
       return $array;
     	
     }
     
     function SelectChildActive2Coop($data) {

       if(!isset($data["countryID"])) {
          $data["countryID"] = 0;
       }
     	
     	
       if ($data["offset"] === 0 && $data["rowCount"] === 0) {
         $sql = "select    
					count(coopbiz.subRegion_id) as 'Count',
        			region.title,
					region.seo,
					region.seobuy,
					region.seosell,
					region.Name,
					region.ID,
					region.parent as 'parentID',
					region.titleParentSelect
	    				from coopbiz, region 
							where coopbiz.status_id in (1,2) 
									and 
		      					coopbiz.region_id=".intval($this->CityID)." 
									and 
		      					coopbiz.subRegion_id > 0  
									and
                      			region.ID = coopbiz.subRegion_id	
		        					and
                      			region.countryID = ".intval($data["countryID"])."
		      						GROUP BY coopbiz.subRegion_id ORDER BY Count desc;";
       } else {
         $sql = "select    
					count(coopbiz.subRegion_id) as 'Count',
        			region.title,
					region.seo,
					region.seobuy,
					region.seosell,
					region.Name,
					region.ID
					region.titleParentSelect
	    				from coopbiz, region 
							where coopbiz.status_id in (1,2) 
									and 
		      					coopbiz.region_id=".intval($this->CityID)." 
									and 
		      					coopbiz.subRegion_id > 0  
									and
                      			region.ID = coopbiz.subRegion_id	
		        					and
                      			region.countryID = ".intval($data["countryID"])."
		      						GROUP BY coopbiz.subRegion_id ORDER BY Count desc  limit ".intval($data["offset"]).",".intval($data["rowCount"]).";";
       } 
       
       $this->sql = $sql;
       
       $result = $this->db->Select($sql);
       $array = Array();
       while ($row = mysql_fetch_object($result)) {
         $obj = new stdclass();
         $obj->ID = $row->ID;
         $obj->parentID = $row->parentID;
         $obj->Name = $row->Name;
         $obj->Count = $row->Count;
         $obj->title = convert_cyr_string($row->title,"koi8-r", "windows-1251");
         $obj->seo = $row->seo;
         $obj->seobuy = $row->seobuy;
         $obj->seosell = $row->seosell;
         $obj->titleParentSelect = $row->titleParentSelect;
         $array[] = $obj;
       }
       return $array;
     	
     }

     function SelectActiveB($data) {
     	
       if(!isset($data["countryID"])) {
          $data["countryID"] = 0;
       }
     	
       if ($data["offset"] === 0 && $data["rowCount"] === 0) {
         $sql = "Select ID, Name, CountB, title, seo, seobuy, seosell from region where 1 and ID in (77,78, 50, 23, 61, 66, 54, 2, 33, 16, 52, 55, 69, 40, 63, 42, 39, 36, 34, 67, 64, 57, 26, 71, 59, 38, 22, 35, 60, 37, 31, 43, 51, 56, 29, 27, 70, 46, 45, 44, 30, 68, 58, 49, 75, 74, 24, 76, 62, 72, 32, 48, 73, 47) and  CountB > 0 and countryID=".$data["countryID"]." order  by ".$data["sort"]." desc;";
       } else {
         $sql = "Select ID, Name, CountB, title, seo, seobuy, seosell from region where 1 and ID in (77,78, 50, 23, 61, 66, 54, 2, 33, 16, 52, 55, 69, 40, 63, 42, 39, 36, 34, 67, 64, 57, 26, 71, 59, 38, 22, 35, 60, 37, 31, 43, 51, 56, 29, 27, 70, 46, 45, 44, 30, 68, 58, 49, 75, 74, 24, 76, 62, 72, 32, 48, 73, 47) and  CountB > 0 and countryID=".$data["countryID"]." order  by ".$data["sort"]."  limit ".intval($data["offset"]).",".$data["rowCount"].";";
       } 
       $result = $this->db->Select($sql);
       $array = Array();
       while ($row = mysql_fetch_object($result)) {
         $obj = new stdclass();
         $obj->ID = $row->ID;
         $obj->Name = $row->Name;
         $obj->CountB = $row->CountB;
         $obj->title = convert_cyr_string($row->title,"koi8-r", "windows-1251");
         $obj->seo = $row->seo;
         $obj->seobuy = $row->seobuy;
         $obj->seosell = $row->seosell;
         $array[] = $obj;
       }
       return $array;
     }

     function SelectActiveE($data) {
     	
       if(!isset($data["countryID"])) {
          $data["countryID"] = 0;
       }
     	
       if ($data["offset"] === 0 && $data["rowCount"] === 0) {
         $sql = "Select ID, Name, CountE, title from region where 1 and  CountE > 0 and countryID=".$data["countryID"]."  and ID in (77,78, 50, 23, 61, 66, 54, 2, 33, 16, 52, 55, 69, 40, 63, 42, 39, 36, 34, 67, 64, 57, 26, 71, 59, 38, 22, 35, 60, 37, 31, 43, 51, 56, 29, 27, 70, 46, 45, 44, 30, 68, 58, 49, 75, 74, 24, 76, 62, 72, 32, 48, 73) order  by ".$data["sort"]." desc;";
       } else {
         $sql = "Select ID, Name, CountE, title from region where 1 and  CountE > 0 and countryID=".$data["countryID"]."  and ID in (77,78, 50, 23, 61, 66, 54, 2, 33, 16, 52, 55, 69, 40, 63, 42, 39, 36, 34, 67, 64, 57, 26, 71, 59, 38, 22, 35, 60, 37, 31, 43, 51, 56, 29, 27, 70, 46, 45, 44, 30, 68, 58, 49, 75, 74, 24, 76, 62, 72, 32, 48, 73) order  by ".$data["sort"]."  limit ".intval($data["offset"]).",".$data["rowCount"].";";
       } 
       $result = $this->db->Select($sql);
       $array = Array();
       while ($row = mysql_fetch_object($result)) {
         $obj = new stdclass();
         $obj->ID = $row->ID;
         $obj->Name = $row->Name;
         $obj->CountE = $row->CountE;
         $obj->title = convert_cyr_string($row->title,"koi8-r", "windows-1251");
         $array[] = $obj;
       }
       return $array;
     }

     function SelectActiveI($data) {
     	
       if(!isset($data["countryID"])) {
          $data["countryID"] = 0;
       }
     	
       if ($data["offset"] === 0 && $data["rowCount"] === 0) {
         $sql = "Select ID, Name, CountI, title from region where 1 and  CountI > 0  and ID in (77,78, 50, 23, 61, 66, 54, 2, 33, 16, 52, 55, 69, 40, 63, 42, 39, 36, 34, 67, 64, 57, 26, 71, 59, 38, 22, 35, 60, 37, 31, 43, 51, 56, 29, 27, 70, 46, 45, 44, 30, 68, 58, 49, 75, 74, 24, 76, 62, 72, 32, 48, 73, 47)  order  by ".$data["sort"]." desc;";
       } else {
         $sql = "Select ID, Name, CountI, title from region where 1 and  CountI > 0 and countryID=".$data["countryID"]."  and ID in (77,78, 50, 23, 61, 66, 54, 2, 33, 16, 52, 55, 69, 40, 63, 42, 39, 36, 34, 67, 64, 57, 26, 71, 59, 38, 22, 35, 60, 37, 31, 43, 51, 56, 29, 27, 70, 46, 45, 44, 30, 68, 58, 49, 75, 74, 24, 76, 62, 72, 32, 48, 73, 47)  order  by ".$data["sort"]."  limit ".intval($data["offset"]).",".$data["rowCount"].";";
       } 
       $this->sql = $sql;
       $result = $this->db->Select($sql);
       $array = Array();
       while ($row = mysql_fetch_object($result)) {
         $obj = new stdclass();
         $obj->ID = $row->ID;
         $obj->Name = $row->Name;
         $obj->CountI = $row->CountI;
         $obj->title = convert_cyr_string($row->title,"koi8-r", "windows-1251");
         $array[] = $obj;
       }
       return $array;
     }

     function SelectActiveC($data) {
     	
       if(!isset($data["countryID"])) {
          $data["countryID"] = 0;
       }
     	
       if ($data["offset"] === 0 && $data["rowCount"] === 0) {
         $sql = "Select ID, Name, CountC, title from region where 1 and  CountC > 0 and countryID=".$data["countryID"]." order  by ".$data["sort"]." desc;";
       } else {
         $sql = "Select ID, Name, CountC, title from region where 1 and  CountC > 0 and countryID=".$data["countryID"]." order  by ".$data["sort"]."  limit ".intval($data["offset"]).",".$data["rowCount"].";";
       } 
       $result = $this->db->Select($sql);
       $array = Array();
       while ($row = mysql_fetch_object($result)) {
         $obj = new stdclass();
         $obj->ID = $row->ID;
         $obj->Name = $row->Name;
         $obj->CountC = $row->CountC;
         $obj->title = convert_cyr_string($row->title,"koi8-r", "windows-1251");
         $array[] = $obj;
       }
       return $array;
     }

    function  GetSeoEquipmentCityList() {
    	
		$aSeoSell = array();
		
		$options = array(
    		'cacheDir' => PERSISTENTCACHE."/",
    		'lifeTime' => 2592000,
    		'pearErrorMode' => CACHE_LITE_ERROR_DIE
		);
		
		$cache = new Cache_Lite($options);
		
		if ($data = $cache->get('cityseoequipment')) {
			
			$aSeoSell = unserialize($data);

		} else { 
		
			$sql = "Select ID, seosellequipment from region;";
			$result = $this->db->Select($sql);
			while ($row = mysql_fetch_object($result)) {
				$aSeoSell[$row->ID] = $row->seosellequipment;
			}
			$cache->save(serialize($aSeoSell));
		}
		
		return 	$aSeoSell;    	
    	
    }     
     
    function  GetSeoSellCityList() {
    	
		$aSeoSell = array();
		
		$options = array(
    		'cacheDir' => PERSISTENTCACHE."/",
    		'lifeTime' => 2592000,
    		'pearErrorMode' => CACHE_LITE_ERROR_DIE
		);
		
		$cache = new Cache_Lite($options);
		
		if ($data = $cache->get('cityseosell')) {
			
			$aSeoSell = unserialize($data);

		} else { 
		
			$sql = "Select ID, seosell from region;";
			$result = $this->db->Select($sql);
			while ($row = mysql_fetch_object($result)) {
				$aSeoSell[$row->ID] = $row->seosell;
			}
			$cache->save(serialize($aSeoSell));
		}
		
		return 	$aSeoSell;    	
    	
    }

	function GetSeoEquipmentCity($id) {

		$SeoEquipmentCity = "";
		
		if(isset($this->aSeoEquipmentCity[$id])) {
			
			$SeoEquipmentCity = $this->aSeoEquipmentCity[$id];
			
		} else {
			
			$result = $this->GetSeoEquipmentCityList();
			$this->aSeoEquipmentCity[$id] = $result[$id];
			$SeoEquipmentCity = $this->aSeoEquipmentCity[$id];	
		}
		return $SeoEquipmentCity;

	}     
    
	function GetSeoSellCity($id) {

		$SeoSellCity = "";
		
		if(isset($this->aSeoSellCity[$id])) {
			
			$SeoSellCity = $this->aSeoSellCity[$id];
			
		} else {
			
			$result = $this->GetSeoSellCityList();
			$this->aSeoSellCity[$id] = $result[$id];
			$SeoSellCity = $this->aSeoSellCity[$id];	
		}
		return $SeoSellCity;
	}     
     
	function GetTitleList() {
		
		$aTitle = array();
		
		$options = array(
    		'cacheDir' => PERSISTENTCACHE."/",
    		'lifeTime' => 2592000,
    		'pearErrorMode' => CACHE_LITE_ERROR_DIE
		);
		
		$cache = new Cache_Lite($options);
		
		if ($data = $cache->get('citytitles')) {
			
			$aTitle = unserialize($data);

		} else { 
		
			$sql = "Select ID, title from region;";
			$result = $this->db->Select($sql);
			while ($row = mysql_fetch_object($result)) {
				$aTitle[$row->ID] = convert_cyr_string($row->title,"koi8-r", "windows-1251");
			}
			$cache->save(serialize($aTitle));
		}
		
		return 	$aTitle;	
	}
	
	function GetTitle($id) {

		$title = "";
		
		if(isset($this->aGetTitle[$id])) {
			
			$title = $this->aGetTitle[$id];
		} else {
			
			$result = $this->GetTitleList();
			$this->aGetTitle[$id] = $result[$id];
			$title = $result[$id];
			
		}
		return $title;

	}     

	function GetItem($data) {

		$obj = new stdclass();
		
		if(isset($this->aGetItem[$data["ID"]])) {
			$obj = $this->aGetItem[$data["ID"]];
		} else {
		
			$sql = "Select phone, phonetitle, yandexwidgetsell, titleyandexwidgetsell, ID, Name, seo, seobuy, seosell, seosellequipment, title, titleParentSelect, Count from region where ID=".trim($data["ID"]).";";
			$result = $this->db->Select($sql);
      
			while ($row = mysql_fetch_object($result)) {

				$obj->ID = $row->ID;
				$obj->Name = $row->Name;
				$obj->seo = $row->seo;
				$obj->seobuy = $row->seobuy;
				$obj->seosell = $row->seosell;	
				$obj->seosellequipment = $row->seosellequipment;
				$obj->title = convert_cyr_string($row->title,"koi8-r", "windows-1251");
				$obj->titleParentSelect = $row->titleParentSelect;	
				$obj->Count = $row->Count;
				$obj->yandexwidgetsell = $row->yandexwidgetsell;
				$obj->titleyandexwidgetsell = $row->titleyandexwidgetsell;
				$obj->phone = $row->phone;
				$obj->phonetitle = $row->phonetitle;
			} 
			$this->aGetItem[$data["ID"]] = $obj;
		}
		return $obj;

	}

     function GetSeoParent($parentID) {
     	
     	$sql = "select seo, seobuy, seosell from `region` where ID=".$parentID."";
     	$result = $this->db->Select($sql);
     	$obj = new stdclass();
     	while ($row = mysql_fetch_object($result)) {
     		$obj->seo = $row->seo;
     		$obj->seobuy = $row->seobuy;
     		$obj->seosell = $row->seosell;
     	}
     	return $obj;
     }

     function GetGeoData() {
     	$sql = "select latitude, longitude, zoom, statusGeoCode from region where region.ID=".intval($this->id)." ";
     	$result = $this->db->Select($sql);
     	$obj = new stdclass();
     	while ($row = mysql_fetch_object($result)) {
     		$obj->latitude = $row->latitude;
     		$obj->longitude = $row->longitude;
     		$obj->zoom = $row->zoom;
     		$obj->statusGeoCode = $row->statusGeoCode;
     	}
     	return $obj;
     	
     }
     
     function GetRegionbyParent($parentID, $useCount = true, $useID = true) {
     	$sql = "select ID, title, Count, seo, seobuy, seosell, url from `region` where 1 and  (parent=".$parentID." ".($useID ? " or ID=".$parentID."  )  " : " ) ")."  ".($useCount ? " and Count > 0" : "")." ORDER by ID asc";
     	$result = $this->db->Select($sql);
		$array = Array();
     	while ($row = mysql_fetch_object($result)) {
			$obj = new stdclass();
     		$obj->seo = $row->seo;
     		$obj->seobuy = $row->seobuy;
     		$obj->seosell = $row->seosell;
     		$obj->city_url = $row->url;
     		$obj->title = convert_cyr_string($row->title,"koi8-r", "windows-1251");
     		$obj->ID = $row->ID;
     		$obj->Count = $row->Count;
			$array[] = $obj;
     	}
     	return $array;
     	
     }
     
     function GetMainParent() {
     	$sql = "select ID, title, parent from `region` where 1 and  parent in (0, 90, 91, 92, 93, 94, 95, 96) ORDER by ID asc";
     	$result = $this->db->Select($sql);
		$array = Array();
     	while ($row = mysql_fetch_object($result)) {
			$obj = new stdclass();
     		$obj->title = convert_cyr_string($row->title,"koi8-r", "windows-1251");
     		$obj->ID = $row->ID;
     		$obj->parentID = $row->parent;
			$array[] = $obj;
     	}
     	return $array;
     	
     }     
     

     function GetRegionbyParentForBuyer($parentID) {
     	$sql = "select ID, title, CountB, seo, seobuy, seosell from `region` where 1 and  (parent=".$parentID." or ID=".$parentID.") and CountB > 0 ORDER by ID asc";
     	$result = $this->db->Select($sql);
		$array = Array();
     	while ($row = mysql_fetch_object($result)) {
			$obj = new stdclass();
     		$obj->seo = $row->seo;
     		$obj->seobuy = $row->seobuy;
     		$obj->seosell = $row->seosell;
     		$obj->title = convert_cyr_string($row->title,"koi8-r", "windows-1251");
     		$obj->ID = $row->ID;
     		$obj->CountB = $row->CountB;
			$array[] = $obj;
     	}
     	return $array;
     	
     }
     
     function GetTitleDistrict() {
     	$sql = "select titleDistrict from `region` where 1 and  ID=".intval($this->id)."";
     	$result = $this->db->Select($sql);
     	while ($row = mysql_fetch_object($result)) {
     		return $row->titleDistrict;
     	}
     }
  }
?>