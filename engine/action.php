<?
    ini_set("magic_quotes_gpc","off");

    global $DATABASE;

    include_once("../phpset.inc");
    include_once("./functions.inc");

    AssignDataBaseSetting("../config.ini");
// ��������� �����������
	require_once($_SERVER['DOCUMENT_ROOT'].'/inside/auth.php');
//

    ini_set("display_startup_errors", "on");
    ini_set("display_errors", "on");
    ini_set("register_globals", "on");

    include_once("./class.Action.inc");
    $action = new Action();

    include_once("./class.CategoryAction.inc");
    $categoryAction = new CategoryAction();

    require_once("../libs/Smarty.class.php");
    $smarty = new Smarty;
    $smarty->template_dir = "../templates";
    $smarty->compile_check = false;
    $smarty->caching = false;
    $smarty->compile_dir  = "../templates_c";
    $smarty->cache_dir = "../cache";
    $smarty->debugging = false;
    $smarty->clear_all_cache();

    function ReadingData(&$action)  {
        $action->_name = trim($_POST['_name']);
        $action->_prepayment = (isset($_POST['_prepayment']) ? 1 : 0);
        $action->_price = $_POST['_price'];
        $action->_statusId = (int)$_POST['_statusId'];
        $action->_categoryActionId = (int)$_POST['_categoryActionId'];
        $action->_duration = $_POST['_duration'];
    }

    if(isset($_POST['actionadd'])) {
        ReadingData($action);
        $_id = $action->Insert();
        if($_id > 0) {
            $smarty->assign("success", "�������� ���� ������� ���������");
        }
    }

     if(isset($_POST['actionupdate'])) {
         $action->_id = (int)$_GET['id'];
         ReadingData($action);
         $action->Update();
         $smarty->assign("success", "�������� ���� ������� ���������");
     }

    if(isset($_GET['id'])) {

        $action->_id = (int)$_GET['id'];
        $smarty->assign("item", $action->GetItem());
    }


    $smarty->assign("category", $categoryAction->Select());
    $smarty->assign("list", $action->Select());
    $smarty->assign("aStatus", Action::$_aStatusId);

?>
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <LINK href="../general.css" type="text/css" rel="stylesheet">
    <meta http-equiv="content-type" content="text/html; charset=windows-1251"/>
    <body>
<?
    $smarty->display("./mainmenu.tpl");
    $smarty->display("./action/success.tpl");
    $smarty->display("./action/list.tpl");
    $smarty->display("./action/add.tpl");
    $smarty->display("./action/help.tpl");
?>
</body>
</html>
<?
?>