<?php

    include_once("class.db.inc");

    class AdsAction {
    
        public $_db = null;
        public $_sql;
        
        public $_id;
        public $_subject;
        public $_body;
        
        
        function __construct() {
            global $DATABASE;
            $this->InitDB();
        }

        function InitDB() {

            global $DATABASE;

            $this->db = new DB($DATABASE["HOSTNAME"], $DATABASE["DATABASENAME"], $DATABASE["DATABASEUSER"], $DATABASE["DATABASEPASSWORD"]);
        }

        function __destruct() {

        }
        
        public function Insert() {
            
            $this->_sql = "insert into AdsAction (`subject`, `body`) values ('".$this->_subject."', '".$this->_body."')";
            $id = $this->db->Insert($this->_sql);
            return $id;
            
        }
        
        public function Update() {
            
            $this->_sql = "update AdsAction set `subject`='".$this->_subject."', `body`='".$this->_body."' where id=".(int)$this->_id." limit 1;";
            $result = $this->db->Sql($this->_sql);
        }

        public function ShortSelect() {
         
            $this->_sql = "select id, subject from AdsAction;";
            
            $result = $this->db->Select($this->_sql);

            $arr = array();	

            while ($row = mysql_fetch_object($result)) {

                    $aRow = array();
                    $aRow['id'] = $row->id;
                    $aRow['subject'] = $row->subject;
                    array_push($arr, $aRow);
            }

            return $arr;		
        }
        
        public function Select() {
         
            $this->_sql = "select * from AdsAction;";
            $result = $this->db->Select($this->_sql);

            $arr = array();	

            while ($row = mysql_fetch_object($result)) {

                    $aRow = array();
                    $aRow['id'] = $row->id;
                    $aRow['subject'] = $row->subject;
                    $aRow['body'] = $row->body;

                    array_push($arr, $aRow);
            }

            return $arr;		
        }
        
        public function GetItem() {
            
            $this->_sql = "select * from AdsAction where id=".(int)  $this->_id.";";
            $result = $this->db->Select($this->_sql);

            while ($row = mysql_fetch_object($result)) {

                    $aRow = array();
                    $aRow['id'] = $row->id;
                    $aRow['subject'] = $row->subject;
                    $aRow['body'] = $row->body;

                    return $aRow;
            }
            
            return array();
            
        }
        
    }

?>
