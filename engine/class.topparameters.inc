<?php
	include_once("class.db.inc");  

	class TopParameters	{

		public $db = "";
		public $sql = "";
		public $Id = 0;
		public $typeId = 0; // seller => 1; buyer => 2;
		public $categoryId = 0;
		public $objId = 0;
		public $cityId = 0;
		public $districtId = 0;
		
		public $par1 = 0; // ���������� ������ ��������� (int)
		public $par2 = 0; // ���������� ������� ���� ����� ������� (int)
		public $par3 = 0; // ������� ������������ ��������  (int)
		public $par4 = 0; // ����������������� (�����, ������ � ��� �����)  (int)
		public $par5 = 0; // ����� ���������� ������������� ������������� ������������ (int)
		public $par6 = 0; // ��� ������������ �������� (int)
		public $par7 = 0; // ������ �� ������ (int)
		 
		
		function __construct() {
			global $DATABASE;
			$this->InitDB();
		}

		function InitDB() {
			global $DATABASE;
			$this->db = new DB($DATABASE["HOSTNAME"], $DATABASE["DATABASENAME"], $DATABASE["DATABASEUSER"], $DATABASE["DATABASEPASSWORD"]);
		}

		function IsExists() {
			$this->sql = " select Id from TopParameters where typeId=".intval($this->typeId)." and categoryId=".intval($this->categoryId)." and objId=".intval($this->objId).";";
			$result = $this->db->Select($this->sql);
			$num_rows = mysql_num_rows($result);
			return ($num_rows > 0 ? true : false);
		}
		
		function Update() {
			
			if($this->IsExists()) {
				
				$this->sql = " update TopParameters set districtId=".intval($this->districtId).", cityId=".intval($this->cityId).", par1=".intval($this->par1).", par2=".intval($this->par2).", par3=".intval($this->par3).", par4=".intval($this->par4).", par5=".intval($this->par5).", par6=".intval($this->par6).", par7=".intval($this->par7)." where objId=".intval($this->objId)." and categoryId=".intval($this->categoryId)." and typeId=".intval($this->typeId)." ";
			
				$result = $this->db->Select($this->sql);
				
			} else {
				
				$this->sql = " insert into TopParameters (`cityId`, `districtId`, `par1`, `par2`, `par3`, `par4`, `par5`, `par6`, `par7`, `typeId`, `categoryId`, `objId`) values (".intval($this->cityId).", ".intval($this->districtId).",".intval($this->par1).",".intval($this->par2).",".intval($this->par3).",".intval($this->par4).",".intval($this->par5).",".intval($this->par6).",".intval($this->par7).",".intval($this->typeId).", ".intval($this->categoryId).", ".intval($this->objId).") ";
				
				$result = $this->db->Select($this->sql);
			}
			
		}
		
		function GetItem() {
			
			$this->sql = " select Id, cityId, districtId, par1, par2, par3, par4, par5, par6, par7, typeId, categoryId, objId from TopParameters where typeId=".intval($this->typeId)." and categoryId=".intval($this->categoryId)." and objId=".intval($this->objId)." ";
			
			$result = $this->db->Select($this->sql);
			
			$topparameters_obj = new TopParameters();
			
			while ($row = mysql_fetch_object($result)) {
			
				$topparameters_obj->Id = $row->Id;
				$topparameters_obj->par1 = $row->par1;
				$topparameters_obj->par2 = $row->par2;
				$topparameters_obj->par3 = $row->par3;
				$topparameters_obj->par4 = $row->par4;
				$topparameters_obj->par5 = $row->par5;
				$topparameters_obj->par6 = $row->par6;
				$topparameters_obj->par7 = $row->par7;
				$topparameters_obj->typeId = $row->typeId;
				$topparameters_obj->categoryId = $row->categoryId;
				$topparameters_obj->objId = $row->objId;
				$topparameters_obj->cityId = $row->cityId;
				$topparameters_obj->districtId = $row->districtId;
			}
			
			return $topparameters_obj;
		}
		
		function ExistsParameters($par = "par1") {
			
			//$this->sql = "select ".$par." as 'par', Count(".$par.") as 'count' from TopParameters where TopParameters.categoryId=".intval($this->categoryId)." ".(intval($this->cityId) > 0 ? "and cityId=".$this->cityId."" : "")." and typeId=".$this->typeId." group by ".$par." ;";	
			$this->sql = "select ".$par." as 'par', Count(".$par.") as 'count' from TopParameters, Sell where TopParameters.objId=Sell.Id and Sell.StatusID in (1,2) and TopParameters.categoryId=".intval($this->categoryId)." ".(intval($this->cityId) > 0 ? "and TopParameters.cityId=".$this->cityId."" : "")." and typeId=".$this->typeId." ".(intval($this->districtId) > 0 ? " and TopParameters.districtId=".intval($this->districtId)." " : "")." group by ".$par." ;";
			//echo $this->sql;
			$result = $this->db->Select($this->sql);
			$array = array();
			while ($row = mysql_fetch_object($result)) {
				$array[$row->par] = $row->count;
			} 
		    return $array;
		}
		
	}
?>