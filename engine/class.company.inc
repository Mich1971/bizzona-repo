<?php
  include_once("class.db.inc");  
    

  class Company {
     var $db = "";
     public $companyName = "";  // ������������ ��������
     public $shortDescription = ""; // ������� �������� ��������
     public $legalAddress = ""; // ����������� �����
     public $actualAddress = ""; // ����������� �����
     public $phoneCompany = ""; // �������
     public $faxCompany = ""; // ����
     public $email = "";
     public $innCompany = ""; // ���
     public $kppCompany = ""; // ���
     
     public $paystatus;
     
     public $nameBank = "";
     public $ksBank = "";
     public $rsBank = "";
     public $bikBank = "";
     
     public $login = "";
     public $password = "";
     
     public $tariffId = 0;
     
     public $Id = "";

     public $datestart = "";
     public $datestop = "";
     
     public $accessId = -1;
     
     public $show_in_partner;
     public $order_num;
     public $site_company;
     public $block_links; 
     public $logo_partner;
     public $logo_in_list;
     public $logo_in_sublist;
     public $prefix_name_company; 
     public $alt_seo_text;
     public $type_company;

     public $datestartStr;
     public $datestopStr;

     public $NameForSite;
     public $about_forsite;
     public $contact_forsite;
     public $service_forsite;

     // update parameters
     public $auto_update;
     public $auto_update_datestart;
     public $auto_update_datestop;
     public $auto_update_period;
     public $access_call_robot;
     public $url_import_robot;
     public $count_per_update;
                 
     function Company() {
       global $DATABASE;
       $this->InitDB();
     } 

     function InitDB() {
       global $DATABASE;
       $this->db = new DB($DATABASE["HOSTNAME"], $DATABASE["DATABASENAME"], $DATABASE["DATABASEUSER"], $DATABASE["DATABASEPASSWORD"]);
     }

     function Close() {
     	$this->db->Close();
     }
     
     
     function parseData() {

       $this->companyName  = addslashes($this->companyName);
       $this->shortDescription = addslashes($this->shortDescription);
       $this->legalAddress = addslashes($this->legalAddress);
       $this->actualAddress = addslashes($this->actualAddress);
       $this->phoneCompany = addslashes($this->phoneCompany);
       $this->faxCompany = addslashes($this->faxCompany);
       $this->innCompany = addslashes($this->innCompany);
       $this->kppCompany = addslashes($this->kppCompany);
       
       $this->nameBank = addslashes($this->nameBank);
       $this->ksBank = addslashes($this->ksBank);
       $this->rsBank = addslashes($this->rsBank);
       $this->bikBank = addslashes($this->bikBank);
       $this->email = addslashes($this->email);
       $this->auto_update_period = (int) $this->auto_update_period;
       $this->count_per_update = (int) $this->count_per_update;
     	    	
     }
     
     
     function Insert() {

         //echo "<h1>".$this->order_num."</h1>";
         
		$this->parseData();	   
     	
        $sql = "insert into company ( `companyName`,
       								 `shortDescription`,
       								 `legalAddress`,
       								 `actualAddress`,
       								 `phoneCompany`,
       								 `faxCompany`,
       								 `innCompany`,
       								 `kppCompany`,
       								 `nameBank`,
       								 `ksBank`,
       								 `rsBank`,
       								 `bikBank`,
       								 `email`,
                                                                 `login`,	
                                                                 `password`,
                                                                 `show_in_partner`,
                                                                 `order_num`,
                                                                 `site_company`,
                                                                 `block_links`,
                                                                 `logo_partner`,
                                                                 `logo_in_list`,
                                                                 `logo_in_sublist`,
                                                                 `prefix_name_company`, 
                                                                 `alt_seo_text`,
                                                                 `type_company`,
                                                                 `datestart`,
                                                                 `datestop`,
                                                                 `NameForSite`,
                                                                 `about_forsite`,
                                                                 `contact_forsite`,
                                                                 `service_forsite`,
                                                                 `auto_update`,
                                                                 `auto_update_datestart`,
                                                                 `auto_update_datestop`,
                                                                 `auto_update_period`,
                                                                 `access_call_robot`,
                                                                 `url_import_robot`,
                                                                 `count_per_update`
							   ) values (
       							      '".$this->companyName."',
       							      '".addslashes($this->shortDescription)."',
       							      '".$this->legalAddress."',
       							      '".$this->actualAddress."',
       							      '".$this->phoneCompany."',
       							      '".$this->faxCompany."',
       							      '".$this->innCompany."',
       							      '".$this->kppCompany."',
       							      '".$this->nameBank."',
       							      '".$this->ksBank."',
       							      '".$this->rsBank."', 
       							      '".$this->bikBank."',
       							      '".$this->email."',
       							      '".$this->login."',
       							      '".$this->password."',
       							      '".$this->show_in_partner."',
       							      '".$this->order_num."',
       							      '".$this->site_company."',
       							      '".addslashes($this->block_links)."',
       							      '".$this->logo_partner."',
       							      '".$this->logo_in_list."',
       							      '".$this->logo_in_sublist."',
       							      '".$this->prefix_name_company."',
       							       '".$this->alt_seo_text."',
       							       '".$this->type_company."', 
       							       '".$this->datestart."', 
       							       '".$this->datestop."',
       							       '".addslashes($this->NameForSite)."',
       							       '".addslashes($this->about_forsite)."',
       							       '".addslashes($this->contact_forsite)."',
       							       '".addslashes($this->service_forsite)."',
       							       '".$this->auto_update."',
       							       '".$this->auto_update_datestart."',
       							       '".$this->auto_update_datestop."',
       							       '".$this->auto_update_period."',
                                                               '".$this->access_call_robot."',
                                                               '".$this->url_import_robot."',
                                                               '".$this->count_per_update."'
       							   )";
       //echo $sql;
       $this->Id = $this->db->Insert($sql);
       return  $this->Id; 
     }

     
    function ShortSelect() {
		$sql = "select company.* from company order by accessId asc;";
		$result = $this->db->Select($sql);
		$array = Array(); 
		while ($row = mysql_fetch_object($result)) {
			$obj = new stdclass();
     		$obj->companyName = $row->companyName;
			$obj->Id = $row->Id;
			$obj->accessId = $row->accessId;
			$array[] = $obj;
		}
     	return $array;
     }

     function  SelectTariff() {
     	$sql = " select * from tariff";
     	
		$result = $this->db->Select($sql);
		$array = Array(); 
		while ($row = mysql_fetch_object($result)) {
			$obj = new stdclass();
     		$obj->Name = $row->Name;
    		$obj->Description = $row->Description;
			$obj->Id = $row->Id;
			$array[] = $obj;
		}
     	return $array;     	
     }
     
     function  ListCompanyInList() {
     	
     	$sql = ' select Id, 
     					logo_in_list, 
     					prefix_name_company,
     					logo_in_sublist, 
     					type_company,
     					shortDescription  
     					 from company; ';
     	$result = $this->db->Select($sql);
     	$array = Array(); 
		while ($row = mysql_fetch_object($result)) {
			
			if(strlen($row->logo_in_list) > 0) {
			
				$_a = array();
			
				$_a['Id'] = $row->Id;
				$_a['logo_in_list'] = $row->logo_in_list;
				$_a['logo_in_sublist'] = $row->logo_in_sublist;
				$_a['prefix_name_company'] = $row->prefix_name_company;
				$_a['type_company'] = $row->type_company;
				$_a['shortDescription'] = $row->shortDescription;
				
				$array[] = $_a;
			}
			
			
		}
		return $array;     	
     	
     }
     
     function ListPartner($type_company = 1) {
     	
     	$where = ' where show_in_partner=1  ';
     	if(intval($type_company) > 0) {
     		$where .= ' and type_company='.intval($type_company).' ';
     	}
     	
     	$sql = ' select Id, 
     					companyName, 
     					shortDescription, 
     					phoneCompany, 
     					site_company, 
     					block_links, 
     					logo_partner, 
     					logo_in_list, 
     					prefix_name_company,
     					alt_seo_text,
     					type_company    
     						from company '.$where.' order by order_num asc; ';
     	$result = $this->db->Select($sql);
     	$array = Array(); 
		while ($row = mysql_fetch_object($result)) {
			$obj = new stdclass();
			
			$obj->Id = $row->Id;
			$obj->companyName = $row->companyName;
			$obj->shortDescription = $row->shortDescription;
			$obj->phoneCompany = $row->phoneCompany;
			$obj->aPhoneCompany = explode(',', $row->phoneCompany);
			$obj->site_company = $row->site_company;
			$obj->block_links = $row->block_links;
			$obj->logo_partner = $row->logo_partner;
			$obj->logo_in_list = $row->logo_in_list;
			$obj->prefix_name_company = $row->prefix_name_company;
			$obj->alt_seo_text = $row->alt_seo_text;
			$obj->type_company = $row->type_company;
			
			$array[] = $obj;
		}
		return $array;     	
     }
     
     function Select() { 

     	$where = 'where 1';
     	$order = ' order by company.Id asc ';
     	
     	if($this->accessId != -1) {
     		$where .= ' and company.accessId ='. (int)$this->accessId. ' ';
     	}
     	
		$sql = "select company.*, datestop < NOW() as 'stopcompany', Services.shortName as 'tariffname' from company left join Services on (company.tariffId = Services.id) {$where} {$order};";
		
		//echo $sql;
		$result = $this->db->Select($sql);
		$array = Array(); 
		while ($row = mysql_fetch_object($result)) {
			$obj = new stdclass();
     		$obj->companyName = $row->companyName;
    		$obj->shortDescription = $row->shortDescription;
			$obj->legalAddress = $row->legalAddress;
			$obj->actualAddress = $row->actualAddress;
			$obj->phoneCompany = $row->phoneCompany;
			$obj->faxCompany = $row->faxCompany;
			$obj->innCompany = $row->innCompany;
			$obj->kppCompany = $row->kppCompany;
			$obj->nameBank = $row->nameBank;
			$obj->ksBank = $row->ksBank;
			$obj->rsBank = $row->rsBank;
			$obj->bikBank = $row->bikBank;
			$obj->tariffId = $row->tariffId;
			$obj->login = $row->login;
			$obj->password = $row->password;
			$obj->email = $row->email;
			$obj->accessId = $row->accessId;
			$obj->tariffname = $row->tariffname;
			 
			$obj->datestart = $row->datestart;
			$obj->datestop = $row->datestop;
			$obj->stopcompany = $row->stopcompany;
			$obj->auto_update = $row->auto_update;
			$obj->order_num = $row->order_num;
                        $obj->count_per_update = $row->count_per_update;
			
			$obj->datestartStr = strftime("%m/%d/%Y",  strtotime($row->datestart));
			$obj->datestopStr = strftime("%m/%d/%Y",  strtotime($row->datestop));
			
			$obj->Id = $row->Id;
			$array[] = $obj;
		}
     	return $array;
     }


     function Update() {
     	
     	$this->parseData();

        $sql = "update company set   `companyName`='".$this->companyName."', 
               						 `shortDescription`='".addslashes($this->shortDescription)."',
       								 `legalAddress`='".$this->legalAddress."',
       								 `actualAddress`='".$this->actualAddress."',
       								 `phoneCompany`='".$this->phoneCompany."',
       								 `faxCompany`='".$this->faxCompany."',
       								 `innCompany`='".$this->innCompany."',
       								 `kppCompany`='".$this->kppCompany."',
       								 `nameBank`='".$this->nameBank."',
       								 `ksBank`='".$this->ksBank."',
       								 `rsBank`='".$this->rsBank."',
       								 `email`='".$this->email."',
       								 `accessId`='".$this->accessId."',
       								 `tariffId`='".$this->tariffId."',
       								 `login`='".$this->login."',
       								 `password`='".$this->password."',
       								 `show_in_partner`='".$this->show_in_partner."',
       								 `site_company`='".$this->site_company."',
       								 `block_links`='".addslashes($this->block_links)."',
       								 `prefix_name_company`='".$this->prefix_name_company."',
       								 `alt_seo_text`='".$this->alt_seo_text."',  
       								 `type_company`='".$this->type_company."', 
       								 `datestart`='".$this->datestart."', 
       								 `datestop`='".$this->datestop."',
       								 `auto_update`='".$this->auto_update."',
       								 `auto_update_datestart`='".$this->auto_update_datestart."',
       								 `auto_update_datestop`='".$this->auto_update_datestop."',
       								 `auto_update_period`='".$this->auto_update_period."',
       								 `NameForSite`='".addslashes($this->NameForSite)."', 
       								 `about_forsite`='".addslashes($this->about_forsite)."', 
       								 `contact_forsite`='".addslashes($this->contact_forsite)."', 
       								 `service_forsite`='".addslashes($this->service_forsite)."', 
                                                                 `access_call_robot`='".$this->access_call_robot."',    
                                                                 `count_per_update`='".$this->count_per_update."',    
                                                                 `url_import_robot`='".addslashes($this->url_import_robot)."', 
       								  ".(strlen(trim($this->logo_partner)) > 0 ? " `logo_partner`='".$this->logo_partner."', " : "")."
       								 ".(strlen(trim($this->logo_in_list)) > 0 ? " `logo_in_list`='".$this->logo_in_list."', " : "")."
       								 ".(strlen(trim($this->logo_in_sublist)) > 0 ? " `logo_in_sublist`='".$this->logo_in_sublist."', " : "")."
       								 `bikBank`='".$this->bikBank."' where `Id`='".$this->Id."' limit 1;";
       								 
       	//echo $sql;
       	$this->db->Sql($sql);
     }


     function Delete() {
     }


     function GetItem() {
            $sql = "select company.* from company where Id='".$this->Id."';";
            //echo $sql;
            $result = $this->db->Select($sql);
            
            while ($row = mysql_fetch_object($result)) {
                
                $this->companyName = stripslashes($row->companyName);
                $this->shortDescription = stripslashes($row->shortDescription);
                $this->legalAddress = stripslashes($row->legalAddress);
                $this->actualAddress = stripslashes($row->actualAddress);
                $this->phoneCompany = stripslashes($row->phoneCompany);
                $this->faxCompany = stripslashes($row->faxCompany);
                $this->innCompany = stripslashes($row->innCompany);
                $this->kppCompany = stripslashes($row->kppCompany);
                $this->nameBank = stripslashes($row->nameBank);
                $this->ksBank = stripslashes($row->ksBank);
                $this->rsBank = stripslashes($row->rsBank);
                $this->bikBank = stripslashes($row->bikBank);
                $this->email = stripslashes($row->email);
                $this->accessId = stripslashes($row->accessId);
                $this->tariffId = stripslashes($row->tariffId);
                $this->order_num = stripslashes($row->order_num);
                $this->count_per_update = stripslashes($row->count_per_update);
                $this->show_in_partner = stripslashes($row->show_in_partner);
                $this->site_company = stripslashes($row->site_company);
                $this->block_links = stripslashes($row->block_links);
                $this->logo_partner = stripslashes($row->logo_partner);
                $this->logo_in_list = stripslashes($row->logo_in_list);
                $this->logo_in_sublist = stripslashes($row->logo_in_sublist);
                $this->prefix_name_company = stripslashes($row->prefix_name_company);
                $this->type_company = stripslashes($row->type_company);
                $this->login = stripslashes($row->login);
                $this->password = stripslashes($row->password);


                $this->NameForSite = $row->NameForSite;
                $this->about_forsite = $row->about_forsite;
                $this->contact_forsite = $row->contact_forsite;
                $this->service_forsite = $row->service_forsite;

                $this->datestartStr = strftime("%m/%d/%Y",  strtotime($row->datestart));
                $this->datestopStr = strftime("%m/%d/%Y",  strtotime($row->datestop));

                $this->auto_update_datestartStr = strftime("%m/%d/%Y",  strtotime($row->auto_update_datestart));
                $this->auto_update_datestopStr = strftime("%m/%d/%Y",  strtotime($row->auto_update_datestop));
                $this->auto_update = $row->auto_update;
                $this->auto_update_period = $row->auto_update_period;
                
                $this->access_call_robot = $row->access_call_robot;
                $this->url_import_robot = $row->url_import_robot;


            }
     }

     function GetAutoUpdatePeriod($brokerId) {
     	$sql = "select auto_update_period from company where Id=".(int)$brokerId." limit 1; ";
     	$result = $this->db->Select($sql);
     	while ($row = mysql_fetch_object($result)) {
     		return $row->auto_update_period;
     	}
     	return 14;
     }
     
     function getAccessCallRobot($brokerId) {
     	$sql =  "select access_call_robot from company where Id=".(int)$brokerId." limit 1";
     	$result = $this->db->Select($sql);
     	while ($row = mysql_fetch_object($result)) {
     		return $row->access_call_robot;
     	}
     	return 0;
     }

     function getUrlImportRobot($brokerId) {
     	$sql =  "select url_import_robot from company where Id=".(int)$brokerId." limit 1";
     	$result = $this->db->Select($sql);
     	while ($row = mysql_fetch_object($result)) {
            return $row->url_import_robot;
     	}
     	return '';
     }
     
     function GetItemAuth() {
		$sql = "select company.*, NOW() >  datestop  as 'paystatus' from company where login='".$this->login."' and password='".$this->password."';";
		//echo $sql;
		$result = $this->db->Select($sql);
		while ($row = mysql_fetch_object($result)) {
			$this->Id = $row->Id;
     		$this->companyName = $row->companyName;
    		$this->shortDescription = $row->shortDescription;
			$this->legalAddress = $row->legalAddress;
			$this->actualAddress = $row->actualAddress;
			$this->phoneCompany = $row->phoneCompany;
			$this->faxCompany = $row->faxCompany;
			$this->innCompany = $row->innCompany;
			$this->kppCompany = $row->kppCompany;
			$this->nameBank = $row->nameBank;
			$this->ksBank = $row->ksBank;
			$this->rsBank = $row->rsBank;
			$this->bikBank = $row->bikBank;
			$this->tariffId = $row->tariffId;
			$this->paystatus = $row->paystatus;
			$this->email = $row->email;
			$this->datestart = strftime("%d %B %Y",  strtotime($row->datestart));
			$this->datestop = strftime("%d %B %Y",  strtotime($row->datestop));
			$this->accessId = $row->accessId;
			$this->auto_update_period = $row->auto_update_period;
                        $this->access_call_robot = $row->access_call_robot;
                        $this->count_per_update = $row->count_per_update;
		}     	
     }
  
     function ChangeStatusObjects($aParams) {
         
         $sql = "update Sell set statusId=".$aParams['toStatusId']." where brokerId=".$aParams['brokerId']."  and  statusId=".$aParams['fromStatusId']."; ";
         $result = $this->db->UpdateAndResult($sql); 
         return $result;
     }
     
     function CountObject($aStatus = array(1)) {
     	
     	array_walk($aStatus, 'intval');
     	$sql = "select count(Id) as 'count'  from Sell where brokerId=".$this->Id." and  statusId in (".implode($aStatus).");";
     	$result = $this->db->Select($sql);
     	while ($row = mysql_fetch_object($result)) {
     		return (int)$row->count;
     	}
     	return 0;
     }
     
     function UpdateOrderNum($type) {
     	
     	$_order_num = 0;
     	
     	$sql = "select order_num from company where Id=".(int)$this->Id." limit 1;";
     	$result = $this->db->Select($sql);
     	
     	while ($row = mysql_fetch_object($result)) {
     		$_order_num = (int)$row->order_num;
     	}
     	
     	if($type == 'up') {
     		if($_order_num > 0) {
     			$_order_num -= 1;
     		}
     	} else if($type == 'down') {
     		$_order_num += 1;
     	}
     	
     	$sql = " update company set order_num={$_order_num} where id=".(int)$this->Id." limit 1;";
     	
     	$this->db->Sql($sql);
     }
     
    function GetMaxOrderNum() {
         
        $sql = "select max(order_num) as 'max' from company;";

        $result = $this->db->Select($sql);
     	while ($row = mysql_fetch_object($result)) {
            return $row->max;
     	}
     	return 0;
     }

    public function CustomUpdateOrderNum($aParams) {

        $sql = " update company set order_num=".$aParams['order_num']." where id=".$aParams['Id']." limit 1;";
        $this->db->Sql($sql);
        
    }
 
    public function GetContact() {

       $sql = "select phoneCompany, email from company where Id=".$this->Id.";";
       $result = $this->db->Select($sql);
       while ($row = mysql_fetch_object($result)) {
          return array(
              'email' => $row->email,
              'phone' => $row->phoneCompany,
          );
       } 
 
       return array(
          'email' => '',
          'phone' => '',
       );
    } 

    public function GetAccess() {
        
        $sql = "select accessId from company where Id=".$this->Id.";";
        $result = $this->db->Select($sql);
        
     	while ($row = mysql_fetch_object($result)) {
            return $row->accessId;
     	}
     	return 0;
        
    }
    
    public function GetListObjects($brokerId) {
        
        $sql = 'select ID from Sell where brokerId='.$brokerId.';';
        $result = $this->db->Select($sql);
        
        $array = array();
        
        while ($row = mysql_fetch_object($result)) {
            $array[] = $row->ID; 
        }
        return $array;
    }
    
    public function IdsBlockedCompany() {
        
        $sql = "select id from company where 1 and accessId=2;";
        $result = $this->db->Select($sql);

        $array = array();
        
        while ($row = mysql_fetch_object($result)) {
            $array[] = $row->id; 
        }
        return $array;
    }
    
  }
?>
