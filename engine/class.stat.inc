<?php

	include_once("class.db.inc");  

	class Stat {
		var $db = "";

		function Stat() {
			global $DATABASE;
			$this->InitDB();
		}

		function InitDB() {
			global $DATABASE;
			$this->db = new DB($DATABASE["HOSTNAME"], $DATABASE["DATABASENAME"], $DATABASE["DATABASEUSER"], $DATABASE["DATABASEPASSWORD"]);
		}

		function Profit() {
			$sql = "SELECT (SUM(summ)/100)/2 * 24 as 'income', MONTH(datepayment) as 'm', YEAR(datepayment) as 'y' FROM `payments` WHERE 1 = 1 group by YEAR(datepayment), MONTH(datepayment) order by datepayment desc;";

                        //echo $sql;
                        
			$result = $this->db->Select($sql);

			$array = Array();

			while ($row = mysql_fetch_object($result)) {

				$obj = new stdclass();
                                $obj->income = strrev(chunk_split (strrev(intval($row->income)), 3,' '));
				$obj->m = $row->m;
				$obj->y = $row->y;

				$array[] = $obj;
			} 
			return $array;
		}

		function ProfitToday() {
			$sql = "SELECT (SUM(summ)/100)/2 * 24 as 'income', 
							DATE_FORMAT(datepayment,'%Y-%m-%d') as 'date'
									FROM `payments` 
											WHERE 1 = 1
												and  
													DATE_FORMAT(datepayment,'%Y-%m-%d') = CURDATE()
											group by DATE_FORMAT(datepayment,'%Y-%m-%d');";

                        //echo $sql; 
                        
			$result = $this->db->Select($sql);

			$obj = new stdclass();

			while ($row = mysql_fetch_object($result)) {

				$obj->income = strrev(chunk_split (strrev(intval($row->income)), 3,' '));
				$obj->date = $row->date;

			} 
			return $obj;
		}
		
		
		function PercentPayment() {
 			$sql = "SELECT (SUM(pay_status)*100)/COUNT(*) as 'p', MONTH(DataCreate) as 'm', YEAR(DataCreate) as 'y' FROM `Sell` WHERE 1 group by YEAR(DataCreate), MONTH(DataCreate) order by DataCreate desc;";

                        //echo $sql;
                        
                        //$sql= "select 1;";
                        
			$result = $this->db->Select($sql);

			$array = Array();

			while ($row = mysql_fetch_object($result)) {

				$obj = new stdclass();
				$obj->p = $row->p;
				$obj->m = $row->m;
				$obj->y = $row->y;

				$array[] = $obj;
			} 
			return $array;

		}  

		function IncomeMoneyWallet() {
			$sql = " select sum(summ) as 'total', date, month(date) as 'm', year(date) as 'y' from Income where 1 group by  YEAR(date), MONTH(date); ";
			
                        //echo $sql;
                        
			$result = $this->db->Select($sql);
			$array = Array();

			while ($row = mysql_fetch_object($result)) {

				$obj = new stdclass();
				$obj->total = $row->total;
				$obj->date = date('Y-m',strtotime($row->date));
				$obj->m = $row->m;
				$obj->y = $row->y;

				$array[] = $obj;
			} 
			
			
			return $array;
		}
		

	}

?>