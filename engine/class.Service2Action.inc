<?php
    include_once("class.db.inc");

    class Service2Action {

        private $_db;
        private $_sql;

        public $_id;
        public $_serviceId;
        public $_actionId;
        public $_aAcionId = array();

        function __construct() {
            global $DATABASE;
            $this->InitDB();
        }

        function InitDB() {
            global $DATABASE;
            $this->db = new DB($DATABASE["HOSTNAME"], $DATABASE["DATABASENAME"], $DATABASE["DATABASEUSER"], $DATABASE["DATABASEPASSWORD"]);
        }

        function __destruct() {

        }

        public function Insert() {

            foreach($this->_aAcionId as $k => $v) {

                $this->_sql =  " insert into Service2Action (`serviceId`,`actionId`) values (".(int)$this->_serviceId.", ".(int)$v.") ";
                $this->db->Insert($this->_sql);
            }

        }

        public function Delete() {

            $this->_sql = " delete from Service2Action where serviceId=".(int)$this->_serviceId."";
            $this->db->Sql($this->_sql);

        }

        public function GeActions() {

            $this->_sql = " select Service2Action.actionId, Action.duration from Service2Action, Action where Action.id=Service2Action.actionId and  Service2Action.serviceId=".(int)$this->_serviceId."";
            $result = $this->db->Select($this->_sql);
            $arr = array();
            while ($row = mysql_fetch_object($result)) {
                $arr[] = array( 'actionId' => $row->actionId,
                                'duration' => $row->duration,
                );
            }
            return $arr;
        }

    }
?>