<?
    ini_set("magic_quotes_gpc","off");

    global $DATABASE;

    include_once("../phpset.inc");
    include_once("./functions.inc"); 
    require_once("./Cache/Lite.php");
 
    AssignDataBaseSetting("../config.ini");
// ��������� �����������
	require_once($_SERVER['DOCUMENT_ROOT'].'/inside/auth.php');
//

    include_once("./class.managerstatus.inc"); 
    $_managerStatus = new ManagerStatus();
    
    
    require_once("../libs/Smarty.class.php");
    $smarty = new Smarty;
    $smarty->template_dir = "../templates";
    $smarty->compile_check = false;
    $smarty->caching = false;
    $smarty->compile_dir  = "../templates_c";
    $smarty->cache_dir = "../cache";
    $smarty->debugging = false;
    $smarty->clear_all_cache();

    $arrayError = array();
    
    function resetcache($data) {

        global $smarty;

        foreach($data as $k => $v) {
        
            // up update cache sell
            $smarty->cache_dir = "../sellcache";
            $useMapGoogle = true;
            $smarty->clear_cache("./site/detailsSell.tpl", intval($v)."-".$useMapGoogle);
            $useMapGoogle = false;
            $smarty->clear_cache("./site/detailsSell.tpl", intval($v)."-".$useMapGoogle);
            $smarty->cache_dir = "../cache";

            $options = array(
                    'cacheDir' => "../sellcache/",
                    'lifeTime' => 1
            );

            $cache = new Cache_Lite($options);

            $cache->remove(''.intval($v).'____sellitem');
            // up update cache sell
        
        }
    }    
    
    
    if(count($_POST) && isset($_POST['change']) && isset($_POST['sel'])) {
        
        $_sel = array_keys($_POST['sel']);

        if(count($_sel)) {
            $_managerStatus->SaveHistory($_sel);
            
            $_managerStatus->UpdateStatus(array(
                'statusId' => 12, 
                'data' => $_sel
            ));
            
            resetcache($_sel);
        }
    } 

    if(isset($_POST['cancel'])) {
        $data = $_managerStatus->Cancel();
        resetcache($data);
    }
    
    
    if(isset($_POST['clear'])) {
        $_managerStatus->ClearHistory();
    }
    
    if(count($_POST) && isset($_POST['search'])) {
        
        $usebroker = isset($_POST['usebroker']) ? true : false;

        $startdatetime = trim($_POST['startdatetime']);
        $stopdatetime = trim($_POST['stopdatetime']);
        
        if(strlen($startdatetime) <= 0 or strlen($stopdatetime) <= 0) {
            
            $arrayError[] = "���������� ������� ��������� ������ ����������: c - ��";
            $smarty->assign("arrayError", $arrayError);
            
        } else {
            
            $_startdatetime = split('/', $startdatetime);
            $_stopdatetime = split('/', $stopdatetime);
            
            $aParams['startdatetime'] = $_startdatetime[2].'-'.$_startdatetime[0].'-'.$_startdatetime[1];
            $aParams['stopdatetime'] = $_stopdatetime[2].'-'.$_stopdatetime[0].'-'.$_stopdatetime[1];
            
            $aParams['usebroker'] = ($usebroker ? true : false);
            
            
            $list = $_managerStatus->Search($aParams);
            $smarty->assign("list", $list);
            
        }
        
    }

    $listHistory = $_managerStatus->ListHistory();
    $smarty->assign("listhistory", $listHistory);
    
?>
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
            <LINK href="../general.css" type="text/css" rel="stylesheet">
            <meta http-equiv="content-type" content="text/html; charset=windows-1251"/> 
            <body>
<?
                $smarty->display("./mainmenu.tpl");
                $smarty->display("./managerstatus/error.tpl");
                $smarty->display("./managerstatus/form.tpl");
		$smarty->display("./managerstatus/list.tpl");
                $smarty->display("./managerstatus/history.tpl");
?>
            </body>
        </html>
<?
?>