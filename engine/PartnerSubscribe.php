<?
    ini_set("magic_quotes_gpc","off");

    global $DATABASE;

    include_once("../phpset.inc");
    include_once("./functions.inc"); 

    AssignDataBaseSetting("../config.ini");
// ��������� �����������
	require_once($_SERVER['DOCUMENT_ROOT'].'/inside/auth.php');
//

    ini_set("display_startup_errors", "on");
    ini_set("display_errors", "on");
    ini_set("register_globals", "on");

    include_once("./class.partner_subscribe.inc"); 
    $pSubscribe = new PartnerSubscribe();

    require_once("../libs/Smarty.class.php");
    $smarty = new Smarty;
    $smarty->template_dir = "../templates";
    $smarty->compile_check = false;
    $smarty->caching = false;
    $smarty->compile_dir  = "../templates_c";
    $smarty->cache_dir = "../cache";
    $smarty->debugging = false;
    $smarty->clear_all_cache();

    $aError = array();

    if(isset($_GET['del'])) {
        
        $pSubscribe->_Id = (int)$_GET['del'];
        $pSubscribe->Delete();
        
    }
    
    if (isset($_POST["update"])) {
        
        if(isset($_POST['email']) && strlen(trim($_POST['email'])) > 0) {
            if(isset($_POST['statusId'])) {
                
                $pSubscribe->_Id = (int)$_GET['Id'];
                $pSubscribe->_email = trim($_POST['email']);
                $pSubscribe->_statusId = trim($_POST['statusId']);
                $pSubscribe->_cityIds = $_POST['cityIds'];
                $pSubscribe->Update();
            
            } else {
                $aError[] = '�� ������ ������';
            }
        } else {
            $aError[] = '�� ������ email';
        }        
        
        
    } else if (isset($_POST["add"])) {
        
        if(isset($_POST['email']) && strlen(trim($_POST['email'])) > 0) {
            if(isset($_POST['statusId'])) {
                
                $pSubscribe->_email = trim($_POST['email']);
                $pSubscribe->_statusId = trim($_POST['statusId']);
                $pSubscribe->_cityIds = $_POST['cityIds'];
                $pSubscribe->Insert();
            
            } else {
                $aError[] = '�� ������ ������';
            }
        } else {
            $aError[] = '�� ������ email';
        }
    }

    if(isset($_GET['Id'])) {
        $pSubscribe->_Id = (int)$_GET['Id'];
        $item = $pSubscribe->GetItem();
        $smarty->assign("itemData", $item);
    }
    
    $list = $pSubscribe->Select();	
    
    $smarty->assign("list", $list);		
    
    $smarty->assign("data", $pSubscribe);
    
    $aListCity = $pSubscribe->ListCity();
    
    $smarty->assign("aListCity", array_chunk($aListCity, ceil(count($aListCity)/3)));

    $smarty->assign("aError", $aError);
    
?>
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
            <LINK href="../general.css" type="text/css" rel="stylesheet">
            <meta http-equiv="content-type" content="text/html; charset=windows-1251"/> 
            <body>
<?
    $smarty->display("./mainmenu.tpl");

    $smarty->display("./PartnerSubscribe/list.tpl");
    $smarty->display("./PartnerSubscribe/error.tpl");
    $smarty->display("./PartnerSubscribe/add.tpl");
?>
    </body>
    </html>
<?
?>