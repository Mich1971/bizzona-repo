<?
	ini_set("magic_quotes_gpc","off");

	global $DATABASE;

	include_once("../phpset.inc");
	include_once("./functions.inc");
	require_once("./Cache/Lite.php");

	st_engine("../st_engine.txt");

	AssignDataBaseSetting("../config.ini");
// ��������� �����������
	require_once($_SERVER['DOCUMENT_ROOT'].'/inside/auth.php');
//

	include_once("./class.district.inc");
	$district = new District();

	include_once("./class.metro.inc");
	$metro = new Metro();

	include_once("./class.category.inc");
	$category = new Category();

	include_once("./class.subcategory.inc");
	$subCategory = new SubCategory();

	include_once("./class.country.inc");
	$country = new Country();

	include_once("./class.city.inc");
	$city = new City();

	include_once("./class.emailoffer.inc");
	$emailoffer = new EmailOffer();

	include_once("./class.sell.inc");
	$sell = new Sell();

  	include_once("./class.bizplan_company.inc");
  	$bizplan_company = new BizPlanCompany();

	include_once("./class.bizplan.inc");
	$bizplan = new BizPlan();

	include_once("./class.buy.inc");
	$buyer = new Buyer();

	include_once("./class.equipment.inc");
	$equipment = new Equipment();

	include_once("./class.investments.inc");
	$investments = new Investments();

	include_once("./class.BuyerSellSubscribe.inc");
	$buyerSellSubscribe = new BuyerSellSubscribe();

	include_once("./class.streets.inc");
	$streets = new streets();

  	include_once("./class.equipment.inc");
  	$equipment = new Equipment();

	include_once("../engine/class.coopbiz.inc");
	$coopbiz = new CoopBiz();

    include_once("./class.templateevent.inc");
    $templateEvent = new TemplateEvent();

	require ('./xajax_core/xajax.inc.php');
	$xajax = new xajax();
	//$xajax->configure('debug', true);

        include_once("./observer_filter.php");
        //filterSet('coopbiz');

	require_once("../libs/Smarty.class.php");
	$smarty = new Smarty;
	$smarty->template_dir = "../templates";
	$smarty->compile_check = false;
	$smarty->caching = false;
	$smarty->compile_dir  = "../templates_c";
	$smarty->cache_dir = "../cache";
	$smarty->debugging = false;
	$smarty->clear_all_cache();


	function SelectDistrict($regionID) {
		global $city, $coopbiz;

		$default_subregion = "";

		if(isset($_GET["ID"])) {
			$coopbiz->id = intval($_GET["ID"]);
			$residence = $coopbiz->GetResidence();
			$default_subregion = $residence->subRegion_id;
		}

		$subregiontext = "";

		$city->id = $regionID;
		$asubregion = $city->GetChildRegionAdmin();
		if(sizeof($asubregion) > 0) {
			$subregiontext = "<select name='subRegionID'>";

			$subregiontext.= "<option  value='0'>������ �������, ����</option>";
			while(list($k, $v) = each($asubregion)) {
				if($v->id == $default_subregion) {
					$subregiontext.= "<option value='".$v->id."' selected>".$v->title."</option>";
				} else {
					$subregiontext.= "<option value='".$v->id."'>".$v->title."</option>";
				}
			}

			$subregiontext.= "</select>";
		}

		$objResponse = new xajaxResponse();
		$objResponse->setCharacterEncoding("windows-1251");
		$objResponse->assign('subRegionDiv', 'innerHTML', $subregiontext);
		return $objResponse;

	}

	function AutoLoadRegionSetting() {
		global $coopbiz;
		if(isset($_GET["ID"])) {
			$coopbiz->id  = intval($_GET["ID"]);
			$residence = $coopbiz->GetResidence();
			return SelectDistrict($residence->region_id);
		}
	}


	$sDistrict =& $xajax->registerFunction('SelectDistrict');
	$sDistrict->setParameter(0, XAJAX_INPUT_VALUE, "region_id");
	$autoLoad =& $xajax->registerFunction('AutoLoadRegionSetting');

	$data = array();
	$data["offset"] = 0;
	$data["rowCount"] = 0;
	$result = $category->Select($data);
	$smarty->assign("listCategory", $result);

	$listSubCategory = $subCategory->Select($data);
	$smarty->assign("listSubCategory", $listSubCategory);


	$xajax->processRequest();
	echo '<?xml version="1.0" encoding="UTF-8"?>';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<LINK href="../general.css" type="text/css" rel="stylesheet">

<head>
<?
	$xajax->printJavascript();
?>
</head>

<meta http-equiv="content-type" content="text/html; charset=windows-1251"/>

<body>
<?

        if(isset($_GET['blockip'])) {

            include_once("./class.BlockIP.inc");
            $blockIp = new BlockIP();

            if($_GET['blockip'] == 'up') {

                $blockIp->BlockUp(array(
                    'ip' => trim($_GET['ip'])
                ));

            } else if ($_GET['blockip'] == 'down') {

                $blockIp->BlockDown(array(
                    'ip' => trim($_GET['ip'])
                ));

            }
        }


	if(isset($_POST) && sizeof($_POST) > 0)
	{

		// up update cache coopbiz
  		$options = array(
  			'cacheDir' => "../coopcache/",
   			'lifeTime' => 1
  		);

  		$cache = new Cache_Lite($options);
 		$cache->remove(''.intval($_GET["ID"]).'____coopitem');


  		$smarty->cache_dir = "../coopcache";
  		$smarty->clear_cache("./site/detailsCoop.tpl", intval($_GET["ID"]));
  		$smarty->cache_dir = "../cache";
  		// up update cache coopbiz


		$coopbiz->id = intval($_GET["ID"]);
		$coopbiz->GetItem();
		$coopbiz->name = $_POST["name"];

		$coopbiz->shortdescription = ParsingDetails($_POST["shortdescription"]);
		$coopbiz->description = TextReplace("description", ParsingDetails($_POST["description"]));

		$coopbiz->region_id = $_POST["region_id"];
		$coopbiz->subRegion_id = $_POST["subRegionID"];
		$coopbiz->region_text = $_POST["region_text"];
		$coopbiz->duration_id = $_POST["duration_id"];
		$coopbiz->status_id = $_POST["status_id"];
		$coopbiz->contact_face = $_POST["contact_face"];
		$coopbiz->contact_phone =  PhoneFix($_POST["contact_phone"]);
		$coopbiz->contact_email = $_POST["contact_email"];
		$coopbiz->contact_site = $_POST["contact_site"];

		$aCategoryID = split("[:]", $_POST["category"]);
		$coopbiz->category_id = $aCategoryID[0];
		$coopbiz->subcategory_id = $aCategoryID[1];

		$coopbiz->Update();

		if ($_POST["status_id"] != 15 && $_POST["status_id"] != 17) {
			if($_POST["category"] == "0") {
				$smarty->display("./error/category.tpl");
			}
			if($_POST["region_id"] == "0" && $_POST["subRegionID"] == "0") {
				$smarty->display("./error/region.tpl");
			}
		}

  		if((int)$_POST["status_id"] == 1) {
  			$templateEvent->EmailEvent(3,5, (int)$_GET["ID"]);
  		} else if ((int)$_POST["status_id"] == 2) {
  			$templateEvent->EmailEvent(4,5, (int)$_GET["ID"]);
  		} else if (in_array((int)$_POST["status_id"], array(0, 3))) {
  			$templateEvent->EmailEvent(1,5, (int)$_GET["ID"]);
  		}


		$smarty->cache_dir = "../cache";
		$resclear = $smarty->clear_all_cache();

		$count_category = $coopbiz->CountByCategory();

		$d = array();
		$d["CountC"] = $count_category+1;
		$d["ID"] = $coopbiz->category_id;
		$category->UpdateCountC($d);

		$count_city = $coopbiz->CountByCity();

		$d = array();
		$d["CountC"] = $count_city+1;
		$d["ID"] = $coopbiz->region_id;
		$city->UpdateCountC($d);
	}


	if(!isset($_GET["ID"])) {
		$data = array();
		$data["sort"] = "`coopbiz`.`id`";
		$data["offset"] = 0;
		$data["rowCount"] = 0;

		$dataPage["offset"] = 0;
		$dataPage["rowCount"] = 0;
		$dataPage["sort"] = "id";

		if (isset($_GET["StatusID"])) {
			$dataPage["status_id"] = intval($_GET["StatusID"]);
			$data["status_id"] = $dataPage["status_id"];
		}

		$resultPage = $coopbiz->Select($dataPage);

		$pagesplit = 40;

		$smarty->assign("CountRecord", sizeof($resultPage));
		$smarty->assign("CountSplit", $pagesplit);
		$smarty->assign("CountPage", ceil(sizeof($resultPage)/40));

		if (isset($_GET["offset"])) {
			$data["offset"] = intval($_GET["offset"]);
		} else {
			$data["offset"] = 0;
		}

		if (isset($_GET["rowCount"])) {
			$data["rowCount"] = intval($_GET["rowCount"]);
		} else {
			$data["rowCount"] = $pagesplit;
		}

		$coopbiz_list = $coopbiz->Select($data);
		$smarty->assign("list", $coopbiz_list);

		$smarty->display("./coopbiz/pagesplit.tpl", $_SERVER["REQUEST_URI"]);
  		$smarty->display("./selectcoopbiz.tpl");
		$smarty->display("./coopbiz/coopbiz_list.tpl");
		$smarty->display("./coopbiz/pagesplit.tpl", $_SERVER["REQUEST_URI"]);
	} else {
		$datacity = array();
		$datacity["offset"] = 0;
		$datacity["rowCount"] = 0;
		$datacity["sort"] = "ID";

		$listCity = $city->Select($datacity);
		$smarty->assign("listCity", $listCity);

		$coopbiz->id = intval($_GET["ID"]);
		$data = $coopbiz->GetItem();

		$smarty->assign("data", $data);
		$smarty->display("./coopbiz/coopbiz_edit.tpl");
	}
?>
<script language="JavaScript">
<?
	$autoLoad->printScript();
	echo ";";
?>
</script>
</body>
