<?php
	include_once("class.db.inc");  

	class Room_Lite 	{

		public $db = "";
		public $sql = "";
		public $Id = 0;
		public $RoomCategoryId = 0;
		public $ShortDescription = "";
		public $Description = "";
		public $area_from = 0;
		public $area_to = 0;
		public $cityId = 0;
		public $aCityId = array();
		public $streetId = 0;
		public $metroId = 0;
		public $cost = 0;
		public $cCostId = 0;
		public $photo_1 = "";
		public $smallphoto_1 = "";
		public $middlephoto_1 = "";
		public $statusId = 0;
		public $fio = "";
		public $phone = "";
		public $email = "";
		public $youtubeURL = "";
		public $youtubeShortURL = "";
		public $fax = "";
		public $pay_status = 0;
		public $pay_summ = 0;
		public $pay_datetime = 0;
		public $pay_smsnumber = 0;
		public $ip = "";
		public $datecreate = 0;
		public $countView = 0;
		public $area_text = "";
		public $cost_text = "";
		public $districtId = 0;
		public $Name = "";
		public $quicksearchurl = "";
		public $offset = 0;
		public $rowCount = 0;
		public $show_area = "";
		public $Address = "";
		public $latitude = 0;
		public $longitude = 0;
		public $zoomId = 7;
		public $RoomCategoryName = "";
		public $SeoTitle = "";
		public $PublicNotify = 0;
		public $AddressLat = "";
		public $sort = "";
		public $brokerId = 0;
		public $partnerid = 0;
		public $startcostf = 0;
		public $stopcostf = 0;
		
		function __construct() {
			global $DATABASE;
			$this->InitDB();
		}

		function InitDB() {
			global $DATABASE;
			$this->db = new DB($DATABASE["HOSTNAME"], $DATABASE["DATABASENAME"], $DATABASE["DATABASEUSER"], $DATABASE["DATABASEPASSWORD"]);
		}
		
		function CountRoom() {
     		$sql = 'select count(*) as \'count\' from Room where statusId in (1);';
			$result = $this->db->Select($sql);
			$row = mysql_fetch_row($result);
			return $row[0]; 			
		}
		
		
		function GetItem() {
			
			$_room = new Room_Lite();
			$this->sql = "select region.latitude as 'reg_latitude', region.longitude as 'reg_longitude',  region.zoom as 'reg_zoom',  Room.datecreate, Room.pay_datetime, Room.pay_status, Room.pay_summ, Room.SeoTitle, Room.cost_text, Room.area_text, Room.brokerId, RoomCategory.Name as 'RoomCategoryName', Room.Id, Room.middlephoto_1, Room.zoomId, Room.Address, Room.latitude, Room.longitude, Room.Name, Room.RoomCategoryId, Room.ShortDescription, Room.Description, Room.area_from, Room.area_to, Room.cityId, Room.streetId, Room.metroId, Room.cost, Room.cCostId, Room.photo_1, Room.smallphoto_1, Room.statusId, Room.fio, Room.phone, Room.email from Room left join RoomCategory on (RoomCategory.Id = Room.RoomcategoryId), region where 1 and region.id=Room.cityId and  Room.Id=".intval($this->Id).";";

			//echo $this->sql;
			
			$result = $this->db->Select($this->sql);
			while ($row = mysql_fetch_object($result)) {
				$_room->Id = intval($row->Id);
				$_room->RoomCategoryId = $row->RoomCategoryId;
				$_room->ShortDescription = $row->ShortDescription;
				$_room->Description = $row->Description;
				$_room->area_from = $row->area_from;
				$_room->area_to = $row->area_to;
				$_room->cityId = $row->cityId;
				$_room->streetId = $row->streetId;
				$_room->metroId = $row->metroId;
				$_room->cost = trim(strrev(chunk_split (strrev($row->cost), 3,' ')));
				$_room->cCostId = $row->cCostId;
				$_room->photo_1 = $row->photo_1;
				$_room->smallphoto_1 = $row->smallphoto_1;
				$_room->statusId = $row->statusId;
				$_room->fio = $row->fio;
				$_room->phone = $row->phone;
				$_room->email = $row->email;
				$_room->Name = $row->Name;
				
				$_room->latitude = (strlen($row->latitude) > 1 ? $row->latitude : $row->reg_latitude);
				$_room->longitude = (strlen($row->longitude) > 1 ? $row->longitude : $row->reg_longitude);
				$_room->zoomId = (intval($row->zoomId) > 0 ? $row->zoomId : $row->reg_zoom);
				
				$_room->Address = $row->Address;
				$_room->middlephoto_1 = $row->middlephoto_1;
				$_room->show_area = $_room->GetArea(true);
				$_room->RoomCategoryName = $row->RoomCategoryName; 
				$_room->SeoTitle = $row->SeoTitle;
				$_room->AddressLat = ruslat_for_google($row->Address);
				$_room->brokerId =  $row->brokerId;
				$_room->area_text = $row->area_text;
				$_room->cost_text = $row->cost_text;
				$_room->pay_summ = $row->pay_summ;
				$_room->pay_status = $row->pay_status;
				$_room->pay_datetime = $row->pay_datetime;
				$_room->datecreate = $row->datecreate;
			}
			return $_room;			
			
		}		

		function CountSelect() {

			$where = " where 1 ";

			if(intval($this->RoomCategoryId) > 0) {
				$where .= " and Room.RoomCategoryId = ".intval($this->RoomCategoryId)." ";
			}

			if(intval($this->cityId) > 0) {
				$where .= " and Room.cityId = ".intval($this->cityId)." ";
			}	
			
			if(sizeof($this->aCityId) > 0) {
				$where .= " and Room.cityId in (".implode(',', $this->aCityId).") ";
			}
			
			
			if(intval($this->districtId) > 0) {
				$where .= " and Room.districtId = ".intval($this->districtId)." ";
			}
			
			if(intval($this->metroId) > 0) {
				$where .= " and Room.metroId = ".intval($this->metroId)." ";
			}			
			
			if(intval($this->streetId) > 0) {
				$where .= " and Room.streetId = ".intval($this->streetId)." ";
			}			
			
			if(intval($this->statusId) > 0) {
				$where .= " and Room.statusId = ".intval($this->statusId)." ";
			}			
			
			if(intval($this->startcostf) > 0 && intval($this->stopcostf) > 0) {
				$where .= ' and  costseach between '.intval($this->startcostf).' and '.intval($this->stopcostf).'  ';
			} else if (intval($this->startcostf) > 0 && intval($this->stopcostf) <= 0) {
				$where .= ' and costseach >= '.intval($this->startcostf).'  ';
			} else if (intval($this->startcostf) <= 0 && intval($this->stopcostf) > 0) {
				$where .= ' and costseach <= '.intval($this->stopcostf).'  ';
			}			
			
			
			$this->sql = "select Count(Id) as 'Count' from Room ".$where.";";
			 
			$result = $this->db->Select($this->sql);
			while ($row = mysql_fetch_object($result)) {
				
				return intval($row->Count);
			}
			
			return 0;
		}
		
		function Select() {
			
			$array = Array();
			
			$where = " where 1 ";
			$limit = " ";
			
			$order = (strlen(trim($this->sort)) > 0 ? " ".$this->sort." " : " order by Room.pay_datetime desc ");
			
			if(intval($this->RoomCategoryId) > 0) {
				$where .= " and Room.RoomCategoryId = ".intval($this->RoomCategoryId)." ";
			}
			
			if(intval($this->cityId) > 0) {
				$where .= " and Room.cityId = ".intval($this->cityId)." ";
			}			
			
			if(sizeof($this->aCityId) > 0) {
				$where .= " and Room.cityId in (".implode(',', $this->aCityId).") ";
			}
			
			if(intval($this->districtId) > 0) {
				$where .= " and Room.districtId = ".intval($this->districtId)." ";
			}			
			
			if(intval($this->metroId) > 0) {
				$where .= " and Room.metroId = ".intval($this->metroId)." ";
			}			
			
			if(intval($this->streetId) > 0) {
				$where .= " and Room.streetId = ".intval($this->streetId)." ";
			}	
			
			if(intval($this->statusId) > 0) {
				$where .= " and Room.statusId = ".intval($this->statusId)." ";
			}					

			if(intval($this->startcostf) > 0 && intval($this->stopcostf) > 0) {
				$where .= ' and  costseach between '.intval($this->startcostf).' and '.intval($this->stopcostf).'  ';
			} else if (intval($this->startcostf) > 0 && intval($this->stopcostf) <= 0) {
				$where .= ' and costseach >= '.intval($this->startcostf).'  ';
			} else if (intval($this->startcostf) <= 0 && intval($this->stopcostf) > 0) {
				$where .= ' and costseach <= '.intval($this->stopcostf).'  ';
			}			
			
				
			if ($this->offset === 0 && $this->rowCount === 0) {
			} else {
				
				if(intval($this->rowCount) > 20) { $this->rowCount = 5; }
				if(intval($this->offset) > 50000) { $this->offset = 0; } 				
				
				$limit = " limit ".intval($this->offset).",".intval($this->rowCount)."";
			}		 	
			
			
			$this->sql = "select Room.partnerid, Room.pay_summ, Room.countView, Room.PublicNotify, Room.Address, Room.ip, Room.Id, Room.Name, RoomCategoryId, RoomCategory.Name as 'RoomCategoryName', Room.ShortDescription, Room.Description, Room.area_from, Room.area_to, Room.cityId, Room.streetId, Room.metroId, Room.cost, Room.cCostId, Room.photo_1, Room.smallphoto_1, Room.statusId, Room.fio, Room.phone, Room.email, Room.datecreate, Room.fax, Room.districtId from Room left join RoomCategory on (RoomCategory.Id = Room.RoomCategoryId) ".$where." ".$order." ".$limit.";";
			
			$result = $this->db->Select($this->sql);
			
			while ($row = mysql_fetch_object($result)) {
				
				$_room = new Room_Lite();
				$_room->Id = intval($row->Id);
				$_room->RoomCategoryId = intval($row->RoomCategoryId);
				$_room->ShortDescription = $row->ShortDescription;
				$_room->Description = $row->Description;
				$_room->area_from = $row->area_from;
				$_room->area_to = $row->area_to;
				$_room->cityId = $row->cityId;
				$_room->streetId = $row->streetId;
				$_room->metroId = $row->metroId;
				$_room->cost = trim(strrev(chunk_split (strrev($row->cost), 3,' ')));  //$row->cost;
				$_room->cCostId = $row->cCostId;
				$_room->photo_1 = $row->photo_1;
				$_room->smallphoto_1 = $row->smallphoto_1;
				$_room->statusId = $row->statusId;
				$_room->fio = $row->fio;
				$_room->phone = $row->phone;
				$_room->email = $row->email;
				$_room->Name = $row->Name;
				$_room->datecreate =  strftime("%d %B %Y",  strtotime($row->datecreate));				
				$_room->fax = $row->fax;
				$_room->districtId = $row->districtId;
				$_room->show_area = $_room->GetArea(true);
				$_room->RoomCategoryName = $row->RoomCategoryName;
				$_room->Address = $row->Address;
				$_room->countView = $row->countView;
				$_room->ip = $row->ip;
				$_room->PublicNotify = $row->PublicNotify;
				$_room->pay_summ = $row->pay_summ;
				$_room->partnerid = $row->partnerid;
 				 		
				$_room->quicksearchurl = "http://www.bizzona.ru/detailssearchroom.php?cityId=".$_room->cityId."&RoomCategoryId=".$_room->RoomCategoryId."&metroId=".$_room->metroId."&districtId=".$_room->districtId."&streetId=".$_room->streetId."";
				$array[] = $_room;
			}
			
			return $array;

		}

		function UpdateCountView() {
			$sql = "update Room set countView=countView+1 where Id=".intval($this->Id)." limit 1; ";			
			$this->db->Update($sql);
		}
		
		function GetArea($show_on_site = false) {
			
			if(intval($this->area_from) > 0 && intval($this->area_to) > 0) {
				
				if(intval($this->area_from) == intval($this->area_to)) {
					return ($show_on_site ?  "".$this->area_from." �&sup2; " : "".$this->area_from."");
				}
				
				if(intval($this->area_to) > intval($this->area_from)) {
					return ($show_on_site ? "".$this->area_from."  �&sup2;  - ".$this->area_to." �&sup2; " : "".$this->area_from." - ".$this->area_to."");
				}
				
				if(intval($this->area_to) < intval($this->area_from)) {
					return ($show_on_site ? "".$this->area_to." �&sup2; - ".$this->area_from." �&sup2;" : "".$this->area_to." - ".$this->area_from."");
				}
				
			}
			
			if(intval($this->area_from) <= 0 && intval($this->area_to) > 0) {
				return ($show_on_site ? "".$this->area_to." �&sup2;" : "".$this->area_to."");
			}
			
			if(intval($this->area_from) <= 0 && intval($this->area_to) <= 0) {
				return ($show_on_site ? "�� �������": "");
			}			
			
			if(intval($this->area_from) > 0 && intval($this->area_to) <= 0) {
				return ($show_on_site ? "".$this->area_from." �&sup2;" : "".$this->area_from."");
			}			
			
			return "";
		}
		
		function CountStatus($StatusID) {
			$sql = "select count(Room.Id) as 'c' from Room where Room.statusId=".intval($StatusID)."; ";
			$result = $this->db->Select($sql);
			while ($row = mysql_fetch_object($result)) {
				return $row->c;
			}
			return 0;
		}  

		function CountPayStatus() {
			$sql = "select COUNT(*) as 'c' from Room where Room.statusId in (5,0) and Room.pay_status=1";
			$result = $this->db->Select($sql);
			while ($row = mysql_fetch_object($result)) {
				return $row->c;
			}
			return 0;
		}		
		
		function ListCountPayStatus() {
			$sql = "select ID from Room where Room.statusId in (5,0) and Room.pay_status=1";
			$array = Array();
			$result = $this->db->Select($sql);
			while ($row = mysql_fetch_object($result)) {
				$array[] = $row->ID;
			}
			return $array;
		}		
		
		function Close() {
			$this->db->Close();
		}		
	}
?>
