<?php
	include_once("class.db.inc");
        require_once('class.bzn_items.inc');          

	define("NEW_INVESTMENTS", 0);
	define("OPEN_INVESTMENTS", 1);
	define("CLOSE_INVESTMENTS",2);
	define("LOOKED_INVESTMENTS", 3);
	define("DUBLICATE_INVESTMENTS", 15);
	define("DUST_INVESTMENTS", 17);
  
  	class Investments {
  	
    	var $db = "";
    	var $id = "";
        var $name = "";
        var $category_id = "";
        var $subCategory_id = "";
        var $region_text = "";
        var $region_id = "";
        var $subRegion_id = "";
        var $cost = "";
        var $ccost = "";
        var $period = "";
        var $payback = "";
        var $yearprofit = "";
        var $cyearprofit = "";
        var $totalinvestcost = "";
        var $totalinvestccost = "";
        var $stage = "";
        var $documents = "";
        var $contactface = "";
        var $contactphone = "";
        var $contactfax = "";
        var $contactemail = "";
        var $qview = "";
        var $period_id = "";
        var $status_id = "";
        var $datecreate = "";
        var $shortDescription = "";
        var $attractedPerson = "";
        var $statussend_id = "";
        var $ip = "";
        var $pay_status = "";
        var $pay_summ = "";
        var $partnerid = 0;
    	
    	function Investments() {
       		global $DATABASE;
       		$this->InitDB();
    	} 

     	function InitDB() {
       		global $DATABASE;
       		$this->db = new DB($DATABASE["HOSTNAME"], $DATABASE["DATABASENAME"], $DATABASE["DATABASEUSER"], $DATABASE["DATABASEPASSWORD"]);
     	}

     	function Close() {
     		$this->db->Close();
     	}
     	
     	function Insert() {
     		$sql = "insert into investments (
												name,
												category_id,
												subCategory_id,
												region_text,
												region_id,
												cost,
												costtxt,
												ccost,
												period,
												payback,
												yearprofit,
												yearprofittxt,
												cyearprofit,
												totalinvestcost,
												totalinvestcosttxt,
												totalinvestccost,
												stage,
												documents,
												contactface,
												contactphone,
												contactfax,
												contactemail,
												qview,
												period_id,
												status_id,
												shortDescription,
												attractedPerson,
												datecreate,
												statussend_id,
												ip,
												partnerid
     										 )
 												values
 											 (
												'".$this->name."',
												'".$this->category_id."',
												'".$this->subCategory_id."',
												'".$this->region_text."',
												'".$this->region_id."',
												'".$this->cost."',
												'".$this->cost."',
												'".$this->ccost."',
												'".$this->period."',
												'".$this->payback."',
												'".$this->yearprofit."',
												'".$this->yearprofit."',
												'".$this->cyearprofit."',
												'".$this->totalinvestcost."',
												'".$this->totalinvestcost."',
												'".$this->totalinvestccost."',
												'".$this->stage."',
												'".$this->documents."',
												'".$this->contactface."',
												'".$this->contactphone."',
												'".$this->contactfax."',
												'".$this->contactemail."',
												'".$this->qview."',
												'".$this->period_id."',
												'".NEW_INVESTMENTS."',
												'".$this->shortDescription."',
												'".$this->attractedPerson."',
												NOW(),
												'".$this->statussend_id."',
												'".$_SERVER['REMOTE_ADDR']."',
												'".$this->partnerid."'
 											 )	    										 
      										 ";
       		$result = $this->db->Insert($sql);

                $this->BZN_Insert(array(
                    'codeId' => $result,
                    'name' => $this->name,
                    'shortDetailsId' => $this->shortDescription,
                    'description' => $this->shortDescription,
                    'typeCurrencyId' => $this->ccost,
                    'minCost' => $this->cost,
                    'maxCost' => 0,
                ));                  
                
       		return $result;
     		
     	}

     	function Select($data) {
       		$limit = "";
       		$where = " where 1 ";
       		$order = " order  by ".$data["sort"]." desc";

       		if ($data["offset"] === 0 && $data["rowCount"] === 0) {
       		} else {
         		$limit = " limit ".intval($data["offset"]).",".$data["rowCount"]."";
       		} 
     	
       		if (isset($data["category_id"])) {
         		$where = $where." and investments.category_id=".intval($data["category_id"])." ";
       		} 

       		if (isset($data["subCategory_id"])) {
         		$where = $where." and investments.subCategory_id=".intval($data["subCategory_id"])." ";
       		} 
       		
       		if (isset($data["region_id"])) {
         		$where = $where." and investments.region_id=".intval($data["region_id"])." ";
       		} 
       
       		if (isset($data["status_id"])) {
         		if ($data["status_id"] == "-1") {
           			$where = $where." and investments.status_id in (1) ";
         		} else {
           			$where = $where." and investments.status_id=".intval($data["status_id"])." ";
         		}
       		} 

     		$sql = "select block_ip.ip as statusblockip, `investments`.*, Category.icon AS 'categoryicon'  from `investments`   LEFT JOIN Category ON ( Category.ID = investments.category_id ) left join block_ip on (block_ip.ip = investments.ip) ".$where." ".$order." ".$limit;
     	
     		
     		$result = $this->db->Select($sql);

       		$array = Array();
       		while ($row = mysql_fetch_object($result)) {
        		$obj = new stdclass();
                        
        		$obj->statusblockip = $row->statusblockip;
    			$obj->id = $row->id;
				$obj->name = $row->name;
				$obj->category_id = $row->category_id;
				$obj->subCategory_id = $row->subCategory_id;
				$obj->region_text = $row->region_text;
				$obj->region_id = $row->region_id;
				$obj->subRegion_id = $row->subRegion_id;
				//$obj->cost = $row->cost;
				$obj->cost = trim(strrev(chunk_split (strrev($row->cost), 3,' '))); 
				$obj->ccost = $row->ccost;
				$obj->period = $row->period;
				$obj->payback = $row->payback;
				//$obj->yearprofit = $row->yearprofit;
				$obj->yearprofit = trim(strrev(chunk_split (strrev($row->yearprofit), 3,' '))); 
				$obj->cyearprofit = $row->cyearprofit;
				$obj->totalinvestcost = $row->totalinvestcost;
				$obj->totalinvestccost = $row->totalinvestccost;
				$obj->stage = $row->stage;
				$obj->documents = $row->documents;
				$obj->contactface = $row->contactface;
				$obj->contactphone = $row->contactphone;
				$obj->contactfax = $row->contactfax;
				$obj->contactemail = $row->contactemail;
				$obj->qview = $row->qview;
				$obj->period_id = $row->period_id;   
				//$obj->datecreate = $row->datecreate;     		
				$obj->datecreate = strftime("%d %B %Y",  strtotime($row->datecreate));
				$obj->status_id = $row->status_id;
				$obj->statussend_id = $row->statussend_id;
				$obj->categoryicon = $row->categoryicon;
				$obj->shortDescription = $row->shortDescription;
				$obj->attractedPerson = $row->attractedPerson;
				$obj->ip = $row->ip;
				$obj->pay_summ = $row->pay_summ;
				$obj->pay_status = $row->pay_status;
				$obj->partnerid = $row->partnerid;
        		
				if($obj->status_id == NEW_INVESTMENTS) {
					$obj->status_text = "�����";
				} else if($obj->status_id == OPEN_INVESTMENTS) {
					$obj->status_text = "������";
				} else if($obj->status_id == LOOKED_INVESTMENTS) {
					$obj->status_text = "����������";
				} else if($obj->status_id == CLOSE_INVESTMENTS) {
					$obj->status_text = "������";
				} else if($obj->status_id == DUBLICATE_INVESTMENTS) {
					$obj->status_text = "��������";
				} else if($obj->status_id == DUST_INVESTMENTS) {
					$obj->status_text = "�����";
				}

				
         		$array[] = $obj;
       		}
       
       		return $array;
     		
     	}

		function CountStatus($StatusID) {
			$sql = "select count(investments.id) as 'c' from investments where investments.status_id=".intval($StatusID)."; ";
                        
                        //echo $sql;
                        
			$result = $this->db->Select($sql);
			while ($row = mysql_fetch_object($result)) {
				return $row->c;
			}
			return 0;
		}  
     	
     	
     	function Update() {
     		
     		$sql = "update `investments` set 	
									name='".addslashes($this->name)."',
									category_id='".addslashes($this->category_id)."',
									subCategory_id='".addslashes($this->subCategory_id)."',
									region_text='".addslashes($this->region_text)."',
									region_id='".addslashes($this->region_id)."',
									subRegion_id='".addslashes($this->subRegion_id)."',
									cost='".addslashes($this->cost)."',
									ccost='".addslashes($this->ccost)."',
									period='".addslashes($this->period)."',
									payback='".addslashes($this->payback)."',
									yearprofit='".addslashes($this->yearprofit)."',
									cyearprofit='".addslashes($this->cyearprofit)."',
									totalinvestcost='".addslashes($this->totalinvestcost)."',
									totalinvestccost='".addslashes($this->totalinvestccost)."',
									stage='".addslashes($this->stage)."',
									documents='".addslashes($this->documents)."',
									contactface='".addslashes($this->contactface)."',
									contactphone='".addslashes($this->contactphone)."',
									contactfax='".addslashes($this->contactfax)."',
									contactemail='".addslashes($this->contactemail)."',
									period_id='".addslashes($this->period_id)."',
									status_id='".addslashes($this->status_id)."',
									shortDescription='".addslashes($this->shortDescription)."',
									attractedPerson='".addslashes($this->attractedPerson)."'
     	 										where `id`='".(int)$this->id."' limit 1;";
     		$result = $this->db->Select($sql);
                
                $this->BZN_UpdateStatus(array(
                    'codeId' => intval($this->id), 
                    'statusId' => $this->ConvertorStatus($this->status_id), 
                ));    
                
                $this->BZN_Update(array(
                    'codeId' => $this->id, 
                    'name' => $this->name,
                    'shortDetailsId' => $this->shortDescription,
                    'description' => $this->shortDescription,
                    'typeCurrencyId' => $this->ccost,
                    'minCost' => $this->cost,
                    'maxCost' => 0,
                ));                  
                
     	}
     	

     	function Delete() {
     	}

     	function GetItem() {

     		$sql = "select `investments`.* from `investments` where id=".$this->id.";";
     		
     		$result = $this->db->Select($sql);
        	$obj = new stdclass();

       		while ($row = mysql_fetch_object($result)) {
		 		$this->id = $obj->id = $row->id;
				$this->name = $obj->name = $row->name;
				$this->category_id = $obj->category_id = $row->category_id;
				$this->subCategory_id = $obj->subCategory_id = $row->subCategory_id;
				$this->region_text = $obj->region_text = $row->region_text;
				$this->region_id = $obj->region_id = $row->region_id;
				$this->cost = $obj->cost = trim(strrev(chunk_split (strrev($row->cost), 3,' '))); 
				$this->ccost = $obj->ccost = $row->ccost;
				$this->costtxt = $obj->costtxt = $row->costtxt;
				$this->period = $obj->period = $row->period;
				$this->payback = $obj->payback = $row->payback;
				//$this->yearprofit = $obj->yearprofit = $row->yearprofit;
				$this->yearprofit = $obj->yearprofit = trim(strrev(chunk_split (strrev($row->yearprofit), 3,' '))); 
				$this->cyearprofit = $obj->cyearprofit = $row->cyearprofit;
				$this->yearprofittxt = $obj->yearprofittxt = $row->yearprofittxt;
				$this->totalinvestcost = $obj->totalinvestcost = trim(strrev(chunk_split (strrev($row->totalinvestcost), 3,' '))); 
				$this->totalinvestccost = $obj->totalinvestccost = $row->totalinvestccost;
				$this->totalinvestcosttxt = $obj->totalinvestcosttxt = $row->totalinvestcosttxt;
				$this->stage = $obj->stage = $row->stage;
				$this->documents = $obj->documents = $row->documents;
				$this->contactface = $obj->contactface = $row->contactface;
				$this->contactphone = $obj->contactphone = $row->contactphone;
				$this->contactfax = $obj->contactfax = $row->contactfax;
				$this->contactemail = $obj->contactemail = $row->contactemail;
				$this->qview = $obj->qview = $row->qview;
				$this->period_id = $obj->period_id = $row->period_id;
				$this->status_id = $obj->status_id = $row->status_id;
				$this->statussend_id = $obj->statussend_id = $row->statussend_id;
				$this->datecreate = $obj->datecreate =  strftime("%d %B %Y",  strtotime($row->datecreate));
				$this->shortDescription = $obj->shortDescription = $row->shortDescription;
				$this->attractedPerson = $obj->attractedPerson = $row->attractedPerson;
				$this->pay_status = $obj->pay_status = $row->pay_status;
				$this->pay_summ = $obj->pay_summ = $row->pay_summ;
       		}

       		return $obj;     	
       		
     	}

     	function IncrementQView() {
        	$sql = "update `investments` set `qview`=`qview`+1 where id=".$this->id.";";
        	$result = $this->db->Select($sql);
     	}
     
     	function CountByCategory() {
     		$sql = "select count(`id`) as 'count' from `investments` where 1 and `investments`.`category_id`='".$this->category_id."' and `investments`.`id` != ".$this->id." and `status_id` in (".OPEN_INVESTMENTS.");";
     		$result = $this->db->Select($sql);
     		while ($row = mysql_fetch_object($result)) {
     			return $row->count;
     		}
     	}
     
     	function CountByCity() {
     		$sql = "select count(`id`) as 'count' from `investments` where 1 and `investments`.`region_id`='".$this->region_id."' and `investments`.`id` != ".$this->id." and `status_id` in (".OPEN_INVESTMENTS.");";
     		$result = $this->db->Select($sql);
     		while ($row = mysql_fetch_object($result)) {
     			return $row->count;
     		}
     	}
     
     	function SetSendEmail() {
        	$sql = "update `investments` set `statussend_id`=".$this->statussend_id." where id=".$this->id.";";
        	$result = $this->db->Select($sql);
     	}
     	
     	
		function SelectSubCategory() {
     	
			$sql = "SELECT count( `investments`.`subCategory_id` ) AS 'count', `SubCategory`.`ID`, `SubCategory`.`name`,  `SubCategory`.`keysellequipment`
					FROM `investments`, `SubCategory` 
						WHERE 1 
							AND `investments`.`category_id` =".$this->category_id."
							AND `SubCategory`.`ID` = `investments`.`subCategory_id` 
							AND `investments`.`status_id` 
							IN ('1')
								GROUP BY `investments`.`subCategory_id` order by `SubCategory`.`ordernum` desc ";     	
     	
			$result = $this->db->Select($sql);

			$array = Array();
			while ($row = mysql_fetch_object($result)) {
				$obj = new stdclass();
				$obj->subCategory_id = $row->ID;
				$obj->Name = $row->name;
				$obj->SeoName = $row->keysellequipment;
				$obj->Count = $row->count;
				$array[] = $obj;
			}
		
			return $array;
		}
     	
		function UpdatePhone() {
			$sql = "update investments set contactphone='".$this->contactphone."' where id=".intval($this->id)."";
			$result = $this->db->Select($sql);
		}		
     	
		function GetResidence() 
		{
			$sql = "select region_id, subRegion_id from investments where 1 and id=".$this->id."";
			$result = $this->db->Sql($sql);
			$obj = new stdclass();
     	
			while ($row = mysql_fetch_object($result)) {
				$obj->region_id = $row->region_id;
				$obj->subRegion_id = $row->subRegion_id;
			}
			return $obj;     	
		}
		
		function SetPayment($CodeID, $PayStatus, $PaySumm, $PayDateTime, $PayNumber)
		{
			$sql = "update investments set   
                                 `pay_status`=".$PayStatus.", 
                                 `pay_summ`=".$PaySumm.", 
                                 `pay_datetime`=NOW(),
                                 `pay_smsnumber`=".$PayNumber." 
                         where id=".$CodeID." ";
			$result = $this->db->Update($sql);
       
			$sql = "insert into payments(summ,datepayment) values ('".$PaySumm."',NOW());";
			$result = $this->db->Update($sql);
		}
		
		function CountPayStatus() {
			
			$sql = "select COUNT(*) as 'c' from investments where investments.status_id in (3,0) and investments.pay_status=1";
                        
                        //echo $sql;
                        
			$result = $this->db->Select($sql);
			while ($row = mysql_fetch_object($result)) {
				return $row->c;
			}
			return 0;
		}
		
		function ListCountPayStatus() {
			$sql = "select ID from investments where investments.status_id in (3,0) and investments.pay_status=1";
			$array = Array();
			$result = $this->db->Select($sql);
			while ($row = mysql_fetch_object($result)) {
				$array[] = $row->ID;
			}
			return $array;
		}		
		
                private function BZN_Update($aParams) {

                    $bzn_items = new BZN_Items();
                    $bzn_items->Update(array(
                        'typeId' => BZN_Items::TYPE_INVESTMENT,
                        'codeId' => $aParams['codeId'],
                        'subTypeId' => BZN_Items::SUPPLIER,
                        'name' => (isset($aParams['name']) ? $aParams['name'] : '' ),
                        'shortDetailsId' => (isset($aParams['shortDetailsId']) ? $aParams['shortDetailsId'] : '' ),
                        'description' => (isset($aParams['description']) ? $aParams['description'] : '' ),    
                        'typeCurrencyId' => (isset($aParams['typeCurrencyId']) ? $aParams['typeCurrencyId'] : '' ),
                        'minCost' => (isset($aParams['minCost']) ? $aParams['minCost'] : '' ),
                        'maxCost' => (isset($aParams['maxCost']) ? $aParams['maxCost'] : '' ),                        
                    ));            

                }                    
                
                private function BZN_Insert($aParams) {

                    $bzn_items = new BZN_Items();
                    $bzn_items->Insert(array(
                        'typeId' => BZN_Items::TYPE_INVESTMENT,
                        'codeId' => $aParams['codeId'],
                        'subTypeId' => BZN_Items::SUPPLIER,
                        'name' => (isset($aParams['name']) ? $aParams['name'] : '' ),
                        'shortDetailsId' => (isset($aParams['shortDetailsId']) ? $aParams['shortDetailsId'] : '' ),
                        'description' => (isset($aParams['description']) ? $aParams['description'] : '' ),  
                        'typeCurrencyId' => (isset($aParams['typeCurrencyId']) ? $aParams['typeCurrencyId'] : '' ),
                        'minCost' => (isset($aParams['minCost']) ? $aParams['minCost'] : '' ),
                        'maxCost' => (isset($aParams['maxCost']) ? $aParams['maxCost'] : '' ),                        
                    ));            

                }       	
               
                private function BZN_UpdateStatus($aParams) {

                    $bzn_items = new BZN_Items();
                    $bzn_items->UpdateStatus(array(
                        'typeId' => BZN_Items::TYPE_INVESTMENT,
                        'codeId' => $aParams['codeId'],
                        'statusId' => $aParams['statusId'],
                        'subTypeId' => BZN_Items::SUPPLIER
                    ));               

                }                  
                
                
                private function ConvertorStatus($statusId) {

                    $aStatus = array(
                        '0' => BZN_Items::STATUS_NEW,
                        '1' => BZN_Items::STATUS_OPEN,
                        '2' => BZN_Items::STATUS_CLOSE,
                        '3' => BZN_Items::STATUS_WAIT,
                        '15' => BZN_Items::STATUS_DUBLICATE,
                        '17' => BZN_Items::STATUS_TRUSH,
                    );

                    return (isset($aStatus[$statusId]) ? $aStatus[$statusId] : '');

                }                 
                
                
  	}
?>
