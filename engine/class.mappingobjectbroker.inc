<?php
	include_once("class.db.inc");  

	class MappingObjectBroker {

		public $Id;
		public $brokerId;
		public $brokerObjectID;
		public $bizzonaObjectId;
		public $ObjectTypeId;
		public $brokerDateObject;	
		public $urlBrokerObject;
		public $arrayBrokerIds = array();
	
		function __construct() {
			global $DATABASE;
			$this->InitDB();
		}

		function InitDB() {
			global $DATABASE;
			$this->db = new DB($DATABASE["HOSTNAME"], $DATABASE["DATABASENAME"], $DATABASE["DATABASEUSER"], $DATABASE["DATABASEPASSWORD"]);
		}		
		
		public function Insert() {
			
			$sql = "replace into MappingObjectBroker (	`brokerId`, 
									   					`brokerObjectID`,
									   					`bizzonaObjectId`,
									   					`ObjectTypeId`,
									   					`brokerDateObject`,
									   					`urlBrokerObject`)
													values
													( 
														'".$this->brokerId."',
														'".trim($this->brokerObjectID)."',
														'".$this->bizzonaObjectId."',
														'".$this->ObjectTypeId."',
														'".$this->brokerDateObject."',
														'".$this->urlBrokerObject."'
													);";
			 
			echo $sql."<hr>";
			
			$result = $this->db->Insert($sql);
			
			//echo "result ".$result."<hr>";
			
			return $result;
		}		
		
		public function IsExistsBrokerObject() {
			
			$exists = false;
			
			$sql = " select Id from MappingObjectBroker USE INDEX (import_broker_brokerobject) where brokerObjectID=".intval($this->brokerObjectID)." and brokerId=".intval($this->brokerId)."; ";

			echo $sql."<hr>";
			
			$result = $this->db->Select($sql);
			while ($row = mysql_fetch_object($result)) {
			
				echo "<h1>".$row->Id."</h1>";
					
				$exists = (intval($row->Id) > 0 ? true : false );
				
			}
			mysql_free_result($result);
			
			return $exists;
			//return true;
		}
		
		public function Select() {
			
			$sql = "select MappingObjectBroker.Id, MappingObjectBroker.brokerId, MappingObjectBroker.brokerObjectID, MappingObjectBroker.bizzonaObjectId, MappingObjectBroker.ObjectTypeId, MappingObjectBroker.brokerDateObject from MappingObjectBroker, Sell where 1 and Sell.ID=MappingObjectBroker.bizzonaObjectId and Sell.StatusID in (1,2) and MappingObjectBroker.ObjectTypeId=".$this->ObjectTypeId." and MappingObjectBroker.brokerId=".$this->brokerId." ";

			//echo $sql."<hr>";
			
			$result = $this->db->Select($sql);

			$array = array();
			
			while ($row = mysql_fetch_object($result)) {
			
				//$temp_mapping = new MappingObjectBroker();
				$temp_mapping = array();
				//$temp_mapping = new stdClass();
				//$temp_mapping->bizzonaObjectId = $row->bizzonaObjectId;
				//$temp_mapping->brokerDateObject = $row->brokerDateObject;
				//$temp_mapping->brokerId = $row->brokerId;
				//$temp_mapping->brokerObjectID = $row->brokerObjectID;	
				
				$temp_mapping["bizzonaObjectId"] = $row->bizzonaObjectId;
				$temp_mapping["brokerDateObject"] = $row->brokerDateObject;
				$temp_mapping["brokerId"] = $row->brokerId;
				$temp_mapping["brokerObjectID"] = $row->brokerObjectID;	
				
				$array[] = $temp_mapping;
			}			
			
			return $array;
		}
		
		public function UpdateBrokerDateObject() {
			
			$sql = "update MappingObjectBroker set brokerDateObject='".$this->brokerDateObject."' where brokerObjectID=".$this->brokerObjectID." ";
			echo $sql."<hr>";
			$result = $this->db->Select($sql);
			
		}
		
		public function ReOpen() {
			
			$a_bizzonaIds = array();

			$sql = "select bizzonaObjectId from MappingObjectBroker where 1 and brokerObjectID in (".implode(", ", $this->arrayBrokerIds).") and brokerId=".$this->brokerId."; ";
			
			//echo "<hr>".$sql."<hr>";
			
			
			$result = $this->db->Select($sql);
			while ($row = mysql_fetch_object($result)) {
				$a_bizzonaIds[] = $row->bizzonaObjectId;
			}
			
			$sql = "update Sell set statusId = 1 where 1 and statusId in (10,12) and ID in (".implode(",", $a_bizzonaIds).") and brokerId = ".$this->brokerId."; ";
			
			echo "<hr>".$sql."<hr>";
                        
                        $result = $this->db->Select($sql);
		}
                
                private function NameStatusStr($statusId) {
                    
                    if($statusId == 0) {
                        return '�����';
                    }
                    
                    if($statusId == 1) {
                        return '����� ������';
                    }                    
                    
                    if($statusId == 10) {
                        return '���������';
                    }                    
                    
                    if($statusId == 5) {
                        return '�����������';
                    }                    
                    
                    if($statusId == 12) {
                        return '������';
                    }                    
                    
                    if($statusId == 15) {
                        return '��������';
                    }                    
                    
                    if($statusId == 17) {
                        return '���������';
                    }                    
                     
                    if($statusId == 250) {
                        return '�����������';
                    }                    
                    
                    return '� ���������';
                }
                
                public function RelationObjects() {
                    
                    $aResult = array();
                    
                    $sql = "select mb.Id, mb.brokerObjectID, mb.bizzonaObjectId, Sell.NameBizID, Sell.StatusId from MappingObjectBroker as mb inner join Sell on (Sell.ID = mb.bizzonaObjectId) where mb.brokerId=".(int)$this->brokerId." and  mb.brokerObjectID > 0  order by mb.brokerObjectID";
                    $result = $this->db->Select($sql);
                    while ($row = mysql_fetch_object($result)) {
                        $aResult[]  = array(
                            'Id' => $row->Id, 
                            'brokerObjectID' => $row->brokerObjectID, 
                            'bizzonaObjectId' => $row->bizzonaObjectId, 
                            'NameBizID' => substr($row->NameBizID, 0, 80)."...", 
                            'StatusId' => $this->NameStatusStr($row->StatusId), 
                        );
                    }
                    
                    return $aResult;
                }
		
	}


?>