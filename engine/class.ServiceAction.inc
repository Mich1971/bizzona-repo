<?
include_once("class.db.inc");  	

class ServicesAction {

	private $_db;
	private $_sql;			

	public $_id;
	public $_serviceId;
	public $_name;
	public $_price;
	public $_statusId;
	public $_prepayment;

	function __construct() {
		global $DATABASE;
		$this->InitDB();
	}

	function InitDB() {
		global $DATABASE;
		$this->db = new DB($DATABASE["HOSTNAME"], $DATABASE["DATABASENAME"], $DATABASE["DATABASEUSER"], $DATABASE["DATABASEPASSWORD"]);
	}

	function __destruct() {
			
	}		

	public function Insert() {
		
		$aParams = array(
			'`serviceId`' => "'".$this->_serviceId."'",
			'`name`' => "'".$this->_name."'",
			'`price`' => "'".$this->_price."'",
			'`statusId`' => "'".$this->_statusId."'",
			'`prepayment`' => "'".$this->_prepayment."'",
			 
		);
		
		$this->_sql = " insert into ServiceAction (".implode(', ', array_keys($aParams)).") values (".implode(',', array_values($aParams))."); ";
		$result = $this->db->Insert($this->_sql);			
		return $result;
	}
	
	public function Update() {

		$aParams = array(
			"`serviceId`='".$this->_serviceId."'", 
			"`name`='".$this->_name."'", 
			"`price`='".$this->_price."'", 
			"`statusId`='".$this->_statusId."'", 
			"`prepayment`='".$this->_prepayment."'", 
		);
		
		$this->_sql =  " update ServiceAction set ".implode(',', $aParams)." where `Id`=".$this->_id." limit 1; ";
		
		$result = $this->db->Sql($this->_sql);	
	}
	
	public function GetItem() {
		
		$this->_sql = " select * from ServiceAction where id={$this->_id};";
		
		$result = $this->db->Select($this->_sql);
		
		$arr = array();	
		
		while ($row = mysql_fetch_object($result)) {
			
			$arr['id'] = $row->id;
			$arr['serviceId'] = $row->serviceId;
			$arr['name'] = $row->name;
			$arr['price'] = $row->price;
			$arr['statusId'] = $row->statusId;
			$arr['prepayment'] = $row->prepayment;
			
		}
		
		return $arr;		
		
	}
	
	public function Select() {
		
		$this->_sql = " select * from ServiceAction where serviceId={$this->_serviceId};";
		
		$result = $this->db->Select($this->_sql);
		
		$arr = array();	
		
		while ($row = mysql_fetch_object($result)) {
			
			$aRow = array();
			$aRow['id'] = $row->id;
			$aRow['serviceId'] = $row->serviceId;
			$aRow['name'] = $row->name;
			$aRow['price'] = $row->price;
			$aRow['statusId'] = $row->statusId;
			$aRow['prepayment'] = $row->prepayment;
			
			array_push($arr, $aRow);
		}
		
		return $arr;		
		
	}
	
	
	
}