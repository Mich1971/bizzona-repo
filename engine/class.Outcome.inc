<?php
	include_once("class.db.inc");  	

	class Outcome {

		private $_db;
		private $_sql;			
		
		public $_Id;
		public $_typeId;
		public $_codeId;
		public $_summ;
		public $_serviceId;
		public $_serviceActionId;
		public $_date;
		
		function __construct() {
			global $DATABASE;
			$this->InitDB();
		}

		function InitDB() {
			global $DATABASE;
			$this->db = new DB($DATABASE["HOSTNAME"], $DATABASE["DATABASENAME"], $DATABASE["DATABASEUSER"], $DATABASE["DATABASEPASSWORD"]);
		}

		function __destruct() {
			
		}		
		
		public function getList() {
			
			//$this->_sql = " select Outcome.summ, Outcome.date, Services.shortName as 'ServiceShortName', ServiceAction.name as 'ServiceActionName' from Outcome, Services, ServiceAction where Outcome.typeId='".$this->_typeId."' and Outcome.codeId=".(int)$this->_codeId." and Services.id=Outcome.serviceId and ServiceAction.serviceId=Outcome.serviceId and ServiceAction.id=serviceActionId  order by Outcome.date desc; ";
			
                        $this->_sql = "select Outcome.Id, Outcome.summ, Outcome.date, Services.shortName as 'ServiceShortName', CategoryAction.name as 'CategoryActionName', Action.name as 'ServiceActionName' from Outcome, Services, Action, CategoryAction where Outcome.typeId='".$this->_typeId."' and Outcome.codeId=".(int)$this->_codeId." and Services.id=Outcome.serviceId and Action.id=Outcome.serviceActionId and Services.id=Outcome.serviceId and CategoryAction.id=Action.categoryActionId  order by Outcome.date desc; ";
                    
			$result = $this->db->Select($this->_sql);
		
			$arr = array();	

		
			while ($row = mysql_fetch_object($result)) {
				
				$_a = array(
                                        'id' => $row->Id, 
					'summ' => $row->summ, 
					'date' => $row->date,
					'ServiceShortName' => $row->ServiceShortName,
					'ServiceActionName' => $row->ServiceActionName,
                                        'CategoryActionName' => $row->CategoryActionName,
				);
				
				$arr[] = $_a;
			}
			
			return $arr; 
			
		} 

		public function getListAdmin($aParams) {
			
			//$this->_sql = "select Outcome.codeId, Outcome.summ, Outcome.date, Services.id as 'ServiceId', Services.shortName as 'ServiceShortName', ServiceAction.name as 'ServiceActionName' from Outcome, Services, ServiceAction where Outcome.typeId='sell'  and Services.id=Outcome.serviceId and ServiceAction.serviceId=Outcome.serviceId and ServiceAction.id=serviceActionId order by Outcome.date desc; ";
                    
                        $limit = '';
                    
                        if(isset($aParams['limit'])) {
                            $limit = ' limit 10 ';
                        }
                    
			$this->_sql = "select Outcome.codeId, Outcome.summ, Outcome.date, Services.id as 'ServiceId', Services.shortName as 'ServiceShortName', Action.name as 'ServiceActionName', CategoryAction.name as 'CategoryActionName' from Outcome, Services, Action, CategoryAction where Outcome.typeId='sell'  and Services.id=Outcome.serviceId and Action.id=Outcome.serviceActionId and CategoryAction.id=Action.categoryActionId  order by Outcome.date desc ".$limit.";";
                        
			$result = $this->db->Select($this->_sql);
		
			$arr = array();	

		
			while ($row = mysql_fetch_object($result)) {
				
				$_a = array(
                                        'isNow' => (date("m/d/y", strtotime($row->date)) == date("m/d/y", time()) ? true : false), 
					'codeId' => $row->codeId, 
					'summ' => $row->summ, 
					'date' => $row->date,
					'ServiceShortName' => $row->ServiceShortName,
					'ServiceActionName' => $row->ServiceActionName,
                                        'ServiceActionName' => $row->ServiceActionName,
					'CategoryActionName' => $row->CategoryActionName,
				);
				
				$arr[] = $_a;
			}
			
			return $arr;
			
		}
		
		
		public function getTotalBalance() {
			
			$this->_sql = " select sum(summ) from Outcome where codeId={$this->_codeId} and typeId='{$this->_typeId}'; ";
			
			//echo $this->_sql.'\n\r';

			$result = $this->db->Select($this->_sql);
			
			$row = mysql_fetch_row($result);
		
			return $row[0];
		}
		
		public function AddBalance() {
			
			$aParameters = array(
				'`typeId`' 		=> "'".$this->_typeId."'",
				'`codeId`' 		=> "'".$this->_codeId."'",
				'`summ`' 		=> "'".$this->_summ."'",
				'`serviceId`' 	=> "'".$this->_serviceId."'",
				'`serviceActionId`' 	=> "'".$this->_serviceActionId."'",
				'`date`' 		=> 'NOW()',
			);
			
			$this->_sql =  "insert into Outcome (".implode(', ', array_keys($aParameters)).") values  (".implode(', ', array_values($aParameters)).")";
			
			$result = $this->db->Insert($this->_sql);
			
		}
		
	}
?>