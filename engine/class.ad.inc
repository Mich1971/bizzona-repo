<?php
	include_once("class.db.inc");  	

	class Ad {
		
		private $_db;
		public $_sql;		
		
    	public $_Id;
		public $_placeId;
		public $_startDate;
		public $_stopDate;
		public $_shareShowAd;
		public $_typeAdsId;
		public $_nameFile;
		public $_listCategoryIds;
		public $_listCityIds;
		public $_statusId;
		public $_siteTypeId;
		public $_url;
		public $_note;
		
		public $_aSiteTypeId = array(
										array('id' => 0, 'name' => '�� �������'),
										array('id' => 1, 'name' => '������ - ��������'), 
										array('id' => 2, 'name' => '���� - ��������'), 
		
										);
		
		public $_aStatusId = array(
										array('id' => 0, 'name' => '�� �������'),
										array('id' => 1, 'name' => '�������')
									);
		
		public $_aPlaceId = array(
		
									array( 'id' => 1, 'name' => 'headerbanner', 'description' => '������ - �����' ),
									array( 'id' => 2, 'name' => 'bannertop_left', 'description' => '������ - ������� - ���� - �����' ),
									array( 'id' => 3, 'name' => 'bannertop_right', 'description' => '������ - ������� - ���� - ������' ),
									array( 'id' => 4, 'name' => 'bannermiddle_left', 'description' => '������ - ������� - ������� - �����' ),
									array( 'id' => 5, 'name' => 'bannermiddle_right', 'description' => '������ - ������� - ������� - ������' ),
                                                                        array( 'id' => 10, 'name' => 'bannermiddle_middle', 'description' => '������ - ������� - ������� - �������' ),
									array( 'id' => 6, 'name' => 'bannerbottom_left', 'description' => '������ - ������� - ������ - �����' ),
									array( 'id' => 7, 'name' => 'bannerbottom_right', 'description' => '������ - ������� - ������ - ������' ),
									array( 'id' => 8, 'name' => 'inner_bannertop_left', 'description' => '������ - ���������� - ���� - �����' ),
									array( 'id' => 9, 'name' => 'inner_bannertop_right', 'description' => '������ - ���������� - ���� - ������' )
		
									);
									

		public $_aTypeAdsId = array(
										array('id' => 1, 'name' => 'flash', 'description' => '����'),
										array('id' => 2, 'name' => 'image', 'description' => '����')
										
									);
										
		
		function __construct() {
			global $DATABASE;
			$this->InitDB();
		}

		function InitDB() {
			global $DATABASE;
			$this->db = new DB($DATABASE["HOSTNAME"], $DATABASE["DATABASENAME"], $DATABASE["DATABASEUSER"], $DATABASE["DATABASEPASSWORD"]);
		}

		function __destruct() {
			
		}		

		function Insert() {
			
			$this->_sql = 'insert into ads (`placeId`,`startDate`,`stopDate`,`shareShowAd`,`typeAdsId`, `nameFile`, `listCategoryIds`, `listCityIds`, `statusId`, `url`, `siteTypeId`, `note`) 
									values 
										   ("'.$this->_placeId.'", "'.$this->_startDate.'", "'.$this->_stopDate.'", "'.$this->_shareShowAd.'", "'.$this->_typeAdsId.'", "'.$this->_nameFile.'", "'.$this->_listCategoryIds.'", "'.$this->_listCityIds.'", "'.$this->_statusId.'", "'.$this->_url.'", "'.$this->_siteTypeId.'", "'.$this->_note.'");';
			
			$result = $this->db->Select($this->_sql);
		}
		
		function Update() {
			$this->_sql = '
							update  ads set placeId="'.$this->_placeId.'", 
							                startDate="'.$this->_startDate.'", 
							                stopDate="'.$this->_stopDate.'",
							                shareShowAd="'.$this->_shareShowAd.'",
							                typeAdsId="'.$this->_typeAdsId.'",
							                siteTypeId="'.$this->_siteTypeId.'", 
							                url="'.$this->_url.'",
							                note="'.$this->_note.'",
							                '.(strlen($this->_nameFile) > 0 ? ' nameFile="'.$this->_nameFile.'",  ' : ' ').'
							                listCategoryIds="'.$this->_listCategoryIds.'",
							                listCityIds="'.$this->_listCityIds.'",
							                statusId="'.$this->_statusId.'"  
							 where Id='.$this->_Id.' limit 1;
			              ';
			$this->db->Update($this->_sql);
		}
		
		function GetItem() {
			
			$where = ' where 1 and Id='.$this->_Id.'';
			$this->_sql = 'select * from ads '.$where.'';
			$result = $this->db->Select($this->_sql);
			
			while ($row = mysql_fetch_object($result)) {
			
				$obj = new stdclass();
				
				$obj->Id = $row->Id;
				$obj->placeId = $row->placeId;
				$obj->note = $row->note;
				$obj->url = $row->url;
				
				$obj->startDate = $row->startDate;
				$obj->startDateStr = strftime("%m/%d/%Y",  strtotime($row->startDate));
				
				$obj->stopDate = $row->stopDate;
				$obj->stopDateStr = strftime("%m/%d/%Y",  strtotime($row->stopDate));
				
				$obj->shareShowAd = $row->shareShowAd;
				$obj->typeAdsId = $row->typeAdsId;
				$obj->nameFile = $row->nameFile; 
				
				$obj->listCategoryIds = $row->listCategoryIds;
				$obj->alistCategoryIds = explode(',',$row->listCategoryIds);
				
				$obj->listCityIds = $row->listCityIds;
				$obj->alistCityIds = explode(',', $row->listCityIds);
				
				$obj->statusId = $row->statusId;
				$obj->siteTypeId = $row->siteTypeId;
				
				return $obj;
				
			}  
			return; 
		}
		
		function Select($data) {
			
			$order = ' order by Id desc';
			
			$this->_sql = 'select * from ads '.$order.'';
			$result = $this->db->Select($this->_sql);
			
			$arr = array();	

			while ($row = mysql_fetch_object($result)) {
				
				$obj = new stdclass();
				
				$obj->Id = $row->Id;
				$obj->placeId = $row->placeId;
				$obj->url = $row->url;
				$obj->note = $row->note;
				$obj->startDate = $row->startDate;
				$obj->startDateStr = $this->GetDateText($row->startDate);
				$obj->stopDate = $row->stopDate;
				$obj->stopDateStr = $this->GetDateText($row->stopDate);
				$obj->shareShowAd = $row->shareShowAd;
				$obj->typeAdsId = $row->typeAdsId;
				$obj->nameFile = $row->nameFile;
				$obj->listCategoryIds = $row->listCategoryIds;
				$obj->listCityIds = $row->listCityIds;
				$obj->statusId = $row->statusId;
				$obj->siteTypeId = $row->siteTypeId;

				$acityname = array();
				$acity = $data['acity'];
				$e_acity = explode(',', $obj->listCityIds);
				while (list($k, $v) = each($e_acity)) {
					while (list($kc, $vc) = each($acity)) {
						if($vc['id'] == $v) {
							array_push($acityname, $vc['name']);
							break;
						}
					}
					reset($acity);
				}
				$obj->listCityStr = implode('<br>', $acityname);
				
				
				$acategoryname = array();
				$acategory = $data['acategory'];
				$e_acategory = explode(',', $obj->listCategoryIds);
				while (list($k, $v) = each($e_acategory)) {
					while (list($kc, $vc) = each($acategory)) {
						while (list($kc1, $vc1) = each($vc)) {
							if($vc1['id'] == $v) {	
								array_push($acategoryname, $vc1['name']);
								break;
							}
						}
					}
					reset($acategory);
				}
				$obj->listCategoryStr = implode('<br>', $acategoryname);
				
				while (list($k, $v) = each($this->_aPlaceId)) {
					if($v['id'] == $obj->placeId) {
						$obj->placeStr = $v['description'];
						reset($this->_aPlaceId);
						break;
					}
				}
				
				while (list($k, $v) = each($this->_aTypeAdsId)) {
					if($v['id'] == $obj->typeAdsId) {
						$obj->typeAdsStr = $v['description'];
						reset($this->_aTypeAdsId);
						break;
					}
				}				
				
				while (list($k, $v) = each($this->_aStatusId)) {
					if($v['id'] == $obj->statusId) {
						$obj->statusStr = $v['name'];
						reset($this->_aStatusId);
						break;
					}
				}				
				
				while (list($k, $v) = each($this->_aSiteTypeId)) {
					if($v['id'] == $obj->siteTypeId) {
						$obj->siteTypeStr = $v['name'];
						reset($this->_aSiteTypeId);
						break;
					}
				}				
				
				array_push($arr, $obj);
				
			}
			
			return $arr;
		}
	
		function InitAd(&$smarty, $get_params = null) {

			$mergeCityRegion = array();
			$mergeCityRegion[63] = 1816;
			$mergeCityRegion[29] = 201;
			$mergeCityRegion[30] = 259;
			$mergeCityRegion[22] = 105;
			$mergeCityRegion[31] = 309;
			$mergeCityRegion[32] = 317;
			$mergeCityRegion[33] = 385;
			$mergeCityRegion[34] = 395;
			$mergeCityRegion[35] = 429;
			$mergeCityRegion[36] = 460;
			$mergeCityRegion[66] = 1915;
			$mergeCityRegion[37] = 514;
			$mergeCityRegion[38] = 554;
			$mergeCityRegion[16] = 1721;
			$mergeCityRegion[39] = 634;
			$mergeCityRegion[40] = 656;
			$mergeCityRegion[42] = 744;
			$mergeCityRegion[43] = 773;
			$mergeCityRegion[44] = 907;
			$mergeCityRegion[23] = 928;
			$mergeCityRegion[24] = 985;
			$mergeCityRegion[45] = 1041;
			$mergeCityRegion[46] = 1054;
			$mergeCityRegion[48] = 1100;
			$mergeCityRegion[49] = 1120;
			$mergeCityRegion[51] = 1217;
			$mergeCityRegion[52] = 1264;
			$mergeCityRegion[53] = 1279;
			$mergeCityRegion[54] = 1310;
			$mergeCityRegion[55] = 1353;
			$mergeCityRegion[57] = 1408;
			$mergeCityRegion[56] = 1384;
			$mergeCityRegion[58] = 1419;
			$mergeCityRegion[59] = 1441;
			$mergeCityRegion[60] = 1512;
			$mergeCityRegion[61] = 1791;
			$mergeCityRegion[62] = 1800;
			$mergeCityRegion[64] = 1847;
			$mergeCityRegion[67] = 1970;
			$mergeCityRegion[26] = 1995;
			$mergeCityRegion[68] = 2025;
			$mergeCityRegion[69] = 2048;
			$mergeCityRegion[70] = 2093;
			$mergeCityRegion[71] = 2101;
			$mergeCityRegion[72] = 2132;
			$mergeCityRegion[73] = 2157;
			$mergeCityRegion[2] = 304;
			$mergeCityRegion[27] = 2227;
			$mergeCityRegion[74] = 2326;
			$mergeCityRegion[75] = 2394;
			$mergeCityRegion[76] = 2530;
			
			
			$category = (isset($get_params['subCatId']) ? intval($get_params['subCatId']) : ( isset($get_params['subcategory']) ? intval($get_params['subcategory']) : 0  ) );
			$city = (isset($get_params['subCityId']) ? intval($get_params['subCityId']) : (isset($get_params['cityId']) ? intval($get_params['cityId']) : 0)   ); 

			if($city <= 0 && isset($get_params['subCityID'])) {
				$city = intval($get_params['subCityID']);
			}
			
			if(isset($get_params['action']) && $get_params['action'] == "city") {
				
				if(isset($mergeCityRegion[intval($get_params['ID'])])) {
				
					$city = $mergeCityRegion[intval($get_params['ID'])];
						
				} 
				
			} else if (isset($get_params['cityId']) && intval($get_params['cityId']) > 0) {
				
				$city = $mergeCityRegion[intval($get_params['cityId'])];
			}

			$allsubcategory = array();
			
			if($category <= 0) {
				
				if(isset($get_params['action']) && $get_params['action'] == "category") {
					
					$this->_sql = 'select ID from SubCategory where CategoryID='.intval($get_params['ID']).';';
					$result = $this->db->Select($this->_sql);
					while ($row = mysql_fetch_object($result)) {
						$allsubcategory[] = $row->ID;
					}
							
				}
				
			}

			$this->_sql = 'select * from ads where NOW()>=startDate and NOW()<=stopDate  and statusId=1;';
			$result = $this->db->Select($this->_sql);
			
			$arr = array();
			
			while ($row = mysql_fetch_object($result)) {
				
				$obj = new stdclass();
				
				$obj->Id = $row->Id;
				$obj->placeId = $row->placeId;
				$obj->url = $row->url;
				$obj->startDate = $row->startDate;
				$obj->stopDate = $row->stopDate;
				$obj->shareShowAd = $row->shareShowAd;
				$obj->typeAdsId = $row->typeAdsId;
				$obj->nameFile = $row->nameFile;
				$obj->listCategoryIds = $row->listCategoryIds;
				$obj->alistCategoryIds = (strlen($row->listCategoryIds) > 0 ? explode(',',$row->listCategoryIds) : '');
				$obj->listCityIds = $row->listCityIds;
				$obj->alistCityIds = (strlen($row->listCityIds) > 0 ? explode(',', $row->listCityIds) : '');
				$obj->statusId = $row->statusId;
				$obj->url = $row->url;
				
				array_push($arr, $obj);
			}
			
			$a_bannertop_left = array();
			while (list($k, $v) = each($arr)) {
				if($v->placeId == 2) {
					$a_bannertop_left[] = array('typeAdsId' => $v->typeAdsId, 'nameFile' => $v->nameFile, 'url' => $v->url);
				}
			}
			reset($arr);

			$a_bannermiddle_middle = array();
			while (list($k, $v) = each($arr)) {
				if($v->placeId == 10) {
					$a_bannermiddle_middle[] = array('typeAdsId' => $v->typeAdsId, 'nameFile' => $v->nameFile, 'url' => $v->url);
				}
			}
			reset($arr);
                        
                        

			$a_bannermiddle_left = array();
			while (list($k, $v) = each($arr)) {
				if($v->placeId == 4) {
					$a_bannermiddle_left[] = array('typeAdsId' => $v->typeAdsId, 'nameFile' => $v->nameFile, 'url' => $v->url);		
				}
			}
			reset($arr);
			
			
			$a_bannerbottom_left = array();
			while (list($k, $v) = each($arr)) {
				if($v->placeId == 6) {
					$a_bannerbottom_left[] = array('typeAdsId' => $v->typeAdsId, 'nameFile' => $v->nameFile, 'url' => $v->url);		
				}
			}
			reset($arr);			
			

			$a_bannertop_right = array();
			while (list($k, $v) = each($arr)) {
				if($v->placeId == 3) {
					$a_bannertop_right[] = array('typeAdsId' => $v->typeAdsId, 'nameFile' => $v->nameFile, 'url' => $v->url);		
				}
			}
			reset($arr);			
			
			
			
			$a_bannermiddle_right = array();
			while (list($k, $v) = each($arr)) {
				if($v->placeId == 5) {
					$a_bannermiddle_right[] = array('typeAdsId' => $v->typeAdsId, 'nameFile' => $v->nameFile, 'url' => $v->url);
				}
			}
			reset($arr);			
			

			
			$a_bannerbottom_right = array();
			while (list($k, $v) = each($arr)) {
				if($v->placeId == 7) {
					$a_bannerbottom_right[] = array('typeAdsId' => $v->typeAdsId, 'nameFile' => $v->nameFile, 'url' => $v->url);
				}
			}
			reset($arr);			
			
			
			
			$a_headerbanner = array();
			while (list($k, $v) = each($arr)) {
				if($v->placeId == 1) {
					$a_headerbanner[] = array('typeAdsId' => $v->typeAdsId, 'nameFile' => $v->nameFile, 'url' => $v->url);
				}
			}
			reset($arr);			
			
			
			
			$a_inner_bannertop_left = array();
			
			if($city > 0 && $category > 0) {
				while (list($k, $v) = each($arr)) {
					if($v->placeId == 8 && in_array($city, $v->alistCityIds) && in_array($category, $v->alistCategoryIds)) {
						$a_inner_bannertop_left[] = array('typeAdsId' => $v->typeAdsId, 'nameFile' => $v->nameFile, 'url' => $v->url);						
					}
				}
				
			} else if ($city > 0) {
				while (list($k, $v) = each($arr)) {
					if($v->placeId == 8 && in_array($city, $v->alistCityIds)) {
						$a_inner_bannertop_left[] = array('typeAdsId' => $v->typeAdsId, 'nameFile' => $v->nameFile, 'url' => $v->url);						
					}
				}				

			} else if ($category > 0) {
				while (list($k, $v) = each($arr)) {
					if($v->placeId == 8 && in_array($category, $v->alistCategoryIds)) {
						$a_inner_bannertop_left[] = array('typeAdsId' => $v->typeAdsId, 'nameFile' => $v->nameFile, 'url' => $v->url);						
					}
				}				
			} else if ($category <= 0 && $city <= 0) {
				
				while (list($k, $v) = each($arr)) {
					
					if($v->placeId == 8 && is_array($v->alistCategoryIds)) {
						
						 $t_a = array_diff($allsubcategory, $v->alistCategoryIds);
						 
						 if(sizeof($t_a) <= 0) {
							$a_inner_bannertop_left[] = array('typeAdsId' => $v->typeAdsId, 'nameFile' => $v->nameFile, 'url' => $v->url);						 	
						 }
						
					}
					
				}
				
				
			}
			
			
			reset($arr);
			
			if(sizeof($a_inner_bannertop_left) <= 0) {
				while (list($k, $v) = each($arr)) {
					if($v->placeId == 8 && $v->alistCityIds == '' && $v->alistCategoryIds == '') {
						$a_inner_bannertop_left[] = array('typeAdsId' => $v->typeAdsId, 'nameFile' => $v->nameFile, 'url' => $v->url);
					}
				} 
			} 
			
			

			$a_inner_bannertop_right = array();
			
			if($city > 0 && $category > 0) {
				while (list($k, $v) = each($arr)) {
					if($v->placeId == 9 && in_array($city, $v->alistCityIds) && in_array($category, $v->alistCategoryIds)) {
						$a_inner_bannertop_right[] = array('typeAdsId' => $v->typeAdsId, 'nameFile' => $v->nameFile, 'url' => $v->url);						
					}
				}
				
			} else if ($city > 0) {
				while (list($k, $v) = each($arr)) {
					if($v->placeId == 9 && in_array($city, $v->alistCityIds)) {
						$a_inner_bannertop_right[] = array('typeAdsId' => $v->typeAdsId, 'nameFile' => $v->nameFile, 'url' => $v->url);						
					}
				}				

			} else if ($category > 0) {
				while (list($k, $v) = each($arr)) {
					if($v->placeId == 9 && in_array($category, $v->alistCategoryIds)) {
						$a_inner_bannertop_right[] = array('typeAdsId' => $v->typeAdsId, 'nameFile' => $v->nameFile, 'url' => $v->url);						
					}
				}				
			} 
			
			reset($arr);
			
			if(sizeof($a_inner_bannertop_right) <= 0) {
				while (list($k, $v) = each($arr)) {
					if($v->placeId == 9 && $v->alistCityIds == '' && $v->alistCategoryIds == '') {
						$a_inner_bannertop_right[] = array('typeAdsId' => $v->typeAdsId, 'nameFile' => $v->nameFile, 'url' => $v->url);
					}
				} 
			} 			
			
			
			
			$result_b = new stdClass();
			
			
			if(sizeof($a_bannermiddle_middle) > 0) {
				$result_b->bannermiddle_middle = $a_bannermiddle_middle[array_rand($a_bannermiddle_middle, 1)];
			} else {
				$result_b->bannermiddle_middle = array();
			}			
			
			if(sizeof($a_bannertop_left) > 0) {
				$result_b->bannertop_left = $a_bannertop_left[array_rand($a_bannertop_left, 1)];
			} else {
				$result_b->bannertop_left = array();
			}
			
			if(sizeof($a_bannermiddle_left) > 0) {
				$result_b->bannermiddle_left = $a_bannermiddle_left[array_rand($a_bannermiddle_left, 1)];
			} else {
				$result_b->bannermiddle_left = array();
			}			
			
			if(sizeof($a_bannerbottom_left) > 0) {
				$result_b->bannerbottom_left = $a_bannerbottom_left[array_rand($a_bannerbottom_left, 1)];
			} else {
				$result_b->bannerbottom_left = array();
			}			
			
			if(sizeof($a_bannertop_right) > 0) {
				$result_b->bannertop_right = $a_bannertop_right[array_rand($a_bannertop_right, 1)];
			} else {
				$result_b->bannertop_right = array();
			}			
			
			if(sizeof($a_bannermiddle_right) > 0) {
				$result_b->bannermiddle_right = $a_bannermiddle_right[array_rand($a_bannermiddle_right, 1)];
			} else {
				$result_b->bannermiddle_right = array();
			}			
			
			if(sizeof($a_bannerbottom_right) > 0) {
				$result_b->bannerbottom_right = $a_bannerbottom_right[array_rand($a_bannerbottom_right, 1)];
			} else {
				$result_b->bannerbottom_right = array();
			}			
			
			if(sizeof($a_headerbanner) > 0) {
				$result_b->headerbanner = $a_headerbanner[array_rand($a_headerbanner, 1)];
			} else {
				$result_b->headerbanner = array();
			}			
			
			if(sizeof($a_inner_bannertop_left) > 0) {
				$result_b->inner_bannertop_left = $a_inner_bannertop_left[array_rand($a_inner_bannertop_left, 1)];
			} else {
				$result_b->inner_bannertop_left = array();
			}			
			
			if(sizeof($a_inner_bannertop_right) > 0) {
				$result_b->inner_bannertop_right = $a_inner_bannertop_right[array_rand($a_inner_bannertop_right, 1)];
			} else {
				$result_b->inner_bannertop_right = array();
			}			
			
			$smarty->assign("bannerads", $result_b);
			
			return $result_b;
		}
		
		function GetDateText($date) {
			
			$b = explode("-",date("Y-m-d"));
      		$yesterday = date("Ymd",mktime(0,0,0,$b[1],$b[2]-1,$b[0]));
			
			if(date("Ymd") == date("Ymd", strtotime($date))) {
				return "������� ";//.strftime("%H:%M",  strtotime($date));
			} else if ($yesterday == date("Ymd", strtotime($date))) { 
				return "����� ";//.strftime("%H:%M",  strtotime($date));
			} else {
				return strftime("%d %B %Y",  strtotime($date));
			}
			
			return strftime("%d %B %Y",  strtotime($date));
		}
		
	}
?>