<?php
	require_once("class.db.inc");  	

	class BZN_Items {

		private $_db;
		private $_sql;			
		public $_Id;
		public $_typeId; // �������, ������� �.�. 
		public $_codeId;
		public $_subTypeId; // 1 - ���������, 2 - �����������
                public $_statusId; // ������ ���������� ����������
                public $_name; // �������� ����������
                public $_shortDetailsId; // ������� �������� ����������
                public $_description; // �������� �������� ����������
		public $_created; // ���� �������� ����������
                public $_typeCurrencyId; // ��� ������
                public $_minCost; // ����������� ���������
                public $_maxCost; // ������������ ���������
                public $_minProfitPerMonth; // ����������� �������� �������
                public $_maxProfitPerMonth; // ������������ �������� �������
                public $_timeInvest; // ����� �������� ����������
                public $_listObject; // �������� ������������ �������� ������������
                public $_communications; // �����: ������(2 ���.����� + ���������� ��������)     
                public $_matProperty; // ������������ ���������
                public $_minMonthAveTurn; // ����������� �������� ������
                public $_maxMonthAveTurn; // ������������ �������� ������
                public $_monthExpemse; // �������������� ����� ������
                public $_sumDebts; // ����� �������������� ������� ���
                public $_wageFund; // ���� ���������� �����
                public $_obligation; // ���� � ������� ������������ ����������� ����� �������� ������


                
                const TYPE_SELL = 1; 
                const TYPE_ROOM = 2;
                const TYPE_INVESTMENT = 3;
                const TYPE_COOPBIZ = 4;
                const TYPE_EQUIPMENT = 5;

                const SUPPLIER = 1; // ���������
                const CONSUMER = 2; // �����������
                
                const IDENTITY_POINT_VALUE = 500000; // �������� ������ ���������� � �������� ���������� �������� �� ����� ������� ���������� � ����� �������.
                
                
                const STATUS_NEW = 0;  // ������ ���������� - �����������
                const STATUS_OPEN = 1; // ������ ���������� - ������������
                const STATUS_CLOSE = 2; // ������ ���������� - �������
                const STATUS_TRUSH = 3; // ������ ���������� - ����
                const STATUS_WAIT = 4; // ������ ���������� - �������� ������
                const STATUS_DUBLICATE = 5; // ������ ���������� - ��������
                const STATUS_BLOCK = 6; // ������ ���������� - ����������
                const STATUS_MADE = 10; // ������ ���������� - ���������
                const STATUS_SOLD = 12; // ������ ���������� - ������
                
                
                
                
		function __construct() {
			global $DATABASE;
			$this->InitDB();
		}

		private function InitDB() {
			global $DATABASE;
			$this->db = new DB($DATABASE["HOSTNAME"], $DATABASE["DATABASENAME"], $DATABASE["DATABASEUSER"], $DATABASE["DATABASEPASSWORD"]);
		}

		function __destruct() {
			
		}		
 
                public function Update($aParams) {
                    
                    $where = " where 1 ";
                    
                    if($aParams['codeId'] >= self::IDENTITY_POINT_VALUE) {
                     
                        $where .= " and bzn_items.Id=".(int)$aParams['codeId']." ";
                        
                    } else {
                        
                        $where .= " and bzn_items.codeId=".(int)$aParams['codeId']." ";
                        
                    }
                     
                    $where .= " and "."`typeId` = ".$aParams['typeId']." ";
                    $where .= " and "."`subTypeId` = ".$aParams['subTypeId']." ";
                    
                    $update = array();
                    foreach($aParams as $k => $v) {
                        
                        if(!in_array($k, array('typeId', 'subTypeId', 'codeId'))) {
                            $update[] = "`".$k."`= '".$v."' ";
                        }
                        
                    }

                    if(count($update)) {
                    
                        $this->_sql = "update bzn_items set ".  implode(', ', $update)." ".$where." limit 1;";                    
                    
                        //echo $this->_sql; 
                    
                        $result = $this->db->Insert($this->_sql);
                    
                        return $result;
                    }
                    
                    return -1;
                }
                
                public function Insert($aParams) {
                    
                    /*
                    $aParametersSQL = array(
				'`typeId`' 		=> $aParams['typeId'],
				'`codeId`' 		=> $aParams['codeId'],
				'`subTypeId`' 		=> $aParams['subTypeId'],
                    );
                    */

                    //$this->_sql =  "insert into bzn_items (".implode(', ', array_keys($aParametersSQL)).") values  (".implode(', ', array_values($aParametersSQL)).")";
			
                    
                    $this->_sql = "insert into bzn_items "
                            . "set"
                            . "`typeId` = ".$aParams['typeId'].","
                            . "`codeId` = ".$aParams['codeId'].","
                            . "`subTypeId` = ".$aParams['subTypeId'].","
                            . "`name` = '".(isset($aParams['name']) ? $aParams['name'] : "")."',"
                            . "`shortDetailsId` = '".(isset($aParams['shortDetailsId']) ? $aParams['shortDetailsId'] : "")."',"
                            . "`created` = ".(isset($aParams['created']) ? "'".$aParams['created']."'" : "NOW()").", " 
                            . "`description` = '".(isset($aParams['description']) ? $aParams['description'] : "")."', "
                            . "`typeCurrencyId` = '".(isset($aParams['typeCurrencyId']) ? $aParams['typeCurrencyId'] : "")."', "
                            . "`minCost` = '".(isset($aParams['minCost']) ? $aParams['minCost'] : "")."', "
                            . "`maxCost` = '".(isset($aParams['maxCost']) ? $aParams['maxCost'] : "")."', "
                            . "`minProfitPerMonth` = '".(isset($aParams['minProfitPerMonth']) ? $aParams['minProfitPerMonth'] : "")."', "
                            . "`maxProfitPerMonth` = '".(isset($aParams['maxProfitPerMonth']) ? $aParams['maxProfitPerMonth'] : "")."', "
                            . "`timeInvest` = '".(isset($aParams['timeInvest']) ? $aParams['timeInvest'] : "")."', "
                            . "`listObject` = '".(isset($aParams['listObject']) ? $aParams['listObject'] : "")."',  "
                            . "`communications` = '".(isset($aParams['communications']) ? $aParams['communications'] : "")."',  "
                            . "`matProperty` = '".(isset($aParams['matProperty']) ? $aParams['matProperty'] : "")."',  "
                            . "`minMonthAveTurn` = '".(isset($aParams['minMonthAveTurn']) ? $aParams['minMonthAveTurn'] : "")."',  "
                            . "`maxMonthAveTurn` = '".(isset($aParams['maxMonthAveTurn']) ? $aParams['maxMonthAveTurn'] : "")."',  "
                            . "`monthExpemse` = '".(isset($aParams['monthExpemse']) ? $aParams['monthExpemse'] : "")."',  "
                            . "`sumDebts` = '".(isset($aParams['sumDebts']) ? $aParams['sumDebts'] : "")."',  "
                            . "`wageFund` = '".(isset($aParams['wageFund']) ? $aParams['wageFund'] : "")."',  "
                            . "`obligation` = '".(isset($aParams['obligation']) ? $aParams['obligation'] : "")."'  "
                            . ";";
                    
                    //echo $this->_sql; 
                    
                    $result = $this->db->Insert($this->_sql);

                    return $result;		
                }
		
                public function UpdateStatus($aParams) {

                    $this->_sql =  "update bzn_items set `statusId`=".$aParams['statusId']." where  `typeId`=".$aParams['typeId']."  and `subTypeId`=".$aParams['subTypeId']." and  `codeId`=".$aParams['codeId']." limit 1; ";
                    //echo $this->_sql; 
                    $this->db->Sql($this->_sql);
                    
                }
                
                public function Import() {
                    
                    // up sell
                    $this->_sql = "select ID, NameBizID, ShortDetailsID, DetailsID from Sell;";
                    $result = $this->db->Select($this->_sql);
                    
                    while ($row = mysql_fetch_object($result)) {

                        $this->Insert(array(
                            'typeId' => self::TYPE_SELL,
                            'codeId' => $row->ID,
                            'name' => $row->NameBizID,
                            'shortDetailsId' => $row->ShortDetailsID,
                            'description' => $row->DetailsID,
                            'subTypeId' => self::SUPPLIER
                        ));                         
                        
                    }
                    
                    // down sell
                    
                    // up buyer
                    $this->_sql = "select id, name, otherinfo, description from buyer;";
                    $result = $this->db->Select($this->_sql);
                    
                    while ($row = mysql_fetch_object($result)) {

                        $this->Insert(array(
                            'typeId' => self::TYPE_SELL,
                            'codeId' => $row->id,
                            'name' => $row->name,
                            'shortDetailsId' => $row->otherinfo,
                            'description' => $row->description,
                            'subTypeId' => self::CONSUMER
                        ));                         
                        
                    }                    
                    
                    // down buyer 

                    
                    // up room
                    $this->_sql = "select Id, Name, ShortDescription, Description from Room;";
                    $result = $this->db->Select($this->_sql);

                     while ($row = mysql_fetch_object($result)) {

                        $this->Insert(array(
                            'typeId' => self::TYPE_ROOM,
                            'codeId' => $row->Id,
                            'name' => $row->Name,
                            'shortDetailsId' => $row->ShortDescription,
                            'description' => $row->Description,
                            'subTypeId' => self::SUPPLIER
                        ));                         
                        
                    }                    
                    
                    // down room
                    
                    
                    // up coopbiz
                    $this->_sql = "select id, name, shortdescription, description from coopbiz;";
                    $result = $this->db->Select($this->_sql);
                    
                     while ($row = mysql_fetch_object($result)) {

                        $this->Insert(array(
                            'typeId' => self::TYPE_COOPBIZ,
                            'codeId' => $row->id,
                            'name' => $row->name,
                            'shortDetailsId' => $row->shortdescription,
                            'description' => $row->description,
                            'subTypeId' => self::SUPPLIER
                        ));                         
                        
                    }                     
                    // down coopbiz
                    
                    
                    // up investments
                    $this->_sql = "select id, name, shortDescription from investments;";
                    $result = $this->db->Select($this->_sql);
                    
                     while ($row = mysql_fetch_object($result)) {

                        $this->Insert(array(
                            'typeId' => self::TYPE_INVESTMENT,
                            'codeId' => $row->id,
                            'name' => $row->name,
                            'shortDetailsId' => $row->shortDescription,
                            'description' => $row->shortDescription,
                            'subTypeId' => self::SUPPLIER
                        ));                         
                        
                    }                    
                    // down investments
                    
                    
                    // up equipment
                    $this->_sql = "select id, name, shortdescription, description from equipment;";
                    $result = $this->db->Select($this->_sql);
                    
                     while ($row = mysql_fetch_object($result)) {

                        $this->Insert(array(
                            'typeId' => self::TYPE_EQUIPMENT,
                            'codeId' => $row->id,
                            'name' => $row->name,
                            'shortDetailsId' => $row->shortdescription,
                            'description' => $row->description,
                            'subTypeId' => self::SUPPLIER
                        ));                         
                        
                    }                       
                    // down equipment
                    
                }
		
		public function GetTypeInfoById($aParams) {
                    
                    if($aParams['codeId'] >= self::IDENTITY_POINT_VALUE) {
                        
                        $this->_sql = "SELECT bzn_items.Id, bzn_items.subTypeId, bzn_items.typeId, bzn_items.codeId FROM bzn_items WHERE bzn_items.Id=".(int)$aParams['codeId'].";";
                        
                    } else {
                        
                        $this->_sql = "SELECT bzn_items.Id, bzn_items.subTypeId, bzn_items.typeId, bzn_items.codeId FROM bzn_items WHERE bzn_items.codeId=".(int)$aParams['codeId'].";";
                    }
                    
                    $result = $this->db->Select($this->_sql);
                    
                    $arr = array();
                    
                    while ($row = mysql_fetch_object($result)) {
                        
                        $url = '';
                        $prefix = '';
                        
                        if($row->typeId == self::TYPE_SELL && $row->subTypeId == self::SUPPLIER) {
                            
                            $url = 'http://www.bizzona.ru/detailsSell/'.$row->codeId;
                            $prefix = '������� �������: ';
                            
                        } else if($row->typeId == self::TYPE_SELL && $row->subTypeId == self::CONSUMER) {
                            
                            $url = 'http://www.bizzona.ru/detailsBuy/'.$row->codeId; 
                            $prefix = '������� �������: ';
                            
                        } else if ($row->typeId == self::TYPE_COOPBIZ) {

                            $url = 'http://www.bizzona.ru/detailsCoop/'.$row->codeId; 
                            $prefix = '���������� ������: ';
                            
                        } else if ($row->typeId == self::TYPE_EQUIPMENT) {

                            $url = 'http://www.bizzona.ru/detailsEquipment/'.$row->codeId; 
                            $prefix = '������� ������������: ';
                            
                        } else if ($row->typeId == self::TYPE_INVESTMENT) {
                            
                            $url = 'http://www.bizzona.ru/detailsInvestments/'.$row->codeId; 
                            $prefix = '����� ����������: ';                            
                            
                        } else if ($row->typeId == self::TYPE_ROOM) {
                            
                            $url = 'http://www.bizzona.ru/detailsroom.php?ID='.$row->codeId; 
                            $prefix = '������� ���������: ';                            
                            
                        }
                        
                        $arr[] = array(
                            'Id' => $row->Id,
                            'subTypeId' => $row->subTypeId,
                            'typeId' => $row->typeId,
                            'codeId' => $row->codeId,
                            'url' => $url, 
                            'prefix' => $prefix, 
                        );
                        
                    }
                    
                    return $arr;
                    
                }
                
                
	}
?>
