<?php
	ini_set("magic_quotes_gpc","off");

        ini_set("magic_quotes_gpc","off");
        ini_set("memory_limit","512M");
        
        
	global $DATABASE;

	include_once("../phpset.inc");
	include_once("./functions.inc"); 
	require_once("./Cache/Lite.php");  
        include_once("./CreateSubDirImg.php");         

	st_engine("../st_engine.txt");

	AssignDataBaseSetting("../config.ini");
// требуется авторизация
	require_once($_SERVER['DOCUMENT_ROOT'].'/inside/auth.php');
//

	ini_set("display_startup_errors", "off");
	ini_set("display_errors", "off");
	ini_set("register_globals", "on");

	include_once("./class.Room.inc"); 
	$room = new Room();	
	
	include_once("./class.company.inc");
	$company = new Company();	
	
	include_once("./class.RoomCategory.inc");
	$roomCategory = new RoomCategory();	
	
	include_once("./class.city.inc");
	$city = new City();	
	
        include_once("./class.templateevent.inc"); 
        $templateEvent = new TemplateEvent();	
	 
        include_once("./observer_filter.php"); 
        //filterSet('room');
	
	require_once("../libs/Smarty.class.php");
	$smarty = new Smarty;
	$smarty->template_dir = "../templates";
	$smarty->compile_check = false;
	$smarty->caching = false;
	$smarty->compile_dir  = "../templates_c";
	$smarty->cache_dir = "../cache";
	$smarty->debugging = false;
	$smarty->clear_all_cache();

        if(isset($_GET['blockip'])) {

            include_once("./class.BlockIP.inc"); 
            $blockIp = new BlockIP();

            if($_GET['blockip'] == 'up') {

                $blockIp->BlockUp(array(
                    'ip' => trim($_GET['ip'])
                ));

            } else if ($_GET['blockip'] == 'down') {

                $blockIp->BlockDown(array(
                    'ip' => trim($_GET['ip'])
                ));

            }
        }        
        
	if(isset($_POST["updatepayment"])) {
		$room->SetStatusPaymentByAdmin(intval($_POST["ID"]), intval($_POST["pay_summ"]), intval($_POST["pay_status"]), $_POST["pay_datetime"], (isset($_POST["pay_datetime_auto"]) ? 1 : 0 ));
	}	
	
	if (isset($_POST["update"])) {
		
            if((int)$_POST["statusId"] == 1) {
                    $templateEvent->EmailEvent(3,6, (int)$_GET["ID"]);
            } else if ( in_array((int)$_POST["statusId"], array(10,12))  ) {
                    $templateEvent->EmailEvent(4,6, (int)$_GET["ID"]);
            } else if (in_array((int)$_POST["statusId"], array(0, 5))) {
                    $templateEvent->EmailEvent(1,6, (int)$_GET["ID"]);  			
            }
		
		
            $room->Id = intval($_GET["ID"]);
            $result = $room->GetItemAdmin();

            // up update cache room
            $options = array(
                    'cacheDir' => "../roomcache/",
                    'lifeTime' => 1
            );

            $cache = new Cache_Lite($options);
            $cache->remove(''.$room->Id.'____roomitem');		
            // up update cache room

		
		
            if(isset($_POST["fio"])) {
                    $result->fio = trim($_POST["fio"]);
            }

            if(isset($_POST["Name"])) {
                    $result->Name = ReplaceSymbolToName(trim($_POST["Name"]));
            }		

            if(isset($_POST["RoomCategoryId"])) {
                    $result->RoomCategoryId = intval($_POST["RoomCategoryId"]);
            }		

            if(isset($_POST["ShortDescription"])) {
                    $result->ShortDescription = TextReplace("ShortDescription", ParsingShortDetails(trim($_POST["ShortDescription"])));
            }	
		
            if(isset($_POST["Description"])) {
                    $result->Description = TextReplace("Description", ParsingShortDetails(trim($_POST["Description"])));
            }	

            if(isset($_POST["area_from"])) {
                    $result->area_from = trim($_POST["area_from"]);
            }	

            if(isset($_POST["area_to"])) {
                    $result->area_to = trim($_POST["area_to"]);
            }	

            if(isset($_POST["cityId"])) {
                    $result->cityId = intval($_POST["cityId"]);
            }	

            if(isset($_POST["streetId"])) {
                    $result->streetId = intval($_POST["streetId"]);
            }	

            if(isset($_POST["metroId"])) {
                    $result->metroId = intval($_POST["metroId"]);
            }	

            if(isset($_POST["cost"])) {
                    $result->cost = str_replace(" ", "",trim($_POST["cost"]));
            }	

            if(isset($_POST["cCostId"])) {
                    $result->cCostId = trim($_POST["cCostId"]);
            }	

            if(isset($_POST["photo_1"])) {
                    $result->photo_1 = trim($_POST["photo_1"]);
            }	

            if(isset($_POST["smallphoto_1"])) {
                    $result->smallphoto_1 = trim($_POST["smallphoto_1"]);
            }	

            if(isset($_POST["statusId"])) {
                    $result->statusId = intval($_POST["statusId"]);
            }	

            if(isset($_POST["fio"])) {
                    $result->fio = trim($_POST["fio"]);
            }	

            if(isset($_POST["phone"])) {
                    $result->phone = PhoneFix(trim($_POST["phone"]));
            }	

            if(isset($_POST["fax"])) {
                    $result->fax = PhoneFix(trim($_POST["fax"]));
            }	
		
		
            if(isset($_POST["email"])) {
                    $result->email = trim($_POST["email"]);
            }	

            if(isset($_POST["latitude"])) {
                    $result->latitude = trim($_POST["latitude"]);
            }	

            if(isset($_POST["longitude"])) {
                    $result->longitude = trim($_POST["longitude"]);
            }	

            if(isset($_POST["Address"])) {
                    $result->Address = ReplaceSymbolToName(trim($_POST["Address"]));
            }	

            if(isset($_POST["zoomId"])) {
                    $result->zoomId = intval($_POST["zoomId"]);
            }	

            if(isset($_POST["middlephoto_1"])) {
                    $result->middlephoto_1 = trim($_POST["middlephoto_1"]);
            }	

            if(isset($_POST["show_area"])) {
                    $result->show_area = trim($_POST["show_area"]);
            }	
		
            if(isset($_POST["brokerId"])) {
                    $result->brokerId = trim($_POST["brokerId"]);
            }		

            if(isset($_POST["SeoTitle"])) {
                    $result->SeoTitle = FixSubTitle(ReplaceSymbolToName(trim($_POST["SeoTitle"])));
            }			

            if (isset($_FILES["smallphoto_1"])) {
                    $newname = time();
                    if (is_uploaded_file($_FILES['smallphoto_1']['tmp_name'])) {
                            $filename = $_FILES['smallphoto_1']['tmp_name'];
                            $ext = substr($_FILES['smallphoto_1']['name'], 1 + strrpos($_FILES['smallphoto_1']['name'], "."));
                            move_uploaded_file($filename, "../sicon/".$newname.".".$ext);
                            $result->smallphoto_1 = $newname.".".$ext;
                    }	
            }
  		
            if (isset($_FILES["middlephoto_1"])) {
                
                $auto_create_path = CreateSubDirImg();

                $newname = time()+1; 
                
                if (is_uploaded_file($_FILES['middlephoto_1']['tmp_name'])) {
                    $filename = $_FILES['middlephoto_1']['tmp_name'];
                    $ext = substr($_FILES['middlephoto_1']['name'], 1 + strrpos($_FILES['middlephoto_1']['name'], "."));
                    move_uploaded_file($filename, "../simg/".$auto_create_path."/".$newname.".".$ext);
                    $result->middlephoto_1 = $auto_create_path."/".$newname.".".$ext;
                }	
            }  				
		
            if(strlen(($result->photo_1)) > 0 && strlen(($result->smallphoto_1)) <= 0) {

    		$dataImg["ID"] = $_GET["ID"];
    		$nImgFile = split("\.", $result->photo_1);
    	
    		// up resizing photo icon on list
    		$sImgFile = CreateDefaultUserIcon("../simg/".$result->photo_1, "../sicon/r".$_GET["ID"]);    	
    		$splImgFile= split("\/", $sImgFile);
    		$dataImg["defaultUserIcon"] = $splImgFile[sizeof($splImgFile)-1];
    		$result->smallphoto_1 = $dataImg["defaultUserIcon"];
    		// down resizing photo icon on list
            }  		
  		
            if(strlen(($result->photo_1)) > 0 && strlen(($result->middlephoto_1)) <= 0) {

                $auto_create_path = CreateSubDirImg();

                $sImgFile = RatioResizeImg300("../simg/".$result->photo_1, "../simg/".$auto_create_path."/".(time()+5));
                $splImgFile= split("\/", $sImgFile);
                $dataImg["sImg1ID"] = $auto_create_path."/".$splImgFile[sizeof($splImgFile)-1];
                $result->middlephoto_1 = $dataImg["sImg1ID"];

            }
		
            $result->UpdateItem();
	} 
    
	if (isset($_GET["ID"]) && intval(isset($_GET["ID"])) > 0) {
		
		$room->Id = intval($_GET["ID"]);
		//echo $room->Id;
		$result = $room->GetItemAdmin();
		//var_dump($result);
		$smarty->assign("data", $result);
		
		$companies = $company->ShortSelect();
		$smarty->assign("companies", $companies);

		$resultRoomCategory = $roomCategory->Select();
		$smarty->assign("dataroomcategory", $resultRoomCategory);
		
		$listCity = $city->SelectForAdmin($data); 
		$smarty->assign("listCity", $listCity);
		
	} else {

		$data = array();
        $dataPage = array(); 
        $pagesplit = 30;

        if (isset($_GET["offset"])) {
        	$room->offset = intval($_GET["offset"]);
        } else {
        	$room->offset = 0;
        }
        if (isset($_GET["rowCount"])) { 
        	$room->rowCount = intval($_GET["rowCount"]);
        } else {
        	$room->rowCount = $pagesplit;
        }

		if (isset($_GET["StatusID"])) {
			$room->statusId = intval($_GET["StatusID"]);
		}    	
        
        
        $room->sort = "order by Room.Id desc";
        $data["sort"] = "ID";		
		
        $result = $room->Select();
		$smarty->assign("data", $result);
	}
	
	AssignDescription("../config.ini");	
	
	$resultPage = $room->CountSelect();

	$smarty->assign("CountRecord", $resultPage);
	$smarty->assign("CountSplit", $pagesplit);
	$smarty->assign("CountPage", ceil($resultPage/30));	

	?>
     <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
     <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">	
	 <LINK href="../general.css" type="text/css" rel="stylesheet">
	 <meta http-equiv="content-type" content="text/html; charset=windows-1251"/>     
	 <body>
	<?
	
	$smarty->display("./pagesplit.tpl", $_SERVER["REQUEST_URI"]);	
	
	$smarty->display("./selectroom.tpl");

	$smarty->display("./room/update.tpl");
	
	$smarty->display("./selectroom.tpl");
	
	$smarty->display("./pagesplit.tpl", $_SERVER["REQUEST_URI"]);	
	
	?>
	</body>
	</html>
	<?
?>