<?php
    include_once("class.db.inc");  


    class HistoryAutoUpdate {

        public $db = '';
        public $sql = '';
        public $typeId = '';
        public $codeId = 0;
        public $date = '';
 
        function __construct() {
                global $DATABASE;
                $this->InitDB();
        }

        function InitDB() {
                global $DATABASE;
                $this->db = new DB($DATABASE["HOSTNAME"], $DATABASE["DATABASENAME"], $DATABASE["DATABASEUSER"], $DATABASE["DATABASEPASSWORD"]);
        }

        function Insert() {
            
            $this->sql = "insert into HistoryAutoUpdate (`typeId`, `codeId`, `date`) values ('".$this->typeId."', ".$this->codeId.", NOW());";
            $result = $this->db->Insert($this->sql);
        }
        
        function Select($aParams = array()) {
            
            $_array = array();
            
            if(count($aParams)) {
                
                 $where = ' where 1 ';
                 if(isset($aParams['codeId'])) {
                     $where .= ' and codeId='.$aParams['codeId'].' ';
                 }
                 
                 if(isset($aParams['brokerId']) && $aParams['brokerId'] > 0) {
                    $this->sql = "select HistoryAutoUpdate.*, company.companyName from HistoryAutoUpdate inner join  Sell on (Sell.ID = HistoryAutoUpdate.codeId and Sell.brokerId=".$aParams['brokerId'].") inner join  company on (company.id = Sell.brokerId) ".$where." order by HistoryAutoUpdate.Id desc limit 40;"; 
                 } else {
                    $this->sql = "select HistoryAutoUpdate.*, company.companyName from HistoryAutoUpdate  inner join  Sell on (Sell.ID = HistoryAutoUpdate.codeId) left join company on (company.id = Sell.brokerId) ".$where." order by HistoryAutoUpdate.Id desc limit 40";
                 }
            } else {
                $this->sql = "select HistoryAutoUpdate.*, company.companyName from HistoryAutoUpdate  inner join  Sell on (Sell.ID = HistoryAutoUpdate.codeId) left join  company on (company.id = Sell.brokerId) where 1 order by Id desc limit 40;";
            }
             
            $result = $this->db->Select($this->sql);
            while ($row = mysql_fetch_object($result)) {
                
                if($row->companyName === NULL) {
                    $row->companyName = 'Собственник';
                }
                
                $_array[] = array(
                    'Id' => $row->Id,
                    'typeId' => $row->typeId,
                    'codeId' => $row->codeId,
                    'companyName' => $row->companyName,
                    'date' =>  strftime("%H:%M  -  %d %B %Y",  strtotime($row->date)),
                );
            }
            
            return $_array;
        }
        
        
    }
 
?>
