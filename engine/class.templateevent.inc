<?
include_once("class.db.inc");  	
require_once('class.phpmailer.php');

class TemplateEvent {

	
	private $_db;
	public $_sql;		
	public $_typeEventId;
	public $_typeEventObjectId;
	public $_template;
	public $_title;
	public $_Id;
	public $_img;
        public $_actionId = 0;
	public $_countdays;
	public $_isBrokerId = 0;
	public $_statusId = 1;
	public $_cityListIds = array();
 
	public $_sumpayment = 0;
	
	
	function __construct() {
		global $DATABASE;
		$this->InitDB();
	}

	function InitDB() {
		global $DATABASE;
		$this->db = new DB($DATABASE["HOSTNAME"], $DATABASE["DATABASENAME"], $DATABASE["DATABASEUSER"], $DATABASE["DATABASEPASSWORD"]);
	}

	function __destruct() {
			
	}		
	
        function reconnect() {

            mysql_close($this->db->link);
            $this->InitDB();
        }
        
	private function GetEnableEmailEvent($typeEventId, $typeEventObjectId, $codeId, $countdays) {

		
		$isBrokerId = 0;
		$cityId = 0;
		
		if($typeEventObjectId == 1 /* sell */) {
			$this->_sql = " select brokerId, CityID as 'cityId' from Sell where ID=".(int)$codeId."; ";
			
			//echo $this->_sql."<hr>";
			
		} else if ($typeEventObjectId == 2  /* buy */) {
			$this->_sql = " select brokerId, regionID as 'cityId' from buyer where ID=".(int)$codeId."; ";
		} else if ($typeEventObjectId == 3  /* invest */) {
			$this->_sql = " select brokerId, region_id  as 'cityId' from investments where ID=".(int)$codeId."; ";
		} else if ($typeEventObjectId == 4  /* equipment */) {
			$this->_sql = " select brokerId, city_id as 'cityId' from equipment where ID=".(int)$codeId."; ";
		} else if ($typeEventObjectId == 5  /* coop */) {
			$this->_sql = " select brokerId, region_id as 'cityId'  from coopbiz where ID=".(int)$codeId."; ";
		} else if ($typeEventObjectId == 6  /* room */) {
			$this->_sql = " select brokerId, cityId from Room where ID=".(int)$codeId."; ";
		} else {
			$this->_sql = " select 0 as 'brokerId', 0 as 'cityId';";
		}
		$result = $this->db->Select($this->_sql);
		while ($row = mysql_fetch_object($result)) {
			if(intval($row->brokerId) > 0) {
				$isBrokerId = 1;		
			}
			if(intval($row->cityId) > 0) {
				$cityId = (int)$row->cityId;
			}
		}

		$resultFound = false;
		$useCity = false;
		
		if($cityId > 0) {
			$this->_sql = "select statusId from  TemplateEvent where typeEventId=".intval($typeEventId)." and typeEventObjectId=".intval($typeEventObjectId)." and isBrokerId='".$isBrokerId."' and countdays=".(int)$countdays." and  (cityListIds like '%,".$cityId.",%' or cityListIds like '".$cityId.",%' or cityListIds like '%,".$cityId."' or  cityListIds = '".$cityId."')    ";			
			//echo  " city >0 ".$this->_sql."<br>";
			$result = $this->db->Select($this->_sql);
			while ($row = mysql_fetch_object($result)) {
				$resultFound = (boolean) $row->statusId;
				$useCity = true;
			}
		}
		
		if(!$resultFound) {
		
			$this->_sql = "select statusId from  TemplateEvent where typeEventId=".intval($typeEventId)." and typeEventObjectId=".intval($typeEventObjectId)." and isBrokerId='".$isBrokerId."' and countdays=".(int)$countdays." ";
			//echo  " city<0 ".$this->_sql."<br>";
			
			$result = $this->db->Select($this->_sql);	
			while ($row = mysql_fetch_object($result)) {
				$resultFound = (boolean) $row->statusId;
			}
		}
		 
		return  array('enabled' => $resultFound, 'cityenabled' => $useCity);
	}
	
	public function EmailEvent($typeEventId, $typeEventObjectId, $codeId, $statusSend = true, $countdays = 0) {
		
		$contentGenerated = new stdClass();
		
		$aEnabled = $this->GetEnableEmailEvent($typeEventId, $typeEventObjectId, $codeId, $countdays);
		
		if(!$aEnabled['enabled']) return;

		
		$email_customer = '';
		$fml_customer   = '';
		$nameads_customer   = '';
		
		$email_bizzona = '';
		$phone_bizzona = '';
		
		$template_customer = '';
		$title_customer = '';
		$login_customer = '';
		$password_customer  = '';
		$city_customer = '';
		$category_customer = '';
		$guid_customer = '';
		$isBroker = 0;
		
		$_countdays = 0;
		
                $ActionName = '';
                $CategoryName = '';
                
                $commentmoderator = '';
                
		$this->_sql = 'select supportPhone, supportEmail from ContactInfo;';
		$result = $this->db->Select($this->_sql);	
		while ($row = mysql_fetch_object($result)) {
			$email_bizzona = $row->supportEmail;	
			$phone_bizzona = $row->supportPhone;
		}
		
                // up comment moderatort for updating
                if($typeEventObjectId == 1 && in_array($typeEventId, array(9, 10))) {
                    $this->_sql = 'select comment from ApprovingSell where typeId=\'sell\' and codeId='.$codeId.' limit 1;';
                    $result = $this->db->Select($this->_sql);
                    while ($row = mysql_fetch_object($result)) {
                       $commentmoderator = $row->comment;
                    }
                }
                // down comment moderatort for updating
                
                // up closed action
                if($this->_actionId > 0 && $typeEventId == 8) {
                    
                    $this->_sql = "select Action.name as ActionName, CategoryAction.name as CategoryName  from Action, CategoryAction where CategoryAction.id=Action.categoryActionId and  Action.id=".(int)$this->_actionId.";";
                    $result = $this->db->Select($this->_sql);	
                    
                    while ($row = mysql_fetch_object($result)) {
                    
                        $ActionName = $row->ActionName;
                        $CategoryName = $row->CategoryName;
                        
                    }
                }
                // down closed action
                
                
		// up sell
		if($typeEventObjectId == 1) {
			$this->_sql = 'select FML, brokerId,  EmailID, NameBizID, CityID, CategoryID, login, password from Sell where ID='.intval($codeId).';';
			
			//echo $this->_sql;
			
			$result = $this->db->Select($this->_sql);	
			while ($row = mysql_fetch_object($result)) {
				$email_customer = $row->EmailID;		
				$fml_customer = $row->FML;
				$nameads_customer = $row->NameBizID;
				$city_customer = $row->CityID;
				$category_customer = $row->CategoryID;
				$login_customer = $row->login;
				$password_customer = $row->password;
				$isBroker = ( (int)$row->brokerId > 0 ? 1 : 0);
			}
		}
		// down sell
		
		// up buy
		if($typeEventObjectId == 2) {
			$this->_sql = 'select FML, brokerId, email, name, regionID, typeBizID, GUID from buyer where id='.intval($codeId).';';
			//echo $this->_sql; 
			
			$result = $this->db->Select($this->_sql);	
			while ($row = mysql_fetch_object($result)) {
				$email_customer = $row->email;		
				$fml_customer = $row->FML;
				$nameads_customer = $row->name;
				$city_customer = $row->regionID;
				$category_customer = $row->typeBizID;
				$guid_customer = $row->GUID;
				$isBroker = ( (int)$row->brokerId > 0 ? 1 : 0);
			}
		}		
		// down buy
		
		// up invest
		if($typeEventObjectId == 3) {
			$this->_sql = 'select contactface, brokerId, contactemail, name, region_id, category_id  from investments where id='.intval($codeId).';';
			$result = $this->db->Select($this->_sql);	
			while ($row = mysql_fetch_object($result)) {
				$email_customer = $row->contactemail;		
				$fml_customer = $row->contactface;
				$nameads_customer = $row->name;
				$city_customer = $row->region_id;
				$category_customer = $row->category_id;
				$isBroker = ( (int)$row->brokerId > 0 ? 1 : 0);
			}
		}		
		// down invest		

		// up equipment
		if($typeEventObjectId == 4) {
			$this->_sql = 'select contact_face, brokerId, email, name, city_id, category_id  from equipment where id='.intval($codeId).';';
			$result = $this->db->Select($this->_sql);	
			while ($row = mysql_fetch_object($result)) {
				$email_customer = $row->email;		
				$fml_customer = $row->contact_face;
				$nameads_customer = $row->name;
				$city_customer = $row->city_id;
				$category_customer = $row->category_id;
				$isBroker = ( (int)$row->brokerId > 0 ? 1 : 0);
			}
		}		
		// down equipment			
		

		// up coop
		if($typeEventObjectId == 5) {
			$this->_sql = 'select contact_face,  brokerId, contact_email, name, region_id, category_id  from coopbiz where id='.intval($codeId).';';
			$result = $this->db->Select($this->_sql);	
			while ($row = mysql_fetch_object($result)) {
				$email_customer = $row->contact_email;		
				$fml_customer = $row->contact_face;
				$nameads_customer = $row->name;
				$city_customer = $row->region_id;
				$category_customer = $row->category_id;
				$isBroker = ( (int)$row->brokerId > 0 ? 1 : 0);
			}
		}		
		// down coop		
		
		
		// up room
		if($typeEventObjectId == 6) {
			$this->_sql = 'select fio, email, brokerId, ShortDescription, cityId, RoomCategoryId  from Room where id='.intval($codeId).';';
			$result = $this->db->Select($this->_sql);	
			while ($row = mysql_fetch_object($result)) {
				$email_customer = $row->email;		
				$fml_customer = $row->fio;
				$nameads_customer = substr($row->ShortDescription, 0, 50 ). " ... ";
				$city_customer = $row->cityId;
				$category_customer = $row->RoomCategoryId;
				$isBroker = ( (int)$row->brokerId > 0 ? 1 : 0);
			}
		}		
		// down room		
		
		
		if(1/*strlen(trim($email_customer)) > 0*/) {

			if((boolean)$aEnabled['cityenabled']) {
				$this->_sql = 'select template, title, countdays from TemplateEvent where typeEventId='.intval($typeEventId).' and typeEventObjectId='.intval($typeEventObjectId).' and isBrokerId=\''.$isBroker.'\'  and  statusId=\'1\' and countdays='.(int)$countdays.'  and  (cityListIds like \'%,'.$city_customer.',%\' or cityListIds like \''.$city_customer.',%\' or cityListIds like \'%,'.$city_customer.'\' or  cityListIds = \''.$city_customer.'\')     ;';
			} else {
				$this->_sql = 'select template, title, countdays from TemplateEvent where typeEventId='.intval($typeEventId).' and typeEventObjectId='.intval($typeEventObjectId).' and isBrokerId=\''.$isBroker.'\'  and  statusId=\'1\' and countdays='.(int)$countdays.' and length(cityListIds) <= 0 ;';
			}
			
			//echo $this->_sql."<hr>";
			
			$result = $this->db->Select($this->_sql);	
			while ($row = mysql_fetch_object($result)) {
				
				$template_customer = $row->template;
				$title_customer = $row->title;
				$_countdays = ($row->countdays > 0 ? $row->countdays : 0);
			
				$template_customer = str_replace("{{CODE}}", $codeId, $template_customer);
				$template_customer = str_replace("{{FIO}}", $fml_customer, $template_customer);
				$template_customer = str_replace("{{EMAIL}}", $email_bizzona, $template_customer);
				$template_customer = str_replace("{{PHONE}}", $phone_bizzona, $template_customer);
				$template_customer = str_replace("{{NAME}}", $nameads_customer, $template_customer);
			
				$template_customer = str_replace("{{LOGIN}}", $login_customer, $template_customer);
				$template_customer = str_replace("{{PASSWORD}}", $password_customer, $template_customer);
				$template_customer = str_replace("{{CITY}}", $city_customer, $template_customer);
				$template_customer = str_replace("{{CATEGORY}}", $category_customer, $template_customer);
				$template_customer = str_replace("{{GUID}}", $guid_customer, $template_customer);
				$template_customer = str_replace("{{SUM}}", $this->_sumpayment, $template_customer);
			
                                $template_customer = str_replace("{{ACTIONNAME}}", $ActionName, $template_customer);
                                $template_customer = str_replace("{{SERVICENAME}}", $CategoryName, $template_customer);
                                
                                $template_customer = str_replace("{{COMMENTMODERATOR}}", $commentmoderator, $template_customer);
                                
				
				$title_customer = str_replace("{{CODE}}", $codeId, $title_customer);
				$title_customer = str_replace("{{FIO}}", $fml_customer, $title_customer);
				$title_customer = str_replace("{{EMAIL}}", $email_bizzona, $title_customer);
				$title_customer = str_replace("{{PHONE}}", $phone_bizzona, $title_customer);
				$title_customer = str_replace("{{NAME}}", $nameads_customer, $title_customer);
			
				$title_customer = str_replace("{{LOGIN}}", $login_customer, $title_customer);
				$title_customer = str_replace("{{PASSWORD}}", $password_customer, $title_customer);
				$title_customer = str_replace("{{CITY}}", $city_customer, $title_customer);
				$title_customer = str_replace("{{CATEGORY}}", $category_customer, $title_customer);
				$title_customer = str_replace("{{GUID}}", $guid_customer, $title_customer);
				$title_customer = str_replace("{{SUM}}", $this->_sumpayment, $title_customer);
				
                                $title_customer = str_replace("{{ACTIONNAME}}", $ActionName, $title_customer);
                                $title_customer = str_replace("{{SERVICENAME}}", $CategoryName, $title_customer);
                                $title_customer = str_replace("{{COMMENTMODERATOR}}", $commentmoderator, $title_customer);

				$mail = new PHPMailer();
                                
				$mail->CharSet = "windows-1251";
				$mail->IsSendmail();
			
				$mail->AddReplyTo($email_bizzona,"������ - ����");
				$mail->SetFrom($email_bizzona, '������ - ����');			
			
                                $mail->AddAddress($email_bizzona);
                                $mail->AddAddress($email_customer);			
			
				$title_customer = html_entity_decode(strip_tags($title_customer));
			
				$mail->Subject    = $title_customer;
				$mail->MsgHTML($template_customer);
			
				$contentGenerated->Subject = $title_customer;
				$contentGenerated->MsgHTML = $template_customer;
				$contentGenerated->EmailCustomer = $email_customer;
			
				if($statusSend) {
                                    $mail->Send();
                                    $this->SaveHistory($typeEventId, $typeEventObjectId, $_countdays, $codeId, $isBroker, ($city_customer>0 ? $city_customer : 0), (isset($email_customer) ? $email_customer : ''), (strlen($mail->ErrorInfo)>0 ? 0 : 1));
				}
			
			}
		}
		
		return $contentGenerated;
	}
	
	public function SendRepeat($Subject, $MsgHTML, $EmailCustomer) {
		
		$email_bizzona = "bizzona.ru@gmail.com";
		
		$this->_sql = 'select supportPhone, supportEmail from ContactInfo;';
		$result = $this->db->Select($this->_sql);	
		while ($row = mysql_fetch_object($result)) {
			$email_bizzona = $row->supportEmail;	
		}
		
		$mail = new PHPMailer();
		$mail->CharSet = "windows-1251";
		$mail->IsSendmail();
			
		$mail->AddReplyTo($email_bizzona,"������ - ����");
		$mail->SetFrom($email_bizzona, '������ - ����');			
		$mail->AddAddress($EmailCustomer);
		$mail->Subject = $Subject;
		$mail->MsgHTML($MsgHTML);
		if($mail->Send()) {
			return true;
		} else {
			return false;
		}
	}
	
	public function Insert() {
		
		$this->_sql = " insert into TemplateEvent (	typeEventId, 
													typeEventObjectId, 
													template, 
													title, 
													countdays, 
													statusId, 
													isBrokerId,
													cityListIds) 
		                                             values (".$this->_typeEventId.", 
		                                                     ".$this->_typeEventObjectId.",
		                                                     '".$this->_template."', 
		                                                     '".$this->_title."',
		                                                     ".intval($this->_countdays).",
		                                                     '".$this->_statusId."',
		                                                     '".$this->_isBrokerId."',
		                                                     '".$this->_cityListIds."');";
		$result = $this->db->Insert($this->_sql);	
		return $result;
	}
	
	public function Update() {
		
		if((int)$this->_Id > 0) {
			
			$this->_sql = 'update TemplateEvent set  typeEventObjectId=\''.intval($this->_typeEventObjectId).'\', typeEventId=\''.intval($this->_typeEventId).'\', isBrokerId=\''.$this->_isBrokerId.'\', statusId=\''.$this->_statusId.'\',  countdays=\''.$this->_countdays.'\', title=\''.addslashes($this->_title).'\' ,template=\''.addslashes($this->_template).'\', cityListIds=\''.$this->_cityListIds.'\' where Id='.intval($this->_Id).' limit 1 ';	
			$result = $this->db->Sql($this->_sql);	
		}
		
	}
	
	public function GetItem() {

		$obj = new stdclass();		
		
		$this->_sql = 'select TemplateEvent.cityListIds, TemplateEvent.typeEventObjectId, isBrokerId, countdays, statusId, template, title, TypeEvent.Id as "TypeEventId",   TypeEvent.name as "TypeEventName", TypeEventObject.name as "TypeEventObjectName" from TemplateEvent, TypeEvent, TypeEventObject  where TemplateEvent.Id='.intval($this->_Id).' and TypeEvent.Id=TemplateEvent.typeEventId and TemplateEvent.typeEventObjectId=TypeEventObject.Id; ';

		$result = $this->db->Select($this->_sql);
		while ($row = mysql_fetch_object($result)) {
			$obj->template =  $row->template;
			$obj->title =  $row->title;
			$obj->TypeEventId =  $row->TypeEventId;
			$obj->TypeEventObjectId =  $row->typeEventObjectId;
			$obj->TypeEventName =  $row->TypeEventName;
			$obj->TypeEventObjectName =  $row->TypeEventObjectName;
			$obj->countdays =  $row->countdays;
			$obj->statusId =  $row->statusId;
			$obj->isBrokerId =  $row->isBrokerId;
			$obj->alistCityIds = explode(',', $row->cityListIds);
			return $obj;
		}
		return $obj;
	}
	
	public function Select($data) {
			
		$order = ' ';
		$this->_sql = 'select  TemplateEvent.cityListIds, TemplateEvent.typeEventId, TemplateEvent.isBrokerId, TemplateEvent.countdays, TemplateEvent.Id, TemplateEvent.statusId, TypeEvent.img, TemplateEvent.title, TypeEvent.name as "TypeEventName", TypeEventObject.name as "TypeEventObjectName" from TemplateEvent, TypeEvent, TypeEventObject where TemplateEvent.typeEventId = TypeEvent.Id and TemplateEvent.typeEventObjectId=TypeEventObject.Id  '.$order.'';
		
		//echo $this->_sql;
		
		$result = $this->db->Select($this->_sql);
			
		$arr = array();	

		while ($row = mysql_fetch_object($result)) {
				
			$obj = new stdclass();
				
			$obj->Id = $row->Id;
			$obj->img = $row->img;
			$obj->TypeEventName = $row->TypeEventName;
			$obj->Title = $row->title;
			$obj->TypeEventObjectName = $row->TypeEventObjectName;
			$obj->statusId = $row->statusId;
			$obj->isBrokerId = $row->isBrokerId;
			$obj->cityListIds = $row->cityListIds;
			if($row->typeEventId == 5) {
				$obj->countdays = $row->countdays;
			} else {
				$obj->countdays = "-----";
			}
				
			$acityname = array();
			$acity = $data['acity'];
			$e_acity = explode(',', $obj->cityListIds);
			while (list($k, $v) = each($e_acity)) {
				while (list($kc, $vc) = each($acity)) {
					if($vc['parent'] == $v) {
						array_push($acityname, $vc['name']);
						break;
					}
				}
				reset($acity);
			}
			$obj->listCityStr = implode('<br>', $acityname);
			
			
			
			array_push($arr, $obj);
				
		}
			
		return $arr;
	}	
	
	private function SaveHistory($typeEventId, $typeEventObjectId, $countdays, $objectId, $isBroker, $cityId = 0, $email = '', $resSend = '') {
 		 
		$this->_sql = " insert into HistoryTemplateEvent (`typeEventId`, `typeEventObjectId`, `countdays`, `objectId`, `datetime`, `isBrokerId`, `cityId`, `email`, `resultStatusSend`)
		                      values (".$typeEventId.", ".$typeEventObjectId.", ".$countdays.", ".$objectId.", NOW(), '".$isBroker."', ".$cityId.", '".$email."', '".$resSend."') ";
		//echo  $this->_sql;
		$this->db->Sql($this->_sql);
		
	}
	
	public function GetLastEventObjectId($aParameters) {

		$this->_sql  = " select max(objectId) as 'objectId' from HistoryTemplateEvent where typeEventId=".intval($aParameters['typeEventId'])." and typeEventObjectId=".intval($aParameters['typeEventObjectId'])." and countdays=".intval($aParameters['countdays'])." and isBrokerId='".$aParameters['isBrokerId']."'  ".(sizeof($aParameters['cityListIds'])>0 ? " and cityId in (".implode(',',$aParameters['cityListIds']).")  " : "")." ;";
		
		//echo "<br>".$this->_sql." <br>";
		
		$result = $this->db->Select($this->_sql);
		while ($row = mysql_fetch_object($result)) {
			return $row->objectId;
		}
		
		return 0;
	}

	public function GetObjectIdForEvent($typeEventObjectId, $objectId, $minDays = 5, $maxDays = 6, $isBrokerId = 0, $cityListIds=array()) {
		
		$codeId = 0;
		
		if ($typeEventObjectId == 1 /* sell */) {
			$this->_sql  =  "select ID from Sell where ID > ".intval($objectId)." and StatusID = 1 and (DATEDIFF(NOW(), DateStart) between ".$minDays." and ".$maxDays.")  ".($isBrokerId > 0 ? " and brokerId > 0  " : " and (brokerId <= 0 or brokerId is null) ")."  ".(sizeof($cityListIds)>0 ? " and  CityID in (".implode(',', $cityListIds).") " : "")."  limit 1;";
		} else if ($typeEventObjectId == 2 /* buy */) {
			$this->_sql  = " select id as 'ID' from buyer where id > ".intval($objectId)." and StatusID = 1 and (DATEDIFF(NOW(), datecreate) between ".$minDays." and ".$maxDays.") ".($isBrokerId > 0 ? " and brokerId > 0  " : " and (brokerId <= 0 or brokerId is null)")."  ".(sizeof($cityListIds)>0 ? " and  regionID in (".implode(',', $cityListIds).") " : "")."  limit 1; ";
		} else if ($typeEventObjectId == 3 /* invest */) {
			$this->_sql  = " select id as 'ID' from investments where id > ".intval($objectId)." and status_id = 1 and (DATEDIFF(NOW(), datecreate) between ".$minDays." and ".$maxDays.") ".($isBrokerId > 0 ? " and brokerId > 0  " : " and (brokerId <= 0 or brokerId is null)")."  ".(sizeof($cityListIds)>0 ? " and region_id in (".implode(',', $cityListIds).") " : "")."  limit 1;  ";
		} else if ($typeEventObjectId == 4 /* equipmnet */) {
			$this->_sql  = " select id as 'ID' from equipment where id > ".intval($objectId)." and status_id = 1 and (DATEDIFF(NOW(), datecreate) between ".$minDays." and ".$maxDays.") ".($isBrokerId > 0 ? " and brokerId > 0  " : " and (brokerId <= 0 or brokerId is null)")."  ".(sizeof($cityListIds)>0 ? " and city_id in (".implode(',', $cityListIds).") " : "")."  limit 1;  ";
		} else if ($typeEventObjectId == 5 /* coop */) {
			$this->_sql  = " select id as 'ID' from coopbiz where id > ".intval($objectId)." and status_id = 1 and (DATEDIFF(NOW(), datecreate) between ".$minDays." and ".$maxDays.") ".($isBrokerId > 0 ? " and brokerId > 0  " : " and (brokerId <= 0 or brokerId is null)")."  ".(sizeof($cityListIds)>0 ? " and region_id in (".implode(',', $cityListIds).") " : "")."  limit 1;  "; 
		} else if ($typeEventObjectId == 6 /* room */) {
			$this->_sql  =  " select id as 'ID' from Room where id > ".intval($objectId)." and statusid = 1 and (DATEDIFF(NOW(), datecreate) between ".$minDays." and ".$maxDays.") ".($isBrokerId > 0 ? " and brokerId > 0  " : " and (brokerId <= 0 or brokerId is null)")."  ".(sizeof($cityListIds)>0 ? " and cityId in (".implode(',', $cityListIds).") " : "")."  limit 1;  ";
		}

		$result = $this->db->Select($this->_sql);
		while ($row = mysql_fetch_object($result)) {
			$codeId = $row->ID;
		}
		
		return $codeId;
	}
	
	public function AutoInitEvent($aParameters) {
		
		$lastCodeId = $this->GetLastEventObjectId($aParameters);
		$codeId = $this->GetObjectIdForEvent($aParameters['typeEventObjectId'], $lastCodeId, $aParameters['minDays'], $aParameters['maxDays'], $aParameters['isBrokerId'], $aParameters['cityListIds'] );
		
		$this->EmailEvent($aParameters['typeEventId'], $aParameters['typeEventObjectId'], $codeId, true, $aParameters['countdays']);
	}
	
	public function SelectHistory($typeeventobjectId, $objectId) {
		
		$array = array();
		
		$this->_sql  = "select HistoryTemplateEvent.Id, HistoryTemplateEvent.resultStatusSend, HistoryTemplateEvent.email, TypeEvent.name, img, countdays, datetime from HistoryTemplateEvent inner join TypeEvent on (HistoryTemplateEvent.typeEventId=TypeEvent.Id) where typeEventObjectId=".$typeeventobjectId." and objectId=".$objectId.";";
		$result = $this->db->Select($this->_sql);
		
		while ($row = mysql_fetch_object($result)) {
			
			$obj = new stdclass();
			$obj->name = $row->name;
			$obj->Id = $row->Id;
			$obj->img = $row->img;
			$obj->countdays = $row->countdays;
                        $obj->email = $row->email;
                        $obj->resultStatusSend = $row->resultStatusSend;
			$obj->dateseconds = strtotime($row->datetime);
			
						
			array_push($array, $obj);
		}
		
		return $array;
	}
	
	public function GetRepeatTemplate($typeeventobjectId, $objectId) {
		
		$typeEventId = 0;
		
		$objectStatus = 0;
		
		
		if($typeeventobjectId == 1 /* sell */) {
			
			$this->_sql  =  "select StatusID from Sell where ID=".intval($objectId)."";	
			$result = $this->db->Select($this->_sql);
			
			while ($row = mysql_fetch_object($result)) {
				$objectStatus = $row->StatusID;
			}
			
			if($objectStatus == 1 /* ������������ */) {
				$typeEventId = 3;		
			} else if (in_array($objectStatus, array(10, 12))  /* �������, ������� */) {
				$typeEventId = 4;
			} else if (in_array($objectStatus, array(0, 5)) /* �����������, ����� */) {
				$typeEventId = 1;
			}
			
		} else if ($typeeventobjectId == 2 /* buy */) {
			
			$this->_sql = "select statusID from buyer where id=".intval($objectId)."";
			$result = $this->db->Select($this->_sql);
			while ($row = mysql_fetch_object($result)) {
				$objectStatus = $row->statusID;
			}			
			
			if($objectStatus == 1 /* ������������ */) {
				$typeEventId = 3;		
			} else if ($objectStatus == 2 /* ������� */) {
				$typeEventId = 4;	
			} else if (in_array($objectStatus, array(0, 3)) /* �����������, ����� */) {
				$typeEventId = 1;
			}
			
		} else if ($typeeventobjectId == 3 /* investments */) {
			
			$this->_sql = "select status_id  from investments where id=".intval($objectId)."";
			$result = $this->db->Select($this->_sql);
			while ($row = mysql_fetch_object($result)) {
				$objectStatus = $row->status_id;
			}			
			
			if($objectStatus == 1 /* ������������ */) {
				$typeEventId = 3;		
			} else if ($objectStatus == 2 /* ������� */) {
				$typeEventId = 4;	
			} else if (in_array($objectStatus, array(0, 3)) /* �����������, ����� */) {
				$typeEventId = 1;
			}			
			
		} else if ($typeeventobjectId == 4 /* equipment */) {

			$this->_sql = "select status_id from equipment where id=".intval($objectId)."";
			$result = $this->db->Select($this->_sql);
			while ($row = mysql_fetch_object($result)) {
				$objectStatus = $row->status_id;
			}			
			
			if($objectStatus == 1 /* ������������ */) {
				$typeEventId = 3;		
			} else if ($objectStatus == 2 /* ������� */) {
				$typeEventId = 4;	
			} else if (in_array($objectStatus, array(0, 3)) /* �����������, ����� */) {
				$typeEventId = 1;
			}			
			
		} else if ($typeeventobjectId == 5 /* coopbiz */) {
			
			$this->_sql = "select status_id from coopbiz where id=".intval($objectId)."";
			$result = $this->db->Select($this->_sql);
			while ($row = mysql_fetch_object($result)) {
				$objectStatus = $row->status_id;
			}			

			if($objectStatus == 1 /* ������������ */) {
				$typeEventId = 3;		
			} else if ($objectStatus == 2 /* ������� */) {
				$typeEventId = 4;	
			} else if (in_array($objectStatus, array(0, 3)) /* �����������, ����� */) {
				$typeEventId = 1;
			}			
			
		} else if ($typeeventobjectId == 6 /* room */) {

			$this->_sql = "select statusId from Room where id=".intval($objectId)."";
			$result = $this->db->Select($this->_sql);
			while ($row = mysql_fetch_object($result)) {
				$objectStatus = $row->statusId;
			}			
			
			if($objectStatus == 1 /* ������������ */) {
				$typeEventId = 3;		
			} else if (in_array($objectStatus, array(10, 12)) /* ������� */) {
				$typeEventId = 4;	
			} else if (in_array($objectStatus, array(0, 5)) /* �����������, ����� */) {
				$typeEventId = 1;
			}				
			
			
		}
		
		$content = $this->EmailEvent($typeEventId, $typeeventobjectId, $objectId, false);
		
		return $content;
	}

	public function ListTypeEvent() {
		
		$arr = array();
		
		$this->_sql = "select Id, name from TypeEvent";
		$result = $this->db->Select($this->_sql);
		while ($row = mysql_fetch_object($result)) {
			
			array_push($arr, array('Id' => $row->Id, 'name' => $row->name));
			
		}
		
		return $arr;
	}
	
	public function ListTypeEventObject() {
		
		$arr = array();
		
		$this->_sql = "select Id, name from TypeEventObject";
		$result = $this->db->Select($this->_sql);
		while ($row = mysql_fetch_object($result)) {
			
			array_push($arr, array('Id' => $row->Id, 'name' => $row->name));
			
		}
		
		return $arr;
	}
	
	public function GetListCountdays($typeEventId=5) {
		
		$arr = array();
		
		$this->_sql = "select countdays, cityListIds from TemplateEvent where typeEventId=".(int)$typeEventId." and statusId=1;";
		$result = $this->db->Select($this->_sql);
		
		while ($row = mysql_fetch_object($result)) {
			$arr[] = array('countdays' => $row->countdays, 'cityListIds' => explode(',', $row->cityListIds));
		}
		
		return $arr;
	}
	
}
?>