<?php

    global $DATABASE;

    include_once("../phpset.inc");
    include_once("./functions.inc"); 

    AssignDataBaseSetting("../config.ini");
// требуется авторизация
	require_once($_SERVER['DOCUMENT_ROOT'].'/inside/auth.php');
//

    ini_set("display_startup_errors", "off");
    ini_set("display_errors", "off");
    ini_set("register_globals", "on");

    include_once("./class.review.inc"); 
    $review = new Review();
    
    require_once("../libs/Smarty.class.php");
    $smarty = new Smarty;
    $smarty->template_dir = "../templates";
    $smarty->compile_check = false;
    $smarty->caching = false;
    $smarty->compile_dir  = "../templates_c";
    $smarty->cache_dir = "../cache";
    $smarty->debugging = false;
    $smarty->clear_all_cache();
    
    
    if(isset($_POST['update'])) {
    
        $review->_id = (int) $_POST['edit'];
        $review->_fio = $_POST['fio'];
        $review->_name = $_POST['name'];
        $review->_comment = $_POST['comment'];
        $review->_statusId = $_POST['statusId'];
        
        $review->Update();
        
    } else if (isset($_POST['add'])) {

        $review->_id = 0;
        $review->_fio = $_POST['fio'];
        $review->_name = $_POST['name'];
        $review->_comment = $_POST['comment'];
        $review->_statusId = $_POST['statusId'];
        $review_id = $review->Insert();
        
        header ('Location: ./review.php');
        
        exit();
    }

 ?>   
     <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
     <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">	
	 <LINK href="../general.css" type="text/css" rel="stylesheet">
	 <meta http-equiv="content-type" content="text/html; charset=windows-1251"/>     
	 <body>
            <?
                $smarty->display("./pagesplit.tpl", $_SERVER["REQUEST_URI"]);	

                if(isset($_GET['edit'])) {
                    
                    $review->_id = (int)$_GET['edit'];
                    $data = $review->GetItem();
                    $smarty->assign('data', $data);  
                    
                    $listStatus = $review->getListStatus();
                    $smarty->assign('listStatus', $listStatus);  
                    
                    $smarty->display("./review/update.tpl");  
                    
                } elseif (isset($_GET['add'])) {

                    $listStatus = $review->getListStatus();
                    $smarty->assign('listStatus', $listStatus);  
                    
                    $smarty->display("./review/update.tpl");  
                    
                } else {
                    $list = $review->Select();
                    $smarty->assign('list', $list); 
                    $smarty->display("./review/list.tpl"); 
                }
                
                $smarty->display("./pagesplit.tpl", $_SERVER["REQUEST_URI"]);	
            ?>
	</body>
    </html>
<?

?>