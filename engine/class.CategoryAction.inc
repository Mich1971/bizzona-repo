<?
include_once("class.db.inc");

class CategoryAction {

    private $_db; 
    private $_sql;

    public $_id;
    public $_name;

    function __construct() {
        global $DATABASE;
        $this->InitDB();
    }

    function InitDB() {
        global $DATABASE;
        $this->db = new DB($DATABASE["HOSTNAME"], $DATABASE["DATABASENAME"], $DATABASE["DATABASEUSER"], $DATABASE["DATABASEPASSWORD"]);
    }

    function __destruct() {

    }

    public function Insert($aParams) {

        if(isset($aParams['name'])) {
            $this->_sql = " insert into  CategoryAction (`name`) values ('".$aParams['name']."') ";
            $id = $this->db->Insert($this->_sql);
            return $id;
        }
        return 0;
    }

    public function GetItem($aParams) {
        return array();
    }

    public function Select() {

        $this->_sql = " select * from CategoryAction;";

        $result = $this->db->Select($this->_sql);

        $arr = array();

        while ($row = mysql_fetch_object($result)) {

            $aRow = array();
            $aRow['id'] = $row->id;
            $aRow['name'] = $row->name;

            array_push($arr, $aRow);
        }

        return $arr;

    }

    public function Delete($aParams) {
        return false;
    }

}

?>