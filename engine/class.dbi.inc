<? 
class DBi {

		protected static $_instance; 
  
        public static function getInstance() { 
            if (self::$_instance === null) { 
                self::$_instance = new self;  
            } 
            return self::$_instance; 
        }
   
        private  function __construct() { 
        	global  $DATABASE;
        	//$this->connect = mysqli_connect($DATABASE["HOSTNAME"], $DATABASE["DATABASEUSER"], $DATABASE["DATABASEPASSWORD"], $DATABASE["DATABASENAME"]);
        	$this->connect = new mysqli($DATABASE["HOSTNAME"], $DATABASE["DATABASEUSER"], $DATABASE["DATABASEPASSWORD"], $DATABASE["DATABASENAME"]);
            $this->connect->query('set names cp1251');   
        }
 
        private function __clone() { 
        }
        
        private function __wakeup() {
        }
   
        public static function query($sql) {
        
            $obj=self::$_instance;
        
            if(isset($obj->connect)){ 
            	
                $obj->count_sql++;
                $start_time_sql = microtime(true);
                
                $result = $obj->connect->query($sql);
                
                $time_sql = microtime(true) - $start_time_sql;
                //echo "<br/><br/><span style='color:blue'> <span style='color:green'># ������ ����� ".$obj->count_sql.": </span>".$sql."</span> <span style='color:green'>(".round($time_sql,4)." msec )</span>";             
                return $result;
            }
            return false;
        }   
    
        public static function fetch_object($object)
        {
            return mysqli_fetch_object($object);
        }

        public static function fetch_array($object)
        {
            return mysqli_fetch_array($object);
        }
        
        public static function insert_id()
        {
            return mysqli_insert_id();
        }
    
}
?>