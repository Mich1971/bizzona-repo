<?php
    header("Content-Type: text/xml; charset=utf-8");

	global $DATABASE;

	include_once("../../phpset.inc");
	include_once("../../cabinet/initdata.inc");	
	include_once("../functions.inc"); 

	AssignDataBaseSetting("../../config.ini");
	global $typeQueryInit;
	
	include_once("../class.operator.inc"); 
  	$operator = new Operator();

  	if(!isset($_GET)) {
  		$_GET = array();
  	}
  	
  	$_POST = array_merge($_POST, $_GET);
	if (isset($_POST) && $_POST["action"] == "set") {
		$operator->query = htmlspecialchars(trim(utf8_to_cp1251($_POST['query'])), ENT_QUOTES);
		$operator->sellId = htmlspecialchars(trim(utf8_to_cp1251($_POST['sellId'])), ENT_QUOTES);
		$operator->typeQueryId = htmlspecialchars(trim(utf8_to_cp1251($_POST['typeQueryId'])), ENT_QUOTES);
		$error = "";
		if(intval($operator->typeQueryId) <= 0) {
			$error = "error_typeQueryId";
		}
		if(strlen($operator->query) <= 0) {
			$error = "error_queryId";
		}
		
		if(strlen($error) <= 0) {
			$operator->Insert();
		}
		
  		$dom = new DOMDocument();
  		$n_result = $dom->createElement("result");
  		$n_result->setAttribute("error", cp1251_to_utf8($error));
  		$dom->appendChild($n_result);
  		echo $dom->saveXML();
		
		return; 
	}
	if (isset($_POST) && $_POST["action"] == "get") {
		$operator->sellId = htmlspecialchars(trim(utf8_to_cp1251($_POST['sellId'])), ENT_QUOTES);
		$list = $operator->Select();

  		$dom = new DOMDocument();
  		$n_operators = $dom->createElement("operators");
  		while (list($k, $v) = each($list)) {
			$n_operator = $dom->createElement("item", cp1251_to_utf8($v->query));
			$n_operator->setAttribute("id", $v->id);
			$n_operator->setAttribute("answer", cp1251_to_utf8($v->answer));
			$n_operator->setAttribute("type", cp1251_to_utf8($typeQueryInit[$v->typeQueryId]));
			$n_operator->setAttribute("datetime", cp1251_to_utf8($v->datetime));
			$n_operators->appendChild($n_operator);
  		}
  		$dom->appendChild($n_operators);
  		echo $dom->saveXML();
		
	}
?>