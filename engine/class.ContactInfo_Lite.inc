<?php
	include_once("class.db.inc");

  	class ContactInfo_Lite {
  	
		public $db = "";
		public $textContactInfo = "";
		public $adsPhone = "";
		public $adsEmail = "";
		public $supportPhone = "";
		public $supportEmail = "";
	
		function __construct() {
			global $DATABASE;
			$this->InitDB();
		}

		function InitDB() {
			global $DATABASE;
			$this->db = new DB($DATABASE["HOSTNAME"], $DATABASE["DATABASENAME"], $DATABASE["DATABASEUSER"], $DATABASE["DATABASEPASSWORD"]);
		}

		
		function GetContactInfo() {
			 
			$sql = 'select * from ContactInfo;';
			$result = $this->db->Select($sql);

			while ($row = mysql_fetch_object($result)) {
				
				$this->textContactInfo = $row->textContactInfo;
				$this->adsPhone = $row->adsPhone;
				$this->adsEmail = $row->adsEmail;
				$this->supportPhone = $row->supportPhone;
				$this->supportEmail = $row->supportEmail;

			}
		}
		
  }
?>