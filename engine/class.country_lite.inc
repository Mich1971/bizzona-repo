<?php
	include_once("class.db.inc");  
    
	class Country_Lite {
		var $db = "";

		var $aEuropa = array('title' => '������� ������� �� ������� ������',
							 'country' => array(
							 					 array('name' => '�������', 'url' => 'http://prodazha-biznesa-belgium.bizzona.ru/', 'title' => '������� ������� �������'),
							 					 array('name' => '��������', 'url' => 'http://prodazha-biznesa-bulgaria.bizzona.ru/', 'title' => '������� ������� ��������'),
							 					 array('name' => '�������', 'url' => 'http://prodazha-biznesa-spain.bizzona.ru/', 'title' => '������� ������� �������'),
							 					 array('name' => '������', 'url' => 'http://prodazha-biznesa-italy.bizzona.ru/', 'title' => '������� ������� ������'),
							 					 array('name' => '���������', 'url' => 'http://prodazha-biznesa-finland.bizzona.ru/', 'title' => '������� ������� ���������'),
							 					 array('name' => '�������', 'url' => 'http://prodazha-biznesa-estonia.bizzona.ru/', 'title' => '������� ������� �������'),
							 					 array('name' => '������', 'url' => 'http://prodazha-biznesa-latvia.bizzona.ru/', 'title' => '������� ������� ������'),
							 					 array('name' => '�����', 'url' => 'http://prodazha-biznesa-lithuania.bizzona.ru/', 'title' => '������� ������� �����'),
							 					 array('name' => '��������', 'url' => 'http://prodazha-biznesa-norway.bizzona.ru/', 'title' => '������� ������� ������'),
							 					 array('name' => '�����', 'url' => 'http://prodazha-biznesa-czech-republic.bizzona.ru/', 'title' => '������� ������� �����'),
							 					 array('name' => '����������', 'url' => 'http://prodazha-biznesa-portugal.bizzona.ru/', 'title' => '������� ������� ����������'),
							 					 array('name' => '�������', 'url' => 'http://prodazha-biznesa-romania.bizzona.ru/', 'title' => '������� ������� �������'),
							 					 array('name' => '����������', 'url' => 'http://www.bizzona.ru/searchsell/city/3676', 'title' => '������� ������� ����������'),
							 					 array('name' => '������', 'url' => 'http://www.bizzona.ru/searchsell/city/4330', 'title' => '������� ������� ������'),
							 					 array('name' => '���������', 'url' => 'http://www.bizzona.ru/searchsell/city/4678', 'title' => '������� ������� ���������'),
							 					 array('name' => '����������', 'url' => 'http://www.bizzona.ru/searchsell/city/4507', 'title' => '������� ������� ����������'),
							 					 array('name' => '�������', 'url' => 'http://www.bizzona.ru/searchsell/city/4542', 'title' => '������� ������� �������'),
							 					 array('name' => '�������', 'url' => 'http://www.bizzona.ru/searchsell/city/4605', 'title' => '������� ������� �������'),
							 					)
							 );
							 
							 
		var $aSNG = array('title' => '������� ������� �� ������� ���',
							 'country' => array(
							 					 array('name' => '�������', 'url' => 'http://prodazha-biznesa-ukraine.bizzona.ru/', 'title' => '������� ������� �������'),
							 					 array('name' => '��������', 'url' => 'http://prodazha-biznesa-belarus.bizzona.ru/', 'title' => '������� ������� ����������'),
							 					 array('name' => '���������', 'url' => 'http://prodazha-biznesa-kazakhstan.bizzona.ru/', 'title' => '������� ������� ���������'),
							 					 array('name' => '����������', 'url' => 'http://prodazha-biznesa-uzbekistan.bizzona.ru/', 'title' => '������� ������� ����������'),
							 					)
							 );							 
							 
		var $aWorld = array('title' => '������� ������� �� ������� ����',
							 'country' => array(
							 					 array('name' => '�����', 'url' => 'http://prodazha-biznesa-china.bizzona.ru/', 'title' => '������� ������� �����'),
							 					 array('name' => '����', 'url' => 'http://prodazha-biznesa-cyprus.bizzona.ru/', 'title' => '������� ������� ����'),
							 					 array('name' => '���', 'url' => 'http://prodazha-biznesa-united-states.bizzona.ru/', 'title' => '������� ������� ���'),
							 					 array('name' => '�����������', 'url' => 'http://prodazha-biznesa-azerbaijan.bizzona.ru/', 'title' => '������� ������� �����������'),
							 					 array('name' => '������', 'url' => 'http://www.bizzona.ru/searchsell/city/3701', 'title' => '������� ������� ������'),
							 					 array('name' => '�������', 'url' => 'http://prodazha-biznesa-moldova.bizzona.ru/', 'title' => '������� ������� �������'),
							 					 array('name' => '���������', 'url' => 'http://www.bizzona.ru/searchsell/city/4419', 'title' => '������� ������� ���������'),
							 					 array('name' => '���', 'url' => 'http://prodazha-biznesa-united-arab-emirates.bizzona.ru/', 'title' => '������� ������� ���')
							 					)
							 );
							  
		var $aCityRussia = array(
									array('id' => '77', 'parent' => '77', 'name' => '������', 'url' => 'http://www.bizzona.ru/moskva.php'),
									array('id' => '78', 'parent' => '78', 'name' => '�����-���������', 'url' => 'http://www.bizzona.ru/sankt-peterburg.php'),
									array('id' => '928', 'parent' => '23', 'name' => '���������', 'url' => 'http://www.bizzona.ru/krasnodar.php'),
									array('id' => '1791', 'parent' => '61', 'name' => '������-��-����', 'url' => 'http://www.bizzona.ru/rostov-na-donu.php'),
									array('id' => '1915', 'parent' => '66', 'name' => '������������', 'url' => 'http://www.bizzona.ru/ekaterinburg.php'),
									array('id' => '1310', 'parent' => '54', 'name' => '�����������', 'url' => 'http://www.bizzona.ru/novosibirsk.php'),
									array('id' => '304', 'parent' => '2', 'name' => '���', 'url' => 'http://www.bizzona.ru/ufa.php'),
									array('id' => '385', 'parent' => '33', 'name' => '��������', 'url' => 'http://www.bizzona.ru/vladimir.php'),
									array('id' => '1721', 'parent' => '15', 'name' => '������', 'url' => 'http://www.bizzona.ru/kazan.php'),
									array('id' => '1279', 'parent' => '53', 'name' => '��������', 'url' => 'http://www.bizzona.ru/novgorod.php'),
									array('id' => '1353', 'parent' => '55', 'name' => '����', 'url' => 'http://www.bizzona.ru/omsk.php'),
									array('id' => '2048', 'parent' => '69', 'name' => '�����', 'url' => 'http://www.bizzona.ru/tver.php'),
									array('id' => '656', 'parent' => '40', 'name' => '������', 'url' => 'http://www.bizzona.ru/kaluga.php'),
									array('id' => '1816', 'parent' => '63', 'name' => '������', 'url' => 'http://www.bizzona.ru/samara.php'),
									array('id' => '744', 'parent' => '42', 'name' => '��������', 'url' => 'http://www.bizzona.ru/kemerovo.php'),
									array('id' => '634', 'parent' => '39', 'name' => '�����������', 'url' => 'http://www.bizzona.ru/kaliningrad.php'),
									array('id' => '460', 'parent' => '36', 'name' => '�������', 'url' => 'http://www.bizzona.ru/voronezh.php'),
									array('id' => '395', 'parent' => '34', 'name' => '���������', 'url' => 'http://www.bizzona.ru/volgograd.php'),
									array('id' => '1970', 'parent' => '67', 'name' => '��������', 'url' => 'http://www.bizzona.ru/smolensk.php'),
									array('id' => '1847', 'parent' => '64', 'name' => '�������', 'url' => 'http://www.bizzona.ru/saratov.php'),
									array('id' => '1408', 'parent' => '57', 'name' => '���', 'url' => 'http://www.bizzona.ru/orel.php'),
									array('id' => '1995', 'parent' => '26', 'name' => '����������', 'url' => 'http://www.bizzona.ru/stavropol.php'),
									array('id' => '2101', 'parent' => '71', 'name' => '����', 'url' => 'http://www.bizzona.ru/tula.php'),
									array('id' => '1441', 'parent' => '59', 'name' => '�����', 'url' => 'http://www.bizzona.ru/perm.php'),
									array('id' => '554', 'parent' => '38', 'name' => '�������', 'url' => 'http://www.bizzona.ru/irkutsk.php'),
									array('id' => '105', 'parent' => '22', 'name' => '�������', 'url' => 'http://www.bizzona.ru/barnaul.php'),
									array('id' => '429', 'parent' => '35', 'name' => '�������', 'url' => 'http://www.bizzona.ru/vologda.php'),
									array('id' => '1512', 'parent' => '60', 'name' => '�����', 'url' => 'http://www.bizzona.ru/pskov.php'),
									array('id' => '514', 'parent' => '37', 'name' => '�������', 'url' => 'http://www.bizzona.ru/ivanovo.php'),
									array('id' => '309', 'parent' => '31', 'name' => '��������', 'url' => 'http://www.bizzona.ru/belgorod.php'),
									array('id' => '773', 'parent' => '43', 'name' => '�����', 'url' => 'http://www.bizzona.ru/kirov.php'),
									array('id' => '1217', 'parent' => '51', 'name' => '��������', 'url' => 'http://www.bizzona.ru/murmansk.php'),
									array('id' => '1384', 'parent' => '56', 'name' => '��������', 'url' => 'http://www.bizzona.ru/orenburg.php'),
									array('id' => '201', 'parent' => '29', 'name' => '�����������', 'url' => 'http://www.bizzona.ru/arhangelsk.php'),
									array('id' => '2227', 'parent' => '27', 'name' => '���������', 'url' => 'http://www.bizzona.ru/habarovsk.php'),
									array('id' => '2093', 'parent' => '70', 'name' => '�����', 'url' => 'http://www.bizzona.ru/tomsk.php'),
									array('id' => '1054', 'parent' => '46', 'name' => '�����', 'url' => 'http://www.bizzona.ru/kursk.php'),
									array('id' => '1041', 'parent' => '45', 'name' => '������', 'url' => 'http://www.bizzona.ru/kurgan.php'),
									array('id' => '907', 'parent' => '44', 'name' => '��������', 'url' => 'http://www.bizzona.ru/kostroma.php'),
									array('id' => '259', 'parent' => '30', 'name' => '���������', 'url' => 'http://www.bizzona.ru/astrahan.php'),
									array('id' => '2025', 'parent' => '68', 'name' => '������', 'url' => 'http://www.bizzona.ru/tambov.php'),
									array('id' => '1419', 'parent' => '58', 'name' => '�����', 'url' => 'http://www.bizzona.ru/penza.php'),
									array('id' => '1120', 'parent' => '49', 'name' => '�������', 'url' => 'http://www.bizzona.ru/magadan.php'),
									array('id' => '2394', 'parent' => '75', 'name' => '����', 'url' => 'http://www.bizzona.ru/chita.php'),
									array('id' => '2326', 'parent' => '74', 'name' => '���������', 'url' => 'http://www.bizzona.ru/chelyabinsk.php'),
									array('id' => '985', 'parent' => '24', 'name' => '����������', 'url' => 'http://www.bizzona.ru/krasnoyarsk.php'),
									array('id' => '2530', 'parent' => '76', 'name' => '���������', 'url' => 'http://www.bizzona.ru/yaroslavl.php'),
									array('id' => '1800', 'parent' => '62', 'name' => '������', 'url' => 'http://www.bizzona.ru/ryazan.php'),
									array('id' => '2132', 'parent' => '72', 'name' => '������', 'url' => 'http://www.bizzona.ru/tyumen.php'),
									array('id' => '317', 'parent' => '32', 'name' => '������', 'url' => 'http://www.bizzona.ru/bryansk.php'),
									array('id' => '1100', 'parent' => '48', 'name' => '������', 'url' => 'http://www.bizzona.ru/lipezk.php'),
									array('id' => '2157', 'parent' => '73', 'name' => '���������', 'url' => 'http://www.bizzona.ru/ulyanovsk.php'),
									array('id' => '1264', 'parent' => '52', 'name' => '���. ��������', 'url' => 'http://www.bizzona.ru/nizhnovgorod.php')
								);								
							 
							   
     	function Country_Lite() {
       		global $DATABASE;
       		$this->InitDB();
     	} 

     	function InitDB() {
       		global $DATABASE;
       		$this->db = new DB($DATABASE["HOSTNAME"], $DATABASE["DATABASENAME"], $DATABASE["DATABASEUSER"], $DATABASE["DATABASEPASSWORD"]);
     	}

     	function Close() {
     		$this->db->Close();
     	}
     
     	function GetCityRussia() {
     		
			foreach ($this->aCityRussia as $key => $row) {
    			$volume[$key]  = $row['name'];
			}     		
     		
     		array_multisort($volume, SORT_ASC, $this->aCityRussia);
     		
     		return $this->aCityRussia;
     	}
     	
     	function GetEuropeCountry() {
     		
			
			foreach ($this->aEuropa['country'] as $key => $row) {
    			$volume[$key]  = $row['name'];
			}			
			reset($this->aEuropa['country']);
			array_multisort($volume, SORT_ASC, $this->aEuropa['country']);
			$triples = array_chunk($this->aEuropa['country'], 3);
			
			
			$this->aEuropa['country'] = $triples;
			
     		return $this->aEuropa;     		
     		
     	}
     	
     	function GetSNGCountry() {
     		
			foreach ($this->aSNG['country'] as $key => $row) {
    			$volume[$key]  = $row['name'];
			}			
			reset($this->aSNG['country']);
			array_multisort($volume, SORT_ASC, $this->aSNG['country']);
			$triples = array_chunk($this->aSNG['country'], 2);
			$this->aSNG['country'] = $triples;
			
     		return $this->aSNG;     		
     		
     	}     	
     	
     	function GetTotalCountry() {

			$aAllcountry  = array('title' => '������� ������� �� ������� ����',
								   'country' => array(array_merge( $this->aEuropa['country'], $this->aSNG['country'], $this->aWorld['country']))
								 );      		
								 

			foreach ($aAllcountry['country'][0] as $key => $row) {
    			$volume[$key]  = $row['name'];
			}			
			
			reset($aAllcountry['country'][0]);
			array_multisort($volume, SORT_ASC, $aAllcountry['country'][0]);
			$triples = array_chunk($aAllcountry['country'][0], 6);
			$aAllcountry['country'] = $triples;
			
     		return $aAllcountry;
     	}
     	
     	function Select($data) {
       		if ($data["offset"] === 0 && $data["rowCount"] === 0) {
         		$sql = "Select * from Country;";
       		} else {
       			
				if(intval($data["rowCount"]) > 20) { $data["rowCount"] = 5; }
				if(intval($data["offset"]) > 50000) { $data["offset"] = 0; }        			
       			
         		$sql = "Select * from Country where 1 order  by ".$data["sort"]." limit ".intval($data["offset"]).",".$data["rowCount"].";";
       		} 
       		$result = $this->db->Select($sql);
       		$array = Array();
       		while ($row = mysql_fetch_object($result)) {
         		$obj = new stdclass();
         		$obj->ID = $row->ID;
         		$obj->Name = $row->Name;
         		$array[] = $obj;
       		}
       		return $array;
     	}

     	function GetItem($data) {
       		$sql = "Select * from Country where ID=".trim($data["ID"]).";";
       		$result = $this->db->Select($sql);
       		$array = array();
       		if (is_array($result)) {
         		reset($result);
         		while(list($k, $v) = each($result)) {
           			$array[$k]= $v[0];
         		}
       		} 
       		return $array;
     	}
     	
	}
?>