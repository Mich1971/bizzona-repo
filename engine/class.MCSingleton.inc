<?php
class MCSingleton {

    private static $instance = null;
    public static function  getInstance() {
        if(!self::$instance) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    private function __clone(){}

    public $connection = null;

    private function __construct() {
        $this->connection = memcache_connect('localhost', 11211);
    }

}
?>