<?
	ini_set("magic_quotes_gpc","off");

	global $DATABASE;

	include_once("../phpset.inc");
	include_once("./functions.inc"); 

	AssignDataBaseSetting("../config.ini");
// требуется авторизация
	require_once($_SERVER['DOCUMENT_ROOT'].'/inside/auth.php');
//

	ini_set("display_startup_errors", "on");
	ini_set("display_errors", "on");
	ini_set("register_globals", "on");

	include("../engine/FCKeditor/fckeditor.php");	
	
	include_once("./class.news.inc"); 
	$news = new News();
	
        require_once("../libs/Smarty.class.php");
        $smarty = new Smarty;
        $smarty->template_dir = "../templates";
        $smarty->compile_check = false;
        $smarty->caching = false;
        $smarty->compile_dir  = "../templates_c";
        $smarty->cache_dir = "../cache";
        $smarty->debugging = false;
        $smarty->clear_all_cache();

	function PreData(news &$news) {
		
		$a_startDate = split('/', $_POST['datetime']);
		$news->_datetime = $a_startDate[2].'-'.$a_startDate[0].'-'.$a_startDate[1];
		
		$news->_shorttext = $_POST["shorttext"];
		$news->_title =  $_POST["title"];
		$news->_detailstext = $_POST["detailstext"];
		$news->_statusId = $_POST['statusId'];
		$news->_Id = (isset($_GET['Id']) ? intval($_GET['Id']) : 0);
		
	}

	$smarty->assign("liststatus", $news->_aStatusId);	
	
	if (isset($_POST["update"])) {
		PreData($news);
		$news->UpdateNews();
		
	} else if (isset($_POST["add"])) {
		PreData($news);
		$news->AddNews();
	}
	
	if (isset($_GET["Id"])) {
		$news->_Id = intval($_GET["Id"]);
		$item = $news->GetItem();
		$smarty->assign("item", $item);	
	}

	$list = $news->Select(array());	
	$smarty->assign("list", $list);		
?>
	<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
    	<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	 	<LINK href="../general.css" type="text/css" rel="stylesheet">
	 	<meta http-equiv="content-type" content="text/html; charset=windows-1251"/> 
	 	<body>
<?
        $smarty->display("./mainmenu.tpl");

		$smarty->display("./news/list.tpl");
		$smarty->display("./news/add.tpl");
?>
		</body>
		</html>
<?
?>