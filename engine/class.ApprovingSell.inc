<?php
    include_once("class.db.inc");  

    class ApprovingSell {

        var $db = '';
        var $id = 0;
        var $codeId = 0;
        var $data = '';
        var $typeId = '';
        var $comment = '';
        var $statusId = 0;
        
        function ApprovingSell() {
            global $DATABASE;
            $this->InitDB();
        }

        function InitDB() {
            global $DATABASE;
            $this->db = new DB($DATABASE["HOSTNAME"], $DATABASE["DATABASENAME"], $DATABASE["DATABASEUSER"], $DATABASE["DATABASEPASSWORD"]);
        }
        
        public function SetCooment($aParams) {
            
            $sql =  "update ApprovingSell set comment='".  addslashes($aParams['comment'])."', statusId='1' where codeId=".(int)$aParams['codeId']." and typeId='".$aParams['typeId']."' limit 1;";
            $result = $this->db->Update($sql);
        }

        public function GetCooment($aParams) {
            
            $sql =  "select comment from ApprovingSell where codeId=".(int)$aParams['codeId']." and typeId='".$aParams['typeId']."'";
            $result = $this->db->Select($sql);
            while ($row = mysql_fetch_object($result)) {
                return $row->comment;
            }
            return '';

        }
        
        public function Delete($aParams) {

            $sql = "delete from ApprovingSell where codeId=".(int)$aParams['codeId']." and typeId='".$aParams['typeId']."' ";
            $result = $this->db->Delete($sql);
        }
        
        public function Save($aParams) {
            
            $_id = $this->IsExists($aParams); 
            
            $this->data = serialize($this->data);
            $this->codeId = $aParams['codeId'];
            $this->typeId = $aParams['typeId'];
            
            if($_id) {
                $this->Update();
            } else {
                $this->Insert();
            }
        }
        
        public function Insert() {
            
            $sql = "insert into ApprovingSell (`codeId`, `data`, `typeId`) values (".(int)$this->codeId.", '".base64_encode($this->data)."', '".$this->typeId."');";
            
            $result = $this->db->Insert($sql);
            
        }
        
        public function Update() {
            
            $sql = "update ApprovingSell set data='".base64_encode($this->data)."', statusId='0' where codeId=".(int)$this->codeId." and typeId='".$this->typeId."' limit 1;";
            $result = $this->db->Update($sql);
            
        }
        
        public function IsExists($aParams) {
            
            $sql = "select id from ApprovingSell where codeId=".(int)$aParams['codeId']." and typeId='".$aParams['typeId']."';"; 
            $result = $this->db->Select($sql);
            while ($row = mysql_fetch_object($result)) {
                return $row->id;
            }
            return 0;
        }
        
        public function Select($aParams) {

            $sql = "select * from ApprovingSell where codeId=".(int)$aParams['codeId']." and typeId='".$aParams['typeId']."';"; 
            $result = $this->db->Select($sql);
            
            while ($row = mysql_fetch_object($result)) {

                //$this->findSerializeError($row->data); 
                $data = base64_decode($row->data);
                
                $data = unserialize($data);
                
                if(!is_object($data)) {

                    $data = base64_decode($row->data);
                    
                    //echo $data;
                    
                    $data = preg_replace('!s:(\d+):"(.*?)";!e', "'s:'.strlen('$2').':\"$2\";'", $data);
                    $data = unserialize($data);
                } 
                
                return $data;
            }
            return null;
        }
        
        public function ListIds($aParams = array('typeId' => 'sell')) {

            $aResult = array();
            
            $sql = "select codeId from ApprovingSell where typeId='".$aParams['typeId']."' and statusId='0'; ";
            $result = $this->db->Select($sql);
            
            while ($row = mysql_fetch_object($result)) {
                $aResult[] = $row->codeId;
            }
            
            return $aResult;
        }
        
        public function findSerializeError($data1) {
            
            echo "<pre>";
            $data2 = preg_replace ( '!s:(\d+):"(.*?)";!e', "'s:'.strlen('$2').':\"$2\";'",$data1 );
            $max = (strlen ( $data1 ) > strlen ( $data2 )) ? strlen ( $data1 ) : strlen ( $data2 );

            //echo $data1 . PHP_EOL;
            //echo $data2 . PHP_EOL;

            for($i = 0; $i < $max; $i ++) {

                if (@$data1 {$i} !== @$data2 {$i}) {

                    echo "Diffrence ", @$data1 {$i}, " != ", @$data2 {$i}, PHP_EOL;
                    echo "\t-> ORD number ", ord ( @$data1 {$i} ), " != ", ord ( @$data2 {$i} ), PHP_EOL;
                    echo "\t-> Line Number = $i" . PHP_EOL;

                    $start = ($i - 20);
                    $start = ($start < 0) ? 0 : $start;
                    $length = 40;

                    $point = $max - $i;
                    if ($point < 20) {
                        $rlength = 1;
                        $rpoint = - $point;
                    } else {
                        $rpoint = $length - 20;
                        $rlength = 1;
                    }

                    echo "\t-> Section Data1  = ", substr_replace ( substr ( $data1, $start, $length ), "<b style=\"color:green\">{$data1 {$i}}</b>", $rpoint, $rlength ), PHP_EOL;
                    echo "\t-> Section Data2  = ", substr_replace ( substr ( $data2, $start, $length ), "<b style=\"color:red\">{$data2 {$i}}</b>", $rpoint, $rlength ), PHP_EOL;
                }
            
            }
        
        }
    }
 
?>
 