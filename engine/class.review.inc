<?php
    include_once("class.db.inc");  

    interface IReview {
        
        const SELL_TYPE = 1;
        
        const STATUS_NEW = 0;
        const STATUS_OPENED = 1;
        const STATUS_CLOSED = 2;
        
    };
 
    class Review implements IReview {
        
        public $db = "";
        public $sql = "";
        public $_id;
        public $_type;
        public $_fio;
        public $_name;
        public $_comment;
        public $_created;
        public $_statusId = IReview::STATUS_NEW;

        function __construct() {
            global $DATABASE;
            $this->InitDB();
        }

        function InitDB() {
            global $DATABASE;
            $this->db = new DB($DATABASE["HOSTNAME"], $DATABASE["DATABASENAME"], $DATABASE["DATABASEUSER"], $DATABASE["DATABASEPASSWORD"]);
        }
 
        function Select($aParams = array()) {
            
            $where = ' where 1 ';
            $limit = ' ';
            
            if(isset($aParams['statusId']) && is_array($aParams['statusId']) && count($aParams['statusId'])) {
                $where .=  ' and statusid in ('.  implode(',', $aParams['statusId']).')';
            }
            
            if(isset($aParams['limit'])) {
                $limit .= ' limit '.(int)$aParams['limit'].' ';
            }
            
            $this->sql = "select * from rewview ".$where." order by id desc ".$limit.";";
            $result = $this->db->Select($this->sql);
            
            $aResult = array();
            
            while ($row = mysql_fetch_object($result)) {
                $aResult[] = array(
                    'id' => $row->id,
                    'type' => $row->type,
                    'fio' => $row->fio,
                    'name' => $row->name,
                    'comment' => $row->comment,
                    'created' => $row->created,
                    'statusId' => $row->statusId,
                    'statusStr' => $this->getStatusStr($row->statusId),
                );
            }
            
            return $aResult;
        }
        
        function GetItem() {

            $_review = new Review;
            $this->sql = "select * from rewview where 1 and id=".intval($this->_id).";";
            $result = $this->db->Select($this->sql);
            while ($row = mysql_fetch_object($result)) {
                $_review->_id = $row->id;
                $_review->_fio = $row->fio; 
                $_review->_name = $row->name;
                $_review->_created = $row->created;
                $_review->_comment = $row->comment;
                $_review->_statusId = $row->_statusId;
            }
            return $_review;

        }

        function Insert() {
            
            $this->sql = "insert into rewview (`type`,`fio`,`name`,`comment`,`created`,`statusId`) values (1, '".$this->_fio."', '".$this->_name."', '".$this->_comment."', NOW(), ".(int)  $this->_statusId.")";
            $result = $this->db->Insert($this->sql);	
            return $result; 
        }

        function CountStatus($statusId) {
            
            $this->sql = "select count(rewview.id) as 'c' from rewview where rewview.statusId=".intval($statusId)."; ";
            $result = $this->db->Select($this->sql);
            while ($row = mysql_fetch_object($result)) {
                return $row->c;
            }
            return 0;
        }         
        
        function Close() {
            $this->db->Close();
        }
 
        function  getListStatus() {
            
            $aResult = array();
            
            $aResult[IReview::STATUS_NEW] = '�����'; 
            $aResult[IReview::STATUS_OPENED] = '������������'; 
            $aResult[IReview::STATUS_CLOSED] = '�������'; 
            
            return $aResult;
            
        }
        
        function getStatusStr($statusId) {
            
            $aList = $this->getListStatus();
            if(isset($aList[$statusId])) {
                return $aList[$statusId];
            }
            
            return '';
        }
        
        function Update() {
            
            $this->_fio = mysql_real_escape_string($this->_fio);
            $this->_name = mysql_real_escape_string($this->_name);
            $this->_comment = mysql_real_escape_string($this->_comment);
            
            $this->sql = "update rewview set fio='".$this->_fio."', name='".$this->_name."', comment='".$this->_comment."', statusId=".(int)$this->_statusId." where id=".$this->_id." limit 1;";
            $result = $this->db->Sql($this->sql);
            
        }
        
        function LastBlockReview() {
            
            global $smarty;
            
            $list = $this->Select(array(
                                    'statusId' => array(1),
                                    'limit' => 10
                                  ));
            
            $smarty->assign("listreview", $list);
            
            return  fminimizer($smarty->fetch("./site/blockreview.tpl"));
            
        }
        
        
    }
?>