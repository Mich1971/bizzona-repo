<?php
	include_once("../engine/class.db.inc");
	
	class Business_Analitics {
		
		public $db = "";
		
		public $city_list = array();
		public $category_list = array();
		public $year_list = array();

			
		public $title = "";
		public $x_axis_steps = "1";
		public $x_axis_3d = "12";
		public $y_legend = "(RUB),12";
		public $y_ticks = "5,10,5";
		public $x_labels = "";
		public $y_min = "";
		public $y_max = "";
		public $bg_colour = "";
		public $x_axis_colour = "#909090";
		public $x_grid_colour = "#ADB5C7";
		public $y_axis_colour = "#909090";
		public $y_grid_colour = "#ADB5C7";

		public $bar_3d = "";
		public $values = "";
		public $bar_3d_2 = "";
		public $values_2 = "";
		public $bar_3d_3 = "";
		public $values_3 = "";
		public $bar_3d_4 = "";
		public $values_4 = "";		
		
		public $pie_labels = "";
		public $pie = "70,#7c3535,{font-size: 11px; color: #7c3535;";
		public $colours = "";
		public $line = "3,#87421F";
		public $tool_tip="%23val%23%25";
		
		public $bar_fade = "95,#c63030,Cost,20";
		public $bar_fade_2 = "95,#006699,Profit,20";
		public $x_label_style = "9,#000000,2";
		
		public $text_cost = "";
		public $text_profit = "";
		
		public $max_cost = 35000000;
		public $max_profit = 1000000;
		public $max_wagefund = 500000;
		public $max_expemse = 1000000;
		public $max_rent = 1000000;
		
		public $value_usd = 30;
		public $value_eur = 40;
		
		public $Id = 0;
		
		
		public $array_color = array(0 => "#c63030", 1 => "#006699", 2 => "#D54C78", 3 => "#71a66d", 4 => "#458830", 5 => "#1b918d", 6 => "#3c7ca0", 7 => "#57388a", 8 => "#8f49a0", 9 => "#c420a3", 10 => "#c42062", 11 => "#cccc99", 12 => "#cc6699", 13 => "#336699");
		
		function __construct() {
			global $DATABASE;
			$this->InitDB();
		}

		function InitDB() {
			global $DATABASE;
			$this->db = new DB($DATABASE["HOSTNAME"], $DATABASE["DATABASENAME"], $DATABASE["DATABASEUSER"], $DATABASE["DATABASEPASSWORD"]);
		}
		
		private function Parse() {
		
			$city_list = $this->city_list;
			while (list($k, $v) = each($city_list)) {
				$city_list[$k] = intval($v);
			}
			$this->city_list = $city_list;
			reset($this->city_list);
			
			$category_list = $this->category_list;
			while (list($k, $v) = each($category_list)) {
				$category_list[$k] = intval($v);
			}			
			$this->category_list = $category_list;
			reset($this->category_list);
			
			$year_list = $this->year_list;
			while (list($k, $v) = each($year_list)) {
				$year_list[$k] = intval($v);
			}			
			$this->year_list = $year_list;
			reset($this->year_list);
		}

		private function ParseMonthToCost_Default(&$arr) {
			
			while (list($k, $v) = each($arr)) {
				for($i=1; $i<=12; $i++) {
					if(!isset($v[$i])) {
						$v[$i] = 0;
					}
				}
				ksort($v);
				$arr[$k] = $v;
			}
			reset($arr);
		}

		private function GetMaxValue($array) {
			
			$max_value = max($array);
			
			return $max_value + intval($max_value*0.10);			
			
		}
		
		private function GetMaxValueOfYears($array_years, $_max_value = 100000) {
			
			$max_value = $_max_value;
			
			if(sizeof($array_years) > 0) {
				
				while (list($k, $v) = each($array_years)) {
					if($max_value < max($v)) {
						$max_value = max($v);
					}
				}
				
			}
			
			return $max_value + intval($max_value*0.10);
		}
		
		private function ParseDataToProcent(&$array) {
			
			$summ = 0;
			
			while (list($k,$v) = each($array)) {
				$summ += $v;
			}
			
			reset($array);
			
			while (list($k,$v) = each($array)) {
				$array[$k] = intval(($v * 100)/$summ);
			}
			reset($array);
		}

		public function GetDataShowInfoForBuyer() {
			
			$array_analitics = array(); 
			
			$array_analitics = $this->Data_RenderShowInfoForBuyer();
			
    		$array_analitics["payback"] = intval($array_analitics["cost"]/$array_analitics["profit"]);
    	
    		$array_analitics["cost"] = strrev(chunk_split (strrev($array_analitics["cost"]), 3,' '));
    		$array_analitics["profit"] = strrev(chunk_split (strrev($array_analitics["profit"]), 3,' '));
    		$array_analitics["wagefund"] = strrev(chunk_split (strrev($array_analitics["wagefund"]), 3,' '));
    		$array_analitics["expemse"] = strrev(chunk_split (strrev($array_analitics["expemse"]), 3,' '));			
    		$array_analitics["rent"] = strrev(chunk_split (strrev($array_analitics["rent"]), 3,' '));			
    		
    		return $array_analitics;
			
		}

		
		public function Data_StatView() {
			
			$result = $this->db->Select("select * from statview where objectId=".$this->Id."  and (unix_timestamp(now()) - datetime) < 86400*30 ; ");
	
			$countperdate = array();
	
			while ($row = mysql_fetch_object($result)) {
		
				$datetime = date('d-M', $row->datetime);
		
				if(!isset($countperdate[$datetime])) {
					$countperdate[$datetime] = 1;
				} else {
					$countperdate[$datetime] = $countperdate[$datetime] + 1;
				}
			}			
			
			return $countperdate;
			
		}
		
		public function Data_RenderShowInfoForBuyer() {

			$array = array();

			$sql = "SELECT  AVG(IF((cMonthExpemseID='usd'), MonthExpemseID*".$this->value_usd.", IF((cMonthExpemseID='eur'), MonthExpemseID*".$this->value_eur.",MonthExpemseID))) as expemse FROM `Sell` WHERE 1 and  `SubCategoryID` in (".implode(",",$this->category_list).") and (MonthExpemseID>0  and IF((cMonthExpemseID='usd'), MonthExpemseID*".$this->value_usd.", IF((cMonthExpemseID='eur'), MonthExpemseID*".$this->value_eur.",MonthExpemseID)) < ".$this->max_expemse.");";
			$result = $this->db->Select($sql);
			while ($row = mysql_fetch_object($result)) {
				$array["expemse"] = intval($row->expemse);
			}
			
			$sql = "SELECT AVG(IF((cWageFundID='usd'), WageFundID*".$this->value_usd.", IF((cWageFundID='eur'), WageFundID*".$this->value_eur.",WageFundID))) as wagefund FROM `Sell` WHERE 1 and  `SubCategoryID` in (".implode(",",$this->category_list).") and (WageFundID>0  and IF((cWageFundID='usd'), WageFundID*".$this->value_usd.", IF((cWageFundID='eur'), WageFundID*".$this->value_eur.",WageFundID)) < ".$this->max_wagefund.");";
			$result = $this->db->Select($sql);
			while ($row = mysql_fetch_object($result)) {
				$array["wagefund"] = intval($row->wagefund);
			}			

			$sql = "SELECT AVG(IF((cCostID='usd'), CostID*".$this->value_usd.", IF((cCostID='eur'), CostID*".$this->value_eur.",CostID))) as cost FROM `Sell` WHERE 1 and  `SubCategoryID` in (".implode(",",$this->category_list).") and (CostID>0  and IF((cCostID='usd'), CostID*".$this->value_usd.", IF((cCostID='eur'), CostID*".$this->value_eur.",CostID)) < ".$this->max_cost.");";
			$result = $this->db->Select($sql);
			while ($row = mysql_fetch_object($result)) {
				$array["cost"] = intval($row->cost);
			}
		
			$sql = "SELECT AVG(CASE when (ProfitPerMonthID > 0 and MaxProfitPerMonthID > 0) then (IF((cProfitPerMonthID='usd'), ProfitPerMonthID*".$this->value_usd.", IF((cProfitPerMonthID='eur'), ProfitPerMonthID*".$this->value_eur.",ProfitPerMonthID)) + IF((cMaxProfitPerMonthID='usd'), MaxProfitPerMonthID*".$this->value_usd.", IF((cMaxProfitPerMonthID='eur'), MaxProfitPerMonthID*".$this->value_eur.",MaxProfitPerMonthID)))/2 when ProfitPerMonthID > 0 then IF((cProfitPerMonthID='usd'), ProfitPerMonthID*".$this->value_usd.", IF((cProfitPerMonthID='eur'), ProfitPerMonthID*".$this->value_eur.",ProfitPerMonthID)) when MaxProfitPerMonthID > 0 then IF((cMaxProfitPerMonthID='usd'), MaxProfitPerMonthID*".$this->value_usd.", IF((cMaxProfitPerMonthID='eur'), MaxProfitPerMonthID*".$this->value_eur.",MaxProfitPerMonthID)) END) as 'profit'   FROM `Sell` WHERE 1 and  `SubCategoryID` in (".implode(",",$this->category_list).") and (ProfitPerMonthID > 0 and IF((cProfitPerMonthID='usd'), ProfitPerMonthID*".$this->value_usd.", IF((cProfitPerMonthID='eur'), ProfitPerMonthID*".$this->value_eur.",ProfitPerMonthID)) < ".$this->max_profit.");";
			$result = $this->db->Select($sql);
			while ($row = mysql_fetch_object($result)) {
				$array["profit"] = intval($row->profit);
			}			
			
			$sql = "SELECT AVG(IF((cArendaCostID='usd'), ArendaCostID*".$this->value_usd.", IF((cArendaCostID='eur'), ArendaCostID*".$this->value_eur.",ArendaCostID))) as rent FROM `Sell` WHERE 1 and  `SubCategoryID` in (".implode(",",$this->category_list).") and (ArendaCostID>0  and IF((cArendaCostID='usd'), ArendaCostID*".$this->value_usd.", IF((cArendaCostID='eur'), ArendaCostID*".$this->value_eur.",ArendaCostID)) < ".$this->max_rent.");";
			$result = $this->db->Select($sql);
			while ($row = mysql_fetch_object($result)) {
				$array["rent"] = intval($row->rent);
			}			
			
			//echo $sql;
			
			return $array;			
		}
		
		public function RenderStatView() {
			
			
			$array = $this->Data_StatView();
			
			var_dump($array);
			
			$this->bg_colour = "#dce4ec";
			
			$arr_parameters = array();
			$arr_parameters["title"] = $this->title;
			$arr_parameters["bg_colour"] = $this->bg_colour;
			$arr_parameters["x_axis_3d"] = $this->x_axis_3d;
			$arr_parameters["x_axis_colour"] = $this->x_axis_colour;
			$arr_parameters["x_axis_steps"] = $this->x_axis_steps;
			$arr_parameters["x_grid_colour"] = $this->x_grid_colour;
			$arr_parameters["x_labels"] = implode(",", array_keys($array));
			$arr_parameters["y_axis_colour"] = $this->y_axis_colour;
			$arr_parameters["y_grid_colour"] = $this->y_grid_colour;
			$arr_parameters["y_legend"] = $this->y_legend;
			$arr_parameters["y_max"] = max($array)+ intval(max($array)*0.1);
			$arr_parameters["y_min"] = 0;
			//$arr_parameters["y_ticks"] = 10;
			

			$arr_parameters["bar_3d"] = "75,".$this->array_color[0].",100";
			$arr_parameters["values"] = implode(",", array_values($array));					
					
			
			$result = "";
			
			while (list($k, $v) = each($arr_parameters)) {
				
				$result .= "&".$k."=".$v;
				
			}
			
			return $result;			
			
		}
		
		public function RenderShowInfoForBuyer() {
			
			$array = $this->Data_RenderShowInfoForBuyer();
			
			//$this->pie_labels = "cost, expemse, wagefund, profit";
			
			$this->bg_colour = "#dce4ec";
			
			$arr_parameters = array();
			$arr_parameters["title"] = $this->title;
			$arr_parameters["pie_labels"] = $this->pie_labels;
			$arr_parameters["values"] = implode(",", $array);
			$arr_parameters["pie"] = $this->pie;
			$arr_parameters["colours"] = implode(",", $this->array_color);
			$arr_parameters["x_axis_steps"] = $this->x_axis_steps;
			$arr_parameters["y_ticks"] = $this->y_ticks;
			$arr_parameters["line"] = $this->line;
			$arr_parameters["y_min"] = 0;
			$arr_parameters["y_max"] = 15000;
			$arr_parameters["bg_colour"] = $this->bg_colour;
			$arr_parameters["tool_tip"] = "%23val%23 (rub)"; 
			
			$result = "";
			
			while (list($k, $v) = each($arr_parameters)) {
				
				$result .= "&".$k."=".$v;
				
			}
			
			return $result;			
			
		}
		
		public function RenderCostProfitDefault() {
			
			$array_cost = array();
			$array_profit = array();
			$array_date = array();
			
			$sql = "select ID,  DATE_FORMAT(datacreate,'%d-%m-%Y') as date, IF((cCostID='usd'), CostID*".$this->value_usd.", IF((cCostID='eur'), CostID*".$this->value_eur.",CostID)) as 'cost', IF((cProfitPerMonthID='usd'), ProfitPerMonthID*".$this->value_usd.", IF((cProfitPerMonthID='eur'), ProfitPerMonthID*".$this->value_eur.",ProfitPerMonthID)) as 'profit'  from Sell where 1 and SubCategoryID in (".implode(",",$this->category_list).") and (IF((cCostID='usd'), CostID*".$this->value_usd.", IF((cCostID='eur'), CostID*".$this->value_eur.",CostID))) BETWEEN 1 AND ".$this->max_cost." and (IF((cProfitPerMonthID='usd'), ProfitPerMonthID*".$this->value_usd.", IF((cProfitPerMonthID='eur'), ProfitPerMonthID*".$this->value_eur.",ProfitPerMonthID))) BETWEEN 0 AND ".$this->max_profit." order by datacreate desc limit 50;";
			$result = $this->db->Select($sql);
			while ($row = mysql_fetch_object($result)) {
				$array_cost[] = $row->cost;	
				$array_profit[] = $row->profit;	
				$array_date[] = $row->date;	
			}

			$array_cost = array_reverse($array_cost);
			$array_profit = array_reverse($array_profit);
			$array_date = array_reverse($array_date);
			
			$this->bg_colour = "#dce4ec";
			$this->bar_fade = "95,#c63030,".$this->text_cost.",15";
			$this->bar_fade_2 = "95,#006699,".$this->text_profit.",15";
			
			$this->x_labels = implode(",", $array_date);
			$this->values = implode(",", $array_cost);
			$this->values_2 = implode(",", $array_profit);
			
			$arr_parameters = array();
			$arr_parameters["title"] = $this->title;
			$arr_parameters["bg_colour"] = $this->bg_colour;
			$arr_parameters["x_labels"] = $this->x_labels;
			$arr_parameters["values"] = $this->values;
			$arr_parameters["values_2"] = $this->values_2;
			
			$arr_parameters["bar_fade"] = $this->bar_fade;
			$arr_parameters["bar_fade_2"] = $this->bar_fade_2;
			
			$arr_parameters["y_ticks"] = $this->y_ticks;
			$arr_parameters["x_axis_steps"] = $this->x_axis_steps;
			$arr_parameters["x_label_style"] = $this->x_label_style;
			
			$arr_parameters["x_axis_colour"] = $this->x_axis_colour;
			$arr_parameters["x_axis_steps"] = $this->x_axis_steps;
			$arr_parameters["x_grid_colour"] = $this->x_grid_colour;
			$arr_parameters["y_axis_colour"] = $this->y_axis_colour;
			$arr_parameters["y_grid_colour"] = $this->y_grid_colour;
			
			$arr_parameters["y_legend"] = $this->y_legend;
			
			
			reset($array_cost);
			$arr_parameters["y_min"] = 0;
			$arr_parameters["y_max"] = $this->GetMaxValue($array_cost);
			
			$result = "";
			
			while (list($k, $v) = each($arr_parameters)) {
				
				$result .= "&".$k."=".$v;
				
			}
			
			return $result;			
			
		}
		
		public function RenderWhySellBusinessesDefault() {
			
			$array = array();
			
			$sql = "select count(ReasonSellBizID) as 'count', ReasonSellBizID as 'typeId'  from Sell where 1 and ReasonSellBizID > 0 group by ReasonSellBizID order by ReasonSellBizID asc;";
			$result = $this->db->Select($sql);
			while ($row = mysql_fetch_object($result)) {
				$array[$row->typeId] = $row->count;	
			}
			
			$this->ParseDataToProcent($array);
			
			$this->bg_colour = "#dce4ec";
			
			$arr_parameters = array();
			$arr_parameters["title"] = $this->title;
			$arr_parameters["pie_labels"] = $this->pie_labels;
			$arr_parameters["values"] = implode(",", $array);
			$arr_parameters["pie"] = $this->pie;
			$arr_parameters["colours"] = implode(",", $this->array_color);
			$arr_parameters["x_axis_steps"] = $this->x_axis_steps;
			$arr_parameters["y_ticks"] = $this->y_ticks;
			$arr_parameters["line"] = $this->line;
			$arr_parameters["y_min"] = 0;
			$arr_parameters["y_max"] = 15000;
			$arr_parameters["bg_colour"] = $this->bg_colour;
			$arr_parameters["tool_tip"] = $this->tool_tip;
			
			$result = "";
			
			while (list($k, $v) = each($arr_parameters)) {
				
				$result .= "&".$k."=".$v;
				
			}
			
			return $result;			
			
		}
		
		public function RenderRequestDefault() {
			
			$this->Parse();
			
			$array_cost_year = array();
			
			while (list($k, $v) = each($this->year_list)) {
				
				$array_cost = array();
				
				$sql = "SELECT COUNT(buyer.id) as 'count', DATE_FORMAT(datecreate, '%m') as date FROM `buyer` WHERE 1 and  `subTypeBizID` in (".implode(",",$this->category_list).") and YEAR(datecreate)=".$v." GROUP BY MONTH(datecreate)  order by MONTH(datecreate) DESC;";
				
				//mail("eugenekurilov@gmail.com", $sql, $sql);
				
				$result = $this->db->Select($sql);				
				while ($row = mysql_fetch_object($result)) {
					$array_cost[intval($row->date)] = intval($row->count);
				}
				
				$array_cost_year[$v] = $array_cost;
			}
			
			$this->ParseMonthToCost_Default($array_cost_year);

			$this->title = $this->title;
			$this->bg_colour = "#dce4ec";
			$this->x_axis_steps = 1;
			$this->y_max = $this->GetMaxValueOfYears($array_cost_year, 10);
			$this->y_min = 0;
			
			$arr_parameters = array();
			$arr_parameters["title"] = $this->title;
			$arr_parameters["bg_colour"] = $this->bg_colour;
			$arr_parameters["x_axis_3d"] = $this->x_axis_3d;
			$arr_parameters["x_axis_colour"] = $this->x_axis_colour;
			$arr_parameters["x_axis_steps"] = $this->x_axis_steps;
			$arr_parameters["x_grid_colour"] = $this->x_grid_colour;
			$arr_parameters["x_labels"] = $this->x_labels;
			$arr_parameters["y_axis_colour"] = $this->y_axis_colour;
			$arr_parameters["y_grid_colour"] = $this->y_grid_colour;
			$arr_parameters["y_legend"] = $this->y_legend;
			$arr_parameters["y_max"] = $this->y_max;
			$arr_parameters["y_min"] = $this->y_min;
			$arr_parameters["y_ticks"] = $this->y_ticks;
			
			
			$iter = 1;
			
			while (list($k, $v) = each($array_cost_year)) {
				
				if($iter == 1) {

					$arr_parameters["bar_3d"] = "75,".$this->array_color[$iter].",".$k.",".sizeof($array_cost_year[$k])."";
					$arr_parameters["values"] = implode(",",$array_cost_year[$k]);					
					
				} else {
					$arr_parameters["bar_3d_".$iter.""] = "75,".$this->array_color[$iter].",".$k.",".sizeof($array_cost_year[$k])."";
					$arr_parameters["values_".$iter.""] = implode(",",$array_cost_year[$k]);
				}
			
				
				$iter +=1;				
			}
			

			$result = "";
			
			while (list($k, $v) = each($arr_parameters)) {
				
				$result .= "&".$k."=".$v;
				
			}
			
			return $result;			
			
		}
		
		public function RenderProfitDefault() {
			
			$this->Parse();
			
			$array_cost_year = array();
			
			while (list($k, $v) = each($this->year_list)) {
				
				$array_cost = array();
				
				$sql = "select AVG(CASE when (ProfitPerMonthID > 0 and MaxProfitPerMonthID > 0) then (IF((cProfitPerMonthID='usd'), ProfitPerMonthID*".$this->value_usd.", IF((cProfitPerMonthID='eur'), ProfitPerMonthID*".$this->value_eur.",ProfitPerMonthID)) + IF((cMaxProfitPerMonthID='usd'), MaxProfitPerMonthID*".$this->value_usd.", IF((cMaxProfitPerMonthID='eur'), MaxProfitPerMonthID*".$this->value_eur.",MaxProfitPerMonthID)))/2 when ProfitPerMonthID > 0 then IF((cProfitPerMonthID='usd'), ProfitPerMonthID*".$this->value_usd.", IF((cProfitPerMonthID='eur'), ProfitPerMonthID*".$this->value_eur.",ProfitPerMonthID)) when MaxProfitPerMonthID > 0 then IF((cMaxProfitPerMonthID='usd'), MaxProfitPerMonthID*".$this->value_usd.", IF((cMaxProfitPerMonthID='eur'), MaxProfitPerMonthID*".$this->value_eur.",MaxProfitPerMonthID)) END) as 'profit', DATE_FORMAT(DataCreate, '%m') as date  from Sell where 1 and (ProfitPerMonthID > 0 and IF((cProfitPerMonthID='usd'), ProfitPerMonthID*".$this->value_usd.", IF((cProfitPerMonthID='eur'), ProfitPerMonthID*".$this->value_eur.",ProfitPerMonthID)) < ".$this->max_cost.") and  `SubCategoryID` in(".implode(",",$this->category_list).") and YEAR(DataCreate)=".$v." GROUP BY MONTH(DataCreate) order by MONTH(DataCreate) DESC;";
				
				$result = $this->db->Select($sql);				
				while ($row = mysql_fetch_object($result)) {
					$array_cost[intval($row->date)] = intval($row->profit);
				}
				
				$array_cost_year[$v] = $array_cost;
			}
			
			$this->ParseMonthToCost_Default($array_cost_year);

			$this->title = $this->title;
			$this->bg_colour = "#dce4ec";
			$this->x_axis_steps = 1;
			$this->y_max = $this->GetMaxValueOfYears($array_cost_year);
			$this->y_min = 0;
			
			$arr_parameters = array();
			$arr_parameters["title"] = $this->title;
			$arr_parameters["bg_colour"] = $this->bg_colour;
			$arr_parameters["x_axis_3d"] = $this->x_axis_3d;
			$arr_parameters["x_axis_colour"] = $this->x_axis_colour;
			$arr_parameters["x_axis_steps"] = $this->x_axis_steps;
			$arr_parameters["x_grid_colour"] = $this->x_grid_colour;
			$arr_parameters["x_labels"] = $this->x_labels;
			$arr_parameters["y_axis_colour"] = $this->y_axis_colour;
			$arr_parameters["y_grid_colour"] = $this->y_grid_colour;
			$arr_parameters["y_legend"] = $this->y_legend;
			$arr_parameters["y_max"] = $this->y_max;
			$arr_parameters["y_min"] = $this->y_min;
			$arr_parameters["y_ticks"] = $this->y_ticks;
			
			
			$iter = 1;
			
			while (list($k, $v) = each($array_cost_year)) {
				
				if($iter == 1) {

					$arr_parameters["bar_3d"] = "75,".$this->array_color[$iter].",".$k.",".sizeof($array_cost_year[$k])."";
					$arr_parameters["values"] = implode(",",$array_cost_year[$k]);					
					
				} else {
					$arr_parameters["bar_3d_".$iter.""] = "75,".$this->array_color[$iter].",".$k.",".sizeof($array_cost_year[$k])."";
					$arr_parameters["values_".$iter.""] = implode(",",$array_cost_year[$k]);
				}
			
				
				$iter +=1;				
			}
			

			$result = "";
			
			while (list($k, $v) = each($arr_parameters)) {
				
				$result .= "&".$k."=".$v;
				
			}
			
			return $result;			
			
		}
		
		public function RenderDefault() {
			
			$this->Parse();
			
			$array_cost_year = array();
			
			while (list($k, $v) = each($this->year_list)) {
				
				$array_cost = array();
				
				$sql = "SELECT AVG(IF((cCostID='usd'), CostID*".$this->value_usd.", IF((cCostID='eur'), CostID*".$this->value_eur.",CostID))) as cost,  DATE_FORMAT(DataCreate, '%m') as date FROM `Sell` WHERE 1 and  `SubCategoryID` in (".implode(",",$this->category_list).") and (CostID>0  and IF((cCostID='usd'), CostID*".$this->value_usd.", IF((cCostID='eur'), CostID*".$this->value_eur.",CostID)) < ".$this->max_cost." ) and YEAR(DataCreate)=".$v." GROUP BY MONTH(DataCreate)  order by MONTH(DataCreate) DESC;";
				//mail("eugenekurilov@gmail.com", $sql, $sql);
				
				$result = $this->db->Select($sql);				
				while ($row = mysql_fetch_object($result)) {
					$array_cost[intval($row->date)] = intval($row->cost);
				}
				
				$array_cost_year[$v] = $array_cost;
			}
			
			$this->ParseMonthToCost_Default($array_cost_year);

			$this->title = $this->title;
			$this->bg_colour = "#dce4ec";
			$this->x_axis_steps = 1;
			$this->y_max = $this->GetMaxValueOfYears($array_cost_year);
			$this->y_min = 0;
			
			$arr_parameters = array();
			$arr_parameters["title"] = $this->title;
			$arr_parameters["bg_colour"] = $this->bg_colour;
			$arr_parameters["x_axis_3d"] = $this->x_axis_3d;
			$arr_parameters["x_axis_colour"] = $this->x_axis_colour;
			$arr_parameters["x_axis_steps"] = $this->x_axis_steps;
			$arr_parameters["x_grid_colour"] = $this->x_grid_colour;
			$arr_parameters["x_labels"] = $this->x_labels;
			$arr_parameters["y_axis_colour"] = $this->y_axis_colour;
			$arr_parameters["y_grid_colour"] = $this->y_grid_colour;
			$arr_parameters["y_legend"] = $this->y_legend;
			$arr_parameters["y_max"] = $this->y_max;
			$arr_parameters["y_min"] = $this->y_min;
			$arr_parameters["y_ticks"] = $this->y_ticks;
			
			
			$iter = 1;
			
			while (list($k, $v) = each($array_cost_year)) {
				
				if($iter == 1) {

					$arr_parameters["bar_3d"] = "75,".$this->array_color[$iter].",".$k.",".sizeof($array_cost_year[$k])."";
					$arr_parameters["values"] = implode(",",$array_cost_year[$k]);					
					
				} else {
					$arr_parameters["bar_3d_".$iter.""] = "75,".$this->array_color[$iter].",".$k.",".sizeof($array_cost_year[$k])."";
					$arr_parameters["values_".$iter.""] = implode(",",$array_cost_year[$k]);
				}
			
				
				$iter +=1;				
			}
			

			$result = "";
			
			while (list($k, $v) = each($arr_parameters)) {
				
				$result .= "&".$k."=".$v;
				
			}
			
			return $result;
		}		
		
		
	}
?>