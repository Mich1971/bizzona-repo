<?php
	include_once("class.db.inc");  
	require_once("Cache/Lite.php");
	
	class Streets_Lite
	{
		var $db = "";
		var $sql = "";
		var $cityId = "";
		var $subCityId = "";
		var $id = "";
		var $aGetItem = array();
		
		function Streets_Lite() {
			global $DATABASE;
			$this->InitDB();
		}

		function InitDB() {
			global $DATABASE;
			$this->db = new DB($DATABASE["HOSTNAME"], $DATABASE["DATABASENAME"], $DATABASE["DATABASEUSER"], $DATABASE["DATABASEPASSWORD"]);
		}
		
		function Select()
		{
			$this->sql = "select streets.id, streets.name from streets where cityId=".$this->cityId.";";
			$result = $this->db->Select($this->sql);
			$array = Array();
			
			while ($row = mysql_fetch_object($result)) {
				$obj = new stdclass();
				$obj->id = $row->id;
				$obj->name = $row->name;
				$array[] = $obj;
			}			

			return $array;		
		}
		
		
		function SelectStreetSell() {
			
			if(intval($this->subCityId) > 0) {
				$this->sql = "select streets.id,
								streets.name,
								count(streets.name) as 'count' 
									from 	Sell,
											streets 
									where 
										Sell.streetID=streets.id 
											and 
										Sell.subCityID=".$this->subCityId."
										    and
										Sell.statusID in (1,2)    
									group by Sell.streetID order by count desc;";
				
			} 
			else {
				$this->sql = "select streets.id,
								streets.name,
								count(streets.name) as 'count' 
									from 	Sell,
											streets 
									where 
										Sell.streetID=streets.id 
											and 
										Sell.CityID=".$this->cityId."  
										    and
										(Sell.subCityID is null or Sell.subCityID=0)    
										    and 
										Sell.statusID in (1,2)    
									group by Sell.streetID order by count desc;";
			}
			
			$result = $this->db->Select($this->sql);
			$array = Array();
			
			while ($row = mysql_fetch_object($result)) {
				$obj = new stdclass();
				$obj->id = $row->id;
				$obj->name = $row->name;
				$obj->count = $row->count;
				$array[] = $obj;
			}			
			return $array;		
			
		}
		
		function GetItem() {
			$obj = new stdclass();
			if(isset($this->aGetItem[$this->id])) {
				return  $this->aGetItem[$this->id];
			} else {
				$this->sql = "select name from streets where 1 and streets.id=".intval($this->id).";";
				$result = $this->db->Select($this->sql);
				while ($row = mysql_fetch_object($result)) {
					$obj->name = $row->name;
				}
				$this->aGetItem[$this->id] = $obj;
			}
			return $obj;
		}

		
		function Close() {
			$this->db->Close();
		}

	}
?>