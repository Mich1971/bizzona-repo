<?php
	ini_set("magic_quotes_gpc","off");
	//ini_set("memory_limit","128M");
	ini_set("memory_limit","512M");	

	global $DATABASE;

	include_once("../phpset.inc");
	include_once("./functions.inc"); 
	require_once("./Cache/Lite.php");
        include_once("./CreateSubDirImg.php");             

 
	AssignDataBaseSetting("../config.ini");
// ��������� �����������
	require_once($_SERVER['DOCUMENT_ROOT'].'/inside/auth.php');
//

	//ini_set("display_startup_errors", "on");
	//ini_set("display_errors", "on");
	//ini_set("register_globals", "on");
	
	
	include_once("./class.district.inc"); 
	$district = new District();

	include_once("./class.metro.inc"); 
	$metro = new Metro();
  
	include_once("./class.category.inc"); 
	$category = new Category();

	include_once("./class.subcategory.inc");
	$subCategory = new SubCategory();
  
	include_once("./class.country.inc");
	$country = new Country();

	include_once("./class.city.inc");
	$city = new City();

	include_once("./class.emailoffer.inc");
	$emailoffer = new EmailOffer();

	include_once("./class.sell.inc");
	$sell = new Sell();
  
  	include_once("./class.bizplan_company.inc");
  	$bizplan_company = new BizPlanCompany();
  
	include_once("./class.bizplan.inc"); 
	$bizplan = new BizPlan();

	include_once("./class.buy.inc");
	$buyer = new Buyer();

	include_once("./class.equipment.inc");
	$equipment = new Equipment();
  
	include_once("./class.investments.inc");
	$investments = new Investments();
  
	include_once("./class.BuyerSellSubscribe.inc");
	$buyerSellSubscribe = new BuyerSellSubscribe();
	
	include_once("./class.streets.inc");
	$streets = new streets();

        include_once("./class.templateevent.inc"); 
        $templateEvent = new TemplateEvent();	

	require ('./xajax_core/xajax.inc.php');
	$xajax = new xajax();
  	
        include_once("./observer_filter.php"); 
        //filterSet('equipment');
        
        if(isset($_GET['blockip'])) {

            include_once("./class.BlockIP.inc"); 
            $blockIp = new BlockIP();

            if($_GET['blockip'] == 'up') {

                $blockIp->BlockUp(array(
                    'ip' => trim($_GET['ip'])
                ));

            } else if ($_GET['blockip'] == 'down') {

                $blockIp->BlockDown(array(
                    'ip' => trim($_GET['ip'])
                ));

            }
        }        
        
	require_once("../libs/Smarty.class.php");
	$smarty = new Smarty;
	$smarty->template_dir = "../templates";
	$smarty->compile_check = false;
	$smarty->caching = false;
	$smarty->compile_dir  = "../templates_c";
	$smarty->cache_dir = "../cache";
	$smarty->debugging = false;
	$smarty->clear_all_cache();
  
	function SelectDistrict($regionID) {
		global $city, $equipment;
		
		$default_subregion = "";

		if(isset($_GET["ID"])) {
			$equipment->id = intval($_GET["ID"]);
			$residence = $equipment->GetResidence();
			$default_subregion = $residence->subCity_id;
		}
		
		$subregiontext = "";
		
		$city->id = $regionID;
		$asubregion = $city->GetChildRegionAdmin();
		if(sizeof($asubregion) > 0) {
			$subregiontext = "<select name='subRegionID'>";
			$subregiontext.= "<option  value='0'>������ �������, ����</option>";
			while(list($k, $v) = each($asubregion)) {
				if($v->id == $default_subregion) {
					$subregiontext.= "<option value='".$v->id."' selected>".$v->title."</option>";
				} else {
					$subregiontext.= "<option value='".$v->id."'>".$v->title."</option>";
				}
			}
			$subregiontext.= "</select>";
		}
		
		$objResponse = new xajaxResponse();
		$objResponse->setCharacterEncoding("windows-1251");		
		$objResponse->assign('subRegionDiv', 'innerHTML', $subregiontext);
		return $objResponse;
	}

	function AutoLoadRegionSetting() {
		global $equipment;
		if(isset($_GET["ID"])) {
			$equipment->id  = intval($_GET["ID"]);
			$residence = $equipment->GetResidence();
			return SelectDistrict($residence->city_id);
		}
	}
	
	$sDistrict =& $xajax->registerFunction('SelectDistrict');
	$sDistrict->setParameter(0, XAJAX_INPUT_VALUE, "city_id");
	$autoLoad =& $xajax->registerFunction('AutoLoadRegionSetting');	
	
  	$data = array();
  	$data["offset"] = 0;
  	$data["rowCount"] = 0;
  	$result = $category->Select($data);
  	$smarty->assign("listCategory", $result);
  
  	$listSubCategory = $subCategory->Select($data);
  	$smarty->assign("listSubCategory", $listSubCategory);
  
	$xajax->processRequest();
	echo '<?xml version="1.0" encoding="UTF-8"?>';  
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<LINK href="../general.css" type="text/css" rel="stylesheet">

<head>
<?
	$xajax->printJavascript();
?>
</head>

<meta http-equiv="content-type" content="text/html; charset=windows-1251"/>

<body>
<?php

	if (isset($_POST) && isset($_POST["updateshop"])) {
		//echo intval($_POST["companyId"]);
		$equipment->SetParametersShop(intval($_POST["ID"]), intval($_POST["companyId"]), intval($_POST["objCompanyId"]), $_POST["urlobjCompanyId"]);
	}

	if (isset($_POST) && isset($_POST["updatepayment"])) {
		//echo "updatepayment";
  		$equipment->SetStatusPaymentByAdmin(intval($_POST["ID"]), intval($_POST["pay_summ"]), intval($_POST["pay_status"]), $_POST["pay_datetime"], (isset($_POST["pay_datetime_auto"]) ? 1 : 0 ));
  	}

  	if (isset($_POST) && sizeof($_POST) > 0 && !isset($_POST["updatepayment"]) && !isset($_POST["updateshop"])) {
  		
		// up update cache equipmentcache
		
  		$smarty->cache_dir = "../equipmentcache";
  		$smarty->clear_cache("./site/detailsEquipment.tpl", intval($_GET["ID"]));
  		$smarty->cache_dir = "../cache";
  		
  		
  		$options = array(
  			'cacheDir' => "../equipmentcache/",
   			'lifeTime' => 1
  		);
		
  		$cache = new Cache_Lite($options);
 		$cache->remove(''.intval($_GET["ID"]).'____equipmentitem');  		
 		
  		// up update cache equipmentcache
  		
  		
  		$equipment->id = intval($_GET["ID"]);
		$equipment->GetItem();
  		$equipment->name = ReplaceSymbolToName($_POST["name"]);
  		$equipment->age = $_POST["age"];
  		$equipment->cost = $_POST["cost"];
  		$equipment->ccost = $_POST["ccost"];
  		$equipment->subCity_id = $_POST["subRegionID"];
  	
		$total_typeBizID = htmlspecialchars(trim($_POST["category_id"]), ENT_QUOTES);
		$arr_typeBizID = split("[:]",$total_typeBizID);
		if (sizeof($arr_typeBizID) == 2) {
			$equipment->category_id = intval($arr_typeBizID[0]);
			$equipment->subCategory_id = intval($arr_typeBizID[1]);
		}
  	
  		$equipment->description =  TextReplace("description", addslashes(ParsingDetails($_POST["description"])));
  		$equipment->shortdescription = TextReplace("shortdescription", addslashes(ParsingShortDetails($_POST["shortdescription"])));
  		$equipment->city_text = $_POST["city_text"];
  		$equipment->city_id = $_POST["city_id"];
  		$equipment->email = $_POST["email"];  	
  		$equipment->contact_face = $_POST["contact_face"];  	  	
  		$equipment->phone = PhoneFix($_POST["phone"]);
  		$equipment->status_id = $_POST["status_id"];  	  	
  		$equipment->duration_id = $_POST["duration_id"];
  		$equipment->leasing = (isset($_POST["leasing"]) ? 1 : 0);
  	
		$equipment->youtubeURL = trim($_POST["youtubeURL"]);
		$equipment->youtubeShortURL = trim($_POST["youtubeShortURL"]);
		
		if(strlen($equipment->youtubeShortURL) <= 0 && strlen($equipment->youtubeURL) > 0) {
			
			$QUERYVAR= parse_url($equipment->youtubeURL, PHP_URL_QUERY);
			$GETVARS = explode('&',$QUERYVAR);
			foreach($GETVARS as $string){
     			list($is,$what) = explode('=',$string);
     			if($is == "v") {
     				$equipment->youtubeShortURL = "http://www.youtube.com/v/".$what."?version=3";
     				break;
     			}
			}			
			
		}  		
  		
  		if(isset($_POST["agreementcost"])) {
  			$equipment->agreementcost = 1;
  		} else {
  			$equipment->agreementcost = 0;
  		}
  	
  		$equipment->cost = str_replace(" ","",$equipment->cost);
  	
		$equipment->Update();

		if ($_POST["status_id"] != 15 && $_POST["status_id"] != 17) {	
			if($_POST["category_id"] == "0") {
				$smarty->display("./error/category.tpl");
			}
			if($_POST["city_id"] == "0" && $_POST["subRegionID"] == "0") {
				$smarty->display("./error/region.tpl");
			}
		}

  		if((int)$_POST["status_id"] == 1) {
  			$templateEvent->EmailEvent(3,4, (int)$_GET["ID"]);
  		} else if ((int)$_POST["status_id"] == 2) {
  			$templateEvent->EmailEvent(4,4, (int)$_GET["ID"]);
  		} else if (in_array((int)$_POST["status_id"], array(0, 3))) {
  			$templateEvent->EmailEvent(1,4, (int)$_GET["ID"]);
  		}
		
		
 		$count_category = $equipment->CountByCategory();
  	
  		$d = array();
  		$d["CountE"] = $count_category+1;
  		$d["ID"] = $equipment->category_id;
  		$category->UpdateCountE($d);
  	
  		$count_city = $equipment->CountByCity();
  	
  		$d = array();
  		$d["CountE"] = $count_city+1;
  		$d["ID"] = $equipment->city_id;
  		$city->UpdateCountE($d);

  		if (isset($_FILES["defaultUserIconFile"])) {
  			if (is_uploaded_file($_FILES['defaultUserIconFile']['tmp_name'])) {
				$filename = $_FILES['defaultUserIconFile']['tmp_name'];
        		$ext = substr($_FILES['defaultUserIconFile']['name'], 1 + strrpos($_FILES['defaultUserIconFile']['name'], "."));
        		$t = time();
        		move_uploaded_file($filename, "../shopimg/e".$t.".".$ext);
        		
    			$equipment->shopcartimg = "e".$t.".".$ext;
    			$equipment->UpdateShopIcon();
  			}  			
  		}
  		
		$smarty->cache_dir = "../cache";
		$resclear = $smarty->clear_all_cache();
  		
  	}
 
	if (!isset($_GET["ID"])) {
  	
		$data = array();
  		$data["sort"] = "`equipment`.`id`";
                $data["offset"] = 0;
                $data["rowCount"] = 0; 	

                $dataPage["offset"] = 0;
                $dataPage["rowCount"] = 0;
                $dataPage["sort"] = "id";
    	
		if (isset($_GET["StatusID"])) {
			$dataPage["status_id"] = intval($_GET["StatusID"]);
			$data["status_id"] = $dataPage["status_id"]; 	
		}    	
    	
                $resultPage = $equipment->Select($dataPage);

                $pagesplit = 40;

                $smarty->assign("CountRecord", sizeof($resultPage));
                $smarty->assign("CountSplit", $pagesplit);
                $smarty->assign("CountPage", ceil(sizeof($resultPage)/40));

                if (isset($_GET["offset"])) {
                        $data["offset"] = intval($_GET["offset"]);
                } else {
                        $data["offset"] = 0;
                }
                if (isset($_GET["rowCount"])) { 
                        $data["rowCount"] = intval($_GET["rowCount"]);
                } else {
                        $data["rowCount"] = $pagesplit;
                }
  	
  		$equipment_list = $equipment->Select($data);
  		$smarty->assign("list", $equipment_list);
  		
  		$smarty->display("./equipment/pagesplit.tpl", $_SERVER["REQUEST_URI"]);
  		$smarty->display("./selectequipment.tpl");
  		$smarty->display("./equipment/equipment_list.tpl");
  		$smarty->display("./equipment/pagesplit.tpl", $_SERVER["REQUEST_URI"]);

  	} else {
  		$datacity = array();
  		$datacity["offset"] = 0;
  		$datacity["rowCount"] = 0;
  		$datacity["sort"] = "ID";

  		$listCity = $city->Select($datacity); 
		$smarty->assign("listCity", $listCity);

  		$equipment->id = intval($_GET["ID"]);
  		$data = $equipment->GetItem();

  		
                if(strlen($data->icon) > 0 && strlen($data->micon) <= 0) {
                    
                    $auto_create_path = CreateSubDirImg();                    

                    $nImgFile = split("\.", $result->ImgID);
                    $sImgFile = RatioResizeImg("../simg/".$data->icon, "../simg/".$auto_create_path."/".time());
                    $splImgFile= split("\/", $sImgFile);
                    $equipment->micon = $auto_create_path."/".$splImgFile[sizeof($splImgFile)-1];
                    $equipment->UpdateMIcon();
                        
                }
    	
  		if(strlen($data->icon) > 0 && strlen($data->sicon) <= 0) {
  			
                    $sImgFile = CreateDefaultUserIcon("../simg/".$data->icon, "../sicon/e".$equipment->id);    	
                    $splImgFile= split("\/", $sImgFile);
                    $equipment->sicon = $splImgFile[sizeof($splImgFile)-1];
                    $equipment->UpdateSIcon();
  		}
  		
  		$smarty->assign("data", $data);
  		$smarty->display("./equipment/equipment_edit.tpl");
  	}
?>

<script language="JavaScript">
<?
	$autoLoad->printScript();
	echo ";";
?>
</script>
</body>
