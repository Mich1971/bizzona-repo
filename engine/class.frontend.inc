<?php
    include_once("class.MCSingleton.inc");

    class DBSingleton {

			private static $instance = null;
			public static function	getInstance() {
				if(!self::$instance) {
					self::$instance = new self();
				}
				return self::$instance;
			}

			private function __clone(){}

			private $connection = null;

			private function __construct() {
				$this->connection = mysqli_connect('localhost','bizzona','gojfap5b');
				if ($this->connection) {
                    $this->connection->query('set names cp1251');
					$this->connection->select_db('bizzona');
				}
			}

			public function Select($sql) {
				$result = $this->connection->query($sql);
				return $result;
			}

			public function Insert($sql) {
			}

			public function Update($sql) {
			}

			public function Drop($sql) {
			}

			public function Sql($sql) {
			}

		}	



    class FrontEnd {

        private static $db = null;
        private static $mc = null;
        public  static $timecache = 10800;

        public static function FormatMoney($money) {
            return trim(strrev(chunk_split (strrev($money), 3,' ')));
        }

        public static function ListColorSet() {

            self::$db = DBSingleton::getInstance();
            self::$mc = MCSingleton::getInstance();

            $_aResult = array();

            if($res = self::$mc->connection->get('colorsell'))  {

                $_aResult = unserialize($res);

            } else {

                $result = self::$db->Select('select codeId from ProcessingActions, Action, Sell where ProcessingActions.codeId=Sell.ID and Sell.statusId=1 and  Action.id=ProcessingActions.actionId and Action.categoryActionId=7  and NOW() > start and NOW() < stop and ProcessingActions.statusId=1;');
                while ($row = $result->fetch_object()) {
                    $_aResult[] = $row->codeId;
                }

                self::$mc->connection->set('colorsell', serialize($_aResult), 0, self::$timecache);

            }
            return $_aResult;
        }

        public static function LeftBlockSell() {

            self::$db = DBSingleton::getInstance();
            self::$mc = MCSingleton::getInstance();

            $_aResult = array();

            if($res = self::$mc->connection->get('leftsell'))  {

                $_aResult = unserialize($res);

            } else {

                $result = self::$db->Select('select Sell.QView,  Sell.ShareSaleID, Sell.TermBizID, Sell.HotShortDetailsID,  Sell.hotimg, Sell.ID, Sell.NameBizID,  Sell.AgreementCostID, Sell.CostID, Sell.cCostID,  Sell.PhoneID, Sell.FML, Sell.ProfitPerMonthID, Sell.cProfitPerMonthID, Sell.MaxProfitPerMonthID, Sell.cMaxProfitPerMonthID, Sell.MonthAveTurnID, Sell.cMonthAveTurnID, Sell.MaxMonthAveTurnID, Sell.cMaxMonthAveTurnID, Sell.MaxMonthAveTurnID, Sell.CityID from Sell, ProcessingActions, Action, CategoryAction where ProcessingActions.actionId=Action.id and CategoryAction.id=Action.categoryActionId and CategoryAction.id=3 and ProcessingActions.statusId=1 and Sell.ID=codeId and Sell.statusID=1 and (NOW() > start and (NOW() < stop)) order by Sell.DateStart desc;');
                while ($row = $result->fetch_object()) {

                    $obj = new stdclass();

                    $obj->AgreementCostID = $row->AgreementCostID;
                    $obj->cCostID = $row->cCostID;
                    $obj->CostID = self::FormatMoney($row->CostID);
                    $obj->ID = $row->ID;
                    $obj->NameBizID = $row->NameBizID;
                    $obj->hotimg = $row->hotimg;
                    $obj->HotShortDetailsID = $row->HotShortDetailsID;
                    $obj->PhoneID = $row->PhoneID;
                    $obj->FML = $row->FML;
                    $obj->CityID = $row->CityID;
                    $obj->TermBizID = $row->TermBizID;
                    $obj->ShareSaleID = $row->ShareSaleID;
                    $obj->ProfitPerMonthID =  self::FormatMoney($row->ProfitPerMonthID);
                    $obj->cProfitPerMonthID = $row->cProfitPerMonthID;
                    $obj->MaxProfitPerMonthID =  self::FormatMoney($row->MaxProfitPerMonthID);
                    $obj->cMaxProfitPerMonthID = $row->cMaxProfitPerMonthID;
                    $obj->QView = $row->QView;

                    $_aResult[] = $obj;
                }

                self::$mc->connection->set('leftsell', serialize($_aResult), 0, self::$timecache);

            }
            return $_aResult;


        }

        public static function Flush() {
            
            self::$mc = MCSingleton::getInstance();
            $result = self::$mc->connection->flush();
            return $result;
        }
        
        public static function RightBlockSell() {

            self::$db = DBSingleton::getInstance();
            self::$mc = MCSingleton::getInstance();

            $_aResult = array();

            if($res = self::$mc->connection->get('rightsell'))  {

                $_aResult = unserialize($res);

            } else {

                $result = self::$db->Select('select Sell.QView,  Sell.ShareSaleID, Sell.TermBizID, Sell.HotShortDetailsID,  Sell.hotimg, Sell.ID, Sell.NameBizID,  Sell.AgreementCostID, Sell.CostID, Sell.cCostID,  Sell.PhoneID, Sell.FML, Sell.ProfitPerMonthID, Sell.cProfitPerMonthID, Sell.MaxProfitPerMonthID, Sell.cMaxProfitPerMonthID, Sell.MonthAveTurnID, Sell.cMonthAveTurnID, Sell.MaxMonthAveTurnID, Sell.cMaxMonthAveTurnID, Sell.MaxMonthAveTurnID, Sell.CityID from Sell, ProcessingActions, Action, CategoryAction where ProcessingActions.actionId=Action.id and CategoryAction.id=Action.categoryActionId and CategoryAction.id=4 and ProcessingActions.statusId=1 and Sell.ID=codeId and Sell.statusID=1 and (NOW() > start and (NOW() < stop)) order by Sell.DateStart desc;');
                while ($row = $result->fetch_object()) {

                    $obj = new stdclass();

                    $obj->AgreementCostID = $row->AgreementCostID;
                    $obj->cCostID = $row->cCostID;
                    $obj->CostID = self::FormatMoney($row->CostID);
                    $obj->ID = $row->ID;
                    $obj->NameBizID = $row->NameBizID;
                    $obj->hotimg = $row->hotimg;
                    $obj->HotShortDetailsID = $row->HotShortDetailsID;
                    $obj->PhoneID = $row->PhoneID;
                    $obj->FML = $row->FML;
                    $obj->CityID = $row->CityID;
                    $obj->TermBizID = $row->TermBizID;
                    $obj->ShareSaleID = $row->ShareSaleID;
                    $obj->ProfitPerMonthID =  self::FormatMoney($row->ProfitPerMonthID);
                    $obj->cProfitPerMonthID = $row->cProfitPerMonthID;
                    $obj->MaxProfitPerMonthID = self::FormatMoney($row->MaxProfitPerMonthID);
                    $obj->cMaxProfitPerMonthID = $row->cMaxProfitPerMonthID;
                    $obj->QView = $row->QView;

                    $_aResult[] = $obj;
                }

                self::$mc->connection->set('rightsell', serialize($_aResult), 0, self::$timecache);

            }
            return $_aResult;


        }

        public static function RightBlockTopSell() {

            self::$db = DBSingleton::getInstance();
            self::$mc = MCSingleton::getInstance();

            $_aResult = array();

            if($res = self::$mc->connection->get('righttopsell'))  {

                $_aResult = unserialize($res);

            } else {

                $result = self::$db->Select('select Sell.QView,  Sell.ShareSaleID, Sell.TermBizID, Sell.HotShortDetailsID,  Sell.hotimg, Sell.ID, Sell.NameBizID,  Sell.AgreementCostID, Sell.CostID, Sell.cCostID,  Sell.PhoneID, Sell.FML, Sell.ProfitPerMonthID, Sell.cProfitPerMonthID, Sell.MaxProfitPerMonthID, Sell.cMaxProfitPerMonthID, Sell.MonthAveTurnID, Sell.cMonthAveTurnID, Sell.MaxMonthAveTurnID, Sell.cMaxMonthAveTurnID, Sell.MaxMonthAveTurnID, Sell.CityID from Sell, ProcessingActions, Action, CategoryAction where ProcessingActions.actionId=Action.id and CategoryAction.id=Action.categoryActionId and CategoryAction.id=6 and ProcessingActions.statusId=1 and Sell.ID=codeId and Sell.statusID=1 and (NOW() > start and (NOW() < stop)) order by Sell.DateStart;');
                while ($row = $result->fetch_object()) {

                    $obj = new stdclass();

                    $obj->AgreementCostID = $row->AgreementCostID;
                    $obj->cCostID = $row->cCostID;
                    $obj->CostID =  self::FormatMoney($row->CostID);
                    $obj->ID = $row->ID;
                    $obj->NameBizID = $row->NameBizID;
                    $obj->hotimg = $row->hotimg;
                    $obj->HotShortDetailsID = $row->HotShortDetailsID;
                    $obj->PhoneID = $row->PhoneID;
                    $obj->FML = $row->FML;
                    $obj->CityID = $row->CityID;
                    $obj->TermBizID = $row->TermBizID;
                    $obj->ShareSaleID = $row->ShareSaleID;
                    $obj->ProfitPerMonthID =  self::FormatMoney($row->ProfitPerMonthID);
                    $obj->cProfitPerMonthID = $row->cProfitPerMonthID;
                    $obj->MaxProfitPerMonthID = self::FormatMoney($row->MaxProfitPerMonthID);
                    $obj->cMaxProfitPerMonthID = $row->cMaxProfitPerMonthID;
                    $obj->QView = $row->QView;

                    $_aResult[] = $obj;
                }

                self::$mc->connection->set('righttopsell', serialize($_aResult), 0, self::$timecache);

            }
            return $_aResult;

        }

        public static function LeftBlockTopSell() {

            self::$db = DBSingleton::getInstance();
            self::$mc = MCSingleton::getInstance();

            $_aResult = array();

            if($res = self::$mc->connection->get('lefttopsell'))  {

                $_aResult = unserialize($res);

            } else {

                $result = self::$db->Select('select Sell.QView,  Sell.ShareSaleID, Sell.TermBizID, Sell.HotShortDetailsID,  Sell.hotimg, Sell.ID, Sell.NameBizID,  Sell.AgreementCostID, Sell.CostID, Sell.cCostID,  Sell.PhoneID, Sell.FML, Sell.ProfitPerMonthID, Sell.cProfitPerMonthID, Sell.MaxProfitPerMonthID, Sell.cMaxProfitPerMonthID, Sell.MonthAveTurnID, Sell.cMonthAveTurnID, Sell.MaxMonthAveTurnID, Sell.cMaxMonthAveTurnID, Sell.MaxMonthAveTurnID, Sell.CityID from Sell, ProcessingActions, Action, CategoryAction where ProcessingActions.actionId=Action.id and CategoryAction.id=Action.categoryActionId and CategoryAction.id=5 and ProcessingActions.statusId=1 and Sell.ID=codeId and Sell.statusID=1 and (NOW() > start and (NOW() < stop)) order by Sell.DateStart;');
                while ($row = $result->fetch_object()) {

                    $obj = new stdclass();

                    $obj->AgreementCostID = $row->AgreementCostID;
                    $obj->cCostID = $row->cCostID;
                    $obj->CostID = self::FormatMoney($row->CostID);
                    $obj->ID = $row->ID;
                    $obj->NameBizID = $row->NameBizID;
                    $obj->hotimg = $row->hotimg;
                    $obj->HotShortDetailsID = $row->HotShortDetailsID;
                    $obj->PhoneID = $row->PhoneID;
                    $obj->FML = $row->FML;
                    $obj->CityID = $row->CityID;
                    $obj->TermBizID = $row->TermBizID;
                    $obj->ShareSaleID = $row->ShareSaleID;
                    $obj->ProfitPerMonthID =  self::FormatMoney($row->ProfitPerMonthID);
                    $obj->cProfitPerMonthID = $row->cProfitPerMonthID;
                    $obj->MaxProfitPerMonthID = self::FormatMoney($row->MaxProfitPerMonthID);
                    $obj->cMaxProfitPerMonthID = $row->cMaxProfitPerMonthID;
                    $obj->QView = $row->QView;

                    $_aResult[] = $obj;
                }

                self::$mc->connection->set('lefttopsell', serialize($_aResult), 0, self::$timecache);

            }
            return $_aResult;
        }

        public static function MainBlockSell() {

						self::$db = DBSingleton::getInstance();
						self::$mc = MCSingleton::getInstance();

                        $_aResult = array();

						if($res = self::$mc->connection->get('mainsell'))  {

								$_aResult = unserialize($res);

                        } else {

								$result = self::$db->Select('select SubCategory.icon AS "SubCategoryIcon", Category.icon AS "CategoryIcon", Sell.Icon, Sell.ID, Sell.typeSellID, Sell.NameBizID, Sell.defaultUserIcon, Sell.AgreementCostID, Sell.CostID, Sell.cCostID, Sell.torgStatusID, Sell.youtubeShortURL, Sell.ShortDetailsID, Sell.PhoneID, Sell.FML, Sell.ProfitPerMonthID, Sell.cProfitPerMonthID, Sell.MaxProfitPerMonthID, Sell.cMaxProfitPerMonthID, Sell.MonthAveTurnID, Sell.cMonthAveTurnID, Sell.MaxMonthAveTurnID, Sell.cMaxMonthAveTurnID, Sell.MaxMonthAveTurnID, Sell.CityID from Sell LEFT JOIN SubCategory ON ( SubCategory.ID = Sell.SubCategoryID ) LEFT JOIN Category ON ( Category.ID = Sell.CategoryID ), ProcessingActions, Action, CategoryAction where ProcessingActions.actionId=Action.id and CategoryAction.id=Action.categoryActionId and CategoryAction.id=2 and ProcessingActions.statusId=1 and Sell.ID=codeId and Sell.statusID=1 and (NOW() > start and (NOW() < stop)) order by Sell.DateStart desc;');
								while ($row = $result->fetch_object()) {

                                    $obj = new stdclass();

                                    $obj->ID = $row->ID;
                                    $obj->typeSellID = $row->typeSellID;
                                    $obj->NameBizID = $row->NameBizID;
                                    $obj->defaultUserIcon = $row->defaultUserIcon;
                                    $obj->AgreementCostID = $row->AgreementCostID;
                                    $obj->CostID = self::FormatMoney($row->CostID);
                                    $obj->cCostID = $row->cCostID;
                                    $obj->torgStatusID = $row->torgStatusID;
                                    $obj->youtubeShortURL = $row->youtubeShortURL;
                                    $obj->ShortDetailsID = $row->ShortDetailsID;
                                    $obj->PhoneID = $row->PhoneID;
                                    $obj->FML = $row->FML;
                                    $obj->ProfitPerMonthID = self::FormatMoney($row->ProfitPerMonthID);
                                    $obj->cProfitPerMonthID = $row->cProfitPerMonthID;
                                    $obj->MaxProfitPerMonthID = self::FormatMoney($row->MaxProfitPerMonthID);
                                    $obj->cMaxProfitPerMonthID = $row->cMaxProfitPerMonthID;
                                    $obj->MonthAveTurnID = self::FormatMoney($row->MonthAveTurnID);
                                    $obj->cMonthAveTurnID = $row->cMonthAveTurnID;
                                    $obj->MaxMonthAveTurnID = self::FormatMoney($row->MaxMonthAveTurnID);
                                    $obj->cMaxMonthAveTurnID = $row->cMaxMonthAveTurnID;
                                    $obj->MaxMonthAveTurnID = $row->MaxMonthAveTurnID;
                                    $obj->CityID = $row->CityID;
                                    $obj->NameBizLatID = self::ruslat_for_name_biz($row->NameBizID);


                                    if(strlen($row->Icon) > 3) {
                                        $obj->CategoryIcon = $row->Icon;
                                    } else if (strlen($row->SubCategoryIcon)  > 0) {
                                        $obj->CategoryIcon = $row->SubCategoryIcon;
                                    } else {
                                        $obj->CategoryIcon = $row->CategoryIcon;
                                    }


                                    $_aResult[] = $obj;
								}

								self::$mc->connection->set('mainsell', serialize($_aResult), 0, self::$timecache);
						}
				   
        		        return $_aResult;
        }

        public static function ruslat_for_name_biz($string) {
            $string = ereg_replace(" ","-",$string);
            $string = ereg_replace("\"","",$string);
            $string = ereg_replace("\'","",$string);
            $string = ereg_replace("\.","-",$string);
            $string = ereg_replace("\,","-",$string);
            $string = ereg_replace("\n\r","-",$string);
            $string = ereg_replace("\(","-",$string);
            $string = ereg_replace("\)","-",$string);
            $string = ereg_replace("�","-",$string);
            $string = ereg_replace("�","-",$string);
            $string = ereg_replace("\/","-",$string);
            $string = ereg_replace("\%","",$string);
            $string = ereg_replace("\:","-",$string);

            $string = ereg_replace("�","zh",$string);
            $string = ereg_replace("�","yo",$string);
            $string = ereg_replace("�","i",$string);
            $string = ereg_replace("�","yu",$string);
            $string = ereg_replace("�","",$string);
            $string = ereg_replace("�","ch",$string);
            $string = ereg_replace("�","sh",$string);
            $string = ereg_replace("�","c",$string);
            $string = ereg_replace("�","u",$string);
            $string = ereg_replace("�","k",$string);
            $string = ereg_replace("�","e",$string);
            $string = ereg_replace("�","n",$string);
            $string = ereg_replace("�","g",$string);
            $string = ereg_replace("�","sh",$string);
            $string = ereg_replace("�","z",$string);
            $string = ereg_replace("�","h",$string);
            $string = ereg_replace("�","",$string);
            $string = ereg_replace("�","f",$string);
            $string = ereg_replace("�","y",$string);
            $string = ereg_replace("�","v",$string);
            $string = ereg_replace("�","a",$string);
            $string = ereg_replace("�","p",$string);
            $string = ereg_replace("�","r",$string);
            $string = ereg_replace("�","o",$string);
            $string = ereg_replace("�","l",$string);
            $string = ereg_replace("�","d",$string);
            $string = ereg_replace("�","y�",$string);
            $string = ereg_replace("�","ja",$string);
            $string = ereg_replace("�","s",$string);
            $string = ereg_replace("�","m",$string);
            $string = ereg_replace("�","i",$string);
            $string = ereg_replace("�","t",$string);
            $string = ereg_replace("�","b",$string);
            $string = ereg_replace("�","yo",$string);
            $string = ereg_replace("�","I",$string);
            $string = ereg_replace("�","YU",$string);
            $string = ereg_replace("�","CH",$string);
            $string = ereg_replace("�","",$string);
            $string = ereg_replace("�","SH\'",$string);
            $string = ereg_replace("�","C",$string);
            $string = ereg_replace("�","U",$string);
            $string = ereg_replace("�","K",$string);
            $string = ereg_replace("�","E",$string);
            $string = ereg_replace("�","N",$string);
            $string = ereg_replace("�","G",$string);
            $string = ereg_replace("�","SH",$string);
            $string = ereg_replace("�","Z",$string);
            $string = ereg_replace("�","H",$string);
            $string = ereg_replace("�","",$string);
            $string = ereg_replace("�","F",$string);
            $string = ereg_replace("�","Y",$string);
            $string = ereg_replace("�","V",$string);
            $string = ereg_replace("�","A",$string);
            $string = ereg_replace("�","P",$string);
            $string = ereg_replace("�","R",$string);
            $string = ereg_replace("�","O",$string);
            $string = ereg_replace("�","L",$string);
            $string = ereg_replace("�","D",$string);
            $string = ereg_replace("�","Zh",$string);
            $string = ereg_replace("�","Ye",$string);
            $string = ereg_replace("�","Ja",$string);
            $string = ereg_replace("�","S",$string);
            $string = ereg_replace("�","M",$string);
            $string = ereg_replace("�","I",$string);
            $string = ereg_replace("�","T",$string);
            $string = ereg_replace("�","B",$string);
            return $string;
        }

        public static function DefaultBlockSell($count = 5) {
            
            self::$db = DBSingleton::getInstance();
            self::$mc = MCSingleton::getInstance();
            
            $_aResult = array();

            if($res = self::$mc->connection->get('defaultsellblock'))  {

                $_aResult = unserialize($res);

            } else {
            
                $_aIds = array();

                $result = self::$db->Select('select id from Sell where Sell.StatusId = 1 and (Sell.brokerId is null or Sell.BrokerId <= 0) and length(Sell.hotimg) > 0 and YEAR(Sell.DataCreate) = YEAR(NOW())');

                while ($row = $result->fetch_object()) {
                    $_aIds[] = $row->id;
                }    

                if(!count($_aIds) || count($_aIds) <= $count) return array();

                $aSell = array();

                while(count($aSell) <= $count) {
                    $id=$_aIds[rand(0,count($_aIds)-1)];
                    if(!in_array($id, $aSell)) {
                        $aSell[] = $id;
                    }
                }

                $result = self::$db->Select('select Sell.QView,  Sell.ShareSaleID, Sell.TermBizID, Sell.HotShortDetailsID,  Sell.hotimg, Sell.ID, Sell.NameBizID,  Sell.AgreementCostID, Sell.CostID, Sell.cCostID,  Sell.PhoneID, Sell.FML, Sell.ProfitPerMonthID, Sell.cProfitPerMonthID, Sell.MaxProfitPerMonthID, Sell.cMaxProfitPerMonthID, Sell.MonthAveTurnID, Sell.cMonthAveTurnID, Sell.MaxMonthAveTurnID, Sell.cMaxMonthAveTurnID, Sell.MaxMonthAveTurnID, Sell.CityID from Sell where 1 and Sell.Id in ('.implode(',', $aSell).')');

                while ($row = $result->fetch_object()) {

                    $obj = new stdclass();

                    $obj->AgreementCostID = $row->AgreementCostID;
                    $obj->cCostID = $row->cCostID;
                    $obj->CostID = self::FormatMoney($row->CostID);
                    $obj->ID = $row->ID;
                    $obj->NameBizID = $row->NameBizID;
                    $obj->hotimg = $row->hotimg;
                    $obj->HotShortDetailsID = $row->HotShortDetailsID;
                    $obj->PhoneID = $row->PhoneID;
                    $obj->FML = $row->FML;
                    $obj->CityID = $row->CityID;
                    $obj->TermBizID = $row->TermBizID;
                    $obj->ShareSaleID = $row->ShareSaleID;
                    $obj->ProfitPerMonthID =  self::FormatMoney($row->ProfitPerMonthID);
                    $obj->cProfitPerMonthID = $row->cProfitPerMonthID;
                    $obj->MaxProfitPerMonthID =  self::FormatMoney($row->MaxProfitPerMonthID);
                    $obj->cMaxProfitPerMonthID = $row->cMaxProfitPerMonthID;
                    $obj->QView = $row->QView;

                    $_aResult[] = $obj;
                }
            
                self::$mc->connection->set('defaultsellblock', serialize($_aResult), 0, 86400);
            }
            
            return $_aResult;
            
        }
        
        
    }
?>
