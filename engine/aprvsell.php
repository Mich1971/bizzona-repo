<?php
    global $DATABASE;

    include_once("../phpset.inc");
    include_once("./functions.inc");

    AssignDataBaseSetting("../config.ini");
// ��������� �����������
	require_once($_SERVER['DOCUMENT_ROOT'].'/inside/auth.php');
//

    ini_set("display_startup_errors", "on");
    ini_set("display_errors", "on");
    ini_set("register_globals", "on");

    require_once("Cache/Lite.php");

    include_once("./class.ApprovingSell.inc");
    $aSell = new ApprovingSell();

    include_once("./class.sell.inc");
    $sell = new Sell();

    include_once("./class.templateevent.inc");
    $templateEvent = new TemplateEvent();

    include_once("./class.BadWords.inc");
    $badWords = new BadWords();


    require_once("../libs/Smarty.class.php");
    $smarty = new Smarty;
    $smarty->template_dir = "../templates";
    $smarty->compile_dir  = "../templates_c";
    $smarty->cache_dir = "../cache";
    $smarty->compile_check = true;
    $smarty->debugging     = false;

    $smarty->clear_all_cache();
    $smarty->clear_compiled_tpl("stat.tpl");


    $data = $aSell->Select(array(
                'codeId' => (int)$_GET['id'],
                'typeId' => 'sell'
    ));

    $updateStatus = false;

    if(isset($_POST['approved'])) {

        $sell->ApproveUpdate($data);
        resetcache();
        $aSell->Delete(array(
            'codeId' => (int)$_GET['id'],
            'typeId' => 'sell'
        ));

        unset($data);

        $updateStatus = true;

        $templateEvent->EmailEvent(9, 1, (int)$_GET['id']);
    }

    if(isset($_POST['ignore'])) {

        $aSell->SetCooment(array(
            'comment' => $_POST['comment'],
            'codeId' => (int)$_GET['id'],
            'typeId' => 'sell',
        ));

        $templateEvent->EmailEvent(10, 1, (int)$_GET['id']);
    }

    $comment = $aSell->GetCooment(array(
                'codeId' => (int)$_GET['id'],
                'typeId' => 'sell'
    ));

    $assignDescription = AssignDescription("../config.ini");

    if(isset($data)) {

        //up words filter

        $data->ListObjectID = $badWords->DistSelect($data->ListObjectID);
        $data->MatPropertyID = $badWords->DistSelect($data->MatPropertyID);
        $data->ShortDetailsID = $badWords->DistSelect($data->ShortDetailsID);
        $data->DetailsID = $badWords->DistSelect($data->DetailsID);

        //down words filter


        $smarty->assign("datasell", $data);

        if(isset($data->BizFormID)) {
            $BizFormID =  NameBizFormByID($data->BizFormID);
            $smarty->assign("BizFormID", $BizFormID);
        }

        if(isset($data->ReasonSellBizID)) {
            $ReasonSellBizID = NameReasonSellBizByID($data->ReasonSellBizID);
            $smarty->assign("ReasonSellBizID", $ReasonSellBizID);
        }

    }

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<LINK href="../general.css" type="text/css" rel="stylesheet">
<meta http-equiv="content-type" content="text/html; charset=windows-1251"/>
<head>
</head>

<body>

<table width='100%' cellpadding="0" cellspacing="0" style='border-top: 1px; border-bottom: 2px; border-left:1px;  border-right:1px;  border-color:#006699;  border-style: solid;'>
    <tr style="background-color:darkgreen ">
        <td  width='10%' align='left' nowrap><a href="http://<?=$_SERVER['SERVER_NAME'];?>/inside/" style="color:white;font-weight:bold;">�������</a></td>
    </tr>
<table>

<table border="0" cellpadding="0" cellspacing="0" class="main" width="100%" bgcolor="#D9D9B9">
    <tr>
        <td width="70%" style="padding:5pt;margin:4pt;" valign="top">
            <?
                if(isset($data)) {

                    if(strlen($comment)) {
                        ?>
                            <table width="100%"><tr>
                            <td align="left" style="padding:10pt;margin:10pt;color:white;" bgcolor="#d41b1b">
                                ����������� ����������: <br>
                                <hr>
                                <?=$comment;?>
                                <hr>
                            </td>
                            </tr></table>
                        <?
                    }

                    $output_detalssellleft = $smarty->fetch("./site/detalssellleft.tpl");
                    echo minimizer($output_detalssellleft);
                }
                if($updateStatus) {
                    ?><h1 align="center">������ ���� ������� � ������� ���������</h1><?
                }
            ?>
        </td>
        <td width="30%" style="background-color:#599a7b;color:white;" valign="top" align="center">

            <table width="95%" bgcolor="#a75959" style="margin:5px;padding:5px;">
                <tr>
                    <td colspan="2">������� ������ �������� ���������:</td>
                </tr>
                <tr>
                    <td colspan="2"><hr></td>
                </tr>
                <tr>
                    <td colspan="2" align="center" style="padding:5pt;">
                        <form method="post" >
                            <input type="hidden" name="approved" value="approved">
                            <input type="submit" value="������� ��������� ������������" style="padding:5pt;">
                        </form>
                    </td>
                </tr>
                <tr>
                    <td colspan="2"><hr></td>
                </tr>
                <tr>
                    <td colspan="2" align="left" style="padding:10pt;" bgcolor="#d41b1b">
                        <form method="post" >
                            ����������� ������:
                            <input type="hidden" name="ignore" value="ignore">
                            <textarea cols="50" rows="7" name="comment"></textarea>
                            <input type="submit" value="����� ��������� ������������" style="padding:5pt;">
                        </form>
                    </td>
                </tr>
            </table>

        </td>
    </tr>
</table>

<?
	function resetcache() {

		global $smarty;

		// up update cache sell
  		$smarty->cache_dir = "../sellcache";
  		$useMapGoogle = true;
  		$smarty->clear_cache("./site/detailsSell.tpl", intval($_GET['id'])."-".$useMapGoogle);
  		$useMapGoogle = false;
  		$smarty->clear_cache("./site/detailsSell.tpl", intval($_GET['id'])."-".$useMapGoogle);
  		$smarty->cache_dir = "../cache";


  		$options = array(
  			'cacheDir' => "../sellcache/",
   			'lifeTime' => 1
  		);

  		$cache = new Cache_Lite($options);


  		$_POST['ID'] = (isset($_POST['ID']) ? $_POST['ID'] : $_GET['id']);

 		$cache->remove(''.intval($_POST["ID"]).'____sellitem');
  		// up update cache sell
	}

?>

