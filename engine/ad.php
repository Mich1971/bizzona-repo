<?
	ini_set("magic_quotes_gpc","off");

	global $DATABASE;

	include_once("../phpset.inc");
	include_once("./functions.inc");

	AssignDataBaseSetting("../config.ini");
// требуется авторизация
	require_once($_SERVER['DOCUMENT_ROOT'].'/inside/auth.php');
//

	ini_set("display_startup_errors", "off");
	ini_set("display_errors", "off");
	ini_set("register_globals", "on");

	include_once("./class.ad.inc");
	$ad = new Ad();

	include_once("./class.country_lite.inc");
	$country = new Country_Lite();

	include_once("./class.category_lite.inc");
	$category = new Category_Lite();

	require_once("../libs/Smarty.class.php");
	$smarty = new Smarty;
	$smarty->template_dir = "../templates";
	$smarty->compile_check = false;
	$smarty->caching = false;
	$smarty->compile_dir  = "../templates_c";
	$smarty->debugging = false;
	$smarty->clear_all_cache();

	function PreData(ad &$ad) {

		$ad->_placeId = $_POST['placeId'];
		$ad->_note = $_POST['note'];

		$a_startDate = split('/', $_POST['startDate']);
		$a_stopDate = split('/', $_POST['stopDate']);

		$ad->_startDate = $a_startDate[2].'-'.$a_startDate[0].'-'.$a_startDate[1];
		$ad->_stopDate = $a_stopDate[2].'-'.$a_stopDate[0].'-'.$a_stopDate[1];

		$ad->_typeAdsId = $_POST['typeAdsId'];
		$ad->_siteTypeId = $_POST['siteTypeId'];
		$ad->_url = $_POST['url'];
		$ad->_statusId = $_POST['statusId'];
		$ad->_shareShowAd = $_POST['shareShowAd'];
		$ad->_listCityIds = (isset($_POST['listCityIds']) ? implode(',', $_POST['listCityIds']) : "");
		$ad->_listCategoryIds = (isset($_POST['listCategoryIds']) ? implode(',', $_POST['listCategoryIds']) : "");

		if(isset($_FILES['fileload'])) {

			$newname = uniqid("ads_");

			if (is_uploaded_file($_FILES['fileload']['tmp_name'])) {

				$filename = $_FILES['fileload']['tmp_name'];
				$ext = substr(strrchr($_FILES['fileload']['name'],'.'), 1);

				move_uploaded_file($filename, '../ads/'.$newname.'.'.$ext);

				$ad->_nameFile = $newname.'.'.$ext;
			}
		}

	}

	$smarty->assign("listplace", $ad->_aPlaceId);
	$smarty->assign("listtypeads", $ad->_aTypeAdsId);
	$smarty->assign("liststatus", $ad->_aStatusId);
	$smarty->assign("listsitetype", $ad->_aSiteTypeId);

	$listcity = $country->GetCityRussia();
	$smarty->assign("listcity", $listcity);

	$listcategory = $category->ListCategoryForAds();
	$smarty->assign("listcategory", $listcategory);


	if (isset($_POST["update"])) {

		$ad->_Id = intval($_GET['Id']);
		PreData($ad);
		$ad->Update();

	} else if (isset($_POST["add"])) {

		PreData($ad);

		$ad->Insert();
	}

	if (isset($_GET["Id"])) {
		$ad->_Id = intval($_GET["Id"]);
		$item = $ad->GetItem();
		$smarty->assign("item", $item);
	}

	$list = $ad->Select(array("acity" => $listcity, "acategory" => $listcategory));
	$smarty->assign("list", $list);

?>
	<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
    	<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	 	<LINK href="../general.css" type="text/css" rel="stylesheet">
	 	<meta http-equiv="content-type" content="text/html; charset=windows-1251"/>
	 	<body>
<?
        $smarty->display("./mainmenu.tpl");

		$smarty->display("./ad/list.tpl");
		$smarty->display("./ad/add.tpl");
?>
		</body>
		</html>
<?
?>