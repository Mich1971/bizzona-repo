<?
global $smarty;
global $count_sql_call;
   	
class DB {
    
    var $hostName = "";
    var $dataBaseName = "";
    var $dataBaseUser = "";
    var $dataBasePassword = "";
    var $faultCode = "";
    var $faultString = "";
    var $version = '1';
    var $result = "";
    var $link = null; 

    function DB($HostName = "127.0.0.1", $DataBaseName = "News", $DataBaseUser = "root", $DataBasePassword = "") {
        
        $this->hostName         = trim($HostName);
        $this->dataBaseName     = trim($DataBaseName);
        $this->dataBaseUser     = trim($DataBaseUser); 
        $this->dataBasePassword = trim($DataBasePassword);
        $this->link = mysql_connect($this->hostName, $this->dataBaseUser, $this->dataBasePassword);
        mysql_query("set names cp1251", $this->link);
        if (mysql_errno()) {
            $this->faultCode   = mysql_errno(); 
            $this->faultString = mysql_error();
        } else if (!mysql_select_db($this->dataBaseName)) {
            $this->faultCode   = mysql_errno(); 
            $this->faultString = mysql_error();
        }
        //echo $this->link;
    }  

    function CallSP($sql) {
    	
    	$mysqli = new mysqli($this->hostName, $this->dataBaseUser, $this->dataBasePassword, "bizzona");
            if (!$mysqli->query($sql)) {
                echo "CALL failed: (" . $mysqli->errno . ") " . $mysqli->error;
            }    	
    	$mysqli->close();
    }
    
    
    function Close() {
    	mysql_close($this->link);
    }
        
    function Select($sql) { 
        
        if(!mysql_ping($this->link)) return false;  
        
        global $count_sql_call;	
        $count_sql_call+=1;	
        //echo $sql."<hr>";
        $result = mysql_query($sql, $this->link);
        if (!$result) {
            die('Invalid query: ' . mysql_error());
        } else {
            $this->result = $result;
            return $result;               
        }
        
        return false;
    }
     
    function Sql($sql)  {
        
        if(!mysql_ping($this->link)) return false;  
        
        global $count_sql_call;	
        $count_sql_call+=1; 	
        //echo $sql."<hr>";
        $result = mysql_query($sql, $this->link);
        if (!$result) {
            die('Invalid query: ' . mysql_error());
        } else {
            $this->result = $result;
            return $result;
        }
        
      return false;    	
    }

    function SelectData($sql) {

        if(!mysql_ping($this->link)) return false;  
        
        global $count_sql_call;	
        $count_sql_call+=1;	 
        //echo $sql."<hr>";
        $result = mysql_query($sql, $this->link);
        $this->result = $result;

        if (!$result) {
            die('Invalid query: ' . mysql_error());
        } else {
        }      
        
        return $result;
    }

    function Delete($sql) {

        if(!mysql_ping($this->link)) return false;  
        
        global $count_sql_call;	
        $count_sql_call+=1;	
        $result = mysql_query($sql, $this->link);
        if (!$result) {
            die('Invalid query: ' . mysql_error());
        } else {
        }   
    }
  
    function Update($sql) {
        
        if(!mysql_ping($this->link)) return false;  

        global $count_sql_call;
        $count_sql_call+=1;
        //echo $sql."<hr>";
        //echo $this->link;
        $result = mysql_query($sql, $this->link);
        if (!$result) {
            //die('Invalid query: ' . mysql_error());
        } else {
        }
        
        return $result;   
    }

    function UpdateAndResult($sql) {
        
        if(!mysql_ping($this->link)) return false;  
        
        global $count_sql_call;
        $count_sql_call+=1;
        //echo $sql."<hr>";
        //echo $this->link;
        $result = mysql_query($sql, $this->link);
        if (!$result) {
            return 0;
        } else {
            return mysql_affected_rows($this->link);
        }
        
        return 0;   
    }
    
    function Insert($sql) {
        
        if(!mysql_ping($this->link)) return false;  
        
        global $count_sql_call;	
        $count_sql_call+=1;	
        //echo $sql."<hr>";
        $result = mysql_query($sql, $this->link);
        if (!$result) {
            return array("error" => mysql_error(), "code" => mysql_errno() );
        } else {
            return mysql_insert_id();
        }
    }

    function CountRows() {
        
        if(!mysql_ping($this->link)) return false;  
        
        global $count_sql_call;	
        $count_sql_call+=1;	
        $num_rows = mysql_num_rows($this->result);
        
        return $num_rows; 
    } 
    
}
?>
