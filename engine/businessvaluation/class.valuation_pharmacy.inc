<?php

	include_once("../engine/class.db.inc");  
	include_once("../engine/businessvaluation/class.valuation.inc");

	class Valuation_Pharmacy  extends Valuation  {

		public $db = "";
		public $sql = "";
		public $Id = 0;
		public $cost = 0;		
		public $profit = 0;
		public $area = 0;
		public $locaton = 0;
		public $arendatype = 0;
		public $cityId = 0;
		public $sellId = 0;
		public $lastId = 0;
			
		function __construct() {
			global $DATABASE;
			$this->InitDB();
		}

		function InitDB() {
			global $DATABASE;
			$this->db = new DB($DATABASE["HOSTNAME"], $DATABASE["DATABASENAME"], $DATABASE["DATABASEUSER"], $DATABASE["DATABASEPASSWORD"]);
		}
		
		function ParseData() {
			
			if($this->profit < 1000 && $this->profit > 0) {
				$this->profit = $this->profit * 1000;
			}
		}		
		
		function Insert() {
			
			$this->ParseData();

			$this->sql = "replace into valuation_pharmacy (	`cost`, 
									   							`area`,
									   							`locaton`,
									   							`arendatype`,
									   							`cityId`,
									   							`profit`,
									   							`sellId`
									   					)
														values
														( 
															'".$this->cost."',
															'".$this->area."',
															'".$this->locaton."',
															'".$this->arendatype."',
															'".$this->cityId."',
															'".$this->profit."',
															'".$this->sellId."'
														);";
			 
			
			$result = $this->db->Insert($this->sql);
			
			return $result;			
			
		}
		
		function GetSell() {
			
			$this->lastId = $this->GetLastId();			
			
			//echo "<p>".$this->lastId."</p>";
			
			$this->sql = "select Sell.ID, Sell.CostID, Sell.cCostID, Sell.CityID, Sell.subCityID, Sell.ProfitPerMonthID, Sell.cProfitPerMonthID, TopParameters.par3 as 'area', TopParameters.par4 as 'locaton', TopParameters.par7 as 'arendatype'  from Sell left join TopParameters on (TopParameters.objId = Sell.ID) where 1 and Sell.SubCategoryID=22 and Sell.ID > ".$this->lastId." order by Sell.ID limit 1;";
			//echo $this->sql;
			$result = $this->db->Select($this->sql);
			while ($row = mysql_fetch_object($result)) {
				
				$this->sellId = $row->ID;
				$this->cost = ($row->cCostID == "usd" ? ($row->CostID * $this->usd) : ($row->cCostID == "eur" ? ($row->CostID * $this->eur) : $row->CostID) );
				$this->profit = ($row->cProfitPerMonthID == "usd" ? ($row->ProfitPerMonthID * $this->usd) : ($row->cProfitPerMonthID == "eur" ? ($row->ProfitPerMonthID * $this->eur) : $row->ProfitPerMonthID) );
				$this->area = $row->area;
				$this->arendatype = $row->arendatype;
				$this->cityId = $row->CityID;
				$this->locaton = $row->locaton;
			}
			
			if($this->sellId > 0) {
				echo "<h1>".$this->sellId."</h1>";
				$this->SetLastId();
				$this->Insert();
				
			} else {
				$this->sellId = 0;
				$this->SetLastId();
			}
		}
		
		function SetLastId() {
			file_put_contents("../engine/businessvaluation/valuation_pharmacy.txt", $this->sellId);
		}
		
		function GetLastId() {
			return intval(file_get_contents("./../engine/businessvaluation/valuation_pharmacy.txt"));
		}
		
		function ValuationAlgorithm_1() {

			$offset = intval($this->profit * $this->offset_percent);
			
			$between_stop = intval($this->profit + $offset);
			$between_start = intval($this->profit - $offset);

			$where = " where 1 and  profit between ".$between_start." and ".$between_stop." ";
			
			if($this->area > 0) {
				$where .= " or area =".$this->area." ";
			}
			
			if($this->locaton > 0) {
				$where .= " or locaton =".$this->locaton." ";
			}			
			
			if($this->arendatype > 0) {
				$where .= " or arendatype =".$this->arendatype." ";
			}			

			$where .=  " and cityId=".$this->cityId." ";			
			
			$this->sql = " select AVG(cost) as 'cost', count(cost) as 'count' from valuation_pharmacy ".$where.";";

			//echo $this->sql;
			
			$result = $this->db->Select($this->sql);
			while ($row = mysql_fetch_object($result)) {			
				if(intval($row->cost) > 0 && intval($row->count) > 0) {
					$cost = intval($row->cost);
					return strrev(chunk_split(strrev($cost), 3,' ')); 
				} else {
					$cost = intval($this->profit * $this->profit_scale);
					return strrev(chunk_split(strrev($cost), 3,' ')); 
				}
				return intval($row->cost);
			}
			return 0;			
			
		}
		
		function ValuationAlgorithm_Multiplicator() {
			
			$offset = intval($this->profit * $this->offset_percent);
			
			$between_stop = intval($this->profit + $offset);
			$between_start = intval($this->profit - $offset);

			$where = " where 1 and  profit between ".$between_start." and ".$between_stop." ";
			$where_add = "";

			$cost_area = 0;
			$cost_locaton = 0;
			$cost_arendatype = 0;
			$cost_city = 0;			

			if($this->area > 0) {
				$where_add = " or area =".$this->area." ";
				$this->sql = " select AVG(cost) as 'cost', count(cost) as 'count' from valuation_pharmacy ".$where." ".$where_add.";";
				$result = $this->db->Select($this->sql);
				while ($row = mysql_fetch_object($result)) {			
					if(intval($row->cost) > 0 && intval($row->count) > 0) {
						$cost_area = intval($row->cost);
					} else {
						$cost_area = intval($this->profit * $this->profit_scale);
					}
				}
			}
		
			if($this->locaton > 0) {
				$where_add  = " and locaton =".$this->locaton." ";
				$this->sql = " select AVG(cost) as 'cost', count(cost) as 'count' from valuation_pharmacy ".$where." ".$where_add.";";
				$result = $this->db->Select($this->sql);
				while ($row = mysql_fetch_object($result)) {			
					if(intval($row->cost) > 0 && intval($row->count) > 0) {
						$cost_locaton = intval($row->cost);
					} else {
						$cost_locaton = intval($this->profit * $this->profit_scale);
					}
				}			
			}	

			if($this->arendatype > 0) {
				$where .= " and arendatype =".$this->arendatype." ";
				$this->sql = " select AVG(cost) as 'cost', count(cost) as 'count' from valuation_pharmacy ".$where." ".$where_add.";";
				$result = $this->db->Select($this->sql);
				while ($row = mysql_fetch_object($result)) {			
					if(intval($row->cost) > 0 && intval($row->count) > 0) {
						$cost_arendatype = intval($row->cost);
					} else {
						$cost_arendatype = intval($this->profit * $this->profit_scale);
					}
				}				
			}					
			
			if($this->cityId > 0) {
				$where_add = " and cityId=".$this->cityId." ";
				$this->sql = " select AVG(cost) as 'cost', count(cost) as 'count' from valuation_pharmacy ".$where." ".$where_add.";";
				$result = $this->db->Select($this->sql);
				while ($row = mysql_fetch_object($result)) {			
					if(intval($row->cost) > 0 && intval($row->count) > 0) {
						$cost_city = intval($row->cost);
					} else {
						$cost_city = intval($this->profit * $this->profit_scale);
					}
				}			
			}			
			
			$array_cost = array();

			if(intval($cost_area) > 0) {
				$array_cost[] = $cost_area;
			}
			if(intval($cost_locaton) > 0) {
				$array_cost[] = $cost_locaton;
			}
			if(intval($cost_arendatype) > 0) {
				$array_cost[] = $cost_arendatype;
			}
			if(intval($cost_city) > 0) {
				$array_cost[] = $cost_city;
			}												
			
			
			$result_cost = intval(array_sum($array_cost)/sizeof($array_cost));
			
			return strrev(chunk_split(strrev($result_cost), 3,' '));			
			
		}		
		
		function GetBusinessValuation() {
		
			//return $this->ValuationAlgorithm_1();
			return $this->ValuationAlgorithm_Multiplicator();			

		}
		
		
		function  GetCity() {
			
			$this->sql = "select cityId, count(cityId) as 'count', title from valuation_pharmacy, region where region.ID=cityId  and cityId in (".implode(", ", $this->array_city).") and count > ".$this->city_scale." group by cityId order by Title;";
			$result = $this->db->Select($this->sql);
			
			$array = array();
			
			$array_result = array();
			
			while ($row = mysql_fetch_object($result)) {			

				$array[$row->cityId] = convert_cyr_string($row->title,"koi8-r", "windows-1251");
			}

			while (list($k,$v) = each($array)) {
				if($k == 77) {
					$array_result[$k] = $v;		
				}
			}
			
			reset($array);
			
			while (list($k,$v) = each($array)) {
				if($k == 78) {
					$array_result[$k] = $v;		
				}
			}			
			
			reset($array);
			
			while (list($k,$v) = each($array)) {
				if($k != 78 && $k != 77) {
					$array_result[$k] = $v;		
				}
			}			
			
			return $array_result;
		}
		
		function Close() {
			$this->db->Close();
		}		
		
	}

?>