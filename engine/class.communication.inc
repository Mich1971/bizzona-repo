<?php
	include_once("class.db.inc");  
	require_once('class.phpmailer.php');

	class Communication 	{

		public $db = "";
		public $sql = "";
		public $Id = 0;
		public $objectId = 0;
		public $typeId = 1;
		public $email = "";
		public $name = "";
		public $message = "";
		public $descriptioncustomer = "";
		public $phone = "";
		public $datetime;
		public $companyEmail = "";
		public $ownerEmail = "";
		public $repeatme = 1;
		public $price = "";
		
		
		function __construct() {
			global $DATABASE;
			$this->InitDB();
		}

		function InitDB() {
			global $DATABASE;
			$this->db = new DB($DATABASE["HOSTNAME"], $DATABASE["DATABASENAME"], $DATABASE["DATABASEUSER"], $DATABASE["DATABASEPASSWORD"]);
		}

		private function Parse() {
			
			$this->name = htmlspecialchars($this->name, ENT_QUOTES);
			$this->email = htmlspecialchars($this->email, ENT_QUOTES);
			$this->phone = htmlspecialchars($this->phone, ENT_QUOTES);
			$this->message = htmlspecialchars($this->message, ENT_QUOTES);
			$this->descriptioncustomer = htmlspecialchars($this->descriptioncustomer, ENT_QUOTES);
			$this->companyEmail = htmlspecialchars($this->companyEmail, ENT_QUOTES);
			$this->ownerEmail = htmlspecialchars($this->ownerEmail, ENT_QUOTES);
			$this->price = htmlspecialchars($this->price, ENT_QUOTES);
			$this->objectId = intval($this->objectId);
		}
		
		function AntiSpan() {
			
			if(!in_array($this->email, array("vjnz17@ro.ru ", "irina@sandrleader.spb.ru", "s.pozdnyakov@scccllc.ru", "info@scccllc.ru", "tok2015@mail.ru", "emmagid@mail.ru", "veliev-av@mail.ru"))) {
				return true;
			}
			
			return false;
		}
		
		function InsertReviiew() {
			$this->Parse();
			$this->EmailMessageReview();
		}
		 
		function InsertEquipment() {

			$this->Parse();
			
			if($this->AntiSpan()) {
			 
				if($this->typeId == 6) {
					$this->EmailAscMessageEquipment($this->ownerEmail);
				} else {
					$this->EmailMessageEquipment($this->ownerEmail);
				}
			
				$this->sql = "insert into communication (name, email, phone, message, typeId, objectId,  datetime, price) values ('".$this->name."', '".$this->email."', '".$this->phone."', '".$this->message."', ".$this->typeId.", ".$this->objectId.", NOW(), '".$this->price."');"; 
				$result = $this->db->Insert($this->sql);
				$this->Id = intval($result);
				return $result;
			} else {
				$this->EmailMessage("bizzona.ru@gmail.com");				
			}
		}

		private function EmailMessageEquipment($email) {
			
			$text = "<h2>��� \"������-����\" -  ������ �� ������� ������������</h2>";
			
			$text .= "<p>���: ".$this->name."   </p>";
			$text .= "<p>�������: ".$this->phone."  </p>";
			$text .= "<p>E-mail ".$this->email."   </p>";
			$text .= "<p><hr></p>";
			$text .= "<p> ".$this->message." </p>";

			$text .= "<hr><b>���: ".$this->objectId."</b>  (<i><a href='http://www.bizzona.ru/detailsEquipment/".$this->objectId."'>http://www.bizzona.ru/detailsEquipment/".$this->objectId."</a></i>)<hr>";
			
			if(strlen($email) > 0) {
				$from  = 'bizzona.ru@gmail.com';
				$mail = new PHPMailer(); 
				$mail->CharSet = "windows-1251";
				$mail->IsSendmail();
				
				$mail->AddReplyTo($this->email, "��� ������ - ����");
				$mail->SetFrom($from, "��� ������ - ����");
				$mail->AddAddress($email);	
				$mail->Subject    = $subject;
				$mail->MsgHTML($text);        
        		$mail->Send();				
			}		
		}		
		
		private function EmailAscMessageEquipment($email) {
			
			$text = "<h2>��� \"������-����\" -  ���������� ����� ������ �� ������������</h2>";
			
			$text .= "<p>���: ".$this->name."   </p>";
			$text .= "<p>�������: ".$this->phone."  </p>";
			$text .= "<p>E-mail ".$this->email."   </p>";
			$text .= "<p><hr></p>";
			$text .= "<p> ".$this->message." </p>";

			$text .= "<hr><b>���: ".$this->objectId."</b>  (<i><a href='http://www.bizzona.ru/detailsEquipment/".$this->objectId."'>http://www.bizzona.ru/detailsEquipment/".$this->objectId."</a></i>)<hr>";
			
			if(strlen($email) > 0) {
				$from  = 'bizzona.ru@gmail.com';
				$mail = new PHPMailer(); 
				$mail->CharSet = "windows-1251";
				$mail->IsSendmail();
				
				$mail->AddReplyTo($this->email, "��� ������ - ����");
				$mail->SetFrom($from, "��� ������ - ����");
				$mail->AddAddress($email);	
				$mail->Subject    = $subject;
				$mail->MsgHTML($text);        
        		$mail->Send();				
			}		
		}		
		
		function Insert() {

			$this->Parse();
			
			if($this->AntiSpan()) {
			 
				$this->EmailMessage("bizzona.ru@gmail.com", true);
				$this->EmailMessage($this->ownerEmail);
				$this->EmailMessage($this->companyEmail);
				$this->EmailMessageRepeatMe();
			
				$this->sql = "insert into communication (name, email, phone, message, typeId, objectId,  datetime, price, descriptioncustomer) values ('".$this->name."', '".$this->email."', '".$this->phone."', '".$this->message."', ".$this->typeId.", ".$this->objectId.", NOW(), '".$this->price."', '".$this->descriptioncustomer."');"; 
				$result = $this->db->Insert($this->sql);
				$this->Id = intval($result);
				return $result;
			} else {
				$this->EmailMessage("bizzona.ru@gmail.com", true);				
			}
		}
		
		private function EmailMessageRepeatMe() {
			
			
			$text = "<h2>��� \"������-����\" -  ���� ������ �� ������� ������� ���� �������</h2>";
			
			if(strlen(trim($this->price)) > 0) {
				$text .= "<p>������������ ����: ".$this->price."   </p>";
			}
			
			$text .= "<p>���: ".$this->name."   </p>";
			$text .= "<p>�������: ".$this->phone."  </p>";
			$text .= "<p>E-mail ".$this->email."   </p>";
			$text .= "<p><hr></p>";
			$text .= "<p> ".$this->message." </p>";
			if(strlen($this->descriptioncustomer) > 0) {
				$text .= "<p><hr></p>";
				$text .= "<p><b>�������������� ������ � ����������:</b></p>";
				$text .= "<p> ".$this->descriptioncustomer." </p>";
			}
			 
			$text .= "<hr><b>���: ".$this->objectId."</b>  (<i><a href='http://www.bizzona.ru/detailsSell/".$this->objectId."'>http://www.bizzona.ru/detailsSell/".$this->objectId."</a></i>)<hr>";
			
			if(strlen($this->email) > 0 && $this->repeatme == 1) {
				$subject = "��� \"������-����\" -  ���� ������ �� ������� ������� ���� �������";
				$from  = 'bizzona.ru@gmail.com';
				$mail = new PHPMailer(); 
				$mail->CharSet = "windows-1251";
				$mail->IsSendmail();
				if(strlen($this->email) > 0) {
					$mail->AddReplyTo($this->email, "��� ������ - ����");
				} else {
					$mail->AddReplyTo($from, "��� ������ - ����");
				}
				$mail->SetFrom($from, "��� ������ - ����");
				$mail->AddAddress($this->email);	
				$mail->Subject    = $subject;
				$mail->MsgHTML($text);        
        		$mail->Send();				
			}
			
		}
		
		private function EmailMessageReview() {

			$text .= "<p>���: ".$this->name."   </p>";
			$text .= "<p>E-mail ".$this->email."   </p>";
			$text .= "<p><hr></p>";
			$text .= "<p> ".$this->message." </p>";
			
			$text .= "<hr>IP: ".$_SERVER['REMOTE_ADDR']."<hr>";	

			if($this->typeId == 10) {
				$subject = "��� ��������� ...";
			} else if ($this->typeId == 20)  {
				$subject = "��� �������� ...";
			} else if ($this->typeId == 30) {
				$subject = "��� �������� ...";
			}
			
			$from  = 'bizzona.ru@gmail.com';
			$mail = new PHPMailer(); 
			$mail->CharSet = "windows-1251";
			$mail->IsSendmail();
				
			$mail->AddReplyTo($from, "��� ������ - ����");
			$mail->SetFrom($from, "��� ������ - ����");
			$mail->AddAddress($from);	
			$mail->Subject    = $subject;
			$mail->MsgHTML($text);        
       		$mail->Send();				
		}
		
		private function EmailMessage($email, $useIP = false) {
			
			if(strlen(trim($this->price)) > 0) {
				$text = "<h2>��� \"������-����\" -  ���������� ��������� ���� ����</h2>";
			} else {
				$text = "<h2>��� \"������-����\" -  ������ �� ������� ������� </h2>";
			}
			
			if(strlen(trim($this->price)) > 0) {
				$text .= "<p>������������ ����: ".$this->price."   </p>";
			}			

			
			$text .= "<p>���: ".$this->name."   </p>";
			$text .= "<p>�������: ".$this->phone."  </p>";
			$text .= "<p>E-mail ".$this->email."   </p>";
			$text .= "<p><hr></p>";
			$text .= "<p> ".$this->message." </p>";
			if(strlen($this->descriptioncustomer) > 0) {
				$text .= "<p><hr></p>";
				$text .= "<p><b>�������������� ������ � ����������:</b></p>";
				$text .= "<p> ".$this->descriptioncustomer." </p>";
			}
			

			$text .= "<hr><b>���: ".$this->objectId."</b>  (<i><a href='http://www.bizzona.ru/detailsSell/".$this->objectId."'>http://www.bizzona.ru/detailsSell/".$this->objectId."</a></i>)<hr>";
			
			if($useIP) {
				$text .= "<hr>IP: ".$_SERVER['REMOTE_ADDR']."<hr>";	
			}
			
			if(strlen($email) > 0) {
			
				if(strlen(trim($this->price)) > 0) {
					$subject = "��� \"������-����\" -  ���������� ��������� ���� ����";
				} else {
					$subject = "��� \"������-����\" -  ������ �� ������� �������";
				}
				$from  = 'bizzona.ru@gmail.com';
				$mail = new PHPMailer(); 
				$mail->CharSet = "windows-1251";
				$mail->IsSendmail();
				
				if(strlen($this->email) > 0) {
					$mail->AddReplyTo($this->email, "��� ������ - ����");
				} else {
					$mail->AddReplyTo($from, "��� ������ - ����");
				}
				$mail->SetFrom($from, "��� ������ - ����");
				$mail->AddAddress($email);	
				//$mail->AddAddress($from);	
				$mail->Subject    = $subject;
				$mail->MsgHTML($text);        
        		$mail->Send();				
				
			}		
		}
		
		
		function Close() {
			$this->db->Close();
		}		
		
                
                public function Select($aParams) {
                    
                    $limit = '';
                    if(isset($aParams['limit'])) {
                        $limit = " limit ".(int)$aParams['limit']."";
                    }
                    
                    $where = ' where 1 and communication.objectId = Sell.ID ';

                    if(isset($aParams['objectId']) && intval($aParams['objectId']) > 0) {
                        $where .= " and communication.objectId = ".(int)$aParams['objectId']." ";
                    }
                    
                    if(isset($aParams['typeId']) && intval($aParams['typeId']) > 0) {
                        $where .= " and communication.typeId = ".(int)$aParams['typeId']." ";
                    }                    
                    
                    if(isset($aParams['brokerId']) && intval($aParams['brokerId']) > 0) {
                        $where .= " and Sell.brokerId = ".(int)$aParams['brokerId']." ";
                    }                    
                    
                    $this->sql = "select communication.* from communication, Sell ".$where." order by communication.Id desc ".$limit.";";
                    
                    //echo $this->sql;
                    
                    
                    $_aRes = array();
                    
                    $result = $this->db->Select($this->sql);
                    while ($row = mysql_fetch_object($result)) {
                        
                        $_aRes[] = array(
                            'isNow' => (date("m/d/y", strtotime($row->datetime)) == date("m/d/y", time()) ? true : false), 
                            'Id' => $row->Id,
                            'objectId' => $row->objectId,
                            'typeId' => $row->typeId,
                            'email' => strip_tags($row->email),
                            'name' => strip_tags($row->name),
                            'message' => strip_tags($row->message),
                            'datetime' => strip_tags($row->datetime),
                            'datetime_str' => strftime("%d %B %Y",  strtotime($row->datetime)),
                            'phone' => strip_tags($row->phone),
                            'price' => strip_tags($row->price),
                            'descriptioncustomer' => strip_tags($row->descriptioncustomer)
                        );
                        
                    }                    
                    
                    return $_aRes;
                }
                
	}
?>