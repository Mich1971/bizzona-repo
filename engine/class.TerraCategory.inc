<?php
	include_once("class.db.inc");  

	class TerraCategory 	{

		public $db = "";
		public $sql = "";
		public $Id = 0;
		public $Name = "";
		public $Description = "";		
		public $SeoTitle = "";
		 
		function __construct() {
			global $DATABASE;
			$this->InitDB();
		}

		function InitDB() {
			global $DATABASE;
			$this->db = new DB($DATABASE["HOSTNAME"], $DATABASE["DATABASENAME"], $DATABASE["DATABASEUSER"], $DATABASE["DATABASEPASSWORD"]);
		}
		
		function GetItem() {
			
			$_terraCategory = new TerraCategory();
			$this->sql = "select Id, Name, Description, SeoTitle from TerraCategory where 1 and Id=".intval($this->Id).";";
			
			$result = $this->db->Select($this->sql);
			while ($row = mysql_fetch_object($result)) {
				$_terraCategory->Id = intval($row->Id);
				$_terraCategory->Name = $row->Name;
				$_terraCategory->Description = $row->Description;
				$_terraCategory->SeoTitle = $row->SeoTitle;
			}
			return $_terraCategory;
			
		}		
		
		function Select() {

			$array = Array();
			
			$this->sql = "select Id, Name, Description, SeoTitle from TerraCategory where 1 order by Name;";
			
			$result = $this->db->Select($this->sql);
			while ($row = mysql_fetch_object($result)) {
				
				$_terraCategory = new TerraCategory();
				$_terraCategory->Id = intval($row->Id);
				$_terraCategory->Name = $row->Name;
				$_terraCategory->Description = $row->Description;
				$_terraCategory->SeoTitle = $row->SeoTitle;
				
				$array[] = $_terraCategory;
			}
			
			return $array;
		}
		
		function Close() {
			$this->db->Close();
		}		
		
	}

?>