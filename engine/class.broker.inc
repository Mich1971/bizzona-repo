<?php
	include_once("class.db.inc");  

	class Broker 	{

		public $db = "";
		public $sql = "";
		public $Id = 0;
		public $companyName = "";
		public $shortDescription = "";
		public $NameForSite = "";
		public $about_forsite = "";
		public $contact_forsite = "";
		public $vacancy_forsite = "";
		
		function __construct() {
			global $DATABASE;
			$this->InitDB();
		}

		function InitDB() {
			global $DATABASE;
			$this->db = new DB($DATABASE["HOSTNAME"], $DATABASE["DATABASENAME"], $DATABASE["DATABASEUSER"], $DATABASE["DATABASEPASSWORD"]);
		}
		
		function GetItem() {
			
			$_broker = new stdClass();
			$this->sql = "select Id, NameForSite, about_forsite, contact_forsite, vacancy_forsite, service_forsite  from company where 1 and  company.Id=".intval($this->Id).";";

			$result = $this->db->Select($this->sql);
			
			while ($row = mysql_fetch_object($result)) {
				$_broker->Id = intval($row->Id);
				$_broker->NameForSite = $row->NameForSite;
				$_broker->about_forsite = $row->about_forsite;
				$_broker->contact_forsite = $row->contact_forsite;
				$_broker->vacancy_forsite = $row->vacancy_forsite;
				$_broker->service_forsite = $row->service_forsite;
				
			}
			return $_broker;			
			
		}		
		
		function Close() {
			$this->db->Close();
		}		
		
	}

?>