<?php
  global $smarty,
         $sell,
         $category,
         $city;
// требуется авторизация
	require_once($_SERVER['DOCUMENT_ROOT'].'/inside/auth.php');
//
     
  if (isset($_POST["insert"])) {

    $data["FML"] = trim($_POST["FML"]);
    $data["EmailID"] = trim($_POST["EmailID"]);
    $data["PhoneID"] = trim($_POST["PhoneID"]);
    $data["NameBizID"] = trim($_POST["NameBizID"]);
    $data["CategoryID"] = trim($_POST["CategoryID"]);
    $data["BizFormID"] = trim($_POST["BizFormID"]);
    $data["CityID"] = trim($_POST["CityID"]);
    $data["SiteID"] = trim($_POST["SiteID"]);
    $data["CostID"] = trim($_POST["CostID"]);
    $data["ProfitPerMonthID"] = trim($_POST["ProfitPerMonthID"]);
    $data["TimeInvestID"] = trim($_POST["TimeInvestID"]);
    $data["ListObjectID"] = trim($_POST["ListObjectID"]);
    $data["ContactID"] = trim($_POST["ContactID"]);
    $data["MatPropertyID"] = trim($_POST["MatPropertyID"]);
    $data["MonthAveTurnID"] = trim($_POST["MonthAveTurnID"]);
    $data["MonthExpemseID"] = trim($_POST["MonthExpemseID"]);
    $data["SumDebtsID"] = trim($_POST["SumDebtsID"]);
    $data["WageFundID"] = trim($_POST["WageFundID"]);
    $data["ObligationID"] = trim($_POST["ObligationID"]);
    $data["CountEmplID"] = trim($_POST["CountEmplID"]);
    $data["CountManEmplID"] = trim($_POST["CountManEmplID"]);
    $data["TermBizID"] = trim($_POST["TermBizID"]);
    $data["ShortDetailsID"] = trim($_POST["TermBizID"]);
    $data["DetailsID"] = trim($_POST["ShortDetailsID"]);
    $data["ImgID"] = trim($_POST["ImgID"]);
    $data["FaxID"] = trim($_POST["FaxID"]);
    $data["ShareSaleID"] = trim($_POST["ShareSaleID"]);
    $data["ReasonSellBizID"] = trim($_POST["ReasonSellBizID"]);
    $data["TarifID"] = trim($_POST["TarifID"]);


    $sell->Insert($data);


    ?><span>Запись добавлена</span><?
  } 
  

  $data["offset"]   = 0;
  $data["rowCount"] = 0;
  $listCategory = $category->Select($data); 


  $data["offset"]   = 0;
  $data["rowCount"] = 0;
  $data["sort"] = "title";
  $listCity = $city->Select($data); 



  $smarty->assign("listCategory", $listCategory);
  $smarty->assign("listCity", $listCity);
  AssignDescription();


  $smarty->display("./sell/insert.tpl");

?>