<?
    ini_set("magic_quotes_gpc","off");

    global $DATABASE;

    include_once("../phpset.inc");
    include_once("./functions.inc");

    AssignDataBaseSetting("../config.ini");
// требуется авторизация
	require_once($_SERVER['DOCUMENT_ROOT'].'/inside/auth.php');
//

    ini_set("display_startup_errors", "on");
    ini_set("display_errors", "on");
    ini_set("register_globals", "on");

    include("../engine/FCKeditor/fckeditor.php");

    include_once("./class.AdsAction.inc");
    $adsAction = new AdsAction();

    require_once("../libs/Smarty.class.php");
    $smarty = new Smarty;
    $smarty->template_dir = "../templates";
    $smarty->compile_check = false;
    $smarty->caching = false;
    $smarty->compile_dir  = "../templates_c";
    $smarty->debugging = false;
    $smarty->clear_all_cache();

    function PreData(AdsAction &$adsAction) {

            $adsAction->_subject = $_POST["subject"];
            $adsAction->_body =  $_POST["body"];
            $adsAction->_id = (isset($_GET['id']) ? intval($_GET['id']) : 0);

    }


    if (isset($_POST["update"])) {
        PreData($adsAction);
        $adsAction->Update();

    } else if (isset($_POST["add"])) {
        PreData($adsAction);
        $adsAction->Insert();
    }

    if (isset($_GET["id"])) {
            $adsAction->_id = intval($_GET["id"]);
            $item = $adsAction->GetItem();
            $smarty->assign("item", $item);
    }

    $list = $adsAction->Select();
    $smarty->assign("list", $list);
?>
	<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
    	<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	 	<LINK href="../general.css" type="text/css" rel="stylesheet">
	 	<meta http-equiv="content-type" content="text/html; charset=windows-1251"/>
	 	<body>
<?
                $smarty->display("./mainmenu.tpl");

		$smarty->display("./adsaction/list.tpl");
		$smarty->display("./adsaction/add.tpl");
?>
		</body>
		</html>
<?
?>