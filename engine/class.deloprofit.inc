<?php
	include_once("interface.sellsdapter.inc");
	include_once("../engine/class.sell.inc");
	include_once("../engine/functions.inc"); 
	include_once("../engine/typographus.php"); 
	include_once("../engine/class.mappingobjectbroker.inc"); 	
	
	function save_image($inPath,$outPath) { 
		
		 $in = fopen($inPath, "rb");     
		 $out =  fopen($outPath, "wb");     
		 while ($chunk = fread($in,8192))     
		 {         
		 	fwrite($out, $chunk, 8192);     
		 }     
		 fclose($in);     
		 fclose($out); 
	} 	
	
	
    class DeloProfit implements SellAdapter {
    	
    	public $id;
    	public $consultant = "�������� ���������"; //��������, ������������� ����������
    	public $phone = "+7 343 290-71-58"; //�������
    	public $town; // �����
    	public $priceru; // pricestr
    	public $pricestr; // ������������
    	public $sredproiz; // �������� ������������
    	public $sertif; // �����������
    	public $dopinfo; // �������������� ����������
    	public $email = "mail@deloprofit.ru";
    	public $description;
    	public $lastupdate;
    	public $titlecategory;
    	public $title;
    	public $nedv;
    	public $product;
    	public $brokerId = 32; 
    	public $img;
    	public $bustype1;
    	public $old;
    	public $employees;
    	public $managers;
    	public $smo;
    	public $profit;
    	public $reason;
    	public $owners;
    	public $sellpart;
    	
    	
    	public $url = "http://deloprofit.ru/_outsell.php";
    	
    	private $array_dp = array();
    	
    	
    	public $category_maps = array (
    	
    											"83" => "" /*������*/,
    											"84" => "96" /*������������, ������������*/,
    											"85" => "97" /*������������ ��������*/,
    											"86" => "115" /*��������, �������� �������*/,
    											"87" => "87" /*�������������*/,
    											"88" => "78" /*�������� ���������*/,
    											"89" => "108" /*����� � ������*/,
    											"90" => "109" /*������ �������, ������-������*/,
    											"91" => "92" /*���������, ����, ����, �����*/,
    											"92" => "82" /*������������, ������*/,
    											"93" => "117" /*����� �����*/,
    											"94" => "101" /*����������*/,
    											"95" => "94" /*������������ ������������ � ���. �������*/,
    											"96" => "114" /*��������-�������, ���, �������*/,
    											"97" => "" /*������ �� �������*/,
    											"99" => "105" /*������, ������������, ���.������*/,
    											"100" => "90" /*�����������, ����������, ���, ���������*/,
    											"101" => "" /*����������, �������� ������*/,
    											"102" => "" /*������������ ��� �������*/,
    											"103" => "111" /*��������*/
    	
    										 );

		public $town_maps = array 	(
		
										"������" => array("cityId" => "72", "subCityId" => "2132"),
										"������ �����" => array("cityId" => "66", "subCityId" => ""),
										"������������" => array("cityId" => "66", "subCityId" => "1915"),
										"������ �����" => array("cityId" => "66", "subCityId" => "1935"),
										"������" => array("cityId" => "77", "subCityId" => ""),
										"���" => array("cityId" => "2", "subCityId" => "304"),
										"���������" => array("cityId" => "74", "subCityId" => "2326"),
										"������� �����" => array("cityId" => "66", "subCityId" => ""),
										"����������� �������" => array("cityId" => "74", "subCityId" => ""),
										"�����������" => array("cityId" => "66", "subCityId" => ""),
										"���������" => array("cityId" => "66", "subCityId" => ""),
										"������������ �������" => array("cityId" => "66", "subCityId" => ""),
										"�����" => array("cityId" => "66", "subCityId" => "1945"),
										"��������" => array("cityId" => "3107", "subCityId" => ""),
										"������" => array("cityId" => "66", "subCityId" => "1907"),
										"���������" => array("cityId" => "23", "subCityId" => "928"),
										"���������" => array("cityId" => "", "subCityId" => ""),
										"��������� �������" => array("cityId" => "72", "subCityId" => ""),
										"�����-���������" => array("cityId" => "78", "subCityId" => ""),
										"��������" => array("cityId" => "66", "subCityId" => "1932"),
										"�������" => array("cityId" => "66", "subCityId" => ""),
										"����������" => array("cityId" => "66", "subCityId" => ""),
										"������������" => array("cityId" => "66", "subCityId" => "1940"),
										"��������" => array("cityId" => "51", "subCityId" => "1217")
									);
    										 
    	
    	function __construct() {
    		
    	}
    	
    	public function Result() {
    		
    		return $this->array_dp;
    		
    	}
    	
    	
		private function ModerateContent($content) {
			
			return $content;
			
		}
    	
    	public function ConvertData() {

    		$typo = new Typographus();
    		
    		$data = array();
    		
			$data["FML"] =  $this->consultant;
			$data["EmailID"] = $this->email;
			$data["PhoneID"] = $this->phone;
			$data["NameBizID"] = ucfirst(strtolower($this->title));
			$data["BizFormID"] = "";
			
			if(isset($this->town_maps[$this->town])) {
				$data["CityID"] = $this->town_maps[$this->town]["cityId"];
				$data["SubCityID"] = $this->town_maps[$this->town]["subCityId"];
			} else {
				$data["CityID"] = "";
				$data["SubCityID"] = "";
			}
			
			
			$data["SiteID"] = $this->town;
			$data["CostID"] = ( intval($this->pricestr)>0 ? intval(str_replace(" ", "", $this->pricestr)) : intval(str_replace(" ", "", $this->priceru)));
			$data["cCostID"] = "rub";
			$data["txtCostID"] = (strlen($this->pricestr) > 0 ? $this->pricestr : $this->priceru);
			$data["txtProfitPerMonthID"] = "";
			$data["ProfitPerMonthID"] = $this->profit;
			$data["cProfitPerMonthID"] = "rub";
			$data["cMaxProfitPerMonthID"] = "";
			$data["TimeInvestID"] = "";
			$data["ListObjectID"] = $this->nedv; 
			$data["ContactID"] = "";
			$data["MatPropertyID"] = $this->sredproiz; 
			$data["txtMonthAveTurnID"] = "";
			$data["MonthAveTurnID"] = $this->smo;
			$data["cMonthAveTurnID"] = "rub";
			$data["cMaxMonthAveTurnID"] = "";
			$data["txtMonthExpemseID"] = "";
			$data["MonthExpemseID"] = "";
			$data["cMonthExpemseID"] = "";
			$data["txtSumDebtsID"] = "";
			$data["SumDebtsID"] = "";
			$data["cSumDebtsID"] = "";
			$data["txtWageFundID"] = "";
			$data["WageFundID"] = "";
			$data["cWageFundID"] = "";
			$data["ObligationID"] = "";
			$data["CountEmplID"] = $this->employees;
			$data["CountManEmplID"] = $this->managers;
			$data["TermBizID"] = $this->old;
			
			if(strlen(trim($this->dopinfo)) <= 0) {
				$this->dopinfo = $this->product;
			}
			
			$this->dopinfo = $this->ModerateContent($this->dopinfo);
			
			$data["ShortDetailsID"] = $this->dopinfo;
			$data["DetailsID"] = $this->dopinfo." ".$this->sertif;
			
			
			if(strlen(trim($this->img)) > 1) {
				$ext = end(explode('.', $this->img));
				if(strlen($ext) > 0 and  strlen($ext) < 5) {
					$fname_0 =  uniqid("dp_").".".$ext;
					save_image(trim($this->img), "../simg/".$fname_0);
					$data["ImgID"] = $fname_0;
				}
			}
			
			
			$data["FaxID"] = ""; 
			$data["ShareSaleID"] = $this->sellpart;
			$data["ReasonSellBizID"] = "";
			$data["TarifID"] = ""; 
			$data["StatusID"] = "";
			$data["DataCreate"] = "";
			$data["DateStart"] = "";
			$data["TimeActive"] = "";
			
			
			$data["CategoryID"] =  (isset($this->category_maps[$this->bustype1]) ? $this->category_maps[$this->bustype1] : "");
			$data["SubCategoryID"] = "";
			$data["ConstGUID"] = "";
			$data["ip"] = "";
			$data["defaultUserCountry"] = "";
			$data["login"] = "";
			$data["password"] = "";
			$data["newspaper"] = "";
			$data["helpbroker"] = "";
			$data["youtubeURL"] = "";    		
    		$data["brokerId"] = $this->brokerId;
    		$data["txtReasonSellBizID"] = $this->reason;
    		
    		
    		//up Typographus
    		
    		$typo = new Typographus();
    		
    		$data["NameBizID"] = $typo->process($data["NameBizID"]);
    		$data["SiteID"] = $typo->process($data["SiteID"]);
    		$data["ListObjectID"] = $typo->process($data["ListObjectID"]);
    		$data["MatPropertyID"] = $typo->process($data["MatPropertyID"]);
    		$data["ShortDetailsID"] = $typo->process($data["ShortDetailsID"]);
    		$data["DetailsID"] = $typo->process($data["DetailsID"]);
    		
    		//down Typographus
    		
    		return $data;
    	}
    	
    	public function CallData() {
    		
    		$xml = simplexml_load_file($this->url);
    		$this->ReadDataCallBack($xml);
    		
    	}
    	
    	
		private function convertXmlObjToArr($obj, &$arr) 
		{ 
			if(!is_object($obj)) throw new Exception("Bizzona.ru: ������ ������� ������ xml ����� �� �������� ���������� ");
			
    		$children = $obj->children(); 
    		foreach ($children as $elementName => $node) 
    		{ 
        		$nextIdx = count($arr); 
        		$arr[$nextIdx] = array(); 
        		$arr[$nextIdx]['@name'] = strtolower((string)$elementName); 
        		$arr[$nextIdx]['@attributes'] = array(); 
        		$attributes = $node->attributes(); 
        		foreach ($attributes as $attributeName => $attributeValue) 
        		{ 
            		$attribName = strtolower(trim((string)$attributeName)); 
            		$attribVal = trim((string)$attributeValue); 
            		$arr[$nextIdx]['@attributes'][$attribName] = $attribVal; 
        		} 
        		$text = (string)$node; 
        		$text = trim($text); 
        		if (strlen($text) > 0) 
        		{ 
            		$arr[$nextIdx]['@text'] = $text; 
        		} 
        		$arr[$nextIdx]['@children'] = array(); 
        		$this->convertXmlObjToArr($node, $arr[$nextIdx]['@children']); 
    		} 
    		return; 
		}  		
		
		private function ReadDataCallBack($arr) {

			$res_arr = array();
			$this->convertXmlObjToArr($arr, $res_arr);	
			
			while (list($k, $v) = each($res_arr)) {
				
				$item = $res_arr[$k];
				$sub_item = $item["@children"];
				
				$_dp = new DeloProfit(); 	
				
				while (list($k1, $v1) = each($sub_item)) {
					
					if($v1["@name"] == "itemid") {
						$_dp->id = $v1["@text"];
					}
					if($v1["@name"] == "title") {
						$_dp->title = utf8_to_cp1251($v1["@text"]);
					}
					if($v1["@name"] == "dopinfo") {
						$_dp->dopinfo = utf8_to_cp1251($v1["@text"]);
					}
					if($v1["@name"] == "consultant") {
						if(strlen($v1["@text"]) > 3) {	
							$_dp->consultant = utf8_to_cp1251($v1["@text"]);
						}
					}
					if($v1["@name"] == "phone") {
						if(strlen($v1["@text"]) > 3) {	
							$_dp->phone = utf8_to_cp1251($v1["@text"]);
						}
					}
					if($v1["@name"] == "town") {
						$_dp->town = utf8_to_cp1251($v1["@text"]);
					}
					if($v1["@name"] == "pricestr") {
						$_dp->pricestr = utf8_to_cp1251($v1["@text"]);
					}
					if($v1["@name"] == "priceru") {
						$_dp->priceru = utf8_to_cp1251($v1["@text"]);
					}					
					if($v1["@name"] == "sredproiz") {
						$_dp->sredproiz = utf8_to_cp1251($v1["@text"]);
					}
					if($v1["@name"] == "nedv") {
						$_dp->nedv = utf8_to_cp1251($v1["@text"]);
					}
					if($v1["@name"] == "sertif") {
						$_dp->sertif = utf8_to_cp1251($v1["@text"]);
					}
					if($v1["@name"] == "product") {
						$_dp->product = utf8_to_cp1251($v1["@text"]);
					}
					if($v1["@name"] == "img") {
						$_dp->img = $v1["@text"];
					}
					if($v1["@name"] == "bustype1") {
						$_dp->bustype1 = $v1["@text"];
					}					
					if($v1["@name"] == "old") {
						$_dp->old = $v1["@text"];
					}
					if($v1["@name"] == "employees") {
						$_dp->employees = intval($v1["@text"]);
					}
					if($v1["@name"] == "managers") {
						$_dp->managers = intval($v1["@text"]);
					}
					if($v1["@name"] == "smo") {
						$_dp->smo = intval($v1["@text"]);
					}
					if($v1["@name"] == "profit") {
						$_dp->profit = intval($v1["@text"]);
					}
					if($v1["@name"] == "sellpart") {
						$_dp->sellpart = intval($v1["@text"]);
					}
					if($v1["@name"] == "reason") {
						$_dp->reason = utf8_to_cp1251($v1["@text"]);
					}
					if($v1["@name"] == "owners") {
						$_dp->owners = intval($v1["@text"]);
					}
					
				}
				
				$this->array_dp[] = $_dp;	

			}
	
		}
    	
    	public function ImportData() {
    		
    		$this->CallData();
    		$result = $this->Result();
    		
    		//mail("eugenekurilov@gmail.com", sizeof($result), sizeof($result));
			    		
    		$sell = new Sell();	
			$mappingobjectbroker = new MappingObjectBroker();	

			$iter = 0;
			
			while (list($k, $v) = each($result)) {

				if($iter == 1) {
					break;
				}
				
				$mappingobjectbroker->brokerObjectID = $v->id;
				$mappingobjectbroker->brokerId = $this->brokerId;
				
				if(!$mappingobjectbroker->IsExistsBrokerObject()) {

												
					$mappingobjectbroker->bizzonaObjectId = $sell->DeloProfit_Insert($v->ConvertData());
					$mappingobjectbroker->brokerId = $this->brokerId;
					$mappingobjectbroker->brokerDateObject = HelpAdapter::unixToMySQL($v->lastupdate);
					$mappingobjectbroker->ObjectTypeId = 1;
					$mappingobjectbroker->Insert();						
					
					
					echo "<h1>OK</h1>";
					
					$iter += 1;
				} 
				
			}
			
			
    	}
    	
    	public function UpdateData() {
    		
    		$this->CallData();
    		$result = $this->Result();

    		$this->CallDataImages();
    		$resultImages = $this->ResultImages();
    		
    		while (list($k, $v) = each($result)) {
    			
    			$arr_images = array();
    			
    			while (list($k1, $v1) = each($resultImages)) {

    				$arr_url = parse_url($v1);
    				parse_str($arr_url["query"],$res_get);
    				if(isset($res_get["id"]) && $res_get["id"] == $v->id) {
    					$arr_images[] = $v1;		
    				}
    			}
    			
    			$v->images = $arr_images;
    			$result[$k] = $v;
    			
    			reset($resultImages);
    		}
    		reset($result);    		
    		
    		$sell = new Sell();
    		
			$mappingobjectbroker = new MappingObjectBroker();
			$mappingobjectbroker->brokerId = $this->brokerId;
			$mappingobjectbroker->ObjectTypeId = 1;
			$array_object = $mappingobjectbroker->Select();
			
			while (list($k, $v) = each($result)) {
				
				while (list($k1, $v1) = each($array_object)) {
					
					if($v1["brokerObjectID"] == $v->id) {
						$v1_date = strtotime($v1["brokerDateObject"]);
						$v_date = strtotime(HelpAdapter::unixToMySQL($v->lastupdate));
						
						if($v1_date < $v_date) {

							$data = $v->ConvertData();
							$data["ID"] = $v1["bizzonaObjectId"];
							
							$this->DeleteFiles($data["ID"]);
							
							$sell->DeloProfit_Update($data);
							
							$mappingobjectbroker->brokerDateObject = HelpAdapter::unixToMySQL($v->lastupdate);
							$mappingobjectbroker->brokerObjectID = $v1["brokerObjectID"];
							$mappingobjectbroker->UpdateBrokerDateObject(); 
						}

						break;
					}
				}
				reset($array_object);
			}
    	}
    	
    	public function DeleteData() {
    		
    		$sell = new Sell();
    		$this->CallData();
    		$result = $this->Result();
    		
			$mappingobjectbroker = new MappingObjectBroker();
			$mappingobjectbroker->brokerId = $this->brokerId;
			$mappingobjectbroker->ObjectTypeId = 1;
			$array_object = $mappingobjectbroker->Select(); 
			
			while (list($k, $v) = each($array_object)) {
				$status = false;	
				while (list($k1, $v1) = each($result)) {
					if($v["brokerObjectID"] == $v1->id) {
						$status = true;
						
					}
				}
				reset($result);
				
				if(!$status) {
			        //echo $v->bizzonaObjectId."<hr>";		
					$data = array();
					$data["StatusID"] = 12;
					$data["ID"] = $v["bizzonaObjectId"];
					$sell->DeloProfit_UpdateStatusID($data);
				}
				
			}
    	}
    	
    }
	

?>