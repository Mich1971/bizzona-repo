<?php

include_once("class.db.inc");
include_once("class.Service2Action.inc");

class ProcessingActions {

    private $_db;
    private $_sql;

    public $_id;
    public $_serviceId;
    public $_actionId;
    public $_codeId;
    public $_start;
    public $_stop;
    public $_statusId = 0;
    public $_created;
    public $_duration = 0;

    public static $aStatus = array(
      '0' => '���������',
      '1' => '��������',
      '2' => '���������',
    );

    function __construct() {
        global $DATABASE;
        $this->InitDB();
    }

    function InitDB() {
        global $DATABASE;
        $this->db = new DB($DATABASE["HOSTNAME"], $DATABASE["DATABASENAME"], $DATABASE["DATABASEUSER"], $DATABASE["DATABASEPASSWORD"]);
    }

    function __destruct() {

    }

    public function DeleteService($data) {

        $this->_sql = " delete from ProcessingActions where serviceId=".(int)$data['serviceId']." and codeId=".(int)$data['codeId']." ";
        $this->db->Sql($this->_sql);

    }

    public function GetPriceActions($data) {

        $this->_sql = "select id, price from Action where id in (".  implode(',', $data).");";
        
        $result = $this->db->Select($this->_sql);

        $ar = array();
        
        while ($row = mysql_fetch_object($result)) {
            
            $ar[$row->id] = $row->price;
            
        }        
        
        return $ar;
        
        
    }
    
    public function GetTotalPriceActions($data) {
        
        $this->_sql = "select sum(price) as total from Action where id in (".  implode(',', $data).");";
        //echo $this->_sql;
        
        $result = $this->db->Select($this->_sql);

        while ($row = mysql_fetch_object($result)) {
            return $row->total;
        }        
        
        return 0;
    }


    public function UpdateActionFromCabinet($data) {
        
        foreach ($data as $k => $v) {
            
            $this->_sql = "update ProcessingActions set statusId=".(int)$v['statusId']." where codeId=".(int)$v['codeId']." and actionId=".(int)$v['actionId']."  and serviceId=".(int)$v['serviceId']." limit 1;";
            
            //echo $this->_sql."<hr>";
            
            $this->db->Sql($this->_sql);
            
        }
        
    }

    public function UpdateActions($data) {

        $_action_start =  $data['action_start'];
        $_action_stop =  $data['action_stop'];
        $_action_status =  $data['action_status'];

        foreach($_action_status as $k => $v) {

            $this->_sql = " update ProcessingActions set statusId=".(int)$v.",
                                                         start='".$_action_start[$k]."',
                                                         stop='".$_action_stop[$k]."' where id=".$k." limit 1;  ";

            $this->db->Sql($this->_sql);
        }

    }

    public function InsertService($data) {

        if(isset($data['serviceId'])) {

            $service2action = new Service2Action();
            $service2action->_serviceId = (int)$data['serviceId'];
            $_aActions = $service2action->GeActions();

            $this->_serviceId = $service2action->_serviceId;
            $this->_codeId = $data['codeId'];

            foreach($_aActions as $k => $v) {
                $this->_actionId = $v['actionId'];
                $this->_duration = $v['duration'];
                $this->Insert();
            }
        }
    }

    public function Insert() {

        $this->_sql = " insert into ProcessingActions (	`serviceId`,
												        `actionId`,
												        `codeId`,
												        `start`,
												        `stop`,
												        `statusId`,
												        `created`
												       )
													values (
													'".$this->_serviceId."',
													'".$this->_actionId."',
													'".$this->_codeId."',
													NOW(),
													NOW() + INTERVAL ".$this->_duration." DAY,
													".$this->_statusId.",
													NOW()
													); ";

        //echo $this->_sql.'<hr>';

        $result = $this->db->Insert($this->_sql);
        return $result;
    }

    public function SelectAdaptFormat($data) {

        $_aResult = $this->Select($data);

        $_aReturn = array();

        foreach($_aResult as $k => $v) {

            if(!isset($_aReturn[$v['serviceId']])) {

                $_aReturn[$v['serviceId']] = array(
                    'name' => $v['ServiceName'],
                    'actions' => array($v),
                );

            } else {

                $_temp = $_aReturn[$v['serviceId']];
                $_aTemp =  $_temp['actions'];
                $_aTemp[]  = $v;
                $_temp['actions'] = $_aTemp;
                $_aReturn[$v['serviceId']] = $_temp;
            }

        }
        return $_aReturn;
    }


    public function Select($data) {

        $where = " ";

        if(isset($data['codeId'])) {
            $where .= "  and codeId=".(int)$data['codeId']." ";
        }

        $this->_sql = " select ProcessingActions.*, CategoryAction.name as 'CategoryActionName', Action.duration as 'ActionDuration', Action.price as 'ActionPrice', Action.name as 'ActionName', Services.shortName as 'ServiceName'  from ProcessingActions, Action, Services, CategoryAction  where ProcessingActions.actionId=Action.id and ProcessingActions.serviceId=Services.id and CategoryAction.id=Action.categoryActionId ".$where.";";

        $result = $this->db->Select($this->_sql);

        $arr = array();

        while ($row = mysql_fetch_object($result)) {

            $aRow = array();
            $aRow['id'] = $row->id;
            $aRow['serviceId'] = $row->serviceId;
            $aRow['actionId'] = $row->actionId;
            $aRow['codeId'] = $row->codeId;
            $aRow['start_datetime'] = $row->start;
            $aRow['stop_datetime'] = $row->stop;
            $aRow['start'] = strftime("%d %B %Y",  strtotime($row->start));
            $aRow['stop'] = strftime("%d %B %Y",  strtotime($row->stop));
            $aRow['statusId'] = $row->statusId;
            $aRow['created'] = $row->created;
            $aRow['ActionName'] = $row->ActionName;
            $aRow['ServiceName'] = $row->ServiceName;
            $aRow['ActionPrice'] = $row->ActionPrice;
            $aRow['ActionDuration'] = $row->ActionDuration;
            $aRow['CategoryActionName'] = $row->CategoryActionName;
            array_push($arr, $aRow);
        }

        return $arr;


    }


}
?>