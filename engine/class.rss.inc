<?php
	include_once("class.db.inc");
	include_once("functions.inc"); 
  
	class RSS {
		
		public $db = "";
		public $sql = "";
		public $parentRegionId = "";
		public $subCategoryId = "";
		public $categoryId = "";
		
		function __construct() {
			global $DATABASE;
			$this->InitDB();
		}

		private function InitDB() {
			global $DATABASE;
			$this->db = new DB($DATABASE["HOSTNAME"], $DATABASE["DATABASENAME"], $DATABASE["DATABASEUSER"], $DATABASE["DATABASEPASSWORD"]);
		}

		function __destruct() {
			$this->db->Close();
		}

		public function SelectRegion() {
			$this->sql = "select * from region where region.parent=".$this->parentRegionId."";
			$result = $this->db->Select($this->sql);
			$array = Array();
			while ($row = mysql_fetch_object($result)) {
				$obj = new stdclass();
				$obj->ID = $row->ID;
				$obj->Name = $row->Name;
				$obj->title = convert_cyr_string($row->title,"koi8-r", "windows-1251");
				$array[] = $obj;
			}
			return $array;
		}

		public function SelectSubCategory()	{
			
			$this->sql = "select SubCategory.ID, SubCategory.Name, SubCategory.CategoryID from SubCategory where SubCategory.CategoryID=".intval($this->subCategoryId).";";
     	
			$result = $this->db->Select($this->sql);

			$array = Array();
			while ($row = mysql_fetch_object($result)) {
				$obj = new stdclass();
				$obj->SubCategoryID = $row->ID;
				$obj->CategoryID = $row->CategoryID;
				$obj->Name = $row->Name;
				$array[] = $obj;
			}
		
			return $array;
			
			
		}
		
		public function ListRSS($data) {
			$array = Array();
			if(sizeof($data) > 0) {
				
				if (isset($data["parentRegionId"]) && intval($data["parentRegionId"]) > 0) {
					
					$regionTitle = "";
					$this->sql = "select region.title from region where region.ID=".intval($data["parentRegionId"]).";";
					$result = $this->db->Select($this->sql);
					while ($row = mysql_fetch_object($result)) {
						$regionTitle = convert_cyr_string($row->title,"koi8-r", "windows-1251");
					}
					
					if (isset($data["subCategoryId"]) && intval($data["subCategoryId"]) > 0) {

						$subCategoryTitle = "";

						$this->sql = "select SubCategory.ID, SubCategory.Name, SubCategory.CategoryID from SubCategory where SubCategory.ID=".intval($data["subCategoryId"]).";";
						$result = $this->db->Select($this->sql);
						while ($row = mysql_fetch_object($result)) {
							$subCategoryTitle = $row->Name;	
						}
					
						$this->sql = "select * from region where region.parent=".intval($data["parentRegionId"])." or region.ID=".intval($data["parentRegionId"])."";
						$result = $this->db->Select($this->sql);
						while ($row = mysql_fetch_object($result)) {
							if(strlen($row->title) > 0) { 
								$obj = new stdclass();
								$obj->ID 	= $row->ID;
								$obj->Name 	= convert_cyr_string($row->title,"koi8-r", "windows-1251");
								$obj->Url	= "http://www.bizzona.ru/rssbiz.php?subCategoryId=".intval($data["subCategoryId"])."&parentRegionId=".$row->ID."";
								$obj->Sector = intval($data["subCategoryId"]);
								$obj->Region = $row->ID;
								$obj->Description 	= "������� �������  \"".$obj->Name."\"&nbsp;(".$subCategoryTitle.")";
								$obj->Title = "������� ������� ".$obj->Name." (".$subCategoryTitle.")";
								$array[] = $obj;
							}
						}
						
						
					} else if (isset($data["categoryId"]) && intval($data["categoryId"]) > 0) {
						
						$categoryTitle = "";
						$this->sql = "select Category.ID, Category.Name from Category where Category.ID=".intval($data["categoryId"]).";";
						$result = $this->db->Select($this->sql);
						while ($row = mysql_fetch_object($result)) {
							$categoryTitle = $row->Name;
						}
					
						$this->sql = "select * from region where region.parent=".intval($data["parentRegionId"])." or region.ID=".intval($data["parentRegionId"])."";
						$result = $this->db->Select($this->sql);
						while ($row = mysql_fetch_object($result)) {
							if(strlen($row->title) > 0) { 
								$obj = new stdclass();
								$obj->ID 	= $row->ID;
								$obj->Name 	= convert_cyr_string($row->title,"koi8-r", "windows-1251");
								$obj->Url	= "http://www.bizzona.ru/rssbiz.php?categoryId=".intval($data["categoryId"])."&parentRegionId=".$row->ID."";
								$obj->Sector = intval($data["categoryId"]);
								$obj->Region = $row->ID;
								$obj->Description 	= "������� �������  \"".$obj->Name."\"&nbsp;(".$categoryTitle.")";
								$obj->Title = "������� ������� ".$obj->Name." (".$categoryTitle.")";
								$array[] = $obj;
							}
						}
						
					} else {
					
						$this->sql = "select Category.ID, Category.Name from Category;";
						$result = $this->db->Select($this->sql);
						while ($row = mysql_fetch_object($result)) {
							$obj = new stdclass();
							$obj->ID 	= $row->ID;
							$obj->Name 	= $row->Name;
							$obj->Url	= "http://www.bizzona.ru/rssbiz.php?categoryId=".$row->ID."&parentRegionId=".intval($data["parentRegionId"])."";
							$obj->Sector = $row->ID;
							$obj->Region = intval($data["parentRegionId"]);
							$obj->Description 	= "������� ������� �� ��������� \"".$row->Name."\"&nbsp;(".$regionTitle.")";
							$obj->Title = "������� ������� �� ��������� ".$row->Name." (".$regionTitle.")";
							$array[] = $obj;
						}
					}
					
				} else if (isset($data["subCategoryId"]) && intval($data["subCategoryId"]) > 0) {

					$subCategoryTitle = "";

					$this->sql = "select SubCategory.ID, SubCategory.Name, SubCategory.CategoryID from SubCategory where SubCategory.ID=".intval($data["subCategoryId"]).";";
					$result = $this->db->Select($this->sql);
					while ($row = mysql_fetch_object($result)) {
						$subCategoryTitle = $row->Name;	
					}
					
					$this->sql = "select * from region where region.parent=".$this->parentRegionId."";
					$result = $this->db->Select($this->sql);
					while ($row = mysql_fetch_object($result)) {
						if(strlen($row->title) > 0) { 
							$obj = new stdclass();
							$obj->ID 	= $row->ID;
							$obj->Name 	= convert_cyr_string($row->title,"koi8-r", "windows-1251");
							$obj->Url	= "http://www.bizzona.ru/rssbiz.php?categoryId=".intval($data["categoryId"])."&parentRegionId=".$row->ID."";
							$obj->Sector = intval($data["categoryId"]);
							$obj->Region = $row->ID;
							$obj->Description 	= "������� �������  \"".$obj->Name."\"&nbsp;(".$subCategoryTitle.")";
							$obj->Title = "������� ������� ".$obj->Name." (".$subCategoryTitle.")";
							$array[] = $obj;
						}
					}
					
					
				} else if (isset($data["categoryId"]) && intval($data["categoryId"]) > 0) {
					
					$categoryTitle = "";
					$this->sql = "select Category.ID, Category.Name from Category where Category.ID=".intval($data["categoryId"]).";";
					$result = $this->db->Select($this->sql);
					while ($row = mysql_fetch_object($result)) {
						$categoryTitle = $row->Name;
					}
					
					$this->sql = "select * from region where region.parent=".$this->parentRegionId."";
					$result = $this->db->Select($this->sql);
					while ($row = mysql_fetch_object($result)) {
						if(strlen($row->title) > 0) { 
							$obj = new stdclass();
							$obj->ID 	= $row->ID;
							$obj->Name 	= convert_cyr_string($row->title,"koi8-r", "windows-1251");
							$obj->Url	= "http://www.bizzona.ru/rssbiz.php?categoryId=".intval($data["categoryId"])."&parentRegionId=".$row->ID."";
							$obj->Sector = intval($data["categoryId"]);
							$obj->Region = $row->ID;
							$obj->Description = "������� �������  \"".$obj->Name."\"&nbsp;(".$categoryTitle.")";
							$obj->Title = "������� ������� ".$obj->Name." (".$categoryTitle.")";
							$array[] = $obj;
						}
					}

				}
				
			} else {
				$this->sql = "select Category.ID, Category.Name from Category;";
				$result = $this->db->Select($this->sql);
				while ($row = mysql_fetch_object($result)) {
					$obj = new stdclass();
					$obj->ID 	= $row->ID;
					$obj->Name 	= $row->Name;
					$obj->Url	= "http://www.bizzona.ru/rssbiz.php?categoryId=".$row->ID."";
					$obj->Sector = $row->ID;
					$obj->Region = 0;
					$obj->Description 	= "������� ������� �� ��������� \"".$row->Name."\"";
					$array[] = $obj;

				}
			}
			return $array;			
		}
		
		public function ListRSSLenta() {
			
			$dom = new DOMDocument("1.0", "UTF-8");
			$n_rss = $dom->createElement("rss");
			
			$n_rss->setAttribute("version", "2.0");
			$n_rss->setAttribute("xmlns:wfw", "http://wellformedweb.org/CommentAPI/");
			$n_rss->setAttribute("xmlns:slash", "http://purl.org/rss/1.0/modules/slash/");
			$n_rss->setAttribute("xmlns:trackback", "http://madskills.com/public/xml/rss/module/trackback/");

			$n_channel = $dom->createElement("channel");
			
			$title = iconv("windows-1251", "UTF-8", "[BizZONA.RU] ������� ������� �������.");
			$n_title = $dom->createElement("title", $title);
			$n_channel->appendChild($n_title);
			
			$link = iconv("windows-1251", "UTF-8", "http://www.bizzona.ru");
			$n_link = $dom->createElement("link", $link);
			$n_channel->appendChild($n_link);
			
			$description = iconv("windows-1251", "UTF-8", "������� ������. ������ ����");
			$n_description = $dom->createElement("description", $description);
			$n_channel->appendChild($n_description);

			$category = iconv("windows-1251", "UTF-8", "Ready Business");
			$n_category = $dom->createElement("category", $category);
			$n_channel->appendChild($n_category);
			
			$language = iconv("windows-1251", "UTF-8", "russia");
			$n_language = $dom->createElement("language", $language);
			$n_channel->appendChild($n_language);
			
			$copyright = iconv("windows-1251", "UTF-8", "Copyright, BizZONA.ru, 2006 - 2008");
			$n_copyright = $dom->createElement("copyright", $copyright);
			$n_channel->appendChild($n_copyright);
			
			$webMaster = iconv("windows-1251", "UTF-8", "webmaster@bizzona.ru");
			$n_webMaster = $dom->createElement("webMaster", $webMaster);
			$n_channel->appendChild($n_webMaster);
			
			$generator = iconv("windows-1251", "UTF-8", "www.bizzona.ru");
			$n_generator = $dom->createElement("generator", $generator);
			$n_channel->appendChild($n_generator);
			
			$n_image = $dom->createElement("image");

			$url = iconv("windows-1251", "UTF-8", "http://www.bizzona.ru");
			$n_url = $dom->createElement("url", $url);
			$n_image->appendChild($n_url);
			
			$title = iconv("windows-1251", "UTF-8", "");
			$n_title = $dom->createElement("title", $title);
			$n_image->appendChild($n_title);

			$link = iconv("windows-1251", "UTF-8", "http://www.bizzona.ru/images/ban.gif");
			$n_link = $dom->createElement("link", $link);
			$n_image->appendChild($n_link);

			
			$n_channel->appendChild($n_image);

			
			$lastBuildDate = iconv("windows-1251", "UTF-8", date("m.d.Y", time()));
			$n_lastBuildDate = $dom->createElement("lastBuildDate", $lastBuildDate);
			$n_channel->appendChild($n_lastBuildDate);
			
			$ttl = iconv("windows-1251", "UTF-8", "60");
			$n_ttl = $dom->createElement("ttl", $ttl);
			$n_channel->appendChild($n_ttl);
			
			$where = " where 1=1 and Sell.StatusID in (1,2) ";
			
			if(intval($this->parentRegionId) > 0) {
				$child = "select child from region where ID='".intval($this->parentRegionId)."'";
				$result_child = $this->db->Select($child);
				while ($row = mysql_fetch_object($result_child)) {
					$str_childs = trim($row->child);
					$str_childs = str_replace(" ",",",$str_childs);
					if(strlen($str_childs) > 0) {
						$str_childs .= ",".$this->parentRegionId;
						$where .= " and (Sell.CityID in (".$str_childs.") or Sell.subCityID in (".$str_childs.")) ";
					} else {
						$where .= " and (Sell.CityID in (".intval($this->parentRegionId).") or Sell.subCityID in (".intval($this->parentRegionId).")) ";
					}
					break;
				}
			}
			
			if(intval($this->categoryId) > 0) {
				$where .= " and Sell.CategoryID=".intval($this->categoryId)." ";
			}
			if(intval($this->subCategoryId) > 0) {
				$where .= " and Sell.SubCategoryID=".intval($this->subCategoryId)." ";
			}

			$where .= " order by ID DESC  limit 0,30";
			
			$this->sql = "Select Sell.DataCreate,  Sell.SiteID, Sell.FML, Sell.ShortDetailsID, Sell.ID, Sell.NameBizID, region.title as 'CityTitleID', Category.Name as 'CategoryTitleID' from Sell left join region on (region.ID = Sell.CityID) left join Category on (Category.ID = Sell.CategoryID) ".$where;
			
			//echo $this->sql;

			
			$result = $this->db->Select($this->sql);
			
			while ($row = mysql_fetch_object($result)) {
			
				$title = $this->ParseForRSS(iconv("windows-1251", "UTF-8", $row->NameBizID));
				$link = $this->ParseForRSS(iconv("windows-1251", "UTF-8", "http://www.bizzona.ru/detailsSell/".$row->ID)); 
				$description = $this->ParseForRSS(iconv("windows-1251", "UTF-8", $row->ShortDetailsID));
				$author = $this->ParseForRSS(iconv("windows-1251", "UTF-8", $row->FML));
				$address = $this->ParseForRSS(iconv("windows-1251", "UTF-8", $row->SiteID));
				$citytitle = $this->ParseForRSS(iconv("windows-1251", "UTF-8", convert_cyr_string($row->CityTitleID,"koi8-r", "windows-1251")));
				$pubDate = $this->ParseForRSS(iconv("windows-1251", "UTF-8", strftime("%d %B %Y",  strtotime($row->DataCreate))));
				$categorytitle = $this->ParseForRSS(iconv("windows-1251", "UTF-8", $row->CategoryTitleID));

				$n_sell = $dom->createElement("item");
				
				
				$n_title = $dom->createElement("title", $title);
				$n_link = $dom->createElement("link", $link);
				$n_description = $dom->createElement("description", $description);
				$n_author = $dom->createElement("author", $author);
				$n_address = $dom->createElement("address", $address);
				$n_citytitle = $dom->createElement("city", $citytitle);
				$n_pubDate = $dom->createElement("pubdate", $pubDate);
				$n_categorytitle = $dom->createElement("category", $categorytitle);
				
				$n_sell->appendChild($n_title);
				$n_sell->appendChild($n_link);
				$n_sell->appendChild($n_description);
				$n_sell->appendChild($n_author);
				$n_sell->appendChild($n_address);
				$n_sell->appendChild($n_citytitle);
				$n_sell->appendChild($n_pubDate);
				$n_sell->appendChild($n_categorytitle);
				
				
				$n_channel->appendChild($n_sell);
			}
			
				
			$n_rss->appendChild($n_channel);
			$dom->appendChild($n_rss);
			
			echo $dom->saveXML();
			return; 
		}
		
		private function ParseForRSS($str) {
			
			$str = htmlspecialchars($str);
			$str = str_replace("&nbsp;", " ", $str);
			$str = str_replace("&ndash;", " ", $str);
			$str = str_replace("&laquo;", " ", $str);
			$str = str_replace("&raquo;", " ", $str);
			$str = str_replace("&reg;", " ", $str);
			$str = str_replace("&sup2;", "2", $str);
			
			return $str;
		}
		
	}
  
	class RSS_Sell extends RSS {
		
		
	}
?>