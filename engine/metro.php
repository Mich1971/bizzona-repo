<?
    ini_set("magic_quotes_gpc","off");

    global $DATABASE;

    include_once("../phpset.inc");
    include_once("./functions.inc"); 

    AssignDataBaseSetting("../config.ini");
// ��������� �����������
	require_once($_SERVER['DOCUMENT_ROOT'].'/inside/auth.php');
//

    ini_set("display_startup_errors", "on");
    ini_set("display_errors", "on");
    ini_set("register_globals", "on");

    include_once("./class.metro.inc"); 
    $metro = new Metro();

    require_once("../libs/Smarty.class.php");
    $smarty = new Smarty;
    $smarty->template_dir = "../templates";
    $smarty->compile_check = false;
    $smarty->caching = false;
    $smarty->compile_dir  = "../templates_c";
    $smarty->debugging = false;
    $smarty->clear_all_cache();

    if(isset($_GET['del'])) {
        $metro->Delete($_GET['del']);
    }    
    
    if (isset($_POST["add"])) {
        
        $metro->name = addslashes($_POST['metro']);
        $metro->seobuy = '������� ������� �� �. '.$metro->name;
        $metro->seosell = '������� ������� �� �. '.$metro->name;
        $metro->regionID = (int)$_POST['city'];
        $metro->districtID = (int)$_POST['district'];
        
        $result = $metro->Insert();
        
        if(is_array($result) && $result['code'] == 1062) {
            $aError[] = '������ ����� ��� ���� ����� ��������� � ���������� ������';
            $smarty->assign('aError', $aError);
        } else {
            $aSuccess = array();
            $aSuccess[] = '�������� ����� ���� ������� ��������� � ���������� ������';
            $smarty->assign('aSuccess', $aSuccess);
        }
        
    } else if (isset($_POST["update"])) {
        
        $metro->ID = (int)$_GET['Id'];
        $metro->name = addslashes($_POST['metro']);
        $metro->seobuy = '������� ������� �� �. '.$metro->name;
        $metro->seosell = '������� ������� �� �. '.$metro->name;
        $metro->regionID = (int)$_POST['city'];
        $metro->districtID = (int)$_POST['district'];
        
        $result = $metro->Update();
        
        $aSuccess = array();
        
        if($result) {
            $aSuccess[] = '������ ����� ���� ������� ��������';
        }
        
        $smarty->assign('aSuccess', $aSuccess);
        
    }
    
    
    if(isset($_GET['Id'])) {
        
        $metro->ID = (int)$_GET['Id'];
        $itemData = $metro->GetItemAdmin();
        
        $smarty->assign("itemData", $itemData);
        
    }    
    
    $list = $metro->SelectAdmin(array());	
    
    $smarty->assign("list", $list);		
    
    $smarty->assign("listcities", $metro->ListCities());		
    $smarty->assign("listdistricts", $metro->SelectDistricts());
    
?>
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
            <LINK href="../general.css" type="text/css" rel="stylesheet">
            <meta http-equiv="content-type" content="text/html; charset=windows-1251"/> 
            <body>
<?
                $smarty->display("./mainmenu.tpl");
                $smarty->display("./metro/success.tpl");   
                $smarty->display("./metro/error.tpl");                
		$smarty->display("./metro/list.tpl");
                $smarty->display("./metro/error.tpl");                
		$smarty->display("./metro/add.tpl");
?>
            </body>
        </html>
<?
?>