<?php
	include_once("class.db.inc");  

	class News_Lite 	{
		
		public $db;
		public $sql;
		public $_Id;
		public $_title;	
		public $_shorttext;		
		public $_detailstext;
		public $_datetime;
		public $_statusId;
		
		public $_aStatusId = array(
										array('id' => 0, 'name' => '�������'),
										array('id' => 1, 'name' => '�������'),
										array('id' => 2, 'name' => '�� �������'),
									);		
		
		function __construct() {
			global $DATABASE;
			$this->InitDB();
		}

		function InitDB() {
			global $DATABASE;
			$this->db = new DB($DATABASE["HOSTNAME"], $DATABASE["DATABASENAME"], $DATABASE["DATABASEUSER"], $DATABASE["DATABASEPASSWORD"]);
		}		
		
		public function GetItem() {
			$this->sql = 'select * from news where Id='.$this->_Id.' limit 1;';
			$result = $this->db->Select($this->sql);
			while ($row = mysql_fetch_object($result)) {		
				$a = array();
				$a['Id'] = $row->Id;
				$a['title'] = $row->title;
				$a['shorttext'] = $row->shorttext;
				$a['detailstext'] = $row->detailstext;
				$a['datetime'] = $row->datetime;
				$a['datetimeStr'] = strftime("%m/%d/%Y",  strtotime($row->datetime));
				$a['statusId'] = $row->statusId;
				return $a;
			}
		}
		
		public function Select($data) {
			
			$where = ' where 1 ';
			if(isset($data['statusId'])) {
				$where .= ' and statusId='.intval($data['statusId']).' ';
			}
			
			$this->sql = 'select * from news '.$where.' order by Id desc;';
			$result = $this->db->Select($this->sql);
			
			$a = array();
			
			while ($row = mysql_fetch_object($result)) {		
				$_a = array();
				$_a['Id'] = $row->Id;
				$_a['title'] = $row->title;
				$_a['shorttext'] = $row->shorttext;
				$_a['detailstext'] = $row->detailstext;
				$_a['datetime'] = $row->datetime;
				$_a['showdatetime'] = strftime("%d.%m.%Y",  strtotime($row->datetime));
				$_a['statusId'] = $row->statusId;

				while (list($k, $v) = each($this->_aStatusId)) {
					if($v['id'] == $row->statusId) {
						$_a['statusStr'] = $v['name'];
						reset($this->_aStatusId);
						break;
					}
				}				
				
				$a[] = $_a;
			}	
			return $a;		
		}
		
	}

?>