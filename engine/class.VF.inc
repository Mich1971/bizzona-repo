<?php
	include_once("interface.sellsdapter.inc");
	include_once("../engine/class.sell.inc"); 
	include_once("../engine/class.mappingobjectbroker.inc"); 	
	
	final class VF_ObjectId {
		
		/**
    		* ����������� (��������) � ������������ ����
    		* $apartments_in_high_rise_building int
    	*/
		const apartments_in_high_rise_building = 1;
		 
		/**
    		* ����������� � ����������� ����� ���������, ��������
    		* $apartments_in_low_rise_housing_complex int
    	*/
		const apartments_in_low_rise_housing_complex = 2;

		/**
    		* ��������������� ��� (�������, �����)
    		* $detached_house int
    	*/
		const detached_house = 3;
				
		/**
    		* ������� ������������ (��������, �����, �������)
    		* $elite_real_estate int
    	*/
		const elite_real_estate = 4;		
			
		/**
    		* ����, ��������
    		* $cafe_restoran int
    	*/
		const cafe_restoran = 5;		
		
		/**
    		* �������
    		* $store int
    	*/
		const store = 6;	
		
		/**
    		* ����
    		* $office int
    	*/
		const office = 7;

		/**
    		* ������������
    		* $production int
    	*/
		const production = 8;		
		
		/**
    		* �����, �������������
    		* $farm_farmland int
    	*/
		const farm_farmland = 9;	
		
		/**
    		* ��������� �������
    		* $land int
    	*/
		const land = 10;		
			
		/**
    		* �����, ���������
    		* $otel int
    	*/
		const otel = 12;	
		
		/**
    		* ���� ������������ ������������
    		* $other_commercial_real int
    	*/
		const other_commercial_real = 13;	

		/**
    		* ��������
    		* $penthouse int
    	*/
		const penthouse = 14;			
		
		static public $maps = array( VF_ObjectId::apartments_in_high_rise_building => 110,
									 VF_ObjectId::apartments_in_low_rise_housing_complex => 110,	
									 VF_ObjectId::detached_house => 110,
									 VF_ObjectId::elite_real_estate => 110,
									 VF_ObjectId::cafe_restoran => 209,
									 VF_ObjectId::store => 100,
									 VF_ObjectId::office => 33,
									 VF_ObjectId::production => 135,
									 VF_ObjectId::farm_farmland => 103,
									 VF_ObjectId::land => 54,
									 VF_ObjectId::otel => 219,
									 VF_ObjectId::other_commercial_real => 281
									 );
		
		
	}
	
    class VF implements SellAdapter {
    	
    	public $id;
    	public $name;
    	public $object_id;
    	//public $country_id;
    	//public $district;
    	//public $type_id;
    	public $currency_id;
    	public $price;
    	public $description;
    	//public $images = array();
    	public $phone;
    	public $contact;
    	public $offer;
    	
    	private $array_VF = array();
    	
    	function __construct() {
    		
    	}
    	
    	public function Result() {
    		
    		return $this->array_VF;
    		
    	}
    	
    	private function GetMargeCategory() {
    		
    		if(isset(VF_ObjectId::$maps[$this->object_id])) {
    			return VF_ObjectId::$maps[$this->object_id];
    		}
    		
    		return 0;
    	}
    	
    	private function GetMargeCity() {
    		
    		if($this->country_id == 283) {
    			return 3108;
    		}
    		return 0;
    	}
    	
    	private function GetMargeCurrency() {
    		
    		if(strtoupper($this->currency_id) == "USD") {
    			return "usd";
    		} else if (strtoupper($this->currency_id) == "EUR") {
    			return "eur";
    		} else if (strtoupper($this->currency_id) == "RUR") {
    			return "rub";
    		} else {
    			return "eur";
    		}
    		
    	}
    	
    	
    	public function ConvertData() {

    		$data = array();
    		
			$data["FML"] = $this->contact; 
			$data["EmailID"] = "info@vashafirma.ru";
			$data["PhoneID"] = $this->phone;
			$data["NameBizID"] = $this->name;
			$data["BizFormID"] = "";
			$data["CityID"] = $this->GetMargeCity();
			$data["SiteID"] = $this->district;
			$data["CostID"] = $this->price;
			$data["cCostID"] = "rub";//$this->GetMargeCurrency();
			$data["txtCostID"] = $this->price;
			$data["txtProfitPerMonthID"] = "";
			$data["ProfitPerMonthID"] = "";
			$data["cProfitPerMonthID"] = "";
			$data["cMaxProfitPerMonthID"] = "";
			$data["TimeInvestID"] = "";
			$data["ListObjectID"] = ""; 
			$data["ContactID"] = "";
			$data["MatPropertyID"] = "";
			$data["txtMonthAveTurnID"] = "";
			$data["MonthAveTurnID"] = "";
			$data["cMonthAveTurnID"] = "";
			$data["cMaxMonthAveTurnID"] = "";
			$data["txtMonthExpemseID"] = "";
			$data["MonthExpemseID"] = "";
			$data["cMonthExpemseID"] = "";
			$data["txtSumDebtsID"] = "";
			$data["SumDebtsID"] = "";
			$data["cSumDebtsID"] = "";
			$data["txtWageFundID"] = "";
			$data["WageFundID"] = "";
			$data["cWageFundID"] = "";
			$data["ObligationID"] = "";
			$data["CountEmplID"] = "";
			$data["CountManEmplID"] = "";
			$data["TermBizID"] = "";
			$data["ShortDetailsID"] = $this->description;
			$data["DetailsID"] = $this->description; 
			$data["ImgID"] = "";
			$data["FaxID"] = ""; 
			$data["ShareSaleID"] = "";
			$data["ReasonSellBizID"] = "";
			$data["TarifID"] = ""; 
			$data["StatusID"] = "";
			$data["DataCreate"] = "";
			$data["DateStart"] = "";
			$data["TimeActive"] = "";
			$data["CategoryID"] = "";
			$data["SubCategoryID"] = $this->GetMargeCategory();
			$data["ConstGUID"] = "";
			$data["ip"] = "";
			$data["defaultUserCountry"] = "";
			$data["login"] = "";
			$data["password"] = "";
			$data["newspaper"] = "";
			$data["helpbroker"] = "";
			$data["youtubeURL"] = "";    		
    		
    		return $data;
    	}
    	
    	public function CallData() {
    		
			$xml_reader = new XMLReader();   
			$xml_reader->open("http://www.bizzona.ru/doc/adverts.xml");
    		$xml_reader->setParserProperty(XMLReader::VALIDATE, false);	
    		
    		
			while ($xml_reader->read()) {
				
				if ($xml_reader->name == "ads") {

					while ($xml_reader->read()){
						
						if ($xml_reader->name == "ad" && $xml_reader->nodeType != XMLREADER::END_ELEMENT ) {
					       $source_id = $xml_reader->getAttribute("source-id");
							//echo $xml_reader->getAttribute("source-id"). " <hr>  ";
							
							while ($xml_reader->read()){
								
								 //echo $xml_reader->."\n";
								 if ($xml_reader->name == "items" &&  $xml_reader->nodeType != XMLREADER::END_ELEMENT){
								 	
								 	$arr = Array();
								 	
								 	
								 	while ($xml_reader->read()){

								 		if ($xml_reader->name == "price" &&  $xml_reader->nodeType != XMLREADER::END_ELEMENT){
								 			
								 			//echo $xml_reader->getAttribute("currency")." = > ";
								 			$xml_reader->read();
								 			echo $xml_reader->value."<br>";
								 			$arr["price"] = $xml_reader->value;
								 		}
								 		
								 		if ($xml_reader->name == "item" &&  $xml_reader->nodeType != XMLREADER::END_ELEMENT){
								 			
								 			
								 			 
								 			if($xml_reader->getAttribute("name") == "offer") {
								 				//echo $xml_reader->getAttribute("name")." = > ";
								 	            $xml_reader->read();
								 	            //echo $xml_reader->value."<br>";
								                //break;
								                $arr["offer"] = $xml_reader->value;
								 				
								 			}
								 			if($xml_reader->getAttribute("name") == "title") {
								 				//echo $xml_reader->getAttribute("name")." = > ";
								 	            $xml_reader->read();
								 	            $_vf->name = $xml_reader->value;
								 	            //echo $xml_reader->value."<br>";
								                //break;
								                $arr["title"] = $xml_reader->value;
								 				
								 			}								 			
								 			if($xml_reader->getAttribute("name") == "phone") {
								 				//echo $xml_reader->getAttribute("name")." = > ";
								 	            $xml_reader->read();
								 	            //echo $xml_reader->value."<br>";
								                //break;								 	
								                $arr["phone"] = $xml_reader->value;			
								 			}	
								 			if($xml_reader->getAttribute("name") == "contact") {
								 				//echo $xml_reader->getAttribute("name")." = > ";
								 	            $xml_reader->read();
								 	            //echo $xml_reader->value."<br>";
								                //break;								 	
								                $arr["contact"] = $xml_reader->value;			
								 			}
								 			if($xml_reader->getAttribute("name") == "description") {
								 				//echo $xml_reader->getAttribute("name")." = > ";
								 	            $xml_reader->read();
								 	            //echo $xml_reader->value."<br>";
								                //break;
								                $arr["description"] = $xml_reader->value;			
								 			}
								 			if($xml_reader->getAttribute("name") == "source-id") {
								 				//echo $xml_reader->getAttribute("name")." = > ";
								 	            $xml_reader->read();
								 	            //echo $xml_reader->value."<br>";
								                //break;
								                $arr["object_id"] = $xml_reader->value;			
								 			}								 		
								 			
								 			if(strlen($xml_reader->getAttribute("currency")) >= 0) {
								 				//echo $xml_reader->getAttribute("name")." = > ";
								 	            $xml_reader->read();
								 	            //echo $xml_reader->value."<br>";
								                //break;
								                $arr["price"] = $xml_reader->value;			
								                $arr["currency_id"] = strtolower($xml_reader->getAttribute("currency"));// "usd";			
								 			}								 			
								 				
								 		}
								 		
								 		
								 		if ($xml_reader->name == "items" &&  $xml_reader->nodeType == XMLREADER::END_ELEMENT){
								 			$this->ReadDataCallBack($arr);
								 		}
								 		
								 	}
								 	
								 }
							}
						}
					}
		
				}
				
			}
			
			$xml_reader->close();
    		
    	}
    	
		private function ReadDataCallBack($arr) {
 		
			$_VF = new VF(); 
			$_VF->name = (isset($arr["title"]) ? iconv('UTF-8','windows-1251', $arr["title"]." (vasha firma) ") : "");
			$_VF->offer = (isset($arr["offer"]) ? iconv('UTF-8','windows-1251', $arr["offer"]) : "");
			$_VF->description = (isset($arr["description"]) ? iconv('UTF-8','windows-1251', $arr["description"]) : "");
			$_VF->contact = (isset($arr["contact"]) ? iconv('UTF-8','windows-1251', $arr["contact"]) : "");
			$_VF->phone = (isset($arr["phone"]) ? $arr["phone"] : "");
			$_VF->id = (isset($arr["id"]) ? $arr["id"] : "");
			$_VF->price = (isset($arr["price"]) ? $arr["price"] : "");
			$_VF->object_id = (isset($arr["object_id"]) ? $arr["object_id"] : ""); 
			$_VF->currency_id = (isset($arr["currency_id"]) ? $arr["currency_id"] : ""); 
			$_VF->id = $_VF->object_id;
			
			$_VF->name = ucfirst(strtolower($_VF->name));
			$_VF->description = ucfirst(strtolower($_VF->description));
			
			$_VF->name = str_replace("!!!","!", $_VF->name);
			$_VF->description = str_replace("!!!","!", $_VF->description);
			
			$_VF->name = str_replace(" � "," � ", $_VF->name);
			$_VF->description = str_replace(" � "," � ", $_VF->description);	
			
			$_VF->name = str_replace(" � "," � ", $_VF->name);
			$_VF->description = str_replace(" � "," � ", $_VF->description);
			
			$_VF->name = str_replace(" � "," � ", $_VF->name);
			$_VF->description = str_replace(" � "," � ", $_VF->description);			
			
			$_VF->name = str_replace(" �. "," �. ", $_VF->name);
			$_VF->description = str_replace(" �. "," �. ", $_VF->description);	
			
			$_VF->name = str_replace(" �. "," �. ", $_VF->name);
			$_VF->description = str_replace(" �. "," �. ", $_VF->description);
			
			$_VF->name = str_replace("���","���", $_VF->name);
			$_VF->description = str_replace("���","���", $_VF->description);			
			
			$_VF->name = str_replace(" �� "," �� ", $_VF->name);
			$_VF->description = str_replace(" �� "," �� ", $_VF->description);	
			
			$_VF->name = str_replace(" �� "," �� ", $_VF->name);
			$_VF->description = str_replace(" �� "," �� ", $_VF->description);					
			
			if(trim($_VF->offer) == "������") { 
				$this->array_VF[] = $_VF;
			}
	
		}
    	
    	public function ImportData() {
    		
    		$this->CallData();
    		$result = $this->Result();
    		
    		$sell = new Sell();	
			$mappingobjectbroker = new MappingObjectBroker();	

			while (list($k, $v) = each($result)) {

				//var_dump($v);
				
				if(intval($v->id) <= 0) continue;
				
				$mappingobjectbroker->brokerObjectID = $v->id;
				$mappingobjectbroker->brokerId = 11;
				
				if(!$mappingobjectbroker->IsExistsBrokerObject()) {
					
					echo $mappingobjectbroker->brokerObjectID."<hr>";	
					$mappingobjectbroker->bizzonaObjectId = $sell->Insert($v->ConvertData());
					//$mappingobjectbroker->bizzonaObjectId = 1000000;
					$mappingobjectbroker->brokerId = 11;
					$mappingobjectbroker->brokerDateObject = HelpAdapter::unixToMySQL($v->lastupdate);
					$mappingobjectbroker->ObjectTypeId = 1;
					$mappingobjectbroker->Insert();						
					
				} 
				
			}
 			
    	}
    	
    	public function UpdateData() {
    		
    		$sell = new Sell();    		
    		
    		$this->CallData();
    		$result = $this->Result();
    		
			$mappingobjectbroker = new MappingObjectBroker();
			$mappingobjectbroker->brokerId = 11;
			$mappingobjectbroker->ObjectTypeId = 1;
			$array_object = $mappingobjectbroker->Select();
			
			while (list($k, $v) = each($result)) {
				
				while (list($k1, $v1) = each($array_object)) {
					
					if($v1->brokerObjectID == $v->id) {
						$v1_date = strtotime($v1->brokerDateObject);
						$v_date = strtotime(HelpAdapter::unixToMySQL($v->lastupdate));
						
						if($v1_date < $v_date) {

							$data = $v->ConvertData();
							$data["ID"] = $v1->bizzonaObjectId;
							$sell->TestUpdate($data);
							
							$mappingobjectbroker->brokerDateObject = HelpAdapter::unixToMySQL($v->lastupdate);
							$mappingobjectbroker->brokerObjectID = $v1->brokerObjectID;
							$mappingobjectbroker->UpdateBrokerDateObject(); 
						}

						break;
					}
				}
				reset($array_object);
			}
    	}
    	
    	public function DeleteData() {
    		
    		$sell = new Sell();
    		$this->CallData();
    		$result = $this->Result();    		
    		
			$mappingobjectbroker = new MappingObjectBroker();
			$mappingobjectbroker->brokerId = 11;
			$mappingobjectbroker->ObjectTypeId = 1;
			$array_object = $mappingobjectbroker->Select(); 
			
			while (list($k, $v) = each($array_object)) {
				$status = false;	
				while (list($k1, $v1) = each($result)) {
					if($v->brokerObjectID == $v1->id) {
						$status = true;
						
					}
				}
				reset($result);
				
				if(!$status) {
					echo "not found ... <br>";
					$data_update = array();
					$data["StatusID"] = 12;
					$data["ID"] = $v->bizzonaObjectId;
					$sell->TestUpdateStatusID();
				}
				
			}
    		//echo "ok";
    	}
    	
    }
	

?>