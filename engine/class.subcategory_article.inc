<?php
	include_once("class.db.inc");  
    
	class subcategory_article {
		var $db = "";
		var $Id = "";
		var $title = "";
		var $shortdescription  = "";
		var $link = "";
		var $subcategoryId = 0;

		function subcategory_article() {
			global $DATABASE;
			$this->InitDB();
		}

		function InitDB() {
			global $DATABASE;
			$this->db = new DB($DATABASE["HOSTNAME"], $DATABASE["DATABASENAME"], $DATABASE["DATABASEUSER"], $DATABASE["DATABASEPASSWORD"]);
		}

		function Select() {
			$sql = "select * from subcategory_article where 1 and subcategoryId=".$this->subcategoryId." order by Id desc; ";
			$result = $this->db->Select($sql);
			$array = Array();
			while ($row = mysql_fetch_object($result)) {
				$obj = new stdclass();
				$obj->Id = $row->Id;
				$obj->title = $row->title;
				$obj->shortdescription = $row->shortdescription;
				$obj->link = $row->link;
				$array[] = $obj;
			}
			return $array;
		}
     
		
		function Close() {
			$this->db->Close();
		}
		
	}
?>
