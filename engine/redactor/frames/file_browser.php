<?php  include "../control/files.php"; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>
  <title>Hello!</title>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <link rel="stylesheet" href="../css/file_browser.css" type="text/css" />
<script src="../js/lib/prototype.js" type="text/javascript"></script>
<script type="text/javascript">

window.onload = function()
{
	id = window.frameElement.id;
	id = id.replace(/redactor_file_browser_/g, '');
	$('nid').value = id;
	$('page_id').value = window.top.Redactor.options.page_id;
}

var last_element = null;

function insert_file()
{
	var alt = $('alt').value;
  	var nid = $('nid').value;
	var align = $('insert_align').value;
	var margin = $('margin').value;

	var mode = $('insert_mode').value;

	var image = $('hfile').value;
	if (mode == 'mini') image = $('sfile').value;


	if (mode == 'link') window.top.Redactor.insert_file_link(nid,image, alt);
	else
	{
		window.top.Redactor.insert_image_at_cursor(nid, image, alt, align, margin);
	}
   	Element.hide('prepare');

}
function insert_link(link, alt)
{
	var nid = $('nid').value
	window.top.Redactor.insert_file_link(nid,link, alt);
}
function goto_href(val,ind)
{
	self.location.href = '?' + ind + '='+ val;
}
function prepare(file,sfile,alt,id)
{
	if (last_element != null) $('a_' + last_element).removeClassName('active');
	last_element = id;
	$('a_' + id).addClassName('active');

	Element.show('prepare');
	var file = file;

	$('hfile').value = file;
	$('sfile').value = sfile;
	$('alt').value = alt;
	$('id').value = id;
}

function test()
{
//	alert(window.self.document.getElementById('file').value);// = 1;
}
</script>
</head>

<body>
<input type="hidden" name="nid" id="nid" value="0" />


<div style="padding: 10px;">

<ul id="tab_group_one" class="subsection_tabs">
    <li><a href="#one">Загрузка</a></li>
    <li><a href="#two">Обзор</a></li>
</ul>



<div id="one">
<form action="" method="post" enctype="multipart/form-data">
<div class="mini">Файл</div>
<div style="margin-top:3px; margin-bottom: 5px;"><input name="file" type="file" size="60" /></div>
<div class="mini">Название</div>
<div style="margin-top:3px;"><input name="file_name" size="40" /></div>

<div class="mini" style="margin-top:10px;">Папка</div>
<div style="margin-top:3px;" class="mini">
<select name="folder_id">
<option value="0">нет
<?php
foreach  ($folders as $k => $v)
{
   	echo '<option value="'.$k.'">'.$v;
}
?>
</select>
 &nbsp;или новая <input name="folder_name" size="30" /></div>
<input type="hidden" id="page_id" name="page_id" value="0" />
<div style="margin-top:15px;"><input type="submit" name="upload" value="Загрузить" /></div>

</form>
</div>


<div id="two">

<table class="selectors">
<tr>
<td class="left">
<div id="prepare" style="display: none;">
<form action="">
<select id="insert_align">
<option value="0">обтекание
<option value="left">слева
<option value="right">справа
</select>
&rarr;
<input type="text" id="margin" name="margin" value="0" size="1" />
<b>px</b>&nbsp;&nbsp;

<select id="insert_mode">
<option value="full">изображение
<option value="mini">мини-изображение
<option value="link">ссылка
</select>
<input type="hidden" id="hfile" name="hfile" value="" />
<input type="hidden" id="sfile" name="sfile" value="" />
<input type="hidden" id="alt" name="alt" value="" />
<input type="hidden" name="id" id="id" value="" />
<input type="hidden" name="mode" value="img" />

<input type="button" name="insert" onclick="insert_file(); return false;" value="Вставить" />&nbsp;&nbsp;&nbsp;
<input type="submit" name="delete" value="Удалить" /></form>
</div>
</td>
<td class="right">

<select name="mode" onchange="goto_href(this.value,'mode')">
<?php
foreach  ($modes as $k => $v)
{
	echo '<option value="'.$k.'" '.$v['selected'].'>'.$v['name'];
}
?>
</select>


<select name="folder_id" onchange="goto_href(this.value + '&view','folder')">
<option value="0">все папки
<?php
foreach  ($folders as $k => $v)
{
	if ($k != 0)
	{
		echo '<option value="'.$k.'"';
		if (isset($_GET['folder'])) if ($_GET['folder'] == $k) echo ' selected';
		echo '>'.$v;
	}
}
?>
</select>

</td>
</tr>
</table>

<?php if ($mode == 'img'): ?>
<div class="image_box">
<?php
foreach  ($files as $k => $v)
{
?>
	<div class="image"><?php echo $v['name']; ?></div>

<?php
}
?>
</div>
<?php elseif ($mode == 'file'): ?>
<table class="rulers">
<?php
foreach  ($files as $k => $v)
{
?>
<tr class="<?php echo $v['class']; ?>">
<td width="100%"><?php echo $v['name']; ?></td>
<td class="mini"><a href="<?php echo $v['href']; ?>">скачать</a></td>
<td><a href="?delete&mode=file&id=<?php echo $v['id']; ?>"><img src="../ico/del.gif" width="16" height="16" alt="" /></a></td>
</tr>
<?php
}
?>
</table>
<?php endif; ?>
</div>
<script>

    tabs = new Control.Tabs('tab_group_one');
	tabs.setActiveTab('<?php echo $display ?>');
</script>
</div>
</body>

</html>
