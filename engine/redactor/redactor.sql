CREATE TABLE `content` (
  `content_id` int(11) NOT NULL auto_increment,
  `page_id` int(11) NOT NULL default '0',
  `content_date` datetime NOT NULL default '0000-00-00 00:00:00',
  `content` longtext NOT NULL,
  PRIMARY KEY  (`content_id`)
) CHARSET=utf8;


CREATE TABLE `files` (
  `file_id` int(11) NOT NULL auto_increment,
  `folder_id` int(11) NOT NULL default '0',
  `file_name` tinytext NOT NULL,
  `file_type` enum('img','file') NOT NULL default 'img',
  `file_date` datetime NOT NULL default '0000-00-00 00:00:00',
  `file_alt` tinytext NOT NULL,
  `file_width` int(11) NOT NULL default '0',
  `file_height` int(11) NOT NULL default '0',
  `file_size` int(32) NOT NULL default '0',
  PRIMARY KEY  (`file_id`)
) CHARSET=utf8;


CREATE TABLE `files_folders` (
  `folder_id` int(11) NOT NULL auto_increment,
  `folder_name` tinytext NOT NULL,
  `folder_count` int(11) NOT NULL default '0',
  PRIMARY KEY  (`folder_id`)
) CHARSET=utf8;