/*
	Redactor v2.0

	Copyryght 2007

	Made in Imperavi, All rights reserved,

	http://www.imperavi.ru

	Code is released under a GNU General Public Licens http://www.opensource.org/licenses/gpl-license.php
*/
var Redactor = {
  	browser: {
	    IE:     !!(window.attachEvent && !window.opera),
    	Opera:  !!window.opera,
	    WebKit: navigator.userAgent.indexOf('AppleWebKit/') > -1,
    	Gecko:  navigator.userAgent.indexOf('Gecko') > -1 && navigator.userAgent.indexOf('KHTML') == -1
	  },

	init: function(className,options)
	{
		// get array textareas of className
		if (className == undefined) className = 'redactor';
		var paras = $A(document.getElementsByClassName(className));

		Redactor.textareas = new Array();

		paras.each(
			function(s, index)
			{
				Redactor.textareas[index] = new Array(s.id, s.name, s.style.width, s.style.height, 'redactor_container_' + index, 'redactor_area_' + index, 'redactor_real_' + index, 'rte_' + index, $(s.id).value, 'redactor_toolbar_' + index);
			}
		);


		// Options
		Redactor.options = new Object.extend(
		{
			src: './frames/blank.html',
			fullscreen: 'off',
			number_lines: true,
			insert_image_mode: false,
			auto_save: false,
			save_period: 1, //minutes
			save_url: 'http://www.bizzona.ru/engine/redactor/control/save.php',
			path: 'http://www.bizzona.ru/engine/redactor/',
			resize: true,
			page_id: 0
        }, options || {});

		Redactor.options.save_period = parseInt(Redactor.options.save_period)*1000*60

		Redactor.visual = new Array();
		Redactor.insert_image_mode = new Array();

		Redactor.textareas.each(
			function(s,index)
			{
				Redactor.create_containers(index);

				Event.observe($('rte_' + index), 'load',  function() { Redactor.editor_replace(index); });

				Redactor.visual[index] = 'on';
				Redactor.insert_image_mode[index] = false;

				Redactor.toolbar(index);
			}
		);

		// inserts
		Redactor.inserts_mode = false;
		Redactor.inserts_b = true;
	},
	get_info: function(nid, prop)
	{
		if (prop == 'id') return Redactor.textareas[nid][0];
		if (prop == 'name') return Redactor.textareas[nid][1];
		if (prop == 'width') return Redactor.textareas[nid][2];
		if (prop == 'height') return Redactor.textareas[nid][3];
		if (prop == 'container') return Redactor.textareas[nid][4];
		if (prop == 'area') return Redactor.textareas[nid][5];
		if (prop == 'real') return Redactor.textareas[nid][6];
		if (prop == 'frame') return Redactor.textareas[nid][7];
		if (prop == 'html') return Redactor.textareas[nid][8];
		if (prop == 'toolbar') return Redactor.textareas[nid][9];
	},
	create_containers: function(nid)
	{

		// create container
		Redactor.rte_container = document.createElement('div');
		Redactor.rte_container.id = Redactor.get_info(nid,'container');
		Redactor.rte_container.className = 'redactor_container';
		Redactor.rte_container.style.width = Redactor.get_info(nid,'width');

		// create toolbar
		Redactor.rte_toolbar = document.createElement('ul');
		Redactor.rte_toolbar.id = Redactor.get_info(nid,'toolbar');
		Redactor.rte_toolbar.className = 'redactor_buttons';
		Redactor.rte_toolbar.style.width = Redactor.get_info(nid,'width');

		Redactor.rte_clear = document.createElement('div');
		Redactor.rte_clear.className = 'clear';

		// version
		Redactor.rte_version = document.createElement('div');
		Redactor.rte_version.id = 'redactor_versions_' + nid;
		Redactor.rte_version.className = 'redactor_versions';
		Redactor.rte_version.style.display = 'none';
		Redactor.rte_version.style.width = Redactor.get_info(nid,'width');

		// for inserts
		Redactor.rte_inserts = document.createElement('div');
		Redactor.rte_inserts.id = 'inserts_' + nid;
		Redactor.rte_inserts.className = 'redactor_inserts';
		Redactor.rte_inserts.style.display = 'none';
		Redactor.rte_inserts.style.width = Redactor.get_info(nid,'width');

		// create file browser
		Redactor.rte_browser_box = document.createElement('div');
		Redactor.rte_browser_box.id = 'redactor_file_browser_box_' + nid;
		Redactor.rte_browser_box.style.display = 'none';

		 // create textarea
		Redactor.rte_textarea = document.createElement('textarea');
	    Redactor.rte_textarea.id = Redactor.get_info(nid,'area');
		Redactor.rte_textarea.style.display = 'none';
		Redactor.rte_textarea.style.width = Redactor.get_info(nid,'width');
		Redactor.rte_textarea.style.height = Redactor.get_info(nid,'height');


		// create real textarea
		Redactor.rte_real_textarea = document.createElement('textarea');
		Redactor.rte_real_textarea.id = Redactor.get_info(nid,'real');
		Redactor.rte_real_textarea.name = Redactor.get_info(nid,'name');
	    Redactor.rte_real_textarea.style.display = 'none';

		// create input debug field
		Redactor.rte_debug = document.createElement('input');
		Redactor.rte_debug.id = 'redactor_debug_' + nid;
		Redactor.rte_debug.type = 'hidden';
		Redactor.rte_debug.value = nid;

		// create iframe
		Redactor.rteElt = document.createElement('iframe');
		Redactor.rteElt.id = Redactor.get_info(nid,'frame');
		Redactor.rteElt.width = Redactor.get_info(nid,'width');
		Redactor.rteElt.height = Redactor.get_info(nid,'height');
		Redactor.rteElt.src = Redactor.options.path + Redactor.options.src;
		Redactor.rteElt.frameBorder = 0;

		Redactor.replace_containers(nid);


		if (Redactor.options.resize)
		{
			// create bottom
			Redactor.rte_bottom = document.createElement('div');
			Redactor.rte_bottom.className = 'redactor_bottom';

			// create resize
			Redactor.rte_resize = document.createElement('div');
			Redactor.rte_resize.className = 'redactor_resize';
			Redactor.rte_resize.id = 'redactor_resize_' + nid;
			Redactor.rte_resize.style.width = Redactor.get_info(nid,'width');

			Redactor.rte_resize.onmousedown =  function() { Redactor.init_resize(this.id, nid); };
			Redactor.rte_resize.onmouseup = function() { Redactor.stopResize };

			Redactor.rte_resize_btn = document.createElement('div');

		   	Redactor.rte_bottom.appendChild(Redactor.rte_resize);
			Redactor.rte_resize.appendChild(Redactor.rte_resize_btn);
			Redactor.rte_container.appendChild(Redactor.rte_bottom);
		}


	},
	replace_containers: function(nid)
	{
		Redactor.rte_container.appendChild(Redactor.rte_toolbar);

		Redactor.rte_container.appendChild(Redactor.rte_version);
		Redactor.rte_container.appendChild(Redactor.rte_inserts);
		Redactor.rte_container.appendChild(Redactor.rte_browser_box);
	 	Redactor.rte_container.appendChild(Redactor.rteElt);

   		Redactor.rte_container.appendChild(Redactor.rte_textarea);

		Redactor.rte_container.appendChild(Redactor.rte_real_textarea);
		Redactor.rte_container.appendChild(Redactor.rte_debug);

		var element = $(Redactor.get_info(nid,'id'));

		element.parentNode.replaceChild(Redactor.rte_container, element);
	},
    onunload: function(nid)
	{
		$(Redactor.get_info(nid,'frame')).designMode = 'off';
	},
	editor_replace: function(nid)
	{
		element = $('rte_' + nid);
		element.onload = null;
		element.contentWindow.document.designMode = 'on';

		setTimeout(function()
		{
			Redactor.run(nid);
		}, 500);

		if (Redactor.options.auto_save)
		{
			setInterval(function()
			{
			   Redactor.save(nid,false);
			}, Redactor.options.save_period);
		}
	},
	get_current_editor: function()
	{

	},
	run: function(nid)
	{
		var html = Redactor.get_info(nid,'html');
	  	Redactor.set_html(nid, html);
		Redactor.set_real_html();

		if (Redactor.browser.Gecko)
		{
		   	$('rte_' + nid).contentWindow.document.designMode = 'off';
		   	$('rte_' + nid).contentWindow.document.designMode = 'on';
		}


		Event.observe($('redactor_area_' + nid), 'keyup', function () { Redactor.set_area_html(); });
		Event.observe($('redactor_area_' + nid), 'click', function () { Redactor.set_area_html(); });

		Event.observe($('rte_' + nid).contentWindow.document, 'keyup', function () { Redactor.set_real_html(); });
		Event.observe($('rte_' + nid).contentWindow.document, 'click', function() { Redactor.elements(nid);	 Redactor.set_real_html();  });


		$('rte_0').contentWindow.focus();

	},
	save: function(nid, version)
	{
		var new_version = 0;
		if (version) new_version = 1;
		var html = Redactor.get_html(nid);
		var opt = {
			postBody: 'mode=save&page_id=' + nid + '&html=' + html + '&new=' + new_version,
			method: 'post'
		}
		new Ajax.Request(Redactor.options.save_url, opt);
	},
	table_controls: function(nid)
	{
		var html = '<table class="redactor_style"><tr><td>'
		+ '<a href="javascript:table_func(' + nid + ',\'add_tr_before\');" title="Добавить строку до"><img src="' + Redactor.options.path + 'ico/row_before.gif" alt="" width="19" height="16" /></a>'
		+ '<a href="javascript:table_func(' + nid + ',\'add_tr_after\');" title="Добавить строку после"><img src="' + Redactor.options.path + 'ico/row_after.gif" alt="" width="19" height="16" /></a>'
		+ '<a href="javascript:table_func(' + nid + ',\'delete_row\');" title="Удалить строку"><img src="' + Redactor.options.path + 'ico/delete_row.gif" alt="" width="19" height="16" /></a>'
		+ '<a href="javascript:table_func(' + nid + ',\'insert_col_before\');" title="Добавить колонку до"><img src="' + Redactor.options.path + 'ico/col_before.gif" alt="" width="19" height="16" /></a>'
		+ '<a href="javascript:table_func(' + nid + ',\'insert_col_after\');" title="Добавить колонку после"><img src="' + Redactor.options.path + 'ico/col_after.gif" alt="" width="19" height="16" /></a>'
		+ '<a href="javascript:table_func(' + nid + ',\'delete_col\');" title="Удалить колонку"><img src="' + Redactor.options.path + 'ico/delete_col.gif" alt="" width="19" height="16" /></a>'
		+ '<a href="javascript:table_func(' + nid + ',\'table_delete\');" title="Удалить таблицу"><img src="' + Redactor.options.path + 'ico/delete_table.gif" alt="" width="19" height="16" /></a>'
		+ '</td></tr></table>';
		return html;
	},
	elements: function(nid)
	{
		var focusElm = getFocusElement(nid);

		if (focusElm == null) return true;
		parent_name = focusElm.parentNode.nodeName;
		//alert(focusElm.nodeName);
		if (parent_name == 'TR' || parent_name == 'TD')
		{
	   		$('inserts_' + nid).innerHTML = Redactor.table_controls(nid);

			if (!Redactor.inserts_mode) Element.show('inserts_' + nid);
			Redactor.inserts_mode = true;

			Redactor.tableElm = getParentElement(nid,focusElm, "table");
		  	Redactor.grid = getTableGrid(Redactor.tableElm);

			Redactor.trElm = getParentElement(nid,focusElm, "tr");
			Redactor.tdElm = getParentElement(nid,focusElm, "td,th");

			if (Redactor.grid != null) Redactor.cpos = getCellPos(Redactor.grid, Redactor.tdElm);
			else Redactor.cpos = null;
			$('rte_toolbar_buttons_bold_' + nid).removeClassName('select');
			$('rte_toolbar_buttons_italic_' + nid).removeClassName('select');
		}
        else if (parent_name == 'B' || focusElm.nodeName == 'B')
		{
		 	$('rte_toolbar_buttons_bold_' + nid).addClassName('select');
		}
        else if (parent_name == 'I' || focusElm.nodeName == 'I')
		{
		 	$('rte_toolbar_buttons_italic_' + nid).addClassName('select');
		}
		else
		{
			$('rte_toolbar_buttons_bold_' + nid).removeClassName('select');
			$('rte_toolbar_buttons_italic_' + nid).removeClassName('select');
			if (Redactor.inserts_mode && Redactor.inserts_b)
			{
				Element.hide('inserts_' + nid);
				Redactor.inserts_mode = false;
			}
		}
	},
	set_html: function(nid, html)
	{
		$('rte_' + nid).contentWindow.document.body.innerHTML = html;
	},
    get_html: function(nid)
	{
		var html = $('rte_' + nid).contentWindow.document.body.innerHTML;
		html = format_html(html);
		return html;
	},
	set_area_html: function()
	{
		Redactor.textareas.each(
			function(s,index)
			{
				var text = $('redactor_area_' + index).value;
				$('redactor_real_' + index).value = text;
        	}
		);
	},
   	set_real_html: function()
	{
    	Redactor.textareas.each(
			function(s,index)
			{
				var text = Redactor.get_html(index);

				if(!Redactor.browser.Gecko) text = cleanupHTML_ie(text);

				text = text.replace(/\<br\>/gi,"<br />");
				if (text == '<br />') text = '';
				$('redactor_real_' + index).value = text;
        	}
		);
	},
	set_version: function(nid,id)
	{

		$('redactor_versions_' + nid).innerHTML = '<div class="redactor_loader"><img src="i/loading.gif" /></div>';

		new Ajax.Request(Redactor.options.path + 'control/versions.php?get&page_id=' + nid + '&id=' + id, {
		onSuccess: function(t) {
			Redactor.toggle_version(nid);
		   Redactor.set_html(nid, t.responseText);
		}
		});
	},
	delete_version: function(nid, id)
	{
		new Ajax.Request(Redactor.options.path + 'control/versions.php?delete&page_id=' + nid + '&id=' + id);
		$('version_row_' + nid + '_' + id).remove();
	},
	toggle_version: function(nid)
	{
	    $('rte_toolbar_buttons_versions_' + nid).toggleClassName('select');

		if ($('rte_' + nid).getStyle('display') == 'none')
		{
			$('rte_' + nid).show();
			if (Redactor.options.resize) $('redactor_resize_' + nid).show();
			$('redactor_versions_' + nid).hide();

		}
		else
		{
			$('redactor_area_' + nid).hide();
			$('inserts_' + nid).hide();
			$('rte_' + nid).hide();
			if (Redactor.options.resize) $('redactor_resize_' + nid).hide();

			Redactor.save(nid,true);

			$('redactor_versions_' + nid).innerHTML = '<div class="redactor_loader"><img src="i/loading.gif" /></div>';
			new Ajax.Updater('redactor_versions_' + nid, Redactor.options.path + 'control/versions.php?page_id=' + nid);

			$('redactor_versions_' + nid).show();
		}
	},
	toggle: function(nid)
	{
		$('rte_toolbar_buttons_code_' + nid).toggleClassName('select');

	   	if (Redactor.visual[nid] == 'on')
		{
		   	Redactor.visual[nid] = 'off';
			$('rte_' + nid).hide();

			Redactor.convert_span(nid);
			var text = Redactor.get_html(nid);

		   	if(!Redactor.browser.Gecko) text = cleanupHTML_ie(text);

			Element.show('redactor_area_' + nid);

			if (text == '<br />') text = '';

			$('redactor_area_' + nid).value = text;


			$('redactor_area_' + nid).focus();
	   	}
	   	else if (Redactor.visual[nid] == 'off')
	  	{

			Redactor.visual[nid] = 'on';

			var html = $('redactor_area_' + nid).value;
			//alert(html);

			Element.hide('redactor_area_' + nid);

			Redactor.set_html(nid, html);
			$('rte_' + nid).show();
			$('rte_' + nid).contentWindow.focus();

	   	}
	},
    toolbar: function(nid)
	{
        var buttons = [
		//	['command', 'name',  'title', 'selector'],
			['Redactor.toggle(' + nid + ');' , 'code', 'Код', false],

           	['Redactor.fullscreen(' + nid + ');' , 'fullscreen', 'На весь экран', false],

           	['Redactor.save(' + nid + ', true);' , 'save', 'Сохранить', false],

			['Redactor.clean_up(' + nid + ');' , 'cleanup', 'Очистить код', true],

            ['Redactor.command(false,\'undo\',' + nid + ');' , 'undo', 'Отменить', false],
			['Redactor.command(false,\'redo\','+ nid + ');', 'redo', 'Повторить', true],

			['Redactor.command(\'b\',\'Bold\',' + nid + ');' , 'bold', 'Полужирный', false],
			['Redactor.command(\'i\',\'Italic\',' + nid + ');', 'italic', 'Наклонный', false],
			['Redactor.command(\'sup\',\'superscript\',' + nid + ');' , 'sup', 'Надстрочный', true],

			['Redactor.command(false,\'removeformat\',' + nid + ');' , 'remove_format', 'Удалить формат', true],

			['Redactor.command(false,\'justifyleft\',' + nid + ');', 'align_left', 'Слева', false],
			['Redactor.command(false,\'justifycenter\',' + nid + ');', 'align_center', 'По центру', false],
            ['Redactor.command(false,\'justifyright\',' + nid + ');' ,'align_right', 'Справа', true],

			['Redactor.command(false,\'insertorderedlist\',' + nid + ');' , 'ol', 'Нумерованный список', false],
			['Redactor.command(false,\'insertunorderedlist\',' + nid + ');' , 'ul', 'Ненумерованный список', true],


			['Redactor.command(false,\'inserthorontalrule\', ' + nid + ');' , 'hr', 'Линия', false],
			['Redactor.set_block(\'blockquote\',' + nid + ');' , 'quote', 'Цитата', true],
			['Redactor.inserts(' + nid + ',\'Вставка ссылки\', \'link\');' , 'link', 'Вставить ссылку', false],
			['Redactor.command(false,\'unlink\',' + nid + ');' , 'unlink', 'Удалить ссылку', false],
			['Redactor.inserts(' + nid + ',\'Вставка спец. символа\', \'charmap\');' , 'symbol', 'Вставить спец. символ', false],
			['Redactor.inserts(' + nid + ',\'Вставка таблицы\', \'table\');' , 'table', 'Вставить таблицу', true],
			['Redactor.insert_image(' + nid + ');' , 'image', 'Вставить изображение', true]

		];


		var toolbar_buttons = '';
		for (var i=0; i<buttons.length; i++)
		{
			if (buttons[i][3]) button_class = 'sel';
			else button_class = '';

			if (buttons[i][4]) onclick = buttons[i][4];
			else onclick = '';

			toolbar_buttons += '<li id="rte_toolbar_buttons_' + buttons[i][1] + '_' + nid + '" class="' + button_class + '"><a href="javascript:' + buttons[i][0] + '" onclick="' + onclick + '"><img src="' + Redactor.options.path + 'ico/' + buttons[i][1] + '.gif" title="' + buttons[i][2] + '" width="19" height="16" border="0" alt="" /></a></li>'
		}


		var toolbar_buttons_style =  '<li class="drop" id="rte_toolbar_buttons_style_' + nid + '"><b><a href="javascript:void(null);" onclick="Redactor.inserts(' + nid + ',\'style\')">Стиль</a></b></li>'

		// Version button
		var version_button;
		version_button = '<li class="drop" id="rte_toolbar_buttons_versions_' + nid + '"><b><a href="javascript:Redactor.toggle_version(' + nid + ');">Версии</a></b></li>';



		$(Redactor.get_info(nid,'toolbar')).innerHTML = toolbar_buttons + toolbar_buttons_style;
		$(Redactor.get_info(nid,'toolbar')).innerHTML += version_button;
	},
    clean_up: function(nid)
	{
		if (Redactor.visual[nid] == 'on')
		{
			var html = clean_up(Redactor.get_html(nid));
			Redactor.set_html(nid,html);
		}
		else
		{
			$('redactor_area_' + nid).value = clean_up($('redactor_area_' + nid).value);
		}
		Redactor.set_real_html();
	},
    fullscreen: function(nid)
	{
		$('rte_toolbar_buttons_fullscreen_' + nid).toggleClassName('select');
    	$('redactor_container_' + nid).toggleClassName('redactor_container_fullscreen');

		if (Redactor.visual[nid] == 'off')
		{
			if ($('redactor_container_' + nid).hasClassName('redactor_container_fullscreen')) $('redactor_area_' + nid).style.height = '700px';
			else  $('redactor_area_' + nid).style.height = Redactor.get_info(nid, 'height');
		}
	},
	get_by_id: function(nid)
	{
		var y;
    	Redactor.textareas.each(
			function(s,index)
			{
				if (s[0] == nid)
				{
					y = index;
				}
			}
		);
		return y;

	},
	command: function(vmd,cmd,nid)
	{
		var bool = false;
		var value = null;

		if (Redactor.visual[nid] == 'on')
		{
			var returnValue = Redactor.in_between(cmd,bool,value, nid);
			$('rte_' + nid).contentWindow.focus();
		}
		else
		{
			if (vmd)
			{
				text = Redactor.get_selection_area(nid);
				text = '<' + vmd + '>' + text + '</' + vmd + '>';
				Redactor.replace_selection(nid, text);
				$('redactor_area_' + nid).focus();
			}
		}

		var writestring = '';


	},
	in_between: function(command,bool,value, nid)
	{
		var returnValue = $('rte_' + nid).contentWindow.document.execCommand(command,bool,value);
		if (returnValue) return returnValue;
	},
    set_block: function (block, nid)
	{
		if (Redactor.visual[nid] == 'on')
		{
			var range = Redactor.get_selection_range(nid);
			if (!Redactor.browser.IE)
			{
				$('rte_' + nid).contentDocument.execCommand('formatblock',false,block);
				if ($('rte_' + nid).contentWindow.document.body.lastChild.tagName == "BLOCKQUOTE")
				{
    	    		$('rte_' + nid).contentWindow.document.body.appendChild($('rte_' + nid).contentWindow.document.createElement("BR"));
			    }
			}
			else
			{
				var html = "<" + block + ">" + range.htmlText + "</" + block + ">";
				range.pasteHTML(html);
			}
	       	Redactor.set_real_html();
			Redactor.convert_span(nid);
			$('rte_' + nid).contentWindow.focus();
		}
		else
		{
			if (block && block != 'empty')
			{
				text = Redactor.get_selection_area(nid);
				text = '<' + block + '>' + text + '</' + block + '>';
				Redactor.replace_selection(nid, text);
				$('redactor_area_' + nid).focus();
			}

		}


	},
	insert_at_cursor: function(nid,data)
	{
		if (data || data != null)
		{
			if(Redactor.browser.IE)
			{
				$('rte_' + nid).contentWindow.focus();
				range = Redactor.get_selection_range(nid);
		    	range.pasteHTML(data);
			}
			else
			{
				$('rte_' + nid).contentDocument.execCommand("inserthtml",false,data);
		   		$('rte_' + nid).contentWindow.focus();
			}
			Redactor.set_real_html();
		}
	},
	insert_at_cursor_area: function(nid, myValue)
	{
		myField =  $('redactor_area_' + nid);

		if (document.selection)
		{
	  		myField.focus();
			range = Redactor.get_selection_range(nid);
		    range.pasteHTML(myValue)
		}
		else if (myField.selectionStart || myField.selectionStart == '0')
		{
			var startPos = myField.selectionStart;
			var endPos = myField.selectionEnd;
			myField.value = myField.value.substring(0, startPos)
			+ myValue
			+ myField.value.substring(endPos, myField.value.length);
		}
		else
		{
			myField.value += myValue;
		}
		Redactor.set_real_html();
		$('redactor_area_' + nid).focus();
	},
	inserts: function(nid, title, command)
	{
		if (title == 'style')
		{
			$('inserts_' + nid).innerHTML = Redactor.echo_style(nid);
			title = false;
		    Redactor.button_class('rte_toolbar_buttons_style_' + nid);
		}
		if (title) $('inserts_' + nid).innerHTML = '<div class="inserts_title">' + title + '</div>';

		if (command == 'charmap')
		{
			$('inserts_' + nid).innerHTML = echo_charmap(nid);
			Redactor.button_class('rte_toolbar_buttons_symbol_' + nid);
		}
		if (command == 'link')
		{
			if (Redactor.browser.IE) Redactor.range_t  = Redactor.get_selection_range(nid);
			$('inserts_' + nid).innerHTML = Redactor.echo_insert_link(nid);
			Redactor.button_class('rte_toolbar_buttons_link_' + nid);
		}
		if (command == 'table')
		{
			$('inserts_' + nid).innerHTML = Redactor.echo_table(nid);
			Redactor.button_class('rte_toolbar_buttons_table_' + nid);
		}

		if (Redactor.inserts_mode)
		{
			Element.hide('inserts_' + nid);
			Redactor.inserts_mode = false;
			Redactor.inserts_b = true;
		}
		else
		{
			Element.show('inserts_' + nid);
			Redactor.inserts_mode = true;
			Redactor.inserts_b = false;
		}
	},
	button_class: function(el)
	{
		if ($(el).hasClassName('select')) $(el).removeClassName('select')
		else $(el).addClassName('select')
	},
	echo_style: function(nid)
	{
		var toolbar_buttons_style = '<table class="redactor_style"><tr><td>';
		var buttons_style = [
			['\'empty\'', 'Обычный', ''],
			['\'p\'', '&sect;', ''],
			['\'div\'', 'Слой', ''],
			['\'code\'', 'Код', ''],
			['\'pre\'', 'Pre', ''],
			['\'h1\'', 'H1', '18px'],
			['\'h2\'', 'H2', '16px'],
			['\'h3\'', 'H3', '14px'],
			['\'h4\'', 'H4', '12px'],
			['\'h5\'', 'H5', '']
		];

		for (var i=0; i<buttons_style.length; i++)
		{
			if (buttons_style[i][2]) button_font = 'font-size: ' + buttons_style[i][2] + ';';
			else button_font = '';
			toolbar_buttons_style += '<a href="javascript:void(null);" onclick="Redactor.set_block(' + buttons_style[i][0] + ',\'' + nid + '\'); return false;" style="' + button_font + '">' + buttons_style[i][1] + '</a></li>'
		}
		toolbar_buttons_style += '</td></tr></table>';
		return toolbar_buttons_style
	},
	echo_insert_link: function (nid)
	{
		html = '<table><tr class="row1"><td colspan="2">'
			+ '<input type="radio" name="insert_link" id="insert_link_mode_web" checked /> <label for="insert_link_mode_web">Сайт</label> '
			+ '<input type="radio" name="insert_link" id="insert_link_mode_email" /> <label for="insert_link_mode_email">Эл. почта</label>'
			+ '</td></tr>'
			+ '<tr class="row2"><td>Текст</td><td style="width:100%;"><input name="" id="insert_link_text" size="40" /></td></tr>'
			+ '<tr class="row1"><td>Адрес</td><td style="width:100%;"><input name="" id="insert_link_web" size="40" /></td></tr>'
		  	+ '<tr class="row2"><td>Title</td><td><input name="" id="insert_link_title" size="40"/></td></tr><tr class="row1">'
			+ '<td colspan="2"><input type="button" name="" onclick="Redactor.insert_link(' + nid + ');" value="Вставить" /></td></tr></table>';
		return html;
	},
	insert_link: function(nid)
	{

	    var title = $('insert_link_title').value;
		var html = $('insert_link_web').value;

		if (!html) return true;

		if ($('insert_link_mode_web').checked == true)
		{
			if (Redactor.browser.Gecko)
			{
				if (!$('insert_link_text').value) var data = Redactor.get_selection(nid);
				else var data = $('insert_link_text').value;

				var href = '<a href="' + html + '" title="' + title + '">' + data + '</a>';

				Redactor.insert_at_cursor(nid,href);
				//Redactor.rteElt.contentDocument.execCommand('createlink',false,html);
			}
	   		else
			{
				if (!$('insert_link_text').value)
				{
					var html = Redactor.range_t.htmlText;
					html = html.replace(/\<p\>([\w\W]*?)\<\/p\>/gi, "$1");
				}
				else var html = $('insert_link_text').value;

				html = '<a href="' + $('insert_link_web').value + '" title="' + $('insert_link_title').value + '">' + html + '</a>';
		    	Redactor.range_t.pasteHTML(html);
			}
		}
		else
		{
			if (Redactor.browser.Gecko)
			{
				var data = Redactor.get_selection(nid);
				var href = '<a href="' + html + '" title="' + title + '">' + data + '</a>';
				Redactor.insert_at_cursor(nid,href);
			}
	   		else
			{
				if (!$('insert_link_text').value)
				{
					var html = Redactor.range_t.htmlText;
					html = html.replace(/\<p\>([\w\W]*?)\<\/p\>/gi, "$1");
				}
				else var html = $('insert_link_text').value;

				html = '<a href="mailto:' + $('insert_link_web').value + '" title="' + $('insert_link_title').value + '">' + html + '</a>';
		    	Redactor.range_t.pasteHTML(html);
			}
		}
		$('inserts_' + nid).hide();
		Redactor.set_real_html();
	},
	echo_table: function(nid)
	{
 		html = '<table>'
		+ '<tr class="row1"><td><b>Ширина</b></td><td><input name="" id="insert_table_width"  value="100" size="3" /></td><td width="100%" colspan="2">'
		+ '<select id="insert_table_units"><option value="%">проценты<option value="px">пиксели</select></td></tr>'
		+ '<tr class="row2"><td><b>Строк</b></td><td><input name="" id="insert_table_rows" size="3" value="2" /></td>'
		+ '<td><b>Столбцов</b></td><td width="100%"><input name="" id="insert_table_cell" size="3" value="2" /></td></tr>'
		+ '<tr class="row1"><td colspan="4"><input type="button" name="" onclick="Redactor.insert_table(' + nid + ');" value="Вставить" /></td></tr></table>';
		return html;
	},
	insert_table: function(nid)
	{

		var table_width = $("insert_table_width").value;
		var table_width_units = $("insert_table_units").value;

		var table = '<table style="width: '+ table_width + table_width_units + ';">';

		var rows = $("insert_table_rows").value;
		var cols = $("insert_table_cell").value;

	   //we create the rows and cells
		for(x=0;x<rows;x++)
		{
			table += '<tr>';
			for(y=0;y<cols;y++)
			{
			   table += '<td></td>';
			}
			table += '</tr>';
		}
		table += '</table><br />';

		Redactor.insert_at_cursor(nid,table);
		$('inserts_' + nid).hide();

	},
	test: function(nid)
	{
		text = '<div>' + Redactor.getSelection(nid) + '</div>';

		Redactor.replaceSelection(nid, text);
		//alert(Redactor.getSelection(nid));
	},
	get_selection_area: function(nid){
	   if (Redactor.browser.IE)
			return document.selection.createRange().text;
		else
			return $('redactor_area_' + nid).value.substring($('redactor_area_' + nid).selectionStart,$('redactor_area_' + nid).selectionEnd);
	},
	replace_selection: function(nid,text){
		if (Redactor.browser.IE)
		{
			$('redactor_area_' + nid).focus();
			var old = document.selection.createRange().text;
			var range = document.selection.createRange();
			if(old == '')
				$('redactor_area_' + nid).value += text;
			else{
				range.text = text;
				range -= old.length - text.length;
			}
		}
		else
		{
			var selection_start = $('redactor_area_' + nid).selectionStart;
			$('redactor_area_' + nid).value = $('redactor_area_' + nid).value.substring(0,selection_start) + text + $('redactor_area_' + nid).value.substring($('redactor_area_' + nid).selectionEnd);
			$('redactor_area_' + nid).setSelectionRange(selection_start + text.length,selection_start + text.length);
		}
		$('redactor_area_' + nid).focus();
	},
    get_selection_text: function(nid)
	{
		if (!Redactor.browser.IE) return (Redactor.get_selection(nid));
		else
		{
			var range = Redactor.get_selection_range(nid);
		    return (range.htmlText);
		}
	},
	get_selection: function (nid)
	{
		if ($('rte_' + nid).contentWindow.getSelection) var selection = $('rte_' + nid).contentWindow.getSelection()
		else if($('rte_' + nid).contentWindow.document.selection) selection = $('rte_' + nid).contentWindow.document.selection.createRange()
		return selection;

	},
   	IsDefined: function(obj, field)
	{
		return typeof obj[field] != "undefined";
	},
	get_selection_range: function(nid)
	{

		var selection = Redactor.get_selection(nid);

		if (Redactor.IsDefined(selection, "rangeCount") && Redactor.IsDefined(selection, "getRangeAt"))
		{
			// Gecko
			if (selection.rangeCount == 0)
			{
				return null;
			}
			return selection.getRangeAt(selection.rangeCount - 1).cloneRange();
		}
		else if (Redactor.IsDefined(selection, "createRange"))
		{
			// IE
			var range = selection.createRange();
			if (Redactor.IsDefined(range, "parentElement"))
			{
				// It's a textRange.
				return range;
			}
			else if (Redactor.IsDefined(range, "length"))
			{
			    var node = range(0);
				var bodyNode = WYSEditor.document.body;
				range = bodyNode.createTextRange();
				range.moveToElementText(node);
				return range;
			}
		}

   		if (Redactor.IsDefined(selection, "baseNode") && isSafari)
		{
			var result = {
				baseOffset: selection.baseOffset,
				baseNode: selection.baseNode,
				extentOffset: selection.extentOffset,
				extentNode: selection.extentNode,
				text: ("" + selection)
			};
			return result;
		}
		return selection;
  	},
	insert_image: function(nid)
	{
		if (Redactor.insert_image_mode[nid] == false)
		{
			Redactor.rte_browser = document.createElement('iframe');
			Redactor.rte_browser.className = 'redactor_file_browser';
			Redactor.rte_browser.id = 'redactor_file_browser_' + nid;
			Redactor.rte_browser.style.width = Redactor.get_info(nid, 'width');
			Redactor.rte_browser.style.display = '';
			Redactor.rte_browser.src = Redactor.options.path + 'frames/file_browser.php';

		    var	browser_box = $('redactor_file_browser_box_' + nid);

			browser_box.appendChild(Redactor.rte_browser);
		}

		Redactor.insert_image_mode[nid] = true;
		Redactor.button_class('rte_toolbar_buttons_image_' + nid);

		if($('redactor_file_browser_box_' + nid).style.display == 'none')
		{
			Element.show('redactor_file_browser_box_' + nid);
	        Element.hide('inserts_' + nid);
			Redactor.inserts_mode = false;
			Redactor.inserts_b = true;
		}
		else
		{
			Element.hide('redactor_file_browser_box_' + nid);
	        Element.show('inserts_' + nid);
			Redactor.inserts_mode = true;
			Redactor.inserts_b = false;
		}
	},
	insert_image_at_cursor: function(nid,image, alt, align, margin)
	{
		if (align == '0') align = '';

		if (margin == '0') margin = '';
		else
		{
			if (align == 'left') margin = 'style="margin-right: ' +margin +'px; margin-bottom: ' +margin +'px;"';
			if (align == 'right') margin = 'style="margin-left: ' +margin +'px; margin-bottom: ' + margin +'px;"';
		}

		var data = '<img src="' + image + '" alt="' + alt + '" align="' + align + '" ' + margin + ' />';

		if (Redactor.visual[nid] == 'on') Redactor.insert_at_cursor(nid,data);
		else Redactor.insert_at_cursor_area(nid,data);
	},
	insert_file_link: function(nid,link, alt)
	{
		if (!alt) alt = 'ссылка';
		var data = '<a href="' + link + '">' + alt + '</a>';
        if (Redactor.visual[nid] == 'on') Redactor.insert_at_cursor(nid,data);
		else Redactor.insert_at_cursor_area(nid,data);
	},
	init_resize: function(worker, nid)
	{
		Redactor.worker = $(worker);
		Redactor.worker.style.cursor = 's-resize';

		if (Redactor.visual[nid] == 'on') Redactor.targetElement = $('rte_' + nid);
		else Redactor.targetElement = $('redactor_area_' + nid);

		Redactor.tmp = nid;

		Event.observe(document, 'mousedown',  Redactor.initResize);
	},
	initResize: function(event)
	{
		if (typeof event == 'undefined') event = window.event;

		Redactor.targetElement.startHeight = Redactor.targetElement.clientHeight;
		Redactor.targetElement.startY = event.clientY;

		Event.observe(document, 'mousemove',  Redactor.resize);
		Event.observe(document, 'mouseup',  Redactor.stopResize);

		try
		{
			event.preventDefault();
		}
		catch(e)
		{
		}
	},
	resize: function(event)
	{
		if (typeof event == 'undefined') event = window.event;

		Redactor.targetElement.style.height = event.clientY - Redactor.targetElement.startY + Redactor.targetElement.startHeight  -15 + 'px';
	},
	stopResize: function(event)
	{
		Event.stopObserving(document, 'mousedown', Redactor.initResize)
		Event.stopObserving(document, 'mousemove', Redactor.resize)

		Redactor.worker.style.cursor = 'text';
	},
	convert_span: function(nid)
	{

		var theSPANs = $('rte_' + nid).contentWindow.document.getElementsByTagName("span");

		while(theSPANs.length > 0)
		{
			var theChildren = new Array();
			var theReplacementElement = null;
			var theParentElement = null;

			for (var j = 0; j < theSPANs[0].childNodes.length; j++)
			{
				theChildren.push(theSPANs[0].childNodes[j].cloneNode(true));
			}


			switch (theSPANs[0].getAttribute("style"))
			{
				case "font-weight: bold;":
					theReplacementElement = $('rte_' + nid).contentWindow.document.createElement("strong");
					theParentElement = theReplacementElement;

					break;

				case "font-style: italic;":
					theReplacementElement = $('rte_' + nid).contentWindow.document.createElement("em");
					theParentElement = theReplacementElement;

					break;

				case "font-weight: bold; font-style: italic;":
					theParentElement = $('rte_' + nid).contentWindow.document.createElement("em");
					theReplacementElement = $('rte_' + nid).contentWindow.document.createElement("strong");
					theReplacementElement.appendChild(theParentElement);

					break;

				case "font-style: italic; font-weight: bold;":
					theParentElement = $('rte_' + nid).contentWindow.document.createElement("strong");
					theReplacementElement = $('rte_' + nid).contentWindow.document.createElement("em");
					theReplacementElement.appendChild(theParentElement);

					break;
			default:
					return true;
					//replaceNodeWithChildren(theSPANs[0]);

				break;
			}

			if (theReplacementElement != null)
			{
				for (var j = 0; j < theChildren.length; j++)
				{
					theParentElement.appendChild(theChildren[j]);
				}

				theSPANs[0].parentNode.replaceChild(theReplacementElement, theSPANs[0]);
			}
			theSPANs = $('rte_' + nid).contentWindow.document.getElementsByTagName("span");
		}

	}
}

/*
 Clean up
*/
var entities=new Array('nbsp','iexcl','cent','pound','curren','yen','brvbar','sect','uml','copy','ordf','laquo','not','shy','reg','macr','deg','plusmn','sup2','sup3','acute','micro','para','middot','cedil','sup1','ordm','raquo','frac14','frac12','frac34','iquest','agrave','aacute','acirc','atilde','auml','aring','aelig','ccedil','egrave','eacute','ecirc','euml','igrave','iacute','icirc','iuml','eth','ntilde','ograve','oacute','ocirc','otilde','ouml','times','oslash','ugrave','uacute','ucirc','uuml','yacute','thorn','szlig','agrave','aacute','acirc','atilde','auml','aring','aelig','ccedil','egrave','eacute','ecirc','euml','igrave','iacute','icirc','iuml','eth','ntilde','ograve','oacute','ocirc','otilde','ouml','divide','oslash','ugrave','uacute','ucirc','uuml','yacute','thorn','yuml','quot','amp','lt','gt','oelig','oelig','scaron','scaron','yuml','circ','tilde','ensp','emsp','thinsp','zwnj','zwj','lrm','rlm','ndash','mdash','lsquo','rsquo','sbquo','ldquo','rdquo','bdquo','dagger','dagger','permil','lsaquo','rsaquo','euro','fnof','alpha','beta','gamma','delta','epsilon','zeta','eta','theta','iota','kappa','lambda','mu','nu','xi','omicron','pi','rho','sigma','tau','upsilon','phi','chi','psi','omega','alpha','beta','gamma','delta','epsilon','zeta','eta','theta','iota','kappa','lambda','mu','nu','xi','omicron','pi','rho','sigmaf','sigma','tau','upsilon','phi','chi','psi','omega','thetasym','upsih','piv','bull','hellip','prime','prime','oline','frasl','weierp','image','real','trade','alefsym','larr','uarr','rarr','darr','harr','crarr','larr','uarr','rarr','darr','harr','forall','part','exist','empty','nabla','isin','notin','ni','prod','sum','minus','lowast','radic','prop','infin','ang','and','or','cap','cup','int','there4','sim','cong','asymp','ne','equiv','le','ge','sub','sup','nsub','sube','supe','oplus','otimes','perp','sdot','lceil','rceil','lfloor','rfloor','lang','rang','loz','spades','clubs','hearts','diams')
var numEntities=new Array('160','161','162','163','164','165','166','167','168','169','170','171','172','173','174','175','176','177','178','179','180','181','182','183','184','185','186','187','188','189','190','191','192','193','194','195','196','197','198','199','200','201','202','203','204','205','206','207','208','209','210','211','212','213','214','215','216','217','218','219','220','221','222','223','224','225','226','227','228','229','230','231','232','233','234','235','236','237','238','239','240','241','242','243','244','245','246','247','248','249','250','251','252','253','254','255','34','38','60','62','338','339','352','353','376','710','732','8194','8195','8201','8204','8205','8206','8207','8211','8212','8216','8217','8218','8220','8221','8222','8224','8225','8240','8249','8250','8364','402','913','914','915','916','917','918','919','920','921','922','923','924','925','926','927','928','929','931','932','933','934','935','936','937','945','946','947','948','949','950','951','952','953','954','955','956','957','958','959','960','961','962','963','964','965','966','967','968','969','977','978','982','8226','8230','8242','8243','8254','8260','8472','8465','8476','8482','8501','8592','8593','8594','8595','8596','8629','8656','8657','8658','8659','8660','8704','8706','8707','8709','8711','8712','8713','8715','8719','8721','8722','8727','8730','8733','8734','8736','8743','8744','8745','8746','8747','8756','8764','8773','8776','8800','8801','8804','8805','8834','8835','8836','8838','8839','8853','8855','8869','8901','8968','8969','8970','8971','9001','9002','9674','9824','9827','9829','9830')

function clean_up(text)
{
	text = text.replace(/\<o:p\>/gi,'');
	text = text.replace(/\<\/o:p\>/gi,'');

	text = text.replace(/\&\#(\d+)\;/gi,"");

	re = /\<p(.*?)\>([\w\W]*?)\<\/p\>/gi;
  	text = text.replace(re, "<p>$2</p>");

	re = /\<\?xml(.*?)\/\>/gi;
	text = text.replace(re, "");


	text = text.replace(/\<p\>&nbsp;\<\/p\>/gi,'');
	text = text.replace(/\<p\>\<\/p\>/gi,'');
	text = text.replace(/\<p\>\<br \/\>\<\/p\>/gi,'');
	text = text.replace(/\<p\>\<br\/\>\<\/p\>/gi,'');

	text = text.replace(/\<span(.*?)\>([\w\W]*?)\<\/span\>/gi, "$2");
	text = text.replace(/\<li(.*?)\>([\w\W]*?)\<\/li\>/gi, "<li>$2</li>");
	text = text.replace(/\<h([1-7])(.*?)\>([\w\W]*?)\<\/h[1-7]\>/gi, "<h$1>$3</h$1>");

	text = text.replace(/\<ol(.*?)\>([\w\W]*?)\<\/ol\>/gi, "<ol>$2</ol>");
	text = text.replace(/\<ul(.*?)\>([\w\W]*?)\<\/ul\>/gi, "<ol>$2</ol>");

   	text = text.replace(/\<p\>&nbsp;\<\/p\>/gi,'');
	text = text.replace(/\<span(.*?)\>([\w\W]*?)\<\/span\>/gi, "$2");
	text = text.replace(/\<span(.*?)\>([\w\W]*?)\<\/span\>/gi, "$2");

	text = text.replace(/\<font(.*?)\>([\w\W]*?)\<\/font\>/gi, "$2");


	text = text.replace(/\&\#(\d+)\;/gi,"");


	text = text.replace(/\<!--\[if !supportLists\]--\>([\w\W]*?)\<!--\[endif\]--\>/gi, "");


	return text;
}

function indexOfArray(ar,item)
{
	var ret=-1;
	for(var x=0;x<ar.length;x++){if(ar[x]==item){ret=x;break;}}
	return(ret);
}

function cleanupHTML_ie(sHtml)
{
	sHtml=sHtml.replace(/\<br\s\/\>/gi,"<br />");
	var iEnt=-1;
	var flagTag=false,begTag=false,flagAttr=false,begAttr=false,flagEntity=false,begEntity=false,flagLi=false,dropTag=false;
	var empty=false,unclosed=false,unquoted=false,unopened=false,unclosedList=false;
	var tag="",lastTag="",ret="",entity="",attr="";

	for(var x=0;x<sHtml.length;x++)
	{
		c=sHtml.charAt(x);

		if(begEntity)
		{
			if(c==";")
			{
				begEntity=false;
				iEnt=indexOfArray(entities,entity);
				if(iEnt>-1)flagEntity=true;
			}
			else if(c==" "){begEntity=false;flagEntity=false;entity="";}
			else entity+=c;
		}

		if(c=="&")begEntity=true;
		if(begTag)
		{
			if(c==" " || c==">")
			{
				switch(tag.toLowerCase())
				{
					case "br":
					case "img":
					case "hr":
					case "input":
					case "link":
					case "base":
					case "basefont":
					case "col":
					case "frame":
					case "isindex":
					case "meta":
					case "param":
						empty=true;
						break;
					case "li":
						if(flagLi && lastTag!="ul" && lastTag!="ol" && lastTag!="/li")unclosed=true;
						flagLi=true;
						empty=false;
						break;
					case "ol":
					case "ul":
						if(lastTag=="/li" || lastTag=="ol" || lastTag=="ul")unopened=true;
						empty=false;
						break;
					case "/ol":
					case "/ul":
						if(lastTag=="/ol" || lastTag=="/ul")unclosedList=true;
						empty=false;
						break;
					case "font":case "/font":
					case "b":case "/b":
					case "i":case "/i":
					case "u":case "/u":
					case "center":case "/center":
					case "marquee":case "/marquee":
					case "blink":case "/blink":
					case "big":case "/big":
					case "small":case "/small":

						dropTag=true;
						empty=false;
						break;
					default:
						empty=false;
						break;
				}
				lastTag=tag.toLowerCase();
				tag="";
				begTag=false;
			}
			else tag+=c;
		}

		if(c=="<"){begTag=true;flagTag=true;}
		//if(c==">" && empty){c=" />";}

		if(begTag)ret+=c.toLowerCase();

		else if(flagTag)
		{
			if(flagAttr)
			{
				if(begAttr)
				{
					if(c!="\""){ret+="\""+c;unquoted=true;begAttr=false;}
					else{ret+=c;unquoted=false;begAttr=false;}
				}
				else if(c==" " || c==">" || c==" />")
				{
					if(unquoted)
					{
						if(sHtml.charAt(x-1)!="\""){ret+="\""+c;flagAttr=false;}
						else ret+=c;
					}
					else ret+=c;
				}
				else if(c=="\""){ret+=c;flagAttr=false;}
				else ret+=c;

				if(!flagAttr)
				{
					switch(attr.toLowerCase())
					{
						case "align":
						//case "background":
						case "bgcolor":
					   //	case "style":
						 //	ret=ret.substr(0,ret.lastIndexOf(attr+"=\""));
						//	if(c==">" || c==" />")ret+=c;
						//	break;
						default:
							break;
					}
				}
			}
			else if(c=="=")
			{
				begAttr=true;flagAttr=true;ret+=c;
				attr=ret.substr(ret.lastIndexOf(" ")+1,ret.length-(ret.lastIndexOf(" ")+2));
			}
			else ret+=c.toLowerCase();

			if(c==">" || c==" />")
			{
				flagTag=false;
				flagAttr=false;
				if(unclosed){ret=insertAt(ret,"</"+lastTag+">",ret.lastIndexOf("<"));unclosed=false;}
				if(unopened){ret=ret.substr(0,ret.lastIndexOf("</li>")) + "<"+lastTag+">";unopened=false;}
				if(unclosedList){ret=insertAt(ret,"</li>",ret.lastIndexOf("<"));unclosedList=false;}
				if(dropTag){ret=ret.substr(0,ret.lastIndexOf("<"));dropTag=false;}
			}
		}
		else ret+=c;


	   	if(flagEntity)
	   {
			ret=ret.substr(0,ret.lastIndexOf("&"))+"&#"+numEntities[iEnt]+ret.substr(ret.lastIndexOf(";"));
		  	entity="";
			flagEntity=false;
		}
	}

	//various cleanups

	//replacing '> <' by '><'
	var rExp=/> </gi;
	ret=ret.replace(rExp,"><");

	return(ret);
}




/*
	Charmap
*/

var charmap = new Array();

charmap = [
	['&nbsp;',    '&#160;',  true, 'no-break space'],
	['&amp;',     '&#38;',   true, 'ampersand'],
	['&quot;',    '&#34;',   true, 'quotation mark'],
// finance
	['&cent;',    '&#162;',  true, 'cent sign'],
	['&euro;',    '&#8364;', true, 'euro sign'],
	['&pound;',   '&#163;',  true, 'pound sign'],
	['&yen;',     '&#165;',  true, 'yen sign'],
// signs
	['&copy;',    '&#169;',  true, 'copyright sign'],
	['&reg;',     '&#174;',  true, 'registered sign'],
	['&trade;',   '&#8482;', true, 'trade mark sign'],
	['&permil;',  '&#8240;', true, 'per mille sign'],
	['&micro;',   '&#181;',  true, 'micro sign'],
	['&middot;',  '&#183;',  true, 'middle dot'],
	['&bull;',    '&#8226;', true, 'bullet'],
	['&hellip;',  '&#8230;', true, 'three dot leader'],
	['&prime;',   '&#8242;', true, 'minutes / feet'],
	['&Prime;',   '&#8243;', true, 'seconds / inches'],
	['&sect;',    '&#167;',  true, 'section sign'],
	['&para;',    '&#182;',  true, 'paragraph sign'],
	['&szlig;',   '&#223;',  true, 'sharp s / ess-zed'],
// quotations
	['&lsaquo;',  '&#8249;', true, 'single left-pointing angle quotation mark'],
	['&rsaquo;',  '&#8250;', true, 'single right-pointing angle quotation mark'],
	['&laquo;',   '&#171;',  true, 'left pointing guillemet'],
	['&raquo;',   '&#187;',  true, 'right pointing guillemet'],
	['&lsquo;',   '&#8216;', true, 'left single quotation mark'],
	['&rsquo;',   '&#8217;', true, 'right single quotation mark'],
	['&ldquo;',   '&#8220;', true, 'left double quotation mark'],
	['&rdquo;',   '&#8221;', true, 'right double quotation mark'],
	['&sbquo;',   '&#8218;', true, 'single low-9 quotation mark'],
	['&bdquo;',   '&#8222;', true, 'double low-9 quotation mark'],
	['&lt;',      '&#60;',   true, 'less-than sign'],
	['&gt;',      '&#62;',   true, 'greater-than sign'],
	['&le;',      '&#8804;', true, 'less-than or equal to'],
	['&ge;',      '&#8805;', true, 'greater-than or equal to'],
	['&ndash;',   '&#8211;', true, 'en dash'],
	['&mdash;',   '&#8212;', true, 'em dash'],
	['&macr;',    '&#175;',  true, 'macron'],
	['&oline;',   '&#8254;', true, 'overline'],
	['&curren;',  '&#164;',  true, 'currency sign'],
	['&brvbar;',  '&#166;',  true, 'broken bar'],
	['&uml;',     '&#168;',  true, 'diaeresis'],
	['&iexcl;',   '&#161;',  true, 'inverted exclamation mark'],
	['&iquest;',  '&#191;',  true, 'turned question mark'],
	['&circ;',    '&#710;',  true, 'circumflex accent'],
	['&tilde;',   '&#732;',  true, 'small tilde'],
	['&deg;',     '&#176;',  true, 'degree sign'],
	['&minus;',   '&#8722;', true, 'minus sign'],
	['&plusmn;',  '&#177;',  true, 'plus-minus sign'],
	['&divide;',  '&#247;',  true, 'division sign'],
	['&frasl;',   '&#8260;', true, 'fraction slash'],
	['&times;',   '&#215;',  true, 'multiplication sign'],
	['&sup1;',    '&#185;',  true, 'superscript one'],
	['&sup2;',    '&#178;',  true, 'superscript two'],
	['&sup3;',    '&#179;',  true, 'superscript three'],
	['&frac14;',  '&#188;',  true, 'fraction one quarter'],
	['&frac12;',  '&#189;',  true, 'fraction one half'],
	['&frac34;',  '&#190;',  true, 'fraction three quarters'],
// math / logical
	['&fnof;',    '&#402;',  true, 'function / florin'],
	['&int;',     '&#8747;', true, 'integral'],
	['&sum;',     '&#8721;', true, 'n-ary sumation'],
	['&infin;',   '&#8734;', true, 'infinity'],
	['&radic;',   '&#8730;', true, 'square root'],
	['&sim;',     '&#8764;', false,'similar to'],
	['&cong;',    '&#8773;', false,'approximately equal to'],
	['&asymp;',   '&#8776;', true, 'almost equal to'],
	['&ne;',      '&#8800;', true, 'not equal to'],
	['&equiv;',   '&#8801;', true, 'identical to'],
	['&isin;',    '&#8712;', false,'element of'],
	['&notin;',   '&#8713;', false,'not an element of'],
	['&ni;',      '&#8715;', false,'contains as member'],
	['&prod;',    '&#8719;', true, 'n-ary product'],
	['&and;',     '&#8743;', false,'logical and'],
	['&or;',      '&#8744;', false,'logical or'],
	['&not;',     '&#172;',  true, 'not sign'],
	['&cap;',     '&#8745;', true, 'intersection'],
	['&cup;',     '&#8746;', false,'union'],
	['&part;',    '&#8706;', true, 'partial differential'],
	['&forall;',  '&#8704;', false,'for all'],
	['&exist;',   '&#8707;', false,'there exists'],
	['&empty;',   '&#8709;', false,'diameter'],
	['&nabla;',   '&#8711;', false,'backward difference'],
	['&lowast;',  '&#8727;', false,'asterisk operator'],
	['&prop;',    '&#8733;', false,'proportional to'],
	['&ang;',     '&#8736;', false,'angle'],
// undefined
	['&acute;',   '&#180;',  true, 'acute accent'],
	['&cedil;',   '&#184;',  true, 'cedilla'],
	['&ordf;',    '&#170;',  true, 'feminine ordinal indicator'],
	['&ordm;',    '&#186;',  true, 'masculine ordinal indicator'],
	['&dagger;',  '&#8224;', true, 'dagger'],
	['&Dagger;',  '&#8225;', true, 'double dagger'],
// alphabetical special chars
	['&Agrave;',  '&#192;',  true, 'A - grave'],
	['&Aacute;',  '&#193;',  true, 'A - acute'],
	['&Acirc;',   '&#194;',  true, 'A - circumflex'],
	['&Atilde;',  '&#195;',  true, 'A - tilde'],
	['&Auml;',    '&#196;',  true, 'A - diaeresis'],
	['&Aring;',   '&#197;',  true, 'A - ring above'],
	['&AElig;',   '&#198;',  true, 'ligature AE'],
	['&Ccedil;',  '&#199;',  true, 'C - cedilla'],
	['&Egrave;',  '&#200;',  true, 'E - grave'],
	['&Eacute;',  '&#201;',  true, 'E - acute'],
	['&Ecirc;',   '&#202;',  true, 'E - circumflex'],
	['&Euml;',    '&#203;',  true, 'E - diaeresis'],
	['&Igrave;',  '&#204;',  true, 'I - grave'],
	['&Iacute;',  '&#205;',  true, 'I - acute'],
	['&Icirc;',   '&#206;',  true, 'I - circumflex'],
	['&Iuml;',    '&#207;',  true, 'I - diaeresis'],
	['&ETH;',     '&#208;',  true, 'ETH'],
	['&Ntilde;',  '&#209;',  true, 'N - tilde'],
	['&Ograve;',  '&#210;',  true, 'O - grave'],
	['&Oacute;',  '&#211;',  true, 'O - acute'],
	['&Ocirc;',   '&#212;',  true, 'O - circumflex'],
	['&Otilde;',  '&#213;',  true, 'O - tilde'],
	['&Ouml;',    '&#214;',  true, 'O - diaeresis'],
	['&Oslash;',  '&#216;',  true, 'O - slash'],
	['&OElig;',   '&#338;',  true, 'ligature OE'],
	['&Scaron;',  '&#352;',  true, 'S - caron'],
	['&Ugrave;',  '&#217;',  true, 'U - grave'],
	['&Uacute;',  '&#218;',  true, 'U - acute'],
	['&Ucirc;',   '&#219;',  true, 'U - circumflex'],
	['&Uuml;',    '&#220;',  true, 'U - diaeresis'],
	['&Yacute;',  '&#221;',  true, 'Y - acute'],
	['&Yuml;',    '&#376;',  true, 'Y - diaeresis'],
	['&THORN;',   '&#222;',  true, 'THORN'],
	['&agrave;',  '&#224;',  true, 'a - grave'],
	['&aacute;',  '&#225;',  true, 'a - acute'],
	['&acirc;',   '&#226;',  true, 'a - circumflex'],
	['&atilde;',  '&#227;',  true, 'a - tilde'],
	['&auml;',    '&#228;',  true, 'a - diaeresis'],
	['&aring;',   '&#229;',  true, 'a - ring above'],
	['&aelig;',   '&#230;',  true, 'ligature ae'],
	['&ccedil;',  '&#231;',  true, 'c - cedilla'],
	['&egrave;',  '&#232;',  true, 'e - grave'],
	['&eacute;',  '&#233;',  true, 'e - acute'],
	['&ecirc;',   '&#234;',  true, 'e - circumflex'],
	['&euml;',    '&#235;',  true, 'e - diaeresis'],
	['&igrave;',  '&#236;',  true, 'i - grave'],
	['&iacute;',  '&#237;',  true, 'i - acute'],
	['&icirc;',   '&#238;',  true, 'i - circumflex'],
	['&iuml;',    '&#239;',  true, 'i - diaeresis'],
	['&eth;',     '&#240;',  true, 'eth'],
	['&ntilde;',  '&#241;',  true, 'n - tilde'],
	['&ograve;',  '&#242;',  true, 'o - grave'],
	['&oacute;',  '&#243;',  true, 'o - acute'],
	['&ocirc;',   '&#244;',  true, 'o - circumflex'],
	['&otilde;',  '&#245;',  true, 'o - tilde'],
	['&ouml;',    '&#246;',  true, 'o - diaeresis'],
	['&oslash;',  '&#248;',  true, 'o slash'],
	['&oelig;',   '&#339;',  true, 'ligature oe'],
	['&scaron;',  '&#353;',  true, 's - caron'],
	['&ugrave;',  '&#249;',  true, 'u - grave'],
	['&uacute;',  '&#250;',  true, 'u - acute'],
	['&ucirc;',   '&#251;',  true, 'u - circumflex'],
	['&uuml;',    '&#252;',  true, 'u - diaeresis'],
	['&yacute;',  '&#253;',  true, 'y - acute'],
	['&thorn;',   '&#254;',  true, 'thorn'],
	['&yuml;',    '&#255;',  true, 'y - diaeresis'],
    ['&Alpha;',   '&#913;',  true, 'Alpha'],
	['&Beta;',    '&#914;',  true, 'Beta'],
	['&Gamma;',   '&#915;',  true, 'Gamma'],
	['&Delta;',   '&#916;',  true, 'Delta'],
	['&Epsilon;', '&#917;',  true, 'Epsilon'],
	['&Zeta;',    '&#918;',  true, 'Zeta'],
	['&Eta;',     '&#919;',  true, 'Eta'],
	['&Theta;',   '&#920;',  true, 'Theta'],
	['&Iota;',    '&#921;',  true, 'Iota'],
	['&Kappa;',   '&#922;',  true, 'Kappa'],
	['&Lambda;',  '&#923;',  true, 'Lambda'],
	['&Mu;',      '&#924;',  true, 'Mu'],
	['&Nu;',      '&#925;',  true, 'Nu'],
	['&Xi;',      '&#926;',  true, 'Xi'],
	['&Omicron;', '&#927;',  true, 'Omicron'],
	['&Pi;',      '&#928;',  true, 'Pi'],
	['&Rho;',     '&#929;',  true, 'Rho'],
	['&Sigma;',   '&#931;',  true, 'Sigma'],
	['&Tau;',     '&#932;',  true, 'Tau'],
	['&Upsilon;', '&#933;',  true, 'Upsilon'],
	['&Phi;',     '&#934;',  true, 'Phi'],
	['&Chi;',     '&#935;',  true, 'Chi'],
	['&Psi;',     '&#936;',  true, 'Psi'],
	['&Omega;',   '&#937;',  true, 'Omega'],
	['&alpha;',   '&#945;',  true, 'alpha'],
	['&beta;',    '&#946;',  true, 'beta'],
	['&gamma;',   '&#947;',  true, 'gamma'],
	['&delta;',   '&#948;',  true, 'delta'],
	['&epsilon;', '&#949;',  true, 'epsilon'],
	['&zeta;',    '&#950;',  true, 'zeta'],
	['&eta;',     '&#951;',  true, 'eta'],
	['&theta;',   '&#952;',  true, 'theta'],
	['&iota;',    '&#953;',  true, 'iota'],
	['&kappa;',   '&#954;',  true, 'kappa'],
	['&lambda;',  '&#955;',  true, 'lambda'],
	['&mu;',      '&#956;',  true, 'mu'],
	['&nu;',      '&#957;',  true, 'nu'],
	['&xi;',      '&#958;',  true, 'xi'],
	['&omicron;', '&#959;',  true, 'omicron'],
	['&pi;',      '&#960;',  true, 'pi'],
	['&rho;',     '&#961;',  true, 'rho'],
	['&sigmaf;',  '&#962;',  true, 'final sigma'],
	['&sigma;',   '&#963;',  true, 'sigma'],
	['&tau;',     '&#964;',  true, 'tau'],
	['&upsilon;', '&#965;',  true, 'upsilon'],
	['&phi;',     '&#966;',  true, 'phi'],
	['&chi;',     '&#967;',  true, 'chi'],
	['&psi;',     '&#968;',  true, 'psi'],
	['&omega;',   '&#969;',  true, 'omega'],
// symbols
	['&alefsym;', '&#8501;', false,'alef symbol'],
	['&piv;',     '&#982;',  false,'pi symbol'],
	['&real;',    '&#8476;', false,'real part symbol'],
	['&thetasym;','&#977;',  false,'theta symbol'],
	['&upsih;',   '&#978;',  false,'upsilon - hook symbol'],
	['&weierp;',  '&#8472;', false,'Weierstrass p'],
	['&image;',   '&#8465;', false,'imaginary part'],
// arrows
	['&larr;',    '&#8592;', true, 'leftwards arrow'],
	['&uarr;',    '&#8593;', true, 'upwards arrow'],
	['&rarr;',    '&#8594;', true, 'rightwards arrow'],
	['&darr;',    '&#8595;', true, 'downwards arrow'],
	['&harr;',    '&#8596;', true, 'left right arrow'],
	['&crarr;',   '&#8629;', false,'carriage return'],
	['&lArr;',    '&#8656;', false,'leftwards double arrow'],
	['&uArr;',    '&#8657;', false,'upwards double arrow'],
	['&rArr;',    '&#8658;', false,'rightwards double arrow'],
	['&dArr;',    '&#8659;', false,'downwards double arrow'],
	['&hArr;',    '&#8660;', false,'left right double arrow'],
	['&there4;',  '&#8756;', false,'therefore'],
	['&sub;',     '&#8834;', false,'subset of'],
	['&sup;',     '&#8835;', false,'superset of'],
	['&nsub;',    '&#8836;', false,'not a subset of'],
	['&sube;',    '&#8838;', false,'subset of or equal to'],
	['&supe;',    '&#8839;', false,'superset of or equal to'],
	['&oplus;',   '&#8853;', false,'circled plus'],
	['&otimes;',  '&#8855;', false,'circled times'],
	['&perp;',    '&#8869;', false,'perpendicular'],
	['&sdot;',    '&#8901;', false,'dot operator'],
	['&lceil;',   '&#8968;', false,'left ceiling'],
	['&rceil;',   '&#8969;', false,'right ceiling'],
	['&lfloor;',  '&#8970;', false,'left floor'],
	['&rfloor;',  '&#8971;', false,'right floor'],
	['&lang;',    '&#9001;', false,'left-pointing angle bracket'],
	['&rang;',    '&#9002;', false,'right-pointing angle bracket'],
	['&loz;',     '&#9674;', true,'lozenge'],
	['&spades;',  '&#9824;', false,'black spade suit'],
	['&clubs;',   '&#9827;', true, 'black club suit'],
	['&hearts;',  '&#9829;', true, 'black heart suit'],
	['&diams;',   '&#9830;', true, 'black diamond suit'],
	['&ensp;',    '&#8194;', false,'en space'],
	['&emsp;',    '&#8195;', false,'em space'],
	['&thinsp;',  '&#8201;', false,'thin space'],
	['&zwnj;',    '&#8204;', false,'zero width non-joiner'],
	['&zwj;',     '&#8205;', false,'zero width joiner'],
	['&lrm;',     '&#8206;', false,'left-to-right mark'],
	['&rlm;',     '&#8207;', false,'right-to-left mark'],
	['&shy;',     '&#173;',  false,'soft hyphen']
];

function renderCharMapHTML(nid)
{
	var html = '';
	var cols=-1;
	for (var i=0; i<charmap.length; i++)
	{
		if (charmap[i][2]==true)
		{
			html += '<a onclick="insert_char(' + nid + ',\'' + charmap[i][1].substring(2,charmap[i][1].length-1) + '\');" href="javascript:void(null)" onmousedown="return false;" title="' + charmap[i][3] + '">'
				+ charmap[i][1]
				+ '</a>';
		}
	 }
  	return html;
}

function insert_char(nid,chr)
{
	chr = '&#' + chr + ';';

	Redactor.insert_at_cursor(nid,chr);
	//$('inserts_' + nid).hide();
}


function echo_charmap(nid)
{
	var charmap = renderCharMapHTML(nid);
	html =	'<table align="center" border="0" cellspacing="0" cellpadding="2">'
	    + '<tr><td class="charmap">'+ charmap + '</td></tr></table>';
	return html;
}

function format_html(html) {

	var p = '', i = 0, li = 0, o = '', l;

	// Replace BR in pre elements to \n
	html = html.replace(/<pre([^>]*)>(.*?)<\/pre>/gi, function (a, b, c) {

	c = c.replace(/<br\s*\/>/gi, '\n');
	return '<pre' + b + '>' + c + '</pre>';
	});

	html = html.replace(/\r/g, '');


// <td> \t
// </td> \n

	html = html.replace(new RegExp('\\n\\s+', 'gi'), '\n');
	html = html.replace(/\<h(\d)\>/gi, '\n<h$1>');
	html = html.replace(/\<\/h(\d)\>/gi, '</h$1>\n');
	html = html.replace(/\<\/p\>/gi, '</p>\n');
	html = html.replace(/\<\/blockquote\>/gi, '</blockquote>\n');
	html = html.replace(/\<table/gi, '\n<table');
	html = html.replace(/\<\/table\>/gi, '\n</table>');

	html = html.replace(/\<td\>\<br\>\n\<\/td\>/gi, '<td></td>');
	html = html.replace(/\<td\>\<br \/\>\n\<\/td\>/gi, '<td></td>');

	html = html.replace(/(\n)\<\/td\>/gi, '</td>');

	html = html.replace(/\<br\>/gi, '<br />\n');
	html = html.replace(/\<br \/\>/gi, '<br />\n');

	html = html.replace(/\<tbody\>/gi, '\n<tbody>');
	html = html.replace(/\<\/tbody\>/gi, '\n</tbody>');

	html = html.replace(/\<tr/gi, '\n\t<tr');
	html = html.replace(/\<\/tr\>/gi, '\t</tr>');
	html = html.replace(/\<td/gi, '\n\t\t<td');

	html = html.replace(/\<li/gi, '\n\t<li');
	html = html.replace(/\<\/li\>/gi, '</li>');
	html = html.replace(/\<ol/gi, '\n<ol');
	html = html.replace(/\<ul/gi, '\n<ul');
	html = html.replace(/\<\/ol\>/gi, '\n</ol>\n');
	html = html.replace(/\<\/ul\>/gi, '\n</ul>\n');
	html = html.replace(/\n\n/gi, '\n');

	return html;
}

function getSel(nid)
{
	if (Redactor.browser.IE) return $('rte_' + nid).contentWindow.document.selection;
	else return $('rte_' + nid).contentWindow.getSelection();
}

function getRng(nid)
{
		var s = getSel(nid);

		if (s == null)
			return null;

		if (Redactor.browser.IE) return s.createRange();

		if (Redactor.browser.WebKit && !s.getRangeAt) return '' + window.getSelection();
		if (s.rangeCount > 0) return s.getRangeAt(0);
		return null;
}

function getFocusElement(nid)
{
	if (Redactor.browser.IE)
	{
		doc = $('rte_' + nid).contentWindow.document;
		rng = doc.selection.createRange();

		elm = rng.item ? rng.item(0) : rng.parentElement();
		return elm;
	}
	else
	{
		sel = getSel(nid);
		rng = getRng(nid);

		if (!sel || !rng) return null;

		elm = rng.commonAncestorContainer;

		if (!rng.collapsed)
		{
			if (rng.startContainer == rng.endContainer)
			{
				if (rng.startOffset - rng.endOffset < 2)
				{
					if (rng.startContainer.hasChildNodes()) elm = rng.startContainer.childNodes[rng.startOffset];
				}
			}

			elm = getParentElement(nid, elm);
		}

		return elm;

	}
}


function getParentElement (nid, n, na, f)
{
	var r = $('rte_' + nid).contentWindow.document.body;
	var re = na ? new RegExp('^(' + na.toUpperCase().replace(/,/g, '|') + ')$') : 0, v;

	if (f && typeof(f) == 'string') return getParentElement(n, na, function(no) {return getAttrib(no, f) !== '';});

	return getParentNode(n, function(n) {
			return ((n.nodeType == 1 && !re) || (re && re.test(n.nodeName))) && (!f || f(n));
		}, r);
}


function getParentNode(n, f, r)
{
	while (n)
	{
		if (n == r) return null;
		if (f(n)) return n;
		n = n.parentNode;
	}
	return null;
}


function getCell(grid, row, col)
{
	if (grid[row] && grid[row][col]) return grid[row][col];
	return null;
}

function getTableGrid(table)
{
	var grid = new Array();
	if (table != null)
	{
 		rows = table.rows;

	var x, y, td, sd, xstart, x2, y2;

	for (y=0; y<rows.length; y++)
	{
		for (x=0; x<rows[y].cells.length; x++)
		{
			td = rows[y].cells[x];
			sd = getColRowSpan(td);

			// All ready filled
			for (xstart = x; grid[y] && grid[y][xstart]; xstart++) ;

			// Fill box
			for (y2=y; y2<y+sd['rowspan']; y2++)
			{
				if (!grid[y2])
				grid[y2] = new Array();

				for (x2=xstart; x2<xstart+sd['colspan']; x2++)
				grid[y2][x2] = td;
			}
		}
	}
	return grid;
	}
	else return null;
}

function getColRowSpan(td)
{
	var colspan = getAttrib(td, "colspan");
	var rowspan = getAttrib(td, "rowspan");

	colspan = colspan == "" ? 1 : parseInt(colspan);
	rowspan = rowspan == "" ? 1 : parseInt(rowspan);

	return {colspan : colspan, rowspan : rowspan};
}

function getAttrib(elm, name, dv)
{
	var v;

	if (typeof(dv) == "undefined") dv = "";

	// Not a element
	if (!elm || elm.nodeType != 1) return dv;

	try
	{
		v = elm.getAttribute(name, 0);
	}
	catch (ex)
	{
		// IE 7 may cast exception on invalid attributes
		v = elm.getAttribute(name, 2);
	}

	// Try className for class attrib
	if (name == "class" && !v) v = elm.className;

	// Workaround for a issue with Firefox 1.5rc2+
	if (Redactor.browser.Gecko)
	{
		if (name == "src" && elm.src != null && elm.src !== '') v = elm.src;

		// Workaround for a issue with Firefox 1.5rc2+
		if (name == "href" && elm.href != null && elm.href !== '')	v = elm.href;
	}
	else if (Redactor.browser.IE)
	{
		switch (name)
		{
			case "http-equiv":
				v = elm.httpEquiv;
			break;

			case "width":
			case "height":
				v = elm.getAttribute(name, 2);
				break;
		}
	}

	 if (name == "style" && !Redactor.browser.Opera) v = elm.style.cssText;
	return (v && v !== '') ? v : dv;
}


function getCellPos(grid, td)
{
	var x, y;

	for (y=0; y<grid.length; y++)
	{
		for (x=0; x<grid[y].length; x++)
		{
			if (grid[y][x] == td) return {cellindex : x, rowindex : y};
		}
	}
	return null;
}


function deleteMarked(tbl)
{
	if (tbl.rows == 0) return;

	var tr = tbl.rows[0];
	do
	{
		var next = nextElm(tr, "TR");

		// Delete row
		if (tr._delete)
		{
			tr.parentNode.removeChild(tr);
			continue;
		}

		// Delete cells
		var td = tr.cells[0];
		if (td.cells > 1)
		{
			do
			{
				var nexttd = nextElm(td, "TD,TH");
				if (td._delete) td.parentNode.removeChild(td);
			}
		   		while ((td = nexttd) != null);
			}
		}
	while ((tr = next) != null);
}

function nextElm(node, names)
{
	var namesAr = names.split(',');

	while ((node = node.nextSibling) != null)
	{
		for (var i=0; i<namesAr.length; i++)
		{
			if (node.nodeName.toLowerCase() == namesAr[i].toLowerCase() ) return node;
		}
		return null;
	}
}


function table_func(nid, control)
{
	if (Redactor.cpos != null)
	{
		if (control == 'add_tr_before') add_tr_before(nid);
		if (control == 'add_tr_after') add_tr_after(nid);
		if (control == 'delete_row') delete_row(nid);
		if (control == 'insert_col_before') insert_col_before(nid);
		if (control == 'insert_col_after') insert_col_after(nid);
		if (control == 'delete_col') delete_col(nid);
		if (control == 'table_delete') table_delete(nid);
	}
}

function add_tr_before(nid)
{

	var newTR = $('rte_' + nid).contentWindow.document.createElement("tr");
	var lastTDElm = null;

	Redactor.cpos.rowindex--;
	if (Redactor.cpos.rowindex < 0) Redactor.cpos.rowindex = 0;

	// Create cells
	for (var x=0; Redactor.tdElm = getCell(Redactor.grid, Redactor.cpos.rowindex, x); x++)
	{
		if (Redactor.tdElm != lastTDElm)
		{
			var sd = getColRowSpan(Redactor.tdElm);
			if (sd['rowspan'] == 1)
			{
				var newTD =  $('rte_' + nid).contentWindow.document.createElement("td");

				newTD.innerHTML = "&nbsp;";
				newTD.colSpan = Redactor.tdElm.colSpan;
				newTR.appendChild(newTD);
			}
			else Redactor.tdElm.rowSpan = sd['rowspan'] + 1;
			lastTDElm = Redactor.tdElm;
		}
	}

	Redactor.trElm.parentNode.insertBefore(newTR, Redactor.trElm);
	Redactor.grid = getTableGrid(Redactor.tableElm);
}


function delete_row(nid)
{
	// Only one row, remove whole table
	if (Redactor.grid.length == 1)
	{
		Redactor.tableElm =getParentElement(nid,Redactor.tableElm, "table"); // Look for table instead of tbody
		Redactor.tableElm.parentNode.removeChild(Redactor.tableElm);
		return true;
	}

	// Move down row spanned cells
	var cells = Redactor.trElm.cells;
	var nextTR = nextElm(Redactor.trElm, "TR");
	for (var x=0; x<cells.length; x++)
	{
		if (cells[x].rowSpan > 1)
		{
			var newTD = cells[x].cloneNode(true);
			var sd = getColRowSpan(cells[x]);
			newTD.rowSpan = sd.rowspan - 1;

			var nextTD = nextTR.cells[x];
			if (nextTD == null)	nextTR.appendChild(newTD);
			else nextTR.insertBefore(newTD, nextTD);
		}
	}

	// Delete cells
	var lastTDElm = null;
	for (var x=0; Redactor.tdElm = getCell(Redactor.grid, Redactor.cpos.rowindex, x); x++)
	{
		if (Redactor.tdElm != lastTDElm)
		{
			var sd = getColRowSpan(Redactor.tdElm);
			if (sd.rowspan > 1) Redactor.tdElm.rowSpan = sd.rowspan - 1;
			else
			{
				Redactor.trElm = Redactor.tdElm.parentNode;
				if (Redactor.trElm.parentNode) Redactor.trElm._delete = true;
			}
			lastTDElm = Redactor.tdElm;
		}
	}

	deleteMarked(Redactor.tableElm);
}

function insert_col_before(nid)
{
	var lastTDElm = null;
	for (var y=0; Redactor.tdElm = getCell(Redactor.grid, y, Redactor.cpos.cellindex); y++)
	{
		if (Redactor.tdElm != lastTDElm) var sd = getColRowSpan(Redactor.tdElm);

		if (sd['colspan'] == 1)
		{
			var newTD = $('rte_' + nid).contentWindow.document.createElement(Redactor.tdElm.nodeName);

			newTD.innerHTML = "&nbsp;";
			newTD.rowSpan = Redactor.tdElm.rowSpan;

			Redactor.tdElm.parentNode.insertBefore(newTD, Redactor.tdElm);
		}
		else Redactor.tdElm.colSpan++;
		lastTDElm = Redactor.tdElm;
	}
	Redactor.grid = getTableGrid(Redactor.tableElm);
}

function insert_col_after(nid)
{
	var lastTDElm = null;

	for (var y=0; Redactor.tdElm = getCell(Redactor.grid, y, Redactor.cpos.cellindex); y++)
	{
		if (Redactor.tdElm != lastTDElm)
		{
			var sd = getColRowSpan(Redactor.tdElm);
			if (sd['colspan'] == 1)
			{
				var newTD = $('rte_' + nid).contentWindow.document.createElement(Redactor.tdElm.nodeName);
				newTD.innerHTML = "&nbsp;";
				newTD.rowSpan = Redactor.tdElm.rowSpan;

				var nextTD = nextElm(Redactor.tdElm, "TD,TH");
				if (nextTD == null) Redactor.tdElm.parentNode.appendChild(newTD);
				else nextTD.parentNode.insertBefore(newTD, nextTD);
			}
			else Redactor.tdElm.colSpan++;
			lastTDElm = Redactor.tdElm;
		}
	}
	Redactor.grid = getTableGrid(Redactor.tableElm);
}

function delete_col(nid)
{
	var lastTDElm = null;

	// Only one col, remove whole table
	if (Redactor.grid.length > 1 && Redactor.grid[0].length <= 1)
	{
		Redactor.tableElm = getParentElement(nid,Redactor.tableElm, "table"); // Look for table instead of tbody
		Redactor.tableElm.parentNode.removeChild(Redactor.tableElm);
		return true;
	}

	// Delete cells
	for (var y=0; Redactor.tdElm = getCell(Redactor.grid, y, Redactor.cpos.cellindex); y++)
	{
		if (Redactor.tdElm != lastTDElm)
		{
			var sd = getColRowSpan(Redactor.tdElm);
			if (sd['colspan'] > 1) Redactor.tdElm.colSpan = sd['colspan'] - 1;
			else
			{
				if (Redactor.tdElm.parentNode) Redactor.tdElm.parentNode.removeChild(Redactor.tdElm);
			}
		lastTDElm = Redactor.tdElm;
		}
	}
}

function add_tr_after(nid)
{
	var newTR = $('rte_' + nid).contentWindow.document.createElement("tr");
	var lastTDElm = null;

	// Create cells
	for (var x=0; Redactor.tdElm = getCell(Redactor.grid, Redactor.cpos.rowindex, x); x++)
	{
		if (Redactor.tdElm != lastTDElm)
		{
			var sd = getColRowSpan(Redactor.tdElm);
			if (sd['rowspan'] == 1)
			{
				var newTD =  $('rte_' + nid).contentWindow.document.createElement("td");
				newTD.innerHTML = "&nbsp;";
				newTD.colSpan = Redactor.tdElm.colSpan;
				newTR.appendChild(newTD);
			}
			else Redactor.tdElm.rowSpan = sd['rowspan'] + 1;
		lastTDElm = Redactor.tdElm;
		}
	}

	if (newTR.hasChildNodes())
	{
		var nextTR = nextElm(Redactor.trElm, "TR");
		if (nextTR) nextTR.parentNode.insertBefore(newTR, nextTR);
	 	else Redactor.tableElm.appendChild(newTR);
	}
	Redactor.grid = getTableGrid(Redactor.tableElm);
}

function table_delete(nid)
{
	var table = getParentElement(nid, getFocusElement(nid), "table");
	if (table)
	{
		table.parentNode.removeChild(table);
	}
	return true;
}

