<?php
header("Content-type: text/html; Charset=utf-8");

include "../config.php";
include ROOT."/functions/global.php";

if (isset($_GET['get']))
{
	$sql = "SELECT `content` FROM `".T_."content` WHERE `page_id`='".intval($_GET['page_id'])."' AND `content_id`='".intval($_GET['id'])."' LIMIT 1";
	$res = mysql_query($sql);
	$row = mysql_fetch_assoc($res);
	echo $row['content'];
	exit;
}

if (isset($_GET['delete']))
{
	$sql = "DELETE FROM `".T_."content` WHERE `page_id`='".intval($_GET['page_id'])."' AND `content_id`='".intval($_GET['id'])."'";
	mysql_query($sql);
	exit;
}

echo '<h3>Версии документа</h3>';


$sql = "SELECT `content_id`, `content_date`, `page_id` FROM `".T_."content` WHERE `page_id`='".intval($_GET['page_id'])."' ORDER BY `content_date` DESC";
$res = mysql_query($sql);

if ($res)
{
	$rows = mysql_num_rows($res);
	$i = $rows;

	$epoch_2 = mktime(date('H'), date('i'),date('s'),date('m'), date('d'), date('Y'));

	echo '<table>';
	while ($row = mysql_fetch_assoc($res))
	{
		$i--;

		$date = $row['content_date'];
		preg_match("/([0-9]{4})-([0-9]{1,2})-([0-9]{1,2}) ([0-9]{1,2}):([0-9]{1,2}):([0-9]{1,2})/i", $date, $regs);
		$epoch_1 = mktime($regs[4], $regs[5], $regs[6], $regs[2], $regs[3], $regs[1]);

		$diff_seconds = $epoch_2 - $epoch_1;
		$days = $diff_seconds/86400;
		if ($days < 1) $date = time_from_integer($diff_seconds);
		else $date = intval($regs[3]).'.'.$regs[2].'.'.$regs[1];

		$class = $i % 2 ? 'row1' : 'row2';

		echo '<tr id="version_row_'.intval($_GET['page_id']).'_'.$row['content_id'].'" class="'.$class.'">
		<td><a href="javascript:Redactor.set_version('.intval($_GET['page_id']).','.$row['content_id'].')">Версия '.$i.'</a></td><td>'.$date.'</td>
		<td><a href="javascript:Redactor.delete_version('.intval($_GET['page_id']).','.$row['content_id'].');">Удалить</a></tr>';
	}
	echo '</table>';
}

?>