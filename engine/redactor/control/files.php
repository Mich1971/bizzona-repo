<?php

include "../config.php";
include ROOT."/functions/global.php";

$err = '';
$dg = '0';
$type = 'file';
$filelimit = get_cfg_var('post_max_size'); // MB

if (isset($_POST['upload']))
{
	if (!empty($_FILES['file']['name']))
	{
		if ($_FILES['file']['type'] == 'image/gif')
		{
			$dg = 'gif';
			$type = 'img';
		}
		if ($_FILES['file']['type'] == "image/jpeg" || $_FILES['file']['type']== "image/pjpeg")
		{
			$dg = 'jpg';
			$type = 'img';
		}
		if ($_FILES['file']['type'] == 'image/png')
		{
			$dg = 'png';
		   	$type = 'img';
		}

		//if ($dg == '0') $err .= "Файлы такого формата нельзя закачивать.<br />";
	}
	else $err = 1;

	if($_FILES["file"]["size"] > 1024*$filelimit*1024)
	{
		$err .= "Размер файла превышает $filelimit MB.<br />";
	}

	if (!$err)
	{
		if (!$_POST['folder_name']) $folder_id = intval($_POST['folder_id']);
		else
		{
			$sql = "INSERT `".T_."files_folders` VALUES
			('',
			'".addslashes($_POST['folder_name'])."',
			'0'
			);";
			mysql_query($sql);
		    $folder_id = mysql_insert_id();
		}


		$sql = "INSERT `".T_."files` VALUES ('',
		'".$folder_id."',
		'',
        '".$type."',
		NOW(),
		'".addslashes($_POST['file_name'])."',
		'0',
		'0',
		'0'
		);";
		mysql_query($sql);

		$file_id = mysql_insert_id();

		$array = explode(".", $_FILES["file"]["name"]);
		$countArray = count($array);
		$resx = $array[$countArray-1];
		$name = str_replace($resx, '', $_FILES["file"]["name"]);


		$file_name = translate($name).'.'.$resx;
		if (file_exists(PATH.$file_name))
		{
			$file_name = translate($name).'_'.$file_id.'.'.$resx;
		}


		copy($_FILES["file"]["tmp_name"], PATH.$file_name);
		$filesize = $_FILES["file"]["size"];

		if ($type == 'img')
		{
              $image = new image_resize();
			  $image->load(PATH.$file_name);
		      $image->setMaxHeight(80);
			  $image->setJpegQuality(100);
			  $image->resize();
			  $image->saveThumbnail(PATH."s_".$file_name);
		}

		$size = getImageSize(PATH.$file_name);
		$width = $size[0];
		$height = $size[1];

		$sql = "UPDATE `".T_."files` SET
		`file_name`='".$file_name."',
		`file_width`='".$width."',
		`file_height`='".$height."',
		`file_size`='".$filesize."'
		WHERE `file_id`='".$file_id."'";
		mysql_query($sql);

		header("Location: ?view");
	}
}

if (isset($_GET['delete']))
{
	$file_id = intval($_GET['id']);
	$sql = "SELECT `file_name` FROM `".T_."files` WHERE `file_id`='".$file_id."'";
	$res = mysql_query($sql);
	$row = mysql_fetch_assoc($res);

	$sql = "DELETE FROM `".T_."files` WHERE `file_id`='".$file_id."'";
	mysql_query($sql);

	@unlink(PATH.'s_'.$row['file_name']);
	unlink(PATH.$row['file_name']);

	header("Location: ?deleted&mode=".$_GET['mode']);

}


// Select folders
$sql = "SELECT * FROM `".T_."files_folders` ORDER BY `folder_name`";
$res = mysql_query($sql);
$rows = mysql_num_rows($res);

$folders = array();
while ($row = mysql_fetch_assoc($res))
{
	$folders[$row['folder_id']] = $row['folder_name'];
}

$modes['img'] = array('name' => 'изображения', 'selected' => '');
$modes['file'] = array('name' => 'файлы', 'selected' => '');


// Select images
$where_type = " AND `file_type`='img'";
$mode = 'img';
if (isset($_GET['mode']))
{
	if ($_GET['mode'] == 'img')
	{
		$where_type = " AND `file_type`='img'";
		$modes['img']['selected'] = 'selected';
	}
	if ($_GET['mode'] == 'file')
	{
		$where_type = " AND `file_type`='file'";
		$modes['file']['selected'] = 'selected';
		$mode = 'file';
	}
}


$where = '';
if (isset($_GET['folder']))
{
	if ($_GET['folder'] != 0) $where = " AND `folder_id`='".intval($_GET['folder'])."'";
}

$sql = "SELECT * FROM `".T_."files` WHERE `file_id`!='0' $where $where_type ORDER BY `file_date` DESC";

$res_img = mysql_query($sql);
$rows_img = mysql_num_rows($res_img);

if ($rows_img != 0 && !isset($_GET['upload'])) $display = 'two';
else $display = 'one';

$files = array();
if ($rows_img != 0):
$i = 0;
while ($row = mysql_fetch_assoc($res_img))
{
	$i++;
	$files[$i]['class'] = $i % 2 ? 'row1' : 'row2';

	$href = 'http://'.APATH.$row['file_name'];
	$s_href = 'http://'.APATH.'s_'.$row['file_name'];

	$files[$i]['href'] = $href;
	$files[$i]['id'] = $row['file_id'];

	if ($row['file_type'] == 'img')
	{
		$value = '<img src="'.$s_href.'" alt="'.$row['file_alt'].'" />';
		$name = $row['file_alt'];
		$files[$i]['name'] = '<a id="a_'.$row['file_id'].'" class="" href="javascript:prepare(\''.$href.'\',\''.$s_href.'\',\''.$row['file_alt'].'\','.$row['file_id'].');">'.$value.'</a>';
	}
	else
	{
		if (!empty($row['file_alt'])) $name = $row['file_alt'];
		else $name = $row['file_name'];
		$value = $name;
		$files[$i]['name'] = '<a href="javascript:insert_link(\''.$href.'\',\''.$row['file_alt'].'\');">'.$value.'</a>';
	}

}
endif;
?>