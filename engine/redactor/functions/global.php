<?php

// $time - seconds;
function time_from_integer($time)
{
	// seconds
	if ($time == 0) return '1 сек';
	else if ($time < 60)
	{
		return $time.' сек.';
	}
	// minutes
	else if ($time > 59 && $time < 3600)
	{
		$minutes = floor($time/60);
		$seconds = $time-($minutes*60);
		if ($seconds != 0) $res_time = $minutes.' мин '.$seconds.' сек.';
		else $res_time = $minutes.' мин ';

		return $res_time;
	}
	// hours
	else if ($time > 3600 && $time < 86400)
	{
		$hours = floor($time/3600);
		$time_t = $time-($hours*3600);
		$minutes = floor($time_t/60);
		if ($minutes != 0) return $hours.' ч '.$minutes.' мин ';
		else return $hours.' ч ';
	}
	// days
	else if ($time > 86400)
	{
		$days = floor($time/86400);
		$time_t = $time-($days*86400);
		$hours = floor($time_t/3600);
		if ($hours != 0) return $days.' д. '.$hours.' ч.';
		return $days.' д.';
	}
}

function translate($str)
{
	$from = array("Я", "Я", "я", "Щ", "Щ", "щ", "Ч", "Ч", "ч", "Ш", "Ш", "ш", "А", "а",
	"Б", "б", "В", "в", "Г", "г", "Д", "д", "Е", "е", "Е", "е", "Ж", "ж", "З", "з",
	"И", "и", "Й", "й", "К", "к", "Л", "л", "М", "м", "Н", "н", "О", "о", "П", "п",
	"Р", "р", "С", "с", "Т", "т", "У", "у", "Ф", "ф", "Х", "х", "Ц", "ц", "Ъ", "ъ",
	"Ы", "ы", "Ь", "ь", "Э", "э", "Ю", "ю");
	$to = array("Ya", "ya", "ya", "SCH", "Sch", "sch", "CH", "Ch", "ch", "SH", "Sh", "sh",
	"A", "a", "B", "b", "V", "v", "G", "g", "D", "d", "E", "e", "E", "e", "J", "j", "Z",
	"z", "I", "i", "Y", "y", "K", "k", "L", "l", "M", "m", "N", "n", "O", "o", "P", "p",
	"R", "r", "S", "s", "T", "t", "U", "u", "F", "f", "H", "h", "C", "c", "", "", "I", "i",
	"'", "'", "E", "e", "U", "u");
	$str = str_replace($from, $to, $str);
	$str = strtolower($str);
	$str = str_replace('\\', '/', $str);
	$str = str_replace(' ', '_', $str);
	$str = str_replace('(', '_', $str);
	$str = str_replace('\'', '', $str);
	$str = str_replace('\"', '', $str);
	$str = str_replace('.', '_', $str);
	$str = str_replace('*', '', $str);
	$str = preg_replace("/[\%=~^`<>']/i", "", $str);
	$str = str_replace(')', '_', $str);
	$str = ereg_replace("^_", "", $str);
	$str = ereg_replace("_$", "", $str);
	return $str;
}

/**
 @author Greg Neustaetter
 @alteration and re-development for php4 lessio.ru
*/

define('MAX_WIDTH', 2);
define('MAX_HEIGHT', 1);
define('BEST_FIT', 3);
define('EXACT', 4);

class image_resize
{
	var $originalPath = null;
	var $original = null;
	var $thumb = null;
	var $type = null;
	var $outputType = null;
	var $originalDimensions = array();
	var $maxWidth = 150;
	var $maxHeight = 100;
	var $quality = 100;



	function load($imagePath)
	{
		$this->originalPath = $imagePath;
		$this->populateImageInfo();
	}

	function getThumbnailDimensions($type) {
		$w = $this->getWidth();
		$h = $this->getHeight();
		$nw = null;
		$nh = null;
		switch($type) {
			case MAX_HEIGHT:
				$nh = ($h > $this->maxHeight) ? $this->maxHeight : $h;
				$nw = intval(($nh * $w) / $h);
				break;
			case MAX_WIDTH:
				$nw = ($w > $this->maxWidth) ? $this->maxWidth : $w;
				$nh = intval(($nw * $h) / $w);
				break;
			case BEST_FIT:
				if(($h / $this->maxHeight) >= ($w / $this->maxWidth)) {
					$nh = ($h > $this->maxHeight) ? $this->maxHeight : $h;
					$nw = intval(($nh * $w) / $h);
				} else {
					$nw = ($w > $this->maxWidth) ? $this->maxWidth : $w;
					$nh = intval(($nw * $h) / $w);
				}
				break;
			case EXACT:
				$nw = $this->maxWidth;
				$nh = $this->maxHeight;
				break;
		}
		return array('width' => $nw, 'height' => $nh);
	}

	/**
	 * Sets the output type of thumbnail images.  If not called, the output type will be the same as input type.
	 *
	 * @param constant $type Valid values include the constants: IMAGETYPE_JPEG, IMAGETYPE_GIF, or IMAGETYPE_PNG
	 * @return Gregphoto_Image The Gregphoto_Image instance - used for 'fluent' access
	 */
	 function setOutputType($type) {
		$this->outputType = $type;
		return $this;
	}

	/**
	 * Returns the image type of the image used to instantiate the class
	 *
	 * @return constant One of the following constants: IMAGETYPE_JPEG, IMAGETYPE_GIF, or IMAGETYPE_PNG
	 */
	function getImageType() {
		return $type;
	}

	/**
	 * Returns the width of the image used to instantiate the class
	 *
	 * @return int The width in pixels
	 */
	function getWidth() {
		return $this->originalDimensions['width'];
	}

	/**
	 * Returns the height of the image used to instantiate the class
	 *
	 * @return int The height in pixels
	 */
	function getHeight() {
		return $this->originalDimensions['height'];
	}

	/**
	 * Sets the maximum height to be used in thumbnail creation
	 *
	 * @param int $maxHeight The height in pixels.  Defaults to 100.
	 * @return Gregphoto_Image The Gregphoto_Image instance - used for 'fluent' access
	 */
	function setMaxHeight($maxHeight) {
		$this->maxHeight = $maxHeight;
		return $this;
	}

	/**
	 * Sets the maximum width to be used in thumbnail creation
	 *
	 * @param int $maxWidth The width in pixels. Defaults to 150.
	 * @return Gregphoto_Image The Gregphoto_Image instance - used for 'fluent' access
	 */
	function setMaxWidth($maxWidth) {
		$this->maxWidth = $maxWidth;
		return $this;
	}


	/**
	 * Sends the proper content-type header to the browser and renders the thumbnail.  Gregphoto_Image::resize must be called prior to this method.
	 *
	 * @return Gregphoto_Image The Gregphoto_Image instance - used for 'fluent' access
	 */
	function showThumbnail() {
	  	header("Content-type: " . image_type_to_mime_type($this->outputType));
		$this->getThumb(NULL);
	}

	/**
	 * Saves the thumbnail to the filesystem.  Gregphoto_Image::resize must be called prior to this method.
	 *
	 * @param string $filename The full file path (relative or absolute) where the image should be saved
	 * @return Gregphoto_Image The Gregphoto_Image instance - used for 'fluent' access
	 */
	function saveThumbnail($filename) {
		$this->getThumb($filename);
		$this->destroy();
	}

	/**
	 * Sets the quality level for JPEG images
	 *
	 * @param int $quality An integer between 0 and 100, where 100 is the best quality.  Defaults to 100.
	 * @return Gregphoto_Image The Gregphoto_Image instance - used for 'fluent' access
	 */
	 function setJpegQuality($quality)
	 {
		$this->quality = $quality;
		return $this;
	}

	/**
	 * Resizes the image
	 *
	 * @param constant $type An image resize method.  Must be one of: Gregphoto_Image::MAX_HEIGHT, Gregphoto_Image::MAX_WIDTH, Gregphoto_Image::BEST_FIT, Gregphoto_Image::EXACT
	 * @return Gregphoto_Image The Gregphoto_Image instance - used for 'fluent' access
	 */
	function resize($type = MAX_HEIGHT) {
		$dimensions = $this->getThumbnailDimensions($type);
		$this->thumb = imagecreatetruecolor($dimensions['width'],$dimensions['height']);
		imagecopyresampled($this->thumb,$this->original,0,0,0,0,$dimensions['width'],$dimensions['height'],$this->getWidth(),$this->getHeight());
		return $this;
	}

	function populateImageInfo() {
		$img_info = getimagesize($this->originalPath);
		switch($img_info[2]) {
			case IMAGETYPE_JPEG:
				$type = IMAGETYPE_JPEG;
				$this->original = imagecreatefromjpeg($this->originalPath);
				break;
			case IMAGETYPE_GIF:
				$type = IMAGETYPE_GIF;
				$this->original = imagecreatefromgif($this->originalPath);
				break;
			case IMAGETYPE_PNG:
				$type = IMAGETYPE_PNG;
				$this->original = imagecreatefrompng($this->originalPath);
				break;
		}
		$this->type = $type;
		$this->outputType = $type;
		$this->originalDimensions = array('width'=>$img_info[0],'height'=>$img_info[1]);
	}

	function getThumb($filename = NULL) {
		switch($this->outputType) {
			case IMAGETYPE_JPEG:
				imagejpeg($this->thumb,$filename,$this->quality);
				break;
			case IMAGETYPE_GIF:
				if(is_null($filename)) {
					imagegif($this->thumb);
				} else {
					imagegif($this->thumb,$filename);
				}
				break;
			case IMAGETYPE_PNG:
				imagepng($this->thumb,$filename);
				break;
		}
	}

	function destroy() {
		if(is_resource($this->original)) imagedestroy($this->original);
		if(is_resource($this->thumb)) imagedestroy($this->thumb);
	}
}
?>
