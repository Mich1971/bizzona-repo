<?php

define('ROOT', '/home/www'); // Path to document root (exp. /home/site.com/www) without end-slash
define('FILE_FOLDER', '/files/');
define('PATH', ROOT.FILE_FOLDER);

define('SERVER', $_SERVER['HTTP_HOST']);
define('APATH', $_SERVER['HTTP_HOST'].'/files/');

define('RESIZE', 80); // copy resize images

// Base settings

define('BASEHOST', 'localhost');
define('BASEUSER', 'root');
define('BASEPASS', '');
define('DBNAME', 'redactor');

define('T_', ''); # DB table prefix

// base connecting ...
$db = mysql_connect(BASEHOST, BASEUSER, BASEPASS);
if (!$db)
{
	print ("Datebase connection failed.");
	exit();
}

if (!mysql_select_db(DBNAME))
{
	print ("Datebase select failed.");
	exit();
}

mysql_query ("SET NAMES 'utf8'");

?>