<?php

    interface  SellAdapter {
    	
    	function ConvertData(); 
    	function CallData();
    	
    }

    final class HelpAdapter {
    	
    	static public function unixToMySQL($timestamp) {
 
    		return date('Y-m-d H:i:s', $timestamp);
    		
    	}
    	
    }
?>