<?php
  include_once("class.db.inc");  

  define("NEW_BUYER", 0);
  define("OPEN_BUYER", 1);
  define("CLOSE_BUYER", 2);
  define("LOOKED_BUYER", 3);

  class Buyer_Lite {
     var $db = "";
     var $id = "";
	 var $datecreate = "";
     var $duration = "";
     var $name = "";
     var $cost_start = "";
     var $cost_stop = "";
     var $regionText = "";
     var $typeBizID = "";
     var $subTypeBizID = "";
     var $description = "";
     var $FML = "";
     var $phone = "";
     var $email = "";
     var $statusID = "";
     var $age = "";
     var $otherinfo = "";
     var $realty = "";
     var $currencyID = "";
     var $part = "";
     var $license = "";
     var $payback = "";
     var $regionID = "";
     var $subRegionID = "";
     var $qView = "";
     var $subscribe_status_id = "";
     var $sendsql = "";
     var $districtID = "";
     var $metroID = "";
     var $pay_summ = "";
     var $pay_status = "";
     var $ip = "";
     var $subtitle = "";
     var $brokerId = "";
     
	 var $offset = 0;
	 var $rowCount = 0;
     
	 var $array_blogSubscribeStatusId = array();

     function Buyer_Lite() {
       global $DATABASE;
       $this->InitDB();
     } 

     function InitDB() {
       global $DATABASE;
       $this->db = new DB($DATABASE["HOSTNAME"], $DATABASE["DATABASENAME"], $DATABASE["DATABASEUSER"], $DATABASE["DATABASEPASSWORD"]);
     }

     function Close() {
     	$this->db->Close();
     }
     
     function CountBuyer() {
     	$sql = 'select count(*) as \'count\' from buyer where statusID in (1);';
		$result = $this->db->Select($sql);
		$row = mysql_fetch_row($result);
		return $row[0];     	
     }
     
     function DetailsCountSelect($data, $sqlDisplay = false) {
     	
       $where = " where 1 ";
       
       if (isset($data["StatusID"])) {
         if ($data["StatusID"] == "-1") {
           $where .= " and buyer.statusID in ('1') ";
         } else {
           $where .= " and buyer.statusID=".intval($data["StatusID"])." ";
         }
       } 
       
	   if (isset($data["subCityId"]) &&  intval($data["subCityId"]) > 0) {
		 $where .= " and subRegionID =".intval($data["subCityId"])." ";
	   } else if(isset($data["cityId"]) && intval($data["cityId"]) > 0) {
	   	 $where .= " and buyer.regionID =".intval($data["cityId"])." ";  	
	   }

 	   if (isset($data["subCatId"]) && intval($data["subCatId"]) > 0) {
		 $where .= " and buyer.subTypeBizID=".intval($data["subCatId"])." ";
	   } else if (isset($data["catId"]) && intval($data["catId"]) > 0) {
		 $where .= " and buyer.typeBizID=".intval($data["catId"])." ";
	   }
	   
	   $where .= " and statusID in ('1') ";

	   $useDistrict = (isset($data["districtId"]) && intval($data["districtId"]) > 0 ? true : false); 
	   
	   if ($useDistrict) {
		 $where .= " and buyerdistrict.buyerID=buyer.id and buyerdistrict.districtID =".intval($data["districtId"])." ";
	   }	   
	   

       $sql = "Select count(buyer.id) as 'count' from buyer ".($useDistrict ? ", buyerdistrict " : "")."  ".$where."  ";

       
       if ($sqlDisplay) {
         echo $sql."<br><br>";
       } 

       $result = $this->db->Select($sql);

       while ($row = mysql_fetch_object($result)) {
       	 return $row->count;
       }	
       
     }
     
     function  DetailsSelect($data, $sqlDisplay = false) {

       $limit = "";
       $where = " where 1 ";
       $order = " order  by ".$data["sort"]." desc";

       
       if (isset($data["StatusID"])) {
         if ($data["StatusID"] == "-1") {
           $where .= " and buyer.statusID in ('1') ";
         } else {
           $where .= " and buyer.statusID=".intval($data["StatusID"])." ";
         }
       } 
       
	   if (isset($data["subCityId"]) &&  intval($data["subCityId"]) > 0) {
		 $where .= " and subRegionID =".intval($data["subCityId"])." ";
	   } else if(isset($data["cityId"]) && intval($data["cityId"]) > 0) {
	   	 $where .= " and buyer.regionID =".intval($data["cityId"])." ";  	
	   }

 	   if (isset($data["subCatId"]) && intval($data["subCatId"]) > 0) {
		 $where .= " and buyer.subTypeBizID=".intval($data["subCatId"])." ";
	   } else if (isset($data["catId"]) && intval($data["catId"]) > 0) {
		 $where .= " and buyer.typeBizID=".intval($data["catId"])." ";
	   }
	   
	   $useDistrict = (isset($data["districtId"]) && intval($data["districtId"]) > 0 ? true : false); 
	   
	   if ($useDistrict) {
		 $where .= " and buyerdistrict.buyerID=buyer.id and buyerdistrict.districtID =".intval($data["districtId"])." ";
	   }	   
	   
       if ($data["offset"] === 0 && $data["rowCount"] === 0) {
       } else {
       	
			if(intval($data["rowCount"]) > 20) { $data["rowCount"] = 5; }
			if(intval($data["offset"]) > 50000) { $data["offset"] = 0; }       		
			
         	$limit = " limit ".intval($data["offset"]).",".$data["rowCount"]."";
       } 


       $sql = "Select buyer.*,  SubCategory.CategoryID AS 'SubCategoryID', SubCategory.icon AS 'SubCategoryIcon', Category.icon AS 'CategoryIcon' from buyer  LEFT JOIN SubCategory ON ( SubCategory.ID = buyer.subTypeBizID ) LEFT JOIN Category ON ( Category.ID = buyer.typeBizID ) ".($useDistrict ? ", buyerdistrict " : "")."  ".$where." ".$order." ".$limit;

       if ($sqlDisplay) {
         echo $sql."<br><br>";
       } 

       $result = $this->db->Select($sql);

       $array = Array();
       while ($row = mysql_fetch_object($result)) {
         $obj = new stdclass();

         $obj->id = $row->id;

		 if(strlen($row->Icon) > 3) {
			$obj->CategoryIcon = $row->Icon;
		 } else if(strlen($row->SubCategoryIcon)  > 0) {
            $obj->CategoryIcon = $row->SubCategoryIcon;
         } else {
            $obj->CategoryIcon = $row->CategoryIcon;
         } 
         
         
         $obj->name = $row->name;
         $obj->namelat = ruslat_for_name_biz($row->name);
		 $obj->duration = $row->duration;						     
		 $obj->cost_start = trim(strrev(chunk_split (strrev($row->cost_start), 3,' ')));
		 $obj->cost_stop = trim(strrev(chunk_split (strrev($row->cost_stop), 3,' ')));
		 $obj->regionText = $row->regionText;
		 $obj->regionID = $row->regionID;
		 $obj->subRegionID = $row->subRegionID;
		 $obj->typeBizID = $row->typeBizID;
		 $obj->subTypeBizID = $row->subTypeBizID;
		 $obj->description = $row->description;
		 $obj->FML = $row->FML;
		 $obj->phone = $row->phone;
		 $obj->email = $row->email;
		 $obj->statusID = $row->statusID;
		 $obj->age = $row->age;
		 $obj->otherinfo = $row->otherinfo;
		 $obj->realty = $row->realty;
		 $obj->currencyID = $row->currencyID;
		 $obj->part = $row->part;
		 $obj->license = $row->license;
		 $obj->payback = $row->payback;
		 $obj->datecreate = strftime("%d %B %Y",  strtotime($row->datecreate));
		 $obj->dateclose = date("d F Y", strtotime($row->datecreate) + 3600*24*30*$row->duration);
		 $obj->qView = $row->qView;
		 $obj->PublicNotify = $row->PublicNotify;
		 $obj->subscribe_status_id = $row->subscribe_status_id;
		 $obj->pay_summ = $row->pay_summ;
		 $obj->pay_status = $row->pay_status;
		 $obj->ip = $row->ip;
		 $obj->defaultUserCountry = $row->defaultUserCountry;
		 $obj->subtitle = $row->subtitle;
		 
		$obj->quicksearchurl = "http://www.bizzona.ru/detailssearchb.php?cityId=".$row->regionID."&subCityId=".$row->subRegionID."&catId=".$row->typeBizID."&subCatId=".$row->subTypeBizID."";		 		 
		 
         $array[] = $obj;
       }
       return $array;

     }     
     
     function Select($data, $sqlDisplay = false) {

       $limit = "";
       $where = " where 1 ";
       $order = " order  by ".$data["sort"]." desc";

       
       if (isset($data["countyId"])) {
			if(strlen($data["countyId"]) > 0) {
	 			$list_country  = explode(",", $data["countyId"]);
	 			while (list($k_country, $v_country) = each($list_country)) {
	 				$list_country[$k_country] = intval($v_country);
	 			}
	 			reset($list_country);
	 			if(sizeof($list_country) <= 0) {
	 				$list_country  = 0;
	 			} else {
	 				$list_country = implode(",", $list_country);
	 			}				
				$where .= " and buyer.regionID in (".$list_country.") ";
			}
       }
       
       if (isset($data["StatusID"])) {
         if ($data["StatusID"] == "-1") {
           $where .= " and buyer.statusID in (1) ";
         } else {
           $where .= " and buyer.statusID=".intval($data["StatusID"])." ";
         }
       } 
       
       if (isset($data["typeBizID"])) {
         $where .= " and buyer.typeBizID=".intval($data["typeBizID"])." ";
       } 

       if (isset($data["regionID"])) {
         $where .= " and buyer.regionID =".intval($data["regionID"])." ";
       } 
       
	   if (isset($data["subRegionID"])) {
		 $where .= " and subRegionID =".intval($data["subRegionID"])." or regionID=".intval($data["subRegionID"])." ";
	   }
       
       
       if (isset($data["SubCategoryID"]) && intval($data["SubCategoryID"]) > 0) {
         $where .= " and buyer.subTypeBizID=".intval($data["SubCategoryID"])." ";
       } 
       
       if(isset($data['aCategoryID'])) {
         $where .= " and  buyer.subTypeBizID in (".implode(',', $data['aCategoryID']).") ";
       }
       
       if(isset($data['aCityID'])) {
         $where .= " and  buyer.regionID in (".implode(',', $data['aCityID']).") ";
       }       
       
       
		if(intval($data["startcostf"]) > 0 && intval($data["stopcostf"]) > 0) {
			
			$where .= ' and  ( coststartseach >= '.intval($data["startcostf"]).'   and coststopseach <= '.intval($data["stopcostf"]).'  ) ';
			
			
		} else if (intval($data["startcostf"]) > 0 && intval($data["stopcostf"]) <= 0) {
			
			$where .= ' coststartseach >= '.intval($data["startcostf"]).'  ';
			
		} else if (intval($data["startcostf"]) <= 0 && intval($data["stopcostf"]) > 0) {
			
			$where .= ' and  coststopseach <=  '.intval($data["stopcostf"]).'  ';
			
		}
       
       
       
       if ($data["offset"] === 0 && $data["rowCount"] === 0) {
       } else {
       	
			if(intval($data["rowCount"]) > 20) { $data["rowCount"] = 5; }
			if(intval($data["offset"]) > 50000) { $data["offset"] = 0; }        	
       	
         	$limit = " limit ".intval($data["offset"]).",".$data["rowCount"]."";
       } 


       $sql = "Select buyer.*,  SubCategory.CategoryID AS 'SubCategoryID', SubCategory.icon AS 'SubCategoryIcon', Category.icon AS 'CategoryIcon' from buyer  LEFT JOIN SubCategory ON ( SubCategory.ID = buyer.subTypeBizID ) LEFT JOIN Category ON ( Category.ID = buyer.typeBizID )  ".$where." ".$order." ".$limit;

       //echo $sql;
       
       if ($sqlDisplay) {
         echo $sql."<br><br>";
       } 

       $result = $this->db->Select($sql);

       $array = Array();
       while ($row = mysql_fetch_object($result)) {
         $obj = new stdclass();

         $obj->id = $row->id;

		 if(strlen($row->Icon) > 3) {
			$obj->CategoryIcon = $row->Icon;
		 } else if(strlen($row->SubCategoryIcon)  > 0) {
            $obj->CategoryIcon = $row->SubCategoryIcon;
         } else {
            $obj->CategoryIcon = $row->CategoryIcon;
         } 
         
         
         $obj->name = $row->name;
         $obj->namelat = ruslat_for_name_biz($row->name);
		 $obj->duration = $row->duration;						     
		 $obj->cost_start = trim(strrev(chunk_split (strrev($row->cost_start), 3,' ')));
		 $obj->cost_stop = trim(strrev(chunk_split (strrev($row->cost_stop), 3,' ')));
		 $obj->regionText = $row->regionText;
		 $obj->regionID = $row->regionID;
		 $obj->subRegionID = $row->subRegionID;
		 $obj->typeBizID = $row->typeBizID;
		 $obj->subTypeBizID = $row->subTypeBizID;
		 $obj->description = $row->description;
		 $obj->FML = $row->FML;
		 $obj->phone = $row->phone;
		 $obj->email = $row->email;
		 $obj->statusID = $row->statusID;
		 $obj->age = $row->age;
		 $obj->otherinfo = $row->otherinfo;
		 $obj->realty = $row->realty;
		 $obj->currencyID = $row->currencyID;
		 $obj->part = $row->part;
		 $obj->license = $row->license;
		 $obj->payback = $row->payback;
		 $obj->datecreate = strftime("%d %B %Y",  strtotime($row->datecreate));
		 $obj->dateclose = date("d F Y", strtotime($row->datecreate) + 3600*24*30*$row->duration);
		 $obj->qView = $row->qView;
		 $obj->PublicNotify = $row->PublicNotify;
		 $obj->subscribe_status_id = $row->subscribe_status_id;
		 $obj->pay_summ = $row->pay_summ;
		 $obj->pay_status = $row->pay_status;
		 $obj->ip = $row->ip;
		 $obj->defaultUserCountry = $row->defaultUserCountry;
		 $obj->subtitle = $row->subtitle;
		 $obj->partnerid = $row->partnerid;

		 $obj->quicksearchurl = "http://www.bizzona.ru/detailssearchb.php?cityId=".$row->regionID."&subCityId=".$row->subRegionID."&catId=".$row->typeBizID."&subCatId=".$row->subTypeBizID."";		 
		 
         $array[] = $obj;
       }
       return $array;

     }

     function GetItem($data) {

       $sql = "Select buyer.*, SubCategory.seoname  from buyer LEFT JOIN SubCategory ON ( SubCategory.ID = buyer.subTypeBizID )  where buyer.id=".trim($data["ID"]).";";
       $this->sendsql = $sql;

       $result = $this->db->Select($sql);

       $obj = new stdclass();

       while ($row = mysql_fetch_object($result)) {

         $this->id = $obj->id = $row->id;
         $this->datecreate = $obj->datecreate = strftime("%d %B %Y",  strtotime($row->datecreate));
         $this->name = $obj->name = $row->name;
         $obj->namelat = ruslat_for_name_biz($row->name);
		 $this->duration = $obj->duration = $row->duration;						     
		 $this->cost_start = $obj->cost_start = trim(strrev(chunk_split (strrev($row->cost_start), 3,' ')));
		 $this->cost_stop = $obj->cost_stop = trim(strrev(chunk_split (strrev($row->cost_stop), 3,' ')));
		 $this->ccost_start = $obj->cost_start;
		 $this->ccost_stop = $obj->cost_stop;
		 $this->regionText = $obj->regionText = $row->regionText;
		 $this->regionID = $obj->regionID = $row->regionID;
		 $this->subRegionID = $obj->subRegionID = $row->subRegionID;
		 $this->typeBizID = $obj->typeBizID = $row->typeBizID;
		 $this->subTypeBizID = $obj->subTypeBizID = $row->subTypeBizID;
		 $this->description = $obj->description = $row->description;
		 $this->FML = $obj->FML = $row->FML;
		 $this->phone = $obj->phone = $row->phone;
		 $this->email = $obj->email = $row->email;
		 $this->statusID = $obj->statusID = $row->statusID;
		 $this->age = $obj->age = $row->age;
		 $this->otherinfo = $obj->otherinfo = $row->otherinfo;
		 $this->realty = $obj->realty = $row->realty;
		 $this->currencyID = $obj->currencyID = $row->currencyID;
		 $this->part = $obj->part = $row->part;
		 $this->license = $obj->license = $row->license;
		 $this->payback = $obj->payback = $row->payback;
		 $this->qView = $obj->qView = $row->qView;
		 $this->RepeatNotify = $obj->RepeatNotify = $row->RepeatNotify;
		 $this->PublicNotify = $obj->PublicNotify = $row->PublicNotify;
		 $this->seoname = $obj->seoname = $row->seoname;
		 $this->subscribe_status_id = $obj->subscribe_status_id = $row->subscribe_status_id;
		 $this->pay_status = $obj->pay_status = $row->pay_status;
		 $this->pay_summ = $obj->pay_summ = $row->pay_summ;
		 $this->ip = $obj->ip = $row->ip;
		 $this->brokerId = $obj->brokerId = $row->brokerId;
		 $this->subtitle = $obj->subtitle = $row->subtitle;
         
       } 
       return $obj;

     }

	 function GetResidence() {
		$sql = "select regionID, subRegionID from buyer where id=".$this->id."";
		$result = $this->db->Select($sql);
		$obj = new stdclass();
     	
		while ($row = mysql_fetch_object($result)) {
			$obj->regionID = $row->regionID;
			$obj->subRegionID = $row->subRegionID;
		}
		return $obj;
	 }

     function GetConstGUID($data) {

       $sql = "Select * from Sell where constGUID='".trim($data["constGUID"])."';";

       $result = $this->db->Select($sql);

       $obj = new stdclass();

       while ($row = mysql_fetch_object($result)) {
         $obj->ID = $row->ID;
         $obj->NameBizID = $row->NameBizID; 
       } 
       return $obj;

     }

     function GetRegion() {
     	$sql = "select regionID from buyer where id=".$this->id."";
     	$result = $this->db->Select($sql);
     	while ($row = mysql_fetch_object($result)) {
     		return $row->regionID;
     	}
     	
     }
     
     function IncrementQView($data) {
        $sql = "update buyer set `qView`=`qView`+1 where ID=".trim($data["ID"]).";";
        $result = $this->db->Select($sql);
     }

     function CountStatus($StatusID) {
	  $sql = "select count(buyer.ID) as 'c' from buyer where buyer.statusID=".intval($StatusID)."; ";
          $result = $this->db->Select($sql);
          while ($row = mysql_fetch_object($result)) {
               return $row->c;
	  }
          return 0;
	
     }  

     function CountSellPeriod($Period = 0) {
		$array = array();
     	switch ($Period) {
     		case 0: {
     			$sql = "select count(DATE_FORMAT(`Sell`.`DateStart`, '%Y-%m')) as 'count',
     			               DATE_FORMAT(`Sell`.`DateStart`, '%M %Y') as 'DateStart' 
     			                        from Sell 
     			                        group by DATE_FORMAT(`Sell`.`DateStart`, '%Y-%m'); ";
     			$result = $this->db->Select($sql);
     			
     			while ($row = mysql_fetch_object($result)) {
					$obj = new stdclass();
					$obj->Count = $row->count;
					$obj->DateStart = $row->DateStart;
					$array[] = $obj;
     			}

     		} break;
     		case 1: {
     			echo "weeks";
     		} break;
     		case 2: {
     			echo "days";
     		} break;
     		default: {
     			echo "unknow period";
     		} break;
     			
     	}
     	return $array;
     }
     
     
     function CountSellByStatus($Period = 0, $Status = 10) {
		$array = array();
     	switch ($Period) {
     		case 0: {
     			$sql = "select count(DATE_FORMAT(`Sell`.`DateStart`, '%Y-%m')) as 'count',
     			               DATE_FORMAT(`Sell`.`DateStart`, '%M %Y') as 'DateStart' 
     			                        from Sell 
     			                          where 1 
     			                             and 
     			                         StatusID = ".$Status."    
     			                        group by DATE_FORMAT(`Sell`.`DateStart`, '%Y-%m'); ";
     			$result = $this->db->Select($sql);
     			
     			while ($row = mysql_fetch_object($result)) {
					$obj = new stdclass();
					$obj->Count = $row->count;
					$obj->DateStart = $row->DateStart;
					$array[] = $obj;
     			}

     		} break;
     		case 1: {
     			echo "weeks";
     		} break;
     		case 2: {
     			echo "days";
     		} break;
     		default: {
     			echo "unknow period";
     		} break;
     			
     	}
     	return $array;
     }     
     

     function SelectSubCategory() {
     	
     	$sql = "SELECT count( `buyer`.`typeBizID` ) AS 'count', `SubCategory`.`ID`, `SubCategory`.`name`,  `SubCategory`.`seoname`
					FROM `buyer` , `SubCategory` 
						WHERE 1 
							AND `buyer`.`typeBizID` =".$this->typeBizID."
							AND `SubCategory`.`ID` = `buyer`.`subTypeBizID`
							AND `buyer`.`statusID` 
							IN ('1')
								GROUP BY `buyer`.`subTypeBizID` order by `SubCategory`.`ordernum` desc";
     	
     	$this->sendsql = $sql;
     	
		$result = $this->db->Select($sql);

		$array = Array();
		while ($row = mysql_fetch_object($result)) {
			$obj = new stdclass();
			$obj->SubCategoryID = $row->ID;
			$obj->Name = $row->name;
			$obj->SeoName = $row->seoname;
			$obj->Count = $row->count;
			$array[] = $obj;
		}
		
		return $array;
     }
     
    function CountSellByTarif($Period = 0, $PeriodID = 1) {
		$array = array();
     	switch ($Period) {
     		case 0: {
     			$sql = "select count(DATE_FORMAT(`Sell`.`DateStart`, '%Y-%m')) as 'count',
     			               DATE_FORMAT(`Sell`.`DateStart`, '%M %Y') as 'DateStart' 
     			                        from Sell 
     			                          where 1 
     			                             and 
     			                         TarifID = ".$PeriodID."    
     			                        group by DATE_FORMAT(`Sell`.`DateStart`, '%Y-%m'); ";
     			$result = $this->db->Select($sql);
     			
     			while ($row = mysql_fetch_object($result)) {
					$obj = new stdclass();
					$obj->Count = $row->count;
					$obj->DateStart = $row->DateStart;
					$array[] = $obj;
     			}

     		} break;
     		case 1: {
     			echo "weeks";
     		} break;
     		case 2: {
     			echo "days";
     		} break;
     		default: {
     			echo "unknow period";
     		} break;
     			
     	}
     	return $array;
     }          

     function CountSelectDistrict() {

			$this->db->Sql("set @districtID = ".(strlen($this->districtID) <=0 ? "null" :  $this->districtID)."");
			$this->db->Sql("set @CategoryID = ".(strlen($this->typeBizID) <=0 ? "null" :  $this->typeBizID)."");
			$this->db->Sql("set @SubCategoryID = ".(strlen($this->subTypeBizID) <=0 ? "null" :  $this->subTypeBizID)."");

			
			$sql = "SELECT COUNT(`buyer`.`id`) as 'Count'  
										FROM 	`buyer` 
												LEFT JOIN SubCategory ON ( SubCategory.ID = buyer.subTypeBizID  ) 
												LEFT JOIN Category ON ( Category.ID =  buyer.typeBizID),
			     								`buyerdistrict`
						WHERE 	1 
								and 
							(@districtID is null or `buyerdistrict`.`districtID` = @districtID) 
								and 
							(@CategoryID is null or buyer.typeBizID = @CategoryID) 
								and 
							(@SubCategoryID is null or buyer.subTypeBizID = @SubCategoryID)
								and 
							`buyerdistrict`.`buyerID` = `buyer`.`id`
								and
							`buyer`.`statusID` in (1);";
			$this->sendsql = $sql;
        					
			$result = $this->db->Sql($sql);
			while ($row = mysql_fetch_object($result)) {
				
				return $row->Count;
			}
			return 0;

		}

		function CountSelectTarget() {

			$this->db->Sql("set @districtID = ".(strlen($this->districtID) <=0 ? "null" :  $this->districtID)."");
			$this->db->Sql("set @metroID = ".(strlen($this->metroID) <=0 ? "null" :  $this->metroID)."");
			$this->db->Sql("set @CategoryID = ".(strlen($this->typeBizID) <=0 ? "null" :  $this->typeBizID)."");
			$this->db->Sql("set @SubCategoryID = ".(strlen($this->subTypeBizID) <=0 ? "null" :  $this->subTypeBizID)."");

			
			$sql = "SELECT COUNT(`buyer`.`id`) as 'Count'  
										FROM 	`buyer` 
												LEFT JOIN SubCategory ON ( SubCategory.ID = buyer.subTypeBizID  ) 
												LEFT JOIN Category ON ( Category.ID =  buyer.typeBizID),
			     								`buyermetro`
						WHERE 	1 
								and 
							(@metroID is null or `buyermetro`.`metroID` = @metroID) 
								and 
							(@CategoryID is null or buyer.typeBizID = @CategoryID) 
								and 
							(@SubCategoryID is null or buyer.subTypeBizID = @SubCategoryID)
								and 
							`buyermetro`.`buyerID` = `buyer`.`id`
								and
							`buyer`.`statusID` in (1);";
			$this->sendsql = $sql;
        					
			$result = $this->db->Sql($sql);
			while ($row = mysql_fetch_object($result)) {
				
				return $row->Count;
			}
			return 0;

		}

		function SelectTarget() {
			
			$this->db->Sql("set @districtID = ".(strlen($this->districtID) <=0 ? "null" :  $this->districtID)."");
			$this->db->Sql("set @metroID = ".(strlen($this->metroID) <=0 ? "null" :  $this->metroID)."");
			$this->db->Sql("set @CategoryID = ".(strlen($this->typeBizID) <=0 ? "null" :  $this->typeBizID)."");
			$this->db->Sql("set @SubCategoryID = ".(strlen($this->subTypeBizID) <=0 ? "null" :  $this->subTypeBizID)."");

			$sql = "SELECT	 buyer.*,  
							SubCategory.CategoryID AS 'SubCategoryID',
							SubCategory.icon AS 'SubCategoryIcon',
							Category.icon AS 'CategoryIcon'  
										FROM 	`buyer` 
												LEFT JOIN SubCategory ON ( SubCategory.ID = buyer.subTypeBizID ) 
												LEFT JOIN Category ON ( Category.ID = buyer.typeBizID ),
			     								`buyermetro`
						WHERE 	1 
								and 
							(@metroID is null or `buyermetro`.`metroID` = @metroID) 
								and 
							(@CategoryID is null or buyer.typeBizID = @CategoryID) 
								and 
							(@SubCategoryID is null or buyer.subTypeBizID = @SubCategoryID)
								and 
							`buyermetro`.`buyerID` = `buyer`.`id`
								and
							`buyer`.`statusID` in (1);";
			$this->sendsql = $sql;

			$result = $this->db->Select($sql);

			$array = Array();
			while ($row = mysql_fetch_object($result)) {
				$obj = new stdclass();
				$obj->id = $row->id;
         		if(strlen($row->SubCategoryIcon)  > 0)
				{
					$obj->CategoryIcon = $row->SubCategoryIcon;
				} else {
					$obj->CategoryIcon = $row->CategoryIcon;
				} 
         
         		$obj->name = $row->name;
         		$obj->namelat = ruslat_for_name_biz($row->name);
				$obj->duration = $row->duration;
				$obj->cost_start = trim(strrev(chunk_split (strrev($row->cost_start), 3,' ')));
				$obj->cost_stop = trim(strrev(chunk_split (strrev($row->cost_stop), 3,' ')));
				$obj->regionText = $row->regionText;
				$obj->regionID = $row->regionID;
				$obj->typeBizID = $row->typeBizID;
				$obj->subTypeBizID = $row->subTypeBizID;
				$obj->description = $row->description;
				$obj->FML = $row->FML;
				$obj->phone = $row->phone;
				$obj->email = $row->email;
				$obj->statusID = $row->statusID;
				$obj->age = $row->age;
				$obj->otherinfo = $row->otherinfo;
				$obj->realty = $row->realty;
				$obj->currencyID = $row->currencyID;
				$obj->part = $row->part;
				$obj->license = $row->license;
				$obj->payback = $row->payback;
				$obj->datecreate = strftime("%d %B %Y",  strtotime($row->datecreate));
				$obj->dateclose = date("d F Y", strtotime($row->datacreate) + 3600*24*30*$row->duration);
				$obj->qView = $row->qView;
				$obj->PublicNotify = $row->PublicNotify;
				$obj->subscribe_status_id = $row->subscribe_status_id;
				$obj->subtitle = $row->subtitle;
				$array[] = $obj;
			}
			return $array;
		}
		
		function SelectDistrict() {
			
			$limit = "";			
			if ($this->offset === 0 && $this->rowCount === 0) {
			} else {
				
				if(intval($this->rowCount) > 20) { $this->rowCount = 5; }
				if(intval($this->offset) > 50000) { $this->offset = 0; } 				
				
				$limit = " limit ".intval($this->offset).",".$this->rowCount."";
			} 
			
			
			$this->db->Sql("set @districtID = ".(strlen($this->districtID) <=0 ? "null" :  $this->districtID)."");
			$this->db->Sql("set @CategoryID = ".(strlen($this->typeBizID) <=0 ? "null" :  $this->typeBizID)."");
			$this->db->Sql("set @SubCategoryID = ".(strlen($this->subTypeBizID) <=0 ? "null" :  $this->subTypeBizID)."");

			$sql = "SELECT	 buyer.*,  
							SubCategory.CategoryID AS 'SubCategoryID',
							SubCategory.icon AS 'SubCategoryIcon',
							Category.icon AS 'CategoryIcon'  
										FROM 	`buyer` 
												LEFT JOIN SubCategory ON ( SubCategory.ID = buyer.subTypeBizID ) 
												LEFT JOIN Category ON ( Category.ID = buyer.typeBizID ),
			     								`buyerdistrict`
						WHERE 	1 
								and 
							(@districtID is null or `buyerdistrict`.`districtID` = @districtID) 
								and 
							(@CategoryID is null or buyer.typeBizID = @CategoryID) 
								and 
							(@SubCategoryID is null or buyer.subTypeBizID = @SubCategoryID)
								and 
							`buyerdistrict`.`buyerID` = `buyer`.`id`
								and
							`buyer`.`statusID` in (1) ".$limit.";";
			$this->sendsql = $sql;

			$result = $this->db->Select($sql);

			$array = Array();
			while ($row = mysql_fetch_object($result)) {
				$obj = new stdclass();
				$obj->id = $row->id;
         		if(strlen($row->SubCategoryIcon)  > 0)
				{
					$obj->CategoryIcon = $row->SubCategoryIcon;
				} else {
					$obj->CategoryIcon = $row->CategoryIcon;
				} 
         
         		$obj->name = $row->name;
         		$obj->namelat = ruslat_for_name_biz($row->name);
				$obj->duration = $row->duration;
				$obj->cost_start = trim(strrev(chunk_split (strrev($row->cost_start), 3,' ')));
				$obj->cost_stop = trim(strrev(chunk_split (strrev($row->cost_stop), 3,' ')));
				$obj->regionText = $row->regionText;
				$obj->regionID = $row->regionID;
				$obj->typeBizID = $row->typeBizID;
				$obj->subTypeBizID = $row->subTypeBizID;
				$obj->description = $row->description;
				$obj->FML = $row->FML;
				$obj->phone = $row->phone;
				$obj->email = $row->email;
				$obj->statusID = $row->statusID;
				$obj->age = $row->age;
				$obj->otherinfo = $row->otherinfo;
				$obj->realty = $row->realty;
				$obj->currencyID = $row->currencyID;
				$obj->part = $row->part;
				$obj->license = $row->license;
				$obj->payback = $row->payback;
				$obj->datecreate = strftime("%d %B %Y",  strtotime($row->datecreate));
				$obj->dateclose = date("d F Y", strtotime($row->datacreate) + 3600*24*30*$row->duration);
				$obj->qView = $row->qView;
				$obj->PublicNotify = $row->PublicNotify;
				$obj->subscribe_status_id = $row->subscribe_status_id;
				$obj->subtitle = $row->subtitle;
				$array[] = $obj;
			}
			return $array;
		}
		
		function GetListMetroByBuyer() {
			
			$sql = "select 
						buyermetro.metroID,
						Metro.name,
        				Metro.seobuy
							from 	buyermetro, 
									Metro 
										where buyermetro.buyerID=".$this->id." 
													and
				      					Metro.ID = buyermetro.metroID;";
			
			$this->sendsql = $sql;

			$result = $this->db->Select($sql);

			$array = Array();
			while ($row = mysql_fetch_object($result)) {
				$obj = new stdclass();
				$obj->metroID = $row->metroID;
				$obj->name = $row->name;
				$obj->seobuy = $row->seobuy;
				$array[] = $obj;
			}
			return $array;
			
		}

		function GetListDistrictByBuyer() {
			
			$sql = "select 
						buyerdistrict.districtID,
						District.name,
        				District.seobuy,
        				District.regionID
							from 	buyerdistrict, 
									District 
										where buyerdistrict.buyerID=".$this->id." 
													and
				      					District.ID = buyerdistrict.districtID;";
			
			$this->sendsql = $sql;

			$result = $this->db->Select($sql);

			$array = Array();
			while ($row = mysql_fetch_object($result)) {
				$obj = new stdclass();
				$obj->districtID = $row->districtID;
				$obj->regionID = $row->regionID;
				$obj->name = $row->name;
				$obj->seobuy = $row->seobuy;
				$array[] = $obj;
			}
			return $array;
			
		}
		
		function GetTitleForUserFriendlySelect() {
			
			$regionID = (intval($this->regionID) > 0 ? intval($this->regionID) : 0);
			$subcategoryID = (intval($this->subTypeBizID) > 0 ? intval($this->subTypeBizID) : 0);
			$metroID = 0;//(intval($this->metroID) > 0 ? intval($this->metroID) : 0);
			$districtID = 0;// (intval($this->districtID) > 0 ? intval($this->districtID) : 0);
			
			$sql = "select 1, (select region.title as 'RegionName' from region where region.ID=".$regionID.") as 'RegionName', (select Metro.name as 'MetroName' from Metro where Metro.ID=".$metroID.") as 'MetroName', (select SubCategory.name as 'SubCategoryName' from SubCategory where SubCategory.ID=".$subcategoryID.") as 'SubCategoryName', (select District.name as 'DistrictName' from District  where District.ID=".$districtID.") as 'DistrictName';";
			//echo $sql;
			$result = $this->db->Select($sql);			
			$obj = new stdclass();
			while ($row = mysql_fetch_object($result)) {
				$obj->DistrictName = $row->DistrictName;
				$obj->MetroName = $row->MetroName;
				$obj->RegionName = $row->RegionName;
				$obj->SubCategoryName = $row->SubCategoryName;
			}

			$str = " ����� ������";
			if(strlen($obj->SubCategoryName) > 0) {
				$str .= " ".strtolower($obj->SubCategoryName);
			}
			
			if(strlen($obj->MetroName) > 0) {
				$str .= ", �. ".$obj->MetroName;
			} else if (strlen($obj->DistrictName) > 0) {
				$str .= ", ".$obj->DistrictName;
			}
			
			if(strlen($obj->RegionName) > 0) {
				$str .= " - ".convert_cyr_string($obj->RegionName,"koi8-r", "windows-1251");
			}			
			
			return $str;
		}
		
		function SelectUserFriendlySellByDistrict() {
			
			//$sql = "select count(District.ID) as 'count', region.ID as 'CityID' , SubCategory.ID as 'SubCategoryID', SubCategory.Name as 'SubCategoryName', region.title as 'RegionTitle',   District.ID as 'districtId', District.name as 'DistrictName'  from buyer, buyerdistrict, District, region, SubCategory  where 1 and buyer.statusID = 1 and (buyer.regionID = ".intval($this->regionID)." or buyer.subRegionID=".intval($this->regionID).") and buyerdistrict.buyerID=buyer.id and District.ID = buyerdistrict.districtID and region.ID = ".intval($this->regionID)." and SubCategory.ID = ".intval($this->subTypeBizID)."    group by District.ID;";
			$sql = "select count(District.ID) as 'count',  District.ID as 'districtId', region.ID as 'CityID', SubCategory.ID as 'SubCategoryID', SubCategory.Name as 'SubCategoryName', region.title as 'RegionTitle', District.name as 'DistrictName'  from District, buyerdistrict, buyer, region, SubCategory  where District.regionID=".intval($this->regionID)." and region.ID= District.regionID and buyerdistrict.districtID=District.ID and buyer.id = buyerdistrict.buyerId and buyer.statusID = 1 and buyer.subTypeBizID=".intval($this->subTypeBizID)." and SubCategory.ID=buyer.subTypeBizID group  by District.ID;";
			
			//echo $sql;
			
			
			$result = $this->db->Select($sql);

			$array = Array();
			while ($row = mysql_fetch_object($result)) {
				$obj = new stdclass();
				
				$obj->Count = $row->count;
				$obj->title = "".$row->SubCategoryName.", ".$row->DistrictName."";
				$obj->quicksearchurl = "http://www.bizzona.ru/detailssearchb.php?cityId=".$row->CityID."&subCatId=".$row->SubCategoryID."&districtId=".$row->districtId."";
				
				$array[] = $obj;
			}			
			
			return $array;			
			
		}

		function ChangeCustomerStatus($params) {
			
			$sql = " update buyer set statusID=".$params['statId']."  where GUID='".$params['guid']."' and id=".$params['Id']." limit 1;";
			$result = $this->db->Select($sql);
		}
			
	}
?>
