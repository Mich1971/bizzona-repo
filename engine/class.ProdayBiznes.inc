<?php
    include_once("interface.sellsdapter.inc");
    include_once("../engine/class.sell.inc");
    include_once("../engine/functions.inc"); 
    include_once("../engine/typographus.php"); 
    include_once("../engine/class.mappingobjectbroker.inc"); 	
    require_once("../engine/Cache/Lite.php");
    include_once($_SERVER['DOCUMENT_ROOT']."/engine/CreateSubDirImg.php");         

    class ProdayBiznes implements SellAdapter {
    	
        public $db = "";
        
    	public $_id;
        public $_DateUpdate;
        public $_Price;
        public $_ProfitPerMonth;
        public $_MaxProfitPerMonth;
        public $_MonthAveTurn;
        public $_MonthExpemse;
        public $_MaxMonthExpemse;
        public $_SumDebts;
        public $_WageFund;
        public $_RentCostId;
        public $_TimeInvest;
        public $_ListObject;
        public $_MatProperty;
        public $_CountEmpl;
        public $_CountManEmpl;
        public $_TermBiz;
        public $_ShareSale;
        public $_Contact;
        public $_offertype;
        public $_businesstype;
        public $_phone;
        public $_contact;
        public $_title;
        public $_description;
        public $_shortDescription;
        public $_web;
        public $_img1;
        public $_img2;
        public $_img3;
        public $_Obligation;
        public $_address;    	

        public $_email;
        public $_town;
        public $_orgform; 
        public $_reason;
        public $_torg;
        public $_fml;

        public $_pic = array();

        public $countProcessed = 0;
                
    	public $brokerId = 31; 
    	
    	public $_cCurency;
    	
    	public $url = "http://www.proday-biznes.ru/upload/bizzona.xml";
    	
    	private $array_prodaybiznes = array();
    	
    	public $town_maps = array(
    							 );
    	
    	function __construct() {
            global $DATABASE;
            $this->InitDB();
    	}

        function InitDB() {
            global $DATABASE;
            $this->db = new DB($DATABASE["HOSTNAME"], $DATABASE["DATABASENAME"], $DATABASE["DATABASEUSER"], $DATABASE["DATABASEPASSWORD"]);
        }    
        
    	public function Result() {
    		
    		return $this->array_prodaybiznes;
    		
    	}
    	
    	public $acCurency_maps  = array(
    									"���" => "rub",
    									"eur" => "eur", 
    									"usd" => "usd", 
                                                                        "" => "rub",
    									);
    	
    	public $aOrgform = array(
    								"���" => "1",
    								"��"  => "8",
    								"���" => "2",
    								"���" => "3"
    							);
		    									
    	
        private function ModerateContent($content) {

                return $content;

        } 
    	
        private function LoadImg($url, $pref = 'sell_') {

            $newname = uniqid($pref);
            $ext = substr($url, 1 + strrpos($url, "."));

            $path = '../simg/';

            $result_name = CreateSubDirImg($path).'/'.$newname.".".$ext;

            file_put_contents($result_name, file_get_contents($url));

            $result_name = str_replace($path, '', $result_name);        

            return $result_name;
        }                
                
                
    	public function ConvertData() {

            $data = array();

            $data["FML"] =  htmlspecialchars($this->_fml, ENT_QUOTES);
            $data["EmailID"] = htmlspecialchars($this->_email, ENT_QUOTES);
            $data["PhoneID"] = htmlspecialchars($this->_phone, ENT_QUOTES);
            $data["NameBizID"] = htmlspecialchars(ucfirst(strtolower($this->_title)), ENT_QUOTES);

            if(isset($this->aOrgform[$this->_orgform])) {
                    $data["BizFormID"] = $this->aOrgform[$this->_orgform];	
            } else {
                    $data["BizFormID"] = "";	
            }

            if(isset($this->town_maps[$this->_town])) {
                    $data["CityID"] = $this->town_maps[$this->_town]["cityId"];
                    $data["SubCityID"] = $this->town_maps[$this->_town]["subCityId"];
            } else {
                    $data["CityID"] = "";
                    $data["SubCityID"] = "";
            }


            $data["SiteID"] = htmlspecialchars($this->_address, ENT_QUOTES);
            $data["CostID"] = intval($this->_Price);

            if(isset($this->acCurency_maps[$this->_cCurency])) {
                    $data["cCostID"] = $this->acCurency_maps[$this->_cCurency];
            } else {
                    $data["cCostID"] = "rub";
            }

            $data["txtCostID"] = $this->_Price;
            $data["txtProfitPerMonthID"] = "";
            $data["ProfitPerMonthID"] = $this->_ProfitPerMonth;
            $data["cProfitPerMonthID"] = $data["cCostID"];
            $data["cMaxProfitPerMonthID"] = $this->_MaxProfitPerMonth;
            $data["TimeInvestID"] = htmlspecialchars(ucfirst(strtolower($this->_TimeInvest)), ENT_QUOTES);
            $data["ListObjectID"] = htmlspecialchars(ucfirst(strtolower($this->_ListObject)), ENT_QUOTES); 
            $data["ContactID"] = htmlspecialchars(ucfirst(strtolower($this->_Contact)), ENT_QUOTES); 
            $data["MatPropertyID"] = htmlspecialchars(ucfirst(strtolower($this->_MatProperty)), ENT_QUOTES); 
            $data["txtMonthAveTurnID"] = $data["cCostID"];
            $data["MonthAveTurnID"] = $this->_MonthAveTurn;
            $data["cMonthAveTurnID"] = $data["cCostID"];
            $data["cMaxMonthAveTurnID"] = "";
            $data["txtMonthExpemseID"] = "";
            $data["MonthExpemseID"] = $this->_MonthExpemse;
            $data["cMonthExpemseID"] = $data["cCostID"];
            $data["txtSumDebtsID"] = "";
            $data["SumDebtsID"] = $this->_SumDebts;
            $data["cSumDebtsID"] = $data["cCostID"];
            $data["txtWageFundID"] = "";
            $data["WageFundID"] = $this->_WageFund;
            $data["cWageFundID"] = $data["cCostID"];
            $data["ObligationID"] = htmlspecialchars(ucfirst(strtolower($this->_Obligation)), ENT_QUOTES);
            $data["CountEmplID"] = $this->_CountEmpl;
            $data["CountManEmplID"] = $this->_CountManEmpl;
            $data["TermBizID"] = $this->_TermBiz;


            $data["ShortDetailsID"] = htmlspecialchars(ucfirst($this->_shortDescription), ENT_QUOTES); 
            $data["DetailsID"] = htmlspecialchars(ucfirst($this->_description), ENT_QUOTES); 


            $data["FaxID"] = ""; 
            $data["ShareSaleID"] = htmlspecialchars(ucfirst(strtolower($this->_ShareSale)), ENT_QUOTES);
            $data["ReasonSellBizID"] = "";//$this->_reason;
            $data["txtReasonSellBizID"] = $this->_reason;
            $data["TarifID"] = ""; 
            $data["StatusID"] = "";
            $data["DataCreate"] = "";
            $data["DateStart"] = "";
            $data["TimeActive"] = "";

            $data["CategoryID"] =  (isset($this->category_maps[$this->_businesstype]) ? $this->category_maps[$this->_businesstype] : "");
            $data["SubCategoryID"] = "";
            $data["ConstGUID"] = "";
            $data["ip"] = "";
            $data["defaultUserCountry"] = "";
            $data["login"] = "";
            $data["password"] = "";
            $data["newspaper"] = "";
            $data["helpbroker"] = "";
            $data["youtubeURL"] = "";    		
            $data["brokerId"] = $this->brokerId;
            $data["ReasonSellBizID"] = htmlspecialchars($this->_reason, ENT_QUOTES);
            $data["AgreementCostID"] = $this->_torg;

            $data["ArendaCostID"] = intval($this->_RentCostId);
            $data["cArendaCostID"] = $data["cCostID"];

            if(count($this->_pic)) {

                if(isset($this->_pic[0])) {
                    $data["ImgID"] = $this->LoadImg(trim($this->_pic[0]));
                } else {
                    $data["ImgID"] = ''; 
                }

                if(isset($this->_pic[1])) {
                    $data["Img1ID"] = $this->LoadImg(trim($this->_pic[1])); 
                } else {
                    $data["Img1ID"] = '';
                }

                if(isset($this->_pic[2])) {
                    $data["Img2ID"] = $this->LoadImg(trim($this->_pic[2])); 
                } else {
                    $data["Img2ID"] = '';
                }

                if(isset($this->_pic[3])) { 
                    $data["Img3ID"] = $this->LoadImg(trim($this->_pic[3]));
                } else {
                    $data["Img3ID"] = '';
                }

                if(isset($this->_pic[4])) { 
                    $data["Img4ID"] = $this->LoadImg(trim($this->_pic[4]));
                } else {
                    $data["Img4ID"] = '';
                }            

            } else {

                $data["ImgID"] = ''; 
                $data["Img1ID"] = ''; 
                $data["Img2ID"] = ''; 
                $data["Img3ID"] = '';
                $data["Img4ID"] = '';
            }

            //up Typographus

            $typo = new Typographus();

            $data["NameBizID"] = $typo->process($data["NameBizID"]);
            $data["SiteID"] = $typo->process($data["SiteID"]);
            $data["ListObjectID"] = $typo->process($data["ListObjectID"]);
            $data["MatPropertyID"] = $typo->process($data["MatPropertyID"]);
            $data["ShortDetailsID"] = $typo->process($data["ShortDetailsID"]);
            $data["DetailsID"] = $typo->process($data["DetailsID"]);

            //down Typographus    		

            $data["source"] = "�����: ������ ������"; 
            
            return $data;
    	}
    	
    	public function CallData() {

            $xml = simplexml_load_file($this->url);
            $this->ReadDataCallBack($xml);
    		
    	}
    	
    	
        private function convertXmlObjToArr($obj, &$arr)  { 
            
            if(!is_object($obj)) throw new Exception("Bizzona.ru: ������ ������� ������ xml ����� �� �������� ������ �������");
			
            $children = $obj->children();  
            foreach ($children as $elementName => $node) 
            { 
                    $nextIdx = count($arr); 
                    $arr[$nextIdx] = array(); 
                    $arr[$nextIdx]['@name'] = strtolower((string)$elementName); 
                    $arr[$nextIdx]['@attributes'] = array(); 
                    $attributes = $node->attributes(); 
                    foreach ($attributes as $attributeName => $attributeValue) 
                    { 
                    $attribName = strtolower(trim((string)$attributeName)); 
                    $attribVal = trim((string)$attributeValue); 
                    $arr[$nextIdx]['@attributes'][$attribName] = $attribVal; 
                    } 
                    $text = (string)$node; 
                    $text = trim($text); 
                    if (strlen($text) > 0) 
                    { 
                    $arr[$nextIdx]['@text'] = $text; 
                    } 
                    $arr[$nextIdx]['@children'] = array(); 
                    $this->convertXmlObjToArr($node, $arr[$nextIdx]['@children']); 
            } 
            
            return; 
            
        }  		
		
        private function ReadDataCallBack($arr) {

            $res_arr = array();
            $this->convertXmlObjToArr($arr, $res_arr);	

            while (list($k, $v) = each($res_arr)) {

                $item = $res_arr[$k];

                $sub_item = $item["@children"];

                $_prodaybiznes = new ProdayBiznes(); 	

                while (list($k1, $v1) = each($sub_item)) {

                    $arr = $v1["@attributes"];	
                    
                    $name = $arr["name"];
                    $name = strtolower($name);

                    if($name == "id") {
                            $_prodaybiznes->_id = $v1["@text"];
                    }
                    if($name == "dateupdate") {
                            $_prodaybiznes->_DateUpdate = $v1["@text"];
                    }
                    if($name == "price") {
                            $_prodaybiznes->_Price = $v1["@text"];
                    }
                    if($name == "profitpermonth") {
                            $_prodaybiznes->_ProfitPerMonth = $v1["@text"];
                    }
                    if($name == "maxprofitpermonth") {
                            $_prodaybiznes->_MaxProfitPerMonth = $v1["@text"];
                    }					

                    if($name == "monthexpemse") {
                            $_prodaybiznes->_MonthExpemse = $v1["@text"];
                    }
                    if($name == "timeinvest") {
                            $_prodaybiznes->_TimeInvest = $v1["@text"];
                    }
                    if($name == "listobject") {
                            $_prodaybiznes->_ListObject = utf8_to_cp1251($v1["@text"]);
                    }
                    if($name == "matproperty") {
                            $_prodaybiznes->_MatProperty = utf8_to_cp1251($v1["@text"]);
                    }
                    if($name == "countempl") {
                            $_prodaybiznes->_CountEmpl = $v1["@text"];
                    }
                    if($name == "countmanempl") {
                            $_prodaybiznes->_CountManEmpl = $v1["@text"];
                    }
                    if($name == "countmanempl") {
                            $_prodaybiznes->_TermBiz = $v1["@text"];
                    }
                    if($name == "sharesale") {
                            $_prodaybiznes->_ShareSale = $v1["@text"];
                    }	
                    if($name == "categoryid") {
                            $_prodaybiznes->_businesstype = utf8_to_cp1251($v1["@text"]);
                    }
                    if($name == "phone") {
                            $_prodaybiznes->_phone = $v1["@text"];
                    } 
                    if($name == "title") {
                            $_prodaybiznes->_title = utf8_to_cp1251($v1["@text"]);
                    }
                    if($name == "description") {
                            $_prodaybiznes->_description = utf8_to_cp1251($v1["@text"]);
                    }
                    if($name == "shortdescription") {
                            $_prodaybiznes->_shortDescription = utf8_to_cp1251($v1["@text"]);
                    }					

                    if($name == "email") {
                            $_prodaybiznes->_email = $v1["@text"];
                    }

                    if($name == "town") {
                            $_prodaybiznes->_town = $v1["@text"];
                    }
                    if($name == "bizformid") {
                            $_prodaybiznes->_orgform = utf8_to_cp1251($v1["@text"]);
                    }
                    if($name == "reasonsellbizid") {
                            $_prodaybiznes->_reason = utf8_to_cp1251($v1["@text"]);
                    }

                    if($name == "contact") {
                            $_prodaybiznes->_fml = utf8_to_cp1251($v1["@text"]);
                    }					

                    if($name == "rentcostid") {
                            $_prodaybiznes->_RentCostId = $v1["@text"];
                    }
                    if($name == "town") {
                            $_prodaybiznes->_address = utf8_to_cp1251($v1["@text"]);
                    }
                    if($name == "wagefund") {
                            $_prodaybiznes->_WageFund = $v1["@text"];
                    }
                    if($name == "obligation") {
                            $_prodaybiznes->_Obligation = utf8_to_cp1251($v1["@text"]);
                    }
                    if($name == "torg") {
                            $_prodaybiznes->_torg = $v1["@text"];
                    }
                    if($name == "ccurency") {
                            $_prodaybiznes->_cCurency = utf8_to_cp1251($v1["@text"]);
                    }	  
                    if($name == "pic") {
                        $_prodaybiznes->_pic[] = $v1["@text"];
                    }                                        
                }

                $this->array_prodaybiznes[] = $_prodaybiznes;	

            }

        }
    	
    	public function ImportData() {
    		
            $this->CallData();
            $result = $this->Result();

            $sell = new Sell();	

            $iter = 0;

            while (list($k, $v) = each($result)) {

                if ($iter == 1) {
                        break;
                }

                $mappingobjectbroker = new MappingObjectBroker();

                $mappingobjectbroker->brokerObjectID = $v->_id;
                $mappingobjectbroker->brokerId = $this->brokerId;

                if (!$mappingobjectbroker->IsExistsBrokerObject()) {

                    $mappingobjectbroker->bizzonaObjectId = $this->InsertSell($v->ConvertData());
                    $mappingobjectbroker->brokerId = $this->brokerId;
                    $mappingobjectbroker->brokerDateObject = HelpAdapter::unixToMySQL(strtotime($v->_DateUpdate));
                    $mappingobjectbroker->ObjectTypeId = 1;
                    $mappingobjectbroker->Insert();

                    if($mappingobjectbroker->bizzonaObjectId > 0) {
                        $this->countProcessed += 1;
                    }

                    echo "<h1>OK</h1>";
                    $iter += 1;
                }
            }
			
    	}
    	
        private function InsertSell($data) {

            foreach ($data as $k => $v) { 
                $data[$k] = $this->ModerateContent($v);
            }

            $sql = " insert into Sell (`FML`, 
                                                                    `EmailID`, 
                                                                    `PhoneID`,
                                                                    `NameBizID`,
                                                                    `BizFormID`, 
                                                                    `CityID`,
                                                                    `subCityID`, 
                                                                    `SiteID`,
                                                                    `CostID`,
                                                                    `cCostID`,
                                                                    `txtCostID`,
                                                                    `txtProfitPerMonthID`,
                                                                    `ProfitPerMonthID`,
                                                                    `cProfitPerMonthID`,
                                                                    `cMaxProfitPerMonthID`,
                                                                    `TimeInvestID`, 
                                                                    `ListObjectID`, 
                                                                    `ContactID`,
                                                                    `MatPropertyID`,
                                                                    `txtMonthAveTurnID`,
                                                                    `MonthAveTurnID`,
                                                                    `cMonthAveTurnID`,
                                                                    `cMaxMonthAveTurnID`,
                                                                    `txtMonthExpemseID`, 
                                                                    `MonthExpemseID`, 
                                                                    `cMonthExpemseID`,
                                                                    `txtSumDebtsID`,
                                                                    `SumDebtsID`,
                                                                    `cSumDebtsID`, 
                                                                    `txtWageFundID`,
                                                                    `WageFundID`,
                                                                    `cWageFundID`,
                                                                    `ObligationID`,
                                                                    `CountEmplID`,
                                                                    `CountManEmplID`,
                                                                    `TermBizID`,
                                                                    `ShortDetailsID`, 
                                                                    `DetailsID`, 
                                                                    `ImgID`,
                                                                    `Img1ID`,
                                                                    `Img2ID`,
                                                                    `Img3ID`,
                                                                    `Img4ID`, 
                                                                    `pdf`, 
                                                                    `FaxID`, 
                                                                    `ShareSaleID`,
                                                                    `ReasonSellBizID`,
                                                                    `txtReasonSellBizID`,
                                                                    `TarifID`, 
                                                                    `StatusID`,
                                                                    `DataCreate`,
                                                                    `DateStart`,
                                                                    `TimeActive`,
                                                                    `CategoryID`,
                                                                    `SubCategoryID`,
                                                                    `ConstGUID`,
                                                                    `ip`,
                                                                    `defaultUserCountry`,
                                                                    `login`,
                                                                    `password`,
                                                                    `newspaper`,
                                                                    `helpbroker`,
                                                                    `youtubeURL`,
                                                                    `brokerId`,
                                                                    `source`)
                                                            values
                                                            ( 
                                                            '".$data["FML"]."',
                                                            '".$data["EmailID"]."',
                                                            '".$data["PhoneID"]."',
                                                            '".$data["NameBizID"]."',
                                                            '".$data["BizFormID"]."',
                                                            '".$data["CityID"]."',
                                                            '".$data["SubCityID"]."',
                                                            '".$data["SiteID"]."',
                                                            '".$data["CostID"]."',
                                                            '".$data["cCostID"]."',
                                                            '".$data["txtCostID"]."',
                                                            '".$data["txtProfitPerMonthID"]."',
                                                            '".$data["ProfitPerMonthID"]."',
                                                            '".$data["cProfitPerMonthID"]."',
                                                            '".$data["cProfitPerMonthID"]."',
                                                            '".$data["TimeInvestID"]."', 
                                                            '".$data["ListObjectID"]."',
                                                            '".$data["ContactID"]."',
                                                            '".$data["MatPropertyID"]."',
                                                            '".$data["txtMonthAveTurnID"]."',
                                                            '".$data["MonthAveTurnID"]."',
                                                            '".$data["cMonthAveTurnID"]."',
                                                            '".$data["cMonthAveTurnID"]."',
                                                            '".$data["txtMonthExpemseID"]."',
                                                            '".$data["MonthExpemseID"]."',
                                                            '".$data["cMonthExpemseID"]."',
                                                            '".$data["txtSumDebtsID"]."',
                                                            '".$data["SumDebtsID"]."',
                                                            '".$data["cSumDebtsID"]."',
                                                            '".$data["txtWageFundID"]."',
                                                            '".$data["WageFundID"]."',
                                                            '".$data["cWageFundID"]."',
                                                            '".$data["ObligationID"]."',
                                                            '".$data["CountEmplID"]."',
                                                            '".$data["CountManEmplID"]."',
                                                            '".$data["TermBizID"]."',
                                                            '".$data["ShortDetailsID"]."',
                                                            '".$data["DetailsID"]."',
                                                            '".$data["ImgID"]."',
                                                            '".$data["Img1ID"]."',
                                                            '".$data["Img2ID"]."',
                                                            '".$data["Img3ID"]."',
                                                            '".$data["Img4ID"]."',
                                                            '".(isset($data["pdf"]) ? $data["pdf"] : '')."',
                                                            '".$data["FaxID"]."',
                                                            '".$data["ShareSaleID"]."',
                                                            '".$data["ReasonSellBizID"]."',
                                                            '".$data["txtReasonSellBizID"]."',
                                                            '".$data["TarifID"]."',
                                                            0,
                                                            NOW(),
                                                            NOW(),
                                                            0,
                                                            '".$data["CategoryID"]."',
                                                            '".$data["SubCategoryID"]."',
                                                            '".$data["ConstGUID"]."',
                                                            '".$_SERVER['REMOTE_ADDR']."',
                                                            '".(isset($_SERVER['GEOIP_COUNTRY_NAME']) ? $_SERVER['GEOIP_COUNTRY_NAME'] : '')."',
                                                            '".generate_password(15)."',
                                                            '".generate_password(15)."',
                                                            '".$data["newspaper"]."',
                                                            '".$data["helpbroker"]."',
                                            '".$data["youtubeURL"]."',
                                            '".$data["brokerId"]."',
                                                '".$data["source"]."'
                                                            );";


            $result = $this->db->Insert($sql);

            return $result;
        }        
        
        private function UpdateSell($data) {

            foreach ($data as $k => $v) { 
                $data[$k] = $this->ModerateContent($v);
            }

            $where = " limit 1";

            $sql = "update LOW_PRIORITY Sell set  
                  `FML`='".trim($data["FML"])."',
                  `EmailID`='".trim($data["EmailID"])."',
                  `PhoneID`='".trim($data["PhoneID"])."',
                  `CostID`='".trim($data["CostID"])."',
                  `cCostID`='".trim($data["cCostID"])."',
                  `ProfitPerMonthID`='".trim($data["ProfitPerMonthID"])."',
                  `cProfitPerMonthID`='".trim($data["cProfitPerMonthID"])."',
                  `MonthAveTurnID`='".trim($data["MonthAveTurnID"])."',
                  `cMonthAveTurnID`='".trim($data["cMonthAveTurnID"])."',
                  `ShortDetailsID`='".trim($data["ShortDetailsID"])."',
                  `DetailsID`='".trim($data["DetailsID"])."',
                  `ListObjectID`='".trim($data["ListObjectID"])."',    
                  `StatusID`=0 
                   where ID='".intval(trim($data["ID"]))."' ".$where.";";


            $result = $this->db->Update($sql);

            return $result;
        }         
        
        public function UpdateData() {

            $this->CallData();
            $result = $this->Result();

            $mappingobjectbroker = new MappingObjectBroker();
            $mappingobjectbroker->brokerId = $this->brokerId;
            $mappingobjectbroker->ObjectTypeId = 1;
            $array_object = $mappingobjectbroker->Select();

            $iter = 0;

            while (list($k, $v) = each($result)) {

                if($iter < 1) {

                    while (list($k1, $v1) = each($array_object)) {

                        if($v1["brokerObjectID"] == $v->_id) {

                            $v1_date = strtotime($v1["brokerDateObject"]);
                            $v_date = strtotime($v->_DateUpdate); 

                            if($v1_date < $v_date) {

                                $data = $v->ConvertData();
                                $data["ID"] = $v1["bizzonaObjectId"]; 

                                $this->UpdateSell($data);

                                $mappingobjectbroker->brokerDateObject =  HelpAdapter::unixToMySQL(strtotime($v->_DateUpdate));
                                $mappingobjectbroker->brokerObjectID = $v1["brokerObjectID"];
                                $mappingobjectbroker->UpdateBrokerDateObject(); 

                                $iter += 1;

                                $this->countProcessed += 1;
                            }

                            break;
                        }
                    }

                }
                reset($array_object);
            }
        }
    	
        private function UpdateStatusSell($data) {
            $sql = "update LOW_PRIORITY Sell set `StatusID`=".intval(trim($data["StatusID"]))."  where ID=".intval(trim($data["ID"]))." limit 1;";
            $result = $this->db->Update($sql);
            return $result;
        }        
        
        
        public function DeleteData() {

            $sell = new Sell();
            $this->CallData();
            $result = $this->Result();

            $mappingobjectbroker = new MappingObjectBroker();
            $mappingobjectbroker->brokerId = $this->brokerId;
            $mappingobjectbroker->ObjectTypeId = 1;
            $array_object = $mappingobjectbroker->Select(); 

            $cache = new Cache_Lite();

            while (list($k, $v) = each($array_object)) {

                $status = false;	
                while (list($k1, $v1) = each($result)) {
                    if(intval($v["brokerObjectID"]) == intval($v1->_id)) {
                            $status = true;
                            break;
                    }
                }
                reset($result);

                if(!$status) {
                    $data = array();
                    $data["StatusID"] = 12;  
                    $data["ID"] = $v["bizzonaObjectId"];
                    $this->UpdateStatusSell($data);
                    $this->countProcessed += 1;
                    $cache->remove(''.$v["bizzonaObjectId"].'____sellitem');
                }
            }
        }
        
        public function ReOpenData() {

            $this->CallData();
            $result = $this->Result();
  
            $mappingobjectbroker = new MappingObjectBroker();

            $mappingobjectbroker->brokerId = $this->brokerId;

            $a_Ids = array();

            while (list($k, $v) = each($result)) {
                    $a_Ids[] = $v->_id;		
            } 

            $mappingobjectbroker->arrayBrokerIds =  $a_Ids;

            $mappingobjectbroker->ReOpen();
        }        
    	
    }
	

?>
