<?
    ini_set("magic_quotes_gpc","off");

    global $DATABASE;

    include_once("../phpset.inc");
    include_once("./functions.inc");

    AssignDataBaseSetting("../config.ini");
// требуется авторизация
	require_once($_SERVER['DOCUMENT_ROOT'].'/inside/auth.php');
//

    ini_set("display_startup_errors", "on");
    ini_set("display_errors", "on");
    ini_set("register_globals", "on");

    include_once("./class.AdsActionToObjects.inc");
    $adsActionToObjects = new AdsActionToObjects();

    include_once("./class.AdsAction.inc");
    $adsAction = new AdsAction();

    require_once("../libs/Smarty.class.php");
    $smarty = new Smarty;
    $smarty->template_dir = "../templates";
    $smarty->compile_check = false;
    $smarty->caching = false;
    $smarty->compile_dir  = "../templates_c";
    $smarty->debugging = false;
    $smarty->clear_all_cache();

    $aParamas = array();

    if(isset($_POST['statusId'])) {
       $aParamas['statusId'] = (int)$_POST['statusId'];
    } else {
        $aParamas['statusId'] = 0;
    }

    if(isset($_POST['typeId'])) {
       $aParamas['typeId'] = (int)$_POST['typeId'];
    }

    if(isset($_POST['adsActionId'])) {
       $aParamas['adsActionId'] = (int)$_POST['adsActionId'];
    }

    $data = $adsActionToObjects->Select($aParamas);

    $smarty->assign('data', $data);

    $actionsdata = $adsAction->ShortSelect();
    $smarty->assign("actionsdata", $actionsdata);


?>
	<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
    	<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	 	<LINK href="../general.css" type="text/css" rel="stylesheet">
	 	<meta http-equiv="content-type" content="text/html; charset=windows-1251"/>
	 	<body>
<?
                $smarty->display("./mainmenu.tpl");

		$smarty->display("./AdsActionToObjects/list.tpl");
?>
		</body>
		</html>
<?
?>