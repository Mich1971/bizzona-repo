<?php
    global $DATABASE;

    include_once("../phpset.inc");
    include_once("./functions.inc");

    AssignDataBaseSetting("../config.ini");
// требуется авторизация
	require_once($_SERVER['DOCUMENT_ROOT'].'/inside/auth.php');
//

    ini_set("display_startup_errors", "on");
    ini_set("display_errors", "on");
    ini_set("register_globals", "on");


    require_once("../libs/Smarty.class.php");
    $smarty = new Smarty;
    $smarty->template_dir = "../templates";
    $smarty->compile_check = false;
    $smarty->caching = false;
    $smarty->compile_dir  = "../templates_c";
    $smarty->debugging = false;
    $smarty->clear_all_cache();

    include_once("./class.company.inc");
    $company = new Company();

    include_once("./class.communication.inc");
    $communication = new Communication();


    $smarty->assign("aComm", $communication->Select(array(
        'limit' => 200,
        'objectId' => (isset($_GET['codeId']) ? (int)$_GET['codeId']  : 0),
        'brokerId' => (isset($_GET['brokerId']) ? (int)$_GET['brokerId']  : 0),
    )));

  $companies = $company->ShortSelect();
  $smarty->assign("companies", $companies);

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <LINK href="../general.css" type="text/css" rel="stylesheet">
    <meta http-equiv="content-type" content="text/html; charset=windows-1251"/>
    <body>
<?php
        $smarty->display("./mainmenu.tpl");
        $smarty->display("./commusers/list.tpl");
?>
    </body>
</html>


