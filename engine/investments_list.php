<?php
	ini_set("magic_quotes_gpc","off");

	global $DATABASE;

	include_once("../phpset.inc");
	include_once("./functions.inc"); 
	require_once("./Cache/Lite.php");

	st_engine("../st_engine.txt");

	AssignDataBaseSetting("../config.ini");
// ��������� �����������
	require_once($_SERVER['DOCUMENT_ROOT'].'/inside/auth.php');
//

	include_once("./class.district.inc"); 
	$district = new District();

	include_once("./class.metro.inc"); 
	$metro = new Metro();
  
	include_once("./class.category.inc"); 
	$category = new Category();

	include_once("./class.subcategory.inc");
	$subCategory = new SubCategory();
  
	include_once("./class.country.inc");
	$country = new Country();

	include_once("./class.city.inc");
	$city = new City();

	include_once("./class.emailoffer.inc");
	$emailoffer = new EmailOffer();

	include_once("./class.sell.inc");
	$sell = new Sell();
  
  	include_once("./class.bizplan_company.inc");
  	$bizplan_company = new BizPlanCompany();
  
	include_once("./class.bizplan.inc"); 
	$bizplan = new BizPlan();

	include_once("./class.buy.inc");
	$buyer = new Buyer();

	include_once("./class.equipment.inc");
	$equipment = new Equipment();
  
	include_once("./class.investments.inc");
	$investments = new Investments();
  
	include_once("./class.BuyerSellSubscribe.inc");
	$buyerSellSubscribe = new BuyerSellSubscribe();
	
	include_once("./class.streets.inc");
	$streets = new streets();

  	include_once("./class.equipment.inc"); 
  	$equipment = new Equipment();
  	
        include_once("./class.templateevent.inc"); 
        $templateEvent = new TemplateEvent();	

	require ('./xajax_core/xajax.inc.php');
	$xajax = new xajax();
  	
        include_once("./observer_filter.php"); 
        
        //filterSet('invest');
        
	require_once("../libs/Smarty.class.php");
	$smarty = new Smarty;
	$smarty->template_dir = "../templates";
	$smarty->compile_check = false;
	$smarty->caching = false;
	$smarty->compile_dir  = "../templates_c";
	$smarty->cache_dir = "../cache";
	$smarty->debugging = false;
	$smarty->clear_all_cache();

	$data = array();
	$data["offset"] = 0;
	$data["rowCount"] = 0;
	$result = $category->Select($data);
	$smarty->assign("listCategory", $result);

	$listSubCategory = $subCategory->Select($data);
	$smarty->assign("listSubCategory", $listSubCategory);

	function SelectDistrict($regionID) {
		global $city, $investments;
		
		$default_subregion = "";

		if(isset($_GET["ID"])) {
			$investments->id = intval($_GET["ID"]);
			$residence = $investments->GetResidence();
			$default_subregion = $residence->subRegion_id;
		}
		
		$subregiontext = "";
		
		$city->id = $regionID;
		$asubregion = $city->GetChildRegionAdmin();
		if(sizeof($asubregion) > 0) {
			$subregiontext = "<select name='subRegionID'>";
			$subregiontext.= "<option  value='0'>������ �������, ����</option>";
			while(list($k, $v) = each($asubregion)) {
				if($v->id == $default_subregion) {
					$subregiontext.= "<option value='".$v->id."' selected>".$v->title."</option>";
				} else {
					$subregiontext.= "<option value='".$v->id."'>".$v->title."</option>";
				}
			}
			$subregiontext.= "</select>";
		}
		
		$objResponse = new xajaxResponse();
		$objResponse->setCharacterEncoding("windows-1251");		
		$objResponse->assign('subRegionDiv', 'innerHTML', $subregiontext);
		return $objResponse;
	}

	function AutoLoadRegionSetting() {
		global $investments;
		if(isset($_GET["ID"])) {
			$investments->id  = intval($_GET["ID"]);
			$residence = $investments->GetResidence();
			return SelectDistrict($residence->region_id);
		}
	}
	
	$sDistrict =& $xajax->registerFunction('SelectDistrict');
	$sDistrict->setParameter(0, XAJAX_INPUT_VALUE, "region_id");
	$autoLoad =& $xajax->registerFunction('AutoLoadRegionSetting');	
	
	$xajax->processRequest();
	echo '<?xml version="1.0" encoding="UTF-8"?>';  
?>  
	<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
	<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

	<LINK href="../general.css" type="text/css" rel="stylesheet">

	<head>
<?
	$xajax->printJavascript();
?>
	</head>

<meta http-equiv="content-type" content="text/html; charset=windows-1251"/>
	
	<body>
<?	

        if(isset($_GET['blockip'])) {

            include_once("./class.BlockIP.inc"); 
            $blockIp = new BlockIP();

            if($_GET['blockip'] == 'up') {

                $blockIp->BlockUp(array(
                    'ip' => trim($_GET['ip'])
                ));

            } else if ($_GET['blockip'] == 'down') {

                $blockIp->BlockDown(array(
                    'ip' => trim($_GET['ip'])
                ));

            }
        }


	if(isset($_POST) && sizeof($_POST) > 0)
	{
		// up update cache investment		
  		$options = array(
  			'cacheDir' => "../investmentscache/",
   			'lifeTime' => 1
  		);
		
  		$cache = new Cache_Lite($options);
 		$cache->remove(''.intval($_GET["ID"]).'____investmentsitem');		
		
  		$smarty->cache_dir = "../investmentscache";
  		$smarty->clear_cache("./site/detailsInvestments.tpl", intval($_GET["ID"]));
  		$smarty->cache_dir = "../cache";
 		// down update cache investment
 		
 		
		$investments->id = intval($_GET["ID"]);
		$investments->GetItem();
		$investments->name = ReplaceSymbolToName(trim($_POST["name"]));
		
		$total_typeBizID = htmlspecialchars(trim($_POST["category_id"]), ENT_QUOTES);
		$arr_typeBizID = split("[:]",$total_typeBizID);
		if(sizeof($arr_typeBizID) == 2) {
			$investments->category_id = intval($arr_typeBizID[0]);
			$investments->subCategory_id = intval($arr_typeBizID[1]);
		}
		
		$investments->region_text = $_POST["region_text"];
		$investments->region_id = $_POST["region_id"];
		$investments->subRegion_id = $_POST["subRegionID"];
		$investments->cost =  str_replace(" ","",$_POST["cost"]);
		$investments->ccost = $_POST["ccost"];
		$investments->period = $_POST["period"];
		$investments->payback = $_POST["payback"];
		$investments->yearprofit = str_replace(" ","", $_POST["yearprofit"]);
		$investments->cyearprofit = $_POST["cyearprofit"];
		$investments->totalinvestcost = str_replace(" ","", $_POST["totalinvestcost"]);
		$investments->totalinvestccost = $_POST["totalinvestccost"];
		$investments->stage = $_POST["stage"];
		$investments->documents = $_POST["documents"];
		$investments->shortDescription = TextReplace("shortDescription", $_POST["shortDescription"]);
		$investments->contactface = $_POST["contactface"];
		$investments->contactphone = PhoneFix($_POST["contactphone"]);
		$investments->contactfax = $_POST["contactfax"];
		$investments->contactemail = $_POST["contactemail"];
		$investments->period_id = $_POST["period_id"];
		$investments->status_id = $_POST["status_id"];
		$investments->attractedPerson = $_POST["attractedPerson"];
		$investments->Update();


		if ($_POST["status_id"] != 15 && $_POST["status_id"] != 17) {
			if($_POST["category_id"] == "0") {
				$smarty->display("./error/category.tpl");
			}
			if($_POST["region_id"] == "0" && $_POST["subRegionID"] == "0") {
				$smarty->display("./error/region.tpl");
			}
		}

  		if((int)$_POST["status_id"] == 1) {
  			$templateEvent->EmailEvent(3,3, (int)$_GET["ID"]);
  		} else if ((int)$_POST["status_id"] == 2) { 
  			$templateEvent->EmailEvent(4,3, (int)$_GET["ID"]);
  		} else if (in_array((int)$_POST["status_id"], array(0,3))) {
  			$templateEvent->EmailEvent(1,3, (int)$_GET["ID"]);
  		}
		
		
	
		$smarty->cache_dir = "../cache";
		$resclear = $smarty->clear_all_cache();
	
		$count_category = $investments->CountByCategory();
  	
		$d = array();
		$d["CountI"] = $count_category+1;
		$d["ID"] = $investments->category_id;
		$category->UpdateCountI($d);
  	
		$count_city = $investments->CountByCity();
  	
		$d = array();
		$d["CountI"] = $count_city+1;
		$d["ID"] = $investments->region_id;
  	
		$city->UpdateCountI($d);
	}


	if(!isset($_GET["ID"])) {
		$data = array();
		
		$data["sort"] = "`investments`.`id`";
		$data["offset"] = 0;
		$data["rowCount"] = 0; 	
  	
		$dataPage["offset"] = 0;
		$dataPage["rowCount"] = 0;
		$dataPage["sort"] = "id";
		
		if (isset($_GET["StatusID"])) {
			$dataPage["status_id"] = intval($_GET["StatusID"]);
			$data["status_id"] = $dataPage["status_id"]; 	
		}
		
		$resultPage = $investments->Select($dataPage);
    
		$pagesplit = 40;
    
		$smarty->assign("CountRecord", sizeof($resultPage));
		$smarty->assign("CountSplit", $pagesplit);
		$smarty->assign("CountPage", ceil(sizeof($resultPage)/40));
    
		if (isset($_GET["offset"])) {
			$data["offset"] = intval($_GET["offset"]);
		} else {
			$data["offset"] = 0;
		}
		if (isset($_GET["rowCount"])) { 
			$data["rowCount"] = intval($_GET["rowCount"]);
		} else {
			$data["rowCount"] = $pagesplit;
		}
  	
		$investments_list = $investments->Select($data);
		$smarty->assign("list", $investments_list);
  		
		$smarty->display("./investments/pagesplit.tpl", $_SERVER["REQUEST_URI"]);
		$smarty->display("./selectinvestment.tpl");
		$smarty->display("./investments/investments_list.tpl");
		$smarty->display("./investments/pagesplit.tpl", $_SERVER["REQUEST_URI"]);

	} else {
		$datacity = array();
		$datacity["offset"] = 0;
		$datacity["rowCount"] = 0;
		$datacity["sort"] = "ID";

		$listCity = $city->Select($datacity); 
		$smarty->assign("listCity", $listCity);

		$investments->id = intval($_GET["ID"]);
		$data = $investments->GetItem();
  	
		$smarty->assign("data", $data);
		$smarty->display("./investments/investments_edit.tpl");
	}
?>
<script language="JavaScript">
<?
	$autoLoad->printScript();
	echo ";";
?>-
</script>
</body>
