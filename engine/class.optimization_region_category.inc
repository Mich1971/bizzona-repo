<?php
	include_once("class.db.inc");

	class OptimizationRegionCategory {
		var $db = "";
		public $regionId = 77;
		public $categoryId = 8;
		public $adId = 1;

		function OptimizationRegionCategory() {
			global $DATABASE;
			$this->InitDB();
		}

		function InitDB() {
			global $DATABASE;
			$this->db = new DB($DATABASE["HOSTNAME"], $DATABASE["DATABASENAME"], $DATABASE["DATABASEUSER"], $DATABASE["DATABASEPASSWORD"]);
		}

		function Close() {
			$this->db->Close();
		}
		
		
		function GetItem() {
			$obj = new stdclass();
			$sql  = "select * from optimization_region_category where regioniD=".intval($this->regionId)." and categoryId=".intval($this->categoryId)." and adId=".intval($this->adId).";";
			//echo $sql; 
			$result = $this->db->Select($sql);

			while ($row = mysql_fetch_object($result)) {
				$obj->Id = $row->Id;
				$obj->regionId = $row->regionId;
				$obj->categoryId = $row->categoryId;
				$obj->url = $row->url;
				$obj->fullurl = $row->fullurl;
				$obj->seotitle = $row->seotitle;
				$obj->seo = $row->seo;
				$obj->adId = $row->adId; 
				$obj->description = $row->description;
				$obj->useownurl = $row->useownurl;
			}

			return $obj;
		}
		
		
	}
?>