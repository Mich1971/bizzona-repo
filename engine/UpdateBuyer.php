<?
	ini_set("magic_quotes_gpc","off");

	global $DATABASE;

	include_once("../phpset.inc");
	include_once("../engine/functions.inc"); 
  	require_once("../engine/Cache/Lite.php");  	
	
	AssignDataBaseSetting("../config.ini");
// ��������� �����������
	require_once($_SERVER['DOCUMENT_ROOT'].'/inside/auth.php');
//

        include_once("./observer_filter.php");         
        
        //filterSet('buyer');
        
	require_once("../libs/Smarty.class.php");
	$smarty = new Smarty;
	$smarty->template_dir = "../templates";
	$smarty->compile_dir  = "../templates_c";
	$smarty->cache_dir = "../cache";
	$smarty->compile_check = true;
	$smarty->debugging     = false;
//  $smarty->caching = true;
	$smarty->clear_all_cache();
	
	include_once("../engine/class.category.inc"); 
	$category = new Category();
	
	include_once("../engine/class.buyermetro.inc"); 
	$buyerMetro = new BuyerMetro();

	include_once("../engine/class.buyerdistrict.inc"); 
	$buyerDistrict = new BuyerDistrict();
	
	include_once("../engine/class.subcategory.inc"); 
	$subCategory = new SubCategory();
  
	include_once("../engine/class.country.inc"); 
	$country = new Country();

	include_once("../engine/class.city.inc"); 
	$city = new City();

	include_once("../engine/class.emailoffer.inc"); 
	$emailoffer = new EmailOffer();

	include_once("../engine/class.sell.inc"); 
	$sell = new Sell();
  
	include_once("../engine/class.bizplan_company.inc"); 
	$bizplan_company = new BizPlanCompany();
  
	include_once("../engine/class.bizplan.inc"); 
	$bizplan = new BizPlan();

	include_once("../engine/class.buy.inc"); 
	$buyer = new Buyer();

	include_once("../engine/class.equipment.inc"); 
	$equipment = new Equipment();
  
	include_once("../engine/class.investments.inc"); 
	$investments = new Investments();
  
	include_once("../engine/class.BuyerSellSubscribe.inc"); 
	$buyerSellSubscribe = new BuyerSellSubscribe();
	
	include_once("../engine/class.district.inc"); 
	$district = new District();

	include_once("../engine/class.metro.inc"); 
	$metro = new Metro();
	
	include_once("./class.company.inc");
	$company = new Company();
	
	include_once("./class.whois.inc");
	$whois = new WhoIs();
	
        include_once("./class.templateevent.inc"); 
        $templateEvent = new TemplateEvent();	
	
	require('../engine/xajax_core/xajax.inc.php');
	$xajax = new xajax();
	//$xajax->configure('debug', true);

        $companies = $company->ShortSelect();
        $smarty->assign("companies", $companies);
	
	
	function SelectDistrict($regionID) {
		global $district, $metro, $buyerMetro, $buyerDistrict, $city, $buyer;

		$default_district = array();
		$default_metro = array();
		$default_subregion = "";
		
		if(isset($_GET["ID"])) {
			$buyer->id = intval($_GET["ID"]);
			$residence = $buyer->GetResidence();
			$default_subregion = $residence->subRegionID;
		}
		
		
		if(isset($_GET["ID"])) {
			$buyerMetro->buyerID = intval($_GET["ID"]);
			$default_metro = $buyerMetro->MetroList();
		}

		if(isset($_GET["ID"])) {
			$buyerDistrict->buyerID = intval($_GET["ID"]);
			$default_district = $buyerDistrict->DistrictList();
		}
		
		$district->regionID = $regionID;
		$metro->regionID = $regionID;
		$res = $district->SelectByRegion();
		$resmetro =  $metro->SelectByRegion();
		
		if(sizeof($res) > 0) {
			$text = "<select name='districtID[]' multiple='multiple'>";
			$text.= "<option  value='0'>������ ������</option>";
			while(list($k, $v) = each($res)) {
				if(in_array($v->ID, $default_district)) {
					$text.= "<option value='".$v->ID."' selected>".$v->name."</option>";
				} else {
					$text.= "<option value='".$v->ID."'>".$v->name."</option>";
				}
			}
			$text.= "</select>";
		} else {
			$text = "";
		}
  	    
		if(sizeof($resmetro) > 0) {
			$textmetro = "<select name='metroID[]' multiple='multiple'>";
			$textmetro.= "<option value='0'>�����</option>";
			while(list($k, $v) = each($resmetro)) {
				if(in_array($v->ID, $default_metro)) {
					$textmetro.= "<option value='".$v->ID."' selected>".$v->name."</option>";
				} else {
					$textmetro.= "<option value='".$v->ID."'>".$v->name."</option>";
				}
			}
			$textmetro.= "</select>";
			
		}
		
		$subregiontext = "";
		
		$city->id = $regionID;
		
		$asubregion = $city->GetChildRegionAdmin();

		if(sizeof($asubregion) > 0)
		{
			$subregiontext = "<select name='subRegionID' onchange='xajax_SelectSubDistrict(xajax.$(\"subRegionID\").value)'>";
			$subregiontext.= "<option  value='0'>������ �������, ����</option>";
			while(list($k, $v) = each($asubregion)) {
				if($v->id == $default_subregion) {
					$subregiontext.= "<option value='".$v->id."' selected>".$v->title."</option>";
				} else {
					$subregiontext.= "<option value='".$v->id."'>".$v->title."</option>";
				}
				
			}
			$subregiontext.= "</select>";
		}

		$objResponse = new xajaxResponse();
		$objResponse->setCharacterEncoding("windows-1251");
		$objResponse->assign('districtDiv', 'innerHTML', $text);
		$objResponse->assign('metroDiv', 'innerHTML', $textmetro);
		$objResponse->assign('subRegionDiv', 'innerHTML', $subregiontext);
		return $objResponse;
	}

	function SelectSubDistrict($regionID) {

		
		global $district, $metro, $buyerMetro, $buyerDistrict, $city, $streets, $buyer;
		
		$default_district = array();
		$default_metro = array();
		
		if(isset($_GET["ID"])) {
			$buyerMetro->buyerID = intval($_GET["ID"]);
			$default_metro = $buyerMetro->MetroList();
		}

		if(isset($_GET["ID"])) {
			$buyerDistrict->buyerID = intval($_GET["ID"]);
			$default_district = $buyerDistrict->DistrictList();
		}
	
		$district->regionID = $regionID;
		$metro->regionID = $regionID;
		$res = $district->SelectByRegion();
		$resmetro =  $metro->SelectByRegion();
	
		if(sizeof($res) > 0) {
			$text = "<select name='districtID[]' multiple='multiple'>";
			$text.= "<option  value='0'>������ ������</option>";
			while(list($k, $v) = each($res)) {
				if(in_array($v->ID, $default_district)) {
					$text.= "<option value='".$v->ID."' selected>".$v->name."</option>";
				} else {
					$text.= "<option value='".$v->ID."'>".$v->name."</option>";
				}
			}
			$text.= "</select>";
		} else {
			$text = "";
		}
  	    
		if(sizeof($resmetro) > 0) {
			$textmetro = "<select name='metroID[]' multiple='multiple'>";
			$textmetro.= "<option value='0'>�����</option>";
			while(list($k, $v) = each($resmetro)) {
				if(in_array($v->ID, $default_metro)) {
					$textmetro.= "<option value='".$v->ID."' selected>".$v->name."</option>";
				} else {
					$textmetro.= "<option value='".$v->ID."'>".$v->name."</option>";
				}
			}
			$textmetro.= "</select>";
			
		}
		
		$objResponse = new xajaxResponse();
		$objResponse->setCharacterEncoding("windows-1251");
		$objResponse->assign('districtDiv', 'innerHTML', $text);
		$objResponse->assign('metroDiv', 'innerHTML', $textmetro);
		return $objResponse;
	}
	
	
	
	function AutoLoadRegionSetting() {
		global $buyer;
		if(isset($_GET["ID"])) {
			$buyer->id = intval($_GET["ID"]);
			$regionID = $buyer->GetRegion();
			return SelectDistrict($regionID);
		}
	}
	
	function AutoLoadSubRegionSetting() {
		global $buyer;
		if(isset($_GET["ID"])) {
			$buyer->id = intval($_GET["ID"]);
			$residence = $buyer->GetResidence();
			return SelectSubDistrict($residence->subRegionID);
		}
	}
	
	
	$autoLoad =& $xajax->registerFunction('AutoLoadRegionSetting');	
	$autoSubLoad =& $xajax->registerFunction('AutoLoadSubRegionSetting');
	
	$sDistrict =& $xajax->registerFunction('SelectDistrict');
	$sDistrict->setParameter(0, XAJAX_INPUT_VALUE, "CityID");

	$sSubDistrict =& $xajax->registerFunction('SelectSubDistrict');
	$sSubDistrict->setParameter(0, XAJAX_INPUT_VALUE, "subRegionID");
	
	$xajax->processRequest();

  	$companies = $company->ShortSelect();
  	$smarty->assign("companies", $companies);
?>

<LINK href="../general.css" type="text/css" rel="stylesheet">

<head>
<?
	$xajax->printJavascript();
?>
</head>
<meta http-equiv="content-type" content="text/html; charset=windows-1251"/>

<body>
    
<?	

        if(isset($_GET['blockip'])) {

            include_once("./class.BlockIP.inc"); 
            $blockIp = new BlockIP();

            if($_GET['blockip'] == 'up') {

                $blockIp->BlockUp(array(
                    'ip' => trim($_GET['ip'])
                ));

            } else if ($_GET['blockip'] == 'down') {

                $blockIp->BlockDown(array(
                    'ip' => trim($_GET['ip'])
                ));

            }
        }


  	if(isset($_POST["updatepayment"])) {

  		$buyer->SetStatusPaymentByAdmin(intval($_POST["ID"]), intval($_POST["pay_summ"]), intval($_POST["pay_status"]), $_POST["pay_datetime"], (isset($_POST["pay_datetime_auto"]) ? 1 : 0 ));
  	
  	}


	
	if (isset($_POST) && sizeof($_POST) > 0 && !isset($_POST['updatepayment'])) {

		// up update cache buy 
  		$smarty->cache_dir = "../buycache";
  		$smarty->clear_cache("./site/detailsBuy.tpl", intval($_GET["ID"]));
  		$smarty->cache_dir = "../cache";
  		
  		$options = array(
  			'cacheDir' => "../buycache/",
   			'lifeTime' => 1
  		);
		
  		$cache = new Cache_Lite($options);
 		$cache->remove(''.intval($_GET["ID"]).'____buyitem');  		
  		
  		// up update cache buy		
		
		
		$buyerMetro->buyerID = intval($_GET["ID"]);
		if(isset($_POST["metroID"])) {
			$buyerMetro->Clear();
			while (list($k, $v) = each($_POST["metroID"])) {
				$buyerMetro->metroID = intval($v);
				$buyerMetro->Insert();
			}
		}
		
		$buyerDistrict->buyerID = intval($_GET["ID"]);
		if(isset($_POST["districtID"])) {
			
			$buyerDistrict->Clear();
			while (list($k, $v) = each($_POST["districtID"])) {
				$buyerDistrict->districtID = intval($v);
				$buyerDistrict->Insert();
			}
		}
		
		$buyer->GetItem(array("ID" => $_GET["ID"]));   	  	
    	$buyer->id = $_GET["ID"];
    	$buyer->name = $_POST["nameID"];
		$buyer->duration = $_POST["durationID"];
		$buyer->cost_start = str_replace(" ","", $_POST["cost_startID"]);
		$buyer->cost_stop = str_replace(" ","", $_POST["cost_stopID"]);
		$buyer->regionText = $_POST["regionTextID"];
		$buyer->regionID = $_POST["CityID"];
		$buyer->subRegionID = $_POST["subRegionID"];
	
		if(isset($_POST["subscribe_status_id"])) {
			$buyer->subscribe_status_id = 1;
		} else {
			$buyer->subscribe_status_id = 0;
		}
	
		$arr_typeBizID = split("[:]",$_POST["categoryID"]);
		if(sizeof($arr_typeBizID) == 2)
		{
			$buyer->typeBizID = intval($arr_typeBizID[0]);
			$buyer->subTypeBizID = intval($arr_typeBizID[1]);
		}
		else 
		{
			$buyer->typeBizID = intval($arr_typeBizID[0]);
			$buyer->subTypeBizID = intval($arr_typeBizID[1]);
		}
	
		$buyer->FML = $_POST["FMLID"];
		$buyer->phone = PhoneFix($_POST["phoneID"]);
		$buyer->email = $_POST["emailID"];
		$buyer->statusID = $_POST["statusID"];
		$buyer->age = $_POST["ageID"];
		$buyer->otherinfo = TextReplace("otherinfoID", ParsingDetails($_POST["otherinfoID"]));
		$buyer->description = TextReplace("descriptionID", ParsingDetails($_POST["descriptionID"]));
		$buyer->realty = TextReplace("realtyID", $_POST["realtyID"]);
		$buyer->currencyID = $_POST["currencyID"];
		$buyer->part = $_POST["partID"];
		$t_part = $buyer->part;
		if(strlen($t_part) > 0 && $t_part[strlen($t_part)-1] != "%") {
		  $buyer->part = $buyer->part."%";
		}
		unset($t_part);
		$buyer->license = $_POST["licenseID"];
		$buyer->payback = $_POST["paybackID"];
		$buyer->subtitle = FixSubTitle($_POST["subtitle"]);
		$buyer->brokerId = trim($_POST["brokerId"]);
		$buyer->Update();
		

		if ($_POST["statusID"] != 15 && $_POST["statusID"] != 17) {
			if($_POST["categoryID"] == "0") {
				$smarty->display("./error/category.tpl");
			}
			if($_POST["CityID"] == "0" && $_POST["subRegionID"] == "0") {
				$smarty->display("./error/region.tpl");
			}
		}


  		if((int)$_POST["statusID"] == 1 && !isset($_POST['updatepayment'])) {
  			$templateEvent->EmailEvent(3,2, (int)$_GET["ID"]);
  		} else if ((int)$_POST["statusID"] == 2 && !isset($_POST['updatepayment'])) {
  			$templateEvent->EmailEvent(4,2, (int)$_GET["ID"]);
  		} else if ( in_array((int)$_POST["statusID"], array(0,3)) && !isset($_POST['updatepayment'])) {
  			$templateEvent->EmailEvent(1,2, (int)$_GET["ID"]);  
  		}

	
		$smarty->cache_dir = "../cache";
		$resclear = $smarty->clear_all_cache();

  		$countCategory = 0;

  		$sdata = Array();
  		$sdata["offset"] = 0;
  		$sdata["rowCount"] = 0;
  		$sdata["sort"] = "id";
  		$sdata["typeBizID"] = $buyer->typeBizID;
  		$sdata["StatusID"] = 1;
  		
  		
  		$arr = $buyer->Select($sdata);
  		$countCategory = $countCategory + sizeof($arr);

  		$d = array();
  		$d["CountB"] = $countCategory;
  		$d["ID"] = $buyer->typeBizID;
  		$category->UpdateCountB($d);	
	
  		$countCity = 0;

  		$sdata = Array();
  		$sdata["offset"] = 0;
  		$sdata["rowCount"] = 0;
  		$sdata["sort"] = "id";
  		$sdata["regionID"] = $buyer->regionID;
  		$sdata["StatusID"] = 1;
  		$arr = $buyer->Select($sdata);
  		$countCity = $countCity + sizeof($arr);
    
  		$d = array();
  		$d["CountB"] = $countCity;
  		$d["ID"] = $buyer->regionID;
  		$city->UpdateCountB($d);  	
	}	

	$data = array();
	$data["offset"] = 0;
	$data["rowCount"] = 0;
	$data["sort"] = "ID";

	$listCategory = $category->Select($data); 
	$smarty->assign("listCategory", $listCategory);
  
	$listSubCategory = $subCategory->Select($data);
	$smarty->assign("listSubCategory", $listSubCategory);
  
	//$listCity = $city->Select($data); 
	$listCity = $city->SelectForAdmin($data); 
	$smarty->assign("listCity", $listCity);
  
	$listSubCategory = $subCategory->Select($data);
	$smarty->assign("listSubCategory", $listSubCategory);
  
	$pagesplit = 40;

	if (isset($_GET["ID"])) {
  		$data_get["ID"] = $_GET["ID"];
  		$result_get = $buyer->GetItem($data_get);
  		if(strlen($result_get->description) <= 0) {
  			$result_get->description = "���������� ������� ";
  		}
  		if(strlen($result_get->otherinfo) <= 0) {
  			$result_get->otherinfo = "���������� ������� ";
  		}
  		
  		$smarty->assign("data", $result_get); 
  		
  		/* 
    	$whois->ip = $result_get->ip;
    	if(isset($_SESSION[$whois->ip])) {
    		$whois_data = $_SESSION[$whois->ip];	
    	} else {
        	//echo "ok";
    		$whois_data = $whois->GetData();
    		$_SESSION[$whois->ip] =  $whois_data;	
    	}
    	*/ 
    	
    	$smarty->assign("whois_city", $whois_data["city"]);
    	$smarty->assign("whois_region", $whois_data["region"]);  		
  		
	} else {
    	if (isset($_GET["offset"])) {
      		$data["offset"] = intval($_GET["offset"]);
    	} else {
      		$data["offset"] = 0;
    	}
    	if (isset($_GET["rowCount"])) { 
      		$data["rowCount"] = intval($_GET["rowCount"]);
    	} else {
      		$data["rowCount"] = $pagesplit;
    	}
	
		if (isset($_GET["StatusID"])) {
			$data["StatusID"] = intval($_GET["StatusID"]);
		}

		if (isset($_GET["CityID"]) && intval($_GET["CityID"]) > 0) {
			$data["CityID"] = intval($_GET["CityID"]);
		}

		if (isset($_GET["brokerId"])) {
			$data["brokerId"] = intval($_GET["brokerId"]);
		}
		
		
    	$result = $buyer->Select($data);
    	$smarty->assign("data", $result);
	}

  	AssignDescription("../config.ini");

	$dataPage["offset"] = 0;
	$dataPage["rowCount"] = 0;
	$dataPage["sort"] = "ID";
	if (isset($_GET["StatusID"])) {
		$dataPage["StatusID"] = intval($_GET["StatusID"]);
	}

	if (isset($_GET["CityID"])) {
		$dataPage["CityID"] = intval($_GET["CityID"]);
	}

	if (isset($_GET["brokerId"])) {
		$dataPage["brokerId"] = intval($_GET["brokerId"]);
	}
	
	
	$resultPage = $buyer->Select($dataPage);
	$smarty->assign("CountRecord", sizeof($resultPage));
	$smarty->assign("CountSplit", $pagesplit);
	$smarty->assign("CountPage", ceil(sizeof($resultPage)/40));

	
	$smarty->display("./pagesplit.tpl", $_SERVER["REQUEST_URI"]);

  	if (!isset($_GET["ID"])) {
  		$smarty->display("./selectbueyr.tpl");
  	}
	
	$smarty->display("./buy/update.tpl");

	$smarty->display("./pagesplit.tpl", $_SERVER["REQUEST_URI"]);

	$district->Close();
	$metro->Close();
	$buyerMetro->Close();
?>

<script language="JavaScript">
<?
	$autoLoad->printScript();
	echo ";";
	$autoSubLoad->printScript();
	echo ";";
	$autoSubLoad->printScript();
?>
</script>
