<?php
 
    global $DATABASE;

    include_once(dirname(__FILE__)."/../../phpset.inc");
    include_once(dirname(__FILE__)."./../functions.inc"); 

    AssignDataBaseSetting(dirname(__FILE__)."/../../config.ini");

    include_once(dirname(__FILE__)."/offers.class.php"); 
    
    ini_set("display_startup_errors", "on");
    ini_set("display_errors", "on");
    ini_set("register_globals", "on");

    

    while(true) {
    
        $offer = new offers();
        $offer->start();
          
        sleep(300);
        
        unset($offer);
    }

?>