<?php
 
include_once(dirname(__FILE__)."/../class.db.inc");  	
include_once(dirname(__FILE__)."/../class.templateevent.inc"); 

 
/*
 * ����� ���������� �� ���������� ������������� ������ ����.�����������.
 */ 
 

class offers {
    
    public $db = "";
    public $sql = "";
    public $diffdays = 45;
    
    function __construct() {
        global $DATABASE;
        $this->InitDB();
    }

    function InitDB() {
        global $DATABASE;
        $this->db = new DB($DATABASE["HOSTNAME"], $DATABASE["DATABASENAME"], $DATABASE["DATABASEUSER"], $DATABASE["DATABASEPASSWORD"]);
    }
 
    function __destruct() {
        mysql_close($this->db->link);
    }		
       
    function reconnect() {
        mysql_close($this->db->link);
        $this->InitDB();
    }
    
    /*
     * ����� ���� ���������� �� ������� �� ������������� ��� ������� ��� �� ���� ���������� �� ���������������.
     */
    private function NextSellId() {

        if(!mysql_ping($this->db->link)) {
           $this->reconnect();
        }
        
        if(!mysql_ping($this->db->link)) {
            echo "\n -- no connection db from class 'offers' -- \n";
            return 0;
        }    
        
        $this->sql = 'select Sell.ID, Sell.date, DATEDIFF(NOW(),Sell.date) as diffdays, HistoryTemplateEvent.datetime as hdatetime from Sell left join HistoryTemplateEvent on (Sell.ID = HistoryTemplateEvent.objectId and HistoryTemplateEvent.typeEventId=12 and HistoryTemplateEvent.isBrokerId=0 and HistoryTemplateEvent.typeEventObjectId = 1)  where 1  and Sell.statusId in (1) and (Sell.brokerId is null or Sell.brokerId = 0) having hdatetime is null  and diffdays > '.(int)$this->diffdays.'  limit 1';
        $result = $this->db->Select($this->sql);
        while ($row = mysql_fetch_object($result)) {
            return $row->ID;
        } 
        
        return 0;
    } 
      
     
    /*
     * �������� ����� ��� ������� �������������� �����
     */
    public function start() {
         
        $objectId = $this->NextSellId(); 
         
        if($objectId) {
            
            $templateEvent = new TemplateEvent();
             
            if(!mysql_ping($templateEvent->db->link)) {
                $templateEvent->reconnect();
            }            
            
            if(!mysql_ping($templateEvent->db->link)) {
                echo "\n -- no connection db from class 'TemplateEvent' -- \n";
                return false;
            } 
            
            
            $templateEvent->EmailEvent(12, 1, $objectId);
             
            unset($templateEvent);
        }
    }
    
}