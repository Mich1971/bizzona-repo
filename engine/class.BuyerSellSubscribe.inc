<?php
 
  include_once("class.db.inc");

  define("NEWSEND_SUBSCRIBE", 0);
  define("OPENSEND_SUBSCRIBE", 1);
  define("CLOSESEND_SUBSCRIBE", 2);
  define("IGNORE_SUBSCRIBE", 3);
  
  
  class BuyerSellSubscribe {
     var $db = "";
     var $id = "";
     var $buyerID = "";
     var $sellID = "";
     var $statusID = "";
     var $datetime = "";
     var $buyer_list = array();
     var $SellSubCategoryID = "";
     var $SellCategoryID = "";
     var $SellRegionID = "";
     
     var $to = "";
     var $from = "";
     var $subject = "";
     var $message = "";
     var $headers = ""; 
     
     
     
     function BuyerSellSubscribe() {
       global $DATABASE;
       $this->InitDB();
     }

     function InitDB() {
       global $DATABASE;
       $this->db = new DB($DATABASE["HOSTNAME"], $DATABASE["DATABASENAME"], $DATABASE["DATABASEUSER"], $DATABASE["DATABASEPASSWORD"]);
     }

     function SetStatus()
     {
     	$sql = "update `BuyerSellSubscribe` set `statusID`=".$this->statusID."  where `id`=".$this->id.";";
     	$result = $this->db->Select($sql);
     }
     
     function Select() {
     	
     	$sql = "select `BuyerSellSubscribe`.id,
     				   `buyer`.`name` as 'BuyerName',
     				   `Sell`.`NameBizID` as 'SellName'	
     					 from  `BuyerSellSubscribe`,
     						   `buyer`,
     						   `Sell` 
     									 	where 
     										 		`BuyerSellSubscribe`.`statusID`='".$this->statusID."'
     										 				and
     										 		`BuyerSellSubscribe`.`buyerID` = `buyer`.`id`
     										 				and
													`BuyerSellSubscribe`.`sellID` = `Sell`.`ID` order by `BuyerSellSubscribe`.`id` desc; ";

      	$result = $this->db->Select($sql);
	
      	$array = Array();
        while ($row = mysql_fetch_object($result)) {
        	
         	$obj = new stdclass();
         	$obj->id = $row->id;
         	$obj->BuyerName = $row->BuyerName;
         	$obj->SellName = $row->SellName;
        	
			$array[] = $obj;
        }
        
     	return $array;
     }

     function Update() {
     }

     function Delete() {
     	
		$sql = "delete from `BuyerSellSubscribe` 
     								where
     									`BuyerSellSubscribe`.`sellID` = '".$this->sellID."'
     														and
     									`BuyerSellSubscribe`.`buyerID` = '".$this->buyerID."'";
     	$result = $this->db->Select($sql);
     }

     function GetItem() {
     	
     	$sql = "select * from `BuyerSellSubscribe` where `id`='".$this->id."'";
      	$result = $this->db->Select($sql);
	
        while ($row = mysql_fetch_object($result)) {
        	$this->sellID = $row->sellID;
        	$this->buyerID = $row->buyerID;
        }
     	
     }

     
     function Insert()
     {
     	$sql = "select * from `BuyerSellSubscribe` 
     									where 
     										`BuyerSellSubscribe`.`sellID` = '".$this->sellID."'
     															and
     										`BuyerSellSubscribe`.`buyerID` = '".$this->buyerID."'
     															and
     										`BuyerSellSubscribe`.`statusID`='".CLOSESEND_SUBSCRIBE."'";
     	$result = $this->db->Select($sql);
     	
     	$row = mysql_fetch_row($result);
     	
     	if(!is_array($row) or sizeof($row) <= 0)
     	{
			$this->Delete();
			
			$sql = "insert into `BuyerSellSubscribe` (
														`sellID`, 
													  	`buyerID`,
													  	`statusID`,
													  	`datetime`
													 ) 
													 	values 
													 (
													 	'".$this->sellID."',
													 	'".$this->buyerID."',
													 	'".NEWSEND_SUBSCRIBE."',
													 	NOW()
													 )";
			$result = $this->db->Select($sql);
     	}
     	
     	
     }

     
     function InitBuyerList()
     {
    	$sql = "select * from `buyer` where `buyer`.`statusID`='1' and `buyer`.`subscribe_status_id`='1'";
      	$result = $this->db->Select($sql);
	
        while ($row = mysql_fetch_object($result)) {

        	$arr = array();
        	$arr["CategoryID"] = $row->typeBizID;
        	$arr["SubCategoryID"] = $row->subTypeBizID;
        	$arr["RegionID"] = $row->regionID;
        	$this->buyer_list[$row->id] = $arr;
        }

     }
     
     
     function InitSendmail()
     {
     	$this->InitBuyerList();

     	while (list($k,$v) = each($this->buyer_list))
     	{
     		if(is_array($v))
     		{
     			if(	($v["CategoryID"] == $this->SellCategoryID || 
     				 $v["SubCategoryID"] == $this->SellSubCategoryID) &&
     				 $v["RegionID"] == $this->SellRegionID)
     				{
     					$this->buyerID = $k;
     					$this->Insert();
     				}
     		}
     	}
     }
     
     function SendEmail($id)
     {
        $this->headers = "From: BizZONA.ru  <".$this->from.">\r\n".
    					 "Reply-To: BizZONA.ru <".$this->from.">\r\n".
    					 "Content-type: text/html; charset=windows-1251;\r\n".
    					 "Return-Path: ".$this->from."\r\n";
     	
     	if(mail($this->to, $this->subject, $this->message, $this->headers))
     	{
     		$this->statusID = CLOSESEND_SUBSCRIBE;
     		$this->id = $id;
     		$this->SetStatus();
     	}
     	
     }
     
     function GetBuyer()
     {
     	$sql = "select * from `buyer` where `id`='".$this->buyerID."';";
      	$result = $this->db->Select($sql);
		$res = new stdClass();      	
        while ($row = mysql_fetch_object($result)) {
        	$res->fml = $row->FML;
        	$res->email = $row->email;
        }
     	return $res;
     }
     
     function GetSell()
     {
     	$sql = "Select Sell.*,  SubCategory.CategoryID AS 'SubCategoryID', SubCategory.icon AS 'SubCategoryIcon', Category.icon AS 'CategoryIcon'  from Sell LEFT JOIN SubCategory ON ( SubCategory.ID = Sell.SubCategoryID ) LEFT JOIN Category ON ( Category.ID = Sell.CategoryID )  where `Sell`.`ID`='".$this->sellID."';";
      	$result = $this->db->Select($sql);
		$res = new stdClass();      	
		
        while ($row = mysql_fetch_object($result)) {
        	
         $res->ID = $row->ID;
         if(strlen($row->Icon) > 3) {
         	$res->CategoryIcon = $row->Icon;
         } else if (strlen($row->SubCategoryIcon)  > 0) {
            $res->CategoryIcon = $row->SubCategoryIcon;
         } else {
            $res->CategoryIcon = $row->CategoryIcon;
         } 
         
         $res->FML = $row->FML;
         $res->EmailID = $row->EmailID;
         $res->Email2ID = $row->Email2ID;
         $res->PhoneID = $row->PhoneID;
         $res->NameBizID = $row->NameBizID;
         $res->BizFormID = $row->BizFormID;
         $res->CityID = $row->CityID;
         $res->SiteID = $row->SiteID;
         $res->txtCostID = $row->txtCostID; 
         $res->CostID = trim(strrev(chunk_split (strrev($row->CostID), 3,' '))); 
         $res->cCostID = $row->cCostID; 
         $res->AgreementCostID = $row->AgreementCostID;
         $res->txtProfitPerMonthID = $row->txtProfitPerMonthID;
         $res->ProfitPerMonthID = trim(strrev(chunk_split (strrev($row->ProfitPerMonthID), 3,' ')));
         $res->cProfitPerMonthID = $row->cProfitPerMonthID;
         $res->MaxProfitPerMonthID = trim(strrev(chunk_split (strrev($row->MaxProfitPerMonthID), 3,' ')));
         $res->cMaxProfitPerMonthID = $row->cMaxProfitPerMonthID;
         $res->TimeInvestID = $row->TimeInvestID;
         $res->ListObjectID = $row->ListObjectID;
         $res->ContactID = $row->ContactID;
         $res->MatPropertyID = $row->MatPropertyID;
         $res->txtMonthAveTurnID = $row->txtMonthAveTurnID; 
         $res->MonthAveTurnID = trim(strrev(chunk_split (strrev($row->MonthAveTurnID), 3,' '))); 
         $res->cMonthAveTurnID = $row->cMonthAveTurnID; 
         $res->MaxMonthAveTurnID = trim(strrev(chunk_split (strrev($row->MaxMonthAveTurnID), 3,' '))); 
         $res->cMaxMonthAveTurnID = $row->cMaxMonthAveTurnID; 
         $res->txtMonthExpemseID = $row->txtMonthExpemseID; 
         $res->MonthExpemseID = trim(strrev(chunk_split (strrev($row->MonthExpemseID), 3,' '))); 
         $res->cMonthExpemseID = $row->cMonthExpemseID; 
         $res->txtSumDebtsID = $row->txtSumDebtsID;
         $res->SumDebtsID = $row->SumDebtsID;
         $res->cSumDebtsID = $row->cSumDebtsID;
         $res->txtWageFundID = $row->txtWageFundID;
         $res->WageFundID = $row->WageFundID;
         $res->cWageFundID = $row->cWageFundID;
         $res->ObligationID = $row->ObligationID;
         $res->CountEmplID = $row->CountEmplID;
         $res->CountManEmplID = $row->CountManEmplID;
         $res->TermBizID = $row->TermBizID;
         $res->ShortDetailsID = $row->ShortDetailsID;
         $res->DetailsID = $row->DetailsID;
         $res->ImgID = $row->ImgID;
         $res->FaxID = $row->FaxID;
         $res->ShareSaleID = $row->ShareSaleID;
         $res->ReasonSellBizID = $row->ReasonSellBizID;
         $res->TarifID = $row->TarifID;
         $res->StatusID = $row->StatusID;
         $res->DataCreate = $row->DataCreate;
         $res->DateStart = $row->DateStart;
         $res->TimeActive = $row->TimeActive;
         $res->CategoryID = $row->CategoryID;
         $res->QView = $row->QView;
         //$obj->DataCreate  = date("d F Y", strtotime($row->DataCreate));
         $res->DataCreate = strftime("%d %B %Y",  strtotime($row->DataCreate));
         $res->DataClose   = date("d F Y", strtotime($row->DateStart) + 3600*24*30*$row->TarifID);
         $res->DateBySecondCreate  = strtotime($row->DataCreate);
         $res->DateBySecondClose   = strtotime($row->DateStart) + 3600*24*30*$row->TarifID;
         $res->PublicNotify = $row->PublicNotify;
         $res->RepeatNotify = $row->RepeatNotify;
         $res->Agent = $row->Agent;
         $res->Icon = $row->Icon;
         $res->SubCategoryID = $row->SubCategoryID;
         $res->pay_summ = $row->pay_summ;
        }
     	return $res;
     }
  }
?>