<?php
	include_once("interface.sellsdapter.inc");
	include_once("../engine/class.sell.inc");
	include_once("../engine/functions.inc"); 
	include_once("../engine/typographus.php"); 
	include_once("../engine/class.mappingobjectbroker.inc"); 	
	
	
	
    class Deloshop implements SellAdapter {
    	
    	public $_id;
		public $_DateUpdate;
		public $_Price;
		public $_ProfitPerMonth;
		public $_MaxProfitPerMonth;
		public $_MonthAveTurn;
		public $_MonthExpemse;
		public $_MaxMonthExpemse;
		public $_SumDebts;
		public $_WageFund;
		public $_RentCostId;
		public $_TimeInvest;
		public $_ListObject;
		public $_MatProperty;
		public $_CountEmpl;
		public $_CountManEmpl;
		public $_TermBiz;
		public $_ShareSale;
		public $_Contact;
		public $_offertype;
		public $_businesstype;
		public $_phone;
		public $_contact;
		public $_title;
		public $_description;
		public $_shortDescription;
		public $_web;
		public $_img1;
		public $_img2;
		public $_img3;
		public $_Obligation;
		public $_address;    	
		
		public $_email;
		public $_town;
		public $_orgform; 
		public $_reason;
		public $_torg;
		public $_fml;
		
    	public $brokerId = 43; 
         
        public $countProcessed = 0;
    	
    	public $_cCurency;
    	
    	//public $url = "http://deloshop.ru/xml/bizzona.xml";
    	public $url = "http://omega.deloshop.ru/xml/bizzona.xml";
    	
    	private $array_deloshop = array();
    	
    	public $town_maps = array(
    							 );
    	
    	function __construct() {
    		
    	}
    	
    	public function Result() {
    		
    		return $this->array_deloshop;
    		
    	}
    	
    	public $acCurency_maps  = array(
    									"���" => "rub",
    									"" => "eur", 
    									"" => "usd", 
    									);
    	
    	public $aOrgform = array(
    								"���" => "1",
    								"��"  => "8",
    								"���" => "2",
    								"���" => "3"
    							);
		    									
    	
		private function ModerateContent($content) {
			
			return $content;
			
		}
    	
    	public function ConvertData() {

    		$data = array();
    		
			$data["FML"] =  htmlspecialchars($this->_fml, ENT_QUOTES);
			$data["EmailID"] = htmlspecialchars($this->_email, ENT_QUOTES);
			$data["PhoneID"] = htmlspecialchars($this->_phone, ENT_QUOTES);
			$data["NameBizID"] = htmlspecialchars(ucfirst(strtolower($this->_title)), ENT_QUOTES);
			
			if(isset($this->aOrgform[$this->_orgform])) {
				$data["BizFormID"] = $this->aOrgform[$this->_orgform];	
			} else {
				$data["BizFormID"] = "";	
			}
			
			if(isset($this->town_maps[$this->_town])) {
				$data["CityID"] = $this->town_maps[$this->_town]["cityId"];
				$data["SubCityID"] = $this->town_maps[$this->_town]["subCityId"];
			} else {
				$data["CityID"] = "";
				$data["SubCityID"] = "";
			}
			
			
			$data["SiteID"] = htmlspecialchars($this->_address, ENT_QUOTES);
			$data["CostID"] = intval($this->_Price);
			
			$data["cCostID"] = "usd";
			
			$data["txtCostID"] = $this->_Price;
			$data["txtProfitPerMonthID"] = "";
			$data["ProfitPerMonthID"] = $this->_ProfitPerMonth;
			$data["cProfitPerMonthID"] = $data["cCostID"];
			$data["cMaxProfitPerMonthID"] = $this->_MaxProfitPerMonth;
			$data["TimeInvestID"] = htmlspecialchars(ucfirst(strtolower($this->_TimeInvest)), ENT_QUOTES);
			$data["ListObjectID"] = htmlspecialchars(ucfirst(strtolower($this->_ListObject)), ENT_QUOTES); 
			$data["ContactID"] = htmlspecialchars(ucfirst(strtolower($this->_Contact)), ENT_QUOTES); 
			$data["MatPropertyID"] = htmlspecialchars(ucfirst(strtolower($this->_MatProperty)), ENT_QUOTES); 
			$data["txtMonthAveTurnID"] = $data["cCostID"];
			$data["MonthAveTurnID"] = $this->_MonthAveTurn;
			$data["cMonthAveTurnID"] = $data["cCostID"];
			$data["cMaxMonthAveTurnID"] = "";
			$data["txtMonthExpemseID"] = "";
			$data["MonthExpemseID"] = $this->_MonthExpemse;
			$data["cMonthExpemseID"] = $data["cCostID"];
			$data["txtSumDebtsID"] = "";
			$data["SumDebtsID"] = $this->_SumDebts;
			$data["cSumDebtsID"] = $data["cCostID"];
			$data["txtWageFundID"] = "";
			$data["WageFundID"] = $this->_WageFund;
			$data["cWageFundID"] = $data["cCostID"];
			$data["ObligationID"] = htmlspecialchars(ucfirst(strtolower($this->_Obligation)), ENT_QUOTES);
			$data["CountEmplID"] = $this->_CountEmpl;
			$data["CountManEmplID"] = $this->_CountManEmpl;
			$data["TermBizID"] = $this->_TermBiz;
			
			
			$data["ShortDetailsID"] = htmlspecialchars(ucfirst($this->_shortDescription), ENT_QUOTES); 
			$data["DetailsID"] = htmlspecialchars(ucfirst($this->_description), ENT_QUOTES); 
			
			
			$data["FaxID"] = ""; 
			$data["ShareSaleID"] = htmlspecialchars(ucfirst(strtolower($this->_ShareSale)), ENT_QUOTES);
			$data["ReasonSellBizID"] = "";//$this->_reason;
			$data["txtReasonSellBizID"] = $this->_reason;
			$data["TarifID"] = ""; 
			$data["StatusID"] = "";
			$data["DataCreate"] = "";
			$data["DateStart"] = "";
			$data["TimeActive"] = "";
			
			$data["CategoryID"] =  (isset($this->category_maps[$this->_businesstype]) ? $this->category_maps[$this->_businesstype] : "");
			$data["SubCategoryID"] = "";
			$data["ConstGUID"] = "";
			$data["ip"] = "";
			$data["defaultUserCountry"] = "";
			$data["login"] = "";
			$data["password"] = "";
			$data["newspaper"] = "";
			$data["helpbroker"] = "";
			$data["youtubeURL"] = "";    		
    		$data["brokerId"] = $this->brokerId;
    		$data["ReasonSellBizID"] = htmlspecialchars($this->_reason, ENT_QUOTES);
    		$data["AgreementCostID"] = $this->_torg;
    		
    		$data["ArendaCostID"] = intval($this->_RentCostId);
    		$data["cArendaCostID"] = $data["cCostID"];
    		//_RentCostId
    		
			$data["ImgID"] = "";
			$data["Img1ID"] = "";
			$data["Img2ID"] = "";
			
    		//up Typographus
    		
    		$typo = new Typographus();
    		
    		$data["NameBizID"] = $typo->process($data["NameBizID"]);
    		$data["SiteID"] = $typo->process($data["SiteID"]);
    		$data["ListObjectID"] = $typo->process($data["ListObjectID"]);
    		$data["MatPropertyID"] = $typo->process($data["MatPropertyID"]);
    		$data["ShortDetailsID"] = $typo->process($data["ShortDetailsID"]);
    		$data["DetailsID"] = $typo->process($data["DetailsID"]);
    		
    		//down Typographus    		
    		
    		
    		return $data;
    	}
    	
    	public function CallData() {
    		
    		$xml = simplexml_load_file($this->url);
    		$this->ReadDataCallBack($xml);
    		
    	}
    	
    	
		private function convertXmlObjToArr($obj, &$arr) 
		{ 
    		$children = $obj->children(); 
    		foreach ($children as $elementName => $node) 
    		{ 
        		$nextIdx = count($arr); 
        		$arr[$nextIdx] = array(); 
        		$arr[$nextIdx]['@name'] = strtolower((string)$elementName); 
        		$arr[$nextIdx]['@attributes'] = array(); 
        		$attributes = $node->attributes(); 
        		foreach ($attributes as $attributeName => $attributeValue) 
        		{ 
            		$attribName = strtolower(trim((string)$attributeName)); 
            		$attribVal = trim((string)$attributeValue); 
            		$arr[$nextIdx]['@attributes'][$attribName] = $attribVal; 
        		} 
        		$text = (string)$node; 
        		$text = trim($text); 
        		if (strlen($text) > 0) 
        		{ 
            		$arr[$nextIdx]['@text'] = $text; 
        		} 
        		$arr[$nextIdx]['@children'] = array(); 
        		$this->convertXmlObjToArr($node, $arr[$nextIdx]['@children']); 
    		} 
    		return; 
		}  		
		
		private function ReadDataCallBack($arr) {

			$res_arr = array();
			$this->convertXmlObjToArr($arr, $res_arr);	
			
			while (list($k, $v) = each($res_arr)) {
				
				$item = $res_arr[$k];
				
				
				$sub_item = $item["@children"];
				
				$_deloshop = new Deloshop(); 	
				
					
				while (list($k1, $v1) = each($sub_item)) {

					$root_attrr = $v["@attributes"];
					
					$name = $v1["@name"];
					
					$_deloshop->_id = $root_attrr["advert_id"];

					if($name == "title") {
						
						$title_attrr = $v1["@attributes"];
						while (list($ktitle, $vtitle) = each($title_attrr)) {
							
							if($ktitle == "value") {
								
								$_deloshop->_Price = $vtitle;
								
							}  else if ($ktitle == "revenue") {
								
							} else if ($ktitle == "profit") {
								
								$_deloshop->_ProfitPerMonth = $vtitle;
								
							} else if ($ktitle == "age") {
								
								$_deloshop->_TermBiz = $vtitle;
								
							} else if ($ktitle == "staff") {
								
								$_deloshop->_CountEmpl = $vtitle;
								
							} else if ($ktitle == "region") {
								
								$_deloshop->_address = utf8_to_cp1251($vtitle);
								
							}
							
						}
						
						$_deloshop->_title = utf8_to_cp1251($v1["@text"]);
					}
					
					if($name == "text") {
					
						$description_children = $v1["@children"];
						
						while (list($k3, $v3) = each($description_children)) {
						
							$name_description = $v3["@name"];		
							
							if($name_description == "moreinfo") {
								$_deloshop->_description = utf8_to_cp1251($v3["@text"]);
							}
							if($name_description == "moreinfo") {
								$_deloshop->_shortDescription = utf8_to_cp1251($v3["@text"]);
							}					
						
						}
					
					}
					
					
					if($name == "contact") {

						$contact_children = $v1["@children"];
						
						while (list($k2, $v2) = each($contact_children)) {
						
							$name_contact = $v2["@name"];		
							
							if($name_contact == "phone") {
								$_deloshop->_phone = $v2["@text"];
							}					
							if($name_contact == "email") {
								$_deloshop->_email = $v2["@text"];
							}
							if($name_contact == "name") {
								$_deloshop->_fml = utf8_to_cp1251($v2["@text"]);
							}					
						}
					}

				}
				
				$this->array_deloshop[] = $_deloshop;	

			}
	
		}
    	
    	public function ImportData() {
    		
    		$this->CallData();
    		$result = $this->Result();


    		$sell = new Sell();	
				

			$iter = 0;
			
			while (list($k, $v) = each($result)) {
	
				if ($iter == 1) {
					break;
				}
 			
				$mappingobjectbroker = new MappingObjectBroker();
					
				$mappingobjectbroker->brokerObjectID = $v->_id;
				$mappingobjectbroker->brokerId = $this->brokerId;
				
				if (!$mappingobjectbroker->IsExistsBrokerObject()) {
						
					$mappingobjectbroker->bizzonaObjectId = $sell->Deloshop_Insert($v->ConvertData());
					$mappingobjectbroker->brokerId = $this->brokerId;
					$mappingobjectbroker->brokerDateObject = HelpAdapter::unixToMySQL(strtotime($v->_DateUpdate));
					$mappingobjectbroker->ObjectTypeId = 1;
					$mappingobjectbroker->Insert();
					
					
					//echo "<h1>OK</h1>";
					$iter += 1;
			
                                        $this->countProcessed += 1;
				}
			}
			
    	}
    	
    	public function UpdateData() {
    		
    		$this->CallData();
    		$result = $this->Result();

    		
    		$sell = new Sell();
    		
			$mappingobjectbroker = new MappingObjectBroker();
			$mappingobjectbroker->brokerId = $this->brokerId;
			$mappingobjectbroker->ObjectTypeId = 1;
			$array_object = $mappingobjectbroker->Select();
			
			$iter = 0;
			
			
			while (list($k, $v) = each($result)) {
				
				if($iter < 1) {
				
					while (list($k1, $v1) = each($array_object)) {
						if($v1["brokerObjectID"] == $v->_id) {
							
							$v1_date = strtotime($v1["brokerDateObject"]);
							$v_date = strtotime($v->_DateUpdate); 

							if($v1_date < $v_date) {
							
								$data = $v->ConvertData();
								$data["ID"] = $v1["bizzonaObjectId"];
							
								$sell->deloshop_Update($data);
							
								$mappingobjectbroker->brokerDateObject = HelpAdapter::unixToMySQL(strtotime($v->_DateUpdate));
								$mappingobjectbroker->brokerObjectID = $v1["brokerObjectID"];
								$mappingobjectbroker->UpdateBrokerDateObject(); 
							
								$iter += 1;
							}

							break;
						}
					}
				
				}
				reset($array_object);
			}
    	}
    	
    	public function DeleteData() {
    		
    		$sell = new Sell();
    		$this->CallData();
    		$result = $this->Result();
    		
			$mappingobjectbroker = new MappingObjectBroker();
			$mappingobjectbroker->brokerId = $this->brokerId;
			$mappingobjectbroker->ObjectTypeId = 1;
			$array_object = $mappingobjectbroker->Select(); 
			
			while (list($k, $v) = each($array_object)) {
				$status = false;	
				while (list($k1, $v1) = each($result)) {
					if($v["brokerObjectID"] == $v1->id) {
						$status = true;
						
					}
				}
				reset($result);
				
				if(!$status) {
			        //echo $v->bizzonaObjectId."<hr>";		
					$data = array();
					$data["StatusID"] = 12;
					$data["ID"] = $v["bizzonaObjectId"];
					$sell->deloshop_UpdateStatusID($data);
				}
				
			}
    	}
    	
    }
	

?>