<?php
    include_once("class.db.inc");  

    class BadWords {
    
        public $db = '';
        public $sql = '';
        public $Id = 0;
        public $word = '';
        
        
        function __construct() {
                $this->InitDB();
        }

        function InitDB() {
                global $DATABASE;
                $this->db = new DB($DATABASE["HOSTNAME"], $DATABASE["DATABASENAME"], $DATABASE["DATABASEUSER"], $DATABASE["DATABASEPASSWORD"]);
        }
        
        public function Insert($aWords = array()) {
            
            foreach ($aWords as $k => $v) {
                $this->sql = 'insert into BadWords (`word`) values ("'.trim($v).'")';
                $this->db->Insert($this->sql);
            }
            
        }
        
        public function DistSelect($text = '') {
            
            $listWords = $this->Select();
                
            $aWords = array();
            
            foreach($listWords as $k => $v) {
                $aWords[] = $v['word'];
            }
             
            if(count($aWords)) {
             
                $strWords = implode('|', $aWords);
                $text = preg_replace('/\b'.$strWords.'\b/i', '<strong style="color:red;">\\0</strong>', $text);
                
            }
            
            return $text;
        }
        
        public function Select() {
            
            $this->sql = 'select * from BadWords where 1 order by word desc;';
            $result = $this->db->Select($this->sql);
            
            $array = array();
            
            while ($row = mysql_fetch_object($result)) {
                
                $array[] = array(
                    'Id' => $row->Id,
                    'word' => $row->word,
                );
                
            }
            
            return $array;
        }
        
        public function Delete($aIds = array()) {
            
            foreach ($aIds as $k => $v) {
                $this->sql = 'delete from BadWords where Id='.(int)$v.' limit 1';
                $this->db->Delete($this->sql);
            }
            
        }
        
    }
?>