<?php
	include_once("class.db.inc");  
        require_once('class.bzn_items.inc');           

	class Room 	{

		public $db = "";
		public $sql = "";
		public $Id = 0;
		public $RoomCategoryId = 0;
		public $ShortDescription = "";
		public $Description = "";
		public $area_from = 0;
		public $area_to = 0;
		public $cityId = 0;
		public $streetId = 0;
		public $metroId = 0;
		public $cost = 0;
		public $cCostId = 0;
		public $photo_1 = "";
		public $smallphoto_1 = "";
		public $middlephoto_1 = "";
		public $statusId = 0;
		public $fio = "";
		public $phone = "";
		public $email = "";
		public $youtubeURL = "";
		public $youtubeShortURL = "";
		public $fax = "";
		public $pay_status = 0;
		public $pay_summ = 0;
		public $pay_datetime = 0;
		public $pay_smsnumber = 0;
		public $ip = "";
		public $datecreate = 0;
		public $countView = 0;
		public $area_text = "";
		public $cost_text = "";
		public $districtId = 0;
		public $Name = "";
		public $quicksearchurl = "";
		public $offset = 0;
		public $rowCount = 0;
		public $show_area = "";
		public $Address = "";
		public $latitude = 0;
		public $longitude = 0;
		public $zoomId = 7;
		public $RoomCategoryName = "";
		public $SeoTitle = "";
		public $PublicNotify = 0;
		public $AddressLat = "";
		public $sort = "";
		public $brokerId = 0;
		public $partnerid = 0;
		
		function __construct() {
			global $DATABASE;
			$this->InitDB();
		}

		function InitDB() {
			global $DATABASE;
			$this->db = new DB($DATABASE["HOSTNAME"], $DATABASE["DATABASENAME"], $DATABASE["DATABASEUSER"], $DATABASE["DATABASEPASSWORD"]);
		}
		
		
		function Insert() {

			//$this->RoomCategoryId = (isset($this->RoomCategoryId) ? $this->RoomCategoryId : 0);
			//$this->ShortDescription = "";
			//$this->Description = "";
			$this->area_from = 0;
			$this->area_to = 0;
			$this->cost = intval($this->cost);
			$this->cCostId = intval($this->cCostId);
			//$this->photo_1 = "";
			//$this->fio = "";
			//$this->phone = "";
			//$this->email = "";
			//$this->youtubeURL = "";
			//$this->fax = "";
			//$this->ip = "";
			
			$this->sql = "insert into Room (RoomCategoryId, ShortDescription, Description, area_from, area_to, cityId, streetId, metroId, cost, cCostId, photo_1, fio, phone, email, youtubeURL, fax, ip, datecreate, area_text, cost_text, Address, partnerid) values (".$this->RoomCategoryId.", '".$this->ShortDescription."', '".$this->Description."', ".$this->area_from.", ".$this->area_to.", ".$this->cityId.", ".$this->streetId.", ".$this->metroId.", ".$this->cost.", ".$this->cCostId.", '".$this->photo_1."', '".$this->fio."', '".$this->phone."', '".$this->email."', '".$this->youtubeURL."', '".$this->fax."', '".$this->ip."', NOW(), '".$this->area_text."', '".$this->cost_text."', '".$this->Address."', '".$this->partnerid."');"; 
			//echo $this->sql; 
			$result = $this->db->Insert($this->sql);
			$this->Id = intval($result);
                        
                        $this->BZN_Insert(array(
                            'codeId' => $this->Id,
                            'name' => $this->ShortDescription,
                            'shortDetailsId' => $this->ShortDescription,
                            'description' => $this->Description,     
                            'typeCurrencyId' => $this->cCostId,
                            'minCost' => $this->cost,
                            'maxCost' => 0,                    
                        ));                           
                        
			return $result;
						
		}
		
		function UpdateItem() {
			
			$this->sql = " update Room set Name='".$this->Name."',
			                         RoomCategoryId=".intval($this->RoomCategoryId).", 
			                         ShortDescription='".  addslashes(trim($this->ShortDescription))."',    
			                         Description='".addslashes(trim($this->Description))."', 
			                         area_from='".trim($this->area_from)."',  
			                         area_to='".trim($this->area_to)."', 
			                         cityId=".intval($this->cityId).", 
			                         streetId=".intval($this->streetId).", 
			                         metroId=".intval($this->metroId).", 
			                         cost='".trim($this->cost)."',  
			                         cCostId='".trim($this->cCostId)."', 
			                         photo_1='".trim($this->photo_1)."', 
			                         smallphoto_1='".trim($this->smallphoto_1)."', 
			                         statusId=".intval($this->statusId).", 
			                         fio='".addslashes(trim($this->fio))."', 
			                         phone='".trim($this->phone)."', 
			                         email='".trim($this->email)."', 
			                         latitude='".trim($this->latitude)."', 
			                         longitude='".trim($this->longitude)."', 
			                         Address='".addslashes(trim($this->Address))."', 
			                         zoomId=".intval($this->zoomId).", 
			                         middlephoto_1='".trim($this->middlephoto_1)."',
			                         SeoTitle='".trim($this->SeoTitle)."',
			                         brokerId=".intval($this->brokerId)." 
			                         where Id=".intval($this->Id)." limit 1; ";
			$this->db->Update($this->sql);	
                        
                        $this->BZN_UpdateStatus(array(
                            'codeId' => intval($this->Id), 
                            'statusId' => $this->ConvertorStatus($this->statusId), 
                        ));      
                        
                        $this->BZN_Update(array(
                            'codeId' => $this->Id, 
                            'name' => $this->Name,
                            'shortDetailsId' => $this->ShortDescription,
                            'description' => $this->Description,        
                            'typeCurrencyId' => $this->cCostId,
                            'minCost' => $this->cost,
                            'maxCost' => 0,                    
                            
                        ));                        
                        
                        
		}

		function GetItemAdmin()	{
			$_room = new Room();
			$this->sql = "select Room.datecreate, Room.pay_datetime, Room.pay_status, Room.pay_summ, Room.SeoTitle, Room.cost_text, Room.area_text, Room.brokerId, RoomCategory.Name as 'RoomCategoryName', Room.Id, Room.middlephoto_1, Room.zoomId, Room.Address, Room.latitude, Room.longitude, Room.Name, Room.RoomCategoryId, Room.ShortDescription, Room.Description, Room.area_from, Room.area_to, Room.cityId, Room.streetId, Room.metroId, Room.cost, Room.cCostId, Room.photo_1, Room.smallphoto_1, Room.statusId, Room.fio, Room.phone, Room.email from Room left join RoomCategory on (RoomCategory.Id = Room.RoomcategoryId) where 1 and  Room.Id=".intval($this->Id).";";

			//echo $this->sql;
			
			$result = $this->db->Select($this->sql);
			while ($row = mysql_fetch_object($result)) {
				$_room->Id = intval($row->Id);
				$_room->RoomCategoryId = $row->RoomCategoryId;
				$_room->ShortDescription = $row->ShortDescription;
				$_room->Description = $row->Description;
				$_room->area_from = $row->area_from;
				$_room->area_to = $row->area_to;
				$_room->cityId = $row->cityId;
				$_room->streetId = $row->streetId;
				$_room->metroId = $row->metroId;
				$_room->cost = trim(strrev(chunk_split (strrev($row->cost), 3,' ')));
				$_room->cCostId = $row->cCostId;
				$_room->photo_1 = $row->photo_1;
				$_room->smallphoto_1 = $row->smallphoto_1;
				$_room->statusId = $row->statusId;
				$_room->fio = $row->fio;
				$_room->phone = $row->phone;
				$_room->email = $row->email;
				$_room->Name = $row->Name;
				
				$_room->latitude = $row->latitude;
				$_room->longitude = $row->longitude;
				$_room->zoomId = $row->zoomId;
				
				$_room->Address = $row->Address;
				$_room->middlephoto_1 = $row->middlephoto_1;
				$_room->show_area = $_room->GetArea(true);
				$_room->RoomCategoryName = $row->RoomCategoryName; 
				$_room->SeoTitle = $row->SeoTitle;
				$_room->AddressLat = ruslat_for_google($row->Address);
				$_room->brokerId =  $row->brokerId;
				$_room->area_text = $row->area_text;
				$_room->cost_text = $row->cost_text;
				$_room->pay_summ = $row->pay_summ;
				$_room->pay_status = $row->pay_status;
				$_room->pay_datetime = $row->pay_datetime;
				$_room->datecreate = $row->datecreate;
			}
			return $_room;			
		}
		
		function GetItem() {
			
			$_room = new Room();
			$this->sql = "select region.latitude as 'reg_latitude', region.longitude as 'reg_longitude',  region.zoom as 'reg_zoom',  Room.datecreate, Room.pay_datetime, Room.pay_status, Room.pay_summ, Room.SeoTitle, Room.cost_text, Room.area_text, Room.brokerId, RoomCategory.Name as 'RoomCategoryName', Room.Id, Room.middlephoto_1, Room.zoomId, Room.Address, Room.latitude, Room.longitude, Room.Name, Room.RoomCategoryId, Room.ShortDescription, Room.Description, Room.area_from, Room.area_to, Room.cityId, Room.streetId, Room.metroId, Room.cost, Room.cCostId, Room.photo_1, Room.smallphoto_1, Room.statusId, Room.fio, Room.phone, Room.email from Room left join RoomCategory on (RoomCategory.Id = Room.RoomcategoryId), region where 1 and region.id=Room.cityId and  Room.Id=".intval($this->Id).";";

			//echo $this->sql;
			
			$result = $this->db->Select($this->sql);
			while ($row = mysql_fetch_object($result)) {
				$_room->Id = intval($row->Id);
				$_room->RoomCategoryId = $row->RoomCategoryId;
				$_room->ShortDescription = $row->ShortDescription;
				$_room->Description = $row->Description;
				$_room->area_from = $row->area_from;
				$_room->area_to = $row->area_to;
				$_room->cityId = $row->cityId;
				$_room->streetId = $row->streetId;
				$_room->metroId = $row->metroId;
				$_room->cost = trim(strrev(chunk_split (strrev($row->cost), 3,' ')));
				$_room->cCostId = $row->cCostId;
				$_room->photo_1 = $row->photo_1;
				$_room->smallphoto_1 = $row->smallphoto_1;
				$_room->statusId = $row->statusId;
				$_room->fio = $row->fio;
				$_room->phone = $row->phone;
				$_room->email = $row->email;
				$_room->Name = $row->Name;
				
				$_room->latitude = (strlen($row->latitude) > 1 ? $row->latitude : $row->reg_latitude);
				$_room->longitude = (strlen($row->longitude) > 1 ? $row->longitude : $row->reg_longitude);
				$_room->zoomId = (intval($row->zoomId) > 0 ? $row->zoomId : $row->reg_zoom);
				
				$_room->Address = $row->Address;
				$_room->middlephoto_1 = $row->middlephoto_1;
				$_room->show_area = $_room->GetArea(true);
				$_room->RoomCategoryName = $row->RoomCategoryName; 
				$_room->SeoTitle = $row->SeoTitle;
				$_room->AddressLat = ruslat_for_google($row->Address);
				$_room->brokerId =  $row->brokerId;
				$_room->area_text = $row->area_text;
				$_room->cost_text = $row->cost_text;
				$_room->pay_summ = $row->pay_summ;
				$_room->pay_status = $row->pay_status;
				$_room->pay_datetime = $row->pay_datetime;
				$_room->datecreate = $row->datecreate;
			}
			return $_room;			
			
		}		

		function CountSelect() {

			$where = " where 1 ";

			if(intval($this->RoomCategoryId) > 0) {
				$where .= " and Room.RoomCategoryId = ".intval($this->RoomCategoryId)." ";
			}

			if(intval($this->cityId) > 0) {
				$where .= " and Room.cityId = ".intval($this->cityId)." ";
			}	
			
			if(intval($this->districtId) > 0) {
				$where .= " and Room.districtId = ".intval($this->districtId)." ";
			}
			
			if(intval($this->metroId) > 0) {
				$where .= " and Room.metroId = ".intval($this->metroId)." ";
			}			
			
			if(intval($this->streetId) > 0) {
				$where .= " and Room.streetId = ".intval($this->streetId)." ";
			}			
			
			if(intval($this->statusId) > 0) {
				$where .= " and Room.statusId = ".intval($this->statusId)." ";
			}			
			
			$this->sql = "select Count(Id) as 'Count' from Room ".$where.";";
			 
			$result = $this->db->Select($this->sql);
			while ($row = mysql_fetch_object($result)) {
				
				return intval($row->Count);
			}
			
			return 0;
		}
		
		function Select() {
			
			$array = Array();
			
			$where = " where 1 ";
			$limit = " ";
			
			$order = (strlen(trim($this->sort)) > 0 ? " ".$this->sort." " : " order by Room.pay_datetime desc ");
			
			if(intval($this->RoomCategoryId) > 0) {
				$where .= " and Room.RoomCategoryId = ".intval($this->RoomCategoryId)." ";
			}
			
			if(intval($this->cityId) > 0) {
				$where .= " and Room.cityId = ".intval($this->cityId)." ";
			}			
			
			if(intval($this->districtId) > 0) {
				$where .= " and Room.districtId = ".intval($this->districtId)." ";
			}			
			
			if(intval($this->metroId) > 0) {
				$where .= " and Room.metroId = ".intval($this->metroId)." ";
			}			
			
			if(intval($this->streetId) > 0) {
				$where .= " and Room.streetId = ".intval($this->streetId)." ";
			}	
			
			if(intval($this->statusId) > 0) {
				$where .= " and Room.statusId = ".intval($this->statusId)." ";
			}					
			
			if ($this->offset === 0 && $this->rowCount === 0) {
			} else {
				$limit = " limit ".intval($this->offset).",".intval($this->rowCount)."";
			}		 	
			
			
			$this->sql = "select block_ip.ip as statusblockip, Room.partnerid, Room.pay_summ, Room.countView, Room.PublicNotify, Room.Address, Room.ip, Room.Id, Room.Name, RoomCategoryId, RoomCategory.Name as 'RoomCategoryName', Room.ShortDescription, Room.Description, Room.area_from, Room.area_to, Room.cityId, Room.streetId, Room.metroId, Room.cost, Room.cCostId, Room.photo_1, Room.smallphoto_1, Room.statusId, Room.fio, Room.phone, Room.email, Room.datecreate, Room.fax, Room.districtId from Room left join RoomCategory on (RoomCategory.Id = Room.RoomCategoryId)  left join block_ip on (block_ip.ip = Room.ip)  ".$where." ".$order." ".$limit.";";
			
			$result = $this->db->Select($this->sql);
			
			while ($row = mysql_fetch_object($result)) {
				
				$_room = new Room();
                                $_room->statusblockip = $row->statusblockip;  
				$_room->Id = intval($row->Id);
				$_room->RoomCategoryId = intval($row->RoomCategoryId);
				$_room->ShortDescription = $row->ShortDescription;
				$_room->Description = $row->Description;
				$_room->area_from = $row->area_from;
				$_room->area_to = $row->area_to;
				$_room->cityId = $row->cityId;
				$_room->streetId = $row->streetId;
				$_room->metroId = $row->metroId;
				$_room->cost = trim(strrev(chunk_split (strrev($row->cost), 3,' ')));  //$row->cost;
				$_room->cCostId = $row->cCostId;
				$_room->photo_1 = $row->photo_1;
				$_room->smallphoto_1 = $row->smallphoto_1;
				$_room->statusId = $row->statusId;
				$_room->fio = $row->fio;
				$_room->phone = $row->phone;
				$_room->email = $row->email;
				$_room->Name = $row->Name;
				$_room->datecreate =  strftime("%d %B %Y",  strtotime($row->datecreate));				
				$_room->fax = $row->fax;
				$_room->districtId = $row->districtId;
				$_room->show_area = $_room->GetArea(true);
				$_room->RoomCategoryName = $row->RoomCategoryName;
				$_room->Address = $row->Address;
				$_room->countView = $row->countView;
				$_room->ip = $row->ip;
				$_room->PublicNotify = $row->PublicNotify;
				$_room->pay_summ = $row->pay_summ;
				$_room->partnerid = $row->partnerid;
 				 		
				$_room->quicksearchurl = "http://www.bizzona.ru/detailssearchroom.php?cityId=".$_room->cityId."&RoomCategoryId=".$_room->RoomCategoryId."&metroId=".$_room->metroId."&districtId=".$_room->districtId."&streetId=".$_room->streetId."";
				$array[] = $_room;
			}
			
			return $array;

		}

		function UpdateCountView() {
			$sql = "update Room set countView=countView+1 where Id=".intval($this->Id)." limit 1; ";			
			$this->db->Update($sql);
		}
		
		function UpdatePublicNotify() {
			$sql = "update Room set PublicNotify=".intval($this->PublicNotify)." where Id=".intval($this->Id)." limit 1; ";
			//mail("eugenekurilov@gmail.com",$sql,$sql);
			$this->db->Update($sql);
		}
		
		function SetPayment($CodeID, $PayStatus, $PaySumm, $PayDateTime, $PayNumber)
		{
			$sql = "update Room set 
                               `pay_status`=".$PayStatus.", 
                               `pay_summ`=`pay_summ`+ ".$PaySumm.", 
                               `pay_datetime`=NOW(),
                               `pay_smsnumber`=".$PayNumber.",
                               `datecreate`=NOW(),
                               `countView`=0 
                       where Id=".$CodeID." ";
			
			//echo $sql;
			
			$result_1 = $this->db->Update($sql);

			$sql = "insert into payments(summ,datepayment) values ('".$PaySumm."',NOW());";
			$result_2 = $this->db->Update($sql);
			
			return $result_1;
		}		
		
		function GetArea($show_on_site = false) {
			
			if(intval($this->area_from) > 0 && intval($this->area_to) > 0) {
				
				if(intval($this->area_from) == intval($this->area_to)) {
					return ($show_on_site ?  "".$this->area_from." �&sup2; " : "".$this->area_from."");
				}
				
				if(intval($this->area_to) > intval($this->area_from)) {
					return ($show_on_site ? "".$this->area_from."  �&sup2;  - ".$this->area_to." �&sup2; " : "".$this->area_from." - ".$this->area_to."");
				}
				
				if(intval($this->area_to) < intval($this->area_from)) {
					return ($show_on_site ? "".$this->area_to." �&sup2; - ".$this->area_from." �&sup2;" : "".$this->area_to." - ".$this->area_from."");
				}
				
			}
			
			if(intval($this->area_from) <= 0 && intval($this->area_to) > 0) {
				return ($show_on_site ? "".$this->area_to." �&sup2;" : "".$this->area_to."");
			}
			
			if(intval($this->area_from) <= 0 && intval($this->area_to) <= 0) {
				return ($show_on_site ? "�� �������": "");
			}			
			
			if(intval($this->area_from) > 0 && intval($this->area_to) <= 0) {
				return ($show_on_site ? "".$this->area_from." �&sup2;" : "".$this->area_from."");
			}			
			
			return "";
		}
		
		function CountStatus($StatusID) {
			$sql = "select count(Room.Id) as 'c' from Room where Room.statusId=".intval($StatusID)."; ";
                        
                        //echo $sql;
                        
			$result = $this->db->Select($sql);
			while ($row = mysql_fetch_object($result)) {
				return $row->c;
			}
			return 0;
		}  

		function CountPayStatus() {
			$sql = "select COUNT(*) as 'c' from Room where Room.statusId in (5,0) and Room.pay_status=1";
                        
                        //echo $sql;
                        
			$result = $this->db->Select($sql);
			while ($row = mysql_fetch_object($result)) {
				return $row->c;
			}
			return 0;
		}		
		
		function ListCountPayStatus() {
			$sql = "select ID from Room where Room.statusId in (5,0) and Room.pay_status=1";
                        
                        //echo $sql;
                        
			$array = Array();
			$result = $this->db->Select($sql);
			while ($row = mysql_fetch_object($result)) {
				$array[] = $row->ID;
			}
			return $array;
		}		
		
		function SetStatusPaymentByAdmin($Id, $pay_summ, $pay_status, $pay_datetime, $pay_datetime_auto) {
			
			if($pay_status == 1) { 
				if($pay_datetime_auto) {
					$sql = "update Room set `pay_status`=1, `pay_summ`='".$pay_summ."', `pay_datetime`=NOW(), `datecreate`=NOW(), `countView`=0  where ID=".$Id."; ";					
				} else {
					$sql = "update Room set `pay_status`=1, `pay_summ`='".$pay_summ."', `pay_datetime`='".$pay_datetime."', `datecreate`='".$pay_datetime."', `countView`=0  where ID=".$Id."; ";
				}
			} else {
				$sql = "update Room set `pay_status`= '".$pay_status."', `pay_summ`='".$pay_summ."' where ID=".$Id.";";				
			}
			
			$result = $this->db->Update($sql);
		}		
		
		function Close() {
			$this->db->Close();
		}		
		
                private function BZN_Update($aParams) {

                    $bzn_items = new BZN_Items();
                    $bzn_items->Update(array(
                        'typeId' => BZN_Items::TYPE_ROOM,
                        'codeId' => $aParams['codeId'],
                        'subTypeId' => BZN_Items::SUPPLIER,
                        'name' => (isset($aParams['name']) ? $aParams['name'] : '' ),
                        'shortDetailsId' => (isset($aParams['shortDetailsId']) ? $aParams['shortDetailsId'] : '' ),
                        'description' => (isset($aParams['description']) ? $aParams['description'] : '' ),                                       
                        'typeCurrencyId' => (isset($aParams['typeCurrencyId']) ? $aParams['typeCurrencyId'] : '' ),
                        'minCost' => (isset($aParams['minCost']) ? $aParams['minCost'] : '' ),
                        'maxCost' => (isset($aParams['maxCost']) ? $aParams['maxCost'] : '' ),                        
                    ));            

                }                  
                
                private function BZN_Insert($aParams) {

                    $bzn_items = new BZN_Items();
                    $bzn_items->Insert(array(
                        'typeId' => BZN_Items::TYPE_ROOM,
                        'codeId' => $aParams['codeId'],
                        'subTypeId' => BZN_Items::SUPPLIER,
                        'name' => (isset($aParams['name']) ? $aParams['name'] : '' ),
                        'shortDetailsId' => (isset($aParams['shortDetailsId']) ? $aParams['shortDetailsId'] : '' ),
                        'description' => (isset($aParams['description']) ? $aParams['description'] : '' ),                                       
                        'typeCurrencyId' => (isset($aParams['typeCurrencyId']) ? $aParams['typeCurrencyId'] : '' ),
                        'minCost' => (isset($aParams['minCost']) ? $aParams['minCost'] : '' ),
                        'maxCost' => (isset($aParams['maxCost']) ? $aParams['maxCost'] : '' ),                        
                    ));            

                }  	
                
                private function BZN_UpdateStatus($aParams) {

                    $bzn_items = new BZN_Items();
                    $bzn_items->UpdateStatus(array(
                        'typeId' => BZN_Items::TYPE_ROOM,
                        'codeId' => $aParams['codeId'],
                        'statusId' => $aParams['statusId'],
                        'subTypeId' => BZN_Items::SUPPLIER
                    ));               

                }                
                
                private function ConvertorStatus($statusId) {

                    $aStatus = array(
                        '0' => BZN_Items::STATUS_NEW,
                        '1' => BZN_Items::STATUS_OPEN,
                        '10' => BZN_Items::STATUS_CLOSE,
                        '5' => BZN_Items::STATUS_WAIT,
                        '15' => BZN_Items::STATUS_DUBLICATE,
                        '17' => BZN_Items::STATUS_TRUSH,
                        '12' => BZN_Items::STATUS_SOLD,
                    );

                    return (isset($aStatus[$statusId]) ? $aStatus[$statusId] : '');

                }                   
                
		
	}

?>