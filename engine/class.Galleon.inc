<?php
include_once("interface.sellsdapter.inc");
include_once("../engine/class.sell.inc");
include_once("../engine/functions.inc"); 
include_once("../engine/typographus.php"); 
include_once("../engine/class.mappingobjectbroker.inc"); 	
require_once("../engine/Cache/Lite.php");
include_once($_SERVER['DOCUMENT_ROOT']."/engine/CreateSubDirImg.php"); 

function save_image($inPath,$outPath) { 

    $in = fopen($inPath, "rb");     
    $out =  fopen($outPath, "wb");     
    while ($chunk = fread($in,8192)) {         
        fwrite($out, $chunk, 8192);     
    }     
    fclose($in);     
    fclose($out); 
} 	
 
 
class Galleon implements SellAdapter {

    public $db = "";
    
    public $_id;
    public $_dateupdate;
    public $_price;
    public $_profitpermonth;
    public $_monthaveturn;
    public $_monthexpemse;
    public $_timeinvest;
    public $_listobject;
    public $_matproperty;
    public $_countempl;
    public $_countmanempl;
    public $_termbiz;
    public $_sharesale;
    public $_offertype;
    public $_businesstype;
    public $_phone;
    public $_contact;
    public $_title;
    public $_description;
    public $_pic = array();

    public $_urlBrokerObject;

    public $_email;
    public $_town;
    public $_orgform; 
    public $_reason; 

    public $brokerId = 102; 

    public $countProcessed = 0;

    public $url = "http://xml.galleon.pro/wp-content/uploads/export_xml/bizzona.ru.xml";
 
    private $array_galleon = array();


    public $orgform_maps = array(
        "���" => "1",
        "���������������" => "8"
    );

    public $category_maps = array (
        "�����������, ����������, ���, ���������" => "90",
        "������, ������������, ���.������" => "105",
        "���������, ���� ������" => "93",
        "�������� ��������, ��������-�������" => "114",
        "������������ ������������" => "94",
        "��������, ���������, �������� �������" => "115",
        "������������ � �������������" => "94",
        "������� ��������" => "89",
        "����������� ����� �����" => "117",
        "������������, ������" => "82",
        "���������, ����, ����, �����" => "92",
        "������ �������, ������" => "109",
        "�������� ���������" => "78",
        "�������������" => "87",
        "������������ ��������" => "97",
        "������������, ������������" => "96",
        "���������� ������" => "119",
        "������ �� �������" => "",
        "������" => ""
    );

    public $town_maps = array(

        "�����" => array("cityId" => "69", "subCityId" => "2048"),
        "�������" => array("cityId" => "40", "subCityId" => "662"),
        "������" => array("cityId" => "77", "subCityId" => ""),
        "�������" => array("cityId" => "50", "subCityId" => "3699"),
        "�������" => array("cityId" => "50", "subCityId" => "1187"),
        "���" => array("cityId" => "2", "subCityId" => "304"),
        "�������" => array("cityId" => "50", "subCityId" => "1202"),
        "��������" => array("cityId" => "50", "subCityId" => "1191"),
        "���������" => array("cityId" => "50", "subCityId" => "1192"),
        "��������" => array("cityId" => "50", "subCityId" => ""),
        "�����-���������" => array("cityId" => "78", "subCityId" => ""),
        "�����������" => array("cityId" => "50", "subCityId" => "1168"),
        "���������������" => array("cityId" => "50", "subCityId" => "3700"),
        "������������" => array("cityId" => "50", "subCityId" => ""),
        "�����" => array("cityId" => "50", "subCityId" => ""),
        "��������" => array("cityId" => "50", "subCityId" => "1188"),
        "�������" => array("cityId" => "50", "subCityId" => "1179"),
        "�������" => array("cityId" => "50", "subCityId" => "1184"),
        "���������" => array("cityId" => "50", "subCityId" => "1164"),
        "�����" => array("cityId" => "50", "subCityId" => "1199"),
        "������" => array("cityId" => "50", "subCityId" => "3698"),
        "�������" => array("cityId" => "50", "subCityId" => "1197"),
        "������" => array("cityId" => "50", "subCityId" => "1201"),
        "��������" => array("cityId" => "50", "subCityId" => "1166"),
        "������" => array("cityId" => "50", "subCityId" => "1176"),
        "����-�������" => array("cityId" => "50", "subCityId" => "1186"),
        "������������" => array("cityId" => "50", "subCityId" => "1203"),
        "�������" => array("cityId" => "50", "subCityId" => "1170"),
        "��������" => array("cityId" => "50", "subCityId" => "1177"),
        "�����" => array("cityId" => "50", "subCityId" => "1182"),
        "�������" => array("cityId" => "50", "subCityId" => "1187"),
        "�������-�����" => array("cityId" => "50", "subCityId" => "1193"),
        "���������" => array("cityId" => "50", "subCityId" => "1173"),
        "�������" => array("cityId" => "50", "subCityId" => "1202"),
        "����������" => array("cityId" => "50", "subCityId" => "1171"),
        "����" => array("cityId" => "50", "subCityId" => "1178"),
        "��������" => array("cityId" => "50", "subCityId" => "1183"),
        "��������" => array("cityId" => "50", "subCityId" => "1188"),
        "��������" => array("cityId" => "50", "subCityId" => "1194"),
        "�����������" => array("cityId" => "50", "subCityId" => "1167"),
        "�����" => array("cityId" => "50", "subCityId" => "1172"),
        "�������" => array("cityId" => "50", "subCityId" => "1179"),
        "�������" => array("cityId" => "50", "subCityId" => "1185"),
        "�������-�����" => array("cityId" => "50", "subCityId" => "1189"),
        "�������������" => array("cityId" => "50", "subCityId" => "1195"),
        "�����������" => array("cityId" => "50", "subCityId" => "1168")
    );

    function __construct() {
        global $DATABASE;
        $this->InitDB();
    }

    function InitDB() {
        global $DATABASE;
        $this->db = new DB($DATABASE["HOSTNAME"], $DATABASE["DATABASENAME"], $DATABASE["DATABASEUSER"], $DATABASE["DATABASEPASSWORD"]);
    }    
    
    public function Result() {
 
        return $this->array_galleon;
    }

    private function ModerateContent($content) {

        return $content;
    }
 
    private function LoadImg($url, $pref = 'sell_') {
        
        $newname = uniqid($pref);
        $ext = substr($url, 1 + strrpos($url, "."));
         
        $result_name = CreateSubDirImg().'/'.$newname.".".$ext;
        
        file_put_contents($result_name, file_get_contents($url));
        
        return $result_name;
    }
    
    public function ConvertData() {

        $data = array();

        $data["FML"] =  htmlspecialchars($this->_contact, ENT_QUOTES);
        $data["EmailID"] = htmlspecialchars($this->_email, ENT_QUOTES);
        $data["PhoneID"] = htmlspecialchars($this->_phone, ENT_QUOTES);
        $data["NameBizID"] = htmlspecialchars(ucfirst(strtolower($this->_title)), ENT_QUOTES);


        if(isset($this->orgform_maps[$this->_orgform])) {
            $data["BizFormID"] = $this->orgform_maps[$this->_orgform];
        } else {
            $data["BizFormID"] = "";
        }

        if(isset($this->town_maps[$this->_town])) {
            $data["CityID"] = $this->town_maps[$this->town]["cityId"];
            $data["SubCityID"] = $this->town_maps[$this->town]["subCityId"];
        } else {
            $data["CityID"] = "";
            $data["SubCityID"] = "";
        }

        $data["SiteID"] = htmlspecialchars($this->_town, ENT_QUOTES);
        $data["CostID"] = intval($this->_price);
        $data["cCostID"] = "rub";
        $data["txtCostID"] = $this->_price;
        $data["txtProfitPerMonthID"] = "";
        $data["ProfitPerMonthID"] = $this->_profitpermonth;
        $data["cProfitPerMonthID"] = "rub";
        $data["cMaxProfitPerMonthID"] = "";
        $data["TimeInvestID"] = htmlspecialchars(ucfirst(strtolower($this->_timeinvest)), ENT_QUOTES);
        $data["ListObjectID"] = htmlspecialchars(ucfirst(strtolower($this->_listobject)), ENT_QUOTES); 
        $data["ContactID"] = "";//$this->_contact;
        $data["MatPropertyID"] = htmlspecialchars(ucfirst(strtolower($this->_matproperty)), ENT_QUOTES); 
        $data["txtMonthAveTurnID"] = "rub";
        $data["MonthAveTurnID"] = $this->_monthaveturn;
        $data["cMonthAveTurnID"] = "rub";
        $data["cMaxMonthAveTurnID"] = "";
        $data["txtMonthExpemseID"] = "";
        $data["MonthExpemseID"] = $this->_monthexpemse;
        $data["cMonthExpemseID"] = "rub";
        $data["txtSumDebtsID"] = "";
        $data["SumDebtsID"] = "";
        $data["cSumDebtsID"] = "";
        $data["txtWageFundID"] = "";
        $data["WageFundID"] = "";
        $data["cWageFundID"] = "";
        $data["ObligationID"] = "";
        $data["CountEmplID"] = $this->_countempl;
        $data["CountManEmplID"] = $this->_countmanempl;
        $data["TermBizID"] = $this->_termbiz;

        $data["ShortDetailsID"] = htmlspecialchars(ucfirst(strtolower($this->_description)), ENT_QUOTES); 
        $data["DetailsID"] = htmlspecialchars(ucfirst(strtolower($this->_description)), ENT_QUOTES); 


        $data["FaxID"] = ""; 
        $data["ShareSaleID"] = "";
        $data["ReasonSellBizID"] = "";
        $data["TarifID"] = ""; 
        $data["StatusID"] = "";
        $data["DataCreate"] = "";
        $data["DateStart"] = "";
        $data["TimeActive"] = "";


        $data["CategoryID"] =  (isset($this->category_maps[$this->_businesstype]) ? $this->category_maps[$this->_businesstype] : "");
        $data["SubCategoryID"] = "";
        $data["ConstGUID"] = "";
        $data["ip"] = "";
        $data["defaultUserCountry"] = "";
        $data["login"] = "";
        $data["password"] = "";
        $data["newspaper"] = "";
        $data["helpbroker"] = "";
        $data["youtubeURL"] = "";    		
        $data["brokerId"] = $this->brokerId;
        $data["txtReasonSellBizID"] = htmlspecialchars($this->_reason, ENT_QUOTES);

        //up Typographus

        $typo = new Typographus();

        $data["NameBizID"] = $typo->process($data["NameBizID"]);
        $data["SiteID"] = $typo->process($data["SiteID"]);
        $data["ListObjectID"] = $typo->process($data["ListObjectID"]);
        $data["MatPropertyID"] = $typo->process($data["MatPropertyID"]);
        $data["ShortDetailsID"] = $typo->process($data["ShortDetailsID"]);
        $data["DetailsID"] = $typo->process($data["DetailsID"]);

        //down Typographus    		

        if(count($this->_pic)) {
            
            if(isset($this->_pic[0])) {
                $data["ImgID"] = $this->LoadImg(trim($this->_pic[0]));
            } else {
                $data["ImgID"] = ''; 
            }

            if(isset($this->_pic[1])) {
                $data["Img2ID"] = $this->LoadImg(trim($this->_pic[1])); 
            } else {
                $data["Img2ID"] = '';
            }

            if(isset($this->_pic[2])) { 
                $data["Img3ID"] = $this->LoadImg(trim($this->_pic[2]));
            } else {
                $data["Img3ID"] = '';
            }
            
        } else {
            
            $data["ImgID"] = ''; 
            $data["Img2ID"] = ''; 
            $data["Img3ID"] = '';
        }
        
        return $data;
    }

    public function CallData() {

        $xml = simplexml_load_file($this->url);
        $this->ReadDataCallBack($xml);
    }


    private function convertXmlObjToArr($obj, &$arr) { 

        if(!is_object($obj)) throw new Exception("Bizzona.ru: ������ ������� ������ xml ����� �� �������� Galleon");

        $children = $obj->children(); 
        
        foreach ($children as $elementName => $node) { 
            
            $nextIdx = count($arr); 
            $arr[$nextIdx] = array(); 
            $arr[$nextIdx]['@name'] = strtolower((string)$elementName); 
            $arr[$nextIdx]['@attributes'] = array(); 
            $attributes = $node->attributes(); 
            foreach ($attributes as $attributeName => $attributeValue) { 
                $attribName = strtolower(trim((string)$attributeName)); 
                $attribVal = trim((string)$attributeValue); 
                $arr[$nextIdx]['@attributes'][$attribName] = $attribVal; 
            } 
            $text = (string)$node; 
            $text = trim($text); 
            if (strlen($text) > 0) { 
                $arr[$nextIdx]['@text'] = $text; 
            } 
            $arr[$nextIdx]['@children'] = array(); 
            $this->convertXmlObjToArr($node, $arr[$nextIdx]['@children']); 
        } 
        
        return; 
    }  		

    private function ReadDataCallBack($arr) {

        $res_arr = array();
        $this->convertXmlObjToArr($arr, $res_arr);	

        while (list($k, $v) = each($res_arr)) {

            $item = $res_arr[$k];

            $sub_item = $item["@children"];
 
            $_galleon = new Galleon(); 	

            while (list($k1, $v1) = each($sub_item)) {

                $arr = $v1["@attributes"];	

                $name = $arr["name"];
 
                $name = strtolower($name);
                
                if($name == "id") {
                    $_galleon->_id = $v1["@text"];
                }
                if($name == "dateupdate") {
                    $_galleon->_dateupdate = $v1["@text"];
                }
                if($name == "price") {
                    $_galleon->_price = $v1["@text"];
                }
                if($name == "profitpermonth") {
                    $_galleon->_profitpermonth = $v1["@text"];
                }
                if($name == "monthaveturn") {
                    $_galleon->_monthaveturn = $v1["@text"];
                }
                if($name == "monthexpemse") {
                    $_galleon->_monthexpemse = $v1["@text"];
                }
                if($name == "timeinvest") {
                    $_galleon->_timeinvest = $v1["@text"];
                }
                if($name == "listobject") {
                    $_galleon->_listobject = utf8_to_cp1251($v1["@text"]);
                }
                if($name == "matproperty") {
                    $_galleon->_matproperty = utf8_to_cp1251($v1["@text"]);
                }
                if($name == "countempl") {
                    $_galleon->_countempl = $v1["@text"];
                }
                if($name == "countmanempl") {
                    $_galleon->_countmanempl = $v1["@text"];
                }
                if($name == "termbiz") {
                    $_galleon->_termbiz = $v1["@text"];
                }
                if($name == "sharesale") {
                    $_galleon->_sharesale = $v1["@text"];
                }	
                if($name == "offertype") {
                    $_galleon->_offertype = utf8_to_cp1251($v1["@text"]);
                }
                if($name == "businesstype") {
                    $_galleon->_businesstype = utf8_to_cp1251($v1["@text"]);
                }
                if($name == "phone") {
                    $_galleon->_phone = $v1["@text"];
                }
                if($name == "contact") {
                    $_galleon->_contact = utf8_to_cp1251($v1["@text"]);
                }					
                if($name == "title") {
                    $_galleon->_title = utf8_to_cp1251($v1["@text"]);
                }
                if($name == "description") {
                    $_galleon->_description = utf8_to_cp1251($v1["@text"]);
                }
                if($name == "web") {
                    $_galleon->_urlBrokerObject = $v1["@text"];
                }
                if($name == "pic") {
                    $_galleon->_pic[] = $v1["@text"];
                }
                if($name == "email") {
                    $_galleon->_email = $v1["@text"];
                }
                if($name == "town") {
                    $_galleon->_town = utf8_to_cp1251($v1["@text"]);
                }
                if($name == "orgform") {
                    $_galleon->_orgform = utf8_to_cp1251($v1["@text"]);
                }
                if($name == "reason") {
                    $_galleon->_reason = utf8_to_cp1251($v1["@text"]);
                }
            }
  
            $this->array_galleon[] = $_galleon;	
        }
    }

    public function ReOpenData() {

        $this->CallData();
        $result = $this->Result();

        $mappingobjectbroker = new MappingObjectBroker();

        $mappingobjectbroker->brokerId = $this->brokerId;

        $a_Ids = array();

        while (list($k, $v) = each($result)) {
            $a_Ids[] = $v->_id;		
        }

        $mappingobjectbroker->arrayBrokerIds =  $a_Ids;
        $mappingobjectbroker->ReOpen();
    }

    private function InsertSell($data) {

        $sql = " insert into Sell (`FML`, 
                                                                `EmailID`, 
                                                                `PhoneID`,
                                                                `NameBizID`,
                                                                `BizFormID`, 
                                                                `CityID`,
                                                                `subCityID`, 
                                                                `SiteID`,
                                                                `CostID`,
                                                                `cCostID`,
                                                                `txtCostID`,
                                                                `txtProfitPerMonthID`,
                                                                `ProfitPerMonthID`,
                                                                `cProfitPerMonthID`,
                                                                `cMaxProfitPerMonthID`,
                                                                `TimeInvestID`, 
                                                                `ListObjectID`, 
                                                                `ContactID`,
                                                                `MatPropertyID`,
                                                                `txtMonthAveTurnID`,
                                                                `MonthAveTurnID`,
                                                                `cMonthAveTurnID`,
                                                                `cMaxMonthAveTurnID`,
                                                                `txtMonthExpemseID`, 
                                                                `MonthExpemseID`, 
                                                                `cMonthExpemseID`,
                                                                `txtSumDebtsID`,
                                                                `SumDebtsID`,
                                                                `cSumDebtsID`, 
                                                                `txtWageFundID`,
                                                                `WageFundID`,
                                                                `cWageFundID`,
                                                                `ObligationID`,
                                                                `CountEmplID`,
                                                                `CountManEmplID`,
                                                                `TermBizID`,
                                                                `ShortDetailsID`, 
                                                                `DetailsID`, 
                                                                `ImgID`,
                                                                `Img1ID`,
                                                                `Img2ID`,
                                                                `pdf`,
                                                                `FaxID`, 
                                                                `ShareSaleID`,
                                                                `ReasonSellBizID`,
                                                                `txtReasonSellBizID`,
                                                                `TarifID`, 
                                                                `StatusID`,
                                                                `DataCreate`,
                                                                `DateStart`,
                                                                `TimeActive`,
                                                                `CategoryID`,
                                                                `SubCategoryID`,
                                                                `ConstGUID`,
                                                                `ip`,
                                                                `defaultUserCountry`,
                                                                `login`,
                                                                `password`,
                                                                `newspaper`,
                                                                `helpbroker`,
                                                                `youtubeURL`,
                                                                `brokerId`)
                                                        values
                                                        ( 
                                                        '".$data["FML"]."',
                                                        '".$data["EmailID"]."',
                                                        '".$data["PhoneID"]."',
                                                        '".$data["NameBizID"]."',
                                                        '".$data["BizFormID"]."',
                                                        '".$data["CityID"]."',
                                                        '".$data["SubCityID"]."',
                                                        '".$data["SiteID"]."',
                                                        '".$data["CostID"]."',
                                                        '".$data["cCostID"]."',
                                                        '".$data["txtCostID"]."',
                                                        '".$data["txtProfitPerMonthID"]."',
                                                        '".$data["ProfitPerMonthID"]."',
                                                        '".$data["cProfitPerMonthID"]."',
                                                        '".$data["cProfitPerMonthID"]."',
                                                        '".$data["TimeInvestID"]."', 
                                                        '".$data["ListObjectID"]."',
                                                        '".$data["ContactID"]."',
                                                        '".$data["MatPropertyID"]."',
                                                        '".$data["txtMonthAveTurnID"]."',
                                                        '".$data["MonthAveTurnID"]."',
                                                        '".$data["cMonthAveTurnID"]."',
                                                        '".$data["cMonthAveTurnID"]."',
                                                        '".$data["txtMonthExpemseID"]."',
                                                        '".$data["MonthExpemseID"]."',
                                                        '".$data["cMonthExpemseID"]."',
                                                        '".$data["txtSumDebtsID"]."',
                                                        '".$data["SumDebtsID"]."',
                                                        '".$data["cSumDebtsID"]."',
                                                        '".$data["txtWageFundID"]."',
                                                        '".$data["WageFundID"]."',
                                                        '".$data["cWageFundID"]."',
                                                        '".$data["ObligationID"]."',
                                                        '".$data["CountEmplID"]."',
                                                        '".$data["CountManEmplID"]."',
                                                        '".$data["TermBizID"]."',
                                                        '".$data["ShortDetailsID"]."',
                                                        '".$data["DetailsID"]."',
                                                        '".$data["ImgID"]."',
                                                        '".$data["Img1ID"]."',
                                                        '".$data["Img2ID"]."',
                                                        '".$data["pdf"]."',
                                                        '".$data["FaxID"]."',
                                                        '".$data["ShareSaleID"]."',
                                                        '".$data["ReasonSellBizID"]."',
                                                        '".$data["txtReasonSellBizID"]."',
                                                        '".$data["TarifID"]."',
                                                        0,
                                                        NOW(),
                                                        NOW(),
                                                        0,
                                                        '".$data["CategoryID"]."',
                                                        '".$data["SubCategoryID"]."',
                                                        '".$data["ConstGUID"]."',
                                                        '".$_SERVER['REMOTE_ADDR']."',
                                                        '".$_SERVER['GEOIP_COUNTRY_NAME']."',
                                                        '".generate_password(15)."',
                                                        '".generate_password(15)."',
                                                        '".$data["newspaper"]."',
                                                        '".$data["helpbroker"]."',
                                        '".$data["youtubeURL"]."',
                                        '".$data["brokerId"]."'
                                                        );";


        $result = $this->db->Insert($sql);

        return $result;
    }
    
    public function ImportData() {

        $this->CallData();
        $result = $this->Result();

        $sell = new Sell();	
        $mappingobjectbroker = new MappingObjectBroker();	

        $iter = 0;

        while (list($k, $v) = each($result)) {

            if($iter == 3) {
                break; 
            }
 
            //echo "<pre>";
            //print_r($v->ConvertData());
            //echo "</pre>";
            
            $mappingobjectbroker->brokerObjectID = $v->_id;
            $mappingobjectbroker->brokerId = $this->brokerId;

            if(!$mappingobjectbroker->IsExistsBrokerObject()) {
 
                $mappingobjectbroker->bizzonaObjectId = $this->InsertSell($v->ConvertData());
                $mappingobjectbroker->brokerId = $this->brokerId;
                $mappingobjectbroker->brokerDateObject = HelpAdapter::unixToMySQL(strtotime($v->_dateupdate));
                $mappingobjectbroker->ObjectTypeId = 1;
                $mappingobjectbroker->urlBrokerObject = $v->_urlBrokerObject;
                $mappingobjectbroker->Insert();			

                if($mappingobjectbroker->bizzonaObjectId > 0) {
                    $this->countProcessed += 1;
                }

                $iter += 1;
            } 
        }
    }

    private function UpdateSell($data) {

        $where = " limit 1";

        $sql = "update LOW_PRIORITY Sell set  
              `FML`='".trim($data["FML"])."',
              `EmailID`='".trim($data["EmailID"])."',
              `PhoneID`='".trim($data["PhoneID"])."',
              `CostID`='".trim($data["CostID"])."',
              `cCostID`='".trim($data["cCostID"])."',
              `ProfitPerMonthID`='".trim($data["ProfitPerMonthID"])."',
              `cProfitPerMonthID`='".trim($data["cProfitPerMonthID"])."',
              `MonthAveTurnID`='".trim($data["MonthAveTurnID"])."',
              `cMonthAveTurnID`='".trim($data["cMonthAveTurnID"])."',
              `StatusID`=0 
               where ID='".intval(trim($data["ID"]))."' ".$where.";";


        $result = $this->db->Update($sql);

        return $result;
    }		
    
    
    public function UpdateData() {

        $this->CallData();
        $result = $this->Result();

        $sell = new Sell();

        $mappingobjectbroker = new MappingObjectBroker();
        $mappingobjectbroker->brokerId = $this->brokerId;
        $mappingobjectbroker->ObjectTypeId = 1;
        $array_object = $mappingobjectbroker->Select();

        $iter = 0;

        while (list($k, $v) = each($result)) {

            if($iter < 1) {

                while (list($k1, $v1) = each($array_object)) {
                    
                    if($v1["brokerObjectID"] == $v->_id) {

                        $v1_date = strtotime($v1["brokerDateObject"]);
                        $v_date = strtotime($v->_dateupdate); 

                        if($v1_date < $v_date) {

                            $data = $v->ConvertData();
                            $data["ID"] = $v1["bizzonaObjectId"];

                            $this->UpdateSell($data);
                            
                            $mappingobjectbroker->brokerDateObject = HelpAdapter::unixToMySQL(strtotime($v->_dateupdate));
                            $mappingobjectbroker->brokerObjectID = $v1["brokerObjectID"];
                            $mappingobjectbroker->UpdateBrokerDateObject(); 

                            $iter += 1;

                            $this->countProcessed += 1;
                        }

                        break;
                    }
                }

            }
            reset($array_object);
        }
    }

    private function UpdateStatusSell($data) {
        $sql = "update LOW_PRIORITY Sell set `StatusID`=".intval(trim($data["StatusID"]))."  where ID=".intval(trim($data["ID"]))." limit 1;";
        $result = $this->db->Update($sql);
        return $result;
    }
    
    public function DeleteData() {

        $sell = new Sell();
        $this->CallData();
        $result = $this->Result();
 
        $mappingobjectbroker = new MappingObjectBroker();
        $mappingobjectbroker->brokerId = $this->brokerId;
        $mappingobjectbroker->ObjectTypeId = 1;
        $array_object = $mappingobjectbroker->Select(); 

        $cache = new Cache_Lite();

        while (list($k, $v) = each($array_object)) {

            $status = false;	
            while (list($k1, $v1) = each($result)) {
                if(intval($v["brokerObjectID"]) == intval($v1->_id)) {
                        $status = true;
                        break;
                }
            }
            reset($result);

            if(!$status) {
                $data = array();
                $data["StatusID"] = 12;  
                $data["ID"] = $v["bizzonaObjectId"];
                $this->UpdateStatusSell($data);
                $this->countProcessed += 1;
                $cache->remove(''.$v["bizzonaObjectId"].'____sellitem');
            }
        }
    }
}
?>