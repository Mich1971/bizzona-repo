<?php
	include_once("interface.sellsdapter.inc");
	include_once("../engine/class.sell.inc");
	include_once("../engine/functions.inc"); 
	include_once("../engine/typographus.php"); 
	include_once("../engine/class.mappingobjectbroker.inc"); 	
	
	function save_image($inPath,$outPath) { 
		
		 $in = fopen($inPath, "rb");     
		 $out =  fopen($outPath, "wb");     
		 while ($chunk = fread($in, 8192)) {         
		 	fwrite($out, $chunk, 8192);     
		 }     
		 fclose($in);     
		 fclose($out); 
	} 	
	
	
    class Trio implements SellAdapter {
    	
    	public $_id;
		public $_DateUpdate;
		public $_Price;
		public $_ProfitPerMonth;
		public $_MaxProfitPerMonth;
		public $_MonthAveTurn;
		public $_MonthExpemse;
		public $_MaxMonthExpemse;
		public $_SumDebts;
		public $_WageFund;
		public $_RentCostId;
		public $_TimeInvest;
		public $_ListObject;
		public $_MatProperty;
		public $_CountEmpl;
		public $_CountManEmpl;
		public $_TermBiz;
		public $_ShareSale;
		public $_Contact;
		public $_offertype;
		public $_businesstype;
		public $_phone;
		public $_contact;
		public $_title;
		public $_description;
		public $_shortDescription;
		public $_web;
		public $_img1;
		public $_img2;
		public $_img3;
		public $_Obligation;
		public $_address;    	
		
		public $_email;
		public $_town;
		public $_orgform; 
		public $_reason;
		public $_torg;
		public $_fml;
		
		public $_brokerId;
    	
    	public $brokerId = 19; 
    	
    	public $url = "http://www.business-trio.ru/bizzona-export.php";
    	
    	private $array_trio = array();
    	
    	public $town_maps = array(
    								"1" => array("cityId" => "77", "subCityId" => "") /*������*/,
    								"2" => array("cityId" => "50", "subCityId" => "1164") /*���������*/,
    								"3" => array("cityId" => "50", "subCityId" => "1166") /*��������*/,
    								"4" => array("cityId" => "50", "subCityId" => "1170") /*�������*/,
    								"5" => array("cityId" => "50", "subCityId" => "1171") /*����������*/,
    								"6" => array("cityId" => "50", "subCityId" => "1172") /*�����*/,
    								"7" => array("cityId" => "50", "subCityId" => "1199") /*�����*/,
    								"8" => array("cityId" => "50", "subCityId" => "1176") /*������*/,
    								"9" => array("cityId" => "50", "subCityId" => "1177") /*��������*/,
    								"10" => array("cityId" => "50", "subCityId" => "1178") /*����*/,
    								"11" => array("cityId" => "50", "subCityId" => "1179") /*�������*/,
    								"12" => array("cityId" => "50", "subCityId" => "3699") /*�������*/,
    								"13" => array("cityId" => "50", "subCityId" => "1184") /*�������*/,
    								"14" => array("cityId" => "50", "subCityId" => "1182") /*�����*/,
    								"15" => array("cityId" => "50", "subCityId" => "1183") /*��������*/,
    								"16" => array("cityId" => "50", "subCityId" => "1185") /*�������*/,
    								"17" => array("cityId" => "50", "subCityId" => "3698") /*������*/,
    								"18" => array("cityId" => "50", "subCityId" => "1186") /*����-�������*/,
    								"19" => array("cityId" => "50", "subCityId" => "1187") /*�������*/,
    								"20" => array("cityId" => "50", "subCityId" => "1188") /*��������*/,
    								"21" => array("cityId" => "50", "subCityId" => "1189") /*�������-�����*/,
    								"22" => array("cityId" => "50", "subCityId" => "1191") /*��������*/,
    								"23" => array("cityId" => "50", "subCityId" => "1192") /*���������*/,
    								"24" => array("cityId" => "50", "subCityId" => "1193") /*�������-�����*/,
    								"25" => array("cityId" => "50", "subCityId" => "1194") /*��������*/,
    								"26" => array("cityId" => "50", "subCityId" => "1195") /*�������������*/,
    								"27" => array("cityId" => "50", "subCityId" => "1197") /*�������*/,
    								"28" => array("cityId" => "50", "subCityId" => "3700") /*���������������*/,
    								"29" => array("cityId" => "50", "subCityId" => "1173") /*���������*/,
    								"30" => array("cityId" => "50", "subCityId" => "1167") /*�����������*/,
    								"31" => array("cityId" => "50", "subCityId" => "1168") /*�����������*/,
    								"32" => array("cityId" => "50", "subCityId" => "1201") /*������*/,
    								"33" => array("cityId" => "50", "subCityId" => "1203") /*������������*/,
    								"34" => array("cityId" => "50", "subCityId" => "1202") /*�������*/,
    								"35" => array("cityId" => "78", "subCityId" => "") /*�����-���������*/,
    								"36" => array("cityId" => "50", "subCityId" => "") /*���������� �������*/,
    								"37" => array("cityId" => "23", "subCityId" => "") /*������������� ����*/,
    								"38" => array("cityId" => "23", "subCityId" => "928") /*���������*/
    							 );
    	
    	function __construct() {
    		
    	}
    	
    	public function Result() {
    		
    		return $this->array_trio;
    		
    	}
    	
    	
		private function ModerateContent($content) {
			
			return $content;
			
		}
    	
    	public function ConvertData() {

    		$data = array();
    		
			$data["FML"] =  htmlspecialchars($this->_fml, ENT_QUOTES);
			$data["EmailID"] = htmlspecialchars($this->_email, ENT_QUOTES);
			$data["PhoneID"] = htmlspecialchars($this->_phone, ENT_QUOTES);
			$data["NameBizID"] = htmlspecialchars(ucfirst(strtolower($this->_title)), ENT_QUOTES);
			
			$data["BizFormID"] = $this->_orgform;

			
			if(isset($this->town_maps[$this->_town])) {
				$data["CityID"] = $this->town_maps[$this->_town]["cityId"];
				$data["SubCityID"] = $this->town_maps[$this->_town]["subCityId"];
			} else {
				$data["CityID"] = "";
				$data["SubCityID"] = "";
			}
			
			
			$data["SiteID"] = htmlspecialchars($this->_address, ENT_QUOTES);
			$data["CostID"] = intval($this->_Price);
			$data["cCostID"] = "rub";
			$data["txtCostID"] = $this->_Price;
			$data["txtProfitPerMonthID"] = "";
			$data["ProfitPerMonthID"] = $this->_ProfitPerMonth;
			$data["cProfitPerMonthID"] = "rub";
			$data["cMaxProfitPerMonthID"] = $this->_MaxProfitPerMonth;
			$data["TimeInvestID"] = htmlspecialchars(ucfirst(strtolower($this->_TimeInvest)), ENT_QUOTES);
			$data["ListObjectID"] = htmlspecialchars(ucfirst(strtolower($this->_ListObject)), ENT_QUOTES); 
			$data["ContactID"] = htmlspecialchars(ucfirst(strtolower($this->_Contact)), ENT_QUOTES); 
			$data["MatPropertyID"] = htmlspecialchars(ucfirst(strtolower($this->_MatProperty)), ENT_QUOTES); 
			$data["txtMonthAveTurnID"] = "rub";
			$data["MonthAveTurnID"] = $this->_MonthAveTurn;
			$data["cMonthAveTurnID"] = "rub";
			$data["cMaxMonthAveTurnID"] = "";
			$data["txtMonthExpemseID"] = "";
			$data["MonthExpemseID"] = $this->_MonthExpemse;
			$data["cMonthExpemseID"] = "rub";
			$data["txtSumDebtsID"] = "";
			$data["SumDebtsID"] = $this->_SumDebts;
			$data["cSumDebtsID"] = "rub";
			$data["txtWageFundID"] = "";
			$data["WageFundID"] = $this->_WageFund;
			$data["cWageFundID"] = "rub";
			$data["ObligationID"] = htmlspecialchars(ucfirst(strtolower($this->_Obligation)), ENT_QUOTES);
			$data["CountEmplID"] = $this->_CountEmpl;
			$data["CountManEmplID"] = $this->_CountManEmpl;
			$data["TermBizID"] = $this->_TermBiz;
			
			
			$data["ShortDetailsID"] = htmlspecialchars(ucfirst($this->_shortDescription), ENT_QUOTES); 
			$data["DetailsID"] = htmlspecialchars(ucfirst($this->_description), ENT_QUOTES); 
			
			
			$data["FaxID"] = ""; 
			$data["ShareSaleID"] = htmlspecialchars(ucfirst(strtolower($this->_ShareSale)), ENT_QUOTES);
			$data["ReasonSellBizID"] = $this->_reason;
			$data["TarifID"] = ""; 
			$data["StatusID"] = "";
			$data["DataCreate"] = "";
			$data["DateStart"] = "";
			$data["TimeActive"] = "";
			
			$data["CategoryID"] =  (isset($this->category_maps[$this->_businesstype]) ? $this->category_maps[$this->_businesstype] : "");
			$data["SubCategoryID"] = "";
			$data["ConstGUID"] = "";
			$data["ip"] = "";
			$data["defaultUserCountry"] = "";
			$data["login"] = "";
			$data["password"] = "";
			$data["newspaper"] = "";
			$data["helpbroker"] = "";
			$data["youtubeURL"] = "";    		
    		$data["brokerId"] = $this->_brokerId;
    		$data["ReasonSellBizID"] = htmlspecialchars($this->_reason, ENT_QUOTES);
    		$data["AgreementCostID"] = $this->_torg;
    		
    		$data["ArendaCostID"] = intval($this->_RentCostId);
    		$data["cArendaCostID"] = "rub";
    		//_RentCostId
    		
			if(strlen(trim($this->_img1)) > 1) {
				$ext = end(explode('.', $this->_img1));
				if(strlen($ext) > 0 and  strlen($ext) < 5) {
					$fname_1 =  uniqid("trio_").".".$ext;
					save_image(trim($this->_img1), "../simg/".$fname_1);
					$data["ImgID"] = $fname_1;
				}
			} else {
				$data["ImgID"] = "";
			}
    		
			if(strlen(trim($this->_img2)) > 1) {
				$ext = end(explode('.', $this->_img2));
				if(strlen($ext) > 0 and  strlen($ext) < 5) {
					$fname_2 =  uniqid("trio_").".".$ext;
					save_image(trim($this->_img2), "../simg/".$fname_2);
					$data["Img1ID"] = $fname_2;
				}
			} else {
				$data["Img1ID"] = "";
			}
    		
			if(strlen(trim($this->_img3)) > 1) {
				$ext = end(explode('.', $this->_img3));
				if(strlen($ext) > 0 and  strlen($ext) < 5) {
					$fname_3 =  uniqid("trio_").".".$ext;
					save_image(trim($this->_img3), "../simg/".$fname_3);
					$data["Img2ID"] = $fname_3;
				}
			} else {
				$data["Img2ID"] = "";
			}
			
    		//up Typographus
    		
    		$typo = new Typographus();
    		
    		$data["NameBizID"] = $typo->process($data["NameBizID"]);
    		$data["SiteID"] = $typo->process($data["SiteID"]);
    		$data["ListObjectID"] = $typo->process($data["ListObjectID"]);
    		$data["MatPropertyID"] = $typo->process($data["MatPropertyID"]);
    		$data["ShortDetailsID"] = $typo->process($data["ShortDetailsID"]);
    		$data["DetailsID"] = $typo->process($data["DetailsID"]);
    		
    		//down Typographus    		
    		
    		
    		return $data;
    	}
    	
    	public function CallData() {
    		
    		$xml = simplexml_load_file($this->url);
    		$this->ReadDataCallBack($xml);
    		
    	}
    	
    	
		private function convertXmlObjToArr($obj, &$arr) 
		{ 
    		$children = $obj->children(); 
    		foreach ($children as $elementName => $node) 
    		{ 
        		$nextIdx = count($arr); 
        		$arr[$nextIdx] = array(); 
        		$arr[$nextIdx]['@name'] = strtolower((string)$elementName); 
        		$arr[$nextIdx]['@attributes'] = array(); 
        		$attributes = $node->attributes(); 
        		foreach ($attributes as $attributeName => $attributeValue) 
        		{ 
            		$attribName = strtolower(trim((string)$attributeName)); 
            		$attribVal = trim((string)$attributeValue); 
            		$arr[$nextIdx]['@attributes'][$attribName] = $attribVal; 
        		} 
        		$text = (string)$node; 
        		$text = trim($text); 
        		if (strlen($text) > 0) 
        		{ 
            		$arr[$nextIdx]['@text'] = $text; 
        		} 
        		$arr[$nextIdx]['@children'] = array(); 
        		$this->convertXmlObjToArr($node, $arr[$nextIdx]['@children']); 
    		} 
    		return; 
		}  		
		
		private function ReadDataCallBack($arr) {

			$res_arr = array();
			$this->convertXmlObjToArr($arr, $res_arr);	
			
			while (list($k, $v) = each($res_arr)) {
				
				$item = $res_arr[$k];
				
				
				$sub_item = $item["@children"];
				
				$_trio = new Trio(); 	
					
				while (list($k1, $v1) = each($sub_item)) {

					
					$name = $v1["@name"];
					
					if($name == "id") {
						$_trio->_id = $v1["@text"];
					}
					if($name == "datecreate") {
						$_trio->_DateUpdate = $v1["@text"];
					}
					if($name == "cost") {
						$_trio->_Price = $v1["@text"];
					}
					if($name == "profitpermonth") {
						$_trio->_ProfitPerMonth = $v1["@text"];
					}
					if($name == "maxprofitpermonth") {
						$_trio->_MaxProfitPerMonth = $v1["@text"];
					}					
					
					if($name == "monthexpemse") {
						$_trio->_MonthExpemse = $v1["@text"];
					}
					if($name == "timeinvest") {
						$_trio->_TimeInvest = $v1["@text"];
					}
					if($name == "listobject") {
						$_trio->_ListObject = utf8_to_cp1251($v1["@text"]);
					}
					if($name == "matproperty") {
						$_trio->_MatProperty = utf8_to_cp1251($v1["@text"]);
					}
					if($name == "countempl") {
						$_trio->_CountEmpl = $v1["@text"];
					}
					if($name == "countmanempl") {
						$_trio->_CountManEmpl = $v1["@text"];
					}
					if($name == "countmanempl") {
						$_trio->_TermBiz = $v1["@text"];
					}
					if($name == "sharesale") {
						$_trio->_ShareSale = $v1["@text"];
					}	
					if($name == "businesstype") {
						$_trio->_businesstype = utf8_to_cp1251($v1["@text"]);
					}
					if($name == "phone") {
						$_trio->_phone = $v1["@text"];
					}
					if($name == "name") {
						$_trio->_title = utf8_to_cp1251($v1["@text"]);
					}
					if($name == "description") {
						$_trio->_description = utf8_to_cp1251($v1["@text"]);
					}
					if($name == "shortDescription") {
						$_trio->_shortDescription = utf8_to_cp1251($v1["@text"]);
					}					
					if($name == "img1") {
						$_trio->_img1 = trim($v1["@text"]);
					}
					if($name == "img2") {
						$_trio->_img2 = trim($v1["@text"]);
					}					
					if($name == "img3") {
						$_trio->_img3 = trim($v1["@text"]);
					}					
					
					if($name == "email") {
						$_trio->_email = $v1["@text"];
					}
					
					if($name == "addressid") {
						$_trio->_town = $v1["@text"];
					}
					if($name == "bizformid") {
						$_trio->_orgform = utf8_to_cp1251($v1["@text"]);
					}
					if($name == "reasonsellbizid") {
						$_trio->_reason = $v1["@text"];
					}
					
					if($name == "brokerid") {
						$_trio->_brokerId = $v1["@text"];
					}					
					
					if($name == "contact") {
						$_trio->_Contact = utf8_to_cp1251($v1["@text"]);
					}

					if($name == "fml") {
						$_trio->_fml = utf8_to_cp1251($v1["@text"]);
					}					

					if($name == "rentcostid") {
						$_trio->_RentCostId = $v1["@text"];
					}
					if($name == "address") {
						$_trio->_address = utf8_to_cp1251($v1["@text"]);
					}
					if($name == "wagefund") {
						$_trio->_WageFund = $v1["@text"];
					}
					if($name == "obligation") {
						$_trio->_Obligation = utf8_to_cp1251($v1["@text"]);
					}
					if($name == "torg") {
						$_trio->_torg = $v1["@text"];
					}
				}
				
				$this->array_trio[] = $_trio;	

			}
	
		}
    	
    	public function ImportData() {
    		
    		$this->CallData();
    		$result = $this->Result();

    		//var_dump($result);
    		
    		$sell = new Sell();	
			$mappingobjectbroker = new MappingObjectBroker();	

			$iter = 0;
			
			while (list($k, $v) = each($result)) {
	
				if ($iter == 1) {
					break;
				}
 				
				$mappingobjectbroker->brokerObjectID = $v->_id;
				$mappingobjectbroker->brokerId = $v->_brokerId;
				
				if (!$mappingobjectbroker->IsExistsBrokerObject()) {

					$mappingobjectbroker->bizzonaObjectId = $sell->Trio_Insert($v->ConvertData());
					$mappingobjectbroker->brokerId = $v->_brokerId;
					$mappingobjectbroker->brokerDateObject = HelpAdapter::unixToMySQL(strtotime($v->_DateUpdate));
					$mappingobjectbroker->ObjectTypeId = 1;
					$mappingobjectbroker->Insert();
					
					$iter += 1;
				}
			}
    	}
    	
    	public function UpdateData() {
    		
    		$this->CallData();
    		$result = $this->Result();

    		
    		$sell = new Sell();
    		
			$mappingobjectbroker = new MappingObjectBroker();
			$mappingobjectbroker->brokerId = $this->brokerId;
			$mappingobjectbroker->ObjectTypeId = 1;
			$array_object = $mappingobjectbroker->Select();
			
			$iter = 0;
			
			
			while (list($k, $v) = each($result)) {
				
				if($iter < 1) {
				
					while (list($k1, $v1) = each($array_object)) {
						if($v1["brokerObjectID"] == $v->_id) {
							
							$v1_date = strtotime($v1["brokerDateObject"]);
							$v_date = strtotime($v->_DateUpdate); 

							if($v1_date < $v_date) {
							
								$data = $v->ConvertData();
								$data["ID"] = $v1["bizzonaObjectId"];
							
								$sell->Trio_Update($data);
							
								$mappingobjectbroker->brokerDateObject = HelpAdapter::unixToMySQL(strtotime($v->_DateUpdate));
								$mappingobjectbroker->brokerObjectID = $v1["brokerObjectID"];
								$mappingobjectbroker->UpdateBrokerDateObject(); 
							
								$iter += 1;
							}

							break;
						}
					}
				
				}
				reset($array_object);
			}
    	}
    	
    	public function DeleteData() {
    		
    		$sell = new Sell();
    		$this->CallData();
    		$result = $this->Result();
    		
			$mappingobjectbroker = new MappingObjectBroker();
			$mappingobjectbroker->brokerId = $this->brokerId;
			$mappingobjectbroker->ObjectTypeId = 1;
			$array_object = $mappingobjectbroker->Select(); 
			
			while (list($k, $v) = each($array_object)) {
				$status = false;	
				while (list($k1, $v1) = each($result)) {
					if($v["brokerObjectID"] == $v1->id) {
						$status = true;
						
					}
				}
				reset($result);
				
				if(!$status) {
			        //echo $v->bizzonaObjectId."<hr>";		
					$data = array();
					$data["StatusID"] = 12;
					$data["ID"] = $v["bizzonaObjectId"];
					$sell->Trio_UpdateStatusID($data);
				}
				
			}
    	}
    	
    }
	

?>