<?php
	include_once("class.Income.inc");  	
	include_once("class.Outcome.inc");  	

	//include_once("../phpset.inc");
	//include_once("./functions.inc"); 

	//AssignDataBaseSetting("../config.ini");
	
	class ControllerWallet {
		 
		public $_typeId;
		public $_codeId;
		public $_summ;
		public $_serviceId;
		public $_serviceActionId;
		public $_typePaymentId;
		public $_externPaymetCodeId;
		
		private $_income = null;
		private $_outcome = null;
		
		public function __construct() {
			$this->_income = new Income();
			$this->_outcome = new Outcome();
		}

		public function ListIncomeAdmin($aParams = array()) {
			return $this->_income->getListAdmin($aParams); 		 
		}
		
		public function ListOutcomeAdmin($aParams = array()) {

			return $this->_outcome->getListAdmin($aParams); 		
		}
		
		
		public function ListOutcome() {

			$this->_outcome->_typeId = $this->_typeId;
			$this->_outcome->_codeId = $this->_codeId;
			
			return $this->_outcome->getList(); 		
		}
		
		public function ListIncome() {
			$this->_income->_codeId = $this->_codeId;
			return $this->_income->getList();
		}
		
		public function GetBalance() {
			
			$this->_income->_typeId = $this->_typeId;
			$this->_income->_codeId = $this->_codeId;

			$this->_outcome->_typeId = $this->_typeId;
			$this->_outcome->_codeId = $this->_codeId;
			
			$resultBalance = $this->_income->getTotalBalance() - $this->_outcome->getTotalBalance();
			
			return $resultBalance;
		}
		
		public function BuyService() {
		
			$this->_outcome->_typeId = $this->_typeId;
			$this->_outcome->_codeId = $this->_codeId;
			$this->_outcome->_summ = $this->_summ;
			$this->_outcome->_serviceId = $this->_serviceId;
			$this->_outcome->_serviceActionId = $this->_serviceActionId;
			
			$this->_outcome->AddBalance();
				
		}
		
		public function AddBalance() {

			$this->_income->_typeId = $this->_typeId;
			$this->_income->_codeId = $this->_codeId;
			$this->_income->_summ = $this->_summ;
			$this->_income->_typePaymentId = $this->_typePaymentId;
			$this->_income->_externPaymetCodeId = $this->_externPaymetCodeId;
			
			$this->_income->AddBalance();
		}
		
	}
	
	//$wallet = new ControllerWallet();
	//$wallet->_codeId = 42007;
	//$wallet->_typeId = 'sell';
	
	//echo $wallet->GetBalance();
	
?>