<?
  global $smarty,
         $category;
// требуется авторизация
	require_once($_SERVER['DOCUMENT_ROOT'].'/inside/auth.php');
//

  if (isset($_POST["update"])) {
    $data = array();
    $data["ID"] = trim($_POST["ID"]);
    $data["Name"] = trim($_POST["Name"]);
    $data["Count"] = trim($_POST["Count"]);
    $category->Update($data);
    ?><span>Запись обновлена</span><?
  }

  if (isset($_GET["ID"])) {
    $data = array();
    $data["offset"] = 0;
    $data["rowCount"] = 0;
    $data["sort"] = "ID";
    $data["ID"] = $_GET["ID"];
    $result = $category->GetItem($data); 
    $smarty->assign("data", $result);

  } else {

    $data = array();
    $data["offset"] = 0;
    $data["rowCount"] = 0;
    $data["sort"] = "ID";
    $listCategory = $category->Select($data); 
    $result = $category->Select($data);
    $smarty->assign("data", $result);

  }
  
  $smarty->display("./category/update.tpl");

?>