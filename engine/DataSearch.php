<?php 
    ini_set("magic_quotes_gpc","off");

    global $DATABASE;

    include_once("../phpset.inc");
    include_once("./functions.inc"); 

    AssignDataBaseSetting("../config.ini");
// требуется авторизация
	require_once($_SERVER['DOCUMENT_ROOT'].'/inside/auth.php');
//

    ini_set("display_startup_errors", "on");
    ini_set("display_errors", "on");
    ini_set("register_globals", "on");

    include_once("./class.DataSearch.inc"); 
    $dataSearch = new DataSearch();

    include_once("./class.AdsAction.inc"); 
    $adsAction = new AdsAction();

    include_once("./class.AdsActionToObjects.inc"); 
    $adsActionToObjects = new AdsActionToObjects();
    
    
    require_once("../libs/Smarty.class.php");
    $smarty = new Smarty;
    $smarty->template_dir = "../templates";
    $smarty->compile_check = false;
    $smarty->caching = false;
    $smarty->compile_dir  = "../templates_c";
    $smarty->debugging = false;
    $smarty->clear_all_cache();

    $sSuccess = false;
    
    if(isset($_POST['bind'])) {
        $adsActionToObjects->Insert($_POST);
        unset($_POST);
        $sSuccess = true;
    }
    
    if(isset($_POST['search'])) {
    
        $aParams = array();
        $aParams['type'] = $_POST['type'];

        if(isset($_POST['datestart']) && $_POST['datestart'] != '') {
            $a_datestart = explode('/', $_POST['datestart']);
            $aParams['datestart'] = ''.$a_datestart[2].'-'.$a_datestart[0].'-'.$a_datestart[1];
        }

        if(isset($_POST['datestop']) && $_POST['datestop'] != '') {
            $a_datestop = explode('/', $_POST['datestop']);
            $aParams['datestop'] = ''.$a_datestop[2].'-'.$a_datestop[0].'-'.$a_datestop[1];
        }
        
        if(isset($_POST['broker'])) {
            $aParams['broker'] = 1;
        }

        if(isset($_POST['owner'])) {
            $aParams['owner'] = 1;
        }
        
        
        if(isset($_POST['coststart']) && $_POST['coststart'] != '') {
            $aParams['coststart'] = trim($_POST['coststart']);
        }

        if(isset($_POST['coststop']) && $_POST['coststop'] != '') {
            $aParams['coststop'] = trim($_POST['coststop']);
        }

        if(isset($_POST['costId']) && $_POST['costId'] != '') {
            $aParams['costId'] = $_POST['costId'];
        }
        
        $data = $dataSearch->Searching($aParams);

        $actionsdata = $adsAction->ShortSelect();
        
        $smarty->assign("data", $data);
        $smarty->assign("actionsdata", $actionsdata);
    }
    
    
    $smarty->assign('columns', array_chunk($dataSearch->showFields, 2));
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
        <LINK href="../general.css" type="text/css" rel="stylesheet">
        <meta http-equiv="content-type" content="text/html; charset=windows-1251"/> 
        <body>
<?
        $smarty->display("./mainmenu.tpl");
        
        if($sSuccess) {
            $smarty->display("./datasearch/success.tpl");    
        }
        
        $smarty->display("./datasearch/search.tpl");
        $smarty->display("./datasearch/list.tpl");
?>
        </body>
        </html>
<?
?>