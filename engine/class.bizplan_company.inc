<?php
 
  include_once("class.db.inc");  
  
  define("NEW_BIZPLAN_COMPANY", 0);
  define("OPEN_BIZPLAN_COMPANY", 1);
  define("CLOSE_BIZPLAN_COMPANY", 2);
  define("LOOKED_BIZPLAN_COMPANY", 3);
  

  class BizPlanCompany {
     var $db = "";
     var $id = "";
     var $name = "";
     var $description = "";
     var $status_id = "";
     var $email = "";
     var $phone = "";
     var $fml = "";
     var $count = "";
     

     function BizPlanCompany() {
       global $DATABASE;
       $this->InitDB();
     } 

     function InitDB() {
       global $DATABASE;
       $this->db = new DB($DATABASE["HOSTNAME"], $DATABASE["DATABASENAME"], $DATABASE["DATABASEUSER"], $DATABASE["DATABASEPASSWORD"]);
     }

     function Insert() {
     	
     	$sql = "insert into `bizplan_company`  (
     											`name`,
     											`description`,
     											`status_id`,
     											`email`,
     											`phone`,
     											`fml`
     											) 
     												values 
     										    (
     										      '".$this->name."',
     										      '".$this->description."',
     										      '".NEW_BIZPLAN_COMPANY."',
     										      '".$this->email."',
     										      '".$this->phone."',
     										      '".$this->fml."'
     										    ); ";
       	$result = $this->db->Insert($sql);
       	return $result;

     }

     function Select() {
		$sql = "select * from `bizplan_company`";
		$result = $this->db->Select($sql);

       $array = Array();
       while ($row = mysql_fetch_object($result)) {
         $obj = new stdclass();

         $obj->id = $row->id;
         $obj->name = $row->name;
		 $obj->description = $row->description;
		 $obj->status_id = $row->status_id;
		 $obj->count = $row->count;
		 if($row->status_id == NEW_BIZPLAN_COMPANY)
		 {
		 	$obj->status_text = "�����";
		 }
		 else if($row->status_id == OPEN_BIZPLAN_COMPANY)
		 {
		 	$obj->status_text = "������";
		 }
		 else if($row->status_id == CLOSE_BIZPLAN_COMPANY)
		 {
		 	$obj->status_text = "������";
		 }
		 else if($row->status_id == LOOKED_BIZPLAN_COMPANY)
		 {
		 	$obj->status_text = "����������";
		 }
         $array[] = $obj;
       }
       return $array;
     }

   
     function SelectByStatus($status_id)
     {
		$sql = "select * from `bizplan_company` where `bizplan_company`.`status_id`='".$status_id."'";
		$result = $this->db->Select($sql);

       $array = Array();
       while ($row = mysql_fetch_object($result)) {
         $obj = new stdclass();

         $obj->id = $row->id;
         $obj->name = $row->name;
		 $obj->description = $row->description;
		 $obj->count = $row->count;
         $array[] = $obj;
       }
       return $array;
     	
     }
     

     function Update() {
     	$sql = "update `bizplan_company` set 
     										`bizplan_company`.`name`='".$this->name."', 
     										`bizplan_company`.`description`='".$this->description."', 
     										`bizplan_company`.`status_id`='".$this->status_id."', 
     										`bizplan_company`.`email`='".$this->email."', 
     										`bizplan_company`.`phone`='".$this->phone."',
     										`bizplan_company`.`fml`='".$this->fml."'
     												where `bizplan_company`.`id`='".$this->id."'; ";
		$result = $this->db->Sql($sql);
     }

     
     function UpdateCount() {
       $sql = "update bizplan_company set  `count`=".$this->count."  where id='".$this->id."'";
       $result = $this->db->Delete($sql);
     }
     
     

     function Delete($data) {
     }


     function GetItem() {
		$sql = "select * from `bizplan_company` where id='".$this->id."';";
		$result = $this->db->Select($sql);
		$obj = new stdclass();
       	while ($row = mysql_fetch_object($result)) {
        	$obj->id = $row->id;
         	$obj->name = $row->name;
		 	$obj->description = $row->description;						     
		 	$obj->status_id = $row->status_id;
		 	$obj->email = $row->email;
		 	$obj->phone = $row->phone;
		 	$obj->fml = $row->fml;
       	}
		return $obj;
     }


  }
?>