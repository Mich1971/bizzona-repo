<?
  global $smarty,
         $country;
// требуется авторизация
	require_once($_SERVER['DOCUMENT_ROOT'].'/inside/auth.php');
//


  if (isset($_GET["option"])) {

     switch(trim($_GET["option"])) {

       case "delete": {
         $data["ID"] = trim($_GET["ID"]);
         $country->Delete($data);
       } break;

       case "update": {
       } break;

       default: {
       }

     }

  } 

  $data = array();
  $data["offset"] = 0;
  $data["rowCount"] = 0;
  $data["sort"] = "ID";

  $result = $country->Select($data);

  $smarty->assign("data", $result);
  $smarty->display("./country/select.tpl");

?>