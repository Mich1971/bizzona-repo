<?
  global $smarty,
         $sell,
         $city,
         $category;
// требуется авторизация
	require_once($_SERVER['DOCUMENT_ROOT'].'/inside/auth.php');
//


  if (isset($_GET["ID"])) {


    $data = array();
    $data["ID"] = intval($_GET["ID"]);
    $data["offset"] = 0;
    $data["rowCount"] = 0;
    $data["sort"] = "ID";
    $result = $sell->Select($data);
   
    $obj = $result[0];

    ////////////////////////////////////
    $countCity = 0;

    $sdata = Array();
    $sdata["offset"] = 0;
    $sdata["rowCount"] = 0;
    $sdata["sort"] = "ID";
    $sdata["CityID"] = $obj->CityID; 
    $sdata["StatusID"] = 1;  
    $arr = $sell->Select($sdata);
    $countCity = $countCity + sizeof($arr);

    $sdata = Array();
    $sdata["offset"] = 0;
    $sdata["rowCount"] = 0;
    $sdata["sort"] = "ID";
    $sdata["CityID"] = $obj->CityID; 
    $sdata["StatusID"] = 2;  
    $arr = $sell->Select($sdata);
    $countCity = $countCity + sizeof($arr);


    $d = array();
    $d["Count"] = $countCity;
    $d["ID"] = $obj->CityID;
    $city->UpdateCount($d);    


    $countCategory = 0;

    $sdata = Array();
    $sdata["offset"] = 0;
    $sdata["rowCount"] = 0;
    $sdata["sort"] = "ID";
    $sdata["CategoryID"] = $obj->CategoryID; 
    $sdata["StatusID"] = 1;  
    $arr = $sell->Select($sdata);
    $countCategory = $countCategory + sizeof($arr);

    $sdata = Array();
    $sdata["offset"] = 0;
    $sdata["rowCount"] = 0;
    $sdata["sort"] = "ID";
    $sdata["CategoryID"] = $obj->CategoryID; 
    $sdata["StatusID"] = 2;  
    $arr = $sell->Select($sdata);
    $countCategory = $countCategory + sizeof($arr);


    $d = array();
    $d["Count"] = $countCategory;
    $d["ID"] = $obj->CategoryID;
    $category->UpdateCount($d);    
   ////////////////////////////////

    $sell->Delete($data);
  }


  $data = array();
  $data["offset"] = 0;
  $data["rowCount"] = 0;
  $data["sort"] = "ID";

  $result = $sell->Select($data);

  $smarty->assign("data", $result);
  $smarty->display("./sell/delete.tpl");
?>