<?
	include_once("class.db.inc");
	require_once("Cache/Lite.php");
  
	class District {
		
		var $db = "";
		var $ID = "";
		var $regionID = "";
		var $name = "";
		var $description = "";
		var $seosell = "";
		var $seobuy = "";
		var $aGetName = array();
		var $aGetSeoSell = array();

		function District() {
			global $DATABASE;
			$this->InitDB();
		}

		function InitDB() {
			global $DATABASE;
			$this->db = new DB($DATABASE["HOSTNAME"], $DATABASE["DATABASENAME"], $DATABASE["DATABASEUSER"], $DATABASE["DATABASEPASSWORD"]);
		}

		function SelectByRegion() {
			
			if(intval($this->regionID) <= 0 ) {
				return array();  
			}
			
			$sql = "select * from District where regionID=".intval($this->regionID)."";
			
			
			$result = $this->db->Select($sql);
			$array = Array();
			while ($row = mysql_fetch_object($result)) {
				$obj = new stdclass();
				$obj->ID = $row->ID;
				$obj->regionID = $row->regionID;
				$obj->name = $row->name;
				$obj->description = $row->description;
				$obj->seosell = $row->seosell;
				$obj->seobuy = $row->seobuy;
				$array[] = $obj;
			}
			return $array;
		}
		
		
		function SelectActive() {
			
			$sql = "select 	District.ID, 
							District.name,
							District.seosell,
							count(District.ID) as 'count'
												from 	District,
		     											Sell	
															where 
																District.regionID=".$this->regionID."
																		and
																(Sell.CityID = ".$this->regionID." or Sell.subCityID = ".$this->regionID.")
																		and
																Sell.districtID = District.ID
																		and
																Sell.StatusID in (1,2)
																	GROUP BY District.ID ORDER BY count DESC;";

			$result = $this->db->Select($sql);
			$array = Array();
			while ($row = mysql_fetch_object($result)) {
				$obj = new stdclass();
				$obj->ID = $row->ID;
				$obj->name = $row->name;
				$obj->count = $row->count;
				$obj->seosell = $row->seosell;
				$array[] = $obj;
			}
			
			return $array;
			
		}

		
		function SelectActiveBuyer() {
			
			$sql = "select
						District.ID, 
						District.name,
						District.seobuy,
						count(District.ID) as 'count'
						
						from 	buyer, 
								buyerdistrict,
								District
						where 
						
							buyer.regionID=".$this->regionID." 
								and 
							buyer.statusID in (1)
								and
							buyerdistrict.buyerID=buyer.id
								and
							District.ID = buyerdistrict.districtID
							
								GROUP BY District.ID ORDER BY count DESC;";
			
			$result = $this->db->Select($sql);
			$array = Array();
			while ($row = mysql_fetch_object($result)) {
				$obj = new stdclass();
				$obj->ID = $row->ID;
				$obj->name = $row->name;
				$obj->count = $row->count;
				$obj->seobuy = $row->seobuy;
				$array[] = $obj;
			}
			return $array;			
			
		}
		
		function GetNameList() {

			$aName = array();
		
			$options = array(
    			'cacheDir' => PERSISTENTCACHE."/",
    			'lifeTime' => 2592000,
    			'pearErrorMode' => CACHE_LITE_ERROR_DIE
			);
		
			$cache = new Cache_Lite($options);
		
			if ($data = $cache->get('districtnames')) {
				
				$aName = unserialize($data);

			} else { 
		
				$sql = "Select ID, name from District;";
				$result = $this->db->Select($sql);
				while ($row = mysql_fetch_object($result)) {
					$aName[$row->ID] = $row->name;
				}
				$cache->save(serialize($aName));
			}
		
			return 	$aName; 			
			
		}
		
		function GetName()
		{
			if($this->aGetName[$this->ID]) {
				return $this->aGetName[$this->ID];
			} else {
			
				if(intval($this->ID) <= 0) {
					return "";
				}
				
				$result = $this->GetNameList();
				$this->aGetName[$this->ID] = $result[$this->ID];
				return $result[$this->ID];
			}
		}
		
		function GetSeoSellList() {
			
			$aSeoSell = array();
		
			$options = array(
    			'cacheDir' => PERSISTENTCACHE."/",
    			'lifeTime' => 2592000,
    			'pearErrorMode' => CACHE_LITE_ERROR_DIE
			);
		
			$cache = new Cache_Lite($options);
		
			if ($data = $cache->get('districtseoselllist')) {
			
				$aSeoSell = unserialize($data);

			} else { 
		
				$sql = "select ID, seosell from District;";
				$result = $this->db->Select($sql);
				while ($row = mysql_fetch_object($result)) {
					$aSeoSell[$row->ID] = $row->seosell;
				}
				$cache->save(serialize($aSeoSell));
			}
		
			return 	$aSeoSell;			
		}
		
		function GetSeoSell()
		{
			if(isset($this->aGetSeoSell[$this->ID])) {
				return $this->aGetSeoSell[$this->ID];
			} else {
				
				if(intval($this->ID) <= 0) {
					return  "";
				}
				
				$result = $this->GetSeoSellList();
				$this->aGetSeoSell[$this->ID] = $result[$this->ID];
				return $result[$this->ID];
			}
		}

		function GetSeoBuy()
		{
			if(intval($this->ID) <= 0) {
				return "";
			}
			
			$sql = "select seobuy from District where ID=".intval($this->ID)."";
			$result = $this->db->Select($sql);
			while ($row = mysql_fetch_object($result)) {
				return $row->seobuy;
			}
		}
		
		
		function GetDescription()
		{
			if(intval($this->ID) <= 0) {
				return "";
			}
			
			$sql = "select description from District where ID=".intval($this->ID)."";
			$result = $this->db->Select($sql);
			while ($row = mysql_fetch_object($result)) {
				return $row->description;
			}
		}
		
		function Close() {
			$this->db->Close();
		}
		
	}
?>