<?
  global $smarty,
         $sell;
// требуется авторизация
	require_once($_SERVER['DOCUMENT_ROOT'].'/inside/auth.php');
//

  $data = array();
  $data["offset"] = 0;
  $data["rowCount"] = 0;
  $data["sort"] = "ID";

  $result = $sell->Select($data);
  $smarty->assign("data", $result);

  $smarty->display("./sell/select.tpl");

?>