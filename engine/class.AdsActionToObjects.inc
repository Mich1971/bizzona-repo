<?php

    include_once("class.db.inc");

    class AdsActionToObjects {
    
        public $_db = null;
        public $_sql;

        public $_id;
        public $_adsActionId;
        public $_objectId;
        public $_email;
        public $_statusId;
        public $_created;
        public $_updated;
        public $_typeId;
        
        function __construct() {
            global $DATABASE;
            $this->InitDB();
        }

        function InitDB() {

            global $DATABASE;

            $this->db = new DB($DATABASE["HOSTNAME"], $DATABASE["DATABASENAME"], $DATABASE["DATABASEUSER"], $DATABASE["DATABASEPASSWORD"]);
        }

        function __destruct() {

        }
        
        public function Select($aParamas) {
            
            $where = "  ";
            
            if(isset($aParamas['statusId'])) {
                $where .=  " and statusId=".(int)$aParamas['statusId']." ";
            }

            if(isset($aParamas['typeId'])) {
                $where .=  " and typeId=".(int)$aParamas['typeId']." ";
            }

            if(isset($aParamas['adsActionId'])) {
                $where .=  " and adsActionId=".(int)$aParamas['adsActionId']." ";
            }
            
            $this->_sql = "select AdsActionToObjects.*, AdsAction.subject from AdsActionToObjects, AdsAction where 1 and AdsActionToObjects.adsActionId=AdsAction.id ".$where."  order by id desc ;";
            $result = $this->db->Select($this->_sql);
            
            $arr = array();	
            
            while ($row = mysql_fetch_object($result)) {

                    $aRow = array();
                    $aRow['id'] = $row->id;
                    $aRow['subject'] = $row->subject;
                    $aRow['objectId'] = $row->objectId;
                    $aRow['email'] = $row->email;
                    
                    if($row->statusId == 1) {
                        $aRow['statusId'] = '���������';
                    } else {
                        $aRow['statusId'] = '� �������';
                    }
                    
                    
                    
                    $aRow['created'] = $row->created;
                    $aRow['updated'] = $row->updated;
                    if($row->typeId == 1) {
                        $aRow['typeId'] = '������� �������';
                    } else {
                        $aRow['typeId'] = $row->typeId;
                    }
                    
                    array_push($arr, $aRow);
            }

            return $arr;		
            
            
        }
        
        public function Insert($data) {
            
            foreach ($data['selected'] as $k => $v) {
                
                $this->_sql = "insert into AdsActionToObjects (
                                                                `adsActionId`,
                                                                `objectId`, 
                                                                `email`, 
                                                                `created`, 
                                                                `typeId`) 
                                                                values
                                                                (
                                                                 ".$data['adsactionId'].",
                                                                 ".$k.",    
                                                                 '".trim($v)."',
                                                                 NOW(),
                                                                 '".$data['type']."'
                                                                );";
                
                //echo $this->_sql;
                //echo '<hr>';
                
                $id = $this->db->Insert($this->_sql);
                
            }
            
        }
        
    }

?>
