<?php
    ini_set("magic_quotes_gpc","off");

    global $DATABASE;

    include_once("../phpset.inc");
    include_once("../engine/functions.inc"); 
    AssignDataBaseSetting("../config.ini");
// ��������� �����������
	require_once($_SERVER['DOCUMENT_ROOT'].'/inside/auth.php');
//


    //ini_set("display_startup_errors", "on");
    //ini_set("display_errors", "on");
    //ini_set("register_globals", "on");


    include_once("./observer_filter.php"); 
    filterSet('company');

    require_once("./Cache/Lite.php");    
    
    require_once("../libs/Smarty.class.php");
    $smarty = new Smarty;
    $smarty->template_dir = "../templates";
    $smarty->compile_check = false;
    $smarty->caching = false;
    $smarty->compile_dir  = "../templates_c";
    $smarty->cache_dir = "../cache";
    $smarty->debugging = false;
    $smarty->clear_all_cache();


    include("../engine/class.company.inc") ;
    $company = new Company();

    include("../engine/class.services.inc") ;
    $services = new Services();
	
    function ReadFormParametersAutoUpdate(&$company) {

        $company->auto_update =  (isset($_POST["auto_update"]) ? 1 : 0);

        $a_auto_update_datestart = split('/', $_POST['auto_update_datestart']);
        $company->auto_update_datestart = $a_auto_update_datestart[2].'-'.$a_auto_update_datestart[0].'-'.$a_auto_update_datestart[1];

        $a_auto_update_datestop = split('/', $_POST['auto_update_datestop']);
        $company->auto_update_datestop = $a_auto_update_datestop[2].'-'.$a_auto_update_datestop[0].'-'.$a_auto_update_datestop[1];

        $company->auto_update_period = intval($_POST["auto_update_period"]);
        $company->count_per_update = intval($_POST["count_per_update"]);
    }
	
	
    if(isset($_GET['up']) or isset($_GET['down'])) {

        $company->Id = (intval($_GET['up']) > 0 ? $_GET['up'] : intval($_GET['down']));

        $type = 'up';
        if(isset($_GET['down'])) {
                $type = 'down';	
        }

        $company->UpdateOrderNum($type);

    }

    if (isset($_GET["ID"])) {
        $company->Id = intval($_GET["ID"]);
        $old_accessId = $company->GetAccess();
    }
?>

<LINK href="../general.css" type="text/css" rel="stylesheet">

<head>
</head>
<meta http-equiv="content-type" content="text/html; charset=windows-1251"/>

<body>

<?php	
	function LoadFile(&$company, $namefile, $field = "logo_partner") {
		global $_FILES;
		if(isset($_FILES[$namefile])) {
			$newname = uniqid("logo_");
			if (is_uploaded_file($_FILES[$namefile]['tmp_name'])) {
				$filename = $_FILES[$namefile]['tmp_name'];
				$ext = substr(strrchr($_FILES[$namefile]['name'],'.'), 1);
				if(move_uploaded_file($filename, '../imgbroker/'.$newname.'.'.$ext)){ 
					$company->$field = $newname.'.'.$ext;
				}
			}
		}			
	}

	if (isset($_POST) && sizeof($_POST) > 0 && !isset($_POST['handorder'])) {

		if(intval($_POST["ID"]) > 0) {
			
			$company->Id = intval($_POST["ID"]);
			
			if(isset($_POST["accessId"])) {
				$company->accessId = 2;	
			} else {
				$company->accessId = 0;	
			}

                        if($company->accessId == 2) {
                            
                            $aParams = array();
                            $aParams['brokerId'] = $company->Id;
                            $aParams['fromStatusId'] = 1;
                            $aParams['toStatusId'] = 250;
                            $resultCount = $company->ChangeStatusObjects($aParams);
                            
                            $resultText = "���� ������������� ������������� {$resultCount} ����������, ������ ������� ����� ����������� <a href='http://www.bizzona.ru/engine/UpdateSell.php?action=UpdateSell&rowCount=30&PageSplit=-10&StatusID=1&brokerId=".$company->Id."&StatusID=250' target='_blank'>�����</a> ";
                            $smarty->assign('infobloker', $resultText);
                            
                            $aObjects = $company->GetListObjects($company->Id);
                            resetcache($aObjects);
                            
                            
                        } else if ($company->accessId == 0) {
                            
                            $aParams = array();
                            $aParams['brokerId'] = $company->Id;
                            $aParams['fromStatusId'] = 250;
                            $aParams['toStatusId'] = 1;
                            $resultCount = $company->ChangeStatusObjects($aParams);

                            if($old_accessId != $company->accessId) {
     
                                $maxOrderNum = $company->GetMaxOrderNum() + 1;
                                 
                                $company->CustomUpdateOrderNum(array(
                                    'order_num' => $maxOrderNum, 
                                    'Id' => $company->Id
                                ));
                                
                                $resultText = "���� ������������� �������������� {$resultCount} ����������, ������ ������� ����� ����������� <a href='http://www.bizzona.ru/engine/UpdateSell.php?action=UpdateSell&rowCount=30&PageSplit=-10&StatusID=1&brokerId=".$company->Id."&StatusID=1' target='_blank'>�����</a> ";
                                $smarty->assign('infobloker', $resultText);

                                $aObjects = $company->GetListObjects($company->Id);
                                resetcache($aObjects);
                                
                            }
                            
                        }
			
			$company->companyName = trim($_POST["companyName"]);
			$company->shortDescription = trim($_POST["shortDescription"]);
			$company->legalAddress = trim($_POST["legalAddress"]);
			$company->actualAddress = trim($_POST["actualAddress"]);
			$company->email = trim($_POST["emailCompany"]);
			
			$company->phoneCompany = trim($_POST["phoneCompany"]);
			$company->faxCompany = trim($_POST["faxCompany"]);
			$company->innCompany = trim($_POST["innCompany"]);
			$company->kppCompany = trim($_POST["kppCompany"]);

			$company->nameBank = trim($_POST["nameBank"]);
			$company->ksBank = trim($_POST["ksBank"]);
			$company->rsBank = trim($_POST["rsBank"]);
			$company->bikBank = trim($_POST["bikBank"]);
			
			$company->tariffId = trim($_POST["tariffId"]);
			
			$company->login = trim($_POST["login"]);
			$company->password = trim($_POST["password"]);
			$company->show_in_partner = trim($_POST["show_in_partner"]);
			$company->site_company = trim($_POST["site_company"]);
			//$company->order_num = intval($_POST["order_num"]);
			
			$company->block_links = trim($_POST["block_links"]);
			$company->prefix_name_company = trim($_POST["prefix_name_company"]);
			$company->alt_seo_text = trim($_POST["alt_seo_text"]);
			$company->type_company = trim($_POST["type_company"]);

			
	 		$company->NameForSite = trim($_POST["NameForSite"]);
			$company->about_forsite = trim($_POST["about_forsite"]);
	 		$company->contact_forsite = trim($_POST["contact_forsite"]);
	 		$company->service_forsite = trim($_POST["service_forsite"]);
                        
                        $company->access_call_robot = (isset($_POST["access_call_robot"]) ? 1 : 0);
                        $company->url_import_robot = trim($_POST["url_import_robot"]);
                        
			
			ReadFormParametersAutoUpdate($company);
			
			
			LoadFile($company, 'logo_partner', 'logo_partner');
			LoadFile($company, 'logo_in_list', 'logo_in_list');
			LoadFile($company, 'logo_in_sublist', 'logo_in_sublist');
			
			$a_startDate = split('/', $_POST['datestart']);
			$a_stopDate = split('/', $_POST['datestop']);		
		
			$company->datestart = $a_startDate[2].'-'.$a_startDate[0].'-'.$a_startDate[1];
			$company->datestop = $a_stopDate[2].'-'.$a_stopDate[0].'-'.$a_stopDate[1];
                        
			$company->Update();
			
		}  else {
		
			$company->companyName = trim($_POST["companyName"]);
			$company->shortDescription = trim($_POST["shortDescription"]);
			$company->legalAddress = trim($_POST["legalAddress"]);
			$company->actualAddress = trim($_POST["actualAddress"]);
			$company->email = trim($_POST["emailCompany"]);
			
			$company->phoneCompany = trim($_POST["phoneCompany"]);
			$company->faxCompany = trim($_POST["faxCompany"]);
			$company->innCompany = trim($_POST["innCompany"]);
			$company->kppCompany = trim($_POST["kppCompany"]);

			$company->nameBank = trim($_POST["nameBank"]);
			$company->ksBank = trim($_POST["ksBank"]);
			$company->rsBank = trim($_POST["rsBank"]);
			$company->bikBank = trim($_POST["bikBank"]);
			
			$company->login = trim($_POST["login"]);
			$company->password = trim($_POST["password"]);			
			$company->show_in_partner = trim($_POST["show_in_partner"]);
			$company->site_company = trim($_POST["site_company"]);
			$company->order_num = intval($_POST["order_num"]);
			$company->block_links = trim($_POST["block_links"]);
			$company->prefix_name_company = trim($_POST["prefix_name_company"]);
			$company->alt_seo_text = trim($_POST["alt_seo_text"]);
			$company->type_company = trim($_POST["type_company"]);
			
			LoadFile($company, 'logo_partner', 'logo_partner');
			LoadFile($company, 'logo_in_list', 'logo_in_list');
			LoadFile($company, 'logo_in_sublist', 'logo_in_sublist');

			$a_startDate = split('/', $_POST['datestart']);
			$a_stopDate = split('/', $_POST['datestop']);		
			
			$company->datestart = $a_startDate[2].'-'.$a_startDate[0].'-'.$a_startDate[1];
			$company->datestop = $a_stopDate[2].'-'.$a_stopDate[0].'-'.$a_stopDate[1];			

	 		$company->NameForSite = trim($_POST["NameForSite"]);
			$company->about_forsite = trim($_POST["about_forsite"]);
	 		$company->contact_forsite = trim($_POST["contact_forsite"]);
	 		$company->service_forsite = trim($_POST["service_forsite"]);

                        $company->access_call_robot = (isset($_POST["access_call_robot"]) ? 1 : 0);
                        $company->url_import_robot = trim($_POST["url_import_robot"]);
                        
			ReadFormParametersAutoUpdate($company);	 		
			
                        $company->order_num = $company->GetMaxOrderNum()+1;
                        
			$company->Insert();
			
			$_GET["ID"] = $_POST["ID"] = $company->Id;
			 
			?>
				<script language="javascript">
					window.location = "http://www.bizzona.ru/engine/UpdateCompany.php?ID=<?=$_GET["ID"];?>";
				</script>
			<?php
		}
	} else if (isset($_POST) && sizeof($_POST) > 0 && isset($_POST['handorder'])) { 
                    
            $aParams = array();
            $aParams['Id'] = (int)$_POST['handorder'];
            $aParams['order_num'] = (int)$_POST['order_num'];
            
            $company->CustomUpdateOrderNum($aParams);
            
        }	

	AssignDescription("../config.ini");	
	
	if (isset($_GET["ID"])) {
		$company->Id = intval($_GET["ID"]);
		$company->GetItem();
		
		$smarty->assign("data", $company);
		
		if(strlen($company->login) <= 0) {
			$company->login = generate_password(15);
		}
		
		if(strlen($company->password) <= 0) {
			$company->password = generate_password(15);
		}
		

		$s_datatariff = $services->SelectIsBrokerShow();
		$smarty->assign("s_datatariff", $s_datatariff);
		
		global $data;
		$data = $company;
		$smarty->display("./company/company_edit.tpl");			
	} else {
		
		if(isset($_GET['accessId'])) {
			$company->accessId = $_GET['accessId'];
		}
		
		$list = $company->Select();
		$smarty->assign("list", $list);
		$smarty->display("./company/company_list.tpl");			
	}
        
        
        
	function resetcache($aIds) {
		
            global $smarty;

            foreach($aIds  as $k => $v) {
            
                // up update cache sell
                $smarty->cache_dir = "../sellcache";
                $useMapGoogle = true;
                $smarty->clear_cache("./site/detailsSell.tpl", intval($v)."-".$useMapGoogle);
                $useMapGoogle = false;
                $smarty->clear_cache("./site/detailsSell.tpl", intval($v)."-".$useMapGoogle);
                $smarty->cache_dir = "../cache";

                $options = array(
                        'cacheDir' => "../sellcache/",
                        'lifeTime' => 1
                );

                $cache = new Cache_Lite($options);

                $cache->remove(''.intval($v).'____sellitem');
                // up update cache sell
            }
	}        
        
        
        
?>
</body>
