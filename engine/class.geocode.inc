<?php
	include_once("class.db.inc");

	define("ACCURACY_UNKNOWN", 0);
	define("ACCURACY_COUNTRY", 1);
	define("ACCURACY_REGION", 2);
	define("ACCURACY_SUBREGION", 3);
	define("ACCURACY_TOWN", 4);
	define("ACCURACY_POSTCODE", 5);
	define("ACCURACY_STREET", 6);
	define("ACCURACY_INTERSECTION", 7);
	define("ACCURACY_ADDRESS", 8);

	define("G_GEO_SUCCESS", 200);
    define("G_GEO_BAD_REQUEST", 400);
    define("G_GEO_SERVER_ERROR", 500);
    define("G_GEO_MISSING_QUERY", 601);
    define("G_GEO_MISSING_ADDRESS", 601);
    define("G_GEO_UNKNOWN_ADDRESS", 602);
    define("G_GEO_UNAVAILABLE_ADDRESS", 603);
    define("G_GEO_UNKNOWN_DIRECTIONS", 604);
    define("G_GEO_BAD_KEY", 610);
    define("G_GEO_TOO_MANY_QUERIES", 620);
	
	$stringXml = "";
    
	class GeoCode {

		var $id = "";
		var $typeServiceId = "";
		var $latitude = "";
		var $longitude = "";
		var $accuracy = "";
		var $status = "";
		var $serviceId = "";
		var $zoom = "";
		var $sql = "";
		
		var $googleHost = "maps.google.com";
		var $googlePort = "80";
		var $googleKey = "ABQIAAAAnavXIsM5jdni-p-JoKqdIxTPIkIa5BjXbMz0WvIzZffrVGiazxTRu-mHw0IpP9Ib5fP2129LbcNtTw";
		var $tempfile = "";
		var $pathfile = "";
		var $address = "";
		
		var $resultStringXml = "";
		var	$level = 0;                           
		var	$list = array();
		var $errorString = "";
		
		var $regionSell = "";
		var $subRegionSell = "";
		var $categorySell = "";
		var $subCategorySell = "";
		
		
    	function GeoCode() {
       		global $DATABASE;
       		$this->InitDB();
    	} 

     	function InitDB() {
       		global $DATABASE;
       		$this->db = new DB($DATABASE["HOSTNAME"], $DATABASE["DATABASENAME"], $DATABASE["DATABASEUSER"], $DATABASE["DATABASEPASSWORD"]);
     	}
     	
     	function Close() {
     		$this->db->Close();
     	}

		function Insert() {
		
			$this->CallGoogleService();
			
			$sql = "replace  into   geocode 	(
												`typeServiceId`,
												`latitude`,
												`longitude`,
												`accuracy`,
												`status`,
												`serviceId`,
												`zoom`
												)
												values (
												'".$this->typeServiceId."',
												'".$this->latitude."',
												'".$this->longitude."',
												'".$this->accuracy."',
												'".$this->status."',
												'".$this->serviceId."',
												'".$this->zoom."'
												)";
       		$result = $this->db->Insert($sql);
       		return $result;
			
		}
		
		function CallGoogleService() {
			$fp = fsockopen($this->googleHost, $this->googlePort);
			if($fp) {
				//$this->address = ruslat($this->address);
				//$this->address = str_replace(" ","+", $this->address);
 			    $this->address = urlencode(cp1251_to_utf8($this->address));	

			    $addr = "/maps/geo?q=".$this->address."&output=xml&key=".$this->googleKey."&oe=utf-8";
				//echo "<br>".$addr."<br>";
				fputs($fp, "GET ".$addr."\r\n HTTP/1.1\r\n");
   				fputs($fp, "Host: ".$this->googleHost."\r\n");
   				fputs($fp, "Content-type: application/x-www-form-urlencoded\r\n\r\n");
   				fputs($fp, "User-Agent: MSIE\r\n");
   				fputs($fp, "Connection: Close\r\n\r\n");
				
				$request = ""; 
				while (!feof($fp)) {
                                   $request = $request.fgets($fp,128);
                                }
                                //mail("eugenekurilov@gmail.com","google", $request);
                                
				fclose($fp); 
    			sscanf($request, '%s %s %s', $protocol, $status_code, $description);
    			if($status_code == '404') {
					$this->errorString = "Error to call google service: 404";
				} else {
					$this->tempfile = tempnam($this->pathfile, "gmaps_");
					$handle = fopen($this->tempfile, "w");
					
					$pos = strpos($request, "<");	
					$request = substr($request,$pos, strlen($request) - $pos);
					
					fwrite($handle, $request);
					fclose($handle);
					chmod($this->tempfile, 0755);
				}
				
				
			}
			$this->XmlFileParsing();
		}
		
		function characterhandler ($parser, $data)	{
			global $stringXml;
			$stringXml .= ":".$data;
        	$stringXml .= "|";
		}


		function starthandler ($parser, $name, $attribs)
		{
			global $stringXml;
			$this->list[] = $name;
        		
			$stringXml .= "|".$name;

			foreach ($attribs as $atname => $val)
			{
				$stringXml .= "|".$atname.":".$val;
			}

			$this->level += 1;
		}
		
		function endhandler ($parser, $name)
		{
			array_pop($this->list);
			$this->level -= 1;

		}

		function XmlFileParsing() 
		{
    		$parser = xml_parser_create();
    		if (!$parser)
    		{
        		//exit ("�� ���� ������� ������");
        		$this->errorString = "Error to creating parser xml";
    		}

			xml_set_element_handler ( $parser, array ( $this, 'starthandler' ), array ( $this, 'endhandler' ) );
    		
    		xml_set_character_data_handler($parser, array ( $this, 'characterhandler' ));
    
    		$fp = fopen ($this->tempfile, "r");
    		if (!$fp)
    		{
        		xml_parser_free($parser);
        		//exit("�� ���� ������� ����");
        		$this->errorString = "Error open a temporary file";
    		}
    
    		while ($data = fread($fp, 4096)) 
    		{
        		if (!xml_parse($parser, $data, feof($fp))) 
        		{   /*
                	die(sprintf("�������� �����: %s � ������ %d",
                            xml_error_string(xml_get_error_code($parser)),
                            xml_get_current_line_number($parser)));
                    */         
        		}
    		}

    		
    		fclose ($fp);
    		xml_parser_free($parser);    		

    		unlink($this->tempfile);
   		
			global $stringXml;
			$this->resultStringXml = $stringXml;

			$res_arr = array();
    
			$arr = split("\|", $this->resultStringXml);

			
			while (list($k, $v) = each($arr)) {
				$t = split(":",$v);    
				if(sizeof($t) == 2) {
					$res_arr[trim($t[0])] = trim($t[1]);
				}
			}
    		
			while (list($k, $v) = each($res_arr)) {
				if($k == "CODE") {
					$this->status = $v;
				} else if ($k == "ACCURACY") {
					$this->accuracy = $v;
				} else if ($k == "COORDINATES") {
					$tc = split(",",$v);
					if(sizeof($tc) == 3) {
						$this->latitude = $tc[0];
						$this->longitude =  $tc[1];
					}
				}
			}
		}
		
		function GetData() {
		
			$sql = "select  id, 
							latitude,
							longitude,
							accuracy,
							status,
							zoom 
							from geocode where 1 and serviceId=".intval($this->serviceId)." and typeServiceId=".$this->typeServiceId."";
			//echo $sql;
			$result = $this->db->Select($sql);

			$obj = new stdclass();
			while ($row = mysql_fetch_object($result)) {
				$obj->id = $row->id;
				$obj->latitude = $row->latitude;
				$obj->longitude = $row->longitude;
				$obj->accuracy = $row->accuracy;
				$obj->status = $row->status;
				$obj->zoom = $row->zoom;
			}
			return $obj;				
		}
		
		function SelectAll() {
			$sql = "select  id, 
							latitude,
							longitude,
							accuracy,
							status,
							zoom 
							from geocode where 1 and typeServiceId=1";
			$result = $this->db->Select($sql);

			$arr = array();
			while ($row = mysql_fetch_object($result)) {
				$obj = new stdclass();
				$obj->id = $row->id;
				$obj->latitude = $row->latitude;
				$obj->longitude = $row->longitude;
				$obj->accuracy = $row->accuracy;
				$obj->status = $row->status;
				$obj->zoom = $row->zoom;
				$arr[] = $obj;
			}
			return $arr;
			
		}
		
		function SelectSell() {
			
			$where = "  ";
			
			if(intval($this->regionSell) > 0) {
				$where .= " and Sell.CityID=".intval($this->regionSell)." ";
			}

			if(intval($this->subRegionSell) > 0) {
				$where .= " and Sell.subCityID=".intval($this->subRegionSell)." ";
			}

			if(intval($this->categorySell) > 0) {
				$where .= " and Sell.CategoryID=".intval($this->categorySell)." ";
			}
			
			if(intval($this->subCategorySell) > 0) {
				$where .= " and Sell.SubCategoryID=".intval($this->subCategorySell)." ";
			}
			
			$where .= " limit 100";
			
			$sql = "select  geocode.id, 
							geocode.latitude,
							geocode.longitude,
							geocode.accuracy,
							geocode.status,
							geocode.zoom,
							SubCategory.googleicon,
							Sell.NameBizID as 'sellName',
							Sell.ID as 'sellId',
							Sell.CostID,
							Sell.cCostID,
							Sell.PhoneID,
							Sell.FML,
							Sell.EmailID,
							Sell.FaxID, 
							Sell.SiteID,
							Sell.ShortDetailsID
								from geocode, 
								     Sell,
								     SubCategory 
						       where 1 and Sell.SubCategoryID=SubCategory.ID and geocode.status=".G_GEO_SUCCESS." and  geocode.serviceId=Sell.ID and Sell.StatusID in (1,2) ".$where.";";

			
			$result = $this->db->Select($sql);

			$arr = array();
			while ($row = mysql_fetch_object($result)) {
				$obj = new stdclass();
				$obj->id = $row->id;
				$obj->latitude = $row->latitude;
				$obj->longitude = $row->longitude;
				$obj->accuracy = $row->accuracy;
				$obj->status = $row->status;
				$obj->zoom = $row->zoom;
				$obj->googleicon = $row->googleicon;
				$obj->sellName =  str_replace("\r\n", " ", $row->sellName);
				$obj->sellId = $row->sellId;
				$obj->PhoneID = $row->PhoneID;
				$obj->FML = $row->FML;
				$obj->EmailID = $row->EmailID;
				$obj->FaxID = $row->FaxID;
				$obj->SiteID = str_replace("\r\n", " ", $row->SiteID);
				$obj->cost = trim(strrev(chunk_split (strrev($row->CostID), 3,' '))); 
				$obj->ccost = $row->cCostID;
				$obj->ShortDetailsID = $row->ShortDetailsID;
				$arr[] = $obj;
			}
			return $arr;
			
		}
		
		function AutoInitRegion() {
			$sql = "select region.ID, region.title  from Sell, region where Sell.StatusID in (1,2) and region.ID=Sell.CityID and (region.statusGeoCode is null or region.statusGeoCode!=".G_GEO_SUCCESS.") group by Sell.CityID;";
			$result = $this->db->Select($sql);
			$arrRegion = array();
			while ($row = mysql_fetch_object($result)) {
				$arrRegion[$row->ID] = convert_cyr_string($row->title,"koi8-r", "windows-1251");
			}
			
			
			while (list($kReg, $vReg) = each($arrRegion)) {
				$this->status = 0;
				$this->address = $vReg;
				$this->CallGoogleService();
				if($this->status == G_GEO_SUCCESS) {
					$sqlReg = "update region set latitude=".$this->latitude.", longitude=".$this->longitude.", statusGeoCode=".G_GEO_SUCCESS." where region.ID=".$kReg.";";
					$result = $this->db->Sql($sqlReg);
					echo $sqlReg."<hr>";
				}
				
			}
			
		}

		function AutoInitRegionAll() {
			$sql = "select region.ID, region.title from region where 1 and region.statusGeoCode is null;";
			$result = $this->db->Select($sql);
			$arrRegion = array();
			while ($row = mysql_fetch_object($result)) {
				$arrRegion[$row->ID] = convert_cyr_string($row->title,"koi8-r", "windows-1251");
			}
			
			
			while (list($kReg, $vReg) = each($arrRegion)) {
				$this->status = 0;
				$this->address = $vReg;
				$this->CallGoogleService();
				if($this->status == G_GEO_SUCCESS) {
					$sqlReg = "update region set latitude=".$this->latitude.", longitude=".$this->longitude.", statusGeoCode=".G_GEO_SUCCESS." where region.ID=".$kReg.";";
					$result = $this->db->Sql($sqlReg);
					echo $sqlReg."<hr>";
				}
				
			}
			
		}
		
		
		
	}
?>