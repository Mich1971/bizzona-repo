<?php
	include_once("class.db.inc");  
    
	class city_article {
		var $db = "";
		var $Id = "";
		var $title = "";
		var $shortdescription  = "";
		var $link = "";
		var $cityId = 0;

		function city_article() {
			global $DATABASE;
			$this->InitDB();
		}

		function InitDB() {
			global $DATABASE;
			$this->db = new DB($DATABASE["HOSTNAME"], $DATABASE["DATABASENAME"], $DATABASE["DATABASEUSER"], $DATABASE["DATABASEPASSWORD"]);
		}

		function Select() {
			$sql = "select * from city_article where 1 and cityId=".$this->cityId." order by Id desc; ";
			$result = $this->db->Select($sql);
			$array = Array();
			while ($row = mysql_fetch_object($result)) {
				$obj = new stdclass();
				$obj->Id = $row->Id;
				$obj->title = $row->title;
				$obj->shortdescription = $row->shortdescription;
				$obj->link = $row->link;
				$array[] = $obj;
			}
			return $array;
		}
     
		
		function Close() {
			$this->db->Close();
		}
		
	}
?>
