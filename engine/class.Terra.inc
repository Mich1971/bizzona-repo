<?php
	include_once("class.db.inc");  

	class Terra {

		public $db = "";
		public $sql = "";
		public $Id = 0;
		public $TerraCategoryId = 0;
		public $ShortDescription = "";
		public $Description = "";
		public $cost = 0;
		public $cCostId = 0;
		public $photo_1 = "";
		public $smallphoto_1 = "";
		public $statusId = 0;
		public $fio = "";
		public $phone = "";
		public $email = "";
		public $youtubeURL = "";
		public $youtubeShortURL = "";
		public $fax = "";
		public $pay_status = 0;
		public $pay_summ = 0;
		public $pay_datetime = 0;
		public $pay_smsnumber = 0;
		public $ip = "";
		public $datecreate = 0;
		public $countView = 0;
		public $area_text = "";
		public $area = 0;
		public $cost_text = "";
		public $districtId = 0;
		public $Name = "";
		public $quicksearchurl = "";
		public $offset = 0;
		public $rowCount = 0;
		public $show_area = "";
		public $Address = "";
		public $cityId = "";

		public $TerraCategoryName = "";
		
		function __construct() {
			global $DATABASE;
			$this->InitDB();
		}

		function InitDB() {
			global $DATABASE;
			$this->db = new DB($DATABASE["HOSTNAME"], $DATABASE["DATABASENAME"], $DATABASE["DATABASEUSER"], $DATABASE["DATABASEPASSWORD"]);
		}
		
		function Insert() {

			$this->area = 0;
			$this->cost = intval($this->cost);
			$this->cCostId = intval($this->cCostId);
			$this->cityId = 0;
			
			$this->sql = "insert into Terra (TerraCategoryId, ShortDescription, Description, area, area_text, cityId, cost, cCostId, photo_1, fio, phone, email, youtubeURL, fax, ip, datecreate, cost_text, Address) values (".$this->TerraCategoryId.", '".$this->ShortDescription."', '".$this->Description."', ".$this->area.", '".$this->area_text."', ".$this->cityId.", ".$this->cost.", ".$this->cCostId.", '".$this->photo_1."', '".$this->fio."', '".$this->phone."', '".$this->email."', '".$this->youtubeURL."', '".$this->fax."', '".$this->ip."', NOW(),  '".$this->cost_text."', '".$this->Address."');"; 
			//echo $this->sql;
			$result = $this->db->Insert($this->sql);
			$this->Id = intval($result);
			return $result;
						
		}
		
		function GetItem() {
			
			$_Terra = new Terra();
			$this->sql = "select Id, Name, TerraCategoryId, ShortDescription, Description, area, area_text, cityId, cost, cCostId, photo_1, smallphoto_1, statusId, fio, phone, email from Terra where 1 and Id=".intval($this->Id).";";
			
			$result = $this->db->Select($this->sql);
			while ($row = mysql_fetch_object($result)) {
				$_Terra->Id = intval($row->Id);
				$_Terra->TerraCategoryId = $row->TerraCategoryId;
				$_Terra->ShortDescription = $row->ShortDescription;
				$_Terra->Description = $row->Description;
				$_Terra->area = $row->area;
				$_Terra->area_text = $row->area_text;
				$_Terra->cityId = $row->cityId;
				$_Terra->cost = $row->cost;
				$_Terra->cCostId = $row->cCostId;
				$_Terra->photo_1 = $row->photo_1;
				$_Terra->smallphoto_1 = $row->smallphoto_1;
				$_Terra->statusId = $row->statusId;
				$_Terra->fio = $row->fio;
				$_Terra->phone = $row->phone;
				$_Terra->email = $row->email;
				$_Terra->Name = $row->Name;
			}
			return $_Terra;			
			
		}		

		function CountSelect() {

			$where = " where 1 ";

			if(intval($this->TerraCategoryId) > 0) {
				$where .= " and Terra.TerraCategoryId = ".intval($this->TerraCategoryId)." ";
			}

			if(intval($this->cityId) > 0) {
				$where .= " and Terra.cityId = ".intval($this->cityId)." ";
			}	
			
			if(intval($this->districtId) > 0) {
				$where .= " and Terra.districtId = ".intval($this->districtId)." ";
			}
			
			if(intval($this->statusId) > 0) {
				$where .= " and Terra.statusId = ".intval($this->statusId)." ";
			}			
			
			$this->sql = "select Count(Id) as 'Count' from Terra ".$where.";";
			 
			$result = $this->db->Select($this->sql);
			while ($row = mysql_fetch_object($result)) {
				
				return intval($row->Count);
			}
			
			return 0;
		}
		
		function Select() {
			
			$array = Array();
			
			$where = " where 1 ";
			$limit = " ";
			$order = " order by Terra.pay_datetime desc";
			
			if(intval($this->TerraCategoryId) > 0) {
				$where .= " and Terra.TerraCategoryId = ".intval($this->TerraCategoryId)." ";
			}
			
			if(intval($this->cityId) > 0) {
				$where .= " and Terra.cityId = ".intval($this->cityId)." ";
			}			
			
			if(intval($this->districtId) > 0) {
				$where .= " and Terra.districtId = ".intval($this->districtId)." ";
			}			
			
			if(intval($this->statusId) > 0) {
				$where .= " and Terra.statusId = ".intval($this->statusId)." ";
			}					
			
			if ($this->offset === 0 && $this->rowCount === 0) {
			} else {
				$limit = " limit ".intval($this->offset).",".intval($this->rowCount)."";
			}			
			
			
			$this->sql = "select Terra.Id, Terra.Name, TerraCategoryId, TerraCategory.Name as 'TerraCategoryName' , Terra.ShortDescription, Terra.Description, Terra.area_text, Terra.area, Terra.cityId, Terra.cost, Terra.cCostId, Terra.photo_1, Terra.smallphoto_1, Terra.statusId, Terra.fio, Terra.phone, Terra.email, Terra.datecreate, Terra.fax, Terra.districtId from Terra left join TerraCategory on (TerraCategory.Id = Terra.TerraCategoryId) ".$where." ".$order." ".$limit.";";
			
			$result = $this->db->Select($this->sql);
			
			while ($row = mysql_fetch_object($result)) {
				
				$_Terra = new Terra();
				$_Terra->Id = intval($row->Id);
				$_Terra->TerraCategoryId = intval($row->TerraCategoryId);
				$_Terra->ShortDescription = $row->ShortDescription;
				$_Terra->Description = $row->Description;
				$_Terra->area = $row->area;
				$_Terra->area_text = $row->area_text;
				$_Terra->cityId = $row->cityId;
				$_Terra->cost = trim(strrev(chunk_split (strrev($row->cost), 3,' ')));  //$row->cost;
				$_Terra->cCostId = $row->cCostId;
				$_Terra->photo_1 = $row->photo_1;
				$_Terra->smallphoto_1 = $row->smallphoto_1;
				$_Terra->statusId = $row->statusId;
				$_Terra->fio = $row->fio;
				$_Terra->phone = $row->phone;
				$_Terra->email = $row->email;
				$_Terra->Name = $row->Name;
				$_Terra->datecreate =  strftime("%d %B %Y",  strtotime($row->datecreate));				
				$_Terra->fax = $row->fax;
				$_Terra->districtId = $row->districtId;
				$_Terra->show_area = $_Terra->GetArea(true);
				$_Terra->TerraCategoryName = $row->TerraCategoryName;
 				 		
				$_Terra->quicksearchurl = "http://www.bizzona.ru/detailssearchTerra.php?cityId=".$_Terra->cityId."&TerraCategoryId=".$_Terra->TerraCategoryId."&districtId=".$_Terra->districtId."";
				$array[] = $_Terra;
			}
			
			return $array;

		}
		
		function SetPayment($CodeID, $PayStatus, $PaySumm, $PayDateTime, $PayNumber)
		{
			$sql = "update Terra set 
                               `pay_status`=".$PayStatus.", 
                               `pay_summ`=`pay_summ`+ ".$PaySumm.", 
                               `pay_datetime`=NOW(),
                               `pay_smsnumber`=".$PayNumber.",
                               `datecreate`=NOW(),
                               `countView`=0 
                       where Id=".$CodeID." ";
			
			//echo $sql;
			
			$result_1 = $this->db->Update($sql);

			$sql = "insert into payments(summ,datepayment) values ('".$PaySumm."',NOW());";
			$result_2 = $this->db->Update($sql);
			
			return $result_1;
		}		
		
		function GetArea($show_on_site = false) {
		
			/*	
			if(intval($this->area_from) > 0 && intval($this->area_to) > 0) {
				
				if(intval($this->area_from) == intval($this->area_to)) {
					return ($show_on_site ?  "".$this->area_from." �&sup2; " : "".$this->area_from."");
				}
				
				if(intval($this->area_to) > intval($this->area_from)) {
					return ($show_on_site ? "".$this->area_from."  �&sup2;  - ".$this->area_to." �&sup2; " : "".$this->area_from." - ".$this->area_to."");
				}
				
				if(intval($this->area_to) < intval($this->area_from)) {
					return ($show_on_site ? "".$this->area_to." �&sup2; - ".$this->area_from." �&sup2;" : "".$this->area_to." - ".$this->area_from."");
				}
				
			}
			
			if(intval($this->area_from) <= 0 && intval($this->area_to) > 0) {
				return ($show_on_site ? "".$this->area_to." �&sup2;" : "".$this->area_to."");
			}
			
			if(intval($this->area_from) <= 0 && intval($this->area_to) <= 0) {
				return ($show_on_site ? "�� �������": "");
			}			
			
			if(intval($this->area_from) > 0 && intval($this->area_to) <= 0) {
				return ($show_on_site ? "".$this->area_from." �&sup2;" : "".$this->area_from."");
			}			
			
			return "";
			*/
			return "";
		}
		
		function Close() {
			$this->db->Close();
		}		
		
	}

?>