<?php
	include_once("class.db.inc");

	class Operator {
		private  $db = "";
		public $typeQueryId = "";
		public $query = "";
		public $answer = "";
		public $datetime = "";
		public $sellId = "";
		public $sendSql = "";
		public $id = "";

		function __construct() {
			global $DATABASE;
			$this->InitDB();
		}

		private function InitDB() {
			global $DATABASE;
			$this->db = new DB($DATABASE["HOSTNAME"], $DATABASE["DATABASENAME"], $DATABASE["DATABASEUSER"], $DATABASE["DATABASEPASSWORD"]);
		}

		function __destruct() {
			$this->db->Close();
		}
		
		public function Insert() {
			
			$this->CheckAttr();
			$sql = " insert into operator 	(
												typeQueryId,
												query,
												answer,
												datetime,
												sellId																																													
											) values (
												".$this->typeQueryId.",
												'".$this->query."',
												'".$this->answer."',
												NOW(),
												".$this->sellId."
											) ";
			
			$this->sendSql = $sql;
			$result = $this->db->Sql($this->sendSql);
		}
		
		public function GetItem() {

			$this->CheckAttr();
			$sql = "select * from operator where operator.id=".$this->id.";";
			$this->sendSql = $sql;

			$result = $this->db->Select($this->sendSql);

			$obj = new stdclass();

			while ($row = mysql_fetch_object($result)) {

				$obj->id 			= $row->id;
				$obj->typeQueryId 	= $row->typeQueryId;
				$obj->query 		= $row->query;
				$obj->answer 		= $row->answer;
				$obj->datetime 		= $row->datetime;
				$obj->sellId 		= $row->sellId;
				
			} 
			return $obj;
			
		}
		
		public function Select() {

			$this->CheckAttr();
			
			$where = "";
			if(intval($this->id) > 0) {
				$where .= " and operator.id=".$this->id."";
			}
			if(intval($this->sellId) > 0) {
				$where .= " and operator.sellId=".$this->sellId."";
			}
			
			$sql = "select * from operator where 1=1 ".$where." order by datetime desc;";
			$this->sendSql = $sql;
			
			$result = $this->db->Select($this->sendSql);

			$arr = array();				

			while ($row = mysql_fetch_object($result)) {

				$obj = new stdclass();

				$obj->id 			= $row->id;
				$obj->typeQueryId 	= $row->typeQueryId;
				$obj->query 		= $row->query;
				$obj->answer 		= $row->answer;
				$obj->datetime 		= $row->datetime;
				$obj->sellId 		= $row->sellId;
				$obj->datetime 		= strftime("%d %B %Y",  strtotime($row->datetime));
				
				array_push($arr, $obj);		
			} 
			return $arr;
			
		}
		
		private function CheckAttr() {
			$this->sellId = intval($this->sellId);
			$this->typeQueryId = intval($this->typeQueryId);
			$this->id = intval($this->id);
			$this->query = trim($this->query);
			$this->answer = trim($this->answer);
		}
		
		
	}

?>