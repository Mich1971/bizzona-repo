<?php
	include_once("class.db.inc");
	require_once("Cache/Lite.php");

	class Metro_Lite {
		var $db = "";
		var $regionID = "";
		var $name = "";
		var $seobuy = "";
		var $seosell = "";
		var $sendSql = "";
		var $ID = "";
		var $aGetItem = array();
		var $aGetName = array();
		var $aSeoSell = array(); 

		function Metro_Lite() {
			global $DATABASE;
			$this->InitDB();
		}

		function InitDB() {
			global $DATABASE;
			$this->db = new DB($DATABASE["HOSTNAME"], $DATABASE["DATABASENAME"], $DATABASE["DATABASEUSER"], $DATABASE["DATABASEPASSWORD"]);
		}

		function Close() {
			$this->db->Close();
		}
		
		function GetSeoSell($id) {

			if($this->aSeoSell[$id]) {
				return $this->aSeoSell[$id];
			} else {
			
				if(intval($id) <= 0) {
					return "";
				}
				
				$result = $this->GetSeoSellList();
				$this->aSeoSell[$id] = $result[$id];
				return $result[$id];
			}			
			
		}
		
		function GetSeoSellList() {
			
			$aSeoSell = array();
		
			$options = array(
    			'cacheDir' => PERSISTENTCACHE."/",
    			'lifeTime' => 2592000,
    			'pearErrorMode' => CACHE_LITE_ERROR_DIE
			);
		
			$cache = new Cache_Lite($options);
		
			if ($data = $cache->get('metroselselllist')) {
				$aSeoSell = unserialize($data);

			} else { 
		
				$sql = "Select ID, seosell from Metro;";
				$result = $this->db->Select($sql);
				while ($row = mysql_fetch_object($result)) {
					$aSeoSell[$row->ID] = $row->seosell;
				}
				$cache->save(serialize($aSeoSell));
			}
		
			return 	$aSeoSell;			
			
		}
		
		function GetNameList() {
			
			$aName = array();
		
			$options = array(
    			'cacheDir' => PERSISTENTCACHE."/",
    			'lifeTime' => 2592000,
    			'pearErrorMode' => CACHE_LITE_ERROR_DIE
			);
		
			$cache = new Cache_Lite($options);
		
			if ($data = $cache->get('metronames')) {
				$aName = unserialize($data);

			} else { 
		
				$sql = "Select ID, name from Metro;";
				$result = $this->db->Select($sql);
				while ($row = mysql_fetch_object($result)) {
					$aName[$row->ID] = $row->name;
				}
				$cache->save(serialize($aName));
			}
		
			return 	$aName;			
			
		}
		
		function GetName($id) {
			if($this->aGetName[$id]) {
				return $this->aGetName[$id];
			} else {
			
				if(intval($id) <= 0) {
					return "";
				}
				
				$result = $this->GetNameList();
				$this->aGetName[$id] = $result[$id];
				return $result[$id];
			}			
		}
		
		function GetItem() {
			$obj = new stdclass();
			if(isset($this->aGetItem[$this->ID])) {
				return $this->aGetItem[$this->ID];
			} else {
				$sql  = "select ID, regionID, name, seobuy, seosell  from Metro where ID=".intval($this->ID)."";
				$result = $this->db->Select($sql);

				while ($row = mysql_fetch_object($result)) {
					$obj->ID = $row->ID;
					$obj->regionID = $row->regionID;
					$obj->name = $row->name;
					$obj->seobuy = $row->seobuy;
					$obj->seosell = $row->seosell;
				}
				$this->aGetItem[$this->ID] = $obj;
			}
			return $obj;
			
		}
		
		
		function SelectByRegion() {
			$sql = "select ID, name  from Metro where `regionID`='".intval($this->regionID)."' order by Name";

			$result = $this->db->Select($sql);

			$array = Array();
			while ($row = mysql_fetch_object($result)) {
				$obj = new stdclass();
				$obj->ID = $row->ID;
				$obj->name = $row->name;
				$array[] = $obj;
			}
			return $array;
		}

		function SelectMetroSell() {
			
			$sql = "set @regionID = null;";
			$this->db->Select($sql);
			
			$sql = "select @regionID:=regionID from Metro where ID=".intval($this->ID).";";
			$this->db->Select($sql);
			
			$sql = "select `Metro`.*, 
							COUNT(`Sell`.`metroID`) as 'Count'
												from 
													`Metro` 
														LEFT JOIN `Sell` on (`Sell`.`metroID` = `Metro`.`ID` and `Sell`.`StatusID` in (1,2) ) 
																where `Metro`.`regionID` = @regionID and ( `Sell`.`CityID`=@regionID or  `Sell`.`SubCityID`=@regionID)  GROUP BY `Sell`.`metroID` ORDER BY `Metro`.`name`;";
				
			$result = $this->db->Select($sql);

			$array = Array();
			while ($row = mysql_fetch_object($result)) {
				if($row->Count > 0) {
					$obj = new stdclass();
					$obj->ID = $row->ID;
					$obj->name = $row->name;
					$obj->Count = $row->Count;
					$obj->seobuy = $row->seobuy;
					$obj->seosell = $row->seosell;
					$array[] = $obj;
				}
			}
			return $array;
		}
		
		function SelectMetroBuyer() {
			$sql = "set @regionID = null;";
			$this->db->Select($sql);
			
			$sql = "select @regionID:=regionID from Metro where ID=".intval($this->ID).";";
			$this->db->Select($sql);
			
			$sql = "select 	Metro.*, 
							b.Count from Metro,
							(	select buyermetro.metroID, count(buyermetro.metroID) as 'Count' from buyermetro, buyer where buyermetro.buyerID = buyer.id and buyer.statusID in (1) group by buyermetro.metroID) b
							where Metro.regionID=@regionID and b.metroID = Metro.ID  ORDER BY `Metro`.`name`;
					";
			
			$result = $this->db->Select($sql);

			$array = Array();
			while ($row = mysql_fetch_object($result)) {
				if($row->Count > 0) {
					$obj = new stdclass();
					$obj->ID = $row->ID;
					$obj->name = $row->name;
					$obj->Count = $row->Count;
					$obj->seobuy = $row->seobuy;
					$obj->seosell = $row->seosell;
					$array[] = $obj;
				}
			}
			return $array;			
		}
	}
?>