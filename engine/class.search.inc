<?php
	include_once("class.db.inc");  
	
	class search 
	{
		var $db = "";
		var $word = "";
		var $sound = "";
		var $wordId = "";
		var $sellId = "";
		var $count = "";
		var $awords = array();
		var $sql = "";
		
		function search() {
			global $DATABASE;
			$this->InitDB();
		}

		function InitDB() {
			global $DATABASE;
			$this->db = new DB($DATABASE["HOSTNAME"], $DATABASE["DATABASENAME"], $DATABASE["DATABASEUSER"], $DATABASE["DATABASEPASSWORD"]);
		}
		
		function GetDateText($date) {
			
			$b = explode("-",date("Y-m-d"));
      		$yesterday = date("Ymd",mktime(0,0,0,$b[1],$b[2]-1,$b[0]));
			
			if(date("Ymd") == date("Ymd", strtotime($date))) {
				return "������� ".strftime("%H:%M",  strtotime($date));
			} else if ($yesterday == date("Ymd", strtotime($date))) { 
				return "����� ".strftime("%H:%M",  strtotime($date));
			} else {
				return strftime("%d %B %Y",  strtotime($date));
			}
			
			return strftime("%d %B %Y",  strtotime($date));
		}		
		
		
		function InsertIndexingWordSell()
		{
			$this->sql = "INSERT IGNORE INTO indexing_word_sell (`word`,`sound`) VALUES ('".$this->word."','".$this->sound."')";

			$result = $this->db->Insert($this->sql);
			if($result <= 0) {
				$this->sql = "SELECT id FROM indexing_word_sell WHERE word='".$this->word."'";
				$res_select = $this->db->Select($this->sql);
				while ($row = mysql_fetch_object($res_select)) {
					$result = $row->id;
				}
			}
			return $result;
		}
	
		function InsertIndexingIndexSell()
		{
			$this->sql = "INSERT IGNORE INTO indexing_index_sell (`sellId`,`word`,`count`) VALUES ('".$this->sellId."','".$this->wordId."','".$this->count."');";
			$result = $this->db->Insert($this->sql);
			return $result;
		}
		
		function Close() {
			$this->db->Close($sql);
		}
		
		function SelectSell($data) {
			
			$limit = "";			
			if ($data["offset"] === 0 && $data["rowCount"] === 0) {
			} else {
				
				if(intval($data["rowCount"]) > 20) { $data["rowCount"] = 5; }
				if(intval($data["offset"]) > 50000) { $data["offset"] = 0; }				
				
				$limit = " limit ".intval($data["offset"]).",".$data["rowCount"]."";
			}
			
			
			$if_clause = "";
			$if_clause_sound = "";
			
			for ($i=0;$i<sizeof($this->awords);$i++)
			{
				$if_clause .= "iw.word='".$this->awords[$i]."' ";
				$if_clause_sound .= "iw.sound='".soundex(ruslat($this->awords[$i]))."' ";
				if($i!=sizeof($this->awords)-1) 
				{
					$if_clause .= " or "; 
					$if_clause_sound .= " or "; 
				}
			}
			$this->sql = "select 	ii.sellId, 
									count(distinct iw.id)*1000 + sum(ii.count) + IF(DATEDIFF(NOW(), DataCreate) <= 14,10,0 ) as rel,
									Sell.ID,
									Sell.FML,
									Sell.EmailID,
									Sell.Email2ID,
									Sell.PhoneID,
									Sell.NameBizID,
									Sell.BizFormID,
									Sell.CityID,
									Sell.subCityID,
									Sell.SiteID,
									Sell.CostID,
									Sell.cCostID,
									Sell.AgreementCostID,
									Sell.ProfitPerMonthID,
									Sell.cProfitPerMonthID,
									Sell.MaxProfitPerMonthID,
									Sell.cMaxProfitPerMonthID,
									Sell.TimeInvestID,
									Sell.TermBizID,
									Sell.ShortDetailsID,
									Sell.ImgID,
									Sell.FaxID,
									Sell.ShareSaleID,
									Sell.DataCreate,
									Sell.DateStart,
									Sell.TimeActive,
									Sell.CategoryID,
									Sell.QView,
									Sell.Icon,
									Sell.getseo,
									Sell.metroID, 									
									Sell.defaultUserIcon, 
									Sell.districtID, 
									Sell.brokerId, 
									Sell.typeSellID, 
									Sell.subtitle, 
									Sell.arendaStatusID, 
									IF(DATEDIFF(NOW(), DataCreate) <= 14,1,0 ) as 'IsNewSell', 
									SubCategory.levelsearch, 
									SubCategory.ID AS 'SubCategoryID',
									SubCategory.icon AS 'SubCategoryIcon',
									Category.icon AS 'CategoryIcon'
										from 
											Sell LEFT JOIN SubCategory ON ( SubCategory.ID = Sell.SubCategoryID ) 
												 LEFT JOIN Category ON ( Category.ID = Sell.CategoryID ),
										    indexing_index_sell ii,
										    indexing_word_sell iw
										  		where (".$if_clause.") 
										  					and
										  				Sell.ID = ii.sellId 
										  					and 
										  				Sell.StatusID in (1,2) 
										  					and 
										  				ii.word = iw.id group by ii.sellId  order by rel desc ".$limit;

			$result = $this->db->Select($this->sql);
			$array = Array();
			
			while ($row = mysql_fetch_object($result)) {
				$obj = new stdclass();
				$obj->ID = $row->ID;
				$obj->rel = $row->rel;
				$obj->FML = $row->FML;
				$obj->EmailID = $row->EmailID;
				$obj->Email2ID = $row->Email2ID;
				$obj->PhoneID = $row->PhoneID;
				$obj->NameBizID = $row->NameBizID;
				$obj->NameBizLatID = ruslat_for_name_biz($row->NameBizID);
				$obj->CityID = $row->CityID;
				$obj->subCityID = $row->subCityID;
				$obj->SiteID = $row->SiteID;
				$obj->CostID = trim(strrev(chunk_split (strrev($row->CostID), 3,' '))); 
				$obj->cCostID =$row->cCostID; 
				$obj->AgreementCostID = $row->AgreementCostID;
				$obj->ProfitPerMonthID = trim(strrev(chunk_split (strrev($row->ProfitPerMonthID), 3,' ')));
				$obj->cProfitPerMonthID = $row->cProfitPerMonthID;
				$obj->MaxProfitPerMonthID = trim(strrev(chunk_split (strrev($row->MaxProfitPerMonthID), 3,' ')));
				$obj->cMaxProfitPerMonthID = $row->cMaxProfitPerMonthID;
				$obj->TimeInvestID = $row->TimeInvestID;
				$obj->MatPropertyID = $row->MatPropertyID;
				$obj->MonthAveTurnID = trim(strrev(chunk_split (strrev($row->MonthAveTurnID), 3,' '))); 
				$obj->cMonthAveTurnID =  $row->cMonthAveTurnID;
				$obj->MaxMonthAveTurnID = trim(strrev(chunk_split (strrev($row->MaxMonthAveTurnID), 3,' '))); 
				$obj->cMaxMonthAveTurnID = $row->cMaxMonthAveTurnID;
				$obj->MonthExpemseID = trim(strrev(chunk_split (strrev($row->MonthExpemseID), 3,' '))); 
				$obj->cMonthExpemseID = $row->cMonthExpemseID;				
				$obj->SumDebtsID = $row->SumDebtsID;
				$obj->WageFundID = $row->WageFundID;
				$obj->ObligationID = $row->ObligationID;
				$obj->ShortDetailsID = $row->ShortDetailsID;
				$obj->ImgID = $row->ImgID;
				$obj->FaxID = $row->FaxID;
				$obj->ShareSaleID = $row->ShareSaleID;
				$obj->DataCreate = $row->DataCreate;
				$obj->DateStart = $row->DateStart;
				$obj->TimeActive = $row->TimeActive;
				$obj->CategoryID = $row->CategoryID;
				$obj->QView = $row->QView;
				$obj->defaultUserIcon = $row->defaultUserIcon;
				//$obj->DataCreate  = date("d F Y", strtotime($row->DataCreate));
				$obj->DataCreateTxt = $this->GetDateText($row->DataCreate);
				$obj->DataCreate = strftime("%d %B %Y",  strtotime($row->DataCreate));
				$obj->DataClose   = date("d F Y", strtotime($row->DateStart) + 3600*24*30*$row->TarifID);
				$obj->DateBySecondCreate  = strtotime($row->DataCreate);
				$obj->DateBySecondClose   = strtotime($row->DateStart) + 3600*24*30*$row->TarifID;
				$obj->Icon = $row->Icon;
				$obj->SubCategoryID = $row->SubCategoryID;
				$obj->getseo = $row->getseo;
				$obj->metroID = $row->metroID;
				$obj->districtID = $row->districtID;
				$obj->brokerId = $row->brokerId;
				$obj->typeSellID = $row->typeSellID;
				$obj->subtitle = $row->subtitle;
				$obj->arendaStatusID = $row->arendaStatusID;
				$obj->torgStatusID = $row->torgStatusID;
				$obj->IsNewSell = $row->IsNewSell;
         
				if(strlen($row->Icon) > 3) {
					$obj->CategoryIcon = $row->Icon;
				} else if (strlen($row->SubCategoryIcon)  > 0) {
					$obj->CategoryIcon = $row->SubCategoryIcon;
				} else {
					$obj->CategoryIcon = $row->CategoryIcon;
				} 
          
				if($row->levelsearch == 2) {
					$obj->quicksearchurl = "http://www.bizzona.ru/detailssearch.php?cityId=".$row->CityID."&subCityId=".$row->subCityID."&catId=".$row->CategoryID."&subCatId=".$row->SubCategoryID."";					
				} else {
					$obj->quicksearchurl = "http://www.bizzona.ru/detailssearch.php?cityId=".$row->CityID."&subCityId=".$row->subCityID."&catId=".$row->CategoryID."&subCatId=".$row->SubCategoryID."&metroId=".$row->metroID."&districtId=".$row->districtID."";
				}

				
				$array[] = $obj;
			}
			
			if(sizeof($array) <= 0) 
			{
				$this->sql = "select 
									ii.sellId, 
									count(distinct iw.id)*1000 + sum(ii.count) + IF(DATEDIFF(NOW(), DataCreate) <= 14,10,0 ) as rel, 
									Sell.ID,
									Sell.FML,
									Sell.EmailID,
									Sell.Email2ID,
									Sell.PhoneID,
									Sell.NameBizID,
									Sell.BizFormID,
									Sell.CityID,
									Sell.subCityID,
									Sell.SiteID,
									Sell.CostID,
									Sell.cCostID,
									Sell.AgreementCostID,
									Sell.ProfitPerMonthID,
									Sell.cProfitPerMonthID,
									Sell.MaxProfitPerMonthID,
									Sell.cMaxProfitPerMonthID,
									Sell.TimeInvestID,
									Sell.TermBizID,
									Sell.ShortDetailsID,
									Sell.ImgID,
									Sell.FaxID,
									Sell.ShareSaleID,
									Sell.DataCreate,
									Sell.DateStart,
									Sell.TimeActive,
									Sell.CategoryID,
									Sell.QView,
									Sell.Icon,
									Sell.getseo,
									Sell.metroID,
									Sell.defaultUserIcon, 
									Sell.districtID, 
									Sell.brokerId, 
									Sell.typeSellID, 
									Sell.subtitle, 
									Sell.arendaStatusID, 									
									SubCategory.levelsearch, 
									SubCategory.ID AS 'SubCategoryID',
									SubCategory.icon AS 'SubCategoryIcon',
									Category.icon AS 'CategoryIcon'				
											from 
												Sell LEFT JOIN SubCategory ON ( SubCategory.ID = Sell.SubCategoryID ) 
													 LEFT JOIN Category ON ( Category.ID = Sell.CategoryID ),											
												indexing_index_sell ii,
												indexing_word_sell iw 
											where (".$if_clause_sound.") 
													and 
												Sell.ID = ii.sellId 
													and 
												Sell.StatusID in (1,2) 
													and	
												ii.word = iw.id group by ii.sellId  order by rel desc ".$limit;
				$result = $this->db->Select($this->sql);
			
				while ($row = mysql_fetch_object($result)) {
					$obj = new stdclass();
					$obj->ID = $row->ID;
					$obj->rel = $row->rel;
					$obj->FML = $row->FML;
					$obj->EmailID = $row->EmailID;
					$obj->Email2ID = $row->Email2ID;
					$obj->PhoneID = $row->PhoneID;
					$obj->NameBizID = $row->NameBizID;
					$obj->NameBizLatID = ruslat_for_name_biz($row->NameBizID);
					$obj->BizFormID = $row->BizFormID;
					$obj->CityID = $row->CityID;
					$obj->subCityID = $row->subCityID;
					$obj->SiteID = $row->SiteID;
					$obj->CostID = trim(strrev(chunk_split (strrev($row->CostID), 3,' '))); 
					$obj->cCostID =$row->cCostID; 
					$obj->AgreementCostID = $row->AgreementCostID;
					$obj->ProfitPerMonthID = trim(strrev(chunk_split (strrev($row->ProfitPerMonthID), 3,' ')));
					$obj->cProfitPerMonthID = $row->cProfitPerMonthID;
					$obj->MaxProfitPerMonthID = trim(strrev(chunk_split (strrev($row->MaxProfitPerMonthID), 3,' ')));
					$obj->cMaxProfitPerMonthID = $row->cMaxProfitPerMonthID;
					$obj->TimeInvestID = $row->TimeInvestID;
					$obj->MatPropertyID = $row->MatPropertyID;
					$obj->MonthAveTurnID = trim(strrev(chunk_split (strrev($row->MonthAveTurnID), 3,' '))); 
					$obj->cMonthAveTurnID =  $row->cMonthAveTurnID;
					$obj->MaxMonthAveTurnID = trim(strrev(chunk_split (strrev($row->MaxMonthAveTurnID), 3,' '))); 
					$obj->cMaxMonthAveTurnID = $row->cMaxMonthAveTurnID;
					$obj->MonthExpemseID = trim(strrev(chunk_split (strrev($row->MonthExpemseID), 3,' '))); 
					$obj->TermBizID = $row->TermBizID;
					$obj->ShortDetailsID = $row->ShortDetailsID;
					$obj->ImgID = $row->ImgID;
					$obj->FaxID = $row->FaxID;
					$obj->ShareSaleID = $row->ShareSaleID;
					$obj->DataCreate = $row->DataCreate;
					$obj->DateStart = $row->DateStart;
					$obj->TimeActive = $row->TimeActive;
					$obj->CategoryID = $row->CategoryID;
					$obj->QView = $row->QView;
					//$obj->DataCreate  = date("d F Y", strtotime($row->DataCreate));
					$obj->DataCreateTxt = $this->GetDateText($row->DataCreate);
					$obj->DataCreate = strftime("%d %B %Y",  strtotime($row->DataCreate));
					$obj->DataClose   = date("d F Y", strtotime($row->DateStart) + 3600*24*30*$row->TarifID);
					$obj->DateBySecondCreate  = strtotime($row->DataCreate);
					$obj->DateBySecondClose   = strtotime($row->DateStart) + 3600*24*30*$row->TarifID;
					$obj->Icon = $row->Icon;
					$obj->SubCategoryID = $row->SubCategoryID;
					$obj->getseo = $row->getseo;
					$obj->metroID = $row->metroID;
         
					$obj->defaultUserIcon = $row->defaultUserIcon;
					$obj->districtID = $row->districtID;
					$obj->brokerId = $row->brokerId;
					$obj->typeSellID = $row->typeSellID;
					$obj->subtitle = $row->subtitle;
					$obj->arendaStatusID = $row->arendaStatusID;
					
					if(strlen($row->Icon) > 3) {
						$obj->CategoryIcon = $row->Icon;
					} else if (strlen($row->SubCategoryIcon)  > 0) {
						$obj->CategoryIcon = $row->SubCategoryIcon;
					} else {
						$obj->CategoryIcon = $row->CategoryIcon;
					} 

					if($row->levelsearch == 2) {
						$obj->quicksearchurl = "http://www.bizzona.ru/detailssearch.php?cityId=".$row->CityID."&subCityId=".$row->subCityID."&catId=".$row->CategoryID."&subCatId=".$row->SubCategoryID."";					
					} else {
						$obj->quicksearchurl = "http://www.bizzona.ru/detailssearch.php?cityId=".$row->CityID."&subCityId=".$row->subCityID."&catId=".$row->CategoryID."&subCatId=".$row->SubCategoryID."&metroId=".$row->metroID."&districtId=".$row->districtID."";
					}
					
					$array[] = $obj;
				}
				
			}
			
			return $array;
		}
		
		
		
		function CountSelectSell() {
			
			$if_clause = "";
			$if_clause_sound = "";
			
			for ($i=0;$i<sizeof($this->awords);$i++)
			{
				$if_clause .= "iw.word='".$this->awords[$i]."' ";
				$if_clause_sound .= "iw.sound='".soundex(ruslat($this->awords[$i]))."' ";
				if($i!=sizeof($this->awords)-1) 
				{
					$if_clause .= " or "; 
					$if_clause_sound .= " or "; 
				}
			}
			$this->sql = "select distinct ii.sellId
										from 
											Sell LEFT JOIN SubCategory ON ( SubCategory.ID = Sell.SubCategoryID ) 
												 LEFT JOIN Category ON ( Category.ID = Sell.CategoryID ),
										    indexing_index_sell ii,
										    indexing_word_sell iw
										  		where (".$if_clause.") 
										  					and
										  				Sell.ID = ii.sellId 
										  					and 
										  				Sell.StatusID in (1,2) 
										  					and 
										  				ii.word = iw.id ";
			//echo $this->sql;

			$result = $this->db->Select($this->sql);
			
			$count = mysql_num_rows($result);
			
			if($count <= 0) 
			{
				$this->sql = "select distinct ii.sellId 
											from 
												Sell LEFT JOIN SubCategory ON ( SubCategory.ID = Sell.SubCategoryID ) 
												 	 LEFT JOIN Category ON ( Category.ID = Sell.CategoryID ),											
												indexing_index_sell ii,
												indexing_word_sell iw 
													where (".$if_clause_sound.") 
																and 
															Sell.ID = ii.sellId 
																and
															Sell.StatusID in (1,2) 
																and	
															ii.word = iw.id  ";
				$result = $this->db->Select($this->sql);
			
				$count = mysql_num_rows($result);
				
			}
			
			return $count;
		}		
		
		
		
	}

?>