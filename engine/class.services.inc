<?
include_once("class.db.inc");  	

class Services {

	private $_db;
	private $_sql;			
	public $_id;
	public $_shortName;
	public $_name;
	public $_description;
	public $_statusId;
	public $_isBroker;
	public $_isFastShow;
	public $_isPrice;
	public $_incr = 'up';
	public $_isCabinet;
 
	function __construct() {
		global $DATABASE;
		$this->InitDB();
	}

	function InitDB() {
		global $DATABASE;
		$this->db = new DB($DATABASE["HOSTNAME"], $DATABASE["DATABASENAME"], $DATABASE["DATABASEUSER"], $DATABASE["DATABASEPASSWORD"]);
	}

	function __destruct() {
			
	}		
	
	public function SetPosition() {
		if((int)$this->_id > 0) {
			if($this->_incr == 'up') {
				$this->_sql = " CALL sp_services_ordernum(".intval($this->_id).",'up'); ";
			} else if ($this->_incr == 'down') {
				$this->_sql = " CALL sp_services_ordernum(".intval($this->_id).",'down'); ";
			}
		}
		
		$this->db->CallSP($this->_sql);
	}
	
	public function Insert() {
		
		$this->_sql = " insert into Services (	`shortName`,
												`name`,
												`description`,
												`statusId`,
												`isBroker`,
												`isFastShow`,
												`isPrice`,
												`isCabinet`) 
													values (
													'".$this->_shortName."',
													'".$this->_name."',
													'".$this->_description."',
													".$this->_statusId.",
													".$this->_isBroker.",
													".$this->_isFastShow.",
													".$this->_isPrice.",
													".$this->_isCabinet."
													); ";
		$result = $this->db->Insert($this->_sql);	
		return $result;
				
	}

	public function Update() {
		
		$this->_sql =  " update Services set 	`shortName`='".$this->_shortName."', 
												`name`='".$this->_name."',
												`description`='".$this->_description."',
												`statusId`='".$this->_statusId."',
												`isBroker`='".$this->_isBroker."',
												`isFastShow`='".$this->_isFastShow."',
												`isPrice`='".$this->_isPrice."',  
												`isCabinet`='".$this->_isCabinet."'  where id=".(int)$this->_id." limit 1;";
		
		$result = $this->db->Sql($this->_sql);	
	}

        public function SelectForCabinet2() {
            
            $this->_sql = "select CategoryAction.name as CategoryActionName, Services.shortName as ServicesShortName, Services.name as 'ServicesName', Action.name as ServiceActionName, Action.price as ServiceActionPrice, Action.duration, Action.categoryActionId as CategoryActionId, Services.id as ServicesId, Service2Action.actionId as ServiceActionId from Services, Service2Action, Action, CategoryAction where CategoryAction.id=Action.categoryActionId and  Action.id=Service2Action.actionId and Service2Action.serviceId=Services.id  and Services.isCabinet=1 and Services.statusId=1";
            
            $result = $this->db->Select($this->_sql);
            
            $arr = array();
            
            while ($row = mysql_fetch_object($result)) {
                
                if(isset($arr[$row->ServicesId])) {
                    
                    if(isset($arr[$row->ServicesId]['Categories'][$row->CategoryActionId])) {
                        
                        $_temp = $arr[$row->ServicesId]['Categories'][$row->CategoryActionId]['Actions'];
                        
                        array_push( $_temp, 
                                        array(
                                                'ServiceActionId' => $row->ServiceActionId, 
                                                'ServiceActionName' => $row->ServiceActionName, 
                                                'ServiceActionPrice' => $row->ServiceActionPrice, 
                                                'CategoryActionId' => $row->CategoryActionId, 
                                        )
                        );
                        
                        $arr[$row->ServicesId]['Categories'][$row->CategoryActionId]['Actions'] = $_temp;
                        
                    } else {
                        
                        $arr[$row->ServicesId]['Categories'][$row->CategoryActionId] = array(
                              'CategoryActionName' => $row->CategoryActionName, 
                              'Actions' => array(
                                            array(
                                                    'ServiceActionId' => $row->ServiceActionId, 
                                                    'ServiceActionName' => $row->ServiceActionName, 
                                                    'ServiceActionPrice' => $row->ServiceActionPrice, 
                                                    'CategoryActionId' => $row->CategoryActionId, 
                                            )
                              )  
                        );
                        
                    }
                    
                    
                    
                } else {
                    
                    $arr[$row->ServicesId] = array(
                        'ServicesShortName' => strip_tags($row->ServicesShortName), 
                        'ServicesName' => strip_tags($row->ServicesName),
                        'ServicesId' => $row->ServicesId, 
                        'minPrice' => 0,
                        'Categories' => array(
                            $row->CategoryActionId => array(
                              'CategoryActionName' => $row->CategoryActionName,   
                              'Actions' => array(
                                            array(
                                                    'ServiceActionId' => $row->ServiceActionId, 
                                                    'ServiceActionName' => $row->ServiceActionName, 
                                                    'ServiceActionPrice' => $row->ServiceActionPrice, 
                                                    'CategoryActionId' => $row->CategoryActionId, 
                                            )
                              )  
                            ),
                        ),
                        
                    );
                    
                }
                
            }
            
            $arr = $this->CalculationMinPrice($arr);
            
            return $arr;
        }

        public function CalculationMinPrice($ar) {
            
            foreach($ar as $k => $v) {

                $minService = 0;
                
                foreach ($v['Categories'] as $k1 => $v1) {
                    
                    $min = 0;
                    
                    foreach ($v1['Actions'] as $k2 => $v2) {
                        
                        if($min <= 0) {
                            $min = $v2['ServiceActionPrice'];
                        } else if ($min > $v2['ServiceActionPrice']) {
                            $min = $v2['ServiceActionPrice'];
                        }
                        
                    }
                    
                    $minService += $min;
                }
                
  
                $ar[$k]['minPrice'] = $minService;
            }
            
            return $ar;
        }

        public function SelectForCabinet() {
		
		$this->_sql = "select ServiceAction.id as 'ServiceActionId', ServiceAction.name as 'ServiceActionName', ServiceAction.price as 'ServiceActionPrice', Services.id as 'ServicesId', Services.shortName as 'ServicesShortName', Services.name as 'ServicesName' from ServiceAction, Services  where Services.id=ServiceAction.serviceId and Services.statusId=1 and  ServiceAction.statusId=1 and Services.isCabinet=1;";
		
		$result = $this->db->Select($this->_sql);
		
		$arr = array();	

		
		while ($row = mysql_fetch_object($result)) {

			if(isset($arr[$row->ServicesId])) {
				
				$_aTemp = $arr[$row->ServicesId]['Actions'];
				
				$_lastPrise = $arr[$row->ServicesId]['minPrice'];
				if($row->ServiceActionPrice < $_lastPrise) {
					$arr[$row->ServicesId]['minPrice'] = $_lastPrise;
				}
				
				array_push( $_aTemp,
						array(
							'ServiceActionId' => $row->ServiceActionId, 
							'ServiceActionName' => $row->ServiceActionName, 
							'ServiceActionPrice' => $row->ServiceActionPrice, 
						)
				);
				
				$arr[$row->ServicesId]['Actions'] = $_aTemp;
				
			} else {
				
				$arr[$row->ServicesId] = array(
					'ServicesShortName' => $row->ServicesShortName, 
					'ServicesName' => $row->ServicesName,
					'ServicesId' => $row->ServicesId, 
					'minPrice' => $row->ServiceActionPrice, 
					'Actions' => array(
						array(
							'ServiceActionId' => $row->ServiceActionId, 
							'ServiceActionName' => $row->ServiceActionName, 
							'ServiceActionPrice' => $row->ServiceActionPrice, 
						)
					), 
				);
				
			}
			
		}
		
		
		return $arr;
	}
	
	public function SelectIsFastShow() {
		
		$this->_sql = ' select * from Services where isFastShow=\'1\' and statusId=\'1\' order by ordernum desc; ';
		$result = $this->db->Select($this->_sql);
			
		$arr = array();	

		while ($row = mysql_fetch_object($result)) {
					
			$aRow = array();
			$aRow['id'] = $row->id;
			$aRow['shortName'] = $row->shortName;
			$aRow['name'] = $row->name;
			$aRow['description'] = $row->description;
			
			array_push($arr, $aRow);
		}
		
		return $arr;
		
	}

	public function SelectIsBrokerShow() { 

		$this->_sql = ' select * from Services where isBroker=\'1\' and statusId=\'1\'; ';
		$result = $this->db->Select($this->_sql);
			
		$arr = array();	

		while ($row = mysql_fetch_object($result)) {
					
			$aRow = array();
			$aRow['id'] = $row->id;
			$aRow['shortName'] = $row->shortName;
			
			array_push($arr, $aRow);
		}
		
		return $arr;
		
		
	}
	
	public function SelectIsPriceShow() {
		
		$email_bizzona = '';
		$phone_bizzona = '';
		
		
		$this->_sql = 'select adsPhone, adsEmail from ContactInfo;';
		$result = $this->db->Select($this->_sql);	
		while ($row = mysql_fetch_object($result)) {
			$email_bizzona = $row->adsEmail;	
			$phone_bizzona = $row->adsPhone;
		}
		
		
		$aPayments = array();
		
		$this->_sql = 'select Name, Value from PaymentInfo;';
		$result = $this->db->Select($this->_sql);	
		while ($row = mysql_fetch_object($result)) {
			$aPayments[$row->Name] = $row->Value;
		}
		
		
		$this->_sql = ' select * from Services where isPrice=\'1\' and statusId=\'1\' order by ordernum desc; ';
		$result = $this->db->Select($this->_sql);
			
		$arr = array();	

		while ($row = mysql_fetch_object($result)) {
					
			$aRow = array();
			$aRow['id'] = $row->id;
			$aRow['shortName'] = $row->shortName;
			$aRow['name'] = $row->name;
			$aRow['description'] = $row->description;
			
			
			$aRow['description'] = str_replace("{{EMAIL}}", $email_bizzona, $aRow['description']);
			$aRow['description'] = str_replace("{{PHONE}}", $phone_bizzona, $aRow['description']);
			
			$aRow['description'] = str_replace("{{RWEBMONEY}}", $aPayments['RWEBMONEY'], $aRow['description']);
			$aRow['description'] = str_replace("{{ZWEBMONEY}}", $aPayments['ZWEBMONEY'], $aRow['description']);
			$aRow['description'] = str_replace("{{YAMONEY}}", $aPayments['YAMONEY'], $aRow['description']);
			
			
			$aRow['name'] = str_replace("{{EMAIL}}", $email_bizzona, $aRow['name']);
			$aRow['name'] = str_replace("{{PHONE}}", $phone_bizzona, $aRow['name']);
			
			array_push($arr, $aRow);
		}
		
		return $arr;
		
	}	
	
	public function Select() {
		
		$this->_sql = " select id, shortName, statusId, isBroker, isFastShow, isPrice, isCabinet, ordernum from Services ";
		
		$result = $this->db->Select($this->_sql);
			
		$arr = array();	

		while ($row = mysql_fetch_object($result)) {
					
			$aRow = array();
			$aRow['id'] = $row->id;
			$aRow['shortName'] = $row->shortName;
			$aRow['statusId'] = $row->statusId;
			$aRow['isBroker'] = $row->isBroker;
			$aRow['isFastShow'] = $row->isFastShow;
			$aRow['isPrice'] = $row->isPrice;
			$aRow['ordernum'] = $row->ordernum;
			$aRow['isCabinet'] = $row->isCabinet;
			array_push($arr, $aRow);
		}
		
		return $arr;
		
	}
	
	public function GetItem() {
		
		$arr = array();
		
		$this->_sql = " select * from Services where id=".(int)$this->_id.";";
		$result = $this->db->Select($this->_sql);
		while ($row = mysql_fetch_object($result)) {
			
			$arr['shortName'] = $row->shortName;
			$arr['name'] = $row->name;
			$arr['description'] = $row->description;
			$arr['statusId'] = $row->statusId;
			$arr['isBroker'] = $row->isBroker;
			$arr['isFastShow'] = $row->isFastShow;
			$arr['isPrice'] = $row->isPrice;
			$arr['isCabinet'] = $row->isCabinet;
		}
		
		return $arr;
	}
	
}
?>