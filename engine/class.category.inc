<?php
	include_once("class.db.inc");  
	require_once("Cache/Lite.php");
    

	class Category {
		var $db = "";
		var $metroID = "";
		var $sql = "";
		var $aGetItem = array();
		var $aGetName = array();
		var $aKeySell = array();
     	var $aKeyEquipment = array();
     
     	function Category() {
       		global $DATABASE;
       		$this->InitDB();
     	} 

     	function InitDB() {
       		global $DATABASE;
       		$this->db = new DB($DATABASE["HOSTNAME"], $DATABASE["DATABASENAME"], $DATABASE["DATABASEUSER"], $DATABASE["DATABASEPASSWORD"]);
     	}
     
     	function Close() {
     		$this->db->Close();
     	}

     	function Insert($data) {
       		$Name  = addslashes(trim($data["Name"]));
       		$sql = " insert into Category (`Name`) values ('".$Name."')";
       		$result = $this->db->Insert($sql);
     	}

     	function SelectForBroker($data) {
     	
     		$sql = "select COUNT(Category.ID) as Count, Category.ID, Category.Name, Category.keysell, Sell.brokerId, company.NameForSite from Category, Sell, company where company.ID=Sell.brokerId and Sell.statusID in (1,2) and Sell.CategoryID=Category.ID and Sell.brokerid=".intval($data["brokerId"])." group by Category.ID order by Count desc;";
     	
       		$result = $this->db->Select($sql);

       		$array = Array();
       	
       		while ($row = mysql_fetch_object($result)) {
        		$obj = new stdclass();
        		$obj->ID = $row->ID;
        		$obj->Name = $row->Name;
        		$obj->Count = $row->Count;
        		$obj->keysell = $row->keysell;
        		$obj->keybuy = $row->keybuy;
        		$obj->brokerId = $row->brokerId;
        		$obj->NameForSite = trim($row->NameForSite);
        		$array[] = $obj;
       		}
       	
       		return $array;     	
     	}	
     
     	function Select($data, $sqlDisplay = false) {
     	
       		if ($data["offset"] === 0 && $data["rowCount"] === 0) {
         		if (isset($data["sort"])) {
           			$sql = "Select ID, Name, Count, CountB, CountP, CountE, CountI, CountC, keysell, keybuy, keysellequipment from Category where 1 order  by ".$data["sort"]." desc; ";
         		} else { 
           			$sql = "Select ID, Name, Count, CountB, CountP, CountE, CountI, CountC, keysell, keybuy, keysellequipment from Category where 1; ";
         		}
       		} else {
         		$sql = "Select ID, Name, Count, CountB, CountP, CountE, CountI, CountC, keysell, keybuy, keysellequipment from Category where 1 order  by ".$data["sort"]."  desc limit ".intval($data["offset"]).",".$data["rowCount"].";";
       		} 

       		if ($sqlDisplay) {
          		echo $sql;
       		}

       		$result = $this->db->Select($sql);

       		$array = Array();
       		
       		while ($row = mysql_fetch_object($result)) {
         		$obj = new stdclass();
         		$obj->ID = $row->ID;
         		$obj->Name = $row->Name;
         		$obj->Count = $row->Count;
         		$obj->CountB = $row->CountB;
         		$obj->CountP = $row->CountP;
         		$obj->CountE = $row->CountE;
         		$obj->CountI = $row->CountI;
         		$obj->CountC = $row->CountC;
         		$obj->keysell = $row->keysell;
         		$obj->keybuy = $row->keybuy;
         		$obj->keysellequipment = $row->keysellequipment;
         		$array[] = $obj;
       		}
       		
       		return $array;
     	}

		
     	function Update($data) {
       		$sql = "update Category set  `Name`='".trim($data["Name"])."'  where ID='".trim($data["ID"])."'";
       		$result = $this->db->Delete($sql);
       		if (isset($data["Count"])) {
         		$this->UpdateCount($data);
       		}
     	}

     	function UpdateCount($data) {
       		$sql = "update Category set  `Count`=".trim($data["Count"])."  where ID='".trim($data["ID"])."'";
       		$result = $this->db->Delete($sql);
     	}

     	function UpdateCountB($data) {
       		$sql = "update Category set  `CountB`=".trim($data["CountB"])."  where ID='".trim($data["ID"])."'";
       		$result = $this->db->Delete($sql);
     	}

     	function UpdateCountP($data) {
       		$sql = "update Category set  `CountP`=".trim($data["CountP"])."  where ID='".trim($data["ID"])."'";
       		//echo $sql;
       		$result = $this->db->Delete($sql);
     	}
     
     	function UpdateCountE($data) {
       		$sql = "update Category set  `CountE`=".trim($data["CountE"])."  where ID='".trim($data["ID"])."'";
       		$result = $this->db->Delete($sql);
     	}

     	function UpdateCountI($data) {
       		$sql = "update Category set  `CountI`=".trim($data["CountI"])."  where ID='".trim($data["ID"])."'";
       		$result = $this->db->Delete($sql);
     	}

     	function UpdateCountC($data) {
       		$sql = "update Category set  `CountC`=".trim($data["CountC"])."  where ID='".trim($data["ID"])."'";
       		$result = $this->db->Delete($sql);
     	}

     
     	function Delete($data) {
       		$sql = "delete from  Category where ID='".trim($data["ID"])."'";
       		$result = $this->db->Delete($sql);
     	}
		
     	function GetNameList() {
     	
			$aName = array();
		
			$options = array(
    			'cacheDir' => PERSISTENTCACHE."/",
    			'lifeTime' => 2592000,
    			'pearErrorMode' => CACHE_LITE_ERROR_DIE
			);
		
			$cache = new Cache_Lite($options);
		
			if ($data = $cache->get('categorynames')) {

				$aName = unserialize($data);

			} else { 
		
				$sql = "Select ID, Name from Category;";
				$result = $this->db->Select($sql);
				while ($row = mysql_fetch_object($result)) {
					$aName[$row->ID] = $row->Name;
				}
				$cache->save(serialize($aName));
			}
		
			return 	$aName;     	
     	}
     
		function GetName($id) {
			$name = "";
     		if(isset($this->aGetName[$id])) {
     			$name = $this->aGetName[$id];	
     		} else {
     		
     			$result = $this->GetNameList();
     			$this->aGetName[$id] = $result[$id];
     			$name = $this->aGetName[$id];
			}
			return $name;
     	}
     
    	function GetKeySellList() {

			$akeysell = array();
		
			$options = array(
    			'cacheDir' => PERSISTENTCACHE."/",
    			'lifeTime' => 2592000,
    			'pearErrorMode' => CACHE_LITE_ERROR_DIE
			);
		
			$cache = new Cache_Lite($options);
		
			if ($data = $cache->get('categorykeyselllist')) {
				$akeysell = unserialize($data);

			} else { 
		
				$sql = "Select ID, keysell from Category;";
				$result = $this->db->Select($sql);
				while ($row = mysql_fetch_object($result)) {
					$akeysell[$row->ID] = $row->keysell;
				}
				$cache->save(serialize($akeysell));
			}
		
			return 	$akeysell;    	
    	}
     
    	function GetKeyEquipmentList() {

			$akeyEquipment = array();
		
			$options = array(
    			'cacheDir' => PERSISTENTCACHE."/",
    			'lifeTime' => 2592000,
    			'pearErrorMode' => CACHE_LITE_ERROR_DIE
			);
		
			$cache = new Cache_Lite($options);
		
			if ($data = $cache->get('categorykeyequipmentlist')) {
				$akeyEquipment = unserialize($data);

			} else { 
		
				$sql = "Select ID, keysellequipment from Category;";
				$result = $this->db->Select($sql);
				while ($row = mysql_fetch_object($result)) {
					$akeyEquipment[$row->ID] = $row->keysellequipment;
				}
				$cache->save(serialize($akeyEquipment));
			}
		
			return 	$akeyEquipment;    	
    	
    	}
    
		function GetKeyEquipmentOpt($id) {
			$keyEquipment = "";
     		if(isset($this->aKeyEquipment[$id])) {
     			$keyEquipment = $this->aKeyEquipment[$id];	
     		} else {
     		
     			$result = $this->GetKeyEquipmentList();
     			$this->aKeyEquipment[$id] = $result[$id];
     			$keyEquipment = $this->aKeyEquipment[$id];
			}
			return $keyEquipment;
     	}
    
		function GetKeySellOpt($id) {
			$keysell = "";
     		if(isset($this->aKeySell[$id])) {
     			$keysell = $this->aKeySell[$id];	
     		} else {
     		
     			$result = $this->GetKeySellList();
     			$this->aKeySell[$id] = $result[$id];
     			$keysell = $this->aKeySell[$id];
			}
			return $keysell;
     	}
     
		function GetItem($data) {
			$obj = new stdclass();
     		if(isset($this->aGetItem[$data["ID"]])) {
     			$obj = $this->aGetItem[$data["ID"]];	
     		} else {
				$sql = "Select ID, Name, Count, keysell, keybuy from Category where ID=".trim($data["ID"]).";";
				$result = $this->db->Select($sql);
				while ($row = mysql_fetch_object($result)) {
					$obj->ID = $row->ID;
					$obj->Name = $row->Name;
					$obj->Count = $row->Count;
					$obj->keysell = $row->keysell;
					$obj->keybuy = $row->keybuy;
				} 
				$this->aGetItem[$data["ID"]] = $obj;
			}
			return $obj;
     	}

     	function Display($data) {
     	}

     	function CountRows() {
     	}

     	function GetListSubCategoryActive() {
     		$sql = "SELECT `Sell`.`SubCategoryID`,`Sell`.`CategoryID`, count( `Sell`.`SubCategoryID` ) AS 'Count', `SubCategory`.`keysell` 
									FROM `Sell` , `SubCategory` 
										WHERE 1 
											AND `SubCategory`.`ID` = `Sell`.`SubCategoryID` 
											AND `Sell`.`StatusID` IN ( 1, 2 ) 
											GROUP BY `Sell`.`SubCategoryID` 
											ORDER BY `count` DESC ";
     	
       		$result = $this->db->Select($sql);
       	

			$array = Array();
       	
       		while ($row = mysql_fetch_object($result)) {
       			if(strlen($row->keysell) > 0) {
       			
       				$obj = new stdclass();
        			$obj->SubCategoryID = $row->SubCategoryID;
        			$obj->CategoryID = $row->CategoryID;
         			$obj->Count = $row->Count;
         			$obj->KeySell = strtolower($row->keysell);
         			$array[] = $obj;
       			}
       		} 
       		return $array;
     	}

     	function GetListSubCategoryActiveBuy() {
     		$sql = "SELECT `buyer`.`subTypeBizID`,`buyer`.`typeBizID`, count( `buyer`.`subTypeBizID` ) AS 'Count', `SubCategory`.`keybuy` 
									FROM `buyer` , `SubCategory` 
										WHERE 1 
											AND `SubCategory`.`ID` = `buyer`.`subTypeBizID` 
											AND `buyer`.`statusID` IN (1) 
											GROUP BY `buyer`.`subTypeBizID` 
											ORDER BY `count` DESC ";
     	
       		$result = $this->db->Select($sql);

			$array = Array();
       	
       		while ($row = mysql_fetch_object($result)) {
       			if(strlen($row->keybuy) > 0) {
       			
       				$obj = new stdclass();
        			$obj->SubCategoryID = $row->subTypeBizID;
        			$obj->CategoryID = $row->typeBizID;
         			$obj->Count = $row->Count;
         			$obj->KeyBuy = strtolower($row->keybuy);
         			$array[] = $obj;
       			}
       		}
       		return $array;
     	}
     
     	function GetKeySell($id) {
     	
     		$sql = "select keysell from Category where ID=".intval($id)."";
			//echo $sql;
     		$result = $this->db->Select($sql);
     		while ($row = mysql_fetch_object($result)) {
     			return $row->keysell;
     		}
     	
     		return "";
     	}

     	function GetKeyBuy($id) {
     	
     		$sql = "select keybuy from Category where ID=".intval($id)."";
     		$result = $this->db->Select($sql);
     		while ($row = mysql_fetch_object($result)) {
     			return $row->keybuy;
     		}
     	
     		return "";
     	}


     	function GetKeySellEquipment($id) {
     		
     		$sql = "select keysellequipment from Category where ID=".intval($id)."";
     		$result = $this->db->Select($sql);
     		while ($row = mysql_fetch_object($result)) {
     			return $row->keysellequipment;
     		}
     	
     		return "";
     	}

     
     	function SelectByMetro() {
     	
     		$sql = 'select 	Category.name,
     					Category.ID,
     					s.Count as \'Count\'
     							from Category LEFT JOIN (select Sell.categoryID, COUNT(Sell.categoryID) as \'Count\' from Sell where Sell.metroID='.$this->metroID.' GROUP BY Sell.categoryID) s on (s.categoryID = Category.ID)'
        			. ' ORDER BY s.Count desc';
        		
			$result = $this->db->Select($sql);

			$array = Array();
			while ($row = mysql_fetch_object($result)) {
				$obj = new stdclass();
				$obj->ID = $row->ID;
				$obj->Name = $row->name;
				$obj->Count = ($row->Count > 0 ? $row->Count : 0);
				$array[] = $obj;
			}
			return $array;
     	}
     
     
     	function SelectByMetro2Buyer() {
     		$this->sql ="select 	Category.name,
									Category.ID,
									Category.keybuy,
									b.Count as 'Count'
										from Category LEFT JOIN 
											(select buyer.typeBizID, count(buyer.typeBizID) as 'Count'  from buyer, buyermetro where buyermetro.buyerID=buyer.id and metroID=".$this->metroID." group by buyer.typeBizID) b on (b.typeBizID = Category.ID)  ORDER BY b.Count desc;";
			$result = $this->db->Select($this->sql);

			$array = Array();
			while ($row = mysql_fetch_object($result)) {
				$obj = new stdclass();
				$obj->ID = $row->ID;
				$obj->Name = $row->name;
				$obj->keybuy = $row->keybuy;
				$obj->Count = ($row->Count > 0 ? $row->Count : 0);
				$array[] = $obj;
			}
			return $array;
     	}
  	}
?>