<?php
	include_once("class.db.inc");  	
	
	class Bizterra {
		
		
		private $db = "";
		public $sql = "";
		public $Id = "";
		public $sellId = "";
		public $languageId = "1";
		public $fml = "";
		public $email = "";
		public $phone = "";
		public $name = "";
		public $shortDescription = "";
		public $description = "";
		public $fax = "";
		public $cost = "";
		public $cCostId = "";
		public $monthAveTurn = ""; 
		public $cMonthAveTurnId = "";
		public $maxMonthAveTurn = "";
		public $cMaxMonthAveTurnId = "";
		public $profitPerMonth = "";
		public $cProfitPerMonthId = "";
		public $maxProfitPerMonth = "";
		public $cMaxProfitPerMonthId = "";
		public $monthExpemse = "";
		public $cMonthExpemseId = "";
		public $maxMonthExpemse = "";
		public $cMaxMonthExpemseId = "";
		public $sumDebts = "";
		public $cSumDebtsId = "";
		public $wageFund = "";
		public $cWageFundId = "";
		public $address = "";
		public $listObject = "";
		public $contact = "";
		public $matProperty = "";
		public $obligation = "";
		public $timeInvest = "";
		public $countEmpl = "";
		public $countManEmpl = "";
		public $termBiz = "";
		public $shareSale = "";
		public $reasonSellBizId = "";
		public $bizFormId = "";
		public $countryId = "";
		public $count = "";
		public $addressId = "";
		public $statusId = "1,2";
		public $startPosition = "0";
		public $stopPosition = "30";
		public $addressGenerate = "";
		public $categoryId = "";
		public $notifyStatusId = 0;
		public $categories = "";
		public $sendEmailCreate = 1;
		public $sourceId = 0;
		
		private $sortColumn = "bizterra_sell.Id";
		private $sortOrder = "desc";
		
		public $currentCurrency = 0;
		
		
		
		function __construct() {
			global $DATABASE;
			$this->InitDB();
		}

		function InitDB() {
			global $DATABASE;
			$this->db = new DB($DATABASE["HOSTNAME"], $DATABASE["DATABASENAME"], $DATABASE["DATABASEUSER"], $DATABASE["DATABASEPASSWORD"]);
		}

		function __destruct() {
			
		}
		
		public function ListHotSells() { 
			
			$this->sql = "use bizterra;";
			$result = $this->db->Select($this->sql);

			$this->sql = "
							select
									bizterra_sell.Id as 'sellId',
									TO_DAYS(NOW()) - TO_DAYS(bizterra_sell.dateCreate) as 'dayDiff',
									bizterra_sell.categories,
									bizterra_sell_data.Id,
									bizterra_sell_data.id as 'sortId', 
									bizterra_sell_data.addressId,
									bizterra_sell_data.cost,
									bizterra_sell_data.cCostId,
									(bizterra_sell_data.cost*bizterra_sell_data.cCostId) as 'sortCost',
									bizterra_sell_data.count,
									bizterra_sell_data.name,
									bizterra_sell_data.shortDescription, 
									bizterra_sell_data.count,
									bizterra_sell_data.timeInvest,
									bizterra_sell_data.monthAveTurn,
									bizterra_sell_data.cMonthAveTurnId,
									(bizterra_sell_data.monthAveTurn*bizterra_sell_data.cMonthAveTurnId) as 'sortMonthAveTurn',
									bizterra_sell_data.maxMonthAveTurn,
									bizterra_sell_data.cMaxMonthAveTurnId,
									(bizterra_sell_data.maxMonthAveTurn*bizterra_sell_data.cMaxMonthAveTurnId) as 'sortMaxMonthAveTurn',
									bizterra_sell_data.monthExpemse,
									bizterra_sell_data.cMonthExpemseId,
									(bizterra_sell_data.monthExpemse * bizterra_sell_data.cMonthExpemseId) as 'sortMonthExpemse',
									bizterra_sell_data.maxMonthExpemse,
									bizterra_sell_data.cMaxMonthExpemseId,
									(bizterra_sell_data.maxMonthExpemse*bizterra_sell_data.cMaxMonthExpemseId) as 'sortMaxMonthExpemse',
									bizterra_sell_data.count,
									bizterra_sell_data.addressGenerate
									
								from
									bizterra_sell,
									bizterra_sell_data
								where 1=1
									and
									bizterra_sell_data.sellId = bizterra_sell.Id
									and
									bizterra_sell_data.languageId=".$this->languageId."
									and
									bizterra_sell_data.statusId in (2) ";
			
			$this->sql = $this->sql."
									 group by bizterra_sell_data.Id order by ".$this->sortColumn." ".$this->sortOrder." limit ".$this->startPosition.", ".$this->stopPosition.";";
		
			
			$result = $this->db->Select($this->sql);
			
			$arr = array();	

			while ($row = mysql_fetch_object($result)) {

				$obj = new stdclass();
				$obj->Id = $row->Id;
				$obj->sellId = $row->sellId;
				$obj->dayDiff = $row->dayDiff;
				$obj->addressId = $row->addressId;
				$obj->cost = $this->ParseToMoney($row->cost, $row->cCostId);
				$obj->cCostId = $this->ParseToCurrency($row->cCostId);
				$obj->count = $row->count;
				$obj->name = $row->name;
				$obj->shortDescription = $row->shortDescription;
				$obj->count = $row->count;
				$obj->timeInvest = $row->timeInvest;
				$obj->monthAveTurn =  $this->ParseToMoney($row->monthAveTurn, $row->cMonthAveTurnId);
				$obj->cMonthAveTurnId = $this->ParseToCurrency($row->cMonthAveTurnId);
				$obj->maxMonthAveTurn = $this->ParseToMoney($row->maxMonthAveTurn, $row->cMaxMonthAveTurnId);
				$obj->cMaxMonthAveTurnId = $this->ParseToCurrency($row->cMaxMonthAveTurnId);
				$obj->monthExpemse = $this->ParseToMoney($row->monthExpemse, $row->cMonthExpemseId);
				$obj->cMonthExpemseId = $this->ParseToCurrency($row->cMonthExpemseId);
				$obj->maxMonthExpemse = $this->ParseToMoney($row->maxMonthExpemse, $row->cMaxMonthExpemseId);
				$obj->cMaxMonthExpemseId = $this->ParseToCurrency($row->cMaxMonthExpemseId);
				$obj->count = $row->count;
				$obj->addressGenerate = $row->addressGenerate;
				$obj->aAddressGenerate = split(",", $row->addressGenerate);
				$obj->categories = $row->categories;
				
				if($obj->cCostId == 1) {
					$obj->cCostId = "rub";
				} else if ($obj->cCostId == 2) {
					$obj->cCostId = "eur";
				} else if ($obj->cCostId == 3) {
					$obj->cCostId = "usd";
				}

				if($obj->cMonthAveTurnId == 1) {
					$obj->cMonthAveTurnId = "rub";
				} else if ($obj->cMonthAveTurnId == 2) {
					$obj->cMonthAveTurnId = "eur";
				} else if ($obj->cMonthAveTurnId == 3) {
					$obj->cMonthAveTurnId = "usd";
				}

				if($obj->cMonthExpemseId == 1) {
					$obj->cMonthExpemseId = "rub";
				} else if ($obj->cMonthExpemseId == 2) {
					$obj->cMonthExpemseId = "eur";
				} else if ($obj->cMonthExpemseId == 3) {
					$obj->cMonthExpemseId = "usd";
				}

				if($obj->cMaxMonthExpemseId == 1) {
					$obj->cMaxMonthExpemseId = "rub";
				} else if ($obj->cMaxMonthExpemseId == 2) {
					$obj->cMaxMonthExpemseId = "eur";
				} else if ($obj->cMaxMonthExpemseId == 3) {
					$obj->cMaxMonthExpemseId = "usd";
				}
				
				
				array_push($arr, $obj);
			} 
			
			$this->sql = "use bizzona;";
			$result = $this->db->Select($this->sql);
			
			
			return $arr;
			
		}

		public function ParseToCurrency($currency) {
			if($this->currentCurrency != 0 && $currency != $this->currentCurrency) {
				$currency = $this->currentCurrency;
			}
			return $currency;
		}
		
		
		public function ParseToMoney($money, $currency) {
			
			if($this->currentCurrency != 0 && $currency != $this->currentCurrency) {
				switch ($currency) {
					case "1": {
						if($this->currentCurrency == 2) {
							$money = intval($money/35);
						} else if($this->currentCurrency == 3) {
							$money = intval($money/26);
						}
					} break;
					case "2": {
						if($this->currentCurrency == 1) {
							$money = $money*35;
						} else if($this->currentCurrency == 3) {
							$money = intval($money*1.38);
						}
					} break;
					case "3": {
						if($this->currentCurrency == 1) {
							$money = $money*26;
						} else if($this->currentCurrency == 2) {
							$money = intval($money/1.38);
						}
					}
					default:{
						
					}
				}
			}
			
			$money = trim(strrev(chunk_split (strrev($money), 3,' ')));
			return $money;
		}
		

		
	}
?>
