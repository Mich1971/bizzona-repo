<?php
	include_once("class.db.inc");  

	class RoomCategory 	{

		public $db = "";
		public $sql = "";
		public $Id = 0;
		public $Name = "";
		public $Description = "";		
		public $SeoTitle = "";
		 
		function __construct() {
			global $DATABASE;
			$this->InitDB();
		}

		function InitDB() {
			global $DATABASE;
			$this->db = new DB($DATABASE["HOSTNAME"], $DATABASE["DATABASENAME"], $DATABASE["DATABASEUSER"], $DATABASE["DATABASEPASSWORD"]);
		}
		
		function GetItem() {
			
			$_roomCategory = new RoomCategory();
			$this->sql = "select Id, Name, Description, SeoTitle from RoomCategory where 1 and Id=".intval($this->Id).";";
			
			$result = $this->db->Select($this->sql);
			while ($row = mysql_fetch_object($result)) {
				$_roomCategory->Id = intval($row->Id);
				$_roomCategory->Name = $row->Name;
				$_roomCategory->Description = $row->Description;
				$_roomCategory->SeoTitle = $row->SeoTitle;
			}
			return $_roomCategory;
			
		}		
		
		function Select() {

			$array = Array();
			
			$this->sql = "select Id, Name, Description, SeoTitle from RoomCategory where 1 order by Name;";
			
			$result = $this->db->Select($this->sql);
			while ($row = mysql_fetch_object($result)) {
				
				$_roomCategory = new RoomCategory();
				$_roomCategory->Id = intval($row->Id);
				$_roomCategory->Name = $row->Name;
				$_roomCategory->Description = $row->Description;
				$_roomCategory->SeoTitle = $row->SeoTitle;
				
				$array[] = $_roomCategory;
			}
			
			return $array;
		}
		
		function Close() {
			$this->db->Close();
		}		
		
	}

?>