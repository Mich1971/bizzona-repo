<?php
include_once("interface.sellsdapter.inc");
include_once("../engine/class.sell.inc");
include_once("../engine/functions.inc"); 
include_once("../engine/typographus.php"); 
include_once("../engine/class.mappingobjectbroker.inc"); 	
require_once("../engine/Cache/Lite.php");
include_once($_SERVER['DOCUMENT_ROOT']."/engine/CreateSubDirImg.php"); 

function save_image($inPath,$outPath) { 

    $in = fopen($inPath, "rb");     
    $out =  fopen($outPath, "wb");     
    while ($chunk = fread($in,8192)) {         
        fwrite($out, $chunk, 8192);     
    }     
    fclose($in);     
    fclose($out); 
} 	
 
class Skaniainvest implements SellAdapter {

    public $db = "";
    
    public $_id;
    public $_dateupdate;
    public $_price;
    public $_profitpermonth;
    public $_monthaveturn;
    public $_monthexpemse;
    public $_timeinvest;
    public $_listobject;
    public $_matproperty;
    public $_countempl;
    public $_countmanempl;
    public $_termbiz;
    public $_sharesale;
    public $_offertype;
    public $_businesstype;
    public $_phone;
    public $_contact;
    public $_title;
    public $_description;
    public $_shortdescription;
    public $_pic = array();

    public $_urlBrokerObject;

    public $_email;
    public $_town;
    public $_orgform;  
    public $_reason; 
 
    public $brokerId = 4; 
 
    public $countProcessed = 0;
    public $packetCount = 2; 
    
    public $useDefaultEmail = true;
    public $defaultEmail = 'bz@skaniainvest.ru';

    public $useDefaultPhone = true;
    public $defaultPhones = array(
        '77' => '+7 (495) 125-34-48',
        '78' => '+7 (812) 385-64-52',
        '2538' => '+7 (800) 700-49-14',
    );
    
    //public $url = "http://skania-admin.ru/export/bizzona.php";
    public $url = "http://skadmin.ru/service/export/export_bizzona.php"; 
 
    private $array_skaniainvest = array();


    public $orgform_maps = array(
        "���" => "1",
        "���������������" => "8"
    ); 

    /*
    public $category_maps = array (
        "�����������, ����������, ���, ���������" => "90",
        "������, ������������, ���.������" => "105",
        "���������, ���� ������" => "93",
        "�������� ��������, ��������-�������" => "114",
        "������������ ������������" => "94",
        "��������, ���������, �������� �������" => "115",
        "������������ � �������������" => "94",
        "������� ��������" => "89",
        "����������� ����� �����" => "117",
        "������������, ������" => "82",
        "���������, ����, ����, �����" => "92",
        "������ �������, ������" => "109",
        "�������� ���������" => "78",
        "�������������" => "87",
        "������������ ��������" => "97",
        "������������, ������������" => "96",
        "���������� ������" => "119",
        "������ �� �������" => "",
        "������" => ""
    );
    */

    
//    public $category_maps = array (
//        "1" => "78", /* ���� */
//        "101" => "94", /* �������� ������ */
//        "135" => "117", /* ������� ������ */
//        "199" => "119", /* ������� ������ � ������ */
//        "359" => "81", /* ������ */
//        "388" => "114", /* ��������, it � ���������� */
//        "447" => "109", /* ������� � �������� */
//        "530" => "106", /* ����������� */
//        "553" => "89", /* ��� � ����������� */
//        "587" => "96", /* �����, ������, ����� */
//        "693" => "92", /* ������� */
//        "820" => "82", /* ������������ */
//        "1236" => "107", /* ��� */
//        "1259" => "87", /* ������������� � ������ */
//        "1345" => "115", /* �������� */
//        "1587" => "97", /* ��������� */
//        "������" => ""
//    );    
    
    public $category_maps = array (
        "����" => "78", 
        "�������� ������" => "94", 
        "������� ������" => "117", 
        "������� ������ � ������" => "119", 
        "������" => "81", 
        "��������, it � ����������" => "114", 
        "������� � ��������" => "109", 
        "�����������" => "106", 
        "��� � �����������" => "89", 
        "�����, ������, �����" => "96", 
        "�������" => "92", 
        "������������" => "82", 
        "���" => "107", 
        "������������� � ������" => "87", 
        "��������" => "115", 
        "���������" => "97", 
        "������" => "" 
    );     
        
    public $town_maps = array(
 
        "�����" => array("cityId" => "69", "subCityId" => "2048"),
        "�������" => array("cityId" => "40", "subCityId" => "662"),
        "������" => array("cityId" => "77", "subCityId" => ""),
        "���������� �������" => array("cityId" => "77", "subCityId" => ""),
        "�����-���������" => array("cityId" => "78", "subCityId" => ""),
        "�������" => array("cityId" => "50", "subCityId" => "3699"),
        "�������" => array("cityId" => "50", "subCityId" => "1187"),
        "���" => array("cityId" => "2", "subCityId" => "304"),
        "�������" => array("cityId" => "50", "subCityId" => "1202"),
        "��������" => array("cityId" => "50", "subCityId" => "1191"),
        "���������" => array("cityId" => "50", "subCityId" => "1192"),
        "��������" => array("cityId" => "50", "subCityId" => ""),
        "�����-���������" => array("cityId" => "78", "subCityId" => ""),
        "�����������" => array("cityId" => "50", "subCityId" => "1168"),
        "���������������" => array("cityId" => "50", "subCityId" => "3700"),
        "������������" => array("cityId" => "50", "subCityId" => ""),
        "�����" => array("cityId" => "50", "subCityId" => ""),
        "��������" => array("cityId" => "50", "subCityId" => "1188"),
        "�������" => array("cityId" => "50", "subCityId" => "1179"),
        "�������" => array("cityId" => "50", "subCityId" => "1184"),
        "���������" => array("cityId" => "50", "subCityId" => "1164"),
        "�����" => array("cityId" => "50", "subCityId" => "1199"),
        "������" => array("cityId" => "50", "subCityId" => "3698"),
        "�������" => array("cityId" => "50", "subCityId" => "1197"),
        "������" => array("cityId" => "50", "subCityId" => "1201"),
        "��������" => array("cityId" => "50", "subCityId" => "1166"),
        "������" => array("cityId" => "50", "subCityId" => "1176"),
        "����-�������" => array("cityId" => "50", "subCityId" => "1186"),
        "������������" => array("cityId" => "50", "subCityId" => "1203"),
        "�������" => array("cityId" => "50", "subCityId" => "1170"),
        "��������" => array("cityId" => "50", "subCityId" => "1177"),
        "�����" => array("cityId" => "50", "subCityId" => "1182"),
        "�������" => array("cityId" => "50", "subCityId" => "1187"),
        "�������-�����" => array("cityId" => "50", "subCityId" => "1193"),
        "���������" => array("cityId" => "50", "subCityId" => "1173"),
        "�������" => array("cityId" => "50", "subCityId" => "1202"),
        "����������" => array("cityId" => "50", "subCityId" => "1171"),
        "����" => array("cityId" => "50", "subCityId" => "1178"),
        "��������" => array("cityId" => "50", "subCityId" => "1183"),
        "��������" => array("cityId" => "50", "subCityId" => "1188"),
        "��������" => array("cityId" => "50", "subCityId" => "1194"),
        "�����������" => array("cityId" => "50", "subCityId" => "1167"),
        "�����" => array("cityId" => "50", "subCityId" => "1172"),
        "�������" => array("cityId" => "50", "subCityId" => "1179"),
        "�������" => array("cityId" => "50", "subCityId" => "1185"),
        "�������-�����" => array("cityId" => "50", "subCityId" => "1189"),
        "�������������" => array("cityId" => "50", "subCityId" => "1195"),
        "�����������" => array("cityId" => "50", "subCityId" => "1168"),
        "����" => array("cityId" => "2538", "subCityId" => ""),
    );

    function __construct() {
        global $DATABASE;
        $this->InitDB();
    }

    function InitDB() {
        global $DATABASE;
        $this->db = new DB($DATABASE["HOSTNAME"], $DATABASE["DATABASENAME"], $DATABASE["DATABASEUSER"], $DATABASE["DATABASEPASSWORD"]);
    }    
    
    public function Result() {
 
        return $this->array_skaniainvest;
    }

    private function RemovetagModerate($str) {
        
        $str = str_replace(
                array(  '[p]', '[/p]', 
                        '[div]', '[/div]', 
                        '[ul]', '[/ul]',
                        '[ol]', '[/ol]',
                        '[li]', '[/li]',
                        '[br]', '[/br]',
                        '[table]', '[/table]',
                        '[tr]', '[/tr]',
                        '[th]', '[/th]',
                        '[td]', '[/td]',
                        ), 
        '', $str);
        
        return $str;
    }
    
    private function ModerateContent($content) {

        $content = strip_tags($content);
        
        $content = str_replace('[p]', '<p>', $content);
        $content = str_replace('[/p]', '</p>', $content);

        $content = str_replace('[div]', '<div>', $content);
        $content = str_replace('[/div]', '</div>', $content);
          
        $content = str_replace('[ul]', '<ul>', $content);
        $content = str_replace('[ol]', '<ol>', $content);
        $content = str_replace('[li]', '<li>', $content); 
        $content = str_replace('[/li]', '</li>', $content);
        $content = str_replace('[/ol]', '</ol>', $content);
        $content = str_replace('[/ul]', '</ul>', $content);        
        
        $content = str_replace('[br]', '</br>', $content);
        
        $content = str_replace('[table]', '<table style="border: solid black 1px;margin:5pt;" width="95%">', $content);
        $content = str_replace('[tr]', '<tr>', $content);
        $content = str_replace('[td]', '<td style="background-color:#e3e6e8;" valign="top">', $content);
        $content = str_replace('[th]', '<th style="background-color:#ccd8de;"  valign="top">', $content);

        $content = str_replace('[/table]', '</table>', $content);
        $content = str_replace('[/tr]', '</tr>', $content);
        $content = str_replace('[/td]', '</td>', $content);
        $content = str_replace('[/th]', '</th>', $content);
        
        return $content;
    }
 
    private function LoadImg($url, $pref = 'sell_') {
        
        $newname = uniqid($pref);
        $ext = substr($url, 1 + strrpos($url, "."));
        
        $path = '../simg/';
        
        $result_name = CreateSubDirImg($path).'/'.$newname.".".$ext;
         
        file_put_contents($result_name, file_get_contents($url));

        $result_name = str_replace($path, '', $result_name);        
        
        return $result_name;
    }
    
    public function ConvertData() {

        $data = array();

        $data["FML"] =  htmlspecialchars($this->_contact, ENT_QUOTES);
        
        if($this->useDefaultEmail) {
            $data["EmailID"] = $this->defaultEmail;
        } else {
            $data["EmailID"] = htmlspecialchars($this->_email, ENT_QUOTES);
        }
        $data["PhoneID"] = htmlspecialchars($this->_phone, ENT_QUOTES);
        $data["NameBizID"] = htmlspecialchars($this->_title, ENT_QUOTES);


        if(isset($this->orgform_maps[$this->_orgform])) {
            $data["BizFormID"] = $this->orgform_maps[$this->_orgform];
        } else {
            $data["BizFormID"] = "";
        }
 
        if(isset($this->town_maps[$this->_town])) {
            $data["CityID"] = $this->town_maps[$this->_town]["cityId"];
            $data["SubCityID"] = $this->town_maps[$this->_town]["subCityId"];
            
            if($this->useDefaultPhone) {
                if(isset($this->defaultPhones[$data["CityID"]])) {
                    $data["PhoneID"] = $this->defaultPhones[$data["CityID"]];
                }
            }
            
        } else {
            $data["CityID"] = "";
            $data["SubCityID"] = "";
        }

        $data["SiteID"] = htmlspecialchars($this->_town, ENT_QUOTES);
        $data["CostID"] = intval($this->_price);
        $data["cCostID"] = "rub";
        $data["txtCostID"] = $this->_price;
        $data["txtProfitPerMonthID"] = "";
        $data["ProfitPerMonthID"] = $this->_profitpermonth;
        $data["cProfitPerMonthID"] = "rub";
        $data["cMaxProfitPerMonthID"] = "";
        $data["TimeInvestID"] = htmlspecialchars($this->_timeinvest, ENT_QUOTES);
        $data["ListObjectID"] = htmlspecialchars($this->_listobject, ENT_QUOTES); 
        $data["ContactID"] = "";//$this->_contact;
        $data["MatPropertyID"] = htmlspecialchars($this->_matproperty, ENT_QUOTES); 
        $data["txtMonthAveTurnID"] = "rub";
        $data["MonthAveTurnID"] = $this->_monthaveturn;
        $data["cMonthAveTurnID"] = "rub";
        $data["cMaxMonthAveTurnID"] = "";
        $data["txtMonthExpemseID"] = "";
        $data["MonthExpemseID"] = $this->_monthexpemse;
        $data["cMonthExpemseID"] = "rub";
        $data["txtSumDebtsID"] = "";
        $data["SumDebtsID"] = "";
        $data["cSumDebtsID"] = "";
        $data["txtWageFundID"] = "";
        $data["WageFundID"] = "";
        $data["cWageFundID"] = "";
        $data["ObligationID"] = "";
        $data["CountEmplID"] = $this->_countempl;
        $data["CountManEmplID"] = $this->_countmanempl;
        $data["TermBizID"] = $this->_termbiz;
 
        $data["ShortDetailsID"] = htmlspecialchars($this->_shortdescription, ENT_QUOTES); 
        $data["DetailsID"] = htmlspecialchars($this->_description, ENT_QUOTES); 


        $data["FaxID"] = ""; 
        $data["ShareSaleID"] = "";
        $data["ReasonSellBizID"] = "";
        $data["TarifID"] = ""; 
        $data["StatusID"] = "";
        $data["DataCreate"] = "";
        $data["DateStart"] = "";
        $data["TimeActive"] = "";


        $data["CategoryID"] =  (isset($this->category_maps[$this->_businesstype]) ? $this->category_maps[$this->_businesstype] : "");
        $data["SubCategoryID"] = "";
        $data["ConstGUID"] = "";
        $data["ip"] = "";
        $data["defaultUserCountry"] = "";
        $data["login"] = "";
        $data["password"] = "";
        $data["newspaper"] = "";
        $data["helpbroker"] = "";
        $data["youtubeURL"] = "";    		
        $data["brokerId"] = $this->brokerId;
        $data["txtReasonSellBizID"] = htmlspecialchars($this->_reason, ENT_QUOTES);

        $data["source"] = "�����: ������-������"; 
        
        //up Typographus

        $typo = new Typographus();

        $data["NameBizID"] = $typo->process($data["NameBizID"]);
        $data["SiteID"] = $typo->process($data["SiteID"]);
        $data["ListObjectID"] = $typo->process($data["ListObjectID"]);
        $data["MatPropertyID"] = $typo->process($data["MatPropertyID"]);
        $data["ShortDetailsID"] = $typo->process($data["ShortDetailsID"]);
        $data["DetailsID"] = $typo->process($data["DetailsID"]);
        
        if(strlen($data["DetailsID"]) <= 0) {
            $data["DetailsID"] = $data["ShortDetailsID"];
        } 

        if(strlen($data["ShortDetailsID"]) > 0) {
            
            $tmpShortDescription = $this->RemovetagModerate($data["ShortDetailsID"]);
            $aTmpShortDescription = explode('.', $tmpShortDescription);
            if(is_array($aTmpShortDescription) && count($aTmpShortDescription) > 4) {
                $data["ShortDetailsID"] =  $aTmpShortDescription[0].'. '.$aTmpShortDescription[1].'. '.$aTmpShortDescription[2].'. '.$aTmpShortDescription[3].'. ';
            }
        } else {
            $data["ShortDetailsID"] = $data["NameBizID"];
        }
        
        //down Typographus    		

        if(count($this->_pic)) {
            
            if(isset($this->_pic[0])) {
                $data["ImgID"] = $this->LoadImg(trim($this->_pic[0]));
            } else {
                $data["ImgID"] = ''; 
            }

            if(isset($this->_pic[1])) {
                $data["Img1ID"] = $this->LoadImg(trim($this->_pic[1])); 
            } else {
                $data["Img1ID"] = '';
            }
            
            if(isset($this->_pic[2])) {
                $data["Img2ID"] = $this->LoadImg(trim($this->_pic[2])); 
            } else {
                $data["Img2ID"] = '';
            }

            if(isset($this->_pic[3])) { 
                $data["Img3ID"] = $this->LoadImg(trim($this->_pic[3]));
            } else {
                $data["Img3ID"] = '';
            }
            
            if(isset($this->_pic[4])) { 
                $data["Img4ID"] = $this->LoadImg(trim($this->_pic[4]));
            } else {
                $data["Img4ID"] = '';
            }            
            
        } else {
            
            $data["ImgID"] = ''; 
            $data["Img1ID"] = ''; 
            $data["Img2ID"] = ''; 
            $data["Img3ID"] = '';
            $data["Img4ID"] = '';
        }
        
        return $data;
    }  

    public function CallData() {
        
        //$buffer = file_get_contents($this->url);
          
        //$buffer = iconv('windows-1251','windows-1251//IGNORE', $buffer);
        
        //$buffer = preg_replace('/[^\x{0009}\x{000a}\x{000d}\x{0020}-\x{D7FF}\x{E000}-\x{FFFD}]+/u', ' ', $buffer);
         
        //$xml = simplexml_load_string($buffer);   
         
        $xml = simplexml_load_file($this->url);
        $this->ReadDataCallBack($xml);
    } 


    private function convertXmlObjToArr($obj, &$arr) { 

        if(!is_object($obj)) throw new Exception("Bizzona.ru: ������ ������� ������ xml ����� �� �������� ������ ������");

        $children = $obj->children(); 
        
        foreach ($children as $elementName => $node) { 
            
            $nextIdx = count($arr); 
            $arr[$nextIdx] = array(); 
            $arr[$nextIdx]['@name'] = strtolower((string)$elementName); 
            $arr[$nextIdx]['@attributes'] = array(); 
            $attributes = $node->attributes(); 
            foreach ($attributes as $attributeName => $attributeValue) { 
                $attribName = strtolower(trim((string)$attributeName)); 
                $attribVal = trim((string)$attributeValue); 
                $arr[$nextIdx]['@attributes'][$attribName] = $attribVal; 
            } 
            $text = (string)$node; 
            $text = trim($text); 
            if (strlen($text) > 0) { 
                $arr[$nextIdx]['@text'] = $text; 
            } 
            $arr[$nextIdx]['@children'] = array(); 
            $this->convertXmlObjToArr($node, $arr[$nextIdx]['@children']); 
        } 
        
        return; 
    }  		

    private function ReadDataCallBack($arr) {

        $res_arr = array();
        $this->convertXmlObjToArr($arr, $res_arr);	

        while (list($k, $v) = each($res_arr)) {

            $item = $res_arr[$k];

            $sub_item = $item["@children"];
 
            $_skaniainvest = new Skaniainvest(); 	

            while (list($k1, $v1) = each($sub_item)) {

                $arr = $v1["@attributes"];	

                $name = $arr["name"];
 
                $name = strtolower($name);
                
                if($name == "id") {
                    $_skaniainvest->_id = $v1["@text"];
                }
                if($name == "dateupdate") {
                    $_skaniainvest->_dateupdate = $v1["@text"];
                }
                if($name == "price") {
                    $_skaniainvest->_price = $v1["@text"];
                }
                if($name == "profitpermonth") {
                    $_skaniainvest->_profitpermonth = $v1["@text"];
                }
                if($name == "monthaveturn") {
                    $_skaniainvest->_monthaveturn = $v1["@text"];
                }
                if($name == "monthexpemse") {
                    $_skaniainvest->_monthexpemse = $v1["@text"];
                }
                if($name == "timeinvest") { 
                    $_skaniainvest->_timeinvest = utf8_to_cp1251($v1["@text"]);
                }
                if($name == "listobject") {
                    $_skaniainvest->_listobject = utf8_to_cp1251($v1["@text"]);
                }
                if($name == "matproperty") {
                    $_skaniainvest->_matproperty = utf8_to_cp1251($v1["@text"]);
                }
                if($name == "countempl") {
                    $_skaniainvest->_countempl = $v1["@text"];
                }
                if($name == "countmanempl") {
                    $_skaniainvest->_countmanempl = $v1["@text"];
                }
                if($name == "termbiz") {
                    $_skaniainvest->_termbiz = utf8_to_cp1251($v1["@text"]);
                }
                if($name == "sharesale") {
                    $_skaniainvest->_sharesale = $v1["@text"];
                }	
                if($name == "offertype") {
                    $_skaniainvest->_offertype = utf8_to_cp1251($v1["@text"]);
                }
                if($name == "businesstype") {
                    $_skaniainvest->_businesstype = utf8_to_cp1251($v1["@text"]);
                }
                if($name == "phone") {
                    $_skaniainvest->_phone = $v1["@text"];
                }
                if($name == "contact") {
                    $_skaniainvest->_contact = utf8_to_cp1251($v1["@text"]);
                }					
                if($name == "title") {
                    $_skaniainvest->_title = utf8_to_cp1251($v1["@text"]);
                }
                if($name == "description") {
                    $_skaniainvest->_description = utf8_to_cp1251($v1["@text"]);
                }
                if($name == "shortdescription") {
                    $_skaniainvest->_shortdescription = utf8_to_cp1251($v1["@text"]);
                }
                if($name == "web") {
                    $_skaniainvest->_urlBrokerObject = $v1["@text"];
                }
                if($name == "pic") {
                    $_skaniainvest->_pic[] = $v1["@text"];
                }
                if($name == "email") {
                    $_skaniainvest->_email = $v1["@text"];
                }
                if($name == "town") {
                    $_skaniainvest->_town = utf8_to_cp1251($v1["@text"]);
                }
                if($name == "orgform") {
                    $_skaniainvest->_orgform = utf8_to_cp1251($v1["@text"]);
                }
                if($name == "reason") {
                    $_skaniainvest->_reason = utf8_to_cp1251($v1["@text"]);
                }
            }
   
            $this->array_skaniainvest[] = $_skaniainvest;	
        }
    }

    public function ReOpenData() {

        $this->CallData();
        $result = $this->Result();

        $mappingobjectbroker = new MappingObjectBroker();

        $mappingobjectbroker->brokerId = $this->brokerId;

        $a_Ids = array();

        while (list($k, $v) = each($result)) {
            $a_Ids[] = $v->_id;		
        }

        $mappingobjectbroker->arrayBrokerIds =  $a_Ids;
        $mappingobjectbroker->ReOpen();
    }

    private function InsertSell($data) {

        foreach ($data as $k => $v) { 
            $data[$k] = $this->ModerateContent($v);
        }
        
        $sql = " insert into Sell (`FML`, 
                                                                `EmailID`, 
                                                                `PhoneID`,
                                                                `NameBizID`,
                                                                `BizFormID`, 
                                                                `CityID`,
                                                                `subCityID`, 
                                                                `SiteID`,
                                                                `CostID`,
                                                                `cCostID`,
                                                                `txtCostID`,
                                                                `txtProfitPerMonthID`,
                                                                `ProfitPerMonthID`,
                                                                `cProfitPerMonthID`,
                                                                `cMaxProfitPerMonthID`,
                                                                `TimeInvestID`, 
                                                                `ListObjectID`, 
                                                                `ContactID`,
                                                                `MatPropertyID`,
                                                                `txtMonthAveTurnID`,
                                                                `MonthAveTurnID`,
                                                                `cMonthAveTurnID`,
                                                                `cMaxMonthAveTurnID`,
                                                                `txtMonthExpemseID`, 
                                                                `MonthExpemseID`, 
                                                                `cMonthExpemseID`,
                                                                `txtSumDebtsID`,
                                                                `SumDebtsID`,
                                                                `cSumDebtsID`, 
                                                                `txtWageFundID`,
                                                                `WageFundID`,
                                                                `cWageFundID`,
                                                                `ObligationID`,
                                                                `CountEmplID`,
                                                                `CountManEmplID`,
                                                                `TermBizID`,
                                                                `ShortDetailsID`, 
                                                                `DetailsID`, 
                                                                `ImgID`,
                                                                `Img1ID`,
                                                                `Img2ID`,
                                                                `Img3ID`,
                                                                `Img4ID`, 
                                                                `pdf`, 
                                                                `FaxID`, 
                                                                `ShareSaleID`,
                                                                `ReasonSellBizID`,
                                                                `txtReasonSellBizID`,
                                                                `TarifID`, 
                                                                `StatusID`,
                                                                `DataCreate`,
                                                                `DateStart`,
                                                                `TimeActive`,
                                                                `CategoryID`,
                                                                `SubCategoryID`,
                                                                `ConstGUID`,
                                                                `ip`,
                                                                `defaultUserCountry`,
                                                                `login`,
                                                                `password`,
                                                                `newspaper`,
                                                                `helpbroker`,
                                                                `youtubeURL`,
                                                                `brokerId`,
                                                                `source`)
                                                        values
                                                        ( 
                                                        '".$data["FML"]."',
                                                        '".$data["EmailID"]."',
                                                        '".$data["PhoneID"]."',
                                                        '".$data["NameBizID"]."',
                                                        '".$data["BizFormID"]."',
                                                        '".$data["CityID"]."',
                                                        '".$data["SubCityID"]."',
                                                        '".$data["SiteID"]."',
                                                        '".$data["CostID"]."',
                                                        '".$data["cCostID"]."',
                                                        '".$data["txtCostID"]."',
                                                        '".$data["txtProfitPerMonthID"]."',
                                                        '".$data["ProfitPerMonthID"]."',
                                                        '".$data["cProfitPerMonthID"]."',
                                                        '".$data["cProfitPerMonthID"]."',
                                                        '".$data["TimeInvestID"]."', 
                                                        '".$data["ListObjectID"]."',
                                                        '".$data["ContactID"]."',
                                                        '".$data["MatPropertyID"]."',
                                                        '".$data["txtMonthAveTurnID"]."',
                                                        '".$data["MonthAveTurnID"]."',
                                                        '".$data["cMonthAveTurnID"]."',
                                                        '".$data["cMonthAveTurnID"]."',
                                                        '".$data["txtMonthExpemseID"]."',
                                                        '".$data["MonthExpemseID"]."',
                                                        '".$data["cMonthExpemseID"]."',
                                                        '".$data["txtSumDebtsID"]."',
                                                        '".$data["SumDebtsID"]."',
                                                        '".$data["cSumDebtsID"]."',
                                                        '".$data["txtWageFundID"]."',
                                                        '".$data["WageFundID"]."',
                                                        '".$data["cWageFundID"]."',
                                                        '".$data["ObligationID"]."',
                                                        '".$data["CountEmplID"]."',
                                                        '".$data["CountManEmplID"]."',
                                                        '".$data["TermBizID"]."',
                                                        '".$data["ShortDetailsID"]."',
                                                        '".$data["DetailsID"]."',
                                                        '".$data["ImgID"]."',
                                                        '".$data["Img1ID"]."',
                                                        '".$data["Img2ID"]."',
                                                        '".$data["Img3ID"]."',
                                                        '".$data["Img4ID"]."',
                                                        '".(isset($data["pdf"]) ? $data["pdf"] : '')."',
                                                        '".$data["FaxID"]."',
                                                        '".$data["ShareSaleID"]."',
                                                        '".$data["ReasonSellBizID"]."',
                                                        '".$data["txtReasonSellBizID"]."',
                                                        '".$data["TarifID"]."',
                                                        0,
                                                        NOW(),
                                                        NOW(),
                                                        0,
                                                        '".$data["CategoryID"]."',
                                                        '".$data["SubCategoryID"]."',
                                                        '".$data["ConstGUID"]."',
                                                        '".$_SERVER['REMOTE_ADDR']."',
                                                        '".(isset($_SERVER['GEOIP_COUNTRY_NAME']) ? $_SERVER['GEOIP_COUNTRY_NAME'] : '')."',
                                                        '".generate_password(15)."',
                                                        '".generate_password(15)."',
                                                        '".$data["newspaper"]."',
                                                        '".$data["helpbroker"]."',
                                        '".$data["youtubeURL"]."',
                                        '".$data["brokerId"]."',
                                            '".$data["source"]."'
                                                        );";


        $result = $this->db->Insert($sql);

        return $result;
    }
    
    public function ImportData() {

        $this->CallData();
        $result = $this->Result();

        $sell = new Sell();	
        $mappingobjectbroker = new MappingObjectBroker();	

        $iter = 0;
 
        while (list($k, $v) = each($result)) {
 
            if($iter == $this->packetCount) { 
                break; 
            } 
  
            $mappingobjectbroker->brokerObjectID = $v->_id;
            $mappingobjectbroker->brokerId = $this->brokerId;

            if(!$mappingobjectbroker->IsExistsBrokerObject()) {
 
                $mappingobjectbroker->bizzonaObjectId = $this->InsertSell($v->ConvertData());
                $mappingobjectbroker->brokerId = $this->brokerId;
                $mappingobjectbroker->brokerDateObject = HelpAdapter::unixToMySQL($v->_dateupdate);
                $mappingobjectbroker->ObjectTypeId = 1;
                $mappingobjectbroker->urlBrokerObject = $v->_urlBrokerObject;
                $mappingobjectbroker->Insert();			

                if($mappingobjectbroker->bizzonaObjectId > 0) {
                    $this->countProcessed += 1;
                }

                $iter += 1;
            } 
        }
    }

    private function UpdateSell($data) {

        foreach ($data as $k => $v) { 
            $data[$k] = $this->ModerateContent($v);
        }
        
        $where = " limit 1";

        $sql = "update LOW_PRIORITY Sell set  
              `FML`='".trim($data["FML"])."',
              `EmailID`='".trim($data["EmailID"])."',
              `PhoneID`='".trim($data["PhoneID"])."',
              `CostID`='".trim($data["CostID"])."',
              `cCostID`='".trim($data["cCostID"])."',
              `ProfitPerMonthID`='".trim($data["ProfitPerMonthID"])."',
              `cProfitPerMonthID`='".trim($data["cProfitPerMonthID"])."',
              `MonthAveTurnID`='".trim($data["MonthAveTurnID"])."',
              `cMonthAveTurnID`='".trim($data["cMonthAveTurnID"])."',
              `ShortDetailsID`='".trim($data["ShortDetailsID"])."',
              `DetailsID`='".trim($data["DetailsID"])."',
              `ListObjectID`='".trim($data["ListObjectID"])."',    
              `StatusID`=0 
               where ID='".intval(trim($data["ID"]))."' ".$where.";";


        $result = $this->db->Update($sql);

        return $result;
    }		
    
    
    public function UpdateData() {

        $this->CallData();
        $result = $this->Result();

        $sell = new Sell();

        $mappingobjectbroker = new MappingObjectBroker();
        $mappingobjectbroker->brokerId = $this->brokerId;
        $mappingobjectbroker->ObjectTypeId = 1;
        $array_object = $mappingobjectbroker->Select();

        $iter = 0;

        while (list($k, $v) = each($result)) {

            if($iter < 1) {

                while (list($k1, $v1) = each($array_object)) {
                    
                    if($v1["brokerObjectID"] == $v->_id) {

                        $v1_date = /*$v1["brokerDateObject"]; */ strtotime($v1["brokerDateObject"]);
                        $v_date = $v->_dateupdate;//strtotime($v->_dateupdate); 

                        if($v1_date < $v_date) {

                            $data = $v->ConvertData();
                            $data["ID"] = $v1["bizzonaObjectId"]; 

                            $this->UpdateSell($data);
                            
                            $mappingobjectbroker->brokerDateObject = HelpAdapter::unixToMySQL($v->_dateupdate);
                            $mappingobjectbroker->brokerObjectID = $v1["brokerObjectID"];
                            $mappingobjectbroker->UpdateBrokerDateObject(); 

                            $iter += 1;

                            $this->countProcessed += 1;
                        }

                        break;
                    }
                }

            }
            reset($array_object);
        }
    }

    private function UpdateStatusSell($data) {
        $sql = "update LOW_PRIORITY Sell set `StatusID`=".intval(trim($data["StatusID"]))."  where ID=".intval(trim($data["ID"]))." limit 1;";
        $result = $this->db->Update($sql);
        return $result;
    }
    
    public function DeleteData() {

        $sell = new Sell();
        $this->CallData();
        $result = $this->Result();
 
        $mappingobjectbroker = new MappingObjectBroker();
        $mappingobjectbroker->brokerId = $this->brokerId;
        $mappingobjectbroker->ObjectTypeId = 1;
        $array_object = $mappingobjectbroker->Select(); 

        $cache = new Cache_Lite();

        while (list($k, $v) = each($array_object)) {

            $status = false;	
            while (list($k1, $v1) = each($result)) {
                if(intval($v["brokerObjectID"]) == intval($v1->_id)) {
                        $status = true;
                        break;
                }
            }
            reset($result);

            if(!$status) {
                $data = array();
                $data["StatusID"] = 12;  
                $data["ID"] = $v["bizzonaObjectId"];
                $this->UpdateStatusSell($data);
                $this->countProcessed += 1;
                $cache->remove(''.$v["bizzonaObjectId"].'____sellitem');
            }
        }
    }
}
?>
