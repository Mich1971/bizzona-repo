<?php
	include_once("class.db.inc");  	

	class Income {

		private $_db;
		private $_sql;			
		public $_Id;
		public $_typeId;
		public $_codeId;
		public $_summ;
		public $_typePaymentId;
		public $_date;
		public $_externPaymetCodeId;
		
		
		function __construct() {
			global $DATABASE;
			$this->InitDB();
		}

		function InitDB() {
			global $DATABASE;
			$this->db = new DB($DATABASE["HOSTNAME"], $DATABASE["DATABASENAME"], $DATABASE["DATABASEUSER"], $DATABASE["DATABASEPASSWORD"]);
		}

		function __destruct() {
			
		}		

		public function getListAdmin($aParams) {
			
                        $limit = '';
                    
                        if(isset($aParams['limit'])) {
                            $limit = ' limit 10 ';
                        }
                    
			$this->_sql = " select * from Income where 1 order by id desc ".$limit.";";
                        
			$result = $this->db->Select($this->_sql);
		
			$arr = array();	

		
			while ($row = mysql_fetch_object($result)) {
				
				$_a = array(
                                        'isNow' => (date("m/d/y", strtotime($row->date)) == date("m/d/y", time()) ? true : false), 
					'summ' => $row->summ, 
					'date' => $row->date,
					'typeId' => $row->typeId,
					'codeId' => $row->codeId,
					'typePaymentId' => $row->typePaymentId,
				);
				
				$arr[] = $_a;
			}
			
			return $arr;
		}
		
		
		public function getList() {
			
			$this->_sql = " select * from Income where codeId=".(int)$this->_codeId." order by date desc;";
			$result = $this->db->Select($this->_sql);
		
			$arr = array();	

		
			while ($row = mysql_fetch_object($result)) {
				
				$_a = array(
					'summ' => $row->summ, 
					'date' => $row->date,
				);
				
				$arr[] = $_a;
			}
			
			return $arr;
		}
		
		public function getTotalBalance() {
			
			$this->_sql = " select sum(summ) from Income where codeId={$this->_codeId} and typeId='{$this->_typeId}'; ";
			
			//echo $this->_sql.'\n\r';

			$result = $this->db->Select($this->_sql);
			
			$row = mysql_fetch_row($result);
		
			return $row[0];
		}
		
		
		public function AddBalance() {
			
			
			$aParameters = array(
				'`typeId`' 		=> "'".$this->_typeId."'",
				'`codeId`' 		=> "'".$this->_codeId."'",
				'`summ`' 		=> "'".$this->_summ."'",
				'`typePaymentId`' 	=> "'".$this->_typePaymentId."'",
				'`externPaymetCodeId`' 	=> "'".$this->_externPaymetCodeId."'",
				'`date`' 		=> 'NOW()',
			);
			
			
			
			$this->_sql =  "insert into Income (".implode(', ', array_keys($aParameters)).") values  (".implode(', ', array_values($aParameters)).")";
			
			//echo $this->_sql;
			
			$result = $this->db->Insert($this->_sql);

			return $result;		
		}
		
		
	}
?>