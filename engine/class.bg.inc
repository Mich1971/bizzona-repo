<?php
	include_once("interface.sellsdapter.inc");
	include_once("../engine/class.sell.inc");
	include_once("../engine/functions.inc"); 
	include_once("../engine/typographus.php"); 
	include_once("../engine/class.mappingobjectbroker.inc"); 	
	
	function save_image($inPath,$outPath) { 
		
		 $in = fopen($inPath, "rb");     
		 $out =  fopen($outPath, "wb");     
		 while ($chunk = fread($in,8192))     
		 {         
		 	fwrite($out, $chunk, 8192);     
		 }     
		 fclose($in);     
		 fclose($out); 
	} 	
	
	
    class BG implements SellAdapter {
    	
    	public $_id;
    	public $_DateUpdate;
    	public $_Price;
    	public $_ListObject;
    	public $_MatProperty;
    	public $_CountEmpl;
    	public $_CountManEmpl;
    	public $_ShareSale;
    	public $_businesstype;
    	public $_phone = "+359(56) 828686";
    	public $_town;
    	public $_email = "office@bgbusiness.ru";
    	public $_reason;
    	public $_contact = "������ � ��������";
    	public $_title;
    	public $_description;
    	public $_pic1;
    	public $_pic2;
    	public $_pic3;
    	
    	public $brokerId = 38; 
    	public $url = "http://www.bgbusiness.ru/ru/xml/bb";
    	private $array_bg = array();
    	
    	public $town_maps = array(
    		
										"�����" => array("CityID" =>"3107", "SubCityID" => "3260"),
										"������ �������" => array("CityID" =>"3107", "SubCityID" => "3265"),
										"������" => array("CityID" =>"3107", "SubCityID" => "3265"),
										"�������" => array("CityID" =>"3107", "SubCityID" => "3253"),
										"������" => array("CityID" =>"3107", "SubCityID" => "3242"),
										"������" => array("CityID" =>"3107", "SubCityID" => "3243"),
										"�����������" => array("CityID" =>"3107", "SubCityID" => "3241"),
										"������" => array("CityID" =>"3107", "SubCityID" => "3241"),
										"������" => array("CityID" =>"3107", "SubCityID" => "3259"),
										"���������" => array("CityID" =>"3107", "SubCityID" => "3250"),
										"�������" => array("CityID" =>"3107", "SubCityID" => "3244"),
										"�����" => array("CityID" =>"3107", "SubCityID" => "3264"),
										"����" => array("CityID" =>"3107", "SubCityID" => "3255"),
										"������" => array("CityID" =>"3107", "SubCityID" => "3258"),
										"�����" => array("CityID" =>"3107", "SubCityID" => "3256"),
										"�������" => array("CityID" =>"3107", "SubCityID" => "3245")
    		
    								 );
    	public $category_maps = array();
    	
    	function __construct() {
    	}
    	
    	public function Result() {
    		return $this->array_bg;
    	}
    	
		private function ModerateContent($content) {
			return $content;
		}
    	
    	public function ConvertData() {

    		$data = array();
    		
			$data["FML"] =  htmlspecialchars($this->_contact, ENT_QUOTES);
			$data["EmailID"] = htmlspecialchars($this->_email, ENT_QUOTES);
			$data["PhoneID"] = htmlspecialchars($this->_phone, ENT_QUOTES);
			$data["NameBizID"] = htmlspecialchars(ucfirst(strtolower($this->_title)), ENT_QUOTES);
			
			
			if(isset($this->town_maps[$this->town])) {
				$data["CityID"] = $this->town_maps[$this->town]["CityID"];
				$data["SubCityID"] = $this->town_maps[$this->town]["SubCityID"];
			} else {
				$data["CityID"] = "";
				$data["SubCityID"] = "";
			}
			
			
			$data["SiteID"] = htmlspecialchars($this->_town, ENT_QUOTES);
			$data["CostID"] = intval($this->_Price);
			$data["cCostID"] = "eur";
			$data["txtCostID"] = $this->_Price;
			$data["ListObjectID"] = htmlspecialchars(ucfirst(strtolower($this->_ListObject)), ENT_QUOTES); 
			$data["MatPropertyID"] = htmlspecialchars(ucfirst(strtolower($this->_MatProperty)), ENT_QUOTES); 
			$data["CountEmplID"] = $this->_CountEmpl;
			$data["CountManEmplID"] = $this->_CountManEmpl;
			
			$data["ShortDetailsID"] = htmlspecialchars(ucfirst(strtolower($this->_description)), ENT_QUOTES); 
			$data["DetailsID"] = htmlspecialchars(ucfirst(strtolower($this->_description)), ENT_QUOTES); 
			
			
			$data["ShareSaleID"] = intval($this->_ShareSale);
			
			$data["CategoryID"] =  (isset($this->category_maps[$this->_businesstype]) ? $this->category_maps[$this->_businesstype] : "");
			$data["SubCategoryID"] = "";

    		$data["brokerId"] = $this->brokerId;
    		
    		//up Typographus
    		$typo = new Typographus();
    		$data["NameBizID"] = $typo->process($data["NameBizID"]);
    		$data["SiteID"] = $typo->process($data["SiteID"]);
    		$data["ListObjectID"] = $typo->process($data["ListObjectID"]);
    		$data["MatPropertyID"] = $typo->process($data["MatPropertyID"]);
    		$data["ShortDetailsID"] = $typo->process($data["ShortDetailsID"]);
    		$data["DetailsID"] = $typo->process($data["DetailsID"]);
    		//down Typographus    		
    		
    		
			if(strlen(trim($this->_pic1)) > 1) {
				$ext = end(explode('.', $this->_pic1));
				if(strlen($ext) > 0 and  strlen($ext) < 5) {
					$fname_0 =  uniqid("bg_").".".$ext;
					save_image(trim($this->_pic1), "../simg/".$fname_0);
					$data["ImgID"] = $fname_0;
				}
			} else {
				$data["ImgID"] = "";
			}
    		
			if(strlen(trim($this->_pic2)) > 1) {
				$ext = end(explode('.', $this->_pic2));
				if(strlen($ext) > 0 and  strlen($ext) < 5) {
					$fname_0 =  uniqid("bg_").".".$ext;
					save_image(trim($this->_pic2), "../simg/".$fname_0);
					$data["Img1ID"] = $fname_0;
				}
			} else {
				$data["Img1ID"] = "";
			}
    		
			if(strlen(trim($this->_pic3)) > 1) {
				$ext = end(explode('.', $this->_pic3));
				if(strlen($ext) > 0 and  strlen($ext) < 5) {
					$fname_0 =  uniqid("bg_").".".$ext;
					save_image(trim($this->_pic3), "../simg/".$fname_0);
					$data["Img2ID"] = $fname_0;
				}
			} else {
				$data["Img2ID"] = "";
			}
    		
    		
    		return $data;
    	}
    	
    	public function CallData() {
    		
    		$xml = simplexml_load_file($this->url);
    		$this->ReadDataCallBack($xml);
    		
    	}
    	
    	
		private function convertXmlObjToArr($obj, &$arr) 
		{ 
    		$children = $obj->children(); 
    		foreach ($children as $elementName => $node) 
    		{ 
        		$nextIdx = count($arr); 
        		$arr[$nextIdx] = array(); 
        		$arr[$nextIdx]['@name'] = strtolower((string)$elementName); 
        		$arr[$nextIdx]['@attributes'] = array(); 
        		$attributes = $node->attributes(); 
        		foreach ($attributes as $attributeName => $attributeValue) 
        		{ 
            		$attribName = strtolower(trim((string)$attributeName)); 
            		$attribVal = trim((string)$attributeValue); 
            		$arr[$nextIdx]['@attributes'][$attribName] = $attribVal; 
        		} 
        		$text = (string)$node; 
        		$text = trim($text); 
        		if (strlen($text) > 0) 
        		{ 
            		$arr[$nextIdx]['@text'] = $text; 
        		} 
        		$arr[$nextIdx]['@children'] = array(); 
        		$this->convertXmlObjToArr($node, $arr[$nextIdx]['@children']); 
    		} 
    		return; 
		}  		
		
		private function ReadDataCallBack($arr) {

			$res_arr = array();
			$this->convertXmlObjToArr($arr, $res_arr);	
			
			while (list($k, $v) = each($res_arr)) {
				
				$item = $res_arr[$k];
				
				
				$sub_item = $item["@children"];
				
				$_bg = new BG(); 	
				
				while (list($k1, $v1) = each($sub_item)) {

					$arr = $v1["@attributes"];	
					
					$name = $arr["name"];
					
				
					if($name == "id") {
						$_bg->_id = $v1["@text"];
					}
					if($name == "DateUpdate") {
						$_bg->_DateUpdate = $v1["@text"];
					}
					if($name == "Price") {
						$_bg->_Price = $v1["@text"];
					}
					if($name == "ListObject") {
						$_bg->_ListObject = utf8_to_cp1251($v1["@text"]);
					}
					if($name == "MatProperty") {
						$_bg->_MatProperty = utf8_to_cp1251($v1["@text"]);
					}
					if($name == "CountEmpl") {
						$_bg->_CountEmpl = $v1["@text"];
					}
					if($name == "CountManEmpl") {
						$_bg->_CountManEmpl = $v1["@text"];
					}
					if($name == "ShareSale") {
						$_bg->_ShareSale = $v1["@text"];
					}	
					if($name == "offertype") {
						$_bg->_offertype = utf8_to_cp1251($v1["@text"]);
					}
					if($name == "businesstype") {
						$_bg->_businesstype = utf8_to_cp1251($v1["@text"]);
					}
					if($name == "phone") {
						if(strlen(trim($v1["@text"])) > 0) {
							$_bg->_phone = $v1["@text"];
						}
					}
					if($name == "contact") {
						if(strlen(trim($v1["@text"])) > 0) {
							$_bg->_contact = utf8_to_cp1251($v1["@text"]);
						}
					}					
					if($name == "title") {
						$_bg->_title = utf8_to_cp1251($v1["@text"]);
					}
					if($name == "description") {
						$_bg->_description = utf8_to_cp1251($v1["@text"]);
					}
					if($name == "pic1") {
						$_bg->_pic1 = $v1["@text"];
					}
					if($name == "pic2") {
						$_bg->_pic2 = $v1["@text"];
					}					
					if($name == "pic3") {
						$_bg->_pic3 = $v1["@text"];
					}
					if($name == "email") {
						if(strlen(trim($v1["@text"])) > 0) {
							$_bg->_email = $v1["@text"];
						}
					}
					if($name == "town") {
						$_bg->_town = utf8_to_cp1251($v1["@text"]);
					}
					if($name == "reason") {
						$_bg->_reason = utf8_to_cp1251($v1["@text"]);
					}
				}
				
				$this->array_bg[] = $_bg;	

			}
	
		}
    	
    	public function ImportData() {
    		
    		$this->CallData();
    		$result = $this->Result();
    		
    		$sell = new Sell();	
			$mappingobjectbroker = new MappingObjectBroker();	

			$iter = 0;
			
			while (list($k, $v) = each($result)) {
	
					
				if($iter == 1) {
					break;
				}
				
				$mappingobjectbroker->brokerObjectID = $v->_id;
				$mappingobjectbroker->brokerId = $this->brokerId;
				
				if(!$mappingobjectbroker->IsExistsBrokerObject()) {

					$mappingobjectbroker->bizzonaObjectId = $sell->BG_Insert($v->ConvertData());
					$mappingobjectbroker->brokerId = $this->brokerId;
					$mappingobjectbroker->brokerDateObject = HelpAdapter::unixToMySQL(strtotime($v->_DateUpdate));
					$mappingobjectbroker->ObjectTypeId = 1;
					$mappingobjectbroker->Insert();			
					
					$iter += 1;
				} 
							
			}
    	}
    	
    	public function UpdateData() {
    		
    		$this->CallData();
    		$result = $this->Result();
    		
    		$sell = new Sell();
    		
			$mappingobjectbroker = new MappingObjectBroker();
			$mappingobjectbroker->brokerId = $this->brokerId;
			$mappingobjectbroker->ObjectTypeId = 1;
			$array_object = $mappingobjectbroker->Select();
			
			$iter = 0;
			
			
			while (list($k, $v) = each($result)) {
				
				if($iter < 1) {
				
					while (list($k1, $v1) = each($array_object)) {
						if($v1["brokerObjectID"] == $v->_id) {
							
							$v1_date = strtotime($v1["brokerDateObject"]);
							$v_date = strtotime($v->_DateUpdate); 

							if($v1_date < $v_date) {
							
								$data = $v->ConvertData();
								$data["ID"] = $v1["bizzonaObjectId"];
							
								$sell->BG_Update($data);
							
								$mappingobjectbroker->brokerDateObject = HelpAdapter::unixToMySQL(strtotime($v->_DateUpdate));
								$mappingobjectbroker->brokerObjectID = $v1["brokerObjectID"];
								$mappingobjectbroker->UpdateBrokerDateObject(); 
							
								$iter += 1;
							}

							break;
						}
					}
				
				}
				reset($array_object);
			}
    	}
    	
    	public function DeleteData() {
    		
    		$sell = new Sell();
    		$this->CallData();
    		$result = $this->Result();
    		
			$mappingobjectbroker = new MappingObjectBroker();
			$mappingobjectbroker->brokerId = $this->brokerId;
			$mappingobjectbroker->ObjectTypeId = 1;
			$array_object = $mappingobjectbroker->Select(); 
			
			while (list($k, $v) = each($array_object)) {
				$status = false;	
				while (list($k1, $v1) = each($result)) {
					if($v["brokerObjectID"] == $v1->id) {
						$status = true;
						
					}
				}
				reset($result);
				
				if(!$status) {
			        //echo $v->bizzonaObjectId."<hr>";		
					$data = array();
					$data["StatusID"] = 12;
					$data["ID"] = $v["bizzonaObjectId"];
					$sell->BG_UpdateStatusID($data);
				}
				
			}
    	}
    	
    }
	

?>
