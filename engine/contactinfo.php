<?
	ini_set("magic_quotes_gpc","off");

	global $DATABASE;

	include_once("../phpset.inc");
	include_once("./functions.inc");

	AssignDataBaseSetting("../config.ini");
// требуется авторизация
	require_once($_SERVER['DOCUMENT_ROOT'].'/inside/auth.php');
//

	ini_set("display_startup_errors", "off");
	ini_set("display_errors", "off");
	ini_set("register_globals", "off");

	include_once("./class.ContactInfo.inc");
	$contact = new ContactInfo();

	require_once("../libs/Smarty.class.php");
	$smarty = new Smarty;
	$smarty->template_dir = "../templates";
	$smarty->compile_check = false;
	$smarty->caching = false;
	$smarty->compile_dir  = "../templates_c";
        $smarty->cache_dir = "../cache";
	$smarty->debugging = false;
	$smarty->clear_all_cache();

	$contact->GetContactInfo();

	if (isset($_POST['update'])) {

		$contact->textContactInfo = $_POST['textContactInfo'];

		$contact->adsEmail = $_POST['adsEmail'];
		$contact->adsPhone = $_POST['adsPhone'];
		$contact->supportPhone = $_POST['supportPhone'];
		$contact->supportEmail = $_POST['supportEmail'];

		$contact->UpdateContactInfo();

		$contact->GetContactInfo();
	}

	$smarty->assign("contact", $contact);
?>
	<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
    	<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	 	<LINK href="../general.css" type="text/css" rel="stylesheet">
	 	<meta http-equiv="content-type" content="text/html; charset=windows-1251"/>
	 	<body>
<?
        $smarty->display("./mainmenu.tpl");

		$smarty->display("./contactinfo/update.tpl");
?>
		</body>
		</html>
<?
?>