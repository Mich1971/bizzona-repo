<?php
	include_once("class.db.inc");
        require_once('class.bzn_items.inc');          

	define("NEW_COOPBIZ", 0);
	define("OPEN_COOPBIZ", 1);
	define("CLOSE_COOPBIZ",2);
	define("LOOKED_COOPBIZ", 3);
	define("DUBLICATE_COOPBIZ", 15);
	define("DUST_COOPBIZ", 17);
  
  	class CoopBiz {
  	
    	var $db = "";
    	var $id = "";
    	var $name = "";
    	var $shortdescription = "";
    	var $description = "";
    	var $category_id = "";
    	var $subcategory_id = "";
    	var $region_id = "";
    	var $subRegion_id = "";
    	var $region_text = "";
    	var $duration_id = "";
    	var $datecreate = "";
    	var $qview = "";
    	var $status_id = "";
    	var $contact_face = "";
    	var $contact_phone = "";
    	var $contact_email = "";
    	var $contact_site = "";
    	var $statussend_id = "";
    	var $ip = "";
    	var $partnerid = 0;
	
    	function CoopBiz() {
       		global $DATABASE;
       		$this->InitDB();
    	} 

     	function InitDB() {
       		global $DATABASE;
       		$this->db = new DB($DATABASE["HOSTNAME"], $DATABASE["DATABASENAME"], $DATABASE["DATABASEUSER"], $DATABASE["DATABASEPASSWORD"]);
     	}

		function Insert() {

			$sql = " insert into `coopbiz` (
    										`name`,
    										`shortdescription`,
    										`description`,
    										`category_id`,
    										`subcategory_id`,
    										`region_id`,
    										`region_text`,
    										`duration_id`,
    										`datecreate`,
    										`qview`,
    										`status_id`,
    										`contact_face`,
    										`contact_phone`,
    										`contact_email`,
    										`contact_site`,
    										`ip`,
    										`partnerid`
										   )
                                             values
                                 			     	( 
    													'".$this->name."',
    													'".$this->shortdescription."',
    													'".$this->description."',
    													'".$this->category_id."',
    													'".$this->subcategory_id."',
    													'".$this->region_id."',
    													'".$this->region_text."',
    													'".$this->duration_id."',
    													NOW(),
    													'".$this->qview."',
    													'".$this->status_id."',
    													'".$this->contact_face."',
    													'".$this->contact_phone."',
    													'".$this->contact_email."',
    													'".$this->contact_site."',
    													'".$_SERVER['REMOTE_ADDR']."',
    													'".$this->partnerid."'
                               						);";

			$result = $this->db->Insert($sql);
                        
                        $this->BZN_Insert(array(
                            'codeId' => $result,
                            'name' => $this->name,
                            'shortDetailsId' => $this->shortdescription,
                            'description' => $this->description,                                                
                        ));                         
                        
			return $result;
		}

		function Select($data) {
			
       		$limit = "";
       		$where = " where 1 ";
       		$order = " order  by ".$data["sort"]." desc";

       		if ($data["offset"] === 0 && $data["rowCount"] === 0) {
       		} else {
         		$limit = " limit ".intval($data["offset"]).",".$data["rowCount"]."";
       		} 
     	
       		if (isset($data["category_id"])) {
         		$where = $where." and coopbiz.category_id=".intval($data["category_id"])." ";
       		} 

       		if (isset($data["subcategory_id"])) {
         		$where = $where." and coopbiz.subcategory_id=".intval($data["subcategory_id"])." ";
       		} 
       		
       		if (isset($data["region_id"])) {
         		$where = $where." and coopbiz.region_id=".intval($data["region_id"])." ";
       		} 
       		
	   		if (isset($data["subRegion_id"])) {
	   			$where = $where." and subRegion_id =".intval($data["subRegion_id"])." or region_id=".intval($data["subRegion_id"])." ";
	   		}
       		
       
       		if (isset($data["status_id"])) {
         		if ($data["status_id"] == "-1") {
           			$where = $where." and coopbiz.status_id in (1) ";
         		} else {
           			$where = $where." and coopbiz.status_id=".intval($data["status_id"])." ";
         		}
       		} 

     		$sql = "select block_ip.ip as statusblockip, `coopbiz`.*, Category.icon AS 'categoryicon'  from `coopbiz`   LEFT JOIN Category ON ( Category.ID = coopbiz.category_id )  left join block_ip on (block_ip.ip = coopbiz.ip)  ".$where." ".$order." ".$limit;
     	
     		//mail("eugenekurilov@gmail.com", $sql, $sql);
     		
     		$result = $this->db->Select($sql);

       		$array = Array();
       		while ($row = mysql_fetch_object($result)) {
        		$obj = new stdclass();
        		
                        $obj->statusblockip = $row->statusblockip;
    			$obj->id = $row->id;
				$obj->name = $row->name;
				$obj->category_id = $row->category_id;
				$obj->subcategory_id = $row->subcategory_id;
				$obj->region_text = $row->region_text;
				$obj->region_id = $row->region_id;
				$obj->subRegion_id = $row->subRegion_id;
				$obj->categoryicon = $row->categoryicon;
    			$obj->shortdescription = $row->shortdescription;
    			$obj->description = $row->description;
    			$obj->subcategory_id = $row->subcategory_id;
    			$obj->duration_id = $row->duration_id;
    			//$obj->datecreate = $row->datecreate;
    			$obj->datecreate = strftime("%d %B %Y",  strtotime($row->datecreate));
    			$obj->qview = $row->qview;
    			$obj->contact_face = $row->contact_face;
    			$obj->contact_phone = $row->contact_phone;
    			$obj->contact_email = $row->contact_email;
    			$obj->contact_site = $row->contact_site;
        		$obj->status_id = $row->status_id;
        		$obj->statussend_id = $row->statussend_id;
				$obj->pay_summ = $row->pay_summ;
				$obj->pay_status = $row->pay_status;
        		$obj->ip = $row->ip;
        		$obj->partnerid = $row->partnerid;
        		
				if($row->status_id == NEW_COOPBIZ) {
					$obj->status_text = "�����";
				} else if($row->status_id == OPEN_COOPBIZ) {
					$obj->status_text = "������";
				} else if($row->status_id == LOOKED_COOPBIZ) {
					$obj->status_text = "����������";
				} else if($row->status_id == CLOSE_COOPBIZ) {
					$obj->status_text = "������";
				} else if($obj->status_id == DUBLICATE_INVESTMENTS) {
					$obj->status_text = "��������";
				} else if($obj->status_id == DUST_INVESTMENTS) {
					$obj->status_text = "�����";
				}
			
         		$array[] = $obj;
       		}
       
       		return $array;
			
		}
		

		function Update() {
			
			$sql = "update `coopbiz` set  
    									`name`='".$this->name."',
    									`shortdescription`='".$this->shortdescription."',
    									`description`='".$this->description."',
    									`category_id`='".$this->category_id."',
    									`subcategory_id`='".$this->subcategory_id."',
    									`region_id`='".$this->region_id."',
    									`subRegion_id`='".$this->subRegion_id."',
    									`region_text`='".$this->region_text."',
    									`duration_id`='".$this->duration_id."',
    									`qview`='".$this->qview."',
    									`status_id`='".$this->status_id."',
    									`contact_face`='".$this->contact_face."',
    									`contact_phone`='".$this->contact_phone."',
    									`contact_email`='".$this->contact_email."',
    									`contact_site`='".$this->contact_site."'
    												
                               where id='".$this->id."'";

			$result = $this->db->Sql($sql);		
                        
                        $this->BZN_UpdateStatus(array(
                            'codeId' => intval($this->id), 
                            'statusId' => $this->ConvertorStatus($this->status_id), 
                        ));       
                        
                        $this->BZN_Update(array(
                            'codeId' => $this->id, 
                            'name' => $this->name,
                            'shortDetailsId' => $this->shortdescription,
                            'description' => $this->description,                            
                        ));                         
                        
                        
     	}

		function Delete() {
		}

		function GetItem() {
			
			$sql = "Select `coopbiz`.*  from `coopbiz`  where `coopbiz`.`id`=".$this->id.";";
			$result = $this->db->Select($sql);

			$obj = new stdclass();

			while ($row = mysql_fetch_object($result)) {

				$obj->id = $row->id;
				$obj->name = $row->name;
				$obj->shortdescription = $row->shortdescription;
				$obj->description = $row->description;
				$obj->category_id = $row->category_id;
				$obj->subcategory_id = $row->subcategory_id;
				$obj->region_id = $row->region_id;
				$obj->subRegion_id = $row->subRegion_id;
				$obj->region_text = $row->region_text;
				$obj->duration_id = $row->duration_id;
				//$obj->datecreate = $row->datecreate;
				$obj->datecreate = strftime("%d %B %Y",  strtotime($row->datecreate));
				$obj->qview = $row->qview;
				$obj->status_id = $row->status_id;
				$obj->contact_face = $row->contact_face;
				$obj->contact_phone = $row->contact_phone;
				$obj->contact_email = $row->contact_email;
				$obj->contact_site = $row->contact_site;
				$obj->statussend_id = $row->statussend_id;
				$obj->pay_status = $row->pay_status;
				$obj->pay_summ = $row->pay_summ;
			}
			
			return $obj;
		}

		function IncrementQView() {
        	  $sql = "update `coopbiz` set `qview`=`qview`+1 where id=".$this->id.";";
        	  $result = $this->db->Select($sql);

		}
		
		function CountStatus($StatusID) {
			$sql = "select count(coopbiz.id) as 'c' from coopbiz where coopbiz.status_id=".intval($StatusID)."; ";
                        
                        //echo $sql;
                        
			$result = $this->db->Select($sql);
			while ($row = mysql_fetch_object($result)) {
				return $row->c;
			}
			return 0;
		}  
		
		function CountByCategory() {
     		$sql = "select count(`id`) as 'count' from `coopbiz` where 1 and `coopbiz`.`category_id`='".$this->category_id."' and `coopbiz`.`id` != ".$this->id." and `status_id` in (".OPEN_COOPBIZ.");";
     		$result = $this->db->Select($sql);
     		while ($row = mysql_fetch_object($result)) {
     			return $row->count;
     		}
		}
     
		function CountByCity() {
     		$sql = "select count(`id`) as 'count' from `coopbiz` where 1 and `coopbiz`.`region_id`='".$this->region_id."' and `coopbiz`.`id` != ".$this->id." and `status_id` in (".OPEN_COOPBIZ.");";
     		$result = $this->db->Select($sql);
     		while ($row = mysql_fetch_object($result)) {
     			return $row->count;
     		}
		}
     
		function SetSendEmail() {
			$sql = "update `coopbiz` set `statussend_id`=".$this->statussend_id." where id=".$this->id.";";
			$result = $this->db->Select($sql);
		}
		
		
		function SelectSubCategory() {
     	
			$sql = "SELECT count( `coopbiz`.`subcategory_id` ) AS 'count', `SubCategory`.`ID`, `SubCategory`.`name`,  `SubCategory`.`keysellequipment`
					FROM `coopbiz`, `SubCategory` 
						WHERE 1 
							AND `coopbiz`.`category_id` =".$this->category_id."
							AND `SubCategory`.`ID` = `coopbiz`.`subcategory_id` 
							AND `coopbiz`.`status_id` 
							IN ('1')
								GROUP BY `coopbiz`.`subcategory_id` order by `SubCategory`.`ordernum` desc ";     	
     	
			$result = $this->db->Select($sql);

			$array = Array();
			while ($row = mysql_fetch_object($result)) {
				$obj = new stdclass();
				$obj->subcategory_id = $row->ID;
				$obj->Name = $row->name;
				$obj->SeoName = $row->keysellequipment;
				$obj->Count = $row->count;
				$array[] = $obj;
			}
		
			return $array;
		}
		
		function UpdatePhone()
		{
			$this->sql = "update `coopbiz` set contact_phone='".$this->contact_phone."' where id=".$this->id."";
			$result = $this->db->Update($this->sql);
		}
		
		function GetResidence() 
		{
			$sql = "select region_id, subRegion_id from coopbiz where 1 and id=".$this->id."";
			$result = $this->db->Sql($sql);
			$obj = new stdclass();
     	
			while ($row = mysql_fetch_object($result)) {
				$obj->region_id = $row->region_id;
				$obj->subRegion_id = $row->subRegion_id;
			}
			return $obj;
		}
		
		function SetPayment($CodeID, $PayStatus, $PaySumm, $PayDateTime, $PayNumber)
		{
			$sql = "update coopbiz set   
                                 `pay_status`=".$PayStatus.", 
                                 `pay_summ`=".$PaySumm.", 
                                 `pay_datetime`=NOW(),
                                 `pay_smsnumber`=".$PayNumber." 
                         where ID=".$CodeID." ";
			$result = $this->db->Update($sql);
       
			$sql = "insert into payments(summ,datepayment) values ('".$PaySumm."',NOW());";
			$result = $this->db->Update($sql);
		}
		
		function CountPayStatus() {
			
			$sql = "select COUNT(*) as 'c' from coopbiz where coopbiz.status_id in (3,0) and coopbiz.pay_status=1";
                        
                        //echo $sql;
                        
			$result = $this->db->Select($sql);
			while ($row = mysql_fetch_object($result)) {
				return $row->c;
			}
			return 0;
		}
		
		function ListCountPayStatus() {
			$sql = "select ID from coopbiz where coopbiz.status_id in (3,0) and coopbiz.pay_status=1";
			$array = Array();
			$result = $this->db->Select($sql);
			while ($row = mysql_fetch_object($result)) {
				$array[] = $row->ID;
			}
			return $array;
		}	
                
                private function BZN_Update($aParams) {

                    $bzn_items = new BZN_Items();
                    $bzn_items->Update(array(
                        'typeId' => BZN_Items::TYPE_COOPBIZ,
                        'codeId' => $aParams['codeId'],
                        'subTypeId' => BZN_Items::SUPPLIER,
                        'name' => (isset($aParams['name']) ? $aParams['name'] : '' ),
                        'shortDetailsId' => (isset($aParams['shortDetailsId']) ? $aParams['shortDetailsId'] : '' ),
                        'description' => (isset($aParams['description']) ? $aParams['description'] : '' ),    
                        'typeCurrencyId' => (isset($aParams['typeCurrencyId']) ? $aParams['typeCurrencyId'] : '' ),
                        'minCost' => (isset($aParams['minCost']) ? $aParams['minCost'] : '' ),
                        'maxCost' => (isset($aParams['maxCost']) ? $aParams['maxCost'] : '' ),                        
                    ));            

                }                 
                
                private function BZN_Insert($aParams) {

                    $bzn_items = new BZN_Items();
                    $bzn_items->Insert(array(
                        'typeId' => BZN_Items::TYPE_COOPBIZ,
                        'codeId' => $aParams['codeId'],
                        'subTypeId' => BZN_Items::SUPPLIER,
                        'name' => (isset($aParams['name']) ? $aParams['name'] : '' ),
                        'shortDetailsId' => (isset($aParams['shortDetailsId']) ? $aParams['shortDetailsId'] : '' ),
                        'description' => (isset($aParams['description']) ? $aParams['description'] : '' ),    
                        'typeCurrencyId' => (isset($aParams['typeCurrencyId']) ? $aParams['typeCurrencyId'] : '' ),
                        'minCost' => (isset($aParams['minCost']) ? $aParams['minCost'] : '' ),
                        'maxCost' => (isset($aParams['maxCost']) ? $aParams['maxCost'] : '' ),                        
                    ));            

                }    
                
                private function BZN_UpdateStatus($aParams) {

                    $bzn_items = new BZN_Items();
                    $bzn_items->UpdateStatus(array(
                        'typeId' => BZN_Items::TYPE_COOPBIZ,
                        'codeId' => $aParams['codeId'],
                        'statusId' => $aParams['statusId'],
                        'subTypeId' => BZN_Items::SUPPLIER
                    ));               

                }                 
                
                private function ConvertorStatus($statusId) {

                    $aStatus = array(
                        '0' => BZN_Items::STATUS_NEW,
                        '1' => BZN_Items::STATUS_OPEN,
                        '2' => BZN_Items::STATUS_CLOSE,
                        '3' => BZN_Items::STATUS_WAIT,
                        '15' => BZN_Items::STATUS_DUBLICATE,
                        '17' => BZN_Items::STATUS_TRUSH,
                    );

                    return (isset($aStatus[$statusId]) ? $aStatus[$statusId] : '');

                }                 
                
                
		
  }
?>