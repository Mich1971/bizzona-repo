<?php
	include_once("interface.sellsdapter.inc");
	include_once("../engine/class.sell.inc");
	include_once("../engine/functions.inc"); 
	include_once("../engine/typographus.php"); 
	include_once("../engine/class.mappingobjectbroker.inc"); 	
	require_once("../engine/Cache/Lite.php");
	
	function save_image($inPath,$outPath) { 
		
		 $in = fopen($inPath, "rb");     
		 $out =  fopen($outPath, "wb");     
		 while ($chunk = fread($in,8192))     
		 {         
		 	fwrite($out, $chunk, 8192);     
		 }     
		 fclose($in);     
		 fclose($out); 
	} 	
	
	
    class VF2 implements SellAdapter {
    	
    	public $_id;
        public $_DateUpdate;
        public $_Price;
        public $_ProfitPerMonth;
        public $_MonthAveTurn;
        public $_MonthExpemse;
        public $_TimeInvest;
        public $_ListObject;
        public $_MatProperty;
        public $_CountEmpl;
        public $_CountManEmpl;
        public $_TermBiz;
        public $_ShareSale;
        public $_offertype;
        public $_businesstype;
        public $_phone;
        public $_contact;
        public $_title;
        public $_description;
        public $_pic;

        public $_urlBrokerObject;

        // need to add
        public $_email;
        public $_town;
        public $_orgform; 
        public $_reason;
    	
    	public $brokerId = 11; 
    	 
        public $countProcessed = 0;
        
    	public $url = "http://www.vashafirma.ru/adverts/bizzona_export.xml";
    	
    	private $array_vf2 = array();
    	
    	
    	public $orgform_maps = array 	(
    										"���" => "1",
    										"���������������" => "8"
    								 	);
    	
    	public $category_maps = array (
    											"�����������, ����������, ���, ���������" => "90",
    											"������, ������������, ���.������" => "105",
    											"���������, ���� ������" => "93",
    											"�������� ��������, ��������-�������" => "114",
    											"������������ ������������" => "94",
    											"��������, ���������, �������� �������" => "115",
    											"������������ � �������������" => "94",
    											"������� ��������" => "89",
    											"����������� ����� �����" => "117",
    											"������������, ������" => "82",
    											"���������, ����, ����, �����" => "92",
    											"������ �������, ������" => "109",
    											"�������� ���������" => "78",
    											"�������������" => "87",
    											"������������ ��������" => "97",
    											"������������, ������������" => "96",
    											"���������� ������" => "119",
    											"������ �� �������" => "",
    											"������" => ""
    										 );

		public $town_maps = array 	(
		
										"�����" => array("cityId" => "69", "subCityId" => "2048"),
										"�������" => array("cityId" => "40", "subCityId" => "662"),
										"������" => array("cityId" => "77", "subCityId" => ""),
										"�������" => array("cityId" => "50", "subCityId" => "3699"),
										"�������" => array("cityId" => "50", "subCityId" => "1187"),
										"���" => array("cityId" => "2", "subCityId" => "304"),
										"�������" => array("cityId" => "50", "subCityId" => "1202"),
										"��������" => array("cityId" => "50", "subCityId" => "1191"),
										"���������" => array("cityId" => "50", "subCityId" => "1192"),
										"��������" => array("cityId" => "50", "subCityId" => ""),
										"�����-���������" => array("cityId" => "78", "subCityId" => ""),
										"�����������" => array("cityId" => "50", "subCityId" => "1168"),
										"���������������" => array("cityId" => "50", "subCityId" => "3700"),
										"������������" => array("cityId" => "50", "subCityId" => ""),
										"�����" => array("cityId" => "50", "subCityId" => ""),
										"��������" => array("cityId" => "50", "subCityId" => "1188"),
										"�������" => array("cityId" => "50", "subCityId" => "1179"),
										"�������" => array("cityId" => "50", "subCityId" => "1184"),
										"���������" => array("cityId" => "50", "subCityId" => "1164"),
										"�����" => array("cityId" => "50", "subCityId" => "1199"),
										"������" => array("cityId" => "50", "subCityId" => "3698"),
										"�������" => array("cityId" => "50", "subCityId" => "1197"),
										"������" => array("cityId" => "50", "subCityId" => "1201"),
										"��������" => array("cityId" => "50", "subCityId" => "1166"),
										"������" => array("cityId" => "50", "subCityId" => "1176"),
										"����-�������" => array("cityId" => "50", "subCityId" => "1186"),
										"������������" => array("cityId" => "50", "subCityId" => "1203"),
										"�������" => array("cityId" => "50", "subCityId" => "1170"),
										"��������" => array("cityId" => "50", "subCityId" => "1177"),
										"�����" => array("cityId" => "50", "subCityId" => "1182"),
										"�������" => array("cityId" => "50", "subCityId" => "1187"),
										"�������-�����" => array("cityId" => "50", "subCityId" => "1193"),
										"���������" => array("cityId" => "50", "subCityId" => "1173"),
										"�������" => array("cityId" => "50", "subCityId" => "1202"),
										"����������" => array("cityId" => "50", "subCityId" => "1171"),
										"����" => array("cityId" => "50", "subCityId" => "1178"),
										"��������" => array("cityId" => "50", "subCityId" => "1183"),
										"��������" => array("cityId" => "50", "subCityId" => "1188"),
										"��������" => array("cityId" => "50", "subCityId" => "1194"),
										"�����������" => array("cityId" => "50", "subCityId" => "1167"),
										"�����" => array("cityId" => "50", "subCityId" => "1172"),
										"�������" => array("cityId" => "50", "subCityId" => "1179"),
										"�������" => array("cityId" => "50", "subCityId" => "1185"),
										"�������-�����" => array("cityId" => "50", "subCityId" => "1189"),
										"�������������" => array("cityId" => "50", "subCityId" => "1195"),
										"�����������" => array("cityId" => "50", "subCityId" => "1168")
									
									);
    										 
    	
    	function __construct() {
    		
    	}
    	
    	public function Result() {
    		
    		return $this->array_vf2;
    		
    	}
    	
    	
		private function ModerateContent($content) {
			
			return $content;
			
		}
    	
    	public function ConvertData($dataCompany = null) {

    		$data = array();
    		
			$data["FML"] =  htmlspecialchars($this->_contact, ENT_QUOTES);
			$data["EmailID"] = htmlspecialchars($this->_email, ENT_QUOTES);
			$data["PhoneID"] = htmlspecialchars($this->_phone, ENT_QUOTES);
			$data["NameBizID"] = htmlspecialchars(ucfirst(strtolower($this->_title)), ENT_QUOTES);
			
			
                        if(strlen(trim($data["EmailID"])) <= 0) {
                           if(!is_null($dataCompany) && isset($dataCompany['email'])) {
                               $data["EmailID"] = $dataCompany['email'];
                           }
                        }

                        if(strlen(trim($data["PhoneID"])) <= 0) {
                           if(!is_null($dataCompany) && isset($dataCompany['email'])) {
                               $data["PhoneID"] = $dataCompany['phone'];  
                           }
                        }

			if(isset($this->orgform_maps[$this->_orgform])) {
				$data["BizFormID"] = $this->orgform_maps[$this->_orgform];
			} else {
				$data["BizFormID"] = "";
			}
			
			/*
			if(isset($this->) {
				$data["BizFormID"] = "";
			} else {
				$data["BizFormID"] = "";
			}
			*/
			
			if(isset($this->town_maps[$this->town])) {
				$data["CityID"] = $this->town_maps[$this->town]["cityId"];
				$data["SubCityID"] = $this->town_maps[$this->town]["subCityId"];
			} else {
				$data["CityID"] = "";
				$data["SubCityID"] = "";
			}
			
			
			$data["SiteID"] = htmlspecialchars($this->_town, ENT_QUOTES);
			$data["CostID"] = intval($this->_Price);
			$data["cCostID"] = "rub";
			$data["txtCostID"] = $this->_Price;
			$data["txtProfitPerMonthID"] = "";
			$data["ProfitPerMonthID"] = $this->_ProfitPerMonth;
			$data["cProfitPerMonthID"] = "rub";
			$data["cMaxProfitPerMonthID"] = "";
			$data["TimeInvestID"] = htmlspecialchars(ucfirst(strtolower($this->_TimeInvest)), ENT_QUOTES);
			$data["ListObjectID"] = htmlspecialchars(ucfirst(strtolower($this->_ListObject)), ENT_QUOTES); 
			$data["ContactID"] = "";//$this->_contact;
			$data["MatPropertyID"] = htmlspecialchars(ucfirst(strtolower($this->_MatProperty)), ENT_QUOTES); 
			$data["txtMonthAveTurnID"] = "rub";
			$data["MonthAveTurnID"] = $this->_MonthAveTurn;
			$data["cMonthAveTurnID"] = "rub";
			$data["cMaxMonthAveTurnID"] = "";
			$data["txtMonthExpemseID"] = "";
			$data["MonthExpemseID"] = $this->_MonthExpemse;
			$data["cMonthExpemseID"] = "rub";
			$data["txtSumDebtsID"] = "";
			$data["SumDebtsID"] = "";
			$data["cSumDebtsID"] = "";
			$data["txtWageFundID"] = "";
			$data["WageFundID"] = "";
			$data["cWageFundID"] = "";
			$data["ObligationID"] = "";
			$data["CountEmplID"] = $this->_CountEmpl;
			$data["CountManEmplID"] = $this->_CountManEmpl;
			$data["TermBizID"] = $this->_TermBiz;
			
			
			$data["ShortDetailsID"] = htmlspecialchars(ucfirst(strtolower($this->_description)), ENT_QUOTES); 
			$data["DetailsID"] = htmlspecialchars(ucfirst(strtolower($this->_description)), ENT_QUOTES); 
			
			
			$data["FaxID"] = ""; 
			$data["ShareSaleID"] = "";
			$data["ReasonSellBizID"] = "";
			$data["TarifID"] = ""; 
			$data["StatusID"] = "";
			$data["DataCreate"] = "";
			$data["DateStart"] = "";
			$data["TimeActive"] = "";
			
			
			$data["CategoryID"] =  (isset($this->category_maps[$this->_businesstype]) ? $this->category_maps[$this->_businesstype] : "");
			$data["SubCategoryID"] = "";
			$data["ConstGUID"] = "";
			$data["ip"] = "";
			$data["defaultUserCountry"] = "";
			$data["login"] = "";
			$data["password"] = "";
			$data["newspaper"] = "";
			$data["helpbroker"] = "";
			$data["youtubeURL"] = "";    		
    		$data["brokerId"] = $this->brokerId;
    		$data["txtReasonSellBizID"] = htmlspecialchars($this->_reason, ENT_QUOTES);
    		
    		
    		//up Typographus
    		
    		$typo = new Typographus();
    		
    		$data["NameBizID"] = $typo->process($data["NameBizID"]);
    		$data["SiteID"] = $typo->process($data["SiteID"]);
    		$data["ListObjectID"] = $typo->process($data["ListObjectID"]);
    		$data["MatPropertyID"] = $typo->process($data["MatPropertyID"]);
    		$data["ShortDetailsID"] = $typo->process($data["ShortDetailsID"]);
    		$data["DetailsID"] = $typo->process($data["DetailsID"]);
    		
    		//down Typographus    		
    		
    		return $data;
    	}
    	
    	public function CallData() {
    		
    		$xml = simplexml_load_file($this->url);
    		$this->ReadDataCallBack($xml);
    		
    	}
    	
    	
		private function convertXmlObjToArr($obj, &$arr) 
		{ 
			
			if(!is_object($obj)) throw new Exception("Bizzona.ru: ������ ������� ������ xml ����� �� �������� ���� �����");
			
    		$children = $obj->children(); 
    		foreach ($children as $elementName => $node) 
    		{ 
        		$nextIdx = count($arr); 
        		$arr[$nextIdx] = array(); 
        		$arr[$nextIdx]['@name'] = strtolower((string)$elementName); 
        		$arr[$nextIdx]['@attributes'] = array(); 
        		$attributes = $node->attributes(); 
        		foreach ($attributes as $attributeName => $attributeValue) 
        		{ 
            		$attribName = strtolower(trim((string)$attributeName)); 
            		$attribVal = trim((string)$attributeValue); 
            		$arr[$nextIdx]['@attributes'][$attribName] = $attribVal; 
        		} 
        		$text = (string)$node; 
        		$text = trim($text); 
        		if (strlen($text) > 0) 
        		{ 
            		$arr[$nextIdx]['@text'] = $text; 
        		} 
        		$arr[$nextIdx]['@children'] = array(); 
        		$this->convertXmlObjToArr($node, $arr[$nextIdx]['@children']); 
    		} 
    		return; 
		}  		
		
		private function ReadDataCallBack($arr) {

			$res_arr = array();
			$this->convertXmlObjToArr($arr, $res_arr);	
			
			while (list($k, $v) = each($res_arr)) {
				
				$item = $res_arr[$k];
				
				
				$sub_item = $item["@children"];
				
				$_vf2 = new VF2(); 	
				
				while (list($k1, $v1) = each($sub_item)) {

					$arr = $v1["@attributes"];	
					
					$name = $arr["name"];
					
					if($name == "id") {
						$_vf2->_id = $v1["@text"];
					}
					if($name == "DateUpdate") {
						$_vf2->_DateUpdate = $v1["@text"];
					}
					if($name == "Price") {
						$_vf2->_Price = $v1["@text"];
					}
					if($name == "ProfitPerMonth") {
						$_vf2->_ProfitPerMonth = $v1["@text"];
					}
					if($name == "MonthAveTurn") {
						$_vf2->_MonthAveTurn = $v1["@text"];
					}
					if($name == "MonthExpemse") {
						$_vf2->_MonthExpemse = $v1["@text"];
					}
					if($name == "TimeInvest") {
						$_vf2->_TimeInvest = $v1["@text"];
					}
					if($name == "ListObject") {
						$_vf2->_ListObject = utf8_to_cp1251($v1["@text"]);
					}
					if($name == "MatProperty") {
						$_vf2->_MatProperty = utf8_to_cp1251($v1["@text"]);
					}
					if($name == "CountEmpl") {
						$_vf2->_CountEmpl = $v1["@text"];
					}
					if($name == "CountManEmpl") {
						$_vf2->_CountManEmpl = $v1["@text"];
					}
					if($name == "TermBiz") {
						$_vf2->_TermBiz = $v1["@text"];
					}
					if($name == "ShareSale") {
						$_vf2->_ShareSale = $v1["@text"];
					}	
					if($name == "offertype") {
						$_vf2->_offertype = utf8_to_cp1251($v1["@text"]);
					}
					if($name == "businesstype") {
						$_vf2->_businesstype = utf8_to_cp1251($v1["@text"]);
					}
					if($name == "phone") {
						$_vf2->_phone = $v1["@text"];
					}
					if($name == "contact") {
						$_vf2->_contact = utf8_to_cp1251($v1["@text"]);
					}					
					if($name == "title") {
						$_vf2->_title = utf8_to_cp1251($v1["@text"]);
					}
					if($name == "description") {
						$_vf2->_description = utf8_to_cp1251($v1["@text"]);
					}
					if($name == "web") {
						$_vf2->_urlBrokerObject = $v1["@text"];
					}
					if($name == "pic") {
						$_vf2->_pic = $v1["@text"];
					}
					if($name == "email") {
						$_vf2->_email = $v1["@text"];
					}
					if($name == "town") {
						$_vf2->_town = utf8_to_cp1251($v1["@text"]);
					}
					if($name == "orgform") {
						$_vf2->_orgform = utf8_to_cp1251($v1["@text"]);
					}
					if($name == "reason") {
						$_vf2->_reason = utf8_to_cp1251($v1["@text"]);
					}
				}
				
				$this->array_vf2[] = $_vf2;	

			}
	
		}
    	
		public function ReOpenData() {

                    $this->CallData();
                    $result = $this->Result();

                    //var_dump($result);


                    $mappingobjectbroker = new MappingObjectBroker();

                    $mappingobjectbroker->brokerId = $this->brokerId;


                    $a_Ids = array();

                    while (list($k, $v) = each($result)) {
                            $a_Ids[] = $v->_id;		
                    } 

                    //var_dump($a_Ids);

                    $mappingobjectbroker->arrayBrokerIds =  $a_Ids;

                    $mappingobjectbroker->ReOpen();
    		
		}
		
		
    	public function ImportData($dataCompany = null) {
    		
    		$this->CallData();
    		$result = $this->Result();
    		
			    		
    		$sell = new Sell();	
			$mappingobjectbroker = new MappingObjectBroker();	

			$iter = 0;
			
			while (list($k, $v) = each($result)) {

				//if($v->_id != 25427) continue;
					
				if($iter == 3) {
					break;
				}
				
				$mappingobjectbroker->brokerObjectID = $v->_id;
				$mappingobjectbroker->brokerId = $this->brokerId;
				
				if(!$mappingobjectbroker->IsExistsBrokerObject()) {

							
					$mappingobjectbroker->bizzonaObjectId = $sell->VF2_Insert($v->ConvertData($dataCompany));
					$mappingobjectbroker->brokerId = $this->brokerId;
					$mappingobjectbroker->brokerDateObject = HelpAdapter::unixToMySQL(strtotime($v->_DateUpdate));
					$mappingobjectbroker->ObjectTypeId = 1;
					$mappingobjectbroker->urlBrokerObject = $v->_urlBrokerObject;
					$mappingobjectbroker->Insert();			
					
                                        if($mappingobjectbroker->bizzonaObjectId > 0) {
                                            $this->countProcessed += 1;
                                        }
                                        
					$iter += 1;
				} 
							
			}
			
    	}
    	
    	public function UpdateData() {
    		
    		$this->CallData();
    		$result = $this->Result();
    		
    		$sell = new Sell();
    		
			$mappingobjectbroker = new MappingObjectBroker();
			$mappingobjectbroker->brokerId = $this->brokerId;
			$mappingobjectbroker->ObjectTypeId = 1;
			$array_object = $mappingobjectbroker->Select();
			
			
			$iter = 0;
			
			
			while (list($k, $v) = each($result)) {
				
				if($iter < 1) {
				
					while (list($k1, $v1) = each($array_object)) {
						if($v1["brokerObjectID"] == $v->_id) {
							
							$v1_date = strtotime($v1["brokerDateObject"]);
							$v_date = strtotime($v->_DateUpdate); 

							if($v1_date < $v_date) {
							
								$data = $v->ConvertData();
								$data["ID"] = $v1["bizzonaObjectId"];
							
								$sell->VF2_Update($data);
							
								$mappingobjectbroker->brokerDateObject = HelpAdapter::unixToMySQL(strtotime($v->_DateUpdate));
								$mappingobjectbroker->brokerObjectID = $v1["brokerObjectID"];
								$mappingobjectbroker->UpdateBrokerDateObject(); 
			 				
								$iter += 1;
                                                                
                                                                $this->countProcessed += 1;
							}

							break;
						}
					}
				
				}
				reset($array_object);
			}
			
    	}
    	
    	public function DeleteData() {
    		
    		$sell = new Sell();
    		$this->CallData();
    		$result = $this->Result();
    		
    		
			$mappingobjectbroker = new MappingObjectBroker();
			$mappingobjectbroker->brokerId = $this->brokerId;
			$mappingobjectbroker->ObjectTypeId = 1;
			$array_object = $mappingobjectbroker->Select(); 
			
			//echo "sizeof: ".sizeof($array_object)."<hr>";
			
			$cache = new Cache_Lite();
			
			while (list($k, $v) = each($array_object)) {
				
				if($v["brokerObjectID"] > 100000) continue;
				
				$status = false;	
				while (list($k1, $v1) = each($result)) {
					if(intval($v["brokerObjectID"]) == intval($v1->_id)) {
						$status = true;
						break;
					}
				}
				reset($result);
				
				if(!$status) {
			        //echo "bizzona: ".$v["bizzonaObjectId"]." => vashafirma: ".$v["brokerObjectID"]."<hr>";		
					//mail("eugenekurilov@gmail.com", "bizzona: ".$v["bizzonaObjectId"]." => vashafirma: ".$v["brokerObjectID"], "bizzona: ".$v["bizzonaObjectId"]." => vashafirma: ".$v["brokerObjectID"]);
					$data = array();
					$data["StatusID"] = 12;  
					$data["ID"] = $v["bizzonaObjectId"];
					$sell->VF2_UpdateStatusID($data);
                                        $this->countProcessed += 1;
					$cache->remove(''.$v["bizzonaObjectId"].'____sellitem');
					
				}
			}
    	}
    	
    }
	

?>
