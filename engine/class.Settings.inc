<?php
    include_once("class.db.inc");  	
    include_once("class.MCSingleton.inc");
    
    class Settings {
    
        private $_db;
        private $_sql;			
        public $_id;
        public $_name;
        public $_value;
        public $_description;
                
        public $timecache = 2592000;        
        
        function __construct() {
                global $DATABASE;
                $this->InitDB();
        }

        function InitDB() {
                global $DATABASE;
                $this->db = new DB($DATABASE["HOSTNAME"], $DATABASE["DATABASENAME"], $DATABASE["DATABASEUSER"], $DATABASE["DATABASEPASSWORD"]);
        }

        function __destruct() {

        }		

        public function GetValue($key) {

            $mc = MCSingleton::getInstance();
            
            if($res = $mc->connection->get("setting_{$key}"))  {
                
                return $_aResult = unserialize($res);
                
            } else {
                
                $this->_sql = " select value from Settings where name='{$key}';";
                $result = $this->db->Select($this->_sql);
                while ($row = mysql_fetch_object($result)) {
                    $value = $row->value;
                }
                
                $mc->connection->set("setting_{$key}", serialize($value), 0, $this->timecache);
                
                return $value;
            
            }
        }
    
        public function SetValue($key, $value) {
            
            $this->_sql = "update Settings set value='{$value}' where name='{$key}' limit 1;";
            $this->db->Sql($this->_sql);
        }
        
        public function GetItem($id) {
            
            $this->_sql = " select * from Settings where id=".(int)$id." limit 1;";
            $result = $this->db->Select($this->_sql);
            
            $ar = array();
             
            while ($row = mysql_fetch_object($result)) {
               $ar['id'] = $row->id; 
               $ar['name'] = $row->name; 
               $ar['value'] = $row->value; 
               $ar['description'] = $row->description; 
            }
            
            return $ar;
        }
        
        public function Select() {
            
            $this->_sql = " select * from Settings;";
            $result = $this->db->Select($this->_sql);
            
            $ar = array();
            
            while ($row = mysql_fetch_object($result)) {
                
                $ar[] = array(
                    'name' => $row->name, 
                    'value' => $row->value,
                    'id' => $row->id,
                    'description' => $row->description, 
                );
            }
            
            return $ar; 
        }
    
        
}    
    
    
    
?>
