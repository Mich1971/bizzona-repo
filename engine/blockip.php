<?
    ini_set("magic_quotes_gpc","off");

    global $DATABASE;

    include_once("../phpset.inc");
    include_once("./functions.inc");

    AssignDataBaseSetting("../config.ini");
// требуется авторизация
	require_once($_SERVER['DOCUMENT_ROOT'].'/inside/auth.php');
//

    ini_set("display_startup_errors", "on");
    ini_set("display_errors", "on");
    ini_set("register_globals", "on");

    include_once("./class.BlockIP.inc");
    $blockIp = new BlockIP();

    require_once("../libs/Smarty.class.php");
    $smarty = new Smarty;
    $smarty->template_dir = "../templates";
    $smarty->compile_check = false;
    $smarty->caching = false;
    $smarty->compile_dir  = "../templates_c";
    $smarty->cache_dir = "../cache";
    $smarty->debugging = false;
    $smarty->clear_all_cache();


    if(isset($_GET['blockip']) == 'up') {

        $blockIp->BlockUp(array(
            'ip' => trim($_GET['ip'])
        ));

    } else if (isset($_GET['blockip']) == 'down') {

        $blockIp->BlockDown(array(
            'ip' => trim($_GET['ip'])
        ));

    }


    $list = $blockIp->Select();

    $smarty->assign("list", $list);

?>
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
            <LINK href="../general.css" type="text/css" rel="stylesheet">
            <meta http-equiv="content-type" content="text/html; charset=windows-1251"/>
            <body>
<?
                $smarty->display("./mainmenu.tpl");
		$smarty->display("./blockip/list.tpl");
?>
            </body>
        </html>
<?
?>