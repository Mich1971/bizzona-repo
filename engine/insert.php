<?php

    global $DATABASE;

    include_once("../phpset.inc");
    include_once("./functions.inc"); 

    AssignDataBaseSetting("../config.ini");
// требуется авторизация
	require_once($_SERVER['DOCUMENT_ROOT'].'/inside/auth.php');
//

    ini_set("display_startup_errors", "on");
    ini_set("display_errors", "on");
    ini_set("register_globals", "on");

    include_once("./class.insert_settings.inc"); 
    $insertSettings = new InsertSettings();

    require_once("../libs/Smarty.class.php");
    $smarty = new Smarty;
    $smarty->template_dir = "../templates";
    $smarty->compile_check = false;
    $smarty->caching = false;
    $smarty->compile_dir  = "../templates_c";
    $smarty->cache_dir = "../cache";
    $smarty->debugging = false;
    $smarty->clear_all_cache();

    
    if(isset($_POST) && count($_POST)) {
         
        $insertSettings->type_insert = (int)$_POST['type_insert'];
        $insertSettings->type_show = (int)$_POST['type_show'];
        $insertSettings->pay = trim($_POST['pay']);
        $insertSettings->no_pay = trim($_POST['no_pay']);
        $insertSettings->promo = trim($_POST['promo']); 
        $insertSettings->use_promo = (isset($_POST['use_promo']) ? 1 : 0);
        
        $insertSettings->Update();
    }
    
    
    $smarty->assign("list", $insertSettings->Select());
    $smarty->assign("listtypeshow", $insertSettings->ListTypeShow());
?>
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
            <LINK href="../general.css" type="text/css" rel="stylesheet">
            <meta http-equiv="content-type" content="text/html; charset=windows-1251"/> 
            <body>
<?
                $smarty->display("./mainmenu.tpl");
		$smarty->display("./insertsettings/list.tpl");
?>
            </body>
        </html>
<?
?>

