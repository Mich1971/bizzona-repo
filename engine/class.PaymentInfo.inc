<?
	include_once("class.dbi.inc");

	class PaymentInfo {
		
		public $_Id;
		public $_Name;
		public $_Description;
		public $_Value;
		public $_sql;
		
		
		public function __construct() {
			DBi::getInstance();
		}
		
		public function ListPayments() {
			
			$result = DBi::query('select * from PaymentInfo;');

			$arr = array();
			
			while($obj=DBi::fetch_object($result)){
				
				$arr[] = array('Id' => $obj->Id, 'Name' => $obj->Name, 'Description' => $obj->Description, 'Value' => $obj->Value);
			}
			
			return $arr;
		}
		
		public function Update() {
			
			$this->_sql = "update PaymentInfo set Value='{$this->_Value}' where Id={$this->_Id} limit 1;";
			DBi::query($this->_sql);
		}
		
	}

?>