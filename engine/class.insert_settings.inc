<?php

    include_once("class.db.inc");  
    include_once("class.ContactInfo_Lite.inc");
     
    interface ConstInsertSettings {
        
        const SELL = 1; 
        const BUYER = 2;
        const EQUIPMENT = 3;
        const INVESTMENTS = 4;
        const COOPBIZ = 5;
        const ROOM = 6;
        const ARENDA_SELL = 7;
        
        const TYPE_SHOW_PAY = 1;
        const TYPE_SHOW_NO_PAY = 2;
        const TYPE_SHOW_PROMO = 3;
    }
    
    class InsertSettings {
    
	private $_db;
	private $_sql;			
        private $aTypeTnsert = array(
            ConstInsertSettings::SELL => '������� �������',
            ConstInsertSettings::BUYER => '������� �������',
            ConstInsertSettings::EQUIPMENT => '������� ������������',
            ConstInsertSettings::ROOM => '������� ���������',
            ConstInsertSettings::COOPBIZ => '���������� ������',
            ConstInsertSettings::INVESTMENTS => '��������������',
            ConstInsertSettings::ARENDA_SELL => '������ �������',
        );
        
        public $type_insert = 0;
        public $type_show = 0;
        public $pay = '';
        public $no_pay = '';
        public $promo = '';
        public $use_promo = 0;
        
	function __construct() {
		global $DATABASE;
		$this->InitDB();
	}

	function InitDB() {
		global $DATABASE;
		$this->db = new DB($DATABASE["HOSTNAME"], $DATABASE["DATABASENAME"], $DATABASE["DATABASEUSER"], $DATABASE["DATABASEPASSWORD"]);
	}

	function __destruct() {
			
	}        
        
        function GetItem() {

            $contact = new ContactInfo_Lite();  
            $contact->GetContactInfo();  
            
            $this->_sql = " select * from insert_settings where `type_insert`=".$this->type_insert.";";
            $result = $this->db->Select($this->_sql);
            
            $arr = array();	
 
            while ($row = mysql_fetch_object($result)) {

                $row->pay = str_replace("{{PHONE}}", $contact->supportPhone, $row->pay);
                $row->no_pay = str_replace("{{PHONE}}", $contact->supportPhone, $row->no_pay);
                $row->promo = str_replace("{{PHONE}}", $contact->supportPhone, $row->promo);
                
                $arr['type_insert'] = $row->type_insert;
                $arr['type_insert_text'] = $this->aTypeTnsert[$row->type_insert];
                $arr['type_show'] = $row->type_show;
                $arr['pay'] = $row->pay;
                $arr['no_pay'] = $row->no_pay;
                $arr['promo'] = $row->promo;
                $arr['use_promo'] = $row->use_promo;
                
                if($arr['type_show'] == ConstInsertSettings::TYPE_SHOW_PAY) {
                    
                    $arr['content'] = $arr['pay']; 
                    
                } else if ($arr['type_show'] == ConstInsertSettings::TYPE_SHOW_NO_PAY) {
                    
                    $arr['content'] = $arr['no_pay'];
                    
                } else if ($arr['type_show'] == ConstInsertSettings::TYPE_SHOW_PROMO) {
                    
                    $arr['content'] = $arr['promo'];
                    
                } else {
                    
                    $arr['content'] = '';
                    
                }
                
            }
            
            return $arr;
        }
        
        function Select() {

            $this->_sql = " select * from insert_settings;";
            $result = $this->db->Select($this->_sql);
            
            $arr = array();	

            while ($row = mysql_fetch_object($result)) {

                $arr[] = array(
                    'type_insert' => $row->type_insert,
                    'type_insert_text' => $this->aTypeTnsert[$row->type_insert],
                    'type_show' => $row->type_show,
                    'pay' => $row->pay,
                    'no_pay' => $row->no_pay,
                    'promo' => $row->promo,
                    'use_promo' => $row->use_promo,
                );
                
            }
            
            return $arr;
        }
        
        function Update() {
            
            $this->parseData();
            
            $this->_sql = "update insert_settings set `type_show`=".$this->type_show.", `pay` = '".$this->pay."', `no_pay`='".$this->no_pay."', `promo`='".$this->promo."', `use_promo`=".$this->use_promo." where `type_insert`=".$this->type_insert." limit 1;";
            $this->db->Sql($this->_sql);	
        }
        
        function ListTypeShow() {
            
            $aResult = array(
                
                ConstInsertSettings::TYPE_SHOW_PAY => '������� ����������',
                ConstInsertSettings::TYPE_SHOW_NO_PAY => '��������� ����������',
                //ConstInsertSettings::TYPE_SHOW_PROMO => '����� ����������',
                
            );
            
            return $aResult;
        }
        
        function parseData() {
            
            $this->pay = mysql_real_escape_string($this->pay);
            $this->promo = mysql_real_escape_string($this->promo);
            $this->no_pay = mysql_real_escape_string($this->no_pay);
            
        }
        
    }