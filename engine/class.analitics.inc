<?
include_once("class.db.inc");  	

class Analitics {

	private $_db;
	private $_sql;			
 
	function __construct() {
		global $DATABASE;
		$this->InitDB();
	}

	function InitDB() {
		global $DATABASE;
		$this->db = new DB($DATABASE["HOSTNAME"], $DATABASE["DATABASENAME"], $DATABASE["DATABASEUSER"], $DATABASE["DATABASEPASSWORD"]);
	}

	function __destruct() {
			
	}		
	
	
	public function StatRegionPayment() {
		
		$this->_sql = 'select CityID, region.title as "name", count(CityID) as  "count" from  Sell, region  where 1 and YEAR(NOW())=YEAR(date) and Sell.CityID = region.ID and  pay_summ > 1  and (brokerId is null or brokerId <= 0) group by CityID order by count desc limit 15;';
		$result = $this->db->Select($this->_sql);
			
		$arr = array();	

		$total = 0;
		
		while ($row = mysql_fetch_object($result)) {
					
			$aRow = array();
			$aRow['name'] = convert_cyr_string($row->name,"koi8-r", "windows-1251");
			$aRow['count'] = $row->count;
			$aRow['procentage'] = 0;
			
			$total += (int) $aRow['count'];
			
			array_push($arr, $aRow);
		}
		
		foreach ($arr as &$v) {
			$v['procentage']  = intval(($v['count'] * 100)/$total);
		}
		
		
		return $arr;
		
	}
	
}
?>