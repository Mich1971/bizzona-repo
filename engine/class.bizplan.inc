<?php
  include_once("class.db.inc");
  
  define("NEW_BIZPLAN", 0);
  define("OPEN_BIZPLAN", 1);
  define("CLOSE_BIZPLAN", 2);
  define("LOOKED_BIZPLAN", 3);
  
  
  class BizPlan {
  	
     var $db = "";
     var $id = "";
     var $company_id = "";
     var $category_id = "";
     var $datecreate = "";
     var $cost = "";
     var $ccost = "";
     var $name = "";
     var $description = "";
     var $shortDescription = "";
     var $status_id = "";
     var $language_id = "";
     var $display_id = "";
     var $count_site = "";
     var $company_text = "";
     var $qview = "";
     var $sendEmail = "";

     function BizPlan() {
       global $DATABASE;
       $this->InitDB();
     } 

     function InitDB() {
       global $DATABASE;
       $this->db = new DB($DATABASE["HOSTNAME"], $DATABASE["DATABASENAME"], $DATABASE["DATABASEUSER"], $DATABASE["DATABASEPASSWORD"]);
     }

     function Insert() {
     	$sql = "insert into `bizplan` ( `company_id`,
     									`category_id`,
     									`company_text`,	
     									`datecreate`,
     									`cost`,
     									`ccost`,
     									`name`,
     									`description`,
     									`shortDescription`,
     									`status_id`,
     									`language_id`,
     									`display_id`,
     									`count_site`
     								  ) 
     								  values ('".$this->company_id."',
     								  		  '".$this->category_id."',
     								  		  '".$this->company_text."',
     								  		  NOW(),
     								  		  '".$this->cost."',
     								  		  '".$this->ccost."',
     								  		  '".$this->name."',
     								  		  '".$this->description."',
     								  		  '".$this->shortDescription."',
     								  		  '".$this->status_id."',
     								  		  '".$this->language_id."',
     								  		  '".$this->display_id."',
     								  		  '".$this->count_site."') ";
     	
       	$result = $this->db->Insert($sql);
       	return $result;


     }

     function Select($data) {
     	
       $limit = "";
       $where = " where 1 ";
       $order = " order  by ".$data["sort"]." desc";

       if ($data["offset"] === 0 && $data["rowCount"] === 0) {
       } else {
         $limit = " limit ".intval($data["offset"]).",".$data["rowCount"]."";
       } 
     	
       if (isset($data["category_id"])) {
         $where = $where." and bizplan.category_id=".intval($data["category_id"])." ";
       } 

       if (isset($data["company_id"])) {
         $where = $where." and bizplan.company_id=".intval($data["company_id"])." ";
       } 
       
       
       if (isset($data["status_id"])) {
         if ($data["status_id"] == "-1") {
           $where = $where." and bizplan.status_id in ('1') ";
         } else {
           $where = $where." and bizplan.status_id=".intval($data["status_id"])." ";
         }
       } 

       
     	
     	$sql = "select `bizplan`.*, `bizplan_company`.`name` as 'companyname', Category.icon AS 'categoryicon' from `bizplan` left join `bizplan_company` on (`bizplan_company`.`id`=`bizplan`.`company_id`)  LEFT JOIN Category ON ( Category.ID = bizplan.category_id ) ".$where." ".$order." ".$limit;
     	
     	$result = $this->db->Select($sql);

       	$array = Array();
       	while ($row = mysql_fetch_object($result)) {
        	$obj = new stdclass();

         	$obj->id = $row->id;
         	$obj->name = $row->name;
		 	$obj->description = $row->description;
		 	$obj->shortDescription = $row->shortDescription;
		 	$obj->company_id = $row->company_id;
		 	$obj->datecreate = strftime("%d %B %Y",  strtotime($row->datecreate));
		 	$obj->cost = trim(strrev(chunk_split (strrev($row->cost), 3,' '))); 
		 	$obj->ccost = $row->ccost;
		 	$obj->language_id = $row->language_id;
		 	$obj->display_id = $row->display_id;
		 	$obj->count_site = $row->count_site;		 	
		 	$obj->companyname = $row->companyname;
		 	$obj->qview = $row->qview;
		 	$obj->categoryicon = $row->categoryicon;
		 	$obj->category_id = $row->category_id;
		 	$obj->sendEmail = $row->sendEmail;
		 	
		 	$obj->status_id = $row->status_id;
		 	if($row->status_id == NEW_BIZPLAN)
		 	{
		 		$obj->status_text = "�����";
		 	}
		 	else if($row->status_id == OPEN_BIZPLAN)
		 	{
		 		$obj->status_text = "������";
		 	}
		 	else if($row->status_id == CLOSE_BIZPLAN)
		 	{
		 		$obj->status_text = "������";
		 	}
		 	else if($row->status_id == LOOKED_BIZPLAN)
		 	{
		 		$obj->status_text = "����������";
		 	}
         	$array[] = $obj;
       }
       
       return $array;     	

     }

   
     function Update() {
     	
     	$sql = "update `bizplan` set `name`='".$this->name."',
     								 `description`='".$this->description."',
     								 `shortDescription`='".$this->shortDescription."',
     								 `company_id`='".$this->company_id."',
     								 `category_id`='".$this->category_id."',
     								 `cost`='".$this->cost."',	
     								 `ccost`='".$this->ccost."',	
     								 `language_id`='".$this->language_id."',
     								 `display_id`='".$this->display_id."',
     								 `count_site`='".$this->count_site."',
     								 `status_id`='".$this->status_id."' where `id`='".$this->id."';";
     	
     	$result = $this->db->Select($sql);
     	
     }


     function Delete() {
     }


     function GetItem() {
     	
     	$sql = "select `bizplan`.* from `bizplan` where id=".$this->id.";";
     	$result = $this->db->Select($sql);
        $obj = new stdclass();
        
       	while ($row = mysql_fetch_object($result)) {
		 	$this->id = $obj->id = $row->id;
         	$this->name = $obj->name = $row->name;
		 	$this->description = $obj->description = $row->description;
		 	$this->shortDescription = $obj->shortDescription = $row->shortDescription;
		 	$this->company_id = $obj->company_id = $row->company_id;
		 	$this->category_id = $obj->category_id = $row->category_id;
		 	$this->datecreate = $obj->datecreate = $row->datecreate;
		 	$this->cost = $obj->cost = trim(strrev(chunk_split (strrev($row->cost), 3,' ')));
		 	$this->ccost = $obj->ccost = $row->ccost;
		 	$this->language_id = $obj->language_id = $row->language_id;
		 	$this->display_id = $obj->display_id = $row->display_id;
		 	$this->count_site = $obj->count_site = $row->count_site;		 	
		 	$this->status_id = $obj->status_id = $row->status_id;
		 	$this->qview = $obj->qview = $row->qview;
		 	$this->sendEmail = $obj->sendEmail = $row->sendEmail;
       }
       return $obj;     	
     }


     function IncrementQView($data) {
        $sql = "update `bizplan` set `qview`=`qview`+1 where id=".$this->id.";";
        $result = $this->db->Select($sql);
     }
     
     function CountByCompany()
     {
     	$sql = "select count(`id`) as 'count' from `bizplan` where 1 and `bizplan`.`company_id`='".$this->company_id."' and `bizplan`.`id` != ".$this->id." and `status_id` in (".OPEN_BIZPLAN.");";
     	$result = $this->db->Select($sql);
     	while ($row = mysql_fetch_object($result)) {
     		return $row->count;
     	}
     }
     
     function CountByCategory()
     {
     	$sql = "select count(`id`) as 'count' from `bizplan` where 1 and `bizplan`.`company_id`='".$this->company_id."' and `bizplan`.`category_id` != ".$this->category_id." and `status_id` in (".OPEN_BIZPLAN.");";
     	$result = $this->db->Select($sql);
     	while ($row = mysql_fetch_object($result)) {
     		return $row->count;
     	}
     }
     
     function SetSendEmail()
     {
        $sql = "update `bizplan` set `sendEmail`=".$this->sendEmail." where id=".$this->id.";";
        $result = $this->db->Select($sql);
     }
     
  }
?>