<?php

  include_once("class.db.inc");  


  class BlockIP {
     var $db = "";
     var $message = "";
     var $answer = ""; 
     var $limit_timepost = 30;

     function BlockIP() {
       global $DATABASE;
       $this->InitDB();
     }  

     function InitDB() {
       global $DATABASE;
       $this->db = new DB($DATABASE["HOSTNAME"], $DATABASE["DATABASENAME"], $DATABASE["DATABASEUSER"], $DATABASE["DATABASEPASSWORD"]);
     }

     function Close() {
     	$this->db->Close();
     }
     
     function BlockDown($aParams) {
        $sql = " insert ignore into block_ip (`ip`,`created`) values ('".$aParams['ip']."', NOW());";
        $result = $this->db->Insert($sql);
     }

     function BlockUp($aParams) {
        $sql = " delete from block_ip  where `ip`='".$aParams['ip']."' limit 1;";
        $result = $this->db->Sql($sql);
     }
     
     function YouAreBlocking($aParams) {
        $sql = " select ip from block_ip where ip='".$aParams['ip']."';"; 
        
        $result = $this->db->Select($sql);
        
        $block = false;
        
        while ($row = mysql_fetch_object($result)) {
            $block = true;
        }
        
        if(!$block) {
            $block = $this->CountPost();
        }
        
        return $block;
     }
      
     function CountPost() {
            
         global $_SESSION, $_SERVER, $_POST;
          
         if(isset($_SESSION['timepost']) && (time() - $_SESSION['timepost'] <= $this->limit_timepost) && isset($_POST) && count($_POST) > 0) {
             
             $this->BlockDown(array(
                 'ip' => $_SERVER['REMOTE_ADDR']
             ));
             
             return true;
         } 
         
         return false;
     }
     
     
     function Select() {
     	$sql = " select * from block_ip  where 1 order by created desc;";
     	$result = $this->db->Select($sql);
        $array = Array();
        while ($row = mysql_fetch_object($result)) {
            $array[] = array(
               'ip' => $row->ip, 
               'created' => $row->created,
            );
        }
        
        return $array;
     }
     
  }   