<?php
	include_once("class.db.inc");  
	require_once("Cache/Lite.php");
    
	class SubCategory_Lite {
		var $db = "";
		var $ID = "";
		var $aGetItem = array();
		var $aGetName = array();
		var $aKeySell = array();
		var $aKeyEquipment = array();

		function SubCategory_Lite() {
			global $DATABASE;
			$this->InitDB();
		}

		function InitDB() {
			global $DATABASE;
			$this->db = new DB($DATABASE["HOSTNAME"], $DATABASE["DATABASENAME"], $DATABASE["DATABASEUSER"], $DATABASE["DATABASEPASSWORD"]);
		}

		function Select($data, $sqlDisplay = false) {
			if ($data["offset"] === 0 && $data["rowCount"] === 0) {
				if (isset($data["sort"])) {
					$sql = "Select * from SubCategory where 1 order  by ".$data["sort"]." desc; ";
				} else { 
					$sql = "Select * from SubCategory where 1; ";
				}
			} else {
				
				if(intval($data["rowCount"]) > 20) { $data["rowCount"] = 5; }
				if(intval($data["offset"]) > 50000) { $data["offset"] = 0; } 				
				
				$sql = "Select * from SubCategory where 1 order  by ".$data["sort"]."  desc limit ".intval($data["offset"]).",".$data["rowCount"].";";
			} 

			if ($sqlDisplay) {
				echo $sql;
			}

			$result = $this->db->Select($sql);

			$array = Array();
			while ($row = mysql_fetch_object($result)) {
				$obj = new stdclass();
				$obj->ID = $row->ID;
				$obj->Name = $row->Name;
				$obj->CategoryID = $row->CategoryID;
				$obj->enabled =  $row->enabled;
				$array[] = $obj;
			}
			return $array;
		}

		function SelectSearch($data, $sqlDisplay = false)
		{
			$sql = "Select * from SubCategory where 1 and enabled = '0' order  by Name asc; ";
			$result = $this->db->Select($sql);
			$array = Array();
			while ($row = mysql_fetch_object($result)) {
				$obj = new stdclass();
				$obj->ID = $row->ID;
				$obj->Name = $row->Name;
				$obj->CategoryID = $row->CategoryID;
				$array[] = $obj;
			}
			return $array;
		}
     

     function GetNameList() {
     	
		$aName = array();
		
		$options = array(
    		'cacheDir' => PERSISTENTCACHE."/",
    		'lifeTime' => 2592000,
    		'pearErrorMode' => CACHE_LITE_ERROR_DIE
		);
		
		$cache = new Cache_Lite($options);
		
		if ($data = $cache->get('subcategorynames')) {
			
			$aName = unserialize($data);

		} else { 
		
			$sql = "Select ID, Name from SubCategory;";
			$result = $this->db->Select($sql);
			while ($row = mysql_fetch_object($result)) {
				$aName[$row->ID] = $row->Name;
			}
			$cache->save(serialize($aName));
		}
		
		return 	$aName;     	
     	
     }		
		
	function GetName($id) {
		$name = "";
     	if(isset($this->aGetName[$id])) {
     		$name = $this->aGetName[$id];	
     	} else {
     		
     		$result = $this->GetNameList();
     		$this->aGetName[$id] = $result[$id];
     		$name = $this->aGetName[$id];
		}
		return $name;
     }     
     
    function GetKeySellList() {

		$akeysell = array();
		
		$options = array(
    		'cacheDir' => PERSISTENTCACHE."/",
    		'lifeTime' => 2592000,
    		'pearErrorMode' => CACHE_LITE_ERROR_DIE
		);
		
		$cache = new Cache_Lite($options);
		
		if ($data = $cache->get('subcategorykeyselllist')) {
			$akeysell = unserialize($data);

		} else { 
		
			$sql = "Select ID, keysell from SubCategory;";
			$result = $this->db->Select($sql);
			while ($row = mysql_fetch_object($result)) {
				$akeysell[$row->ID] = $row->keysell;
			}
			$cache->save(serialize($akeysell));
		}
		
		return 	$akeysell;    	
    	
    } 

    function GetKeyEquipmentList() {

		$akeyEquipment = array();
		
		$options = array(
    		'cacheDir' => PERSISTENTCACHE."/",
    		'lifeTime' => 2592000,
    		'pearErrorMode' => CACHE_LITE_ERROR_DIE
		);
		
		$cache = new Cache_Lite($options);
		
		if ($data = $cache->get('subcategorykeyequipmentlist')) {
			$akeyEquipment = unserialize($data);

		} else { 
		
			$sql = "Select ID, keysellequipment from SubCategory;";
			$result = $this->db->Select($sql);
			while ($row = mysql_fetch_object($result)) {
				$akeyEquipment[$row->ID] = $row->keysellequipment;
			}
			$cache->save(serialize($akeyEquipment));
		}
		
		return 	$akeyEquipment;    	
    	
    }    
      
	function GetKeyEquipmentOpt($id) {
		$keyEquipment = "";
     	if(isset($this->aKeyEquipment[$id])) {
     		$keyEquipment = $this->aKeyEquipment[$id];	
     	} else {
     		
     		$result = $this->GetKeyEquipmentList();
     		$this->aKeyEquipment[$id] = $result[$id];
     		$keyEquipment = $this->aKeyEquipment[$id];
			
		}
		return $keyEquipment;
     }    
     
	function GetKeySellOpt($id) {
		$keysell = "";
     	if(isset($this->aKeySell[$id])) {
     		$keysell = $this->aKeySell[$id];	
     	} else {
     		
     		$result = $this->GetKeySellList();
     		$this->aKeySell[$id] = $result[$id];
     		$keysell = $this->aKeySell[$id];
			
		}
		return $keysell;
     }    
	
     function GetItem($data) {
			$obj = new stdclass();
			if(isset($this->aGetItem[$data["ID"]])) {
				$obj = $this->aGetItem[$data["ID"]];
			} else {
				$sql = "Select ID, Name, titlekeybuy, titlekeysell, keysell, keybuy, keysellequipment, url, seoarticle from SubCategory where ID=".trim($data["ID"]).";";
				//echo $sql; 
				$result = $this->db->Select($sql);
				while ($row = mysql_fetch_object($result)) {
					$obj->ID = $row->ID;
					$obj->Name = $row->Name;
					$obj->Url = $row->url; 
					//$obj->Count = $row->Count;
         			$obj->titlekeysell = $row->titlekeysell;
         			$obj->titlekeybuy = $row->titlekeybuy;
         			$obj->keysell = $row->keysell;
         			$obj->keybuy = $row->keybuy;
         			$obj->keysellequipment = $row->keysellequipment;
         			$obj->seoarticle = $row->seoarticle;
				}
				$this->aGetItem[$data["ID"]] = $obj;
			}
			return $obj;
		}

		function GetIcon() {
			$sql = "select icon from SubCategory where ID=".intval($this->ID)."";
			$result = $this->db->Select($sql);
			while ($row = mysql_fetch_object($result)) {
				return $row->icon;				
			}
		}

		
		function GetKeySellEquipment() {
			$sql = "select keysellequipment from SubCategory where ID=".$this->ID.";";
			$result = $this->db->Select($sql);
			
			while ($row = mysql_fetch_object($result)) {
				return $row->keysellequipment;
			}
			
		}
		
		function GetSubcategoryForFullSearch() {
			
			$array = array();
			
			$sql = "select SubCategory.ID as 'ID', count(SubCategory.ID) as 'count', SubCategory.Name, Category.Name as 'ParentName', Category.ID as 'ParentID' from SubCategory, Sell, Category where 1 and Category.ID=SubCategory.CategoryID  and SubCategory.ID=Sell.SubCategoryID and Sell.StatusID in (1,2) group by SubCategory.ID order by count desc;";
			$result = $this->db->Select($sql);
			while ($row = mysql_fetch_object($result)) {

				if(!isset($array["".$row->ParentID.""])) {
					
					$array["".$row->ParentID.""]["title"] = $row->ParentName;
					$arr_child = array();
					$arr_child["".$row->ID.""]["title"] = $row->Name;
					$arr_child["".$row->ID.""]["count"] = $row->count;
					$array["".$row->ParentID.""]["child"] = $arr_child;					
					
				} else {
					$arr_child = $array["".$row->ParentID.""]["child"];
					$arr_child["".$row->ID.""]["title"] = $row->Name;
					$arr_child["".$row->ID.""]["count"] = $row->count;
					$array["".$row->ParentID.""]["child"] = $arr_child;
				}
				
			}
			
			return $array;
		}
		
		function Close() {
			$this->db->Close();
		}
	}
?>