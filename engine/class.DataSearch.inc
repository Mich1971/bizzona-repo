<?php
    include_once("class.db.inc");

    class DataSearch {

        public $showFields = array(
                                          array(
                                            'field' => 'id', 
                                            'title' => '����� ����������', 
                                            'sell' => 'id',
                                          ),
                                          array(
                                            'field' => 'name', 
                                            'title' => '������������ ����������',
                                            'sell' => 'NameBizID', 
                                          ),
                                          array(
                                             'field' => 'price', 
                                             'title' => '����',
                                             'sell' => 'CostID',    
                                          ), 
                                          array(
                                              'field' => 'date', 
                                              'title' => '���� ��������',
                                              'sell' => 'date', 
                                          ),
                                          array(
                                              'field' => 'email', 
                                              'title' => 'Email',
                                              'sell' => 'EmailID', 
                                          ), 
                                          array(
                                              'field' => 'phone', 
                                              'title' => '�������',
                                              'sell' => 'PhoneID',
                                          ),
                                          array(
                                              'field' => 'fio', 
                                              'title' => '���',
                                              'sell' => 'FML',
                                          ), 
                                    );

                                            
        function __construct() {
            global $DATABASE;
            $this->InitDB();
        }

        function InitDB() {

            global $DATABASE;

            $this->db = new DB($DATABASE["HOSTNAME"], $DATABASE["DATABASENAME"], $DATABASE["DATABASEUSER"], $DATABASE["DATABASEPASSWORD"]);
        }

        function __destruct() {

        }
        
        public function Searching($aParams) {
            
            if(isset($aParams['type'])) {
                
                if($aParams['type'] == '1' /*sell*/) {
                    
                    return $this->SearchSell($aParams);
                }
                
            }
            return array();
        }
        
        private function SearchSell($aParams) {
            
            $arr = array();
            
            $where = "where 1 ";
            
            if(isset($aParams['datestart'])) {
                $where .=  " and date >= '".$aParams['datestart']."'";
            }
            
            if(isset($aParams['datestop'])) {
                $where .=  " and date <= '".$aParams['datestop']."'";
            } 

            if(isset($aParams['broker'])) {
                $where .= " and brokerId > 0 "; 
            }

            if(isset($aParams['owner'])) {
                $where .= " and  (brokerId is null or brokerId <= 0) ";
            }
            
            if(isset($aParams['coststart'])) {
                
                if($aParams['costId'] == "usd") {
                    $aParams['coststart'] *= 32;
                } else if ($aParams['costId'] == "eur") {
                    $aParams['coststart'] *= 40;
                }
                
                $where .= " and costseach >= ".$aParams['coststart']." ";
            }
            if(isset($aParams['coststop'])) {
                
                if($aParams['costId'] == "usd") {
                    $aParams['coststop'] *= 32;
                } else if ($aParams['costId'] == "eur") {
                    $aParams['coststop'] *= 40;
                }
                
                $where .= " and costseach <= ".$aParams['coststop']." ";
            }

            
            
            $order = " order by id desc ";
            
            $this->_sql = "select costseach, id, NameBizID, CostID, cCostID, date, EmailID, PhoneID, FML from Sell {$where} {$order} limit 1000;";
            
            //echo $this->_sql;
            
            $result = $this->db->Select($this->_sql);
            
            while ($row = mysql_fetch_object($result)) {
            
                $_temp = array();
                
                $_temp['id'] = $row->id;
                $_temp['name'] = $row->NameBizID;
                $_temp['price'] = $row->CostID;
                $_temp['cprice'] = $row->cCostID;
                $_temp['date'] = $row->date;
                $_temp['email'] = $row->EmailID;
                $_temp['phone'] = $row->PhoneID;
                $_temp['fio'] = $row->FML;
                
                $arr[] = $_temp;
            }
            
            return $arr;
        }
        
    }


?>
