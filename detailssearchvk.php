<?
	global $DATABASE;
 
	include_once("./phpset.inc");
	include_once("./engine/functions.inc"); 

	$count_sql_call = 0;
	$start = get_formatted_microtime(); 
	$base_memory_usage = memory_get_usage();	

	AssignDataBaseSetting();
   
	include_once("./engine/class.category_lite.inc"); 
	$category = new Category_Lite();

	include_once("./engine/class.country_lite.inc"); 
	$country = new Country_Lite();	

	include_once("./engine/class.city_lite.inc"); 
	$city = new City_Lite();

	include_once("./engine/class.sell_lite.inc"); 
	$sell = new Sell_Lite();	

	include_once("./engine/class.buy_lite.inc"); 
	$buyer = new Buyer_Lite();	
  
  	include_once("./engine/class.metro_lite.inc"); 
  	$metro = new Metro_Lite();	
  
  	include_once("./engine/class.district_lite.inc"); 
  	$district = new District_Lite();	

  	include_once("./engine/class.subcategory_lite.inc");
  	$subcategory = new SubCategory_Lite();	

  	include_once("./engine/class.streets_lite.inc");
  	$streets = new Streets_Lite();	

	include_once("./engine/class.topparameters_lite.inc"); 
	$topparameters = new TopParameters_Lite();

	include_once("./engine/class.frontend.inc");
	
	require_once("./libs/Smarty.class.php");
	$smarty = new Smarty;
	$smarty->template_dir = "./templates";
	$smarty->compile_dir  = "./templates_c";
	//$smarty->cache_dir = "./cache";
	$smarty->compile_check = true;
	$smarty->debugging     = false;

	
	
	require_once("./regfuncsmarty.php");
  
	if(isset($_GET["ID"])) {
		$_GET["ID"] = intval($_GET["ID"]);
	}
  
	if(isset($_GET["rowCount"])) {
		$_GET["rowCount"] = intval($_GET["rowCount"]);
	}

	if(isset($_GET["offset"])) {
		$_GET["offset"] = intval($_GET["offset"]);
	}

	if(isset($_GET["streetID"])) {
		$_GET["streetID"] = intval($_GET["streetID"]);
	}
  
	if(isset($_GET["cityId"])) {
		$_GET["cityId"] = intval($_GET["cityId"]);
	}	

	if(isset($_GET["subCityId"])) {
		$_GET["subCityId"] = intval($_GET["subCityId"]);
	}	

	if(isset($_GET["catId"])) {
		$_GET["catId"] = intval($_GET["catId"]);
	}	
	
	if(isset($_GET["subCatId"])) {
		$_GET["subCatId"] = intval($_GET["subCatId"]);
	}	
	
	$name_words = array("turagenstvo", "restoran", "parik", "krasota", "otel");
	if(isset($_GET["name"])) {
		if(in_array($_GET["name"], $name_words)) {
		} else {
			$_GET["name"] = "";
		}
	}
  
	$action_words = array("category", "city", "price", "topbiz");
	if(isset($_GET["action"])) {
		if(in_array($_GET["action"], $action_words)) {
		
		} else {
			$_GET["action"] = "";
		}
	}

	$sell->districtID = (isset($_GET["districtId"]) ? intval($_GET["districtId"]) : 0);
	$sell->metroID = (isset($_GET["metroId"]) ? intval($_GET["metroId"]) : 0);
	$sell->SubCategoryID = (isset($_GET["subCatId"]) ? intval($_GET["subCatId"]) : 0);
	$sell->CategoryID = (isset($_GET["catId"]) ? intval($_GET["catId"]) : 0);
	
	if(isset($_GET["subCityId"]) and intval($_GET["subCityId"]) > 0) {
		$sell->CityID = intval($_GET["subCityId"]);
	} else {
		$sell->CityID = (isset($_GET["cityId"]) ? intval($_GET["cityId"]) : 0);	
	}
	$title = $sell->GetTitleForUserFriendlySelect();
	
	$title = (strlen($title) > 0 ? $title : "������� �������� �������");
	
	$smarty->assign("title", $title);
	 
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
  <HEAD>
   <title><?=$title;?></title>		
	<META NAME="Description" CONTENT="<?=$title;?>">
        <meta name="Keywords" content="<?=$title;?>"> 
        <LINK href="http://www.bizzona.ru/general.css" type="text/css" rel="stylesheet">
        <meta http-equiv="content-type" content="text/html; charset=windows-1251"/>
        <link rel="shortcut icon" href="http://www.bizzona.ru/images/favicon.ico">
    </HEAD>
<body>
<div>
<table class="w" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td valign="top">
        	<?
				ShowTopParametersVK();
        	?>
            <?
                    $data = array();
                    $dataPage = array(); 
                    $pagesplit = GetPageSplit();

                    if (isset($_GET["offset"])) {
                      $data["offset"] = intval($_GET["offset"]);
                    } else {
                      $data["offset"] = 0;
                    }
                    if (isset($_GET["rowCount"])) { 
                      $data["rowCount"] = intval($_GET["rowCount"]);
                    } else {
                      $data["rowCount"] = $pagesplit;
                    }

                    $data["sort"] = "ID";

                    
                    $data["cityId"] = intval($_GET["cityId"]);
                    $data["subCityId"] = intval($_GET["subCityId"]);
                    $data["catId"] = intval($_GET["catId"]);
                    $data["subCatId"] = intval($_GET["subCatId"]);
                    $data["metroId"] = intval($_GET["metroId"]);
                    $data["districtId"] = intval($_GET["districtId"]);
                    $data["brokerId"] = intval($_GET["brokerId"]);
                    
           
                    $_GET["cityId"] = $data["cityId"];
                    $_GET["subCityId"] = $data["subCityId"];
                    $_GET["catId"] = $data["catId"];
                    $_GET["subCatId"] = $data["subCatId"];
                    $_GET["metroId"] = $data["metroId"];
                    $_GET["districtId"] = $data["districtId"];
                    $_GET["brokerId"] = $data["brokerId"];
                    
                    if(isset($_GET["par1"])) {
                    	$data["par1"] = intval($_GET["par1"]);
                    	$_GET["par1"] = $data["par1"];
                    	$dataPage["par1"] = $data["par1"];
                    }
                    if(isset($_GET["par2"])) {
                    	$data["par2"] = intval($_GET["par2"]);
                    	$_GET["par2"] = $data["par2"];
                    	$dataPage["par2"] = $data["par2"];
                    }
                    if(isset($_GET["par3"])) {
                    	$data["par3"] = intval($_GET["par3"]);
                    	$_GET["par3"] = $data["par3"];
                    	$dataPage["par3"] = $data["par3"];
                    }
                    if(isset($_GET["par4"])) {
                    	$data["par4"] = intval($_GET["par4"]);
                    	$_GET["par4"] = $data["par4"];
                    	$dataPage["par4"] = $data["par4"];
                    }
                    if(isset($_GET["par5"])) {
                    	$data["par5"] = intval($_GET["par5"]);
                    	$_GET["par5"] = $data["par5"];
                    	$dataPage["par5"] = $data["par5"];
                    }
                    if(isset($_GET["par6"])) {
                    	$data["par6"] = intval($_GET["par6"]);
                    	$_GET["par6"] = $data["par6"];
                    	$dataPage["par6"] = $data["par6"];
                    }
                    
                    reset($data);

                    $dataPage["offset"] = 0;
                    $dataPage["rowCount"] = 0;
                    $dataPage["sort"] = "ID";
                    $dataPage["cityId"] = $data["cityId"];
                    $dataPage["subCityId"] = $data["subCityId"];
                    $dataPage["catId"] = $data["catId"];
                    $dataPage["subCatId"] = $data["subCatId"];
                    $dataPage["metroId"] = $data["metroId"];
                    $dataPage["districtId"] = $data["districtId"];
                    $dataPage["brokerId"] = $data["brokerId"];
                    
                    $resultPage = $sell->DetailsCountSelect($dataPage);
                    
                    
                    $smarty->assign("CountRecord",$resultPage);
                    $smarty->assign("CountSplit", $pagesplit);
                    $smarty->assign("CountPage", ceil($resultPage/5));

                    $data["StatusID"] = "-1";
                    $data["sort"] = "pay_datetime";

                    $result = $sell->DetailsSelect($data);

                    $smarty->assign("data", $result);

				  $output_pagesplit = $smarty->fetch("./site/pagesplitdetailsvk.tpl", $_SERVER["REQUEST_URI"]);
				  echo fminimizer($output_pagesplit);
                  
                  
				  $saveme = $smarty->fetch("./site/saveme.tpl");
				  echo $saveme;
                 
				  $smarty->assign('colorsell', FrontEnd::ListColorSet());
 
				  $output_ilistsell = $smarty->fetch("./site/ilistsellvk.tpl", $_SERVER["REQUEST_URI"]);
				  echo fminimizer($output_ilistsell);
				   
                   if($resultPage <= 0) {
                   	?>
                   		<div>
                    		<div style="font-family:Arial;font-size:13pt;color:#7C3535;font-weight:bold;padding-top:4pt;margin:0pt;">�� ������� ������� �������� �� ������� �� �������</div>
                    	</div>		
                    <?	
                    }
               ?>
        </td>
    </tr>
</table>
</div>
</body>
</html>
<?
	$sell->Close();
	$category->Close();
	$city->Close();
	$country->Close();
	$buyer->Close();
	$metro->Close();
	$district->Close();
	$subcategory->Close();
	$streets->Close();
?>
